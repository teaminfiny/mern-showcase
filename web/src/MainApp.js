import React from 'react';
import {ConnectedRouter} from 'connected-react-router'
import {Provider} from 'react-redux';
import {Route, Switch} from 'react-router-dom';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import configureStore, {history} from './store';
import './firebase/firebase';
import App from './containers/App';
import '../src/Seller.css'
import './App.css'
import 'react-credit-cards/es/styles-compiled.css'
import "react-datepicker/dist/react-datepicker.css";
import 'font-awesome/css/font-awesome.min.css';
import 'react-input-range/lib/css/index.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';

export const store = configureStore();

const MainApp = () =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
    <MuiPickersUtilsProvider utils={DateFnsUtils}  >
      <Switch>
        <Route path="/" component={App}/>
      </Switch>
      </MuiPickersUtilsProvider>
    </ConnectedRouter>
  </Provider>;


export default MainApp;
import {
  CHANGE_TAB
} from 'constants/ActionTypes';

export function changeTab(tabValue) {
  return {type: CHANGE_TAB, tabValue};
}

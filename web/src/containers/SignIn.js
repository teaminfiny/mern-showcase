import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { Col, Row } from 'reactstrap';
import TextBox from '../components/textBox'
import { Field, reduxForm } from 'redux-form'
import { validateData, required, emailField } from '../constants/validations'


import {
  hideMessage,
  showAuthLoader,
  userSignIn
} from 'actions/Auth';

class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      user_type: 'seller',
      showNotification: true
    }
  }


  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  gotoSignup = () => {
    if (this.props.location.pathname === '/login') {
      this.props.history.push('/buyersignup')
    }
    else if (this.props.location.pathname === '/signin') {
      this.props.history.push('/signup')
    }
  }

  gotoForgotPassword = () => {
    this.props.history.push('/forgotPassword')
      localStorage.setItem( 'user','seller')
  }

  handleSigninOnEnter = (e) => {
    let email = this.state.email;
    let password = this.state.password;
    if (e.key === 'Enter' && email !== '' && password !== '') {

    }
  }

  handleEmailChange = (e, newValue, previousValue, key) => {
    if (this.state.showNotification === false) {
      this.setState({showNotification:true});
    }
    this.setState({ [key]: (e.target.value).toLowerCase() })
  }

  handlePasswordChange = (e, newValue, previousValue, key) => {
    if (this.state.showNotification === false) {
      this.setState({showNotification:true});
    }
    this.setState({ [key]: e.target.value })
  }


  onSubmit = () => {
    const { email, password, user_type } = this.state;
    if (this.state.showNotification) {
      localStorage.clear();
      this.props.showAuthLoader();
      this.props.userSignIn({ email, password, user_type, history: this.props.history });
      this.setState({showNotification:false});
    }
  }

  render() {


    const { handleSubmit } = this.props

    return (
      <div className="col-xl-8 col-lg-8 signinContainer" >
        <div className="jr-card signinCard">
          <div className="animated slideInUpTiny animation-duration-3">
            <div className="login-header mb-0">
              <a className="app-logo" href='javascript:void(0);' title="Jambo">
                <img src={require("assets/images/medimny.png")} className='medilogo' alt="Medimny" title="Medimny" />
              </a>
            </div>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <Row>

                <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                  <Field name="email" type="text"
                    component={TextBox} label="Email"
                    validate={emailField}
                    value={this.state.email}
                    onChange={(event, newValue, previousValue) => this.handleEmailChange(event, newValue, previousValue, 'email')}
                  />
                </Col>

                <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                  <Field name="password" type="password"
                    component={TextBox} label="Password"
                    validate={required}
                    value={this.state.password}
                    onChange={(event, newValue, previousValue) => this.handlePasswordChange(event, newValue, previousValue, 'password')}
                  />
                </Col>

                <Col sm={6} md={6} lg={6} xs={6} xl={6} >
                  <Button type='submit' variant='contained' color='primary' className='m-2 signin' size='medium'  >LOGIN</Button>
                </Col>

                <Col sm={6} md={6} lg={6} xs={6} xl={6} className='d-flex justify-content-end align-items-center'>
                  <a href='javascript:void(0);' onClick={this.gotoForgotPassword}><span color='primary'>Forgot your password?</span></a>
                </Col>

                <Col sm={8} md={4} lg={8} xs={8} xl={8} className='d-flex  align-items-center'>
                  <a href='javascript:void(0);' onClick={this.gotoSignup}><span>Create an account</span></a>
                </Col>

              </Row>
              <div>
              </div>
            </form>
          </div>

        </div>
      </div>

    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser }
};

SignIn = reduxForm({
  form: 'fieldLevelValidation',// a unique identifier for this form
  validateData
})(SignIn)
SignIn = connect(
  mapStateToProps,
  {
    userSignIn,
    hideMessage,
    showAuthLoader,
  }               // bind account loading action creator
)(SignIn)
export default SignIn



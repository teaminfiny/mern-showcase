import React, { Component } from 'react';
import logo from '../assets/images/medimny.png'
import { getPromotionsListing } from 'actions/buyer'
import { connect } from 'react-redux';


class UnderMaintenance extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    componentDidMount(){
    this.interval = setInterval(() => {
        this.props.getPromotionsListing({ history: this.props.history })
        if(this.props.promotionsList.length > 0 ){
            window.location.href = "/";
        }
    }, 4000) 
    }
    componentWillUnmount() {
        clearInterval(this.interval);
      }
    render() { 
        const { promotionsList } = this.props;
        let online = window.navigator.onLine;
        return ( 
            online ?
            <React.Fragment>
                <div className="col-xl-8 col-lg-8 signinContainer" >
                    <div className="jr-card UnderMaintenance">
                        <div className="animated slideInUpTiny animation-duration-3 ">

                            <div className="login-header mb-0">
                            <img src={logo} className="maintenanceImage"/>
                                <h2 style={{ textAlign: "center", color: "#5b5b5b"}}>
                                    We are upgrading our system to enhance user experience, we will be back soon.
                                </h2>                                
                            </div>

                        </div>
                    </div>
                </div>
            </React.Fragment>
            :
            <React.Fragment>
                <div className="col-xl-8 col-lg-8 signinContainer" >
                    <div className="jr-card UnderMaintenance">
                        <div className="animated slideInUpTiny animation-duration-3 ">

                            <div className="login-header mb-0">
                            <img src={logo} className="maintenanceImage"/>
                                <h2 style={{ textAlign: "center", color: "#5b5b5b"}}>
                                    Check your internet connection & try again.
                                </h2>                                
                            </div>

                        </div>
                    </div>
                </div>
            </React.Fragment>

         );
    }
}
const mapStateToProps = ({ buyer }) => {
    const {  promotionsList } = buyer;
    return { promotionsList }
  };
export default connect(mapStateToProps, { getPromotionsListing})(UnderMaintenance);
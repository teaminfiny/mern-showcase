import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import Switch from '@material-ui/core/Switch';
import IntlMessages from 'util/IntlMessages';
import CustomScrollbars from 'util/CustomScrollbars';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import moment from 'moment';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import AppConfig from '../../constants/config'
import { NotificationManager } from 'react-notifications';
import DatePicker from "react-datepicker";
import RenderReactDatePicker from '../../components/RenderReactDatePicker';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { getUserDetail } from '../../actions/seller'

class SidenavContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedA: false,
      open: false,
      selectedDate: moment().format(),
      vacationDate: '',
      vacationMode: false
    };
  }

  componentDidMount() {
    const { history } = this.props;
    const that = this;
    const pathname = `${history.location.pathname}`;// get current path
    const menuLi = document.getElementsByClassName('menu');
    for (let i = 0; i < menuLi.length; i++) {
      menuLi[i].onclick = function (event) {

        const parentLiEle = that.closest(this, 'li');
        if (menuLi[i] && menuLi[i].classList.contains('menu') && parentLiEle !== null) {
          event.stopPropagation();

          if (menuLi[i].classList.contains('open')) {
            menuLi[i].classList.remove('open', 'active');
          } else {
            menuLi[i].classList.add('open', 'active');
          }
        } else {
          for (let j = 0; j < menuLi.length; j++) {
            const parentLi = that.closest(this, 'li');
            if (menuLi[j] !== this && (parentLi === null || !parentLi.classList.contains('open'))) {
              menuLi[j].classList.remove('open');
            } else {
              if (menuLi[j].classList.contains('open')) {
                menuLi[j].classList.remove('open');
              } else {
                menuLi[j].classList.add('open');
              }
            }
          }
        }
      }
    }

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  componentWillReceiveProps(nextProps) {
    const { history, userDetails } = nextProps;
    const pathname = `${history.location.pathname}`;// get current path
    if (nextProps.userDetails !== this.props.userDetails) {
      this.setState({
        checkedA: nextProps.userDetails ? nextProps.userDetails.vaccation ? nextProps.userDetails.vaccation.vaccationMode : false : false,
        vacationMode: nextProps.userDetails ? nextProps.userDetails.vaccation ? nextProps.userDetails.vaccation.vaccationMode : false : false,
        selectedDate: nextProps.userDetails ? nextProps.userDetails.vaccation ? nextProps.userDetails.vaccation.tillDate ? moment(nextProps.userDetails.vaccation.tillDate).format() : moment().format() : moment().format() : moment().format(),
        vacationDate: nextProps.userDetails ? nextProps.userDetails.vaccation ? nextProps.userDetails.vaccation.tillDate ? moment(nextProps.userDetails.vaccation.tillDate).format() : moment().format() : moment().format() : moment().format()
      })
    }
    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }

    } catch (error) {

    }
  }

  closest(el, selector) {
    try {
      let matchesFn;
      // find vendor prefix
      ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
        if (typeof document.body[fn] === 'function') {
          matchesFn = fn;
          return true;
        }
        return false;
      });

      let parent;

      // traverse parents
      while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
          return parent;
        }
        el = parent;
      }
    } catch (e) {

    }

    return null;
  }

  handleChange = name => (event, checked) => {
    if (checked) {
      this.setState({ [name]: checked, open: !this.state.open });
    } else {
      this.setState({ [name]: checked, open: !this.state.open });
    }
  };

  handleRequestClose = () => {
    this.setState({ open: false, vacationMode: this.props.userDetails ? this.props.userDetails.vaccation ? this.props.userDetails.vaccation.vaccationMode === true ? true : false : false : false, vacationDate: this.props.userDetails ? this.props.userDetails.vaccation ? this.props.userDetails.vaccation.vaccationMode === true ? this.props.userDetails.vaccation.tillDate : '' : '' : '', checkedA: this.props.userDetails ? this.props.userDetails.vaccation ? this.props.userDetails.vaccation.vaccationMode : false : false, })
  }

  handleRequestSave = () => {
    let tempDate = this.state.selectedDate;
    axios({
      method: 'post',
      url: `${AppConfig.baseUrl}seller/enableDisableVaccation`,
      headers: {
        token: localStorage.getItem('token')
      },
      data: {
        vacationMode: !this.state.vacationMode,
        tillDate: moment(this.state.selectedDate).format()
      }
    })
      .then((result) => {

        if (result.data.error === false) {
          NotificationManager.success(result.data.title)
          this.setState({ open: false, vacationMode: !this.state.vacationMode, vacationDate: !this.state.vacationMode ? moment(tempDate).format() : '', selectedDate: moment().format() });
          this.props.getUserDetail({history : this.props.history})
        } else {
          NotificationManager.error(result.data.title)
        }

      })


  }

  handleDateChange = (e, key) => {
    this.setState({ [key]: e });
  }

  render() {
    const { open, selectedDate, checkedA, vacationDate, vacationMode } = this.state;
    const { history } = this.props;
    return (
      <CustomScrollbars className=" scrollbar">
        <ul className="nav-menu">

          <li className="nav-header">
            {/* <IntlMessages id="Menu" /> */}
          </li>
          {
            this.props.sidebar.length > 0 && this.props.sidebar.map((data) => {
              return <li key={data.name} className={history.location.pathname.includes(data.pathname) ? 'open menu no-arrow' : 'menu no-arrow'} >
                <NavLink to={data.pathname}>
                  <i className={`zmdi ${data.icon} zmdi-hc-fw`} />
                  <span className="nav-text">{data.name}</span>
                </NavLink>
              </li>
            })
          }
          {

         /*
          <li className={history.location.pathname.includes('/seller/orders/') === true ? 'open menu no-arrow' : 'menu no-arrow'}>
            <NavLink to="/seller/orders">
              <i className="zmdi zmdi-truck zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="Orders" /></span>
            </NavLink>
          </li>
          <li className={history.location.pathname === '/seller/inventory' ? 'open menu no-arrow' : 'menu no-arrow'}>
            <NavLink to="/seller/inventory">
              <i className="zmdi zmdi-local-store zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="Inventory" /></span>
            </NavLink>
          </li>

          <li className={history.location.pathname === '/seller/settlement' ? 'open menu no-arrow' : 'menu no-arrow'}>
            <NavLink to="/seller/settlement">
              <i className="zmdi zmdi-balance-wallet zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="Settlements" /></span>
            </NavLink>
          </li>


          <li className={history.location.pathname === '/seller/product-Request' ? 'open menu no-arrow' : 'menu no-arrow'}>
            <NavLink to="/seller/product-Request">
              <i className="zmdi zmdi-collection-item zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="Product Requests" /></span>
            </NavLink>
          </li>

          <li className={history.location.pathname === '/seller/staffs' ? 'open menu no-arrow' : 'menu no-arrow'}>
            <NavLink to="/seller/staffs">
              <i className="zmdi zmdi-account zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="staff Members" /></span>
            </NavLink>
          </li>

          <li className={history.location.pathname === '/seller/groups' ? 'open menu no-arrow' : 'menu no-arrow'}>
            <NavLink to="/seller/groups">
              <i className="zmdi zmdi-accounts zmdi-hc-fw" />
              <span className="nav-text"><IntlMessages id="groups" /></span>
            </NavLink>
          </li>
         */}
          {
            this.props.isVaccatioMode ?
              <li className=" no-arrow">
                <span className="vacationMode">
                  <i className="zmdi zmdi-card-travel zmdi-hc-fw" />
                  <span className="nav-text"><IntlMessages id="Vacation" />
                    <Switch
                      color='primary'
                      checked={this.state.checkedA}
                      onChange={this.handleChange('checkedA')}
                      aria-label="checkedA"
                      className="vacationToggle"
                    />
                  </span>
                </span>
                <span className={(vacationMode && checkedA) ? 'vacationMode vacationDate' : 'd-none'}> 
                On Vacation Untill {moment(selectedDate).format('DD-MM-YYYY')}
                </span>
              </li> : ''
          }

        </ul>
        {/* <div >
          <img src={require("assets/images/logo3.png")} className='medilogo' alt="Medimny" title="Medimny" />
        </div> */}
        <div className='sellerSideBarModal'>
          <Dialog open={open} onClose={this.handleRequestClose}
            fullWidth={true}
            scroll='paper'
            className='sdfsfsf'
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
            maxWidth={'sm'}>
            <DialogTitle id="scroll-dialog-title">
              {
                !checkedA && 'End your vacation'
              }
              {
                checkedA && 'On Vacation untill'
              }
              <DialogContentText className="mt-3">
                {
                  !checkedA ? 'Are you sure you want to end your vacation?' : `Please select vacation end date`
                }

              </DialogContentText>
            </DialogTitle>
            <DialogContent className="mb-5" style={{minHeight: this.state.vacationMode === false ?  "250px" : "10px"}}>
            <DialogContentText  id="scroll-dialog-description" className="mt-3 mb-5">
              {

                checkedA && 
                  <Field id="date" name="date" type="text"
                    component={RenderReactDatePicker} label="Date"
                    // validate={[required]}
                    value={moment(selectedDate).format()}
                    onChange={(date) => this.handleDateChange(date, 'selectedDate')}
                  />
                 
              }
              </DialogContentText>
            </DialogContent>
            
            <DialogActions>
              <Button onClick={this.handleRequestClose} color='secondary' >
                Cancel
            </Button>
              <Button onClick={this.handleRequestSave} color='primary'>
                Save
            </Button>
            </DialogActions>
          </Dialog>
        </div>
      </CustomScrollbars>
    );
  }
}
const mapStateToProps = ({ seller }) => {
  const { sidebar, userDetails,isVaccatioMode } = seller
  return { sidebar, userDetails,isVaccatioMode }
};

SidenavContent = connect(
  mapStateToProps,
  {
    getUserDetail
  }    // bind account loading action creator
)(SidenavContent)

export default SidenavContent = reduxForm({
  form: 'SidenavContent',// a unique identifier for this form
  destroyOnUnmount: false,
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('SidenavContent')();
    dispatch(initialize('SidenavContent', newInitialValues));
  }
})(withRouter(SidenavContent))

// export default withRouter(connect(mapStateToProps)(SidenavContent));


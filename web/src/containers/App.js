import React, { Component } from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import URLSearchParams from 'url-search-params'
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl'
import "assets/vendors/style"
import indigoTheme from './themes/indigoTheme';
import darkTheme from './themes/darkTheme';
import AppLocale from '../lngProvider';
import SignIn from './SignIn';
import AdminSignIn from './AdminSignIn';
import SignUp from './SignUp';
import asyncComponent from 'util/asyncComponent';

import BuyerSignUp from './BuyerSignUp';


import VerifyToken from './verifyToken';
import ForgotPassword from './forgotPassword';
import { setInitUrl } from '../actions/Auth';
import RTL from 'util/RTL';
import Buyers from 'app/buyers'
import Sellers from '../app/sellers'
import { setDarkTheme, setThemeColor } from "../actions/Setting";
import { NotificationContainer, NotificationManager } from 'react-notifications';
import UnderMaintenance from '../../src/containers/UnderMaintenance'
import { getFeaturedProductList } from '../actions/buyer';
import OrderSuccessful from 'app/routes/myCart/OrderSuccessful'
// import IdleTimer from 'react-idle-timer';

const RestrictedRoute = ({ component: Component, authUser, ...rest }) =>
  <Route
    {...rest}
    render={props =>
      authUser
        ? <Component {...props} />
        : <Redirect
          to={{
            pathname: '/signin',
            state: { from: props.location }
          }}
        />}
  />;

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      errorOccurred: false
    }
    this.idleTimer = null
    this.handleOnAction = this.handleOnAction.bind(this)
    this.handleOnActive = this.handleOnActive.bind(this)
    this.handleOnIdle = this.handleOnIdle.bind(this)
  }


  componentWillMount() {
    window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
    if (this.props.initURL === '') {
      this.props.setInitUrl(this.props.history.location.pathname);
    }
    const params = new URLSearchParams(this.props.location.search);
    if (params.has("theme-name")) {
      this.props.setThemeColor(params.get('theme-name'));
    }
    if (params.has("dark-theme")) {
      this.props.setDarkTheme();
    }
  }

  getColorTheme(themeColor, applyTheme) {
    applyTheme = createMuiTheme(indigoTheme);

    return applyTheme;
  }

  componentDidCatch(error, info) {
    if (error) {
      this.setState({ errorOccurred: true })
    }
  }

  componentDidMount() {
    this.props.getFeaturedProductList({ history: this.props.history })
  }

  handleOnAction = (event) => {
  }

  handleOnActive = (event) => {
  }

  handleOnIdle = async(event) => {
    await localStorage.clear();
    if(this.props.location.pathname.includes('seller')){
      this.props.history.push('/signin')
    }else{
      window.location.href = '/'
    }
    
  }
  render() {
    const { match, location, isDarkTheme, locale, initURL, isDirectionRTL } = this.props;
    const { data, featured, bannerArr, dealsArr, fastMovingArr } = this.props;
    console.log('daodcimaoifm',match)
    let { themeColor } = this.props;

    let authUser = localStorage.getItem('token')

    let authBuyer = localStorage.getItem('buyer_token')

    let user_type = localStorage.getItem('user_type')

    let applyTheme = createMuiTheme(indigoTheme);

    if (isDarkTheme) {
      document.body.classList.add('dark-theme');
      applyTheme = createMuiTheme(darkTheme)
    }

    else {
      applyTheme = this.getColorTheme(themeColor, applyTheme);
    }

    if (location.pathname == '/!') {     
      return (<Redirect to={'/'} />);      
    }

    if (location.pathname === '') {
      if (authUser === null || authUser === undefined) {
        console.log('this.props.match App authUser === null1');
        // return (<Redirect to={'/'} />);
      }

      else if (initURL === '' || initURL === '/signin') {
        console.log('this.props.match App initURL');

        if (user_type === 'buyer') {
          console.log('this.props.match App buyer1');
          // return (<Redirect to={'/'} />);
        }

        else if (user_type === 'seller') {
          console.log('this.props.match App seller');
          return (<Redirect to={'/seller/dashboard'} />);
        }

      }

      else {
        console.log('this.props.match App initURL else1');
        return (<Redirect to={initURL} />);
      }
    }


    if (location.pathname === '') {
      if (authBuyer === null || authBuyer === undefined) {
        console.log('this.props.match App authUser === null');
        // return (<Redirect to={'/'} />);
      }

      else if (initURL === '' || initURL === '/login') {
        console.log('this.props.match App initURL');

        if (user_type === 'buyer') {
          console.log('this.props.match App buyer2');
          // return (<Redirect to={'/'} />);
        }

        else if (user_type === 'seller') {
          console.log('this.props.match App seller');
          return (<Redirect to={'/'} />);
        }
      }
      else {
        console.log('this.props.match App initURL else2',initURL);
        if(initURL == '/adminLogin'){
          return (<Redirect to={'/'} />);
        }else{
          return (<Redirect to={initURL} />);
        }
      }
    }

    if (location.pathname === '/seller') {
      return (<Redirect to={'/seller/dashboard'} />);
    }

    if (location.pathname === '/signin') {
      if (authBuyer !== null) {
        return (<Redirect to={'/'} />);
      }
    }

    if (location.pathname === '/login' && this.props.location.state !== '/MyCart') {
      return (<Redirect to={'/'} />);
    }

    if (isDirectionRTL) {
      applyTheme.direction = 'rtl';
      document.body.classList.add('rtl')
    }

    if (location.pathname === '/profile') {

      if (authBuyer === null || authBuyer === undefined || user_type === 'seller') {
        return (<Redirect to={'/'} />);
      }
    }

    else {
      document.body.classList.remove('rtl');
      applyTheme.direction = 'ltr';
    }


    const currentAppLocale = AppLocale[locale.locale];

    console.log('daodcimaoifm-',match)
    return (
      
      <ThemeProvider theme={applyTheme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <IntlProvider
            locale={currentAppLocale.locale}
            messages={currentAppLocale.messages}>
            <RTL>
            {/* UnderMaintenance */}

              <div className="app-main">
                {/* <IdleTimer
                  ref={ref => { this.idleTimer = ref }}
                  timeout={1000 * 60 * 30}
                  onActive={this.handleOnActive}
                  onIdle={this.handleOnIdle}
                  onAction={this.handleOnAction}
                  debounce={250}
                /> */}
                <Switch>

                  <RestrictedRoute path={`${match.url}seller`} authUser={authUser} component={Sellers} />

                  <Route path='/adminLogin' component={AdminSignIn} history={this.props.history}/>

                  <Route path='/login' component={SignIn} history={this.props.history}/>
                  <Route path='/buyersignup' component={BuyerSignUp} />

                  <Route path='/signin' component={SignIn} />
                  <Route path='/signup' component={SignUp} />

                  <Route path='/verifyToken' component={VerifyToken} />
                  <Route path='/forgotPassword' component={ForgotPassword} />

                  <Route path='/underMaintenance' component={UnderMaintenance} />
                  <Route path='/orderStatus' component={OrderSuccessful} />
                  <Route path='/' component={Buyers} />
                  {/* <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/> */}
                </Switch>
                <NotificationContainer />
              </div>
            </RTL>
          </IntlProvider>
        </MuiPickersUtilsProvider>
      </ThemeProvider>
    );
  }
}
const mapStateToProps = ({ settings, auth, buyer }) => {
  const { themeColor, sideNavColor, darkTheme, locale, isDirectionRTL } = settings;
  const { authUser, initURL } = auth;

  const { featured, bannerArr, dealsArr, fastMovingArr, data, productsForYou, visitedProduct } = buyer;

  return {
    themeColor, sideNavColor, isDarkTheme: darkTheme, locale, isDirectionRTL, authUser, initURL,
    featured, bannerArr, dealsArr, fastMovingArr, data, productsForYou, visitedProduct
  }
};
export default withRouter(connect(mapStateToProps, { setInitUrl, setThemeColor, setDarkTheme, getFeaturedProductList })(App));
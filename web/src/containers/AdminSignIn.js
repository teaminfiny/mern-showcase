import React from 'react';
import AppConfig from 'constants/config'
import axios from 'axios';
import {withRouter} from 'react-router-dom';
class AdminSignIn extends React.Component {
  constructor(props) {
    super(props);
    let url = props.location.search
    let json = url ? JSON.parse('{"' + decodeURIComponent(url.substring(1).replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}') : ''
    this.state = {
      email: json ? json.Id ? json.Id : '' :'',
      otp: json ? json.otp ? json.otp : '':'',
      user_type: 'admin',
      showNotification: true,
      admin: json ? Boolean(json.admin) ? Boolean(json.admin) : false:false,
    }
  }


  componentDidMount = async() => {
    if( this.state.admin ){
      localStorage.clear();
      localStorage.setItem('asAdmin', 1);
      const {history} =this.props
      let url1 = await (AppConfig.baseUrl).includes('infiny.dev') ? 'https://medideals-web.infiny.dev' : 'https://medideals.in';
      let data = { otp:this.state.otp }
      let signInUser = await axios.post(`${AppConfig.baseUrl}admin/adminToUser`,data,{
          headers:{
            'Content-Type':'application/json'
          }
        }).then((result)=>result);
        localStorage.setItem('isDocumentExpired', false);
        localStorage.setItem('user_type', 'buyer')
        localStorage.setItem('buyer_token',signInUser.data.token)
        console.log('oasidmaoismdc',history)
        // history.location.pathname = '/';
        history.push('/');
        console.log('oasidmaoismdc-',history)
        setTimeout(function(){ window.location.reload(); }, 1000);
    }
  }

  render() {

    return (
      <div className="col-xl-8 col-lg-8 signinContainer" >
        <div className="jr-card signinCard">
          <div className="animated slideInUpTiny animation-duration-3">
            <div className="login-header mb-0">
              <a className="app-logo" href='javascript:void(0);' title="Jambo">
                <img src={require("assets/images/medimny.png")} className='medilogo' alt="Medimny" title="Medimny" />
              </a>
            </div>
          </div>

        </div>
      </div>

    );
  }
}

export default withRouter(AdminSignIn);

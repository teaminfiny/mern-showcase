import React, { Component } from 'react'
import Form1 from '../components/BuyerSignUp/form1/index'
import Form2 from '../components/BuyerSignUp/form2/index.js'
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';
import CustomScrollbars from '../util/CustomScrollbars'

import axios from '../constants/axios';
import helperFunction from '../constants/helperFunction';
import Step from '@material-ui/core/Step';
import Stepper from '@material-ui/core/Stepper';
import StepLabel from '@material-ui/core/StepLabel';
import moment from "moment"
const getSteps = () => {
  return ['Welcome', 'Compliance Form'];
}

class BuyerSignUp extends Component {
  constructor(props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.state = {
      page: 0,
      drugLic20BExpiry: moment().add(2, 'M'),
      drugLic21BExpiry: moment().add(2, 'M'),
      fassaiLicExpiry: moment().add(2, 'M'),
      red: 0,
      green: 0,
      blue: 0,
      showNotification: true
    }
  }

  componentDidMount = () => {
    let rgbValue = helperFunction.getRGB();
    this.setState({ red: rgbValue[0], green: rgbValue[1], blue: rgbValue[2] })
  }

  nextPage() {
    this.setState({ page: this.state.page + 1 })
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 })
  }

  onChangeHandle = async (data) => {
    

    let postData = {
        drugLic20BLicNo:data.drugLic20BLicNo,
        drugLic21BLicNo:data.drugLic21BLicNo,
        fassaiLicNo:data.fassaiLicNo,
        gstLicNo:data.gstLicNo,
        password: data.password,
        confirmPassword: data.confirmPassword,
        email: (data.email).toLowerCase(),
        first_name: data.first_name,
        last_name: data.last_name,
        phone: data.phone,
        drugLic20BExpiry: data.drugLic20BExpiry ? data.drugLic20BExpiry : this.state.drugLic20BExpiry,
        drugLic21BExpiry: data.drugLic21BExpiry ? data.drugLic21BExpiry : this.state.drugLic21BExpiry,
        fassaiLicExpiry: data.fassaiLicExpiry ? data.fassaiLicExpiry : this.state.fassaiLicExpiry,
        drugLic20B: data.drugLic20B,
        drugLic21B: data.drugLic21B,
        gstLic: data.gstLic,
        fassaiLic: data.fassaiLic,
        user_type: data.user_type,
        user_address: data.user_address+" "+(data.user_address1 ? data.user_address1 : ''),
        user_state: data.user_state,
        red: this.state.red,
        green: this.state.green,
        blue: this.state.blue,
        user_pincode: data.user_pincode,
        user_city:data.user_city,
        company_name: data.company_name
      }

    await axios.post('/users/register', postData, {
      headers: {
        'Content-Type': 'application/json',
        // 'token': localStorage.getItem('token')
      }
    }
    ).then(result => {
      let that = this;
      if (result.data.error) {
        NotificationManager.error(result.data.title)
      } else {
        if (that.state.showNotification) {
          NotificationManager.success(result.data.title, '', 200);
          localStorage.setItem('verifyToken', result.data.token);
          localStorage.setItem('isBuyerVerification', true)
          return that.props.history.push('/verifyToken')
        }
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }

  render() {
    const { onSubmit } = this.props
    const { page } = this.state
    return (
      <CustomScrollbars>
        <div className="col-xl-12 col-lg-12 signinContainer" >
          <div className="jr-card signinCard mt-5">
            <React.Fragment>
            <div className="login-header mb-0">
            <a className="app-logo" href='javascript:void(0);' title="Jambo">
              <img src={require("assets/images/medimny.png")} className='medilogo' alt="Medimny" title="Medimny" />
            </a>
          </div>

              <Stepper activeStep={page} alternativeLabel={true} className="horizontal-stepper-linear">
                {getSteps().map(label => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>

              {page === 0 && <Form1 onSubmit={this.nextPage} />}
              {page === 1 && <Form2 previousPage={this.previousPage}  onSubmit={this.onChangeHandle} />}
            </React.Fragment>
          </div>
        </div>
      </CustomScrollbars>
    )
  }
}

BuyerSignUp.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

export default BuyerSignUp
import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';
import { Col, Row, Card } from 'reactstrap';
import axios from '../constants/axios';
import { required } from '../constants/validations';
import renderTextField from '../components/textBox';
import { Field, reduxForm } from 'redux-form'

import {
  hideMessage,
  showAuthLoader,
  userSignIn,
} from 'actions/Auth';

class VerifyToken extends React.Component {
  constructor() {
    super();
    this.state = {
      otp: ''
    }
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  handleChange = (e, newValue, previousValue, key) => {
    this.setState({ [key]: e.target.value })
  }

  onSubmit = async () => {
    let data = {
      verificationOtp: this.state.otp
    }
    let api = localStorage.getItem('forgotPassword') == 'true' ?  'users/verifyOTP' : 'users/mobileVerification';
    let token = localStorage.getItem('forgotPassword') == 'true' ? localStorage.getItem('forgotPasswordToken') : localStorage.getItem('verifyToken')
    let users = localStorage.user
    await axios({
      method: 'post',
      url: `${api}`,
      data,
      headers: {
        'token': token
      }
    }
    ).then(result => {
      let that = this;
      if (result.data.error) {
        NotificationManager.error(result.data.title);
      } 
      
      else {
        if (api == 'users/verifyOTP') {


          if (result.data.token === null) {
            localStorage.setItem('forgotPassword', false)
            localStorage.setItem('forgotPasswordToken', null)


            NotificationManager.success('Otp verified successfully.');
            setTimeout(() => {
              NotificationManager.error('Your document is under verification, please try to login after sometime.');
              return that.props.history.push('/seller/signin')
            }, 2000)
          } else if (users === 'buyer')  {
            localStorage.setItem('buyer_token',result.data.token)

            NotificationManager.success(result.data.title);
            setTimeout(() => {
              return that.props.history.push('/profile')
            }, 2000)
          }
          else if (users === 'seller')  {
            localStorage.setItem('token',result.data.token)


            NotificationManager.success(result.data.title);
            setTimeout(() => {
              return that.props.history.push('/seller/change-Password')
            }, 2000)
          }
        } 
        
        else {
          let buyer = localStorage.getItem('isBuyerVerification')

          
          if(buyer === 'true'){

            localStorage.removeItem('isBuyerVerification')
            that.props.history.push('/')
          }
          
          else{
            setTimeout(() => {
            return that.props.history.push('/')
          }, 2000)
          }
          NotificationManager.success(result.data.title);
          
        }

      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }

  onResend = async () => {
    let apiResend = localStorage.getItem('resendOTP') == 'true'? 'users/resendOTP':'';
    let token = localStorage.getItem('forgotPassword') == 'true' ? localStorage.getItem('forgotPasswordToken') : localStorage.getItem('verifyToken');
    let api = localStorage.getItem('forgotPassword') == 'true' ?  'users/verifyOTP' : 'users/mobileVerification';
    let users = localStorage.user
    await axios({
      method: 'post',
      url:'users/resendOTP',
      // data,
      headers: {
        'token': token
      }
    }
    ).then(result => {
      let that = this;
      if (result.data.error) {
        NotificationManager.error(result.data.title);
      } 
      
      else {
// jkl
        NotificationManager.success(result.data.title);
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }


  render() {
    const { } = this.state;
    const { showMessage, loader, alertMessage, handleSubmit } = this.props;
    return (
      <div className="col-xl-12 col-lg-12 verifyTokenContainer" >
        <div className="jr-card verifyTokenCard">
          <div className="animated slideInUpTiny animation-duration-3">
            <div className="mb-4">
              <h3>{localStorage.getItem('forgotPassword') == 'true' ? 'Verify OTP' : 'Mobile OTP verification'}</h3>
            </div>

            
            <form onSubmit={handleSubmit(this.onSubmit)} autocomplete="off">
              <Field name="otp" type="text"
                component={renderTextField} label="OTP"
                validate={required}
                value={this.state.value}
                onChange={(event, newValue, previousValue) => this.handleChange(event, newValue, previousValue, 'otp')}
              />
            
              <div className="mb-2" style={{display:'inline',float:'right'}}>
                <Button color="primary" variant="contained" className="text-white " style={{width:'120px'}}
                  type='submit'>
                  Verify
                   </Button>
              </div>
            </form>
            <Row>
              <Col>
            <div className="mb-2" >
              <Button color="primary" onClick={this.onResend} variant="contained" className="text-white float-right"  style={{width:'128px'}}>
                Resend OTP
                   </Button>
            </div></Col>
            </Row>
           
            
              {/* </form> */}
            <div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser }
};

VerifyToken = reduxForm({
  form: 'VerifyToken',// a unique identifier for this form
})(VerifyToken)

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
})(VerifyToken);

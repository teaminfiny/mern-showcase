import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';
import renderTextField from '../components/textBox';
import { Field, reduxForm } from 'redux-form'
import axios from '../constants/axios';
import {
  hideMessage,
  showAuthLoader,
  userSignIn,
} from 'actions/Auth';

import { emailPhone } from '../constants/validations'

class ForgotPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      email: ''
    }
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  handleChange = (e, newValue, previousValue, key) => {
    this.setState({ [key]: e.target.value })
  }

  onSubmit = async (e) => {
    let data = {
      email: this.state.email
    }
    await axios.post('/users/forgotPassword', data, {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    ).then(result => {
      let that = this;
      if (result.data.error) {
        NotificationManager.error(result.data.title);
      } else {
        localStorage.setItem('forgotPassword', true)
        localStorage.setItem('forgotPasswordToken', result.data.token)
        NotificationManager.success(result.data.title);
        setTimeout(() => {
          return that.props.history.push('/verifyToken')
        }, 2000)
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }


  render() {
    const { email } = this.state;
    const { showMessage, loader, alertMessage, handleSubmit } = this.props;
    return (
      <div className="col-xl-12 col-lg-12 forgotPasswordContainer" >
        <div className="jr-card forgotPasswordCard">
          <div className="animated slideInUpTiny animation-duration-3">
            <div className="mb-4">
              <h3>Forgot Password</h3>
            </div>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <Field name="email" type="text"
                component={renderTextField} label="Email/Mobile"
                validate={emailPhone}
                value={email}
                onChange={(event, newValue, previousValue) => this.handleChange(event, newValue, previousValue, 'email')}
              />
              <div className="mb-2">
                {/* <Button color="primary" variant="contained" className="text-white"
                  type="submit"
                >
                  Submit
                   </Button> */}
                <Button type='submit' variant='contained' color='primary' style={{ backgroundColor: '#072791' }} className='text-white' > Submit </Button>
              </div>
            </form>
            <div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser }
};

ForgotPassword = reduxForm({
  form: 'ForgotPassword',// a unique identifier for this form
})(ForgotPassword)

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
})(ForgotPassword);

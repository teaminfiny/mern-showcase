import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from 'components/BuyersHeader';
import Footer from 'components/Footer';
import Dashboard from 'app/routes/bDashboard';
// import ViewProducts from 'app/routes/bViewproducts'
// import Widgets from 'app/routes/widgets'
// import Metrics from 'app/routes/metrics'
// import Editors from 'app/routes/editors';
// import Pickers from 'app/routes/pickers';
// import Extensions from 'app/routes/extensions';
// import Table from 'app/routes/table';
// import Chart from 'app/routes/charts';
// import Map from 'app/routes/map';
// import Calendar from 'app/routes/calendar';
// import TimeLine from 'app/routes/timeLine';
// import CustomViews from 'app/routes/customViews';
// import ExtraElements from 'app/routes/extraElements'
// import eCommerce from 'app/routes/eCommerce'
// import AppModule from 'app/routes/appModule'
import ViewSeller from 'app/routes/bSeller';
import ViewCompanyFront from 'app/routes/bCompanyFront';
import SearchResult from 'app/routes/bSearchResults'
import ListNotifications from 'app/routes/listNotification'
import EditProfile from 'app/routes/BEditprofile'
import AboutUs from '../../components/Footer/pages/AboutUs'
import ContactUs from '../../components/Footer/pages/ContactUs'
import Services from '../../components/Footer/pages/Services'
import Careers from '../../components/Footer/pages/Careers'
import ReturnPolicy from '../../components/Footer/pages/ReturnPolicy'
import TermsOfUse from '../../components/Footer/pages/TermsOfUse'
import Privacy from '../../components/Footer/pages/Privacy'
import Cookies from '../../components/Footer/pages/Cookies'
import ShippingAndReturnPolicy from '../../components/Footer/pages/ShippingAndReturnPolicy'




import { getUserDetail } from 'actions'

import {
  ABOVE_THE_HEADER,
  BELOW_THE_HEADER,
  COLLAPSED_DRAWER,
  FIXED_DRAWER,
  HORIZONTAL_NAVIGATION,
} from 'constants/ActionTypes';

import { isIOS, isMobile } from 'react-device-detect';
import ProductDetails from 'app/routes/bProductDetails';
import Profile from 'app/routes/bProfile'
import MyCart from '../routes/myCart';
import MediWallet from 'app/routes/bProfile/MediWallet';

import BuyerSignUp from '../../containers/BuyerSignUp';
import ShortBook from '../../components/BShortBook' 

class Buyer extends React.Component {

  render() {
    const { match, drawerType, navigationStyle, horizontalNavPosition } = this.props;
    const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'fixed-drawer' : drawerType.includes(COLLAPSED_DRAWER) ? 'collapsible-drawer' : 'mini-drawer';
    //set default height and overflow for iOS mobile Safari 10+ support.
    if (isIOS && isMobile) {
      document.body.classList.add('ios-mobile-view-height')
    }
    else if (document.body.classList.contains('ios-mobile-view-height')) {
      document.body.classList.remove('ios-mobile-view-height')
    }

    let pathname = this.props.location.pathname
    let urls = pathname.split('/')
    let mobilePath = (urls[urls.length - 1]).toLowerCase() === 'mobile'
    return (
      <div className={`app-container ${drawerStyle}`}>


        <div className="app-main-container">
          {!mobilePath && <div
            className={`app-header ${'app-header-horizontal'}`}>


            <Header />
            {/* <SecondaryHeader/> */}


          </div>}

          <main className="app-main-content-wrapper">
            <div className="app-main-content buyersMainContent">
              <Switch>
                
                {/* <Route path={`/view-products`} component={ViewProducts} /> */}
                <Route path={`/view-seller/:id`} component={ViewSeller} />
                <Route path={`/view-company/:id`} component={ViewCompanyFront} />
                <Route path={`/search-results/:keyword/:search`} component={SearchResult} />
                <Route path={`/myCart`} component={MyCart} />
                <Route path={`/list-Notification`} component={ListNotifications} />
                <Route path='/product-details/:keyword/:id' component={ProductDetails} />
                { !localStorage.getItem('asAdmin') ?
                <Route path={`/edit-profile`} component={EditProfile} /> :''}
                { !localStorage.getItem('asAdmin') ?
                <Route path={`/profile`} component={Profile} />:''}

                {/* <Route path={`/editor`} component={Editors} />
                <Route path={`/pickers`} component={Pickers} />
                <Route path={`/extensions`} component={Extensions} />
                <Route path={`/table`} component={Table} />
                <Route path={`/chart`} component={Chart} />
                <Route path={`/map`} component={Map} />
                <Route path={`/calendar`} component={Calendar} />
                <Route path={`/time-line`} component={TimeLine} />
                <Route path={`/custom-views`} component={CustomViews} />

                <Route path={`/widgets`} component={Widgets} />
                <Route path={`/metrics`} component={Metrics} />
                <Route path={`/extra-elements`} component={ExtraElements} />
                <Route path={`/ecommerce`} component={eCommerce} />
                <Route path={`/app-module`} component={AppModule} /> */}

                <Route path={`/MediWallet`} component={MediWallet} />

                <Route path={`/buyerSignUp`} component={BuyerSignUp} />

                <Route path={`/about-us`} component={AboutUs} />
                <Route path={`/ContactUs`} component={ContactUs} />
                <Route path={`/Services`} component={Services} />
                <Route path={`/Careers`} component={Careers} />
                <Route path={`/ReturnPolicy`} component={ReturnPolicy} />
                <Route path={`/TermsOfUse`} component={TermsOfUse} />
                <Route path={`/Privacy`} component={Privacy} />
                <Route path={`/Cookies`} component={Cookies} />
                <Route path={`/ShippingAndReturnPolicy`} component={ShippingAndReturnPolicy} />
                <Route path={`/ShippingAndReturnPolicy/:id`} component={ShippingAndReturnPolicy} />

                
                <Route path={`/shortbook`} component={ShortBook} />
                <Route path='/' component={Dashboard} />





              </Switch>
            </div>
            <Footer />
          </main>
        </div>

      </div>
    );
  }
}


const mapStateToProps = ({ settings, auth, seller }) => {
  const { drawerType, navigationStyle, horizontalNavPosition } = settings;

  const { userDetails } = seller
  const { user_details } = auth;
  let users = userDetails ? userDetails : user_details

  return { drawerType, navigationStyle, horizontalNavPosition, user_details, users }
};

export default withRouter(connect(mapStateToProps, { getUserDetail })(Buyer));
import React, { Component } from 'react';
import { NotificationManager } from 'react-notifications';
import { Col, Row } from 'reactstrap';
import IntlMessages from 'util/IntlMessages';
import ContainerHeader from 'components/ContainerHeader';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required, emailField, validatePhone,validatePincode,specailChar } from '../../../../constants/validations';
import renderTextField from '../../../../components/textBox';
import RenderSelectField from '../../../../components/RenderSelectField';
import axios from '../../../../constants/axios'
import { getUserDetail } from '../../../../actions/seller'

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      phone: '',
      gender: 'male',
      address: '',
      email : '',
      pincode:'',
      city:'',
      state:'',
      states:[],
      company_name:''
    };

  }

  handleChange = (event, value) => {
    this.setState({ gender: value });
  };

  componentDidMount() {
    axios.get('/admin/getStates', {}, {
			headers: {
				'Content-Type': 'application/json',
				'token': localStorage.getItem('token')
			}
		}
		).then(result => {
			let that = this;
			if (result.data.error) {
			} else {
				that.setState({ states: result.data.detail })
			}
		})
			.catch(error => {
      });
      let userData=this.props.userDetails.mainUser?this.props.userDetails.mainUser:this.props.userDetails
    this.setState({
      first_name: this.props.userDetails ? this.props.userDetails.first_name : this.state.first_name,
      last_name: this.props.userDetails ? this.props.userDetails.last_name : this.state.last_name,
      phone: this.props.userDetails ? this.props.userDetails.phone : this.state.phone,
      gender: this.props.userDetails ? this.props.userDetails.gender !== undefined ? this.props.userDetails.gender : this.state.gender : this.state.gender,
      address: userData ? userData.user_address : this.state.user_address,
      email: this.props.userDetails ? this.props.userDetails.email : this.state.email,
      pincode:userData?userData.user_pincode?userData.user_pincode:this.state.pincode:this.state.pincode,
      city:userData?userData.user_city?userData.user_city:this.state.pincode:this.state.city,
      state:userData?userData.user_state?userData.user_state._id?userData.user_state._id:userData.user_state:this.state.state:this.state.state,
      company_name:userData?userData.company_name?userData.company_name:this.state.company_name:this.state.company_name,

    });

    this.props.initialize({
      first_name: this.props.userDetails ? this.props.userDetails.first_name : this.state.first_name,
      last_name: this.props.userDetails ? this.props.userDetails.last_name : this.state.last_name,
      phone: this.props.userDetails ? this.props.userDetails.phone : this.state.phone,
      address: userData ? userData.user_address : this.state.user_address,
      email: this.props.userDetails ? this.props.userDetails.email : this.state.email,
      pincode:userData?userData.user_pincode?userData.user_pincode:this.state.pincode:this.state.pincode,
      city:userData?userData.user_city?userData.user_city:this.state.pincode:this.state.city,
      state:userData?userData.user_state?userData.user_state._id?userData.user_state._id:userData.user_state:this.state.state:this.state.state,
      company_name:userData?userData.company_name?userData.company_name:this.state.company_name:this.state.company_name,

    });
  }

  handledateChange = (value, key) => {
    this.setState({ [key]: value })
  }

  onSubmit = async () => {
    let data = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      phone: this.state.phone,
      user_address: this.state.address,
      gender: this.state.gender,
      user_pincode:this.state.pincode,
      user_state:this.state.state,
      user_city:this.state.city,
      company_name:this.state.company_name
    }

    await axios.post('/users/editUserProfile', data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }
    ).then(result => {
      if (result.data.error) {
        NotificationManager.error(result.data.title);
      } else {
        NotificationManager.success(result.data.title);
        this.props.getUserDetail({history : this.props.history})
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }


  render() {
    let { first_name, last_name, phone, address, email,pincode,city,states,company_name} = this.state;
    let { userDetails, handleSubmit } = this.props;
    let isDisabled = userDetails.mainUser?true:false
    return (
      <div className="col-xl-12 col-lg-12">
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Edit Profile" />} />
        <div className="jr-card">
          <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off">
            <Row>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
                <Field id="first_name" name="first_name" type="text"
                  component={renderTextField} label="First Name"
                  validate={[required,specailChar]}
                  value={first_name}
                  onChange={(event) => this.setState({ first_name: event.target.value })}
                />
              </Col>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
                <Field id="last_name" name="last_name" type="text"
                  component={renderTextField} label="Last Name"
                  validate={[required,specailChar]}
                  value={last_name}
                  onChange={(event) => this.setState({ last_name: event.target.value })}
                />
              </Col>
            </Row>
            <Row>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
                <Field id="phone" name="phone" type="text"
                  component={renderTextField} label="Phone"
                  validate={[validatePhone]}
                  value={phone}
                  onChange={(event) => this.setState({ phone: event.target.value })}
                />
              </Col>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
              {
                isDisabled?<Field id="address" name="address" type="text"
                component={renderTextField} label="Address"
                validate={[required]}
                value={address}
                input={{ disabled: isDisabled, value : address }}
                disabled={isDisabled}
                onChange={(event) => this.setState({ address: event.target.value })}
              />:<Field id="address" name="address" type="text"
              component={renderTextField} label="Address"
              validate={[required]}
              value={address}
              onChange={(event) => this.setState({ address: event.target.value })}
            />
              }
              
                
              </Col>
              
            </Row>
            <Row>
            <Col sm={6} md={6} lg={6} xs={6} xl={6}>
              {
                isDisabled?
                <Field id="pincode" name="pincode" type="text"
                  component={renderTextField} label="Pincode"
                  validate={[required,validatePincode]}
                  value={pincode}
                  disabled={isDisabled}
                  input={{ disabled: isDisabled, value : pincode }}
                  onChange={(event) => this.setState({ pincode: event.target.value })}
                />:<Field id="pincode" name="pincode" type="text"
                component={renderTextField} label="Pincode"
                validate={[required,validatePincode]}
                value={pincode}
                onChange={(event) => this.setState({ pincode: event.target.value })}
              />
              }
                
              </Col>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
              {
                isDisabled?
                <Field id="city" name="city" type="text"
                  component={renderTextField} label="City"
                  validate={[required,specailChar]}
                  value={city}
                  input={{ disabled: isDisabled, value : city }}
                  onChange={(event) => this.setState({ city: event.target.value })}
                />:
                <Field id="city" name="city" type="text"
                  component={renderTextField} label="City"
                  validate={[required,specailChar]}
                  value={city}
                  onChange={(event) => this.setState({ city: event.target.value })}
                />
              }
                
              </Col>
              <Col sm={6} md={6} lg={6} xs={12} xl={6} className="text-left">
								{isDisabled?
                <Field
									name="user_state"
									component={RenderSelectField}
                  label=""
                  type='select'
                  value={this.state.state}
									fullWidth={true}
                  margin="normal"
                  onChange={(event) => this.setState({ state: event.target.value })}
                  input={{ disabled: isDisabled, value : this.state.state }}
								>
									<option value="" selected = {this.state.state===''}/>
									{
										states && states.length > 0 ? states.map((stateData, key) => {
											return <option value={stateData._id} selected={this.state.state==stateData._id}>{stateData.name}</option>
										}) : ''
									}
								</Field>:<Field
                name="state"
                component={RenderSelectField}
                label=""
                type='select'
                validate={required}
                value={this.state.state}
                fullWidth={true}
                margin="normal"
                onChange={(event) => this.setState({ state: event.target.value })}
                
              >
                <option value="" selected = {this.state.state===''}/>
                {
                  states && states.length > 0 ? states.map((stateData, key) => {
                    return <option value={stateData._id} selected={this.state.state==stateData._id}>{stateData.name}</option>
                  }) : ''
                }
              </Field>}
              </Col>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
              {
                isDisabled?
                <Field id="company_name" name="company_name" type="text"
                  component={renderTextField} label="Company"
                  validate={[required,specailChar]}
                  value={company_name}
                  disabled={isDisabled}
                  input={{ disabled: isDisabled, value : company_name }}
                  onChange={(event) => this.setState({ company_name: event.target.value })}
                />:
                <Field id="company_name" name="company_name" type="text"
                  component={renderTextField} label="Company"
                  validate={[required,specailChar]}
                  value={company_name}
                  onChange={(event) => this.setState({ company_name: event.target.value })}
                />
              }
                
              </Col>
              <Col sm={6} md={6} lg={6} xs={6} xl={6}>
                <Field id="email" name="email" type="text"
                  component={renderTextField} label="Email"
                  validate={[emailField]}
                  value={email}
                  input={{ disabled: true, value : email }}
                  // onChange={(event) => this.setState({ email: event.target.value })}
                />
              </Col>
            </Row>
            <div className={'customButton'}>
              <Button style={{ backgroundColor: '#072791' }} type="submit" variant="contained" color="primary">Save</Button>
            </div>
          </form>
        </div>
      </div >
    );
  }
}

const mapStateToProps = ({ seller }) => {
  const { userDetails } = seller;
  return { userDetails }
};

EditProfile = connect(
  mapStateToProps ,
  {
    getUserDetail
  }          // bind account loading action creator
)(EditProfile)

EditProfile = reduxForm({
  form: 'EditProfile',// a unique identifier for this form
})(EditProfile);

export default EditProfile;
import React, { Component } from 'react';
import IntlMessages from 'util/IntlMessages';
import ContainerHeader from 'components/ContainerHeader';
import Button from '@material-ui/core/Button';
import axios from '../../../../constants/axios';
import { required, minLength6, confirmPasswordMatch, validatePassword } from '../../../../constants/validations';
import renderTextField from '../../../../components/textBox';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { NotificationManager } from 'react-notifications';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      password: '',
      confirmPassword: '',
      showNotification: true
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState !== this.state;
  }

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
    if (this.state.showNotification === false) {
      this.setState({ showNotification: true });
    }

  }


  onSubmit = async () => {
    let data = {
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
    }

    await axios.post('/users/changePassword', data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }
    ).then(result => {
      let that = this;
      if (this.state.showNotification) {
        if (result.data.error) {
          NotificationManager.error(result.data.title);
        } else {
          NotificationManager.success(result.data.title);
        }
        that.setState({ showNotification: false });
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }

  render() {
    let { oldPassword, password, confirmPassword } = this.state;
    const { showMessage, loader, alertMessage, handleSubmit } = this.props;

    return (
      <div className="col-xl-12 col-lg-12">
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Change Password" />} />
        <div className="jr-card">
          <form onSubmit={handleSubmit(this.onSubmit)}>
            <Field id="newPassword" name="newPassword" type="password"
              component={renderTextField} label="New Password"
              // validate={validatePassword}
              validate={[required, minLength6]}
              value={password}
              onChange={(event) => this.handleChange(event, 'password')}
            />

            <Field name="confirmPassword" type="password"
              component={renderTextField} label="Confirm Password"
              validate={confirmPasswordMatch('newPassword')}
              value={confirmPassword}
              onChange={(event) => this.handleChange(event, 'confirmPassword')}
            />
            <div className="mb-2">
              <div className={'customButton'}>
                <Button style={{ backgroundColor: '#072791' }} variant="contained" color="primary" type="submit">Save</Button>
              </div>
            </div>
          </form>
        </div >
      </div >
    );
  }
}


export default ChangePassword = reduxForm({
  form: 'ChangePassword',// a unique identifier for this form
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('ChangePassword')();
    dispatch(initialize('ChangePassword', newInitialValues));
  }
})(ChangePassword)


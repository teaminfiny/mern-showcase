import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from '../../../util/asyncComponent';

const OrderDetails = ({match}) => (
  <React.Fragment>
    <Switch>
      <Redirect exact from={`${match.url}/orderDetails/`} to={`${match.url}/:id`}/>
      <Route path={`${match.url}/:id`} component={asyncComponent(() => import('./routes/OrderDetails'))}/>
      <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/>
    </Switch>
  </React.Fragment>
);

export default OrderDetails;

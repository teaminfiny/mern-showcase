import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import FormLabel from '@material-ui/core/FormLabel';
import { Col, Row } from 'reactstrap';
import Avatar from '@material-ui/core/Avatar';

class ChangeStatus extends Component {
  state = {
    open: false,
    quantity: 0,
    refQuantity: 0,
    refCost: 0,
    cost: 0,
    index: 0,
    purchase:0,
    removeProd:false
  };

  handleRequestClose = (e) => {
    this.setState({ open: false, quantity: this.props.qty });
    this.props.handleClick(e, true, '', '')
  };

  handleRequestCloseProd = (e) => {
    this.setState({ removeProd:false,open: false,quantity: this.props.qty });
    // this.props.handleClick(e, true, '', '')
  };
  handleRequestCloseProdYes = (e) =>{
    this.setState({removeProd:false})
    // this.handleRequest(e);
  }
  handleRequest = (e) => {
    this.setState({ open: false, removeProd:false });
    if ((this.state.cost !== this.state.refCost || this.state.quantity !== this.state.refQuantity) && (this.props.status === "New")) {
      let isDisabled = true;
      if(this.props.status === "New" && (this.state.cost !== this.state.refCost || this.state.quantity !== this.state.refQuantity)) {
        isDisabled = false;
      } 

      this.props.handleClick(e, isDisabled, this.state.cost, this.state.quantity, this.state.index, this.props.status)
    } /* else {
      this.props.handleClick(e, true, this.state.cost, this.state.quantity, this.state.index, this.props.status)
    } */
    
  };

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  componentDidMount() {
    this.setState({ quantity: this.props.qty, refQuantity: this.props.qty, cost: this.props.cost, refCost: this.props.cost, index: this.props.index, purchase: this.props.purchase })
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props.qty != prevProps.qty) {
      this.setState({ quantity: this.props.qty, refQuantity: this.props.qty })
    }
  }
  handleCartValue = async(e, type) => {
    let tempQuantity = this.state.quantity;
    // if(this.state.purchase !== 0 && (this.state.purchase != undefined || this.state.purchase != null)){
      if(this.state.purchase !== 0 && this.state.purchase !== undefined){ 
      if (type === 'decrement' && tempQuantity >= 1) {
      tempQuantity -= this.state.purchase;
      await this.setState({ quantity: tempQuantity })
    } else if (type === 'increment') {
      tempQuantity += this.state.purchase;
      await this.setState({ quantity: tempQuantity })
    }
  }
  else
  {
    if (type === 'decrement' && tempQuantity > 0) {
      tempQuantity -= 1;
      await this.setState({ quantity: tempQuantity })
    } else if (type === 'increment') {
      tempQuantity += 1;
      await this.setState({ quantity: tempQuantity })
    }
  }
  if(this.state.quantity == 0){
    this.setState({ removeProd: true })
  }
  }
  
  validate = (evt) => {
    let t = evt.target.value;
    evt.target.value = (t.indexOf(".") >= 0) ? (t.substr(0, t.indexOf(".")) + t.substr(t.indexOf("."), 3)) : t;
  }

  render() {
    let { quantity, refQuantity, cost } = this.state;
    let { title } = this.props;
    return (
      <div>
        <Button variant="outlined" color="primary" className={'button orderListButton'} onClick={() => this.setState({ open: true })}>Edit</Button>
        <Dialog open={this.state.open} onClose={this.handleRequestClose}
          fullWidth={false}
          className="EditorderPopup" >

          <DialogTitle>
            {title}
            <DialogContentText className='mt-3'>
              {/* To subscribe to this website, please enter your email address here. We will send
              updates occasionally. */}
            </DialogContentText>
          </DialogTitle>
          <DialogContent>

            <form noValidate autoComplete="off">
              <div className="justify-content-center d-flex mb-4">
                <span className="mr-3  editOrderIcon align-self-center text-primary" onClick={(e) => this.handleCartValue(e, 'decrement')}><i class="zmdi zmdi-minus-circle-outline"></i></span>
                <Avatar className="text-white bg-primary showQty">{quantity}</Avatar>
                <span className={`ml-3 editOrderIcon align-self-center ${quantity >= refQuantity ? 'text-muted' : 'text-primary'}`} onClick={(e) => quantity >= refQuantity ? false : this.handleCartValue(e, 'increment')}><i class="zmdi zmdi-plus-circle-o"></i></span>
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={(e) => this.handleRequestClose(e)} color="secondary" >
              Cancel
            </Button>
            <Button onClick={(e) => this.handleRequest(e)} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
        {/*----------------------- Remove prod ---------------------- */}
        <Dialog open={this.state.removeProd} onClose={this.handleRequestCloseProd} fullWidth={true}
          maxWidth={'sm'}>
          <DialogTitle>
            Remove Product
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              {'Are you sure you want to remove this product ? This product will be removed from your inventory as well. Please click to continue.'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={(e) =>this.handleRequestCloseProd(e)} color='secondary' >
              No
            </Button>
            <Button color='primary' onClick={(e) =>this.handleRequest(e)} >
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div >
    );
  }
}

export default ChangeStatus;
import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import EditOrder from './editOrderDetail';
import { connect } from 'react-redux';
import { getOrderDetails } from '../../../../actions/order'
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'constants/axios';
import {NotificationManager} from 'react-notifications';
import AppConfig from 'constants/config'
class OrderDetailTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            orderProductData: [],
            status: '',
            cancel: false,
            removedProd: [],
            mediData: '',
            orderMedicineType:''
        }
    }

    handleClick = async(e, value, ptr, quantity, index) => {
        // this.props.onClickHandle(e, value);
        if (quantity !== '' && ptr !== '') {
            let tempOrderProduct = this.state.orderProductData;
            tempOrderProduct[index].paid = ptr;
            tempOrderProduct[index].quantity = quantity;
            await this.setState({ orderProductData: tempOrderProduct, removedProd: [] });
            this.props.onClickHandle(e, value, tempOrderProduct);
            if(this.state.orderProductData.length > 0 && this.props.orderDetails.products!==tempOrderProduct && !this.state.cancel){
                let finalGrand = 0;
                this.state.orderProductData.map((eachProduct) =>{
                    return(
                        finalGrand = finalGrand + Number(this.getFinalValueWithoutGst(eachProduct))
                    )
                })
                this.state.orderProductData.map((eachProduct) =>{
                    this.props.orderDetails.products.filter(async(e) => {
                        if(e._id == eachProduct._id && e.quantity != eachProduct.quantity){
                            await this.setState({removedProd: this.state.removedProd.concat(eachProduct)})
                            // await this.setState({removedProd: [...this.state.removedProd, eachProduct]})
                        }
                    })
                })
                console.log('tempOrderProduct--',this.state.removedProd)
                if(finalGrand < this.state.orderMedicineType.chgReqAmount){
                  this.setState({ cancel: true })
                }
                // if(this.props.orderDetails.orderType && this.props.orderDetails.orderType.toLowerCase() == 'cool chain' && finalGrand < 8000){
                //   this.setState({ cancel: true })
                // }
                // if(this.props.orderDetails.orderType && this.props.orderDetails.orderType.toLowerCase() != 'cool chain' && finalGrand < 2000){
                //     this.setState({ cancel: true })
                // }
            }
        } else {
            this.props.onClickHandle(e, value, '');
        }
    }

    componentDidMount = async() => {
        let {products} = this.props.orderDetails?this.props.orderDetails:[]
        this.setState({ orderProductData: JSON.parse(JSON.stringify(products)), status: this.props.orderDetails.requested });
        await axios({
            method:'get',
            url:`${AppConfig.baseUrl}medicine/listMediCategory`,
            headers: {
              'Content-Type': 'application/json',
              token: localStorage.getItem('token')
          }}).then(async(result) => {
            if (result.data.error) {
            } else {
              await this.setState({ mediData: result.data.data })
              let mediTypeData = this.state.mediData.filter(e => e.name.toLowerCase() === this.props.orderDetails.orderType.toLowerCase());
              this.setState({ orderMedicineType: mediTypeData[0] })
            }
          }).catch(error => {
              console.log("error catch", error)
          });
           
    }

    componentDidUpdate(prevProps, prevState){
        let {products} = this.props.orderDetails?this.props.orderDetails:[]
        if(prevProps.orderDetails!==this.props.orderDetails){
            this.setState({ orderProductData: JSON.parse(JSON.stringify(products)), status: this.props.orderDetails.requested });
        }
    }
    getFinalValueWithoutGst = (data)=>{
        let tempValue = data.inventory_id && data.inventory_id.discount_id && (data.inventory_id.discount_id.name == 'Same' || data.inventory_id.discount_id.name == 'SameAndDiscount' ) ?
        (((data.quantity / data.inventory_id.discount_id.discount_on_product.purchase ) * data.inventory_id.discount_id.discount_on_product.bonus) + Number(data.quantity)) * ( data.ePTR ? data.ePTR : data.PTR )
        : ( data.ePTR ? data.ePTR : data.PTR ) * data.quantity 
        let finalValue = tempValue;
        return finalValue
    }

    button = (title, qty, index, cost,purchase) => {
        if (title !== undefined, qty !== undefined, index !== undefined, cost !== undefined) {
            return (
                < div className={'action'} >
                    <EditOrder handleClick={this.handleClick} title={title} status={this.state.status} index={index} qty={parseInt(qty)} cost={cost} purchase={purchase}/>
                </div >
            )
        }
    }
    getValue = (data)=>{
        if(!data.discount_on_product && data.discount_per > 0){
            let totalPtr = (data.PTR-((data.PTR/100)*data.discount_per))
            return totalPtr*Number(data.quantity)
        }else if(data.discount_on_product && data.discount_per > 0){
            let totalPtr = (data.PTR-((data.PTR/100)*data.discount_per))
            return totalPtr*Number(data.quantity)
        }else{
           return data.PTR*Number(data.quantity)
        }
    }

    getSingleItemFinalValue = (data)=>{
        if(!data.discount_on_product && data.discount_per > 0){
            let totalPtrSingle = (data.PTR-((data.PTR/100)*data.discount_per))
            return totalPtrSingle
        }else if(data.discount_on_product && data.discount_per > 0){
            let totalPtrSingle = (data.PTR-((data.PTR/100)*data.discount_per))
            return totalPtrSingle
        }else{
           return data.PTR
        }
    }

    getFinalValue = (data)=>{
        // let tempValue = this.getValue(data)
        let tempValue = data.discount_name && ( data.discount_name == 'Same' || data.discount_name == 'SameAndDiscount' ) ?
        (((data.quantity / data.discount_on_product.purchase ) * data.discount_on_product.bonus) + Number(data.quantity)) * ( data.ePTR ? data.ePTR : data.PTR )
        : ( data.ePTR ? data.ePTR : data.PTR ) * data.quantity 
        let finalValue = ((Number(data.GST)/100)*tempValue)+tempValue;
        return finalValue
    }

    handleClose = (e) => {
        this.setState({ cancel: false, cancelPop: false })
        this.props.getOrderDetails({ history: this.props.history, data: { orderId: this.props.match.params.id } })
    }

    handleCancelOrder = async(e) => {
    let cool = this.props.orderDetails.orderType && this.props.orderDetails.orderType.toLowerCase() == 'cool chain';
    let prod = []
    this.state.removedProd.map(async(data) => {
        let inven = await this.props.orderDetails.products.filter(e => e._id == data._id);
        prod.push({
            productName: data.productName,
            product_id: data.inventory_id.product_id._id,
            inventory_id: data.inventory_id._id,
            quantity: inven[0].quantity
        })
    })
    let data = {
        order_cancel_reason: `Order value below Rs.${this.state.orderMedicineType.chgReqAmount}`,
        status: 'Cancelled',
        orderId: this.props.match.params.id,
        products: prod
        }
        await axios.post('/order/proccessCancelOrder', data, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
        }
        ).then(result => {
        let that = this;
        if (result.data.error) {
            NotificationManager.error(result.data.title)
        } else {
            this.setState({ cancel: false })
            NotificationManager.success(result.data.title);
            that.props.getOrderDetails({ history: this.props.history, data : {orderId : that.props.match.params.id} })
        }
        })
        .catch(error => {
            NotificationManager.error('Something went wrong, Please try again')
        });
    }
    
    render() {
        let { orderProductData, status } = this.state;
        let cool = this.props.orderDetails.orderType && this.props.orderDetails.orderType.toLowerCase() == 'cool chain';
        
        

        return (
            <React.Fragment>
            <Paper className={'tableRoot'}>
                {orderProductData && orderProductData.length > 0 ?
                    <Table className={'tableMain'} aria-label="spanning table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Title</TableCell>
                                <TableCell align="left">Packing</TableCell>
                                <TableCell align="left">Expiry</TableCell>
                                <TableCell align="left">Offer</TableCell>
                                <TableCell align="left">MRP</TableCell>
                                <TableCell align="left">PTR</TableCell>
                                
                                <TableCell align="left">Qty</TableCell>
                                <TableCell align="left">Effective PTR</TableCell>
                                <TableCell align="left">GST</TableCell>
                                <TableCell align="left">Final Price</TableCell>
                                {
                                     status === "New" ? <TableCell align="left">Action</TableCell>:''
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {orderProductData.map((eachProduct, index) => (
                                <TableRow key={eachProduct.title + '' + index}>
                                    <TableCell align="left">{eachProduct.name} <br/>
                                    { eachProduct.surCharge && eachProduct.surCharge > 0 ? <span className='text-white bg-danger' style={{ padding: "1px 5px" }}>Surcharge ({eachProduct.surCharge}%)</span> :'' }
                                    </TableCell>
                                    <TableCell align="left">{eachProduct.inventory_id.product_id.Type.name}</TableCell>
                                    <TableCell align="left">{moment(eachProduct.inventory_id && eachProduct.inventory_id.expiry_date).format('MM/YYYY')}</TableCell>
                                    <TableCell align="left">
                                        {/* {
                                        eachProduct.discount_on_product ? 
                                            eachProduct.discount_on_product.purchase + '+' + eachProduct.discount_on_product.bonus
                                            //  + ' ' + eachProduct.discount_on_product.name 
                                            : 
                                            eachProduct.discount_per + '% discount'
                                        }    */}
                                        {
                                            eachProduct.discount_name && eachProduct.discount_name == 'SameAndDiscount' ?
                                            eachProduct.discount_on_product.purchase + '+' + eachProduct.discount_on_product.bonus+' (Bonus) and '+eachProduct.discount_per+'% Off' :

                                            eachProduct.discount_name && eachProduct.discount_name == 'Same' ? 
                                            eachProduct.discount_on_product.purchase + '+' + eachProduct.discount_on_product.bonus+' (Bonus)' :

                                            eachProduct.discount_name && eachProduct.discount_name == 'Different' ?
                                            eachProduct.discount_on_product.purchase + '+' + eachProduct.discount_on_product.bonus +' '+ eachProduct.discount_on_product.name
                                            :
                                            eachProduct.discount_name && eachProduct.discount_name == 'DifferentAndDiscount' ?
                                            eachProduct.discount_on_product.purchase + '+' + eachProduct.discount_on_product.bonus +' '+ eachProduct.discount_on_product.name + ' and ' + eachProduct.discount_per+'% Off'
                                            :

                                            eachProduct.discount_per + '% discount'
                                        }
                                    </TableCell>
                                   
                                    <TableCell align="left">&#x20B9;{(Number(eachProduct.MRP)).toFixed(2)}</TableCell>
                                    <TableCell align="left">&#x20B9;{(Number(eachProduct.PTR)).toFixed(2)}</TableCell>
                                    
                                    <TableCell align="left">{eachProduct.quantity}</TableCell>
                                    {/* <TableCell align="left">{(this.getSingleItemFinalValue(eachProduct)).toFixed(2)}</TableCell> */}
                                    <TableCell align="left">{(eachProduct.ePTR ? eachProduct.ePTR :eachProduct.PTR).toFixed(2)}</TableCell>
                                    <TableCell align="left">{eachProduct.inventory_id.product_id.GST.value + "%"}</TableCell>
                                    <TableCell align="left">{this.getFinalValue(eachProduct).toFixed(2)}</TableCell>

                                    {
                                        status === "New" ?
                                            <TableCell align="left">
                                                {this.button(eachProduct.name, eachProduct.quantity, index, eachProduct.PTR, eachProduct && eachProduct.discount_on_product
                                                && eachProduct.discount_on_product.purchase)}
                                            </TableCell>
                                            :
                                            ''
                                    }
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    : 'No Items Found'}

            </Paper>
            {/* -------------------------Cancel Order < 8000/2000 ------------------------------- */}
            <Dialog open={this.state.cancel} onClose={this.handleClose} fullWidth={true}
                maxWidth={'sm'}>
                <DialogTitle className="pb-0">
                Cancel Order
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    Order amount is below Rs.{this.state.orderMedicineType.chgReqAmount} (without GST), it must be greater than Rs.{this.state.orderMedicineType.chgReqAmount}, Would you like to cancel the order ?&nbsp;
                    {this.props.cancelText && this.props.cancelText}
                    </DialogContentText>
                    {
                        this.state.removedProd && this.state.removedProd.map((data, index) => {
                            return <h4 className='text-muted'> {++index}. {data.productName} </h4>
                        })
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={(e) =>this.handleClose(e)} color='secondary' >
                    No
                    </Button>
                    <Button color='primary' onClick={(e) =>this.handleCancelOrder(e)} >
                     Yes
                    </Button>
                </DialogActions>
             </Dialog>
        </React.Fragment>
        );
    }
}

const mapStateToProps = ({ order }) => {
    const { orderDetails } = order;
    return { orderDetails }
};

export default connect(mapStateToProps, { getOrderDetails })(OrderDetailTable);

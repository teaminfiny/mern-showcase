import React, { Component } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withRouter } from 'react-router-dom';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import axios from '../../../../constants/axios';
import { NotificationManager } from 'react-notifications';
import { connect } from 'react-redux';
import { getOrderDetails } from '../../../../actions/order';
import { Input } from 'reactstrap';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#137cbd',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#106ba3',
    },
  },
});

function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

class ChangeStatus extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      view: false,
      reason:'',
      selectedValue: 'Sorry, We do not have expected quantity.',
      removedItem: []
    };
  }

  handleRequestClose = () => {
    this.setState({ open: false, status: '',openPop: false });
  };

  handleSubmit = async () => {
    if(this.state.removedItem.length > 0){
    let prod = []
    await this.state.removedItem.map(async(data) => {
        let inven = await this.props.orderDetails.products.filter(e => e._id == data);
        prod.push({
            productName: inven[0].productName,
            product_id: inven[0].inventory_id.product_id._id,
            inventory_id: inven[0].inventory_id._id,
            quantity: inven[0].quantity
        })
    })
    let data = {
      order_cancel_reason: this.state.selectedValue == 'Other' && this.state.reason.length > 0 ? this.state.reason : this.state.selectedValue,
      status: 'Cancelled',
      orderId: this.props.match.params.id,
      products: prod
    }
    await axios.post('/order/proccessCancelOrder', data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }
    ).then(result => {
      let that = this;
      if (result.data.error) {
        NotificationManager.error(result.data.title)
      } else {
        NotificationManager.success(result.data.title);
        that.props.getOrderDetails({ history: that.props.match.params.id, data : {orderId : that.props.match.params.id} })
      }
    }).catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
    }else{
      NotificationManager.error('Please select atleast 1 product.')
    }
    this.setState({ open: false, selectedValue:'Sorry, We do not have expected quantity.',
    view: false, reason:'', openPop: false, removedItem:[] });
  }

  handleChange = (event) => {
    event.preventDefault();
    this.setState({ selectedValue: event.target.value })
    if(event.target.value == 'Other'){
      this.setState({ view: true, reason:''})
    }
  }
  handleChanged = (e,key) => {
    this.setState({ [key]: e.target.value})
  }
  handleRequestCloseCancel = () => {
    this.setState({ open: false })
  }
  
  handleCheck = async(key, e) => {
    let temp = [...this.state.removedItem]
    if (e.target.checked) {
      temp.push(e.target.value)
      this.setState({ removedItem: temp })
    } else {
      temp.splice(this.getChecked(e.target.value), 1)
      this.setState({ removedItem: temp })
    }
  }
  getChecked = (data) => {
    let index = this.state.removedItem.findIndex((val) => val == data)
    return index
  }

  render() {
    let { selectedValue, view, removedItem } = this.state;
    let { status } = this.props;
    return (
      <div>
        <Button className={'jr-btn-xs ml-1'} disabled={(status === 'Processed' || status === 'Requested' || status === 'Cancelled' || status === 'Delivered') ? true : false} variant="contained" color="primary" onClick={() => this.setState({ open: true })}> CANCEL ORDER </Button>

        <Dialog open={this.state.open} onClose={this.handleRequestClose}
          fullWidth={true}
          maxWidth={'sm'}>
          <DialogTitle className="pb-0">
            Cancel Order
            <DialogContentText className="mt-0 mb-0">
              Are you sure you want to cancel the order ? {this.props.cancelText && this.props.cancelText}<br/>
              <span className='text-danger'>Please select atleast 1 product!</span>
            </DialogContentText>
          </DialogTitle>
          <DialogContent>
            { this.props.orderDetails && this.props.orderDetails.products.map((data,index) => {
							return <h4 className='text-muted mb-2'>
                <Checkbox
                  onClick={(e) => {
                    this.handleCheck('category_id', e);
                  }}
                  value={data._id}
                  color="primary"
                  checked={this.getChecked(data._id) > -1 ? true : false}
                  tabIndex="-1" /> {++index}. {data.productName ? data.productName : data.name} </h4>
							})
						}
            <DialogContentText className='mb-0 mt-3 text-dark'>
              Please select a reason for cancellation of this order:
            </DialogContentText>
            <FormControl component="fieldset">
              <RadioGroup aria-label="reason" name="customized-radios" value={selectedValue} onChange={(e) => this.handleChange(e)}>
                <FormControlLabel value="Sorry, We do not have expected quantity." control={<StyledRadio />} label="Sorry, We do not have expected quantity." />
                <FormControlLabel value="Other" control={<StyledRadio />} label="Other" />
                {/* <FormControlLabel value="Order by mistake" control={<StyledRadio />} label="Order by mistake" />
                <FormControlLabel value="Reason not listed" control={<StyledRadio />} label="Reason not listed" /> */}
              </RadioGroup>
            </FormControl>
            { view && <Input type="text" name="reason" id="reason" placeholder='* Optional' onChange={(e) => this.handleChanged(e, 'reason')} />}
            
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleRequestClose} color='secondary' >
              No
            </Button>
            <Button onClick={this.handleSubmit} color='primary' disabled={removedItem.length == 0}>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = ({ order }) => {
  const { orderDetails } = order;
  return { orderDetails }
};


export default connect(mapStateToProps, { getOrderDetails })( withRouter(ChangeStatus))
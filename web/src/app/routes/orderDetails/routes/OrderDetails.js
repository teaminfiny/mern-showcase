import React from 'react';
import Button from '@material-ui/core/Button';

import OrderDetailsHeader from './OrderDetailsHeader';
import { Col, Row, Form, Badge, Label, Input } from 'reactstrap';
import IntlMessages from 'util/IntlMessages';
import { withRouter, NavLink } from 'react-router-dom';
import OrderDetailsTable from "../component/orderDetailTable";
import {updateOrder, getOrderDetails } from '../../../../actions/order';
import { connect } from "react-redux";
import moment from 'moment'
import AppConfig from '../../../../constants/config'
import CircularProgress from '@material-ui/core/CircularProgress';
import async from 'async'

let  some = 0;

class OrderDetails extends React.Component {

  constructor() {
    super();
    this.state = {
      isRequested: false,
      updatedProduct: [],
      status: 'New',
      orderDetailsState: {},
      statusBadge: ' ',
      totalValue:0,
      finalfinal: 0,
    }
  }


  componentDidMount() {
    this.props.getOrderDetails({ history: this.props.history, data: { orderId: this.props.match.params.id } })
  }

  componentWillReceiveProps = (nextProps, nextState) => {
    if (nextProps.orderDetails && (this.props.orderDetails !== nextProps.orderDetails) && Object.keys(nextProps.orderDetails).length > 0) {
      let tempStatusBadge = nextProps.orderDetails.requested === nextProps.orderDetails.order_status[nextProps.orderDetails.order_status.length - 1].status ? nextProps.orderDetails.requested : nextProps.orderDetails.requested === "Requested" ? "Requested" : nextProps.orderDetails.requested === "Approved" ? "Approved" : nextProps.orderDetails.order_status[nextProps.orderDetails.order_status.length - 1].status;

      this.setState({ status: nextProps.orderDetails.requested, orderDetailsState: nextProps.orderDetails, isRequested: true, statusBadge: tempStatusBadge,totalValue:nextProps.totalValue });
    } else if (this.props.orderDetails && (this.props.orderDetails !== nextProps.orderDetails) && Object.keys(this.props.orderDetails).length > 0) {
      let tempStatusBadge = this.props.orderDetails.requested === this.props.orderDetails.order_status[this.props.orderDetails.order_status.length - 1].status ? this.props.orderDetails.requested : this.props.orderDetails.requested === 'Requested' ? 'Requested' : this.props.orderDetails.requested === 'Approved' ? 'Approved' : this.props.orderDetails.order_status[this.props.orderDetails.order_status.length - 1].status;
      
      this.setState({ 
        status: this.props.orderDetails.requested, 
        orderDetailsState: this.props.orderDetails, 
        isRequested: true, statusBadge: tempStatusBadge,
        totalValue:this.props.totalValue 
      });
    }
  }

  onClickHandle = (e, value, orderProductData) => {
    e.preventDefault();
    if (this.state.isRequested) {
      this.setState({ isRequested: value });
    }

    if (orderProductData !== '') {
      this.setState({ updatedProduct: orderProductData })
    }
  }
  getValue = (data)=>{
    if(!data.discount_on_product&&data.discount_per>0){
        let totalPtr = (((data.discount_per/100)*data.PTR)+data.PTR)
        return totalPtr*Number(data.quantity)
    }else{
       return data.PTR*Number(data.quantity)
    }
  }
  getFinalValueData = async(data)=>{
    let tempValue = await this.getValue(data)
    let finalValue = ((data.inventory_id.product_id.GST.value/100)*tempValue)+tempValue
    return finalValue
  }
  totalValue = (value,cb1)=>{
    let val = 0
    async.eachOfSeries(value.products,async function(data,key,cb){
      if(!data.discount_on_product&&data.discount_per>0){
        let totalPtr = (((data.discount_per/100)*data.PTR)+data.PTR)
        let tempVal =  totalPtr*Number(data.quantity)
        let finalValue = ((data.inventory_id.product_id.GST.value/100)*tempVal)+tempVal
        val = val+finalValue
        cb()
    }else{
      let tempVal = data.PTR*Number(data.quantity)
      let finalValue = ((data.inventory_id.product_id.GST.value/100)*tempVal)+tempVal
      val = val+finalValue
      cb()
    }
    },function(err){
       cb1(val) 
    })
  }

  getValue = (data) => {
    if (!data.discount_on_product && data.discount_per > 0) {
      let totalPtr = (data.PTR - ((data.PTR / 100) * data.discount_per))
      return totalPtr * Number(data.quantity)
    } else {
      return data.PTR * Number(data.quantity)
    }
  }

  getFinalValue = (data) => {
    let tempValue = this.getValue(data)
    let finalValue = ((data.inventory_id.product_id.GST.value / 100) * tempValue) + tempValue;
    return finalValue
  }

  
  
  render() {

    let { isRequested, updatedProduct, status, orderDetailsState, statusBadge } = this.state;

    const { orderDetails } = this.props;

    let finalGrand = 0;
    

    orderDetails && orderDetails.products && orderDetails.products.map((eachProduct) => {
      return (
        finalGrand = finalGrand + Number(this.getFinalValue(eachProduct))

      )
    })



    return (
      <React.Fragment>
        <div className="order-details" >
          {
            (statusBadge !== ' ') ? <Button className={statusBadge.includes("Processed") ? "text-primary" : statusBadge.includes("Ready For Dispatch") ? "text-amber" : statusBadge.includes("Placed") ? "text-yellow" : statusBadge.includes("Cancelled") ? "text-grey" : statusBadge.includes("Delivered") ? "text-green" : statusBadge.includes("Requested") ? "text-info" : statusBadge.includes("New") ? "text-danger" : "text-info"}>{statusBadge === 'Processed' ? 'Processed' : statusBadge}</Button> :
              <div className="loader-view">
                <CircularProgress />
              </div>
          }
        </div>
        {(orderDetailsState && Object.keys(orderDetailsState).length > 0) ?
          <div className="dashboard animated slideInUpTiny animation-duration-3">

            <OrderDetailsHeader 
              orderDetails = {orderDetailsState} 
              text = {this.props.text}
              cancelText = {this.props.cancelText}
              isRequested={isRequested} 
              status={status} 
              updatedProduct={updatedProduct} 
              match={this.props.match} 
              title={<IntlMessages id={`Order ID`} />} 
            />

            <div className="row">
              <div className="col-12">
                <div className="jr-card chart-user-statistics bg-primary darken-4 text-white">
                  <div className="orderDetails px-4 mb-4">
                    <Row className="pb-4">
                      <Col xs={12} xl={6} sm={12} md={6} lg={6} className="orderDetailsMainCol">
                        <h3 style={{ fontSize: 15 }}> ORDER PLACED BY</h3>
                        <div className="p-1">
                          <i className="zmdi zmdi-info-outline zmdi-hc-fw zmdi-hc-sm text-white align-self-center" />
                          <span className="align-self-center ml-1">
                            {this.props.match.params.id}
                          </span>
                        </div>
                        <div className="p-1">
                          <i className="zmdi zmdi-account-o zmdi-hc-fw zmdi-hc-sm text-white align-self-center" />
                          <span className="align-self-center ml-1">
                            {
                              orderDetailsState.user_id !== undefined ? orderDetailsState.user_id.company_name : 'N/A'
                            }
                          </span>
                        </div>
                        <div className="p-1">
                          <i className="zmdi zmdi-pin zmdi-hc-fw zmdi-hc-sm text-white align-self-center" />
                          <span className="align-self-center ml-1">
                            {
                              orderDetailsState.user_id !== undefined ? orderDetailsState.user_id.user_address : 'N/A'
                            }
                          </span>
                          {/* <p className="ml-4 mb-0">Sector 17 Sanpada</p>
                        <p className="ml-4 mb-0">Palm Beach </p> */}
                        </div>
                        <div className="p-1">
                          <i className="zmdi zmdi-city zmdi-hc-fw zmdi-hc-sm text-white align-self-center" />
                          <span className="align-self-center ml-1">
                          {
                              orderDetailsState.user_id !== undefined ? orderDetailsState.user_id.user_city : ''
                            },&nbsp;
                            {
                              (orderDetailsState.user_id !== undefined && orderDetailsState.user_id.user_state !== undefined) ? orderDetailsState.user_id.user_state.name : 'N/A'
                            }
                          </span>
                          <p className="ml-4 mb-0">
                            {
                              orderDetailsState.user_id !== undefined ? orderDetailsState.user_id.user_pincode : 'N/A'
                            }
                          </p>
                        </div>
                        <div className="p-1">
                          <i className="zmdi zmdi-phone zmdi-hc-fw zmdi-hc-sm text-white align-self-center" />
                          <span className="align-self-center ml-1">
                            {
                              orderDetailsState.user_id !== undefined ? orderDetailsState.user_id.phone : 'N/A'
                            }
                          </span>
                        </div>
                      </Col>
                      <Col xs={12} xl={4} sm={4} md={4} lg={4} className="orderDetailsMainCol">
                        <h3 style={{ fontSize: 15 }}> KYC DETAILS</h3>

                        {orderDetailsState.user_id && orderDetailsState.user_id.drugLic20B && orderDetailsState.user_id.drugLic20B.name ?
                          <div className="p-1">
                            <span className="align-self-center ml-1">
                              FORM 20&nbsp;<span className="small">&#40;{orderDetailsState.user_id && orderDetailsState.user_id.drugLic20B && orderDetailsState.user_id.drugLic20B.expires ? `${orderDetailsState.user_id.drugLic20B.lic ? orderDetailsState.user_id.drugLic20B.lic : ''} \u007C ` + moment(orderDetailsState.user_id.drugLic20B.expires).format('DD/MM/YYYY') : ''}&#41;</span>
                            </span>
                            <span className="align-self-center ml-4 pull-right">
                              <a target='blank' download href={`${AppConfig.baseUrl}users/${orderDetailsState.user_id && orderDetailsState.user_id.drugLic20B && orderDetailsState.user_id.drugLic20B.name ? orderDetailsState.user_id.drugLic20B.name : ''}`}>
                                <i className="zmdi zmdi-cloud-download zmdi-hc-fw zmdi-hc-md text-white align-self-center" />
                              </a>
                            </span>
                          </div>
                          : ''}

                        {orderDetailsState.user_id && orderDetailsState.user_id.drugLic21B && orderDetailsState.user_id.drugLic21B.name ?
                          <div className="p-1">
                            <span className="align-self-center ml-1">
                              FORM 21&nbsp;<span className="small">&#40;{orderDetailsState.user_id && orderDetailsState.user_id.drugLic21B && orderDetailsState.user_id.drugLic21B.expires ? `${orderDetailsState.user_id.drugLic21B.lic ? orderDetailsState.user_id.drugLic21B.lic : ''} \u007C ` + moment(orderDetailsState.user_id.drugLic21B.expires).format('DD/MM/YYYY') : ''}&#41;</span>
                            </span>
                            <span className="align-self-center ml-4 pull-right">
                              <a target='blank' download href={`${AppConfig.baseUrl}users/${orderDetailsState.user_id && orderDetailsState.user_id.drugLic21B && orderDetailsState.user_id.drugLic21B.name ? orderDetailsState.user_id.drugLic21B.name : ''}`}>
                                <i className="zmdi zmdi-cloud-download zmdi-hc-fw zmdi-hc-md text-white align-self-center" />
                              </a>
                            </span>
                          </div>
                          : ''}

                        {orderDetailsState.user_id && orderDetailsState.user_id.fassaiLic && orderDetailsState.user_id.fassaiLic.name ?
                          <div className="p-1">
                            <span className="align-self-center ml-1">
                              FASSAI&nbsp;
                            {orderDetailsState.user_id && orderDetailsState.user_id.fassaiLic && 
                            <span className="small">
                               &#40;
                               {orderDetailsState.user_id && orderDetailsState.user_id.fassaiLic && orderDetailsState.user_id.fassaiLic.expires ? 
                               `${orderDetailsState.user_id.fassaiLic.lic ? orderDetailsState.user_id.fassaiLic.lic : ""} \u007C ` + 
                               moment(orderDetailsState.user_id.fassaiLic.expires).format('DD/MM/YYYY') 
                               + '' : ''
                               }
                               &#41;</span>}
                            </span>

                            <span className="align-self-center ml-4 pull-right">
                              <a target='blank' download href={`${AppConfig.baseUrl}users/${orderDetailsState.user_id && orderDetailsState.user_id.fassaiLic && orderDetailsState.user_id.fassaiLic.name ? orderDetailsState.user_id.fassaiLic.name : ''}`}>
                                <i className="zmdi zmdi-cloud-download zmdi-hc-fw zmdi-hc-md text-white align-self-center" />
                              </a>
                            </span>

                          </div>
                          : ''}

                        {orderDetailsState.user_id && orderDetailsState.user_id.gstLic && orderDetailsState.user_id.gstLic.name ?
                          <div className="p-1">
                            <span className="align-self-center ml-1">
                              GSTN&nbsp;

                            {orderDetailsState.user_id && orderDetailsState.user_id.gstLic && 
                            <span className="small"> 
                              
                            {orderDetailsState.user_id.gstLic.lic ?
                            <React.Fragment>
                              &#40;
                              {orderDetailsState.user_id && orderDetailsState.user_id.gstLic ?                               
                              `${orderDetailsState.user_id.gstLic.lic ? orderDetailsState.user_id.gstLic.lic : " "}`: ''}
                             &#41;
                             </React.Fragment>
                             : null }

                            </span>}
                            </span>
                            <span className="align-self-center ml-4 pull-right">
                              <a target='blank' download href={`${AppConfig.baseUrl}users/${orderDetailsState.user_id && orderDetailsState.user_id.gstLic && orderDetailsState.user_id.gstLic.name ? orderDetailsState.user_id.gstLic.name : ''}`}>
                                <i className="zmdi zmdi-cloud-download zmdi-hc-fw zmdi-hc-md text-white align-self-center" />
                              </a>
                            </span>
                          </div>
                          : ''}
                      <h3 style={{ fontSize: 15, marginBottom:'0px' ,marginTop:'10px'}}>PAYMENT TYPE &#40;{orderDetails && orderDetails.isBulk ? 'Bulk Prepaid' : orderDetails.paymentType === 'Online' ? 'Prepaid' : orderDetails && orderDetails.paymentType}&#41;</h3>
                        <div className="p-1">
                          <span className="align-self-center ml-1">
                             Grand Total ₹{ (orderDetailsState.total_amount).toFixed(2) } 
                          </span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-12">

                <OrderDetailsTable 
                match={this.props.match}
                onClickHandle={this.onClickHandle}
                cancelText={this.props.cancelText} 
                />
              </div>
            </div>
          </div>
          : <div className="loader-view">
            <CircularProgress />
          </div>
        }
      </React.Fragment>
    );
  }
}
const mapStateToProps = ({ order }) => {
  const { orderDetails,totalValue, text, cancelText} = order;
  return { orderDetails,totalValue, text, cancelText }
};

export default connect(mapStateToProps, { getOrderDetails })(withRouter(OrderDetails));
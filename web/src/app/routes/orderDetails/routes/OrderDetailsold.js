import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import EditOrder from '../component/editOrderDetail';
import Button from '@material-ui/core/Button';
import buyerImage from 'app/sellers/buyerImage';
import customHead from 'constants/customHead'


const columns = [
  {
    name: "title",
    label: "Title",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "amount",
    label: "Cost",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "action",
    label: "Action",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
];




const options = {
  filterType: 'dropdown',
  filter: false,
  print: false,
  download: false,
  selectableRows : 'none',
  selectableRowsOnClick: false,
  selectableRowsHeader: false,
  responsive: 'scrollMaxHeight',
  search: false,
  viewColumns: false,
  textLabels: {
    filter: {
      all: "All",
      title: "FILTERS",
    },
  }
};

class OrderDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  // handleTrackOrderClick = (e) => {
  //   this.props.history.push('/seller/trackOrder');
  // }

  button = (title) => (
    <div className={'action'}>
      <EditOrder title={title} />
      {/* <Button variant="outlined"  onClick={(e) => this.handleTrackOrderClick(e)} className={'button btn-success text-success orderListButton'}>Track</Button> */}
    </div>
  )

  render() {


    const data = [
      {
        title: 'Bluetooth Speaker', amount: '₹100', action: this.button('Bluetooth Speaker')
      },
      { title: 'Earphone', amount: '₹500', action: this.button('Earphone') },
      { title: 'Screen Gaurd', amount: '₹600', action: this.button('Screen Gaurd') },
      { title: 'Mobile', amount: '₹9000', action: this.button('Mobile') },
      { title: 'Bluetooth Speaker', amount: '₹100', action: this.button('Bluetooth Speaker') },
    ];

    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Order Details " />} />
        <MUIDataTable
          data={data}
          columns={columns}
          options={options}
        />
      </React.Fragment>
    );
  }
}

export default OrderDetails;
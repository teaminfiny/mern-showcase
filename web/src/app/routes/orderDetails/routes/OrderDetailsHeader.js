import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import IntlMessages from 'util/IntlMessages';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom'
import ChangeStatusDailog from 'app/routes/orders/component/changeStatus';
import CancelOrderDailog from '../component/orderCancelDailog';
import { Col, Row } from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { updateOrder, getOrderDetails } from '../../../../actions/order';
import {  connect } from 'react-redux';
import helper from 'constants/helperFunction';
class OrderDetailsHeader extends Component {

	constructor(props) {
		super(props)
		this.state = {
			open: false,
			openPop: false,
			title: '',
			productData: []
		}
	}


	componentDidUpdate = (prevProps) => {
		if ((this.props.updatedProduct && this.props.updatedProduct.length > 0 ) && (this.props.updatedProduct !== prevProps.updatedProduct)) {
			this.setState({ productData: this.props.updatedProduct });
		}
		this.disableChangeRequest()
	}

	handleRequest = (e, title) => {
		this.setState({ open: true, title: title });

	}

	handleRequestClose = () => {
		this.setState({ open: false, openPop: false });
		this.props.getOrderDetails({ history: this.props.history, data: { orderId: this.props.match.params.id } })
	};

	handleSubmitRequest = (e) => {
		e.preventDefault();
		this.props.updateOrder({history : this.props.history, data : {products : this.state.productData, orderId : this.props.match.params.id, status : 'Requested'}});
		this.setState({ open: false });
		this.props.getOrderDetails({ history: this.props.history, data: { orderId: this.props.match.params.id } })

	}

	handleSubmitRequestPop = (e) => {
		e.preventDefault();
		this.setState({ openPop: false, open: true });
	}
	
	disableChangeRequest = ()=>{
		let {status,isRequested} = this.props
		if(this.props.orderDetails&&this.props.orderDetails.user_id&&this.props.orderDetails.user_id.user_status!=='active'){
			return true
		}
		else if(status === 'Accepted' && isRequested){
			return true
		}
		else if((status === 'Requested' || status === 'Approved' || status === 'New' || status === 'Processed' || status === "Cancelled" || status === "Delivered") && isRequested){
			return true
		}
		else{
			return false
		}
	}
	render() {
		const path = this.props.match.path.substr(1);
		let removedItem = [];
		const subPath = path.split('/');
		const title = this.props.title;
		let { requestButtonColor } = this.state;
		let { updatedProduct, status, isRequested, orderDetails} = this.props;

		this.props.orderDetails.products.map((val,index) => {
      if(this.state.productData && val.quantity > (this.state.productData[index] && this.state.productData[index].quantity) ){
         removedItem.push(this.state.productData[index]);
      }
    });
		return (
			<div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center">
				<span style={{display:'flex'}}> 
					<span><h2 className="title mb-3 mb-sm-0">Order Detail</h2></span>&nbsp;&nbsp;
					<span className="badge text-uppercase text-white"	style={{backgroundColor:`${helper.getProdColor(orderDetails.orderType)}`}}>
          {orderDetails.orderType ? orderDetails.orderType : 'N/A'}
        	</span>
				</span>

				<Breadcrumb className="mb-0" tag="nav">
					{subPath.map((sub, index) => {
						if (sub === 'orderDetails') {
							return (
								<Row>
									<Col className="text-nowrap p-0 mr-3">
										<Button 
											className={'jr-btn-xs mr-1'} 
											variant="contained" 
											disabled={this.disableChangeRequest()} 
											color={((status === 'Requested' || status === 'New' || status === 'Approved' || status === 'Processed' || status === "Cancelled" || status === "Delivered") && isRequested) ? 'grey' : 'primary'} 
											onClick={(e) => this.handleRequest(e, 'Change Request')}> 
											Change Request 
										</Button>
									</Col>
									<Col className="text-nowrap p-0">
										<ChangeStatusDailog isRequested ={isRequested} orderDetails={this.props.orderDetails} status={this.props.status} color={this.props.isRequested === false ? 'grey' : 'primary'} />
									</Col>
									<Col className="text-nowrap p-0">
										<CancelOrderDailog isRequested ={isRequested} orderDetails={this.props.orderDetails} status={this.props.status} color={this.props.isRequested === true ? 'grey' : 'primary'} cancelText = {this.props.cancelText} />
									</Col>
								</Row>
							)
						}
					}
					)}
				</Breadcrumb>

				<Dialog open={this.state.open} onClose={this.handleRequestClose}
					fullWidth={true}
					maxWidth={'sm'}>
					<DialogTitle className="pb-1">
						Change Request
            <DialogContentText className="mb-2">
						{this.props.text && this.props.text}
            </DialogContentText>
					</DialogTitle>
					<DialogContent className="pt-0">
						{ removedItem && removedItem.map((data,index) => {
							return <h4 className='text-muted pb-1'> {++index}. {data.productName} </h4>
							})
						}
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleRequestClose} color='secondary' >
							No
            </Button>
						<Button onClick={(e) => this.handleSubmitRequest(e)} color='primary'>
							Yes
            </Button>
					</DialogActions>
				</Dialog>
			</div >
		)
	}
};


export default connect(null, { updateOrder, getOrderDetails })(withRouter(OrderDetailsHeader));


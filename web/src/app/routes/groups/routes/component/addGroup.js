import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Col, Row } from 'reactstrap';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DialogContentText from '@material-ui/core/DialogContentText';
import { listPermissionModules, addGroup } from '../../../../../actions/seller';
import { connect } from 'react-redux';
import ReactStrapTextField from '../../../../../components/ReactStrapTextField';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required } from '../../../../../constants/validations';
import { NotificationManager } from 'react-notifications';

var error = false
class AddGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			permission: [],
			admin_access: false
		}
	}

	componentDidMount = () => {
		this.props.listPermissionModules({ history: this.props.history })
	}


	handleRequestClose = (e) => {
		e.preventDefault();
		error = false;
		this.props.initialize({name : ''})
		this.props.handleClick('addGroup')
	};

	onSubmit = () => {
		let data = {
			permissions: this.state.permission,
			admin_access: this.state.admin_access,
			name: this.state.name
		}
		if (data.permissions.length > 0) {
			this.props.addGroup({ history: this.props.history, data, listGroup: { searchText: '', page: 1, perPage: 10 } });
			this.props.handleClick('addGroup')
			this.setState({ permission: [], admin_access: false, name: '' });
		} else {
			error = true
			NotificationManager.error("Please select at least one permission.")
		}
	}


	handleChange = (e, key) => {
		this.setState({ [key]: e.target.value });
	}

	handleAdminCheck = (e) => {
		e.preventDefault();
		if (this.state.admin_access === false) {
			this.setState({ permission: this.props.listpermissionModulesdata, admin_access: true });
		} else {
			this.setState({ permission: [], admin_access: false });
		}
	}

	handleCheck = (e, index) => {
		e.preventDefault();
		if(!this.state.admin_access){
			let tempPermission = [...this.state.permission];
		let permissionIndex = tempPermission.findIndex((e) => e._id == this.props.listpermissionModulesdata[index]._id);
		if (permissionIndex > -1) {
			tempPermission.splice(permissionIndex, 1);
			this.setState({ permission: tempPermission });
		} else {
			tempPermission.push(this.props.listpermissionModulesdata[index]);
			this.setState({ permission: tempPermission });
		}
		if (tempPermission.length === this.props.listpermissionModulesdata.length) {
			this.setState({ permission: tempPermission, admin_access: true });
		}
		}
		
		
	}

	render() {
		const { name, permission } = this.state;
		const { buttonType, add_group, title, listpermissionModulesdata, handleSubmit, selectedData } = this.props;

		return (
			<React.Fragment>
				<Dialog open={add_group} onClose={(e) => this.handleRequestClose(e)}
					fullWidth={true}
					maxWidth={'sm'}>
					<form onSubmit={handleSubmit(this.onSubmit)}>
						<DialogTitle>
							{title}
						</DialogTitle>
						<DialogContent>
							<Row>
								<Col >
									<Field id="name" name="name" type="text"
										component={ReactStrapTextField} label="Name"
										validate={[required]}
										value={name}
										onChange={(event) => this.handleChange(event, 'name')}
									/>
								</Col>
							</Row>
							<Row>
								<Col xs={12} md={12} sm={12} xl={12} lg={12}>
									<Row>
										<Col xs={6} md={6} sm={6} xl={6} lg={6}>
											<FormControlLabel
												onClick={(e) => this.handleAdminCheck(e)}
												control={
													<Checkbox
														checked={listpermissionModulesdata !== undefined && listpermissionModulesdata.length === permission.length}
														value={'admin_access'}
														color="primary"
													/>
												}
												label={'Admin Access'}
											/>
										</Col>
										{
											(listpermissionModulesdata !== undefined && listpermissionModulesdata.length > 0) ? listpermissionModulesdata.map((value, key) => {
												let index = permission.findIndex((e) => e._id === value._id)
												return <Col xs={6} md={6} sm={6} xl={6} lg={6}>
													<FormControlLabel
														onClick={(e) => this.handleCheck(e, key)}
														disabled={listpermissionModulesdata.length === permission.length ? true : false}
														control={
															<Checkbox
																checked={index > -1 ? true : false}
																value={value.value}
																color="primary"
																disabled={listpermissionModulesdata.length === permission.length ? true : false}
															/>
														}
														label={value.name}
													/>
												</Col>
											}) : ''
										}
									</Row>
								</Col>
							</Row>
						</DialogContent>
						<DialogActions className="pr-4">
							<Button onClick={(e) => this.handleRequestClose(e)} color='secondary' >	Cancel </Button>
							<Button type="submit" color='primary'>Add </Button>
						</DialogActions>
					</form>
				</Dialog>
			</React.Fragment >
		);
	}
}

const mapStateToProps = ({ seller }) => {
	const { listpermissionModulesdata } = seller;
	return { listpermissionModulesdata }
};

AddGroup = connect(
	mapStateToProps,
	{
		listPermissionModules,
		addGroup
	}               // bind account loading action creator
)(AddGroup)

export default AddGroup = reduxForm({
	form: 'AddGroup',// a unique identifier for this form
	onSubmitSuccess: (result, dispatch) => {
		if (!error) {
			const newInitialValues = getFormInitialValues('AddGroup')();
			dispatch(initialize('AddGroup', newInitialValues));
		}
	}
})(AddGroup)
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Col, Row } from 'reactstrap';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DialogContentText from '@material-ui/core/DialogContentText';
import { listPermissionModules, addGroup } from '../../../../../actions/seller';
import { connect } from 'react-redux';
import ReactStrapTextField from '../../../../../components/ReactStrapTextField';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { required } from '../../../../../constants/validations';
import { NotificationManager } from 'react-notifications';
let error = false;
class EditGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            permission: [],
            admin_access: false,
            groupId: ''
        }
    }

    componentDidMount = () => {
        this.props.listPermissionModules({ history: this.props.history })
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        if (this.props !== nextProps) {
            return true;
        }
        if (this.state !== nextState) {
            return true;
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.selectedData !== prevProps.selectedData) {
            let tempState = this.state.permission;
            tempState = this.props.selectedData ? this.props.selectedData.Permissions : tempState;
            this.props.dispatch(change('EditGroup', 'name', this.props.selectedData ? this.props.selectedData.name : ''));
            if (this.props.selectedData && this.props.selectedData.Permissions.length === this.props.listpermissionModulesdata.length) {
                this.setState({ permission: tempState, name: this.props.selectedData.name, admin_access: true, groupId: this.props.selectedData._id })
            } else {
                this.setState({ permission: tempState, name: this.props.selectedData ? this.props.selectedData.name : this.state.name, admin_access: false, groupId: this.props.selectedData ? this.props.selectedData._id : this.state.groupId })
            }
        }
    }

    handleRequestClose = (e) => {
        e.preventDefault();
        error = false;
        this.props.initialize({ name: this.props.selectedData ? this.props.selectedData.name : '' });
        this.props.handleClick('editGroup');
    };

    onSubmit = () => {
        let data = {
            permissions: this.state.permission,
            admin_access: this.state.admin_access,
            name: this.state.name,
            groupId: this.state.groupId
        }
        if (data.permissions.length > 0) {
            this.props.handleClick('editGroup');
            this.props.addGroup({ history: this.props.history, data, listGroup: { searchText: '', page: 1, perPage: 10 } })
            this.setState({ permission: [], admin_access: false, name: '' });
        } else {
            error = true;
            NotificationManager.error("Please select at least one permission.")
        }
    }


    handleChange = (e, key) => {
        this.setState({ [key]: e.target.value });
    }

    handleAdminCheck = (e) => {
        e.preventDefault();
        if (this.state.admin_access === false) {
            this.setState({ permission: this.props.listpermissionModulesdata, admin_access: true });
        } else {
            this.setState({ permission: [], admin_access: false });
        }
    }

    handleCheck = (e, index) => {
        e.preventDefault();
        let tempPermission = this.state.permission;

        let permissionIndex = tempPermission.findIndex((e) => e._id == this.props.listpermissionModulesdata[index]._id);
        if (permissionIndex > -1) {
            tempPermission.splice(permissionIndex, 1);
            this.setState({ permission: tempPermission });
        } else {
            tempPermission.push(this.props.listpermissionModulesdata[index]);
            this.setState({ permission: tempPermission });
        }
        if (tempPermission.length === this.props.listpermissionModulesdata.length) {
            this.setState({ permission: tempPermission, admin_access: true });
        }
    }

    render() {
        const { name, permission } = this.state;
        const { editGroup, title, listpermissionModulesdata, handleSubmit, selectedData } = this.props;

        return (
            <React.Fragment>
                <Dialog open={editGroup} onClose={(e) => this.handleRequestClose(e)}
                    fullWidth={true}
                    maxWidth={'sm'}>
                    <form onSubmit={handleSubmit(this.onSubmit)}>
                        <DialogTitle>
                            {title}
                        </DialogTitle>
                        <DialogContent>
                            <Row>
                                <Col >
                                    <Field id="name" name="name" type="text"
                                        component={ReactStrapTextField} label={"Name"}
                                        validate={[required]}
                                        value={name}
                                        onChange={(event) => this.handleChange(event, 'name')}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} md={12} sm={12} xl={12} lg={12}>
                                    <Row>
                                        <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={listpermissionModulesdata !== undefined && listpermissionModulesdata.length === permission.length}
                                                        onClick={(e) => this.handleAdminCheck(e)}
                                                        value={'admin_access'}
                                                        color="primary"
                                                    />
                                                }
                                                label={'Admin Access'}
                                            />
                                        </Col>
                                        {
                                            (listpermissionModulesdata !== undefined && listpermissionModulesdata.length > 0) ? listpermissionModulesdata.map((value, key) => {
                                                let index = permission.findIndex((e) => e._id === value._id)
                                                return <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                checked={index > -1 ? true : false}
                                                                onClick={(e) => this.handleCheck(e, key)}
                                                                value={value.value}
                                                                color="primary"
                                                                disabled={listpermissionModulesdata.length === permission.length ? true : false}
                                                            />
                                                        }
                                                        label={value.name}
                                                    />
                                                </Col>
                                            }) : ''
                                        }
                                    </Row>
                                </Col>
                            </Row>
                        </DialogContent>
                        <DialogActions className="pr-4">
                            <Button onClick={(e) => this.handleRequestClose(e)} color='secondary' >	Cancel </Button>
                            <Button type="submit" color='primary'> Edit </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </React.Fragment >
        );
    }
}


const mapStateToProps = ({ seller }) => {
    const { listpermissionModulesdata } = seller;
    return { listpermissionModulesdata }
};

EditGroup = connect(
    mapStateToProps,
    {
        listPermissionModules,
        addGroup
    }           // bind account loading action creator
)(EditGroup)

export default EditGroup = reduxForm({
    form: 'EditGroup',// a unique identifier for this form
    enableReinitialize: true,
    onSubmitSuccess: (result, dispatch) => {
        if (!error) {
            const newInitialValues = getFormInitialValues('EditGroup')();
            dispatch(initialize('EditGroup', newInitialValues));
        }
    }
}, mapStateToProps)(EditGroup)
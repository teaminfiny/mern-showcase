import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import Button from '@material-ui/core/Button';
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
// import buyerImage from 'app/sellers/buyerImage';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import customHead from 'constants/customHead';
import DeleteGroup from '../../inventory/component/deleteInventory';
import AddGroup from './component/addGroup';
import EditGroup from './component/editGroup';
import { listGroup } from '../../../../actions/seller';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';

const columns = [
  {
    name: "name",
    label: "Name",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "noOfMember",
    label: "No Of Members",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "action",
    label: "Action",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  }
];

class Groups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editGroup: false,
      delete: false,
      addGroup: false,
      page: 0,
      searchedText: '',
      perPage: 50,
      selectedData : {}
    }
  }

  changePage = (page) => {
    let pages = page + 1
    this.props.listGroup({ userId: '', searchText: this.state.searchedText, page: pages, perPage: this.state.perPage })
    this.setState({ page })
  };

  changeRowsPerPage = (perPage) => {
    this.props.listGroup({ page: 1, perPage })
    this.setState({ page: 0, perPage })
  }

  handleSearch = (searchText) => {
    this.props.listGroup({ userId: '', searchText: searchText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage })
    this.setState({ searchedText: searchText })
  };

  handleClick = (key, value) => {
    this.setState({ [key]: !this.state[key], selectedData : value })
  }

  componentDidMount = () => {
    this.props.listGroup({ searchText: this.state.searchedText, page: 1, perPage: this.state.perPage })
  }

  button = (data) => (
    <ButtonGroup color="primary" aria-label="outlined primary button group">
      <Button className={'text-primary'} onClick={() => this.handleClick('editGroup', data)} > Edit</Button>
      <Button className={'text-danger'} onClick={() => this.handleClick('delete', data)}> Delete</Button>
    </ButtonGroup>
  )

  render() {
    let { listGroupData } = this.props;
    let { selectedData } = this.state;
    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      // rowsPerPage: this.state.perPage,
      rowsPerPage: 50,
      rowsPerPageOptions: [],
      page: this.state.page,
      fixedHeader: false,
      print: false,
      download: false,
      filter: false,
      sort: false,
      selectableRows: false,
      count: this.props.listGroupData.length > 0 ? this.props.listGroupData[0].metadata.length > 0 ? this.props.listGroupData[0].metadata[0].total : 0 : 0,
      serverSide: true,
      rowsPerPage: this.state.perPage,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      searchText: this.state.searchedText,
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
        }
      },
    }

    var groupData = [];
    listGroupData.length > 0 && listGroupData[0].data.length > 0 && listGroupData[0].data.map((data, index) => {
      groupData.push([data.name, data.members, this.button(data)]);
    });

    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Groups" />} />
        {
          this.props.loading == false ?
            <MUIDataTable
              title={<Button className={'text-primary'} selectedData={selectedData} onClick={(e) => this.handleClick('addGroup', selectedData)}> Add Group +</Button>}
              data={groupData}
              columns={columns}
              options={options}
            />
            :
            <div className="loader-view" style={{ textAlign: "center", marginTop: "300px" }}>
              <CircularProgress />
            </div>
        }
        <EditGroup selectedData={selectedData} editGroup={this.state.editGroup} title={'Edit Group'} handleClick={(e) => this.handleClick('editGroup',selectedData)} />
        <AddGroup buttonType={'addGroup'}  add_group={this.state.addGroup} title={'Add Group'} handleClick={(e) => this.handleClick('addGroup',selectedData)} />
        <DeleteGroup delete={this.state.delete} selectedData={selectedData} deleteFor={'group'} handleClick={this.handleClick} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ seller }) => {
  const { listGroupData, loading } = seller;
  return { listGroupData, loading }
};

Groups = connect(
  mapStateToProps,
  {
    listGroup
  }               // bind account loading action creator
)(Groups)

export default Groups;
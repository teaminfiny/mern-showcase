import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { getUserDetail } from '../../../../actions/seller'
import moment from "moment";
import Avatar from '@material-ui/core/Avatar';
import { connect } from 'react-redux'
import { DatePicker } from 'material-ui-pickers';
import Tooltip from '@material-ui/core/Tooltip';
import axios from '../../../../constants/axios';
import { NotificationManager } from 'react-notifications';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import AppConfig from 'constants/config'
import SaveEditProfileModal from 'components/BEditProfile/SaveEditProfileModal'
class ComplianceForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expiryDate: moment().add(2, 'M'),
            minimumDate: moment(),
            value: '',
            isVisited:false,
            licNo:'',
            openModal: false,
        }
    }
    componentDidMount() {
        // this.setState({value:})
        if (this.props.fileKey !== undefined) {
            let user = this.props.userDetails.mainUser?this.props.userDetails.mainUser:this.props.userDetails
            this.setState({ value: (user[this.props.fileKey] && user[this.props.fileKey].name !== undefined) ? `${AppConfig.baseUrl}users/${user[this.props.fileKey].name}` : '',
             expiryDate: user[this.props.fileKey] ? user[this.props.fileKey].expires : this.state.expiryDate,
            licNo:user[this.props.fileKey] ? user[this.props.fileKey].lic : this.state.licNo})
        }
    }
    handleFileSelect(e) {
        // const { input: { onChange } } = this.props
        let document = "";
        let reader = new FileReader();
        if (e.target.files.length > 0) {
            reader.readAsDataURL(e.target.files[0]);
            reader.onload = () => {
                let data = {}
                document = reader.result;
                data[this.props.fileKey] = document

                this.setState({value:document})
                // this.apiCall('value', data)
                // onChange(document)

            };
            reader.onerror = function (error) {
            };
        }

    }
    apiCall = (key, data) => {
        axios.post('/users/updateComplainceForm', data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            },
        }
        ).then(result => {
            this.setState({ loader: false })
            if (result.data.error) {
                NotificationManager.error(result.data.title);
            } else {
                NotificationManager.success('Compliance details updated successfully.');
                // this.setState({ [key]: key === 'value' ? `${AppConfig.baseUrl}users/${result.data.detail[this.props.fileKey].name}` : result.data.detail[this.props.fileKey].expires })
                // this.props.getUserDetail()
                this.setState({openModal:false})
                setTimeout(() => {
                    this.props.getUserDetail({ userType: 'seller' }) 
                  }, 2000);
            }
        })
        .catch(error => {
            this.setState({ loader: false })
            NotificationManager.error('Something went wrong, Please try again')
        });
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.userDetails!==prevProps.userDetails){
            let user = this.props.userDetails.mainUser?this.props.userDetails.mainUser:this.props.userDetails
            if (this.props.fileKey !== undefined) {
                this.setState({ value: (user[this.props.fileKey] && user[this.props.fileKey].name !== undefined) ? `${AppConfig.baseUrl}users/${user[this.props.fileKey].name}` : '',
                expiryDate: user[this.props.fileKey] ? user[this.props.fileKey].expires : this.state.expiryDate,
                licNo:user[this.props.fileKey] ? user[this.props.fileKey].lic : this.state.licNo})
            }
        }
    }
    handledateChange = (value, key) => {
        let data = {}
        data[this.props.name] = value
        // this.apiCall('expiryDate', data)
        this.setState({ expiryDate: value })
    }
    handleChange = (e)=>{
        this.setState({licNo:e.target.value,isVisited:true})
    }
    handleFocus = ()=>{
        this.setState({isVisited:true})
    }
    handleSubmit = ()=>{
        let {value,licNo,expiryDate} = this.state
        if(value&&licNo){
            let data = {}
            data[this.props.fileKey] = value.length>200?value:'';
            data[this.props.name] = expiryDate;
            data[this.props.licKey] = licNo
            this.handleOpenModal('value', data)
            
        }else if(!licNo){
            this.setState({isVisited:true})
            
        }else if(!value){
            NotificationManager.error('Please upload photo')
        }
    }
    getDisabled = ()=>{
        let user = this.props.userDetails.mainUser?this.props.userDetails.mainUser:this.props.userDetails
        let expiryDate =  user[this.props.fileKey] ? user[this.props.fileKey].expires : this.state.expiryDate
        let imageValue = (user[this.props.fileKey] && user[this.props.fileKey].name !== undefined) ? `${AppConfig.baseUrl}users/${user[this.props.fileKey].name}` : ''
        let licNo = user[this.props.fileKey] ? user[this.props.fileKey].lic : this.state.licNo
        if(this.state.expiryDate==expiryDate&&imageValue==this.state.value&&licNo==this.state.licNo){
            return true
        }else{
            return false
        }
    }
    handleOpenModal = () => {
        this.setState({ openModal: true })
      }
    
      handleClose = () => {
        this.setState({ openModal: false })
      }
      onSave = ()=>{
        let {value,licNo,expiryDate} = this.state
        let data = {}
        data[this.props.fileKey] = value.length>200?value:'';
        data[this.props.name] = expiryDate;
        data[this.props.licKey] = licNo
        this.apiCall('value',data)
      }
    render() {
        // const { input: { value },name } = this.props
        const { minimumDate, expiryDate, value ,isVisited,licNo,openModal} = this.state
        const { input, label, required, name,label2, } = this.props  //whatever props you send to the component from redux-form Field
        let isMainUser = this.props.userDetails.mainUser?false:true
        return (
            <React.Fragment>
            
                <div className='text-center'>
                    <div className="p-2">
                        <input
                            type='file'
                            accept='.jpg, .png, .jpeg, .pdf'
                            style={{ display: 'none' }}
                            onChange={(e) => this.handleFileSelect(e)} ref={(ref) => this.drugLic20B = ref}
                        />
                        <div class="img-wrap">

                        </div>
                        <img src={value !== '' ? require('assets/img/uploaded.png') : require('assets/img/file.png')} className='d-block mx-auto' />
                    </div>
                    <label className="text-dark" style={{ fontSize: 17 }}>{label}</label>
                </div>
                {
                    name &&
                    <div className="pt-2 text-center">
                        <span>Expiry date : </span>
                        <DatePicker
                            onChange={this.handledateChange}
                            name={name}
                            value={expiryDate}
                            minDate={minimumDate}
                            // autoOk
                            // fullWidth
                            id="datePickerStyle"
                            className={'datePickerStyle cursor-pointer'}
                            leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                            rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                            format="MM/DD/YYYY"
                        />

                    </div>
                }
                <div className="pt-2 text-center">
                        <p>{label2}</p>
                        <TextField
                            error = {isVisited&&!licNo}
                            id="outlined-error-helper-text"
                            label={''}
                            className={'cursor-pointer liCNo'}
                            helperText={isVisited&&!licNo?"This field is required":""}
                            margin="normal"
                            variant="standard"
                            value = {licNo}
                            onClick = {this.handleFocus}
                            onChange = {(e)=>this.handleChange(e)}
                            />
                </div>

                <div className='row mx-md-n5 text-center justify-content-center d-flex pt-2'>
                    <div className='mr-1 ml-1'>
                        <Tooltip title={`Upload ${label}`}>
                            <Avatar className='bg-primary cursor-pointer' onClick={(e) => this.drugLic20B.click()} size={2}>
                                <i className='zmdi zmdi-upload'></i>
                            </Avatar>
                        </Tooltip>
                    </div>
                    <div className='mr-1 ml-1'>
                        <Tooltip title={`${value ? 'View' + ' ' + label : label + ' not uploaded'} `} >
                            <Avatar className='bg-warning' color='primary' size={2}>
                                <a href={value} style={value ? {padding : 0} : { pointerEvents: 'none', cursor: 'default'}} disabled={value ? false : true} target='_blank'><i className='zmdi zmdi-eye text-white'></i></a>
                            </Avatar>
                        </Tooltip>
                    </div>
                    {
                        !this.getDisabled()&&isMainUser&&
                        <div className='mr-1 ml-1'>
                        <Tooltip title={`Save ${label}`}>
                            <Avatar  className={`bg-success cursor-pointer `} onClick={(e) => this.handleSubmit()} size={2}>
                                <i className='zmdi zmdi-floppy'></i>
                            </Avatar>
                        </Tooltip>
                    </div>
                    }
                    
                </div >
                <Dialog open={openModal == true}
          onClose={this.handleClose}
          fullWidth={true}
        >
          <DialogTitle id="alert-dialog-title" ><h2 style={{ color: "#fd4081" }}>Important Message</h2></DialogTitle>
          <DialogContent>

            <SaveEditProfileModal />

          </DialogContent>
          <DialogActions className="pr-4">
            <Button onClick={this.onSave} color='primary'> SAVE </Button>
            <Button onClick={this.handleClose} color='secondary' >	Cancel </Button>
          </DialogActions>
        </Dialog>
            </React.Fragment >
        );
    }
}
const mapStateToProps = ({ seller }) => {
    const { userDetails } = seller;
    return { userDetails }
}
export default connect(mapStateToProps, { getUserDetail })(ComplianceForm);
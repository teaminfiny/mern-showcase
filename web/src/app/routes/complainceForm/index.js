import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from '../../../util/asyncComponent';

const ComplainceForm = ({match}) => (
  <div className="app-wrapper">
    <Switch>
      <Redirect exact from={`${match.url}/complaince-form`} to={`${match.url}/`}/>
      <Route path={`${match.url}/`} component={asyncComponent(() => import('./routes/ComplainceForm'))}/>
      <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/>
    </Switch>
  </div>
);

export default ComplainceForm;

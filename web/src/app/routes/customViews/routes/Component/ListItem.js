import React from 'react';
import moment from 'moment'

const ListItem = ({ styleName, data }) => {
  const { createdAt, message } = data;
  return (
    <div className={`user-list d-flex flex-row  ${styleName}`}>
      {/* <Avatar
        alt='U'
        style={{ fontSize: 20, background: helperFunction.getGradient(buyerId.backGroundColor), width: 20, height: 20, marginRight: 5 }}
      >
        {
          buyerId !== undefined ? helperFunction.getNameInitials(`${buyerId.first_name} ${buyerId.last_name}`) : ''
        }
      </Avatar> */}
      <div className="description">
        {/* <h5>{buyerId.first_name} {buyerId.last_name}</h5> */}
        <p className="text-muted">{message}</p>
        <ul className="list-inline d-sm-flex flex-sm-row ">
          <li style={{ fontSize: 13}}>{moment(createdAt).format('MMM Do YYYY, hh:mm A')}</li>
        </ul>
      </div>

    </div>
  );
}

export default ListItem;
import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from '../../../util/asyncComponent';

const TrackOrders = ({match}) => (
  <React.Fragment>
    <Switch>
      <Redirect exact from={`${match.url}/TrackOrder`} to={`${match.url}/:id`}/>
      <Route path={`${match.url}/:id`} component={asyncComponent(() => import('./routes/TrackOrder'))}/>
      <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/>
    </Switch>
  </React.Fragment>
);

export default TrackOrders;

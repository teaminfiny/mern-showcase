import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import asyncComponent from '../../../util/asyncComponent';
import OrderDetails from '../orderDetails';
import TrackOrder from '../trackOrder';
import { NavLink, withRouter } from 'react-router-dom';


const Orders = ({ match }) => (
  <div className="app-wrapper">
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/orderList`} />
      <Route path={`${match.url}/orderList`} fileKey={'UploadInvoice'} component={asyncComponent(() => import('./routes/OrdersTab'))} />
      <Route path={`${match.url}/orderDetails/`} component={OrderDetails} />
      <Route path={`${match.url}/trackOrder/`} component={TrackOrder} />
      <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))} />
    </Switch>
  </div>
);

export default withRouter(Orders);

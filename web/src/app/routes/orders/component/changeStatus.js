import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { getOrderDetails, getOrder } from '../../../../actions/order';
import { FormGroup, Label, Input, DropdownItem, DropdownMenu, DropdownToggle, ButtonDropdown } from 'reactstrap';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { withRouter, NavLink } from 'react-router-dom';
import moment from 'moment';
import ReactStrapTextField from '../../../../components/ReactStrapTextField';
import ReactstrapSelectField from '../../../../components/reactstrapSelectField';
import RenderReactDatePicker from '../../../../components/RenderReactDatePicker';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required, number, invoiceValue, invoiceValueOnline } from '../../../../constants/validations';
import axios from '../../../../constants/axios';
import { NotificationContainer, NotificationManager } from 'react-notifications';
// import FieldFileInput from '../../../../components/FieldFileInput';
import InvoiceValidate from '../routes/InvoiceValidate';
import Checkbox from '@material-ui/core/Checkbox';

class ChangeStatus extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      payment: '',
      invoice: '',
      weight: '',
      invoiceNumber: '',
      selectedDate: moment().format(),
      dropDownOpen: false,
      length:'',
      breadth:'',
      height:'',
      uploadedInvoice: '',
      set: false,
      check1: false,
      check2: false,
      check3: false,
    };
  }


  componentDidMount = () => {
    this.props.getOrderDetails({ history: this.props.history, data: { orderId: this.props.match.params.id } })
  }

  componentDidUpdate = (prevProps) => {
    if ( this.props.orderDetails !== prevProps.orderDetails) {
      if(this.props.orderDetails){
        this.setState({
          length:this.props.orderDetails?this.props.orderDetails.length?this.props.orderDetails.length:25:25,
          breadth:this.props.orderDetails.breadth?this.props.orderDetails.breadth:'25',
          height:this.props.orderDetails.height?this.props.orderDetails.height:'25', 
          invoice: this.props.orderDetails.total_amount,
          uploadedInvoice: this.props.orderDetails.uploadedInvoice ? this.props.orderDetails.uploadedInvoice : '', 
          set:false,
        });

        this.props.initialize({
          length:this.props.orderDetails.length ?this.props.orderDetails.length:'25',
          breadth:this.props.orderDetails.breadth?this.props.orderDetails.breadth:'25',
          height:this.props.orderDetails.height?this.props.orderDetails.height:'25',
          invoice: this.props.orderDetails.total_amount, 
          selectedDate: this.state.selectedDate,
          uploadedInvoice: this.props.orderDetails.uploadedInvoice ? this.props.orderDetails.uploadedInvoice : '', invoice2: this.props.orderDetails.total_amount

        });
      }
      
    }
  }

  handleRequestClose = () => {
    this.setState({ open: false });
    this.props.reset('ChangeStatus');
  };

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  handleClick = (e) => {
    e.preventDefault();
    this.props.history.push(`/seller/orders/orderDetails/${this.props.orderId}`)
  }

  handleTrackOrderClick = (e) => {
    this.props.history.push(`/seller/orders/trackOrder/${this.props.orderId !== undefined ? this.props.orderId : this.props.match.params.id}`);
  }

  toggle = () => {
    this.setState({
      dropDownOpen: !this.state.dropDownOpen
    })
  };

  handleDateChange = (date) => {
    this.setState({ selectedDate : new Date(date)})
  }


  onSubmit = async () => {
    await this.setState({ set:true })
    let data = {
      invoice: Number(this.state.invoice),
      weight: this.state.weight,
      invoiceNumber: this.state.invoiceNumber,
      selectedDate: moment().format(),
      orderId: this.props.orderId !== undefined ? this.props.orderId : this.props.match.params.id,
      status: 'Processed',
      length:this.state.length,
      breadth:this.state.breadth,
      height:this.state.height,
      uploadedInvoice: this.state.uploadedInvoice
    }
    if (this.state.set) {
      await axios.post('/order/proccessCancelOrder', data, {
        headers: {
          'Content-Type': 'application/json',
          'token': localStorage.getItem('token')
        }
      }
      ).then(result => {
        if (result.data.error) {
          NotificationManager.error(result.data.title)
          this.setState({ set:false, check1: false, check2: false, check3: false })
        } else {
          NotificationManager.success(result.data.title);
          this.props.getOrder({ history: this.props.history, data: { page: 1, perPage: 10 } });
          this.props.getOrderDetails({ history: this.props.history, data: { orderId: this.props.orderId !== undefined ? this.props.orderId : this.props.match.params.id } })
        }
      }).catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
    }
    this.props.reset('ChangeStatus');
    this.setState({ open: false });
  }

  handleFileSelect = async (e, key) => {
    e.preventDefault();
    let document = "";
    let reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = () => {
        document = reader.result;
        this.setState({ [key]: document })
    };
    reader.onerror = function (error) {
    };
    this.props = Object.assign({}, this.props, { key: document });
}

  handleChangeDocuments = (e)=>{
    this.setState({uploadedInvoice:e})
  }

  handleCheck = name => (event, checked) => {
    this.setState({[name]: checked});
  };
  render() {
    let { payment, invoice, weight, invoiceNumber, selectedDate, length, breadth, height, uploadedInvoice, set ,check1, check2, check3} = this.state;
    let { orderId, status, handleSubmit, orderDetails } = this.props;
    const path = this.props.match.path.substr(1);
    const subPath = path.split('/');

    return (
      <div>
        <div >
          <input
            type='file'
            accept='.jpg, .png, .jpeg, .pdf'
            style={{ display: 'none' }}
            onChange={(e) => this.handleFileSelect(e)} ref={(ref) => this.drug = ref}
          />
          {/* <img src={value !== '' ? require('assets/img/uploaded.png') : require('assets/img/file.png')} className='d-block mx-auto' /> */}
        </div>
        {
          this.props.match.path.includes('orderDetails') ? <Button className={'jr-btn-xs'} variant="contained" disabled={((this.props.orderDetails&&this.props.orderDetails.user_id&&this.props.orderDetails.user_id.user_status!=='active')||((status === 'Requested' || status === 'Cancelled' || status === "Processed")&&this.props.isRequested)||(status==='New'&&!this.props.isRequested) || (status === 'Delivered') ? true : false)} color="primary" onClick={() => this.setState({ open: true })}> Process Order</Button> :
            subPath[1] === 'orders' ? <React.Fragment>
              <ButtonGroup color="primary" aria-label="outlined primary button group">
                <Button variant="outlined" className={'text-warning'} onClick={(e) => this.handleClick(e)} >
                  Details
                </Button>
                <Button variant="outlined" onClick={(e) => this.handleTrackOrderClick(e)} className={'text-success'}>
                  Track
                </Button>
                <Button variant="outlined" disabled={(status === 'Requested' || status === 'Cancelled' || status === "Processed" || status === 'Delivered') ? true : false} onClick={() => this.setState({ open: true })}>
                  Process
                </Button>
              </ButtonGroup>
            </React.Fragment> : ''
        }

        <Dialog open={this.state.open} onClose={this.handleRequestClose}
          fullWidth={true}
          maxWidth={'sm'}>
          <form onSubmit={handleSubmit(this.onSubmit)}>
            <DialogTitle>
              Process Order
            <DialogContentText className="mt-3">
                {/* To subscribe to this website, please enter your email address here. We will send
                updates occasionally. */}
            </DialogContentText>
            </DialogTitle>
            <DialogContent>

              {/* <form noValidate autoComplete="off"> */}
                <React.Fragment>
                  {/* <FormGroup>
                    <Field id="date" name="date" type="text"
                      component={RenderReactDatePicker} label="Date"
                      // validate={[required]}
                      value = {moment(selectedDate).format()}
                      onChange={(date) => this.handleDateChange(date, 'selectedDate')}
                    />
                  </FormGroup> */}

                  <FormGroup>
                    {/* <Label for="cost">Value of invoice</Label>
                    <Input type="number" name="invoice" id="invoice" onChange={(e) => this.handleChange(e, 'invoice')} /> */}
                    <Field id="invoice2" name="invoice2" type="text" hide={true}
                      component={ReactStrapTextField} label={"Value of invoice ( 10% modification allowed )"}
                      // validate={[invoiceValue]}
                      // value={invoice}
                      // disabled={true}
                    />
                    { this.props.orderDetails && this.props.orderDetails.paymentType == 'COD' ?
                      <Field id="invoice" name="invoice" type="text"
                      component={ReactStrapTextField} label={"Value of invoice ( 10% modification allowed )"}
                      validate={[invoiceValue,number]}
                      onChange={(event) => this.handleChange(event, 'invoice')}
                      // value={invoice}
                      // disabled={true}
                    /> :
                    <Field id="invoice" name="invoice" type="text"
                      component={ReactStrapTextField} label={"Value of invoice ( 10% modification allowed )"}
                      validate={[invoiceValueOnline,number]}
                      onChange={(event) => this.handleChange(event, 'invoice')}
                      // value={invoice}
                      // disabled={true}
                    />
                  }

                  </FormGroup>

                  <FormGroup>
                    {/* <Label for="invoiceNumber">Invoice Number</Label>
                    <Input type="text" name="invoiceNumber" id="invoiceNumber" onChange={(e) => this.handleChange(e, 'invoiceNumber')} /> */}
                    <Field id="invoiceNumber" name="invoiceNumber" type="text"
                      component={ReactStrapTextField} label="Invoice Number"
                      validate={[required]}
                      value={invoiceNumber}
                      onChange={(event) => this.handleChange(event, 'invoiceNumber')}
                    />
                  </FormGroup>
                  <FormGroup>
                    {/* <Label for="invoiceNumber">Invoice Number</Label>
                    <Input type="text" name="invoiceNumber" id="invoiceNumber" onChange={(e) => this.handleChange(e, 'invoiceNumber')} /> */}
                    <Field id="length" name="length" type="text"
                      component={ReactStrapTextField} label="Length (cm)"
                      validate={[required]}
                      value={length}
                      disabled={true}
                    />
                  </FormGroup>
                  <FormGroup>
                    {/* <Label for="invoiceNumber">Invoice Number</Label>
                    <Input type="text" name="invoiceNumber" id="invoiceNumber" onChange={(e) => this.handleChange(e, 'invoiceNumber')} /> */}
                    <Field id="breadth" name="breadth" type="text"
                      component={ReactStrapTextField} label="Breadth (cm)"
                      validate={[required]}
                      value={breadth}
                      disabled={true}
                    />
                  </FormGroup>
                  <FormGroup>
                    {/* <Label for="invoiceNumber">Invoice Number</Label>
                    <Input type="text" name="invoiceNumber" id="invoiceNumber" onChange={(e) => this.handleChange(e, 'invoiceNumber')} /> */}
                    <Field id="height" name="height" type="text"
                      component={ReactStrapTextField} label="Height (cm)"
                      validate={[required]}
                      value={height}
                      disabled={true}
                    />
                  </FormGroup>

                  <FormGroup>
                    <Field
                      name="weight"
                      component={ReactstrapSelectField}
                      label="Weight"
                      validate={required}
                      value={weight}
                      type="select"
                      onChange={(event, newValue, previousValue) => this.handleChange(event, 'weight')}
                    >
                      <option value=" ">Select weight</option>
                      <option value='0.5'>0.5 kg</option>
                      <option value='1'>1.0 kg</option>
                      <option value='1.5'>1.5 kg</option>
                      <option value='2'>2.0 kg</option>
                      <option value='2.5'>2.5 kg</option>
                      <option value='3'>3.0 kg</option>
                      <option value='3.5'>3.5 kg</option>
                      <option value='4'>4.0 kg</option>
                    </Field>
                  </FormGroup>
                  {
                    orderDetails && orderDetails.orderType && orderDetails.orderType.toLowerCase() === 'cool chain' ?
                    <>
                      <FormGroup className='mb-0'>
                        <Checkbox
                          checked={this.state.check1}
                          onChange={this.handleCheck('check1')}
                          value={this.state.check1}
                          color="primary" />
                        I confirm that I have used Thermocol box for packing this order.
                    </FormGroup>
                    <FormGroup className='mb-0'>
                        <Checkbox
                          checked={this.state.check2}
                          onChange={this.handleCheck('check2')}
                          value={this.state.check2}
                          color="primary" />
                        I confirm that I have put 3 frozen chillpacks in this order.
                    </FormGroup>
                    <FormGroup className='mb-0'>
                      <Checkbox
                        checked={this.state.check3}
                        onChange={this.handleCheck('check3')}
                        value={this.state.check3}
                        color="primary" />
                      I undertake full responsibility for RTO due to non compliance.
                    </FormGroup> 
                    </> : ''
                  }
                  <FormGroup>

                  <Field 
                  value={uploadedInvoice} 
                  name="uploadedInvoice"
                  component={InvoiceValidate} 
                  validate = {[required]}
                  label="UploadInvoice" 
                  onChange={(e)=>this.handleChangeDocuments(e)}

                  // validate={(value)=> (value === undefined ? 'Please  Upload Invoice' : '')} 
                  />
                  
                  <span>{this.state.imageName}</span>
                  
                  </FormGroup>
                </React.Fragment>
              {/* </form> */}
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleRequestClose} color='secondary' >
                Cancel
            </Button>
              {
                orderDetails && orderDetails.orderType && orderDetails.orderType.toLowerCase() === 'cool chain' && !set && check1 && check2 && check3 ?
                  <Button type="submit" color='primary'>
                    Process
              </Button> :
                   (orderDetails && orderDetails.orderType == '' || orderDetails && orderDetails.orderType == undefined || orderDetails && orderDetails.orderType.toLowerCase() !== 'cool chain') && !set ?
                    <Button type="submit" color='primary'>
                      Process
                </Button> :
                    set ? <Button color='primary'>
                      Processing...
                </Button> :
                      <Button color='primary' disabled={true}>
                        Process
                </Button>
              }
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = ({ order }) => {
  const { orderDetails } = order;
  return { orderDetails }
};

ChangeStatus = connect(
  mapStateToProps,
  {
    getOrderDetails,
    getOrder
  }               // bind account loading action creator
)(ChangeStatus)

export default ChangeStatus = reduxForm({
  form: 'ChangeStatus',// a unique identifier for this form
  destroyOnUnmount: true,
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('ChangeStatus')();
    dispatch(initialize('ChangeStatus', newInitialValues));
  }
})(withRouter(ChangeStatus))

// export default connect(mapStateToProps, {getOrderDetails})(withRouter(ChangeStatus));

{/* 
  <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
<Field 
value={drugLic20B} 
name="drugLic20B" 
component={FieldFileInput} 
label="Drug Lic 20B" 
validate={(value)=> (value === undefined ? 'Please Upload Drug Lic 20B' : '')} 
/>
<Field name="drugLic20BLicNo" validate={[required]} label = 'Drug Lic20B no.' component={renderTextField}  />
<Field name="drugLic20BExpiry"  component={RenderDatePicker}  />
</Col> 

*/}
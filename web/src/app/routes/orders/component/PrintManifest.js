import React,{Component} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import {Col, Row, Form, Badge, Label, Input, DropdownItem, DropdownMenu, DropdownToggle, ButtonDropdown,FormGroup } from 'reactstrap';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import axios from 'axios'
import AppConfig from 'constants/config'

class PrintManifest extends Component{
  constructor(){
  super();
  this.state = {
    name:'',
    phone:''
    }
  }
  
  onSubmit = async() => {
    let newArr = []
    await this.props.orderArr.map((data)=>{
      newArr.push(data.order_id)
    })
    let data = { 
      orderArr: this.props.orderArr,
      orderID: newArr,
      name: this.state.name,
      phone: this.state.phone
    }
    await axios.post(`${AppConfig.baseUrl}shippingManifest/markManifested`, data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }
    ).then(result => {
      if (result.data.error) {
        NotificationManager.error(result.data.title)
      } else {
        NotificationManager.success(result.data.title);
        this.props.handleClick()
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }

  handleRequestClose = () => {
    this.props.handleClick()
  }

  handleChange = (key,e) => {
    this.setState({[key]: e.target.value})
  }

  render(){
    let { handleSubmit, orderArr, name, phone } = this.props;

    return(
      <Dialog open={this.props.open} onClose={this.handleRequestClose}
      fullWidth={true}
    maxWidth={'md'}>
      <DialogTitle className='pb-0'>
        Print Manifest
      </DialogTitle>
      <DialogContent>

      <Paper className={'tableRoot'}>
                {orderArr && orderArr.length > 0 ?
                    <Table className={'tableMain'} aria-label="spanning table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="center" size='small'>Order Id</TableCell>
                                <TableCell align="center" size='small'>Amount</TableCell>
                                <TableCell align="center" size='small'>Awb</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {orderArr.map((eachOrder, index) => (
                                <TableRow key={index}>
                                    <TableCell align="center" size='small'>{eachOrder.order_id}</TableCell>
                                    <TableCell align="center" size='small'>{eachOrder.amount}</TableCell>
                                    <TableCell align="center" size='small'>{eachOrder.awb}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    : 'No Orders Found'}

            </Paper>
            <Row className='pl-3 mt-2'>
              <FormGroup>
              {/* <Col sm={12} md={6} xs={12} lg={6} xl={6}> */}
                <Label for="name">Name</Label>
                <Input type="text" name="name" id="name" value={name} onChange={(e)=>this.handleChange('name',e)}/>
                {/* </Col> */}
              </FormGroup>
              <FormGroup className='ml-3'>
              {/* <Col sm={12} md={6} xs={12} lg={6} xl={6}> */}
                <Label for="phone">Phone</Label>
                <Input type="number" name="phone" id="phone" value={phone} onChange={(e)=>this.handleChange('phone',e)}/>
                {/* </Col> */}
              </FormGroup>
            </Row>
      </DialogContent>
      <DialogActions>
        <Button onClick={this.handleRequestClose} color='secondary' >
          Cancel
      </Button>
        <Button onClick={this.onSubmit} color='primary'>
          Print
      </Button>
      </DialogActions>
  </Dialog>
    )
  }
  
}
export default PrintManifest;
//  = reduxForm({
//   form: 'PrintManifest',// a unique identifier for this form
//   // onSubmitSuccess: (result, dispatch) => {
//   //   const newInitialValues = getFormInitialValues('PrintManifest')();
//   // }
// })(PrintManifest)
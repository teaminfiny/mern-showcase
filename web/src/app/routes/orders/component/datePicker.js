import React from 'react';
import { DatePicker } from 'material-ui-pickers';
import FormHelperText from '@material-ui/core/FormHelperText'
import moment from 'moment'

const renderFromHelper = ({ touched, error }) => {
  if (!(touched && error)) {
    return
  } else {
    return <FormHelperText style={{ color: '#f44336' }}>{touched && error}</FormHelperText>
  }
}

const RenderDatePicker = ({ label, input: { onChange, value, name }, meta: { touched, error } }) => {
  console.log('input.value',value)
  const selectedDate = value ?
    moment(value) : moment()
  return (
    <React.Fragment>
      <DatePicker
        onChange={onChange}
        name={name}
        label={label}
        value={selectedDate}
        // autoOk
        fullWidth
        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
        format="MM/DD/YYYY"
      />
      {renderFromHelper({ touched, error })}
    </React.Fragment>
  );
}

export default RenderDatePicker;
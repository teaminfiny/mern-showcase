import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { Col, Row, Form, Badge, Label, Input } from 'reactstrap';
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import buyerImage from 'app/sellers/buyerImage';
import customHead from 'constants/customHead'


const columns = [
  {
    name: "buyerName",
    label: "Buyer",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  /*  {
     name: "orders",
     label: "Orders",
     options: {
       filter: false,
       sort: false,
       customHeadRender: (o, updateDirection) => {
         return customHead(o, updateDirection);
       }
     }
   }, */
  {
    name: "amount",
    label: "Amount",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "status",
    label: "Status",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "date",
    label: "Date",
    options: {
      filter: true,
      filterType: 'custom',
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      },
      customFilterListRender: v => {
        return false;
      },
      filterOptions: {
        names: [],
        logic() {
          console.log('call filter api here..');
        },
        display: () => (
          <React.Fragment>
            <Row form>
              <Col md={6} xl={6} xs={6} lg={6}>
                <FormGroup>
                  <Label for="exampleSelect">Select Month</Label>
                  <Input type="select" name="select" id="exampleSelect">
                    <option>January</option>
                    <option>February</option>
                    <option>March</option>
                    <option>April</option>
                    <option>May</option>
                    <option>June</option>
                    <option>July</option>
                    <option>August</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col md={6} xl={6} xs={6} lg={6}>
                <FormGroup>
                  <Label for="exampleSelect">Select Month</Label>
                  <Input type="select" name="select" id="exampleSelect">
                    <option>2015</option>
                    <option>2016</option>
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <div style={{ paddingTop: 15 }} >
              <Button variant="contained" className='filterButton' color='primary'>Apply Filter</Button>
              <Button variant="contained" className='filterButton' color='primary'>Reset Filter</Button>
            </div>
          </React.Fragment>
        ),
      },
    }
  }
];

const options = {
  filterType: 'checkbox',
  print: false,
  download: false,
  selectableRows : 'none',
  selectableRowsOnClick: false,
  selectableRowsHeader: false,
  responsive: 'scrollMaxHeight',
  search: true,
  viewColumns: false
};

class RecentOrders extends Component {

  render() {
    const statusStyle = (status) => {
      return status.includes("Dispatched") ? "text-white bg-success" : status.includes("Ready for dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-primary" : "text-white bg-danger";
    }

    const data = [
      {
        buyerName: 'Avanish', amount: "₹10", status: <div className={` badge text-uppercase ${statusStyle('Placed')}`}>Placed</div>, date: '22/08/2019'
      },
      { buyerName: 'Abhidnya', amount: "₹20", status: <div className={` badge text-uppercase ${statusStyle('Ready for dispatch')}`}>Ready for dispatch</div>, date: '22/08/2019' },
      { buyerName: 'Priti', amount: "₹5", status: <div className={` badge text-uppercase ${statusStyle('Dispatched')}`}>Dispatched</div>, date: '22/08/2019' },
      { buyerName: 'Praful', amount: "₹50", status: <div className={` badge text-uppercase ${statusStyle('Dispatched')}`}>Dispatched</div>, date: '22/08/2019' },
      { buyerName: 'Ajay', amount: "₹21", status: <div className={` badge text-uppercase ${statusStyle('Dispatched')}`}>Dispatched</div>, date: '22/08/2019' },
      { buyerName: 'Eldhose', amount: "₹15", status: <div className={` badge text-uppercase ${statusStyle('Placed')}`}>Placed</div>, date: '22/08/2019' },
      { buyerName: 'Abhidnya', amount: "₹05", status: <div className={` badge text-uppercase ${statusStyle('Placed')}`}>Placed</div>, date: '22/08/2019' }
    ];

    return (
      <React.Fragment>
        <MUIDataTable
          title={'Recent Orders'}
          data={data}
          columns={columns}
          options={options}
        />
      </React.Fragment>
    );
  }
}

export default RecentOrders;
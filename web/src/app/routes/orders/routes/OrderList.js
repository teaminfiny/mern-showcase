import React, { Component } from "react";
import MUIDataTable from "../../../../components/DataTable";
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import customHead from 'constants/customHead'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { getOrder } from '../../../../actions/order';
import {Col, Row, Label, Input,FormGroup } from 'reactstrap';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { withRouter, NavLink } from 'react-router-dom';
import moment from 'moment';
import ReactStrapTextField from '../../../../components/ReactStrapTextField';
import ReactstrapSelectField from '../../../../components/reactstrapSelectField';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required, number, invoiceValue, invoiceValueOnline } from '../../../../constants/validations';
import axios from '../../../../constants/axios';
import { NotificationManager } from 'react-notifications';
import InvoiceValidate from './InvoiceValidate';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import PrintManifest from '../component/PrintManifest'
import { DatePicker } from 'material-ui-pickers';
import Tooltip from '@material-ui/core/Tooltip';
import helper from '../../../../constants/helperFunction';
import { MuiThemeProvider } from '@material-ui/core/styles';
import './index.css'
import AppConfig from "constants/config";

const customStyles = {
  BusinessAnalystRow: {
    '& td': { backgroundColor: "#00000012" }
  }
};

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let date = new Date();

class OrderList extends Component {
  constructor(props) {
    super(props);
    let url = props.location.search
    let json = url ? JSON.parse('{"' + decodeURIComponent(url.substring(1).replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}') : ''
    this.state = {
      filter: [],
      // page: 0,
      perPage: 100,
      openModal:false,
      open: false,
      payment: '',
      invoice: '',
      weight: '',
      invoiceNumber: '',
      selectedDate: moment().format(),
      dropDownOpen: false,
      length:Number,
      breadth:Number,
      height:Number,
      orderDetails:'',
      imageName:'',
      uploadedInvoice: '',
      searchedText:json?json.searchO ? json.searchO :'' :'',
      page: json?json.pageO ? Number(json.pageO) : 0:0,
      from:json? json.from_dateO ? json.from_dateO :moment().subtract(1, 'months').format():moment().subtract(1, 'months').format(),
      to:json? json.to_dateO ? json.to_dateO :moment().format():moment().format(),
      tempFrom: json.tempFromO ? json.from_dateO :moment().format(),
      tempTo: json.tempToO ? json.to_dateO :moment().format(),
      status:json?json.statusO ? json.statusO :'':'',
      loader:false,
      type:json?json.typeO ? json.typeO :'':'',
      orderArr: [],
      order: [],
      openPrint: false,
      openCheckbox: json ? json.openCheckbox ? true: false: false,
      manifest: json ? json.manifest ? false : '': '',
      selectedDate:json? json.from_dateO ? json.from_dateO :moment().subtract(1, 'months').format():moment().subtract(1, 'months').format(),
      selectedDateTo:json? json.to_dateO ? json.to_dateO :moment().format():moment().format(),
      set:false,
      check1: false,
      check2: false,
      check3: false,
    };

  }


  statusStyle = (status) => {
    return status.includes("Processed") ? "text-white bg-primary" : status.includes("Ready For Dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-yellow" : status.includes("Cancelled") ? "text-white bg-grey" : status.includes("Delivered") ? "text-white bg-green" : status.includes("Requested") ? "text-white bg-info" : status.includes("New") ? "text-white bg-danger" : "text-white bg-info";
  }
  openNewWindow = (data)=>{
    if(data){
      // window.open(`${data}`, '_blank');
      window.open(`${AppConfig.baseUrl}users/generateLabel1?orderId=${data.order_id}`, '_blank');
    }
  }
  button = (orderId, status) => {
    let disabled = status === 'New' ? false : true;
    return(<React.Fragment>
          <ButtonGroup color="primary" aria-label="outlined primary button group">
            <Button variant="outlined" className={'text-warning'} >
              {/* <a href={`/seller/orders/orderDetails/${orderId.order_id}`}
              style={{textDecoration: 'none', color: 'inherit', cursor: 'auto'}}> */}
              <NavLink to={`/seller/orders/orderDetails/${orderId.order_id}`} style={{color: 'inherit'}}>
                Details
              </NavLink>
                {/* </a> */}
            </Button>
            <Button variant="outlined" className={'text-success'}>
              <a target='_blank' href={`/seller/orders/trackOrder/${orderId.order_id}`} 
              style={{textDecoration: 'none', color: 'inherit', cursor: 'auto'}} > Track</a>
            </Button>
            {
              status==='Processed'?
              <Button variant="outlined" disabled = {!orderId.docket} onClick={() => this.openNewWindow(orderId)}>
                Docket
              </Button>
              : 
              <Button variant="outlined" disabled={this.disableChangeRequest(orderId,status)} onClick={() => this.openModal(orderId)}>
              Process
              </Button>
            }
            
             
          </ButtonGroup>
        </React.Fragment> )
    
  }

  
  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  componentDidMount = (prevProps) => {
    let data = {
      page: this.state.page + 1,
      perPage: this.state.perPage,
      filter: '',
      searchText: this.state.searchedText,
      from_date:moment(this.state.from).format('YYYY-MM-DD'),
      to_date:moment(this.state.to).add(1,'days').format('YYYY-MM-DD'),
      status:this.state.status ,
      type:this.state.type,
      // openCheckbox:false,
      isManifested: this.state.manifest,
      tab: 'orderListing'
    }
    if (this.props.tab == 0) {
    this.props.getOrder({ data, history: this.props.history })
    }
    
  }
  changePage = (page) => {
    let pages = page + 1
    let data = {
      page: pages,
      perPage: this.state.perPage,
      filter: '',
      searchText: this.state.searchedText,
      // month: this.state.month,
      // year: this.state.year,
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status ? this.state.status :''
    }
    this.setState({ page })
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: page,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)

    this.setState({ loader: true })
    const t = setTimeout(() => {
      this.setState({ loader: false })
    }, 2000);
  };
  openModal = (orderId)=>{
    this.setState({ open: true ,orderDetails:orderId})
    this.setState({
      length:orderId.length?orderId.length:25,
      breadth:orderId.breadth?orderId.breadth:25,
      height:orderId.height?orderId.height:25,
      invoice: orderId.total_amount,
      uploadedInvoice:this.state.uploadedInvoice?this.state.uploadedInvoice:'',
      set:false,
    });
    this.props.initialize({
      length:orderId.length?orderId.length:25,
      breadth:orderId.breadth?orderId.breadth:25,
      height:orderId.height?orderId.height:25,
      invoice: orderId.total_amount,
      selectedDate: this.state.selectedDate,
      invoice2: orderId.total_amount,
      uploadedInvoice:''
    });
  }

  changeRowsPerPage = (perPage) => {
    let data = {
      page: 1,
      perPage: perPage,
      filter: '',
      searchText: this.state.searchedText,
      // month: this.state.month,
      // year: this.state.year
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
    }
    this.props.getOrder({ data, history: this.props.history })
    this.setState({ page: 0, perPage })
  }
  handleSearch = (searchText) => {
    this.setState({ searchedText: searchText })
    let data = { searchText: searchText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage, 
      // month : this.state.month,year :this.state.year 
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status ? this.state.status :''
    }
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: 0,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
    // this.props.getOrder({ data, history: this.props.history })
  };
  handleCloseSearch=()=>{
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: 0,searchO:'',statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
  }

  resetFilter = (filter) => {
    // e.preventDefault();
    filter();
    this.setState({
        // month: moment().format("MMMM"),
        // year: moment().format("GGGG"),
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
      filter: '',
      to_date:'',
      from_date:'',
      status:'',
      from:'',to:''
    });
    let data = {
        page: 1,
        perPage: this.state.perPage,
        filter: this.state.filter,
        // month: moment().format("MMMM"),
        // year: moment().format("GGGG"),
        // month: monthNames[date.getMonth()],
        // year: date.getFullYear(),
        from_date:'',
        to_date:'',
        status:'',
        searchText:this.state.searchedText ? this.state.searchedText :''
    }
    // this.props.getOrder({ data, history: this.props.history })
    let obj = { from_dateO: '', to_dateO: '', pageO: '',searchO:this.state.searchedText,statusO:'',typeO:'' }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
}

  handleRequestClose = () => {
    this.setState({ open: false });
    this.props.reset('ChangeStatusP');
  };

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  handleClick = (data) => {
    this.props.history.push(`/seller/orders/orderDetails/${data.order_id}`)
    // this.props.history.push({pathname: `/seller/orders/orderDetails/${data.order_id}`, state: data.company_name})      

  }

  handleTrackOrderClick = (data) => {
    this.props.history.push(`/seller/orders/trackOrder/${data.order_id}`);
  }

  toggle = () => {
    this.setState({
      dropDownOpen: !this.state.dropDownOpen
    })
  };
  
  handleDateChange = (date,key) => {
    this.setState({ [key] :  moment(date).format() })
    // moment(date).format('MM/DD/YYYY')
  }


  onSubmit = async () => {
    await this.setState({ set:true })
    let data = {
      invoice: Number(this.state.invoice),
      weight: this.state.weight,
      invoiceNumber: this.state.invoiceNumber,
      selectedDate: moment().format(),
      orderId: this.state.orderDetails.order_id,
      status: 'Processed',
      length:this.state.orderDetails.length?this.state.orderDetails.length:25,
      breadth:this.state.orderDetails.breadth?this.state.orderDetails.breadth:25,
      height:this.state.orderDetails.height?this.state.orderDetails.height:25,
      uploadedInvoice: this.state.uploadedInvoice

    }
    if (this.state.set) {
      await axios.post('/order/proccessCancelOrder', data, {
        headers: {
          'Content-Type': 'application/json',
          'token': localStorage.getItem('token')
        }
      }).then(result => {
        if (result.data.error) {
          NotificationManager.error(result.data.title)
          this.setState({ set:false, check1: false, check2: false, check3: false })
        } else {
          NotificationManager.success(result.data.title);
          this.props.getOrder({
            history: this.props.history, data: {
              page: this.state.page + 1,
              perPage: this.state.perPage,
              from_date: moment(this.state.from).format('YYYY-MM-DD'),
              to_date: moment(this.state.to).add(1, 'days').format('YYYY-MM-DD'),
              tab: 'orderListing',
              searchText: this.state.searchedText,
              status: this.state.status,
              type: this.state.type,
              isManifested: this.state.manifest
            }
          });
          if (result.data.detail && result.data.detail.docket) {
            this.openNewWindow(result.data.detail.docket)
          }
          // this.props.getOrderDetails({ history: this.props.history, data: { orderId: data.orderId } })
        }
      }).catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
    }
    this.props.reset('ChangeStatusP');
    this.setState({ open: false });
  }
  disableChangeRequest = (data,status)=>{
		if(data&&data.user_id&&data.user_id.user_status!=='active'){
			return true
		}else if((status === 'Requested' || status === 'Approved'  || status === 'Processed' || status === "Cancelled")){
			return true
		}else{
			return false
		}
  }
  
  handleFileSelect = async (e, key) => {
    e.preventDefault();
    let document = "";
    let reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = () => {
        document = reader.result;
        this.setState({ [key]: document })
    };
    reader.onerror = function (error) {
    };
    this.props = Object.assign({}, this.props, { key: document });
}

  handleChangeDocuments = (e)=>{
    this.setState({uploadedInvoice:e})
  }
  applyFilter = (filter) => {
    filter()
    let data = {
      page: 1,
      perPage: 100,
      filter: '',
      searchText:this.props.searchText ? this.props.searchText :'',
      // month: this.state.month,
      // year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status
    }
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: this.state.page,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
      // this.props.getOrder({ data, history: this.props.history })
  }
  checkBox = (data) => {
    return this.state.openCheckbox && <Checkbox
      checked={this.state.orderArr.findIndex(value => value._id == data._id) > -1 ? true : false}
      onChange={() => this.handleChangeOrder(data)}
      color='primary'
      inputProps={{ 'aria-label': 'primary checkbox' }}
    />
  }

  handleChangeOrder = (data) => {
    let temp = [...this.state.orderArr]
    let tempAmount = this.state.amount
    let index = temp.findIndex(val => val._id == data._id)
    if (index > -1) {
      temp.splice(index, 1)
      // tempAmount = tempAmount - data.transaction_amt
    } else {
      // tempAmount = tempAmount + data.transaction_amt
      temp.push(data)
    }
    this.setState({ orderArr: temp })
  }

  handlePrint = async () => {
    const { orderArr } = this.state;
    const orderId = []
    this.setState({ openPrint:true })
    orderArr.map((data) => {
      orderId.push({_id:data._id, order_id: data.order_id, amount: data.total_amount, awb: data.shipping_awb})
    })
    await this.setState({order: orderId})
  }
  generateManifest = () => {
    this.setState({ 
      // manifest:true,
       openCheckbox: true })
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: this.state.page,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type, manifest: 'false', openCheckbox: true }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
  }
  cancelManifest = () => {
    this.setState({
      //  manifest:false,
      openCheckbox: false,  orderArr:[], openPrint:false })
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: this.state.page,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
  }

  handleCheck = name => (event, checked) => {
    this.setState({[name]: checked});
  };

  render() {
    let { orderData, } = this.props;
    let { payment, invoice, weight, invoiceNumber, selectedDate, length, breadth, height,status, uploadedInvoice,type,selectedDateTo,from,to, set, check1, check2, check3, orderDetails } = this.state;
    // let { payment, invoice, weight, invoiceNumber, selectedDate, length, breadth, height } = this.state;


    const options = {
      searchPlaceholder:'Press Enter to Search',
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      rowsPerPage: this.state.perPage,
      page: this.state.page ,
      fixedHeader: false,
      print: false,
      download: false,
      filter: true,
      sort: false,
      selectableRows: false,
      count: orderData && orderData.count ,
      serverSide: true,
      rowsPerPage: this.state.perPage,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      search : true,
      searchText: this.state.searchedText,
      textLabels: {
        filter: {
          all: "",
          title: "FILTERS",
        },
      },
      customToolbar: () => {
        return (
          <React.Fragment>
            { !this.state.openCheckbox ?
              <Button variant="outlined" className={'text-warning'} onClick={(e) => this.generateManifest(e)} >
              Generate Manifest</Button> :
              <Button variant="outlined" disableA={this.state.orderArr.length == 0} className={this.state.orderArr.length > 0 ? 'text-success' : 'text-grey'} onClick={(e) => this.handlePrint(e)} disabled={this.state.orderArr.length > 0 ? false : true}>Print Manifest</Button>
            }
            { this.state.openCheckbox &&
              <Button variant="outlined" className={'text-danger ml-2'} onClick={(e) => this.cancelManifest(e)} >
              Cancel</Button>
            }
          </React.Fragment>
        );
      },
        onTableChange: (action, tableState) => {
          switch (action) {
            case 'changePage':
              this.changePage(tableState.page);
              break;
            case 'changeRowsPerPage':
              this.changeRowsPerPage(tableState.rowsPerPage)
              break;
            case 'search':
              onkeypress = (e) => {
                if(e.key == 'Enter'){
                  this.handleSearch(tableState.searchText)
                }
              }
              break;
            case 'onSearchClose':
                this.handleCloseSearch();
              break;  
          }
        },
    }

    const columns = [
      {
        name: "",
        label: "",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "",
        label: "",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "orderId",
        label: "Order",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "amount",
        label: "Amount",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "date",
        label: "Date",
        options: {
          filter: true,
          filterType: 'custom',
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          },
          customFilterListRender: v => {
            return false;
          },
          filterOptions: {
            names: [],
            logic() {
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => (
              <React.Fragment >
                
                <Row form style={{ maxWidth: 300 }}>
                 
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                
                  <FormControl className="w-100 mb-2 ">
                  <Label for="fromDate">FROM</Label>
                     <DatePicker
                      onChange={(date) => this.handleDateChange(date, 'from')}
                      name='from'
                      id="from"
                      value={from}
                      // autoOk
                      fullWidth
                      leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                      rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                      format="DD/MM/YYYY"
                    />
                  </FormControl>
                </Col>
                  
               
               
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                
                  <FormControl >
                  <Label for="toDate">TO</Label>
                  <DatePicker
                    onChange={(date) => this.handleDateChange(date, 'to')}
                    name='to'
                    id="to"
                    value={to}
                    // autoOk
                    fullWidth
                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                    format="DD/MM/YYYY"
                  />
                  </FormControl>
                </Col>
                </Row> 

                <Row>
                 <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Status</Label>
                      <Input type="select" value={status} onChange={(e) => this.handleChange(e, 'status')} name="status" id="status">

                        <option value="" >Select Status</option>
                        <option value="New" >New</option>
                        <option value="Processed" >Processed</option>
                        <option value="Requested" >Requested</option>
                        <option value="Manifested" >Manifested</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Type</Label>
                      <Input type="select" value={type} onChange={(e) => this.handleChange(e, 'type')} name="type" id="type">
                        <option value="" >Select Type</option>
                        <option value="Bulk" >Bulk Prepaid</option>
                        <option value="COD" >COD</option>
                        <option value="Online" >Prepaid</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <div style={{ paddingTop: 15 }} >
                  <Button variant="contained" onClick={() => this.applyFilter(applyFilter)} className='filterButton' color='primary'>Apply Filter</Button>
                  <Button variant="contained" onClick={() => this.resetFilter(applyFilter)} className='filterButton' color='primary'>Reset Filter</Button>
                </div>
              </React.Fragment>
            ),
            onFilterChange: () => {
            }
          },
        }
      },
      {
        name: "action",
        label: "Action",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      }
    ];
 
    let data = []
      orderData && orderData.detail && orderData.detail.length > 0 && orderData.detail.map((data1, index) => {
        data.push([
        <span style={{display: 'flex',flexDirection: 'column'}}>
          {data1.orderType?<Tooltip title={`${data1.orderType}`} placement="top-start">
            <span style={helper.styleForDiv} className={`${helper.styleDivLine(data1.orderType)}`}>&nbsp;&nbsp;</span>
          </Tooltip>:<span style={helper.styleForDiv}>&nbsp;&nbsp;</span>}
          {data1.serviceName ? <Tooltip title={`${data1.serviceName}`} placement="top-start">
            <span style={helper.styleForDiv} className={`${helper.styleDivLine2(data1.serviceName)}`}>&nbsp;&nbsp;</span>
          </Tooltip> :
          <span style={helper.styleForDiv}>&nbsp;&nbsp;</span>
          } 
        </span>,
        <span>{this.checkBox(data1)}</span>,
        <div>
          <h4><span className="font-weight-bolder">{data1.products.length} {data1.products.length > 1 ? 'items' : 'item'} by </span>{data1.user_id.company_name} </h4>
        <p className="small">{data1.order_id}{data1.seller_invoice ? ` | ${data1.seller_invoice}` : ''} | {data1.isBulk ? 'Bulk Prepaid' : data1.paymentType == 'Online' ? 'Prepaid' : data1.paymentType }
        {(moment().diff(moment(data1.createdAt), 'hours')) <= 48 ?
          <span className="size-8 rounded-circle d-inline-block ml-1" style={{ backgroundColor: 'green' }}></span> :
          (moment().diff(moment(data1.createdAt), 'hours')) >= 48 && (moment().diff(moment(data1.createdAt), 'hours')) <= 72 ?
            <span className="size-8 rounded-circle d-inline-block ml-1" style={{ backgroundColor: 'orange' }}></span> :
            (moment().diff(moment(data1.createdAt), 'hours')) >= 72 && data1.order_status[data1.order_status.length - 1].status === 'New' ?
              <span className="size-8 rounded-circle d-inline-block ml-1 blink_me" style={{ backgroundColor: 'red' }}></span> : ''
        }</p>
        </div>,
        <div><p className={'ptrPriceFont1 priceColorInventory m-0'}>
          ₹{(Number(data1.total_amount)).toFixed(2)}
          </p></div>,
        <div key={'recent'} className={` badge text-uppercase ${this.statusStyle(data1.requested !== "Requested" ? data1.order_status[data1.order_status.length - 1].status : 'Requested')}`}>{data1.requested === "Requested" ? 'Requested' : data1.order_status[data1.order_status.length - 1].status}</div>,
        moment(data1.createdAt).format(' D MMM, YYYY h:mm a'),
        this.button(data1, data1.requested),<>{data1._id}</>
        ])
      })
    let { orderId,  handleSubmit,loading } = this.props;
    const { loader, openPrint } = this.state;
    return (
      <div>
        <div >
          <input
            type='file'
            accept='.jpg, .png, .jpeg, .pdf'
            style={{ display: 'none' }}
            onChange={(e) => this.handleFileSelect(e)} ref={(ref) => this.drug = ref}
          />
        </div>
        <div style={{ padding: '0px 1px 0 1px', boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)' }}>
        {loading ? <div className="loader-view"
          style={{ height: this.props.width >= 1200 ? 'calc(100vh - 259px)' : 'calc(100vh - 238px)' }}>
          <CircularProgress />
        </div> : null}
      {
          loading == true ?
            null :
        <MuiThemeProvider theme={helper.getMuiTheme()}>
          <MUIDataTable
            data={data}
            columns={columns}
            options={options}
            style={{ borderRadius: '0px !important' }}
          />
        </MuiThemeProvider>
      } 

            { openPrint && <PrintManifest open={openPrint} handleClick={this.cancelManifest} orderArr={this.state.order}/>}

          <Dialog open={this.state.open} onClose={this.handleRequestClose}
            fullWidth={true}
          maxWidth={'sm'}>
          <form onSubmit={handleSubmit(this.onSubmit)}>
            <DialogTitle className='pb-0'>
              Process Order
            </DialogTitle>
            <DialogContent>
                <React.Fragment>

                  <FormGroup>
                    <Field id="invoice2" name="invoice2" type="text" hide={true}
                      component={ReactStrapTextField} label={"Value of invoice ( 10% modification allowed )"}
                      // validate={[invoiceValue]}
                      // value={invoice}
                      // disabled={true}
                    />
                    {this.state.orderDetails.paymentType == 'COD' ?
                    <Field id="invoice" name="invoice" type="text"
                      component={ReactStrapTextField} label={"Value of invoice ( 10% modification allowed )"}
                      validate={[invoiceValue,number]}
                      onChange={(event) => this.handleChange(event, 'invoice')}
                      // value={invoice}
                      // disabled={true}
                    /> :
                    <Field id="invoice" name="invoice" type="text"
                      component={ReactStrapTextField} label={"Value of invoice ( 10% modification allowed )"}
                      validate={[invoiceValueOnline,number]}
                      onChange={(event) => this.handleChange(event, 'invoice')}
                      // value={invoice}
                      // disabled={true}
                    />
                  }
                    
                  </FormGroup>

                  <FormGroup>
                    <Field id="invoiceNumber" name="invoiceNumber" type="text"
                      component={ReactStrapTextField} label="Invoice Number"
                      validate={[required]}
                      value={invoiceNumber}
                      onChange={(event) => this.handleChange(event, 'invoiceNumber')}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Field id="length" name="length" type="text"
                      component={ReactStrapTextField} label="Length (cm)"
                      validate={[required]}
                      value={length}
                      disabled={true}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Field id="breadth" name="breadth" type="text"
                      component={ReactStrapTextField} label="Breadth (cm)"
                      validate={[required]}
                      value={breadth}
                      disabled={true}
                    />
                  </FormGroup>
                  <FormGroup>
                    <Field id="height" name="height" type="text"
                      component={ReactStrapTextField} label="Height (cm)"
                      validate={[required]}
                      value={height}
                      disabled={true}
                    />
                  </FormGroup>

                  <FormGroup>
                    <Field
                      name="weight"
                      component={ReactstrapSelectField}
                      label="Weight"
                      validate={required}
                      value={weight}
                      type="select"
                      onChange={(event, newValue, previousValue) => this.handleChange(event, 'weight')}
                    >
                      <option value=" ">Select weight</option>
                      <option value='0.5'>0.5 kg</option>
                      <option value='1'>1.0 kg</option>
                      <option value='1.5'>1.5 kg</option>
                      <option value='2'>2.0 kg</option>
                      <option value='2.5'>2.5 kg</option>
                      <option value='3'>3.0 kg</option>
                      <option value='3.5'>3.5 kg</option>
                      <option value='4'>4.0 kg</option>
                    </Field>
                  </FormGroup>
                  {
                    orderDetails && orderDetails.orderType && orderDetails.orderType.toLowerCase() === 'cool chain' ?
                    <>
                      <FormGroup className='mb-0'>
                        <Checkbox
                          checked={this.state.check1}
                          onChange={this.handleCheck('check1')}
                          value={this.state.check1}
                          color="primary" />
                        I confirm that I have used Thermocol box for packing this order.
                    </FormGroup>
                    <FormGroup className='mb-0'>
                        <Checkbox
                          checked={this.state.check2}
                          onChange={this.handleCheck('check2')}
                          value={this.state.check2}
                          color="primary" />
                        I confirm that I have put 3 frozen chillpacks in this order.
                    </FormGroup>
                    <FormGroup className='mb-0'>
                      <Checkbox
                        checked={this.state.check3}
                        onChange={this.handleCheck('check3')}
                        value={this.state.check3}
                        color="primary" />
                      I undertake full responsibility for RTO due to non compliance.
                    </FormGroup> 
                    </> : ''
                  }
                  <FormGroup>

                  <Field 
                  value={uploadedInvoice} 
                  name="uploadedInvoice"
                  component={InvoiceValidate} 
                  validate = {[required]}
                  label="UploadInvoice" 
                  onChange={(e)=>this.handleChangeDocuments(e)} 
                  />
                  

                  <span>{this.state.imageName}</span>

                  </FormGroup>
                </React.Fragment>
              
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleRequestClose} color='secondary' >
                Cancel
            </Button>
            {
                orderDetails && orderDetails.orderType && orderDetails.orderType.toLowerCase() === 'cool chain' && !set && check1 && check2 && check3 ?
                  <Button type="submit" color='primary'>
                    Process
              </Button> :
                  (orderDetails && orderDetails.orderType == '' || orderDetails && orderDetails.orderType == undefined || orderDetails && orderDetails.orderType.toLowerCase() !== 'cool chain') && !set ?
                    <Button type="submit" color='primary'>
                      Process
                </Button> :
                    set ? <Button color='primary'>
                      Processing...
                </Button> :
                      <Button color='primary' disabled={true}>
                        Process
                </Button>
            }
            </DialogActions>
          </form>
        </Dialog>
      </div>
      </div>

    );
  }
}

const mapStateToProps = ({ order }) => {
  const { orderData, loading } = order;
  return { orderData, loading }
};

OrderList= connect(mapStateToProps, { getOrder })(withStyles(customStyles)(OrderList));
export default OrderList = reduxForm({
  form: 'ChangeStatusP',// a unique identifier for this form
  destroyOnUnmount: true,
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('ChangeStatusP')();
    dispatch(initialize('ChangeStatusP', newInitialValues));
  }
})(withRouter(OrderList))
import React, { Component } from "react";
import MUIDataTable from "../../../../components/DataTable";
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';
import customHead from 'constants/customHead'
import { getOrderHistory } from '../../../../actions/order'
import { connect } from 'react-redux';
import AppConfig from 'constants/config'
import {Col, Row, Badge, Label, Input,FormGroup } from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import { reduxForm } from 'redux-form'
import { withRouter, NavLink } from 'react-router-dom';
import { DatePicker } from 'material-ui-pickers';
import Tooltip from '@material-ui/core/Tooltip';
import helper from '../../../../constants/helperFunction';
import { MuiThemeProvider } from '@material-ui/core/styles';

const customStyles = {
  BusinessAnalystRow: {
    '& td': { backgroundColor: "#00000012" }
  }
};

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let date = new Date();

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    let url = props.location.search
    let json = url ? JSON.parse('{"' + decodeURIComponent(url.substring(1).replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}') : ''
    this.state = {
      filter: [],
      // page: 0,
      perPage: 100,
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
      status:'',
      selectedDate: moment().format(),
      searchedText:json ?json.searchOH ? json.searchOH :'':'' ,
      page: json ? json.pageOH ? Number(json.pageOH) :0:0,
      from:json? json.from_dateOH ? json.from_dateOH :moment().subtract(1, 'months').format():moment().subtract(1, 'months').format(),
      to:json? json.to_dateOH ? json.to_dateOH :moment().format():moment().format(),
      tempFrom: json.tempFromOH ? json.from_dateOH :moment().format(),
      tempTo: json.tempToOH ? json.to_dateOH :moment().format(),
      status:json?json.statusOH ? json.statusOH :'':'',
      type:json?json.typeOH ? json.typeOH :'':'',
      selectedDate:json? json.from_dateOH ? json.from_dateOH :moment().subtract(1, 'months').format():moment().subtract(1, 'months').format(),
      selectedDateTo:json? json.to_dateOH ? json.to_dateOH :moment().format():moment().format()
    };

  }

  statusStyle = (status) => {
    return status.includes("Processed") ? "text-white bg-primary" : status.includes("Ready for dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-danger" : status.includes("Cancelled") ? "text-white bg-grey" : status.includes("Delivered") ? "text-white bg-green" : "text-white bg-danger";
  }

  button = (e) => (
    <ButtonGroup color="primary" aria-label="outlined primary button group">
      <a target='blank' download href={`${AppConfig.baseUrl}invoice/${e}`}>
      <Button variant="outlined" color="primary">
        Invoice
      </Button>
      </a>
    </ButtonGroup>
  )

  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  componentDidMount = () => {
    let data = {
      page: this.state.page + 1,
      perPage: this.state.perPage,
      filter: '',
      searchText: this.state.searchedText,
      from_date:moment(this.state.from).format('YYYY-MM-DD'),
      to_date:moment(this.state.to).add(1,'days').format('YYYY-MM-DD'),
      status:this.state.status ? this.state.status :'',
      type:this.state.type,
      tab: 'orderHistory'
    }
    if (this.props.tab == 1) {
    this.props.getOrderHistory({ data, history: this.props.history })
    }
  }

  changePage = (page) => {
    let pages = page + 1
    let data = {
      page: pages,
      perPage: this.state.perPage,
      filter: '',
      searchText: this.state.searchedText,
      // month: this.state.month,
      // year: this.state.year,
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status ? this.state.status :''
    }
    this.setState({ page })
    let obj = { from_dateOH: this.state.from, to_dateOH: this.state.to, pageOH: page
      ,searchOH:this.state.searchedText,statusOH:this.state.status,typeOH:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
    // this.props.getOrderHistory({ data, history: this.props.history })
    
  };

  changeRowsPerPage = (perPage) => {
    let data = {
      page: 1,
      perPage: perPage,
      filter: '',
      searchText: this.state.searchedText,
      // month: this.state.month,
      // year: this.state.year
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status ? this.state.status :''
    }
    this.props.getOrderHistory({ data, history: this.props.history })
    this.setState({ page: 0, perPage })
  }


  handleSearch = (searchText) => {
    this.setState({ searchedText: searchText })
    let data = { searchText: searchText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage,
      //  month :this.state.month,year :this.state.year 
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status ? this.state.status :''
      }
      let obj = { from_dateOH: this.state.from, to_dateOH: this.state.to, pageOH: 0
        ,searchOH:this.state.searchedText,statusOH:this.state.status,typeOH:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
    // this.props.getOrderHistory({ data, history: this.props.history })
  };
  handleCloseSearch=()=>{
    let obj = { from_dateOH: this.state.from, to_dateOH: this.state.to, pageOH: 0,searchOH:'',statusOH:this.state.status,typeOH:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
  }
  resetFilter = (filter) => {
    // e.preventDefault();
    filter();
    this.setState({
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
      filter: '',
      to_date:'',
      from_date:'',
      status:''
    });
    let data = {
        page: 1,
        perPage: this.state.perPage,
        filter: this.state.filter,
        from_date:'',
        to_date:'',
        status:'',
        searchText:this.state.searchedText ? this.state.searchedText :''
    }
    let obj = { from_dateOH: '', to_dateOH: '', pageOH: ''
      ,searchOH:this.state.searchedText,statusOH:'',typeOH:'' }
  let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
  this.props.history.replace(`/seller/orders/orderList?${url}`)
    // this.props.getOrderHistory({ data, history: this.props.history })
}
  handleClick = (orderId) => {
    this.props.history.push(`/seller/orders/orderDetails/${orderId}`)
  }
  handleDateChange = (date,key) => {
    this.setState({ [key] :  moment(date).format()})
    // moment(date).format('MM/DD/YYYY') }
  }
  applyFilter = (filter) => {
    filter()
    let data = {
      page: 1,
      perPage: 100,
      filter: '',
      searchText:this.props.searchText ? this.props.searchText :'',
      // month: this.state.month,
      // year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status
    }
    let obj = { from_dateOH: this.state.from, to_dateOH: this.state.to, pageOH: this.state.page
      ,searchOH:this.state.searchedText,statusOH:this.state.status,typeOH:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
      // this.props.getOrderHistory({ data, history: this.props.history })
  }
  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }
  render() {
    let { orderHistoryList } = this.props;
    let { status, selectedDate,type,selectedDateTo,from,to } = this.state;
    const options = {
      searchPlaceholder:'Press Enter to Search',
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      rowsPerPage: this.state.perPage,
      page: this.state.page ,
      fixedHeader: false,
      print: false,
      download: false,
      filter: true,
      sort: false,
      selectableRows: false,
      count: orderHistoryList && orderHistoryList.count,
      serverSide: true,
      rowsPerPage: this.state.perPage,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      search : true,
      searchText: this.state.searchedText,
      textLabels: {
        filter: {
          all: "",
          title: "FILTERS",
        },
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            onkeypress = (e) => {
              if(e.key == 'Enter'){
                this.handleSearch(tableState.searchText)
              }
            }
            break;
          case 'onSearchClose':
              this.handleCloseSearch();
            break;  
        }
      },
    }

    const columns = [
      {
        name: "",
        label: "",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "orderId",
        label: "Order",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "amount",
        label: "Amount",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "date",
        label: "Date",
        options: {
          filter: true,
          filterType: 'custom',
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          },
          customFilterListRender: v => {
            return false;
          },
          filterOptions: {
            names: [],
            logic() {
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => (
              <React.Fragment >
                
                <Row form style={{ maxWidth: 300 }}>
                 
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                
                  <FormControl className="w-100 mb-2 ">
                  <Label for="fromDate">FROM</Label>
                    <DatePicker
                      onChange={(date) => this.handleDateChange(date, 'from')}
                      name='from'
                      id="from"
                      value={from}
                      // autoOk
                      fullWidth
                      leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                      rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                      format="DD/MM/YYYY"
                    />
                  </FormControl>
                </Col>
                  
               
               
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                
                  <FormControl >
                  <Label for="toDate">TO</Label>
                  <DatePicker
                    onChange={(date) => this.handleDateChange(date, 'to')}
                    name='to'
                    id="to"
                    value={to}
                    // autoOk
                    fullWidth
                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                    format="DD/MM/YYYY"
                  />
                  </FormControl>
                </Col>
                </Row> 

                <Row>
                 <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Status</Label>
                      <Input type="select" value={status} onChange={(e) => this.handleChange(e, 'status')} name="status" id="status">
                        <option value="" >Select Status</option>
                        <option value="Cancelled" >Cancelled</option>
                        <option value="Delivered" >Delivered</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Type</Label>
                      <Input type="select" value={type} onChange={(e) => this.handleChange(e, 'type')} name="type" id="type">
                        <option value="" >Select Type</option>
                        <option value="Bulk" >Bulk Prepaid</option>
                        <option value="COD" >COD</option>
                        <option value="Online" >Prepaid</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <div style={{ paddingTop: 15 }} >
                  <Button variant="contained" onClick={() => this.applyFilter(applyFilter)} className='filterButton' color='primary'>Apply Filter</Button>
                  <Button variant="contained" onClick={() => this.resetFilter(applyFilter)} className='filterButton' color='primary'>Reset Filter</Button>
                </div>
              </React.Fragment>
            ),
            onFilterChange: () => {
            }
          },
        }
      },
      {
        name: "action",
        label: "Action",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      }
    ];
    let data = []
    orderHistoryList && orderHistoryList.detail && orderHistoryList.detail.length > 0 && orderHistoryList.detail.map((data1, index) => {
      data.push([
        <span style={{display: 'flex',flexDirection: 'column'}}>
          {data1.orderType?<Tooltip title={`${data1.orderType}`} placement="top-start">
            <span style={helper.styleForDiv} className={`${helper.styleDivLine(data1.orderType)}`}>&nbsp;&nbsp;</span>
          </Tooltip>:<span style={helper.styleForDiv}>&nbsp;&nbsp;</span>}
          {data1.serviceName ? <Tooltip title={`${data1.serviceName}`} placement="top-start">
            <span style={helper.styleForDiv} className={`${helper.styleDivLine2(data1.serviceName)}`}>&nbsp;&nbsp;</span>
          </Tooltip> :
          <span style={helper.styleForDiv}>&nbsp;&nbsp;</span>
          } 
        </span>,
      <div onClick={(e) => this.handleClick(data1.order_id)} style={{cursor:'pointer'}}>
        <h4><span className="font-weight-bolder">{data1.products.length} {data1.products.length > 1 ? 'items' : 'item'} by </span>{data1.user_id.company_name} </h4>
      <p className="small">{data1.order_id}{data1.seller_invoice ? ` | ${data1.seller_invoice}` : ''} | {data1.isBulk ? 'Bulk Prepaid' : data1.paymentType == 'Online' ? 'Prepaid' : data1.paymentType }</p>
      </div>,
      <div onClick={(e) => this.handleClick(data1.order_id)} style={{cursor:'pointer'}}><p className={'m-0'}>&#8377;{(data1.total_amount).toFixed(2)}</p></div>,
      <div onClick={(e) => this.handleClick(data1.order_id)} style={{cursor:'pointer'}} key={'recent'} className={` badge text-uppercase ${this.statusStyle(data1.order_status[data1.order_status.length - 1].status)}`}>{data1.order_status[data1.order_status.length - 1].status}</div>,
      <div onClick={(e) => this.handleClick(data1.order_id)} style={{cursor:'pointer'}}>{moment(data1.order_status[data1.order_status.length - 1].date).format(' D MMM, YYYY h:mm a')}</div>,
      data1.order_status[data1.order_status.length - 1].status === 'Cancelled' ? data1.order_cancel_reason : this.button(data1.uploadedInvoice)
      ])
    })
    return (
      <div style={{ padding: '0px 1px 0 1px', boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)' }}>
        <MuiThemeProvider theme={helper.getMuiTheme()}>
        <MUIDataTable
          // title={<h3>{`Showing data for ${this.state.month} ${this.state.year?this.state.year:'All'}`}</h3>}
          data={data}
          columns={columns}
          options={options}
          style={{ borderRadius: '0px !important' }}
        />
        </MuiThemeProvider>
      </div>
    );
  }
}

const mapStateToProps = ({ order }) => {
  const { orderHistoryList } = order;
  return { orderHistoryList }
};

// export default connect(mapStateToProps, { getOrderHistory })(withStyles(customStyles)(OrderHistory));
OrderHistory= connect(mapStateToProps, { getOrderHistory })(withStyles(customStyles)(OrderHistory));
export default OrderHistory = reduxForm({
  form: 'orderhistoryseller',// a unique identifier for this form
  destroyOnUnmount: false,
  // onSubmitSuccess: (result, dispatch) => {
  //   const newInitialValues = getFormInitialValues('ChangeStatus')();
  //   dispatch(initialize('ChangeStatus', newInitialValues));
  // }
})(withRouter(OrderHistory))
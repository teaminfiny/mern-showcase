import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import IntlMessages from 'util/IntlMessages';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom'
import { Col, Row } from 'reactstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import{FormGroup,Label,Input} from 'reactstrap';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import AppConfig from 'constants/config'
import {  connect } from 'react-redux';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
// import { getCodOnOff } from 'actions/admin';
// import { connect } from 'react-redux';
import axios from 'axios';
import moment from 'moment';
import { DatePicker } from 'material-ui-pickers';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import RenderDatePicker from '../component/datePicker';
import './index.css'

class OrderHeader extends Component {

	constructor(props) {
		super(props)
		this.state = {
      checkedA: false,
      selectedDate: moment().format(),
      openFilter:false,
      seller_id:'',
      from:moment().subtract(1, 'months'),
      to:moment().format()
		}
    }
    

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }
  handleDateChange = (date,key) => {
    this.setState({ [key] :  date })
    // moment(date).format('MM/DD/YYYY')
  }  
  applyFilter = async() => {
    window.open(`${AppConfig.baseUrl}order/export?token=${localStorage.getItem('token')}&from_date=${this.state.from ? moment(this.state.from): moment().format()}&to_date=${this.state.to ? moment(this.state.to) : moment().format()}&status=${this.state.status ? this.state.status :''}&seller=${true} `, '_blank');
        this.setState({ 
          openFilter: false,
          status:'',
          seller_id:''
        })
  } 
  handleRequestClose = () => {
    this.setState({ 
      openFilter: false,
      status:'',
      seller_id:'',
      from_date:'',
      to_date:''
     }); 
  }
  handleDateChange = (date,key) => {
    this.setState({ [key] :  moment(date).format() })
    if([key] == 'from'){
      this.setState({from: moment(date).format()})
    }
    if([key] == 'to'){
      this.setState({to: moment(date).format()})
    }
  }
	render() {

    let { month, year, yearList, status, filterData, seller_id, selectedDate,from ,to } = this.state;
		return (
			<div className="page-heading buyerDetailsHeader d-sm-flex justify-content-sm-between align-items-sm-center">
				<h2 className="title mb-3 mb-sm-0">Orders </h2>	
        {/* <Button variant="outlined" className={'text-primary'} onClick={(e)=>{window.open(`${AppConfig.baseUrl}order/export?token=${localStorage.getItem('admin_token')}`, '_blank');}}> */}
        <Button variant="outlined" className={'text-primary'} onClick={()=>{this.setState({openFilter: true})}}>
              Download
            </Button>

       
        <Dialog open={this.state.openFilter} onClose={this.handleRequestClose}>
        <DialogContent >
                
                <Row form >
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                  <FormControl className="w-100 mb-2 ">
                  <Label for="fromDate">FROM</Label>
                  <DatePicker
                      onChange={(date) => this.handleDateChange(date, 'from')}
                      name='from'
                      id="from"
                      value={from}
                      // autoOk
                      fullWidth
                      leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                      rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                      format="DD/MM/YYYY"
                    />
                  </FormControl>
                </Col>
               
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                  <FormControl >
                  <Label for="toDate">TO</Label>
                  <DatePicker
                    onChange={(date) => this.handleDateChange(date, 'to')}
                    name='to'
                    id="to"
                    value={to}
                    // autoOk
                    fullWidth
                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                    format="DD/MM/YYYY"
                  />
                  </FormControl>
                </Col>
                </Row> 

                <Row>
              {/* <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                <FormGroup className={'mt-1'}>
                  <Label for="exampleSelect">Sellers</Label>
                  <Input type="select" name="seller" id="seller"
                    value={seller_id}
                    onChange={(e) => this.handleChange(e, 'seller_id')}>
                    <option selected={seller_id == ''} value={''}>Select Seller</option>
                    {
                      filterData && filterData.map((val) => {
                        return (<option selected={val._id == seller_id} value={val._id}>{val.company_name}</option>)
                      })
                    }
                  </Input>
                </FormGroup>
              </Col> */}
                 <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Status</Label>
                      <Input type="select" value={status} onChange={(e) => this.handleChange(e, 'status')} name="status" id="status">

                        <option value="" >All</option>
                        <option value="New" >New</option>
                        <option value="Processed" >Processed</option>
                        <option value="Requested" >Requested</option>
                        <option value="Manifested" >Manifested</option>
                        <option value="Cancelled" >Cancelled</option>
                        <option value="Delivered" >Delivered</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <DialogActions>
                  <Button onClick={() => this.handleRequestClose()} color='secondary'>Cancel</Button>
                  <Button onClick={() => this.applyFilter()} color='primary'>Download</Button>
                </DialogActions>
              </DialogContent>
        </Dialog>

			</div>
		)
	}
};


// export default OrderHeader;
export default OrderHeader = reduxForm({
  form: 'OrderHeaderAdmin',// a unique identifier for this form
  destroyOnUnmount: false,
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('OrderHeaderAdmin')();
    dispatch(initialize('OrderHeaderAdmin', newInitialValues));
  }
})(OrderHeader)
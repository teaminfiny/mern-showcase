import React, { Component } from "react";
import MUIDataTable from "../../../../components/DataTable";
import Button from '@material-ui/core/Button';
import customHead from 'constants/customHead'
import {Col, Row, Label, Input,FormGroup } from 'reactstrap';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import moment from 'moment';
import RenderDatePicker from '../component/datePicker';
import { Field } from 'redux-form'
import axios from '../../../../constants/axios';
import AppConfig from 'constants/config'
import { NotificationManager } from 'react-notifications';
import FormControl from '@material-ui/core/FormControl';



let date = new Date();

class OldManifest extends Component {
  constructor(props) {
    super(props);
    let url = props.location.search
    let json = url ? JSON.parse('{"' + decodeURIComponent(url.substring(1).replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}') : ''
    this.state = {
      filter: [],
      page: 0,
      perPage: 100,
      resData:[]
    };

  }
  
  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  componentDidMount = async() => {
    let data = {
      page: this.state.page + 1,
      perPage: this.state.perPage,
    }
    let history =  this.props.history
    if (this.props.tab == 2) {
    await axios.post(`${AppConfig.baseUrl}shippingManifest/getManifest`,data,{
        headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }).then(result => {
      if (result.data.error) {
        NotificationManager.error(result.data.title)
      } else {
        this.setState({ resData: result.data.detail[0] })
        // NotificationManager.success(result.data.title)
      }
    }).catch(error => {
          NotificationManager.error('Something went wrong, Please try again')
        });
    }
  }

  changePage = (page) => {
    let pages = page + 1
    let data = {
      page: pages,
      perPage: this.state.perPage,
      filter: '',
      searchText: this.state.searchedText,
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status ? this.state.status :''
    }
    this.setState({ page })
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: page,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)

    this.setState({ loader: true })
    const t = setTimeout(() => {
      this.setState({ loader: false })
    }, 2000);
  };

  resetFilter = (filter) => {
    // e.preventDefault();
    filter();
    this.setState({
      year: date.getFullYear(),
      filter: '',
      to_date:'',
      from_date:'',
      status:'',
      from:'',to:''
    });
    let data = {
        page: 1,
        perPage: this.state.perPage,
        filter: this.state.filter,
        from_date:'',
        to_date:'',
        status:'',
        searchText:this.state.searchedText ? this.state.searchedText :''
    }
    let obj = { from_dateO: '', to_dateO: '', pageO: '',searchO:this.state.searchedText,statusO:'',typeO:'' }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
}


  handleChangeDocuments = (e)=>{
    this.setState({uploadedInvoice:e})
  }
  applyFilter = (filter) => {
    filter()
    let data = {
      page: 1,
      perPage: 100,
      filter: '',
      searchText:this.props.searchText ? this.props.searchText :'',
      from_date:this.state.from ? this.state.from:'',
      to_date:this.state.to ? this.state.to :'',
      status:this.state.status
    }
    let obj = { from_dateO: this.state.from, to_dateO: this.state.to, pageO: this.state.page,searchO:this.state.searchedText,statusO:this.state.status,typeO:this.state.type }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.replace(`/seller/orders/orderList?${url}`)
  }

  handleOpen = (data) => {
      window.open(`${AppConfig.baseUrl}shippingManifest/getManifestReport?Id=${data._id}`, '_blank');
  }

  handleDelete = async(data) => {
    await axios.get(`${AppConfig.baseUrl}shippingManifest/deleteManifested?Id=${data._id}`,{
      headers: {
      'Content-Type': 'application/json',
      'token': localStorage.getItem('token')
    }
  }).then(result => {
    if (result.data.error) {
      NotificationManager.error(result.data.title)
    } else {
      this.componentDidMount()
      NotificationManager.success(result.data.title)
    }
  }).catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }

  button = (data) => {
    return (
      <ButtonGroup>
        <Button variant="outlined" className={'text-danger'} onClick={(e) => this.handleDelete(data)} >
            Delete
        </Button>
        <Button variant="outlined" className={'text-warning'} onClick={(e) => this.handleOpen(data)} >
            Print
        </Button>
      </ButtonGroup>
    )
  }

  render() {
    let { orderData, } = this.props;
    let { payment, invoice, weight, invoiceNumber, selectedDate, length, breadth, height,status, uploadedInvoice,type, resData } = this.state;

    const options = {
      // searchPlaceholder:'Press Enter to Search',
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      rowsPerPage: this.state.perPage,
      page: this.state.page ,
      fixedHeader: false,
      print: false,
      download: false,
      filter: false,
      sort: false,
      selectableRows: false,
      serverSide: true,
      rowsPerPage: this.state.perPage,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      search : false,
      searchText: this.state.searchedText,
      textLabels: {
        filter: {
          all: "",
          title: "FILTERS",
        },
      },
        onTableChange: (action, tableState) => {
          switch (action) {
            case 'changePage':
              this.changePage(tableState.page);
              break;
            case 'changeRowsPerPage':
              this.changeRowsPerPage(tableState.rowsPerPage)
              break;
            case 'search':
              onkeypress = (e) => {
                if(e.key == 'Enter'){
                  this.handleSearch(tableState.searchText)
                }
              }
              break;
            case 'onSearchClose':
                this.handleCloseSearch();
              break;  
          }
        },
    }

    const columns = [
      {
        name: "date",
        label: "Date",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "orderId",
        label: "Order Id",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "action",
        label: "Actions",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection)
          },
          filterOptions: {
            names: [],
            logic() {
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => (
              <React.Fragment >
                
                <Row form style={{ maxWidth: 300 }}>
                 
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                
                  <FormControl className="w-100 mb-2 ">
                  <Label for="fromDate">FROM</Label>
                    <Field className="form-control" placeholder='MM/DD/YYYY'  autoComplete='off'
                    name="from"  id="from" onChange={(date) => this.handleDateChange(date, 'from')}
                    value={moment(selectedDate).format()} 
                    component={RenderDatePicker}
                    />
                  </FormControl>
                </Col>
                  
               
               
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                
                  <FormControl >
                  <Label for="toDate">TO</Label>
                  <Field className="form-control" placeholder='MM/DD/YYYY' autoComplete='off' 
                  name="to"  id="to" onChange={(date) => this.handleDateChange(date, 'to')}
                  value={moment(selectedDate).format()}
                  component={RenderDatePicker}/>
                  </FormControl>
                </Col>
                </Row> 

                <Row>
                 <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Status</Label>
                      <Input type="select" value={status} onChange={(e) => this.handleChange(e, 'status')} name="status" id="status">

                        <option value="" >Select Status</option>
                        <option value="New" >New</option>
                        <option value="Processed" >Processed</option>
                        <option value="Requested" >Requested</option>
                        <option value="Manifested" >Manifested</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                    <FormGroup className={'mt-1'}>
                      <Label for="exampleSelect">Type</Label>
                      <Input type="select" value={type} onChange={(e) => this.handleChange(e, 'type')} name="type" id="type">
                        <option value="" >Select Type</option>
                        <option value="COD" >COD</option>
                        <option value="Online" >Prepaid</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
                <div style={{ paddingTop: 15 }} >
                  <Button variant="contained" onClick={() => this.applyFilter(applyFilter)} className='filterButton' color='primary'>Apply Filter</Button>
                  <Button variant="contained" onClick={() => this.resetFilter(applyFilter)} className='filterButton' color='primary'>Reset Filter</Button>
                </div>
              </React.Fragment>
            ),
            onFilterChange: () => {
            }
          },
        }
      }
    ];
 
    let data = []
    resData && resData.data && resData.data.map((data1, index) => {
      let orderids = data1.order_id.map(order => {
        return order;
      }).join(',');
      data.push([
      <div>{moment(data1.date).format('DD/MM/YYYY')}</div>,
      <div>{orderids}</div>,
      this.button(data1)
      ])
    })
    let { orderId,  handleSubmit } = this.props;
    const { loader, openPrint } = this.state;
    return (
      <div>
        <div style={{ padding: '0px 1px 0 1px', boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)' }}>
        
          <MUIDataTable
            data={data}
            columns={columns}
            options={options}
            style={{ borderRadius: '0px !important' }}
          />
      </div>
      </div>

    );
  }
}


export default OldManifest;
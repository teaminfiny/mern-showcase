import React, { Component } from "react";
import FullWidthTabs from '../../components/routes/tabs/fixed/FullWidthTabs';
import CircularProgress from '@material-ui/core/CircularProgress';
import OrderHeader from './OrderHeader'
class OrdersTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({loading: false})
    }, 2500)
  }

  render() {
    return (
      <React.Fragment>
        {/* <ContainerHeader match={this.props.match} title={<IntlMessages id="Orders" />} /> */}
        <OrderHeader match={this.props.match}/>
        {
          this.state.loading == false ?
            <FullWidthTabs history={this.props.history} tabFor={'order'} />
            :
            <div className="loader-view" style={{ textAlign: "center", marginTop: "300px" }}>
              <CircularProgress />
            </div>
        }

      </React.Fragment>
    );
  }
}

export default OrdersTab;
import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import Button from '@material-ui/core/Button';
import { FormGroup, Label, Input } from 'reactstrap';
import ReactStrapTextField from 'components/ReactStrapTextField';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { connect } from 'react-redux';
import { addToCart } from 'actions/buyer'
import axios from 'constants/axios';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import AxiosRequest from 'sagas/axiosRequest'
import { getCartDetails } from 'actions/buyer'
class PopOverInCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Inventory: '',
      quantity: 0,
      updatedCount:0
    }
    this.handleIncrement = this.handleIncrement.bind(this)
    this.handleDecrement = this.handleDecrement.bind(this)
    this.handleIncremented = this.handleIncremented.bind(this)
    this.handleDecremented = this.handleDecremented.bind(this)
  }

  // handleChange(evt) {
  //     const quantity = (evt.target.validity.valid) ? evt.target.value : this.state.quantity;
  //     this.setState({ quantity });
  // }
  
  handleIncrement = () => {
    // if (this.state.quantity < this.props.dataFromParent.max_order_quantity) {
    this.setState(prevState => {
      return {
        quantity: Number(prevState.quantity) + 1
      }
    })
    // }
    // else if(this.state.quantity <= this.props.dataFromParent.max_order_quantity) 
    {
      this.setState({ invalid: true })
    }
  }
  handleDecrement = () => {
    if (this.state.quantity > this.props.dataFromParent.min_order_quantity) {
      this.setState(prevState => {
        return {
          quantity: prevState.quantity - 1
        }
      })
    }
    else if (this.state.quantity < this.props.dataFromParent.min_order_quantity) {
      this.setState({ invalid: true })
    }
  }

handleQuantityChange = (key, event) => {
        // if(event.target.value < this.props.productData.max_order_quantity && event.target.value > this.props.productData.min_order_quantity){
         if(event.target.value.length <=4){
        this.setState({ quantity:parseInt(event.target.value) });
        }
        // }
      };

  // handleChange = (key, event) => {
  //     this.setState({ quantity:parseInt(event.target.value) });
  // };
  maxLengthCheck = (event) => {
    if (event.target.value > this.props.dataFromParent.max_order_quantity) {
      event.target.value = this.props.dataFromParent.min_order_quantity
      this.setState({ invalid: true })
    }
  }


handleIncremented = (key, event) => {
 
  if (this.state.updatedCount + this.props.dataFromParent.min_order_quantity > this.props.dataFromParent.max_order_quantity){
      
    // self.view.makeToast("Quantity should be a multiple of minimum quantity and cannot exceed max quantity.", duration: Config.defaultToastDuration, position: .center)
}
else if(this.state.updatedCount <=  Number(this.props.dataFromParent.max_order_quantity)){
      this.setState({
        updatedCount : Number(this.state.updatedCount) + Number(this.props.dataFromParent.min_order_quantity)
      })
}

}

handleDecremented = (key, event) => {
  
  if (this.state.updatedCount != this.props.dataFromParent.min_order_quantity && this.state.updatedCount - this.props.dataFromParent.min_order_quantity >= this.props.dataFromParent.min_order_quantity){
      this.setState({
        
        updatedCount : Number(this.state.updatedCount) - Number(this.props.dataFromParent.min_order_quantity)
      })

  }
    // (this.state.updatedCount >=  Number(this.props.productData[0].min_order_quantity))
      
}
handleQuantityChanged = (key, event) => {
  NotificationManager.warning('Please use buttons for increment and decrement')
  // this.setState({ quantity:parseInt(event.target.value) * 2 ,       
// });
};
  
  onSubmit = async (e) => {
    this.props.closepop()
    // const isLogin = localStorage.getItem("buyer_token");
    // if (isLogin) {
      if (this.props.dataFromParent && this.props.dataFromParent.discount && (this.props.dataFromParent.discount.type === "Same" || this.props.dataFromParent.discount.type === "SameAndDiscount" || this.props.dataFromParent.discount.type === "Different" || this.props.dataFromParent.discount.type === "DifferentAndDiscount")){
      if (this.state.updatedCount >= this.props.dataFromParent.min_order_quantity && this.state.updatedCount <= this.props.dataFromParent.max_order_quantity && Number.isInteger(this.state.updatedCount) === true) {
        let data = {
          cart_details: [{
            inventory_id: this.state.Inventory,
            quantity: this.state.updatedCount
          }]
        }
        let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'cart/addtoCart', '', data)
        if (response.data.error) {
          NotificationManager.error("Please Sign In.")
        } else {
          // NotificationManager.success(response.data.title)
          NotificationManager.success("Quantity changed successfully.")
          const isLogin = localStorage.getItem("buyer_token");

          if (isLogin) {
          this.props.getCartDetails({ history: this.props.history })
          }
        }
      }
    }
      else if (this.state.quantity >= this.props.dataFromParent.min_order_quantity && this.state.quantity <= this.props.dataFromParent.max_order_quantity && Number.isInteger(this.state.quantity) === true) {
        let data = {
          cart_details: [{
            inventory_id: this.state.Inventory,
            quantity: this.state.quantity
          }]
        }
        let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'cart/addtoCart', '', data)
        if (response.data.error) {
          NotificationManager.error("Please Sign In.")
        } else {
          // NotificationManager.success(response.data.title)
          NotificationManager.success("Quantity changed successfully.")
          const isLogin = localStorage.getItem("buyer_token");

          if (isLogin) {
          this.props.getCartDetails({ history: this.props.history })
          }
        }
      }
      else {
        NotificationManager.error("Please check Minimum & Maximum order quantity.")
      }
    }

  //   else if ((this.state.quantity >= this.props.dataFromParent.min_order_quantity && this.state.quantity <= this.props.dataFromParent.max_order_quantity)) {
  //     let obj = JSON.parse(localStorage.getItem("obj") ? localStorage.getItem("obj") : "[]");
  //     let data;
  //     let inventory_id;
  //     var position = obj.findIndex((val) => val.inventory_id === this.props.dataFromParent.inventory_id)
  //     if (position > -1) {
  //       obj.splice(position, 1);
  //       obj.push(
  //         data = {
  //           "inventory_id": this.props.dataFromParent.inventory_id,
  //           "quantity": this.state.quantity,
  //           "title": this.props.dataFromParent.title,
  //           "image": this.props.dataFromParent.image,
  //           "seller_name": this.props.dataFromParent.seller_id,
  //           "price": this.props.dataFromParent.price,
  //           "gstPercentage": this.props.dataFromParent.gstPercentage,
  //           "user_id": this.props.dataFromParent.user_id,
  //           "Discount": this.props.dataFromParent && this.props.dataFromParent.discount,
  //           "discountPercentage": this.props.dataFromParent && this.props.dataFromParent.discountPercentage,
  //           "min_order_quantity": this.props.dataFromParent.min_order_quantity,
  //           "max_order_quantity": this.props.dataFromParent.max_order_quantity,
  //           //  "time":this.props.dataFromParent.updatedAt,
  //           //  "MRP":this.props.dataFromParent.MRP,
  //           "PTR": this.props.dataFromParent.price,
  //         })
  //       // this.setState({obj})
  //       // this.forceUpdate() 
  //     }
  //     else {
  //       obj.push(
  //         data = {
  //           // cart_details: [{
  //           "inventory_id": this.props.dataFromParent.inventory_id,
  //           "quantity": this.state.quantity,
  //           "title": this.props.dataFromParent.title,
  //           "image": this.props.dataFromParent.image,
  //           "seller_name": this.props.dataFromParent.seller_id,
  //           "price": this.props.dataFromParent.price,
  //           "gstPercentage": this.props.dataFromParent.gstPercentage,
  //           "user_id": this.props.dataFromParent.user_id,
  //           "Discount": this.props.dataFromParent && this.props.dataFromParent.discount,
  //           "discountPercentage": this.props.dataFromParent && this.props.dataFromParent.discountPercentage,
  //           "min_order_quantity": this.props.dataFromParent.min_order_quantity,
  //           "max_order_quantity": this.props.dataFromParent.max_order_quantity,
  //           //  "time":this.props.dataFromParent.updatedAt,
  //           //  "MRP":this.props.dataFromParent.MRP,
  //           "PTR": this.props.dataFromParent.price,
  //           // }]
  //         })
  //     }
  //     this.setState({ obj })
  //     localStorage.setItem('obj', JSON.stringify(obj))
  //     NotificationManager.success("Item updated successfully")
  //   }
  //   else {
  //     NotificationManager.error("Please check Minimum & Maximum order quantity.")
  //   }
  // }

  componentDidMount() {
    const { dataFromParent } = this.props;
    const { min_order_quantity, inventory_id, quantity } = this.props.dataFromParent;
    let inventoryData = localStorage.getItem('obj');
    let obj = JSON.parse(inventoryData);
    this.setState({ obj })
    this.setState({
      quantity: quantity,
      updatedCount:quantity,
      Inventory: dataFromParent.inventory_id
    })
  }

  componentDidUpdate(prevProps, prevState){
    if(prevProps.dataFromParent !== this.props.dataFromParent){
      this.setState({
        quantity: this.props.dataFromParent.quantity, 
        updatedCount: this.props.dataFromParent.quantity,
        invalid: false
      })
    }
  }

  render() {
    const { dataFromParent } = this.props;
    let { quantity } = this.state;
    return (
      <div >
        <div className=" media-body " style={{ display: "flex" }} >
          {/* <h1>{this.state.quantity}</h1> */}
          {/* <span className="mr-1 text-danger ml-1" style={{ boxShadow: "2px", border: "none" }} onClick={this.handleDecrement}><i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span> */}
{ this.props.dataFromParent && this.props.dataFromParent.discount && (this.props.dataFromParent.discount.type == "Same" || this.props.dataFromParent.discount.type == "SameAndDiscount" || this.props.dataFromParent.discount.type == "Different" || this.props.dataFromParent.discount.type == "DifferentAndDiscount") ?
<React.Fragment>
          <span className="mt-1 text-grey" 
          // onClick={this.handleDecremented}
          onClick={(e) => this.handleDecremented('minsOrdfdntiy', e)}

          ><i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span>
          <Input
            id="quantity"
            name="quantity"
            type="number"
            // placeholder={this.props.dataFromParent.min_order_quantity}
            // value={this.state.quantity}
            // updatedCount={this.props.dataFromParent.min_order_quantity}
            value={this.state.updatedCount} 
            // max={this.props.dataFromParent.max_order_quantity}
            // min={this.props.dataFromParent.min_order_quantity}
            onChange={(e) => this.handleQuantityChanged('minOrderQuantity', e)}
            // onInput={this.maxLengthCheck.bind(this)}
            // component={ReactStrapTextField}
            style={{ width: "70px", fontSize: "17px", marginLeft: "5px", marginRight: "1px", textAlign: 'center', height: "50%" }}
          // onChange={(e) => this.handleChange('quantity', e)}
          />
          <span className="ml-1 mt-1 text-grey" 
          // onClick={this.handleIncremented}
          onClick={(e) => this.handleIncremented('mOrgdadnstity', e)}
          ><i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span>
          </React.Fragment>
          :
<React.Fragment>
          <span className="mt-1 text-grey" onClick={this.handleDecrement}><i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span>
          <Input
            id="quantity"
            name="quantity"
            type="number"
            value={this.state.quantity}
            
            // max={this.props.dataFromParent.max_order_quantity}
            // min={this.props.dataFromParent.min_order_quantity}
            onChange={(e) => this.handleQuantityChange('minOrderQuantity', e)}
            // onInput={this.maxLengthCheck.bind(this)}
            // component={ReactStrapTextField}
            style={{ width: "70px", fontSize: "17px", marginLeft: "5px", marginRight: "1px", textAlign: 'center', height: "50%" }}
          // onChange={(e) => this.handleChange('quantity', e)}
          />
          <span className="ml-1 mt-1 text-grey" onClick={this.handleIncrement}><i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span>
          </React.Fragment>
          }
          {/* <span className="ml-1 text-success "
                        onClick={this.handleIncrement}><i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span> */}
        </div>
        <div>
          <span style={{ paddingTop: "3px" }}>Min Order: {dataFromParent.min_order_quantity}</span><br />
          <span style={{ paddingBottom: "2px" }}>Max Order: {dataFromParent.max_order_quantity}</span>
        </div>
        <button
          style={{ padding: '7px', backgroundColor: '#072791', color: 'white', borderRadius: '7px', border: 'none', width: "80%" }}
          onClick={this.onSubmit}
          variant="contained" color="primary">Update</button>
      </div>
    );
  }
}
const mapStateToProps = ({ buyer }) => {
  const { addToCart, cartDetails } = buyer
  return { addToCart, cartDetails };
};
export default connect(mapStateToProps, { addToCart, getCartDetails })(PopOverInCart);


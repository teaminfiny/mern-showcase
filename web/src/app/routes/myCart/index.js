import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import IntlMessages from 'util/IntlMessages';
import ContainerHeader from 'components/ContainerHeader';
import DeliveryAddress from './DeliveryAddress';

import { connect } from 'react-redux';

import { getUserDetail } from 'actions'

import { getCartDetails } from 'actions/buyer'

import { getMediWallet } from 'actions/buyer'

import { removeFromCart,emptyCart } from 'actions/buyer'

import { addToCart } from 'actions/buyer'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';

import MyCartModalError from './MyCartModalError'


import Radio from '@material-ui/core/Radio';
import { NotificationManager } from 'react-notifications';
import { NavLink, withRouter } from 'react-router-dom';
import AxiosRequest from 'sagas/axiosRequest'
import CartItems from './CartItems'
import axios from 'axios'
import AppConfig from 'constants/config'
import moment from 'moment';
import './index.css'

const borderEthical = {
  border: '2px solid #ff7000',
  borderRadius: '8px',
  padding: '5px',
  c: '#ff7000'
}
const borderCoolChain = {
  border: '2px solid #0b68a8',
  borderRadius: '8px',
  padding: '5px',
  c: '#0b68a8'
}
const borderGeneric = {
  border: '2px solid  #038d0e',
  borderRadius: '8px',
  padding: '5px',
  c: '#038d0e'
}
const fontS = {
  fontWeight: 'bolder',
  marginTop: '5px'
}
const ccAvenue = (AppConfig.baseUrl).includes('api') ? 'api' : 'dev';
class MyCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSignIn: true,
      openModal: false,
      selectedValue: '',
      errorData:[],
      localData:[],
      isForm:false,
      paytmParams:'',
      checksum:'',
      url:'',

      acceptModal: false,
      acceptDetails: '',
      successData: [],

      minOrderError: false,

      removeInventoryList: [],
      // removeInventoryLists:[],
      open:false,

      globalCod:'',
      isLoading:false,
      medi:0,
      individualPop:false,
      individualSeller:'',
      individualInven:'',
      medicinePop: false,
      mediTypeData:'',
      mediData:'',
      ethicalData: '',
      coolChainData: '',
      genericData: '',
      deliChrg: 0,
      asAdmin: localStorage.getItem('asAdmin'),
      totalCartValue: 0
    };
  }

  handleRadioChange = event => {
    this.setState({ selectedValue: event.target.value });
  };

  componentDidMount = async () => {
    const isUserLoggedIn = localStorage.getItem("buyer_token");
    if (isUserLoggedIn !== null) {
      let data = {
        page: 1,
        perPage: 10,
        filter: '',
        // searchText: '',
        month: this.state.month,
        year: this.state.year
      }
      this.props.getMediWallet({ history: this.props.history, data })
      this.props.getCartDetails({ history: this.props.history })
    }
    else{
      let inventoryData=localStorage.getItem('obj');
    let localData = JSON.parse( inventoryData);
    this.setState({localData})
    }

    await axios({
      method:'post',
      url:`${AppConfig.baseUrl}systemConfig/getAllSystemConfigList`,
      headers: {
        'Content-Type': 'application/json',
        token: localStorage.getItem('buyer_token')
      }
    }
    ).then(result => {
      if (result.data.error) {
      } else {
        result.data.detail.map( (data)=>{
          if(data.key == "globalCod"){
              this.setState({ globalCod: data })
          }
        })           
      }
    }).catch(error => {
    }); 
    await axios({
      method:'get',
      url:`${AppConfig.baseUrl}medicine/listMediCategory`,
      headers: {
        'Content-Type': 'application/json',
        token: localStorage.getItem('buyer_token')
    }}).then(async(result) => {
      if (result.data.error) {
      } else {
        result.data.data.map( async(data)=>{
          if(data.name == 'Ethical branded'){
            await this.setState({ ethicalData: data })
          }
          if (data.name == 'Cool chain'){
            await this.setState({ coolChainData: data })
          }
          if (data.name == 'Generic'){
            await this.setState({ genericData: data })
          }
        })
        await this.setState({ mediData: result.data.data })
      }
    }).catch(error => {
    });
  }

  handleChange = (e, key, index) => {
    let tempCartData = this.state.cartdata;
    if (parseInt(e.target.value) === 0) {
      tempCartData.splice(index, 1)
      this.setState({ cartdata: tempCartData })
    } else {
      tempCartData[index][key] = e.target.value;
      this.setState({ cartdata: tempCartData })
    }
  }


  handleRemoveFromCart = (e, index,event) => {
    const isUserLoggedInn = localStorage.getItem("buyer_token");

  if (isUserLoggedInn !== null) {
    this.props.removeFromCart({ inventory_id: index,history:this.props.history})    
  }

  else{
    // localStorage.removeItem([0].index);
    // this.setState({ value:  event.target.value });   
    
    let inventoryData = localStorage.getItem('obj');
    let localData = JSON.parse(inventoryData);
    // var position = localData.map(function(e) { return e.inventory_id; }).indexOf(index);
    var position = localData.findIndex((val)=>val.inventory_id===index)
        // localStorage.removeItem("position")
        localData.splice(position, 1);
        this.setState({localData})
        // localData.splice(position, 1);
        localStorage.setItem('obj', JSON.stringify(localData));
        // localStorage.removeItem("obj");
        // this.props.getCartDetails();
  }
}

  handleMedicineOrder = async() => {
    await this.setState({isLoading:true})
      let data = {
        type: this.state.selectedValue,
        seller: this.state.mediTypeData ? this.state.mediTypeData.ID :'',
        medicineId: this.state.mediTypeData ? this.state.mediTypeData.medicineId :'' }
      let data2 =  AxiosRequest.axiosBuyerHelperFunc('post','order/createOrder','',data)
      data2.then((result)=>{
        if(result.data.error){
          this.setState({ openModal: true, errorData:result.data.data })
          this.setState({isLoading:false})
        }else{
          if(this.state.selectedValue==='Online'&&result.data.token){
            window.location.assign(`https://${ccAvenue}.medideals.in/payOnline/${result.data.token}`)
          }else{
            NotificationManager.success(result.data.title)
            this.props.getUserDetail({userType:"buyer"})
            let orderIds = result.data.detail.map(data=>{
              return data.order_id
            }).join(',');
          this.setState({acceptModal: true, successData: orderIds,individualSeller:'',isLoading:false,individualPop:false,individualInven:'',medicinePop: false,deliChrg:0})
          }
        }
      })
  }
  handleOnlineOrder = async() => {
      await this.setState({isLoading:true})
      let mid = 'LFLTrO22899669406687'
      let order_id = '464646464'
      let data  = {type:this.state.selectedValue,seller:this.state.individualSeller && this.state.individualSeller.ID };
      let data2 =  AxiosRequest.axiosBuyerHelperFunc('post','order/createOrder','',data)
      data2.then((result)=>{
        if(result.data.error){
          this.setState({ openModal: true, errorData:result.data.data })
          this.setState({isLoading:false})
        }else{
          // ${AppConfig.dev}
          if(this.state.selectedValue==='Online'&&result.data.token){
            window.location.assign(`https://${ccAvenue}.medideals.in/payOnline/${result.data.token}`)
            // this.setState({url:"https://securegw-stage.paytm.in/order/process",paytmParams:result.data.params,isForm:true,checksum:result.data.checksum})
          }else{
            NotificationManager.success(result.data.title)
            // this.props.emptyCart({
            //   inventory: this.state.individualInven ? this.state.individualInven :''
            // })
            this.props.getUserDetail({userType:"buyer"})
            // this.props.getCartDetails({ history: this.props.history })
            let orderIds = result.data.detail.map(data=>{
              return data.order_id
            }).join(',');
          this.setState({acceptModal: true, successData: orderIds,individualSeller:'',isLoading:false,individualPop:false,individualInven:'',deliChrg:0})
          }
          
        }
      })
  }

  handleOnlineModal = () => {
    this.setState({
        open:true
      })
  }
  handleOnlineModalClose = async() => {
    await this.setState({
      open:false,
      individualPop:false,
      deliChrg:0,
      selectedValue:'',
      individualSeller:'',
      medicinePop: false
    })
  }

  handleOpenModal = async() => {
    await this.setState({isLoading:true})
    let mid = 'LFLTrO22899669406687'
    let order_id = '464646464'
      let data  = {type:this.state.selectedValue}
      let data2 =  AxiosRequest.axiosBuyerHelperFunc('post','order/createOrder','',data)
      data2.then((result)=>{
        if(result.data.error){
          this.setState({ openModal: true, errorData:result.data.data })
          this.setState({isLoading:false})
        }else{
         
          if(this.state.selectedValue==='Online'&&result.data.token){
            window.location.assign(`https://${ccAvenue}.medideals.in/payOnline/${result.data.token}`)
            // this.setState({url:"https://securegw-stage.paytm.in/order/process",paytmParams:result.data.params,isForm:true,checksum:result.data.checksum})
          }else{
            NotificationManager.success(result.data.title)
          let orderIds1 = result.data.detail.map(data=>{
            return data.order_id
          }).join(',');
          this.setState({acceptModal: true, successData: orderIds1,individualSeller:'',isLoading:false,individualPop:false,individualInven:'',deliChrg:0})
          }
        }
      })
    
  }

  closeAllDialog = () => {
    this.setState({ openModal: !this.state.openModal,individualPop:false,deliChrg:0 })
  }
  handleClose = () => {
    this.setState({ openModal: !this.state.openModal,open:false, individualPop:false,individualSeller:'',medicinePop: false,deliChrg:0 })
  }

  yesShowCart = async() => {
    const {errorData} = this.state;

    errorData.map((dataOne) => {
      if(dataOne.type === "update"){
        let data = {
          cart_details: [{
              inventory_id:  dataOne.inventory_id,
              quantity: dataOne.quantity
          }]
      }
        this.props.addToCart({ data })
      }
      // else if(dataOne.type === "remove"){
      //   this.props.removeFromCart({ inventory_id: dataOne.inventory_id})  
      // }
    })
    let removeArr = [];
    let data1 = errorData.map(e => e.type === 'remove' ? removeArr.push(e.inventory_id) : '');
    if (removeArr.length > 0) {
      await axios({
        method: 'post', 
        url: `${AppConfig.baseUrl}cart/removeAll`,
        headers: {
          'Content-Type': 'application/json',
          token: localStorage.getItem('buyer_token')
        },
        data: { inventory: removeArr }
      }).then(result => {
        if (result.data.error) {
        } else {
          this.props.getCartDetails({ history: this.props.history })
        }
      }).catch(error => {
      });
    }
    this.handleClose()
  }

  handleNavClick = (e, link) => {
    this.props.history.push(link)
  }

  handleAcceptModal = (details) => {
    const {mainOrderData, value} = this.props;
    let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
    if(index>-1){
      this.setState({
        acceptModal:true,
        acceptDetails: mainOrderData[index]})
    }
  }

  handleAcceptClose = () => {
    this.setState({acceptModal:false, acceptDetails:''})
    this.props.history.push({pathname: '/', state: '/MyCart'})      
  }

  handleMinOrderError = () => {
    this.setState({ minOrderError: true })
  }

  handleMinOrderErrorClose = () => {
    this.setState({ minOrderError: false,minOrderErrorIndividual:false, isLoading:false })
  }
  handleMinOrderErrorIndividual = (data) => {
    this.setState({ minOrderErrorIndividual: true,individualSeller:data })
  }
  minOrderErrorProceed = async(removeInventoryLists) => { //place order without removing products
    this.handleOpenModal()
    this.setState({ minOrderError: false })
  }
  minOrderErrorProceed1 = async(removeInventoryLists) => { //remove items < 2000 of seller for overall checkout
    const { removeInventoryList } = this.state;
    const newArray = [...new Set(removeInventoryLists)]
    await axios({
      method:'post',
      url:`${AppConfig.baseUrl}cart/removeAll`,
      headers: {
        'Content-Type': 'application/json',
        token: localStorage.getItem('buyer_token')
      },
      data:{inventory: newArray}
    }
    ).then(result => {
      if (result.data.error) {
      } else {
        this.props.getCartDetails({ history: this.props.history })
        this.handleOpenModal()            
      }
    }).catch(error => {
      });
    this.setState({ minOrderError: false })
  }

  redirectSeller = (data) =>{
    this.props.history.push(`view-seller/${data.user_id}`)
  }
  individualCheckout = (data) => {
    let inventoryFor=[]
    let { cartDetails, users } = this.props;
    let totalForDeli = 0;
    if( data.coolChain > 0 && data.coolChain > this.state.coolChainData.minOrder ) {
      totalForDeli = totalForDeli + 1;
    }
    if( data.ethical > 0 && data.ethical > this.state.ethicalData.minOrder ) {
      totalForDeli = totalForDeli + 1;
    }
    if( data.generic > 0 && data.generic > this.state.genericData.minOrder ) {
      totalForDeli = totalForDeli + 1;
    }
    let delichrg = users && users.isDeliveryCharge === true ? totalForDeli == 3 ? 150 : totalForDeli == 2 ? 100 : totalForDeli == 1 ? 50 : 0 : 0;
    cartDetails && cartDetails.detail && cartDetails.detail.length > 0 && cartDetails.detail.map((dataOne, index) => {
        if(data.ID === dataOne.Seller._id){
          inventoryFor.push([index]=dataOne.Inventory._id)
        }
      })
    this.setState({deliChrg:delichrg,individualPop:true,individualSeller:data,individualInven:inventoryFor})
  }
  medicineFunction = async(data, key, value) => {
    data.key = (key == 'ethical') ? 'Ethical branded' : (key == 'cool') ? 'Cool chain' : 
    (key == 'generic') ? 'Generic' : 'Others';
    let prodCod = value && value.filter(e => e.isPrepaid === true);
    let invenCod = value && value.filter(e => e.prepaidInven === true);
    let med = this.state.mediData.filter(e=> e.name == data.key);
    data.medicineId = med[0]._id;
    data.showCod = med[0].onlyPrepaid;
    data.showCodForProd = prodCod.length > 0 ? true : false;
    data.showCodForInven = invenCod.length > 0 ? true : false;
    await this.setState({ mediTypeData: data, medicinePop: true })
  }
  activeButton = (data) => {
    console.log('activeButton',data)
    let cool = true;
    let ethic = true;
    let gene = true;
    if( data.coolChain > 0 && data.coolChain < this.state.coolChainData.minOrder ) {
      cool = false;
    }
    if( data.ethical > 0 && data.ethical < this.state.ethicalData.minOrder ) {
      ethic = false;
    }
    if( data.generic > 0 && data.generic < this.state.genericData.minOrder ) {
      gene = false;
    }
    if( cool && ethic && gene ){
      return true
    }else{
      return false
    }
  }
  short = (type, data) => {
    if(type == 'coolChain' && data.coolChain > 0 && data.coolChain < this.state.coolChainData.minOrder){
      return true
    }
    if(type == 'ethicalBranded' && data.ethical > 0 && data.ethical < this.state.ethicalData.minOrder){
      return true
    }
    if(type == 'generic' && data.generic > 0 && data.generic < this.state.genericData.minOrder){
      return true
    }
    return false;
  }
  // totalValue = (ptr, quantity) => {
  //   console.log('awcoimevoiamv-',ptr, quantity)
  //   let sum = (ptr * quantity).toFixed(2)
  //   // a += Number(sum);
  //   this.setState({ totalCartValue: this.state.totalCartValue + Number(sum) })
  //   console.log('awcoimevoiamv--',this.state.totalCartValue)
  // }
  render() {
    const { acceptModal, successData, removeInventoryList,individualSeller, mediTypeData, mediData,
    ethicalData, coolChainData, genericData, showMediCod, deliChrg, asAdmin, totalCartValue } = this.state;
    const isUserLoggedIn = localStorage.getItem("buyer_token");
    const threeMonth = (new Date(moment().add(3,"M"))).getTime();

    const {isForm,paytmParams,checksum,url} = this.state
    let { cartdata,errorData } = this.state;
    // let inventoryFor =[]
    let TotalPrice = 0
    let gst = 0
    let singleItemTotalGST = 0
    let AllItemsTotalPrice = 0
    let AllItemsTotalGST = 0
    let GST = 0
    let one = [];
    let i = 1;
    let j = 1;
    let discountMinus = 0
    let priceAfterDiscount = 0
    let newGST = 0
    let totalDiscountMinus = 0
    let totalAmount = []
    let finalGST = 0
    let data = []
    let sellerDetails = [];
    // let removeInventoryList = [];
    let total = 0;	
    let singleItemTotalPrice;
	  let totalMinusWallet=0
    const { users } = this.props;
    let rtoCharge = users && users.charges && users.charges.amt ? users && users.charges && users.charges.amt : 0;
    let individualDeliveryChrg = users && users.isDeliveryCharge === true ? 50 : 0;

    const { cartDetails } = this.props;

    let inventoryData=localStorage.getItem('obj');
    let localData = JSON.parse( inventoryData);
    const isLogined = localStorage.getItem("buyer_token");
    let removeInventoryLists=[]
    let isCoolChain;
    let isPrepaid = [];
    let prepaidInvenAll;


if(isLogined){
  cartDetails && cartDetails.detail &&  cartDetails.detail.length > 0 && cartDetails.detail.map((dataOne, index) => {
      data.push({
        image: dataOne.Inventory.product_id.images[0],
        title: dataOne.Inventory.product_id.name,
        time: dataOne.Inventory.updatedAt,
        seller_id: dataOne.Seller.company_name,
        sellerID: dataOne.Seller._id,
        sellerStatus: dataOne.Seller.user_status,
        sellerVac: dataOne.Seller.vaccation.vaccationMode,
        price: dataOne.Inventory.PTR,
        gstPercentage: dataOne.Inventory.product_id.gst.value,
        quantity: dataOne.cart_details.quantity,
        stock: 0,
        inventory_id: dataOne.Inventory._id,
        discount: dataOne.Discount ? dataOne.Discount : {},
        discountPercentage: dataOne.Discount.discount_per,
        min_order_quantity: dataOne.Inventory.min_order_quantity,
        max_order_quantity: dataOne.Inventory.max_order_quantity,
        user_id: dataOne.Inventory.user_id,
        inventory: dataOne.Inventory ? dataOne.Inventory : {},
        discountProduct: dataOne.DiscountProduct ? dataOne.DiscountProduct : {},
        isPrepaid: dataOne.Inventory.product_id.isPrepaid ? dataOne.Inventory.product_id.isPrepaid : false ,
        prepaidInven: dataOne.Inventory.prepaidInven ? dataOne.Inventory.prepaidInven : false 
      })
    })


    let totalItemsInCart = []

    cartDetails && cartDetails.detail && cartDetails.detail.length > 0 && cartDetails.detail.map((dataOne, index) => {
      totalItemsInCart.push({
        user_id: dataOne.Inventory.user_id, 
        price: dataOne.Discount && dataOne.Discount.type === "Discount" && dataOne.Discount.discount_per ? 

        (dataOne.Inventory.ePTR) * dataOne.cart_details.quantity 
         : 
         dataOne.Discount && (dataOne.Discount.type === "Same" || dataOne.Discount.type === "SameAndDiscount") ? 
         ((dataOne.cart_details.quantity)/(dataOne.Discount.discount_on_product.purchase) *(dataOne.Discount.discount_on_product.bonus) + dataOne.cart_details.quantity) * dataOne.Inventory.ePTR
         :
        dataOne.Inventory.ePTR * dataOne.cart_details.quantity,
        seller_name: dataOne.Seller.company_name,
        inventory_id: dataOne.Inventory._id,
        discount_type: dataOne.Discount.type,
        discount_per: dataOne.Discount && dataOne.Discount.discount_per ? dataOne.Discount.discount_per : "noPercentage" ,
        gstValue: dataOne.Discount && (dataOne.Discount.type === "Same" || dataOne.Discount.type === "SameAndDiscount") ?
        (dataOne.Inventory.ePTR * ((dataOne.cart_details.quantity)/(dataOne.Discount.discount_on_product.purchase) *(dataOne.Discount.discount_on_product.bonus) + dataOne.cart_details.quantity))/100 * dataOne.Inventory.product_id.gst.value
        :
        (dataOne.Inventory.ePTR * dataOne.cart_details.quantity)/100 * dataOne.Inventory.product_id.gst.value,
        mediType: dataOne.Inventory.medicineTypeName,
        isPrepaid: dataOne.Inventory.product_id.isPrepaid ? dataOne.Inventory.product_id.isPrepaid : false,
        prepaidInven: dataOne.Inventory.prepaidInven ? dataOne.Inventory.prepaidInven : false
      })
    })

    isCoolChain = totalItemsInCart.filter( e => e.mediType === 'Cool chain' );
    prepaidInvenAll = totalItemsInCart.filter(e => ( e.prepaidInven === true ));
    var holder = {};
    totalItemsInCart.forEach(function(d) {
      if (holder.hasOwnProperty(d.seller_name)) {
        holder[d.seller_name] = holder[d.seller_name] + d.price;
      } else {
        holder[d.seller_name] = d.price;
      }
    });
    isPrepaid = totalItemsInCart.filter( e => e.isPrepaid === true );

    var deliveryCal = [];

    for (var prop in holder) {
      deliveryCal.push({ seller_name: prop, price: holder[prop] });
    }

  
    
//------------------------------- sellerDetails  -------------------------------------------------------
    
      totalItemsInCart.map(each => {
        let filteredArray = totalItemsInCart.filter(eC => eC.user_id === each.user_id)
        let sum = filteredArray.reduce((acc,current)=> acc+current.price,0)
        let gst2 = filteredArray.reduce((acc,current)=> acc+current.gstValue,0)

        let genericC = totalItemsInCart.filter(eC => (eC.user_id === each.user_id &&
        (eC.mediType == 'Generic' || eC.mediType == 'OTC' || eC.mediType == 'Surgical')));
        let coolChainC = totalItemsInCart.filter(eC => (eC.user_id === each.user_id && eC.mediType == 'Cool chain' ));
        let ethicalC = totalItemsInCart.filter(eC => (eC.user_id === each.user_id && (eC.mediType == 'Ethical branded' || eC.mediType == 'Others') ));

        let genericP = genericC.reduce((accumulator, currentValue) => accumulator + currentValue.price,0);
        let coolChainP = coolChainC.reduce((accumulator, currentValue) => accumulator + currentValue.price,0);
        let ethicalP = ethicalC.reduce((accumulator, currentValue) => accumulator + currentValue.price,0);

        let genericGst = genericC.reduce((accumulator, currentValue) => accumulator + currentValue.gstValue,0);
        let coolChainGst = coolChainC.reduce((accumulator, currentValue) => accumulator + currentValue.gstValue,0);
        let ethicalGst = ethicalC.reduce((accumulator, currentValue) => accumulator + currentValue.gstValue,0);
        // filteredArray.map(e => {
        //   sum += e.price
        // })
        // filteredArray.map(e => {
        //   gst2 += e.gstValue
        // })
        let prepaid = totalItemsInCart.filter(e => (e.user_id === each.user_id && e.isPrepaid == true ));
        let prepaidInven = totalItemsInCart.filter(e => (e.user_id === each.user_id && e.prepaidInven == true ));
        let obj = {}
        obj.user_id = each.user_id;
        obj.seller_name = each.seller_name;
        obj.price = sum;
        obj.gst1 = gst2;
        obj.generic = genericP;
        obj.coolChain = coolChainP;
        obj.ethical = ethicalP;
        obj.genericGst = genericGst;
        obj.coolChainGst = coolChainGst;
        obj.ethicalGst = ethicalGst;
        obj.prepaid = prepaid.length;
        obj.prepaidInven = prepaidInven.length
        // obj.discount_type = each.discount_type;
        // obj.discount_per = each.discount_per;
  
        if(obj.price < 2000){
            removeInventoryLists.push(each.inventory_id)
          }
        let lessthan2000=[]
        if (sellerDetails.length === 0) {
          sellerDetails.push(obj)
          
        }
        else {
          let index = sellerDetails.findIndex(e => e.user_id === each.user_id)
          if (index === -1) {
            sellerDetails.push(obj)
          }
        }
      })
      


    // totalItemsInCart.map(each => {
    //   inventory_list.push(each.inventory_id)
    // })


  }

  // const inventoryID = cartDetails && cartDetails.data && cartDetails.data.Inventory && cartDetails.data.Inventory._id;
  // let data = []
  // let inventoryData=localStorage.getItem('obj');
  //     let localData = JSON.parse( inventoryData);
  //     let isloggedin = localStorage.getItem('buyer_token')
  // let varablebleTomap = isloggedin?cartDetails.detail:localData
  // varablebleTomap &&  varablebleTomap.length > 0 && varablebleTomap.map((dataOne, index) => {
  //   data.push({
  //     image: dataOne.Inventory.product_id.images[0],
  //     title: dataOne.Inventory.product_id.name,
  //     time: dataOne.Inventory.updatedAt,
  //     seller_id: "Avanish Mishra",
  //     price: dataOne.Inventory.PTR,
  //     gstPercentage: dataOne.Inventory.product_id.gst.value,
  //     quantity: dataOne.cart_details.quantity,
  //     stock: 0,
  //     inventory_id: dataOne.Inventory._id,
  //     discount: dataOne.Discount,
  //     discountPercentage: dataOne.Discount.discount_per,
  //     min_order_quantity: dataOne.Inventory.min_order_quantity,
  //     max_order_quantity: dataOne.Inventory.max_order_quantity,
  //     user_id: dataOne.Inventory.user_id, 
  //   })
  // })
  // else{
  //   localData &&  localData.length > 0 && localData.map((dataOne, index) => {
  //      data.push({
  //        image: dataOne.image,
  //        title: dataOne.title,
  //        time: dataOne.updatedAt,
  //        // seller_id: dataOne.Seller.first_name + " " + dataOne.Seller.last_name,
  //        price: dataOne.price,
  //        gstPercentage: dataOne.gstPercentage,
  //        quantity: dataOne.quantity,
  //        stock: 0,
  //        inventory_id: dataOne.inventory_id,
  //        discount: dataOne.Discount,
  //        discountPercentage: dataOne.Discount &&  dataOne.Discount.discount_per,
  //        min_order_quantity: dataOne.min_order_quantity,
  //        max_order_quantity: dataOne.max_order_quantity,
  //        user_id: dataOne.user_id, 
  //      })
  //    })
 
  //    let totalItemsInCart = []
 
  //    localData && localData && localData.length > 0 && localData.map((dataOne, index) => {
  //      totalItemsInCart.push({
  //        user_id: dataOne.user_id, 
  //        price: dataOne.price * dataOne.quantity,
  //      })
  //    })
 
  //    var holder = {};
 
  //    totalItemsInCart.forEach(function(d) {
  //      if (holder.hasOwnProperty(d.user_id)) {
  //        holder[d.user_id] = holder[d.user_id] + d.price;
  //      } else {
  //        holder[d.user_id] = d.price;
  //      }
  //    });
 
  //    var deliveryCal = [];
 
  //    for (var prop in holder) {
  //      deliveryCal.push({ user_id: prop, price: holder[prop] });
  //    }
     
  //  }
 
 
 //---------------------------------- Delivery Total --------------------------------------------------------------

     let deliveryTotal = []
     let totalDeliChrgInCart = 0
     {sellerDetails.map((data, index) => { //seller must cross 2k 
              if( users.isDeliveryCharge === true ){
                deliveryTotal.push(50)
              }
              if(data.ethical && data.ethical > 0 && users.isDeliveryCharge === true){
                totalDeliChrgInCart = totalDeliChrgInCart + 50
              }
              if(data.coolChain && data.coolChain > 0 && users.isDeliveryCharge === true){
                totalDeliChrgInCart = totalDeliChrgInCart + 50
              }
              if(data.generic && data.generic > 0 && users.isDeliveryCharge === true){
                totalDeliChrgInCart = totalDeliChrgInCart + 50
              }
      })
     }
     let totalDeliveryCharge =  deliveryTotal.reduce((a, b) => a + b, 0)
   



    let minOrderErrorArray = []
    let noMinOrderErrorArray = []

    sellerDetails.map(data => {
      if(data.price < 2000){
        minOrderErrorArray.push({price:data.price,Id:data.user_id})
      }
      else if(data.price > 2000){
        noMinOrderErrorArray.push(data.price)
      }
    })

   
    let sellerId=[];
    // let inventoryFor=[]
    sellerDetails.map((data2, index) => { 
      sellerId.push({
        ID:data2.user_id,
        name:data2.seller_name,
        price:data2.price,
        gst:data2.gst1,
        ethical: data2.ethical,
        coolChain: data2.coolChain,
        generic: data2.generic,
        ethicalGst: data2.ethicalGst,
        coolChainGst: data2.coolChainGst,
        genericGst: data2.genericGst,
        prepaid: data2.prepaid,
        prepaidInven: data2.prepaidInven
      })
    })
    
    return (
      <div className="app-wrapper">
        <div className="row">

          <div className="col-xl-12 col-lg-12">
            <ContainerHeader match={this.props.match} title={<IntlMessages id="Shopping Cart" />} />
          </div>

          <div className=" col-xl-9 col-lg-9 col-md-8 col-sm-8">


            {users ?

              <DeliveryAddress
                userDataFromParent={users} />

              :

              <br />}
             { sellerId.map((eachId,index) => {
               let errorOnCheckout ;
               one=data.filter((val) => val.sellerID == eachId.ID)
              // console.log('seifmsoiefgm',one,index,eachId)
              // one.map(data1=> this.totalValue(data1.inventory.PTR, data1.quantity));
              let ethical = one.filter(e=> (e.inventory.medicineTypeName == 'Ethical branded' || 
                e.inventory.medicineTypeName == 'Others')); 
              let coolChain = one.filter(e=> e.inventory.medicineTypeName == 'Cool chain');
              let generic = one.filter(e=> ( 
                e.inventory.medicineTypeName == 'Generic' ||  e.inventory.medicineTypeName == 'OTC' || e.inventory.medicineTypeName == 'Surgical' ) ); 
                
             return(
            <div className="jr-card">
              <div className="animated slideInUpTiny animation-duration-3">
                <div className="row">
                  <div className=" col-xl-9 col-lg-9 col-md-9 col-sm-9">
                  <NavLink className="buyerRedirectNavlink" to={`/view-seller/${one[0].sellerID}`}>
                    <h3>{one[0].seller_id} ({one.length} {one.length === 1 ? "item" : "items"})<br/>
                    <span className={this.activeButton(eachId) ? 'more2000' : 'less2000'}>₹{(eachId.price).toFixed(2)}</span>&nbsp;<span style={{fontSize:'smaller'}} >*excluding GST</span></h3>
                  </NavLink>
                  </div>
                  {/* { 
                  errorOnCheckout = minOrderErrorArray.findIndex((val) => val.Id == one[0].sellerID)
                  } */}
                  {/* errorOnCheckout == -1 */}
                  {  this.activeButton(eachId) ?    
                  <div className=" col-xl-3 col-lg-3 col-md-3 col-sm-3" style={{ textAlign: "end" }}>
                    <button style={{ padding: '10px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} key={index}
                    onClick={(e) => this.individualCheckout(eachId)}>Seller CheckOut
                    </button>
                  </div> :
                  
                  <div className=" col-xl-3 col-lg-3 col-md-3 col-sm-3" style={{ textAlign: "end" }}>
                  <button style={{ padding: '10px', backgroundColor: '#dfdfdf', color: 'black', borderRadius: '5px', border: 'none', cursor: 'default' }} key={index}>Seller CheckOut
                  </button>
                  </div>
                  }

                
                </div>
                <hr />
                {coolChain.length > 0 &&
                <React.Fragment> 
                <div style={borderCoolChain} >
                  <div className="row">
                    <div className=" col-xl-8 col-lg-8 col-md-8 col-sm-8">
                      <h3 className='ml-3' style={fontS}>Cool Chain &nbsp;
                      {this.short('coolChain',eachId) && 
                      <span className='less2000' style={{fontWeight: 'initial'}}>(Short of ₹{((coolChainData && coolChainData.minOrder) - eachId.coolChain).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits:2})})</span> }
                      </h3>
                    </div>
                    { eachId.coolChain >=  (coolChainData && coolChainData.minOrder) ?
                      <div className=" col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: "end" }}>
                      <button className='mr-3' style={{ padding: '10px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} key={index}
                      onClick={(e) => this.medicineFunction(eachId,'cool',coolChain)}>
                        CheckOut
                      </button>
                    </div> :
                    <div className=" col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: "end" }}>
                    <button className='mr-3' style={{ padding: '10px', backgroundColor: '#dfdfdf', color: 'black', borderRadius: '5px', border: 'none', cursor: 'default' }} key={index}>CheckOut
                    </button>
                    </div>
                    }
                  </div>
                 { coolChain && coolChain.map((data, index) => {
                  let expDateC = new Date(moment(data.inventory.expiry_date)).getTime();
                 if( data.inventory.isDeleted == false && data.inventory.isHidden == false && (expDateC > threeMonth) && data.sellerStatus === 'active' && data.sellerVac === false){
                singleItemTotalPrice = (data.discount.name == 'Same' || data.discount.name == 'SameAndDiscount') ? ((data.quantity/data.discount.discount_on_product.purchase * (data.discount.discount_on_product.bonus) + data.quantity) * data.inventory.ePTR) : data.inventory.ePTR * data.quantity  //data.price * data.quantity
                  GST = data.inventory.ePTR/100 * Number(data.gstPercentage) //data.price / 100 * Number(data.gstPercentage)

                  singleItemTotalGST = (data.discount.name == 'Same' || data.discount.name == 'SameAndDiscount') ? ((data.quantity/data.discount.discount_on_product.purchase * (data.discount.discount_on_product.bonus) + data.quantity)) * GST :   GST *  data.quantity

                  AllItemsTotalPrice = AllItemsTotalPrice + singleItemTotalPrice
                  AllItemsTotalGST = AllItemsTotalGST + singleItemTotalGST
                  
                  if(data.discount&&data.discount.type==='Discount'){
                    discountMinus = singleItemTotalPrice/100 * data.discount.discount_per
                    priceAfterDiscount = singleItemTotalPrice - discountMinus
                    newGST = priceAfterDiscount/100 * Number(data.gstPercentage) 
                    finalGST = finalGST + newGST
                    totalDiscountMinus = totalDiscountMinus + discountMinus

                  }
                  if(data.discount&&(data.discount.type==='SameAndDiscount'||data.discount.type==='DifferentAndDiscount')){
                    discountMinus = singleItemTotalPrice/100 * data.discount.discount_per
                    priceAfterDiscount = singleItemTotalPrice - discountMinus
                    newGST = priceAfterDiscount/100 * data.gstPercentage 
                    finalGST = finalGST + newGST
                    totalDiscountMinus = totalDiscountMinus + discountMinus

                  }
                  if(data.discount&&data.discount.type !=='Discount'){
                    finalGST = finalGST + singleItemTotalGST
                  }
                  // total = ((AllItemsTotalPrice - totalDiscountMinus) + finalGST + totalDeliveryCharge).toFixed(2);
                  total = AllItemsTotalPrice + AllItemsTotalGST
                   return (
                    
                    <CartItems
                    handleRem={this.handleRemoveFromCart}
                      data={data}
                      index={index}
                      key={index}
                      singleItemTotalPrice={singleItemTotalPrice}
                      GST={GST}
                      singleItemTotalGST={singleItemTotalGST}
                      AllItemsTotalPrice={AllItemsTotalPrice}
                      AllItemsTotalGST={AllItemsTotalGST}
                      priceAfterDiscount={priceAfterDiscount}
                      newGST={newGST}
                      discountMinus={discountMinus}
                      color={borderCoolChain.c}
                    />
                   ) 
                 }
                }
                 )}
                 </div><hr/>
                 </React.Fragment>
                 }
                {ethical.length > 0 &&  
                <React.Fragment>
                <div style={borderEthical} >                  
                  <div className="row">
                    <div className=" col-xl-8 col-lg-8 col-md-8 col-sm-8">
                      <h3 className='ml-3'  style={fontS}>{'Ethical Branded & PCD'}&nbsp;
                      {this.short('ethicalBranded',eachId) && 
                      <span className='less2000' style={{fontWeight: 'initial'}}>(Short of ₹{((ethicalData && ethicalData.minOrder) - eachId.ethical).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits:2})})</span> }
                      </h3> 
                    </div>
                    { eachId.ethical >= (ethicalData && ethicalData.minOrder) ?
                      <div className=" col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: "end" }}>
                      <button className='mr-3' style={{ padding: '10px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} key={index}
                      onClick={(e) => this.medicineFunction(eachId,'ethical',ethical)}>CheckOut
                      </button>
                    </div> :
                    <div className=" col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: "end" }}>
                    <button className='mr-3' style={{ padding: '10px', backgroundColor: '#dfdfdf', color: 'black', borderRadius: '5px', border: 'none', cursor: 'default' }} key={index}>CheckOut
                    </button>
                    </div>
                    }
                  </div>
                 { ethical && ethical.map((data, index) => { 
                   let expDateE = new Date(moment(data.inventory.expiry_date)).getTime();
                 if( data.inventory.isDeleted == false && data.inventory.isHidden == false && (expDateE > threeMonth) && data.sellerStatus === 'active' && data.sellerVac === false){
                singleItemTotalPrice = (data.discount.name == 'Same' || data.discount.name == 'SameAndDiscount') ? ((data.quantity/data.discount.discount_on_product.purchase * (data.discount.discount_on_product.bonus) + data.quantity) * data.inventory.ePTR) : data.inventory.ePTR * data.quantity  //data.price * data.quantity
                  GST = data.inventory.ePTR/100 * Number(data.gstPercentage) //data.price / 100 * Number(data.gstPercentage)

                  singleItemTotalGST = (data.discount.name == 'Same' || data.discount.name == 'SameAndDiscount') ? ((data.quantity/data.discount.discount_on_product.purchase * (data.discount.discount_on_product.bonus) + data.quantity)) * GST :   GST *  data.quantity

                  AllItemsTotalPrice = AllItemsTotalPrice + singleItemTotalPrice
                  AllItemsTotalGST = AllItemsTotalGST + singleItemTotalGST
                  
                  if(data.discount&&data.discount.type==='Discount'){
                    discountMinus = singleItemTotalPrice/100 * data.discount.discount_per
                    priceAfterDiscount = singleItemTotalPrice - discountMinus
                    newGST = priceAfterDiscount/100 * Number(data.gstPercentage) 
                    finalGST = finalGST + newGST
                    totalDiscountMinus = totalDiscountMinus + discountMinus

                  }
                  if(data.discount&&(data.discount.type==='SameAndDiscount'||data.discount.type==='DifferentAndDiscount')){
                    discountMinus = singleItemTotalPrice/100 * data.discount.discount_per
                    priceAfterDiscount = singleItemTotalPrice - discountMinus
                    newGST = priceAfterDiscount/100 * data.gstPercentage 
                    finalGST = finalGST + newGST
                    totalDiscountMinus = totalDiscountMinus + discountMinus

                  }
                  if(data.discount&&data.discount.type !=='Discount'){
                    finalGST = finalGST + singleItemTotalGST
                  }
                  // total = ((AllItemsTotalPrice - totalDiscountMinus) + finalGST + totalDeliveryCharge).toFixed(2);
                  total = AllItemsTotalPrice + AllItemsTotalGST
                   return (
                    
                    <CartItems
                    handleRem={this.handleRemoveFromCart}
                      data={data}
                      index={index}
                      key={index}
                      singleItemTotalPrice={singleItemTotalPrice}
                      GST={GST}
                      singleItemTotalGST={singleItemTotalGST}
                      AllItemsTotalPrice={AllItemsTotalPrice}
                      AllItemsTotalGST={AllItemsTotalGST}
                      priceAfterDiscount={priceAfterDiscount}
                      newGST={newGST}
                      discountMinus={discountMinus}
                      color={borderEthical.c}
                    />
                   ) 
                 }
                }
                 )}
                 </div><hr/>
                 </React.Fragment>
                 }
                 
                {generic.length > 0 && 
                <React.Fragment>
                <div style={borderGeneric} >
                  <div className="row">
                    <div className=" col-xl-8 col-lg-8 col-md-8 col-sm-8">
                      <h3 className='ml-3'  style={fontS}>{'Generic & Surgical'}&nbsp;
                      {this.short('generic',eachId) && 
                      <span className='less2000' style={{fontWeight: 'initial'}}>(Short of ₹{((genericData && genericData.minOrder) - eachId.generic).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits:2})})</span> }
                      </h3> 
                    </div>
                    { eachId.generic >= (genericData && genericData.minOrder) ?
                      <div className=" col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: "end" }}>
                      <button className='mr-3' style={{ padding: '10px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} key={index}
                      onClick={(e) => this.medicineFunction(eachId,'generic',generic)}>CheckOut
                      </button>
                    </div> :
                    <div className=" col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: "end" }}>
                    <button className='mr-3' style={{ padding: '10px', backgroundColor: '#dfdfdf', color: 'black', borderRadius: '5px', border: 'none', cursor: 'default' }} key={index}>CheckOut
                    </button>
                    </div>
                    }
                  </div>
               { generic && generic.map((data, index) => { 
                 let expDateG = new Date(moment(data.inventory.expiry_date)).getTime();
                 if( data.inventory.isDeleted == false && data.inventory.isHidden == false && (expDateG > threeMonth) && data.sellerStatus === 'active' && data.sellerVac === false){
                singleItemTotalPrice = (data.discount.name == 'Same' || data.discount.name == 'SameAndDiscount') ? ((data.quantity/data.discount.discount_on_product.purchase * (data.discount.discount_on_product.bonus) + data.quantity) * data.inventory.ePTR) : data.inventory.ePTR * data.quantity  //data.price * data.quantity
                  GST = data.inventory.ePTR/100 * Number(data.gstPercentage) //data.price / 100 * Number(data.gstPercentage)

                  singleItemTotalGST = (data.discount.name == 'Same' || data.discount.name == 'SameAndDiscount') ? ((data.quantity/data.discount.discount_on_product.purchase * (data.discount.discount_on_product.bonus) + data.quantity)) * GST :   GST *  data.quantity

                  AllItemsTotalPrice = AllItemsTotalPrice + singleItemTotalPrice
                  AllItemsTotalGST = AllItemsTotalGST + singleItemTotalGST
                  
                  if(data.discount&&data.discount.type==='Discount'){
                    discountMinus = singleItemTotalPrice/100 * data.discount.discount_per
                    priceAfterDiscount = singleItemTotalPrice - discountMinus
                    newGST = priceAfterDiscount/100 * Number(data.gstPercentage) 
                    finalGST = finalGST + newGST
                    totalDiscountMinus = totalDiscountMinus + discountMinus

                  }
                  if(data.discount&&(data.discount.type==='SameAndDiscount'||data.discount.type==='DifferentAndDiscount')){
                    discountMinus = singleItemTotalPrice/100 * data.discount.discount_per
                    priceAfterDiscount = singleItemTotalPrice - discountMinus
                    newGST = priceAfterDiscount/100 * data.gstPercentage 
                    finalGST = finalGST + newGST
                    totalDiscountMinus = totalDiscountMinus + discountMinus

                  }
                  if(data.discount&&data.discount.type !=='Discount'){
                    finalGST = finalGST + singleItemTotalGST
                  }
                  // total = ((AllItemsTotalPrice - totalDiscountMinus) + finalGST + totalDeliveryCharge).toFixed(2);
                  total = AllItemsTotalPrice + AllItemsTotalGST
                   return (
                    
                    <CartItems
                    handleRem={this.handleRemoveFromCart}
                      data={data}
                      index={index}
                      key={index}
                      singleItemTotalPrice={singleItemTotalPrice}
                      GST={GST}
                      singleItemTotalGST={singleItemTotalGST}
                      AllItemsTotalPrice={AllItemsTotalPrice}
                      AllItemsTotalGST={AllItemsTotalGST}
                      priceAfterDiscount={priceAfterDiscount}
                      newGST={newGST}
                      discountMinus={discountMinus}
                      color={borderGeneric.c}
                    />
                   ) 
                 }
                }
                 )}
                 </div><hr/>
                 </React.Fragment>
                 }
              </div>
            </div>)
                 
                // :<h1 className="EmptyCart">Your shopping cart is empty.</h1>
              
               })}  
          </div>

          <div className='col-xl-3 col-lg-3 col-md-4 col-sm-4 mt-3'>
            <div className="shadow border-0 bg-white text-black card">
              <div className="card-body">
                <h3 className="font-weight-bold ">Order Summary</h3>
                <hr />
                <table style={{ width: '100%' }}>
                  { users && <tr class="mb-1">
                    <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                    <td className="h4  mt-1 mb-1" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>
                    MNY {(users.wallet_balance).toLocaleString('en-IN',{minimumFractionDigits:2,maximumFractionDigits:2})}
                    </td>
                  </tr>}
                  <tr class="mb-1">
                    <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Items Total:</td>
                    <td className="h4  mt-1 mb-1" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>
                      {/* ₹{(AllItemsTotalPrice - totalDiscountMinus).toFixed(2)} */}
                      ₹{(AllItemsTotalPrice).toFixed(2)}
                    </td>
                  </tr>

                  <tr class="mb-1">
                    <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Total GST:</td>
                    <td className="h4  mt-1 mb-1" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>₹
                      {/* {finalGST.toFixed(2)} */}
                    {(AllItemsTotalGST).toFixed(2)}</td>
                  </tr>
                  {totalDeliChrgInCart > 0 ?
                    <tr class="mb-1">
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Delivery Charge:</td>
                      <td className="h4  mt-1 mb-1" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>₹{totalDeliChrgInCart.toFixed(2)}</td>
                    </tr> : ''
                  }
                  {totalDeliChrgInCart > 0 ?
                    <tr class="mb-1">
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Invoice Total:</td>
                      <td className="h4  mt-1 mb-1" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>
                        {/* ₹{((AllItemsTotalPrice - totalDiscountMinus) + finalGST + totalDeliChrgInCart).toLocaleString('en-IN',{maximumFractionDigits:2})} */}
                        ₹{(AllItemsTotalPrice + AllItemsTotalGST + totalDeliChrgInCart).toLocaleString('en-IN',{maximumFractionDigits:2})}
                        </td>
                    </tr> :
                    <tr class="mb-1">
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Invoice Total:</td>
                      <td className="h4  mt-1 mb-1" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>
                        {/* ₹{((AllItemsTotalPrice - totalDiscountMinus) + finalGST).toLocaleString('en-IN',{maximumFractionDigits:2})} */}
                        ₹{(AllItemsTotalPrice + AllItemsTotalGST).toLocaleString('en-IN',{maximumFractionDigits:2})}
                        </td>
                    </tr>
                  }
                  

                  {users.wallet_balance > 0 && this.state.selectedValue == 'Online' ?
                    <tr class="mb-1">
                      <td className="h4 text-success" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                      <td className="h3  mt-1 text-success" style={{ float: 'right', justifyContent: 'end', padding: 0 }}>
                      { Number(total+totalDeliChrgInCart) > Number(users && users.wallet_balance) ?	
	                      <React.Fragment>	
	                      -₹{Number(users && users.wallet_balance ).toFixed(2)}	
	                      </React.Fragment> :	
	                      Number(users && users.wallet_balance) == 0 ?	
	                      <React.Fragment>	
	                      ₹{0}  	
	                      </React.Fragment> :	
	                      <React.Fragment>	
	                      -₹{(Number(total+totalDeliChrgInCart)).toFixed(2)}	
	                      </React.Fragment>	
	                    }
                      </td>
                    </tr> :
                    users.wallet_balance < 0 && this.state.selectedValue == 'Online' ?
                    <tr height='25px'>
                      <td className="h4 text-success" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                      <td className="h3  mt-2 text-success" style={{  textAlign: 'end', padding: 0 }}>
	                      <React.Fragment>	
	                      +₹{Math.abs(users && users.wallet_balance ).toFixed(2)}	
	                      </React.Fragment> 
                      </td>
                    </tr>
                    : null
                  }


                </table>

                 <hr /> 
                 {/* --------------------------------------Seller detials------------------------------- */}
                 {/* {minOrderErrorArray.length > 0 ? 
                 <React.Fragment>
                 <h3 className="font-weight-bold ">Seller Details</h3>
                <hr />
                <table style={{ width: '100%' }}>

              
                  {sellerDetails.map((data, index) => {
                    
                    return (
                      <tr>
                        <td className="h4 mt-1" style={{ float: 'left', justifyContent: 'end', marginBottom: "0px"}}>


                        {data.price < 2000 
                        ?
                            <React.Fragment>                              

                                {(i++) + "." + " " + "Seller: "}
                              <NavLink className="buyerRedirectNavlink" to={`/view-seller/${data.user_id}`}>
                                {data.seller_name}
                              </NavLink>
                              
                            </React.Fragment>
                         :
                         null
                        }
                      



                          {data.price < 2000 
                          ?

                              data.discount_type == "Discount" && data.discount_per ?

                            <h6 className="ml-3" style={{ color: "red" }}>
                              {"Minimum order amount is ₹2000 excluding GST. Add items worth ₹" + (2000 - (data.price - (data.price/100 * Number(data.discount_per)))).toFixed(2) + " more."}
                            </h6>

                            :

                            <h6 className="ml-3" style={{ color: "red" }}>
                              {data.price < 2000 ? "Minimum order amount is ₹2000 excluding GST. Add items worth ₹" + (2000 - data.price).toFixed(2) + " more." : ''}
                            </h6>



                            :
                            null
                          }






                        </td>
                      </tr>
                    )
                  })
                  }
                  <br/>


                </table>  <hr /> </React.Fragment> : '' } */}


                <table style={{ width: '100%' }}>

                  <tr>
                    <td className="font-weight-bold h3 text-maroon" >Total:</td>
                    <td className="font-weight-bold h3 text-red mt-1" style={{ textAlign: 'right', float: 'right', padding: 0 }}>
                    { this.state.selectedValue == 'Online' &&  Number(users && users.wallet_balance) > Number(total+totalDeliChrgInCart) ?
                    <React.Fragment>	
                     ₹{0}  	
                    </React.Fragment> :
                    this.state.selectedValue == 'Online' &&  Number(users && users.wallet_balance) < Number(total+totalDeliChrgInCart) ?
                    <React.Fragment>	
                     ₹{(Number(total+totalDeliChrgInCart) - Number(users && users.wallet_balance )).toLocaleString('en-IN',{maximumFractionDigits:2})}  	
                    </React.Fragment> :
                    this.state.selectedValue !== 'Online' ? 
                    <React.Fragment>	
                     ₹{Number(total+totalDeliChrgInCart).toLocaleString('en-IN',{maximumFractionDigits:2})}  	
                    </React.Fragment> : ''
	                    }
                    </td>
                  </tr>
                </table>
                <hr />

                <h3 className="font-weight-bold ">Payment Method</h3>
                <hr />
                
              { users && users.cod && users.cod === true && users.codService && users.codService === true  && this.state.globalCod && this.state.globalCod.key == 'globalCod' && this.state.globalCod.value == '1' && isCoolChain.length == 0 && isPrepaid.length < 1 && prepaidInvenAll.length == 0?
                <Row>
                  <Col md={9} xl={9} lg={9} sm={9} xs={9} className="mt-2">
                    <h4>Cash on Delivery:</h4>
                  </Col>
              
                  <Col md={3} xl={3} lg={3} sm={3} xs={3}>
                    <Radio color="primary" checked={this.state.selectedValue === 'COD'} onChange={this.handleRadioChange} value="COD" name="radio button demo" aria-label="A" />
                  </Col>
                  
                </Row>
                : ''
              }

                {/* --------------------------------------  PAYTM IS DISABLED TEMPORARILY----------------------------------------------------------- */}
                { !asAdmin ?
                <Row>  
                  <Col md={9} xl={9} lg={9} sm={9} xs={9} className="mt-2">
                    <h4>Pay Now:</h4>
                  </Col>
                  <Col md={3} xl={3} lg={3} sm={3} xs={3}>
                    <Radio color="primary" checked={this.state.selectedValue === 'Online'} onChange={this.handleRadioChange} value="Online" name="radio button demo" aria-label="B" />
                  </Col>
                </Row>
                : ''}
                <br /> 
                {

                // users && AllItemsTotalPrice.toFixed(2) < 2000 && cartDetails&&cartDetails.detail&&cartDetails.detail.length !== 0 ?

                // <React.Fragment>
                //   <h4 >Minimum order amount is ₹2000.</h4>
                //   <h4 >Add items worth ₹{ (2000 - (AllItemsTotalPrice - totalDiscountMinus)).toFixed(2)} more.</h4>
                // </React.Fragment>

                //     :
                
                // (users && users.cod && users.cod == false || users.codService && users.codService == false  || this.state.globalCod && this.state.globalCod.key == 'globalCod' && this.state.globalCod.value == '0') ?
                
                // <button style={{ padding: '10px', width: '100%', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }}>
                //     COD is Disabled
                //   </button>

                //   :

                //////--------------------////////// 2000 min order error ///////-------------/////////
                // minOrderErrorArray.length > 0 && this.state.selectedValue !== '' ?

                //   <button style={{ padding: '10px', width: '100%', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} disabled={this.state.isLoading}
                //     onClick={this.handleMinOrderError}>
                //     Confirm Your Order
                //   </button>
                
                // :
                                       
                users  ?
                   cartDetails&&cartDetails.detail&&cartDetails.detail.length !== 0 && this.state.selectedValue !== '' ?

                  <button style={{ padding: '10px', width: '100%', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} disabled={this.state.isLoading}
                    onClick={this.handleOnlineModal}>
                      Confirm Your Order
                  </button>

                :
                users  &&
                cartDetails&&cartDetails.detail&&cartDetails.detail.length !== 0  && this.state.selectedValue == '' ?
                <button style={{ padding: '10px', width: '100%', backgroundColor: '#dfdfdf', color: 'black', borderRadius: '5px', border: 'none', cursor: "pointer" }}>
                  Select Payment Method
                </button>
                :

                  <NavLink className="buyerRedirectNavlink" to={{ pathname:'/login', state: '/'}}>             
                    <button style={{ padding: '10px', width: '100%', backgroundColor: '#dfdfdf', color: 'black', borderRadius: '5px', border: 'none', cursor: "pointer" }}>
                      Add some items
                    </button>
                  </NavLink>

                    :

                isUserLoggedIn === null ?

                  <NavLink className="buyerRedirectNavlink" to={{ pathname:'/login', state: '/MyCart'}}>             
                    <button style={{ padding: '10px', width: '100%', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }}>
                      Sign In To Continue
                    </button>
                  </NavLink>

                    :

                // users && AllItemsTotalPrice.toFixed(2) > 50000 ?

                //   <h3 style={{ textAlign: "center" }}>Total order amount should not exceed ₹50000.</h3>

                //   :
              //   <button style={{ padding: '10px', width: '100%', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }}
              //   onClick={this.handleOnlineModal}>
              //     Confirm Your Order
              // </button>
                  ''
                }

              </div>
            </div>
          </div>
{/* -------------------------- ERROR --------------------------------------- */}

          <Dialog open={this.state.openModal == true} onClose={this.handleClose} fullWidth={true} maxWidth={'md'}>
            <DialogContent className="mb-1" style={{ overflow: "hidden" }}>
              <MyCartModalError errorData = {errorData}/>
              {/* {this.state.individualSeller == '' ?  */}
                <h4 className="mt-1">Do you accept the above changes?</h4>
                {/* :''
              } */}
            </DialogContent>
            <DialogActions className="pr-4">
              {/* { this.state.individualSeller == '' ?  */}
              <React.Fragment>
                <Button onClick={this.handleClose} color='secondary'>No</Button>
                <Button type="submit" color='primary' onClick={this.yesShowCart}>Yes, update cart</Button>
              </React.Fragment>
              {/* :
              <React.Fragment>
              <Button type="submit" color='primary' onClick={this.closeAllDialog}>Ok</Button>
              </React.Fragment>
            } */}
            </DialogActions>
          </Dialog>

{/* -------------------------- 2000 MIN ORDER ERROR --------------------------------------- */}

          <Dialog open={this.state.minOrderError == true} onClose={this.handleMinOrderErrorClose} fullWidth={true}>
            <DialogContent style={{ overflow: "hidden" }}>
              <DialogTitle className="" id="alert-dialog-title" style={{ textAlign: "center" }}>
                <span className="text-danger mb-3">
                  <i class="zmdi zmdi-alert-circle animated wobble zmdi-hc-5x"></i>
                </span>
                {/* <h1 className="mt-4 font-weight-bold">Order Placed Successfully</h1> */}
              </DialogTitle>
              <h4 className="font-weight mt-3 mb-5">You have not crossed the minimum order price of ₹2000 for the following sellers:</h4>

              {sellerDetails.map((data) => {
                return (
                  <tr >
                  <td className="h4 mt-1" style={{ float: 'left', justifyContent: 'end', marginBottom: "0px",cursor:'pointer'}}>

                    
                  {/* <span onClick={(e) => this.redirectSeller(data) }> */}
                    {data.price < 2000 ? 

                          data.discount_type == "Discount" && data.discount_per ?
                    <React.Fragment>
                    { (j++) + "." + " " + "Seller Name: "   }
                       <NavLink className="buyerRedirectNavlink" to={`/view-seller/${data.user_id}`}>
                       {data&&data.seller_name}
                       </NavLink>
                    {  " " +  "₹" + (2000 - (data.price - (data.price/100 * Number(data.discount_per)))).toFixed(2) + " short of ₹2000" }
                      </React.Fragment>:<React.Fragment>
                    {(j++) + "." + " " + "Seller Name: "   }
                       <NavLink className="buyerRedirectNavlink" to={`/view-seller/${data.user_id}`}>
                    {data&&data.seller_name} 
                    </NavLink>
                    
                   {  " " +  "₹" + (2000 - data.price).toFixed(2) + " short of ₹2000" }
                    </React.Fragment> : 
                    ' '
                    }
                  {/* </span> */}
                  </td>
                </tr>
                )
              })}

              {
                noMinOrderErrorArray.length < 1 ?
                <h4 className="font-weight mt-5 mb-3">
                  Please continue shopping and add more products offered by these sellers.
                </h4>
                :
                <h4 className="font-weight mt-5 mb-3">
                  Please continue shopping and add more products offered by these sellers, or place this order for products excluding the ones offered by these sellers.
                </h4>
              }

            </DialogContent>
            
            <DialogActions className="pr-4">
              <Button onClick={this.handleMinOrderErrorClose} color='secondary'>Continue shopping</Button>

              {
                noMinOrderErrorArray.length < 1 ?
                null
                :
                  <Button type="submit" color='primary' onClick={()=>this.minOrderErrorProceed(removeInventoryLists)}>Place Order</Button>
              }
            {/* Remove and  */}

            </DialogActions>

          </Dialog>

{/* -------------------------- 2000 MIN ORDER ERROR --------------------------------------- */}

<Dialog open={this.state.minOrderErrorIndividual == true} onClose={this.handleMinOrderErrorClose} fullWidth={true}>
            <DialogContent style={{ overflow: "hidden" }}>
              <DialogTitle className="" id="alert-dialog-title" style={{ textAlign: "center" }}>
                <span className="text-danger mb-3">
                  <i class="zmdi zmdi-alert-circle animated wobble zmdi-hc-5x"></i>
                </span>
                {/* <h1 className="mt-4 font-weight-bold">Order Placed Successfully</h1> */}
              </DialogTitle>
              <h4 className="font-weight mt-3 mb-5">You have not crossed the minimum order price of ₹2000 for the following seller:</h4>

              {sellerDetails.map((data) => {
                return (
                  data.user_id == this.state.individualSeller.ID ? 
                  <tr >
                  <td className="h4 mt-1" style={{ float: 'left', justifyContent: 'end', marginBottom: "0px",cursor:'pointer'}}>

                    
                  {/* <span onClick={(e) => this.redirectSeller(data) }> */}
                    {data.price < 2000 ? 

                          data.discount_type == "Discount" && data.discount_per ?
                    <React.Fragment>
                    { "Seller Name: "   }
                       <NavLink className="buyerRedirectNavlink" to={`/view-seller/${data.user_id}`}>
                       {data&&data.seller_name}
                       </NavLink>
                    {  " " +  "₹" + (2000 - (data.price - (data.price/100 * Number(data.discount_per)))).toFixed(2) + " short of ₹2000" }
                      </React.Fragment>:<React.Fragment>
                    {"Seller Name: "   }
                       <NavLink className="buyerRedirectNavlink" to={`/view-seller/${data.user_id}`}>
                    {data&&data.seller_name} 
                    </NavLink>
                    
                   {  " " +  "₹" + (2000 - data.price).toFixed(2) + " short of ₹2000" }
                    </React.Fragment> : 
                    ' '
                    }
                  {/* </span> */}
                  </td>
                </tr>
                :'')
              })}

              {/* {
                noMinOrderErrorArray.length < 1 ? */}
                <h4 className="font-weight mt-5 mb-3">
                  Please continue shopping and add more products offered by this seller.
                </h4>
              {/* //   :
              //   <h4 className="font-weight mt-5 mb-3">
              //     Please continue shopping and add more products offered by these sellers, or place this order for products excluding the ones offered by these sellers.
              //   </h4>
              // } */}

            </DialogContent>
            
            <DialogActions className="pr-4">
              <Button onClick={this.handleMinOrderErrorClose} color='secondary'>Continue shopping</Button>

              {/* {
                noMinOrderErrorArray.length < 1 ?
                null
                :
                  <Button type="submit" color='primary' onClick={this.minOrderErrorProceed}>Place Order</Button>
              } */}
            

            </DialogActions>

          </Dialog>
{/* ===========================FOR ONLINE======================================= */}


          <Dialog open={this.state.open} onClose={this.handleOnlineModalClose} fullWidth={true}
            maxWidth={'sm'}>
            <DialogTitle className='pb-0'>
              Checkout
          </DialogTitle>
            <DialogContent>
              <p style={{marginBottom: '2px',textAlign:'left',fontWeight: 'bold'}}>
                "Please note that our company treats Return / Non-acceptance of orders by customers very strictly. In such events, COD facility shall be withdrawn and delivery charges might be applicable on subsequent orders of such customers.<br/>  
               
              In case of any disagreement with courier personnel or issue with the Order, please contact our support team at +91 9321927004"</p>
              <DialogContentText>
                {'Are you sure you want to checkout ?'}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleOnlineModalClose} color='secondary' >
                Cancel
            </Button>
              <Button color='primary' disabled={this.state.isLoading} onClick={this.handleOnlineOrder} >
                Order
            </Button>
            </DialogActions>
          </Dialog>

     {/* ------------------------Individual pop--------------------------------------- */}

     <Dialog open={this.state.individualPop} onClose={this.handleOnlineModalClose} fullWidth={true}
            maxWidth={'sm'}>
            <DialogTitle style={{paddingBottom: '0px'}}>
              Checkout: {individualSeller.name}
          </DialogTitle>
        <p style={{marginBottom: '2px',textAlign:'left',padding:'0px 25px',fontWeight: 'bold'}}>
          "Please note that our company treats Return / Non-acceptance of orders by customers very strictly. In such events, COD facility shall be withdrawn and delivery charges might be applicable on subsequent orders of such customers.<br/>  
               
              In case of any disagreement with courier personnel or issue with the Order, please contact our support team at +91 9321927004"</p>
            <DialogContent className='pt-0'>
              <DialogContentText style={{marginBottom: '0px'}}>
                {'Are you sure you want to checkout ?'}<br/>
                <div className='col-xl-11 col-lg-11 col-md-11 col-sm-11 ' style={{paddingLeft:'0px'}}>
                  <table style={{width:'100%'}}>
                  { users && <tr>
                    <td className="h4" style={{ justifyContent: 'start' }}>MediWallet:</td>
                    <td className="h3 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                      MNY {(users.wallet_balance).toLocaleString('en-IN',{minimumFractionDigits:2,maximumFractionDigits:2})}
                    </td>
                  </tr>}
                  <tr height='25px'>
                    <td className="h4" style={{ justifyContent: 'start' }}>Items Total:</td>
                    <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                      ₹{individualSeller&&(individualSeller.price).toLocaleString('en-IN',{maximumFractionDigits:2})}
                    </td>
                  </tr>
                  <tr height='25px'>
                    <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Total GST:</td>
                    <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                      ₹{(individualSeller&&individualSeller.gst).toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                  </tr>
                  {deliChrg > 0 ?
                    <tr height='25px'>
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Delivery Charge:</td>
                      <td className="h4  mt-2" style={{  justifyContent: 'end', padding: 0 }}>₹{deliChrg.toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                    </tr> : ''
                  }
                  {deliChrg > 0 ?
                    <tr height='25px'>
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Invoice Total:</td>
                      <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                        ₹{((individualSeller.price) + (individualSeller.gst) + deliChrg).toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                    </tr> :
                    <tr height='25px'>
                      <td className="h4" >Invoice Total:</td>
                      <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                        ₹{((individualSeller.price) + (individualSeller.gst)).toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                    </tr>
                  }
                  

                  {users.wallet_balance > 0 && this.state.selectedValue == 'Online' ?
                    <tr height='25px'>
                      <td className="h4 text-success" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                      <td className="h3  mt-2 text-success" style={{  justifyContent: 'end', padding: 0 }}>
                      { Number((Number(individualSeller.gst) +Number(individualSeller.price))+deliChrg) > Number(users && users.wallet_balance) ?	
	                      <React.Fragment>	
	                      -₹{Number(users && users.wallet_balance ).toFixed(2)}	
	                      </React.Fragment> :	
	                      Number(users && users.wallet_balance) == 0 ?	
	                      <React.Fragment>	
	                      ₹{0}  	
	                      </React.Fragment> :	
	                      <React.Fragment>	
	                      -₹{(Number((individualSeller.gst) +Number(individualSeller.price)+deliChrg)).toLocaleString('en-IN',{maximumFractionDigits:2})}	
	                      </React.Fragment>	
	                    }
                      </td>
                    </tr> :
                    users.wallet_balance < 0 && this.state.selectedValue == 'Online' ?
                    <tr height='25px'>
                      <td className="h4 text-success" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                      <td className="h3  mt-2 text-success" style={{  justifyContent: 'end', padding: 0 }}>
	                      <React.Fragment>	
	                      +₹{Math.abs(users && users.wallet_balance ).toFixed(2)}	
	                      </React.Fragment> 
                      </td>
                    </tr>
                    : null
                  }
                  <tr height='25px'>
                    <td className="font-weight-bold h4 text-maroon" >Total:</td>
                    <td className="font-weight-bold h4 text-red" style={{ textAlign: 'left',  padding: 0 }}>
                    { this.state.selectedValue == 'Online' &&  Number(users && users.wallet_balance) > Number((individualSeller.gst) + individualSeller.price) + deliChrg ?
                    <React.Fragment>	
                     ₹{0}  	
                    </React.Fragment> :
                    this.state.selectedValue == 'Online' &&  Number(users && users.wallet_balance) < Number(((individualSeller.gst) + individualSeller.price) + deliChrg) ?
                    <React.Fragment>	
                     ₹{(Number(((individualSeller.gst) +individualSeller.price)+deliChrg) - Number(users && users.wallet_balance )).toLocaleString('en-IN',{maximumFractionDigits:2})}  	
                    </React.Fragment> :
                    this.state.selectedValue !== 'Online' ? 
                    <React.Fragment>	
                     ₹{Number(((individualSeller.gst) + individualSeller.price)+deliChrg).toLocaleString('en-IN',{maximumFractionDigits:2})}  	
                    </React.Fragment> : ''
	                    }
                    </td>
                  </tr>
                  </table>
                </div>
                <hr />
                 <h4 className="font-weight-bold mb-0 mt-3">Payment Method</h4>
                { users && users.cod && users.cod === true && users.codService && users.codService === true  && this.state.globalCod && this.state.globalCod.key == 'globalCod' && this.state.globalCod.value == '1' && individualSeller.coolChain == 0 && individualSeller.prepaid == 0 && individualSeller.prepaidInven == 0?
                <Row>
                  <Col md={4} xl={4} lg={4} sm={4} xs={4} className="mt-3">
                    <h4 className="mb-0">Cash on Delivery:</h4>
                  </Col>
              
                  <Col md={2} xl={2} lg={2} sm={2} xs={2} className="mt-2">
                    <Radio color="primary" checked={this.state.selectedValue === 'COD'} onChange={this.handleRadioChange} value="COD" name="radio button demo" aria-label="A" />
                  </Col>
                  <Col md={6} xl={6} lg={6} sm={6} xs={6}>
                  </Col>
                </Row>
                : ''
              }
                { !asAdmin ?
                <Row>  
                  <Col md={4} xl={4} lg={4} sm={4} xs={4} className="mt-2">
                    <h4>Pay Now:</h4>
                  </Col>
                  <Col md={2} xl={2} lg={2} sm={2} xs={2} >
                    <Radio color="primary" checked={this.state.selectedValue === 'Online'} onChange={this.handleRadioChange} value="Online" name="radio button demo" aria-label="B" />
                  </Col>
                  <Col md={6} xl={6} lg={6} sm={6} xs={6}>
                  </Col>
                </Row>
                : ''}
              </DialogContentText>
              <DialogActions style={{paddingRight:'0px'}}>
              <Button onClick={this.handleOnlineModalClose} style={{paddingRight:'0px'}} color='secondary' >
                Cancel
            </Button>
              { this.state.selectedValue != '' ?
                <Button color='primary' disabled={this.state.isLoading} onClick={this.handleOnlineOrder} >
                Order
            </Button> :
            <Button color='primary' style={{cursor: "pointer",paddingRight:'0px'}}  >
            Select Payment Method
        </Button>
            }
            </DialogActions>
            </DialogContent>
            
          </Dialog>

          {/* ------------------------Individual medicine pop--------------------------------------- */}

     <Dialog open={this.state.medicinePop} onClose={this.handleOnlineModalClose} fullWidth={true}
            maxWidth={'sm'}>
            <DialogTitle style={{paddingBottom: '0px'}}>
              Checkout: {`${mediTypeData.name} (${mediTypeData.key})`} 
          </DialogTitle>
        <p style={{marginBottom: '2px',textAlign:'left',padding:'0px 25px',fontWeight: 'bold'}}>
          "Please note that our company treats Return / Non-acceptance of orders by customers very strictly. In such events, COD facility shall be withdrawn and delivery charges might be applicable on subsequent orders of such customers.<br/>  
               
              In case of any disagreement with courier personnel or issue with the Order, please contact our support team at +91 9321927004"</p>
            <DialogContent className='pt-0'>
              <DialogContentText style={{marginBottom: '0px'}}>
                {'Are you sure you want to checkout ?'}<br/>
                <div className='col-xl-11 col-lg-11 col-md-11 col-sm-11 ' style={{paddingLeft:'0px'}}>
                  <table style={{width:'100%'}}>
                  { users && <tr>
                    <td className="h4" style={{ justifyContent: 'start' }}>MediWallet:</td>
                    <td className="h3 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                      MNY {(users.wallet_balance).toLocaleString('en-IN',{minimumFractionDigits:2,maximumFractionDigits:2})}
                    </td>
                  </tr>}
                  <tr height='25px'>
                    <td className="h4" style={{ justifyContent: 'start' }}>Items Total:</td>
                    <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                    ₹{(mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic : 0).toLocaleString('en-IN',{maximumFractionDigits:2})}
                    </td>
                  </tr>
                  <tr height='25px'>
                    <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Total GST:</td>
                    <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                      ₹{(mediTypeData.key === 'Cool chain' ? mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.genericGst : 0).toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                  </tr>
                  {individualDeliveryChrg > 0 ?
                    <tr height='25px'>
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Delivery Charge:</td>
                      <td className="h4  mt-2" style={{  justifyContent: 'end', padding: 0 }}>₹{individualDeliveryChrg.toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                    </tr> : ''
                  }
                  {individualDeliveryChrg > 0 ?
                    <tr height='25px'>
                      <td className="h4" style={{ padding: 0, justifyContent: 'start' }}>Invoice Total:</td>
                      <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                        ₹{((
                        mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0) 
                       + 
                      individualDeliveryChrg).toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                    </tr> :
                    <tr height='25px'>
                      <td className="h4" >Invoice Total:</td>
                      <td className="h4 mt-2" style={{  justifyContent: 'end', padding: 0 }}>
                        ₹{(
                        mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0).toLocaleString('en-IN',{maximumFractionDigits:2})}</td>
                    </tr>
                  }
                  

                  {users.wallet_balance > 0 && this.state.selectedValue == 'Online' ?
                    <tr height='25px'>
                      <td className="h4 text-success" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                      <td className="h3  mt-2 text-success" style={{  justifyContent: 'end', padding: 0 }}>
                      { Number(Number( 
                        mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0)+individualDeliveryChrg) > Number(users && users.wallet_balance) ?	
	                      <React.Fragment>	
	                      -₹{Number(users && users.wallet_balance ).toFixed(2)}	
	                      </React.Fragment> :	
	                      Number(users && users.wallet_balance) == 0 ?	
	                      <React.Fragment>	
	                      ₹{0}  	
	                      </React.Fragment> :	
	                      <React.Fragment>	
	                      -₹{(Number( 
                        mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0)+individualDeliveryChrg).toLocaleString('en-IN',{maximumFractionDigits:2})}	
	                      </React.Fragment>	
	                    }
                      </td>
                    </tr> :
                    users.wallet_balance < 0 && this.state.selectedValue == 'Online' ?
                    <tr height='25px'>
                      <td className="h4 text-success" style={{ padding: 0, justifyContent: 'start' }}>MediWallet:</td>
                      <td className="h3  mt-2 text-success" style={{  justifyContent: 'end', padding: 0 }}>
	                      <React.Fragment>	
	                      +₹{Math.abs(users && users.wallet_balance ).toFixed(2)}	
	                      </React.Fragment> 
                      </td>
                    </tr>
                    : null
                  }
                  <tr height='25px'>
                    <td className="font-weight-bold h4 text-maroon" >Total:</td>
                    <td className="font-weight-bold h4 text-red" style={{ textAlign: 'left',  padding: 0 }}>
                    { this.state.selectedValue == 'Online' &&  Number(users && users.wallet_balance) > Number( mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0) + individualDeliveryChrg ?
                    <React.Fragment>	
                     ₹{0}  	
                    </React.Fragment> :
                    this.state.selectedValue == 'Online' &&  Number(users && users.wallet_balance) < Number(( mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                    mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                    mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0) + individualDeliveryChrg) ?
                    <React.Fragment>	
                     ₹{(Number(( 
                      mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0)+individualDeliveryChrg) - Number(users && users.wallet_balance )).toLocaleString('en-IN',{maximumFractionDigits:2})}  	
                    </React.Fragment> :
                    this.state.selectedValue !== 'Online' ? 
                    <React.Fragment>	
                     ₹{Number(( 
                      mediTypeData.key === 'Cool chain' ? mediTypeData.coolChain + mediTypeData.coolChainGst : 
                      mediTypeData.key === 'Ethical branded' ? mediTypeData.ethical + mediTypeData.ethicalGst : 
                      mediTypeData.key === 'Generic' ? mediTypeData.generic + mediTypeData.genericGst : 0)+individualDeliveryChrg).toLocaleString('en-IN',{maximumFractionDigits:2})}  	
                    </React.Fragment> : ''
	                    }
                    </td>
                  </tr>
                  </table>
                </div>
                <hr />
                 <h4 className="font-weight-bold mb-0 mt-3">Payment Method</h4>
                { users && users.cod && users.cod === true && users.codService && users.codService === true  && this.state.globalCod && this.state.globalCod.key == 'globalCod' && this.state.globalCod.value == '1' && !mediTypeData.showCod && !mediTypeData.showCodForProd && !mediTypeData.showCodForInven
                 ?
                <Row>
                  <Col md={4} xl={4} lg={4} sm={4} xs={4} className="mt-3">
                    <h4 className="mb-0">Cash on Delivery:</h4>
                  </Col>
              
                  <Col md={2} xl={2} lg={2} sm={2} xs={2} className="mt-2">
                    <Radio color="primary" checked={this.state.selectedValue === 'COD'} onChange={this.handleRadioChange} value="COD" name="radio button demo" aria-label="A" />
                  </Col>
                  <Col md={6} xl={6} lg={6} sm={6} xs={6}>
                  </Col>
                </Row>
                : ''
              }
                { !asAdmin ?
                <Row>  
                  <Col md={4} xl={4} lg={4} sm={4} xs={4} className="mt-2">
                    <h4>Pay Now:</h4>
                  </Col>
                  <Col md={2} xl={2} lg={2} sm={2} xs={2} >
                    <Radio color="primary" checked={this.state.selectedValue === 'Online'} onChange={this.handleRadioChange} value="Online" name="radio button demo" aria-label="B" />
                  </Col>
                  <Col md={6} xl={6} lg={6} sm={6} xs={6}>
                  </Col>
                </Row>
                : ''}
              </DialogContentText>
              <DialogActions style={{paddingRight:'0px'}}>
              <Button onClick={this.handleOnlineModalClose} style={{paddingRight:'0px'}} color='secondary' >
                Cancel
            </Button>
            { this.state.selectedValue != '' ?
                <Button color='primary' disabled={this.state.isLoading} onClick={this.handleMedicineOrder} >
                  Order
                </Button> :
                <Button color='primary' style={{cursor: "pointer",paddingRight:'0px'}}  >
                  Select Payment Method
                </Button>
            }
            </DialogActions>
            </DialogContent>
            
          </Dialog>
{/* -------------------------- ORDER PLACED --------------------------------------- */}

<Dialog open = {acceptModal} onClose = {this.handleAcceptClose} fullWidth = {true}>
            <DialogContent style={{ overflow: "hidden" }}>
              <DialogTitle className="" id="alert-dialog-title" style={{ textAlign: "center" }}>

                <span className="text-success mb-3">
                  <i class="zmdi zmdi-check-circle animated fadeInUp zmdi-hc-5x"></i>
                </span>
                <h1 className="mt-4 font-weight-bold">Order Placed Successfully</h1>
              </DialogTitle>
  <h3  style={{ textAlign: "center" }}>Your order {`${successData.length>12?`id's:`:'id:'}`} {successData && successData}</h3>
            </DialogContent>
            <DialogActions className="pr-4">
              <Button onClick={this.handleAcceptClose} color='secondary'>Shop More</Button>
            </DialogActions>
          </Dialog>


        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ auth, seller, buyer }) => {

  const { userDetails } = seller;
  const { user_details } = auth;
  const { cartDetails, mediWallet, removeFromCart, addToCart } = buyer;

  let users = userDetails ? userDetails : user_details

  return { user_details, users, cartDetails, mediWallet, removeFromCart, addToCart }
};

export default withRouter(connect(mapStateToProps, { getUserDetail, getCartDetails, getMediWallet, removeFromCart,emptyCart, addToCart })(MyCart));
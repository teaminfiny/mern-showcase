import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {Redirect, withRouter, Link, NavLink} from 'react-router-dom';
class OrderSuccessful extends Component {
    constructor(props) {
        super(props);
        let url = props.location.search
        let json = url ? JSON.parse('{"' + decodeURIComponent(url.substring(1).replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}') : ''
        this.state = {
            openModal: false,
            status: json ? json.status ? json.status :'':'',
            orderId : json ? json.orderId ? json.orderId :'':'',
        }
    }

    componentDidMount() {
        this.setState({
            openModal: true
        })
    }
    handleClose = () => {
        console.log('facmacea',this.props.history)
        this.setState({ openModal: !this.state.openModal })
        this.setState({ requestSuccess: false })
        // this.props.history.replace('/')
        // return (<Redirect push to="/" />);
        // return <Link to="/" replace />
        console.log('facmacea-',this.props.history)
    }
    render() {
        let { status, orderId } = this.state;
        return (
            <React.Fragment>
                <div className="" >
                    <Dialog open={this.state.openModal == true}
                        onClose={this.handleClose}
                        fullWidth={true}>
                        <DialogContent style={{ overflow: "hidden" }}>
                            <DialogTitle className="" id="alert-dialog-title" style={{ textAlign: "center" }}>

                                <span className={`text-${status == 'success' ? 'success' : 'danger'} mb-3`}>
                                    <i className={`zmdi zmdi-${status == 'success' ? 'check' : 'alert'}-circle animated fadeInUp zmdi-hc-5x`}></i>
                                </span>
                                <h1 className="mt-4 font-weight-bold">{status == 'success' ? 'Order placed succesfully' : 'Unable to place order'}</h1>
                            </DialogTitle>
                            { status == 'success' ?
                                <h3  style={{ textAlign: "center" }}>Your order {`${orderId.length>12?`id's:`:'id:'}`} {orderId && orderId}</h3> : ''
                            }
                        </DialogContent>
                        <DialogActions className="pr-4">
                            <a href='/'>
                            <Button onClick={this.handleClose} color='secondary' >	Close </Button>
                            </a>
                        </DialogActions>
                    </Dialog>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(OrderSuccessful);
import React, { Component } from 'react';
import CustomScrollbars from 'util/CustomScrollbars';
import './CartNoti.css'

import { Col, Row, Container } from 'reactstrap'

import {connect} from 'react-redux';

import {getUserDetail} from  'actions'

import {getCartDetails} from 'actions/buyer'

import {removeFromCart} from 'actions/buyer'
import helpertFn from 'constants/helperFunction';


class CartNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 280,
      value:'',
      localData:[]
    }
  }

  handleDeleteChange = (e, index) => {
    let tempCartNoti = this.state.notifications;
    tempCartNoti.splice(index, 1)
    this.setState({ notifications: tempCartNoti })
  }


  componentDidMount = () => {
    const isUserLoggedIn = localStorage.getItem("buyer_token");
    if (isUserLoggedIn !== null) {
      this.props.getCartDetails({ history: this.props.history })
    }
    // else{
    //   let inventoryData=localStorage.getItem('obj');
    // let localData = JSON.parse( inventoryData);
    // this.setState({localData})
    // }
  }

  handleRemoveFromCart = (e, index,event) => {
    // const isUserLoggedInn = localStorage.getItem("buyer_token");
  // if (isUserLoggedInn !== null) {
    this.props.removeFromCart({ inventory_id: index,history:this.props.history})    
  // }
  // else{
   
  //   let inventoryData = localStorage.getItem('obj');
  //   let localData = JSON.parse(inventoryData);
    
  //   var position = localData.findIndex((val)=>val.inventory_id===index)
       
  //       localData.splice(position, 1);
  //       this.setState({localData})
        
  //       localStorage.setItem('obj', JSON.stringify(localData));
       
  // }
}
    
addDefaultSrc(ev){
  ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
}

  render() {
    
    let { height } = this.state;

    const { cartDetails } = this.props;

    let notifications = [];
    let data = []
    // let inventoryData=localStorage.getItem('obj');
    // let localData = JSON.parse( inventoryData);
    // const isLogined = localStorage.getItem("buyer_token");
    // if(isLogined){
    cartDetails && cartDetails.detail && cartDetails.detail.length > 0 && cartDetails.detail.map((dataOne,index) => {
      notifications.push({
        image: dataOne.Inventory.product_id.images[0],
        title: dataOne.Inventory.product_id.name,
        time: dataOne.Inventory.updatedAt,
        offer: dataOne.discount_per,
        quantity:  dataOne.cart_details.quantity,
        MRP: dataOne.Inventory.MRP,
        ePTR: dataOne.Inventory.ePTR,
        PTR: dataOne.Inventory.PTR,
        inventory_id: dataOne.Inventory._id,
        discount: dataOne.Discount,
        discountProduct: dataOne.DiscountProduct,
      })
    })

    

  // }
  //   else{
  //     localData && localData && localData.length > 0 && localData.map((dataOne,index,value) => {
  //       notifications.push({
  //         image: dataOne.image,
  //         title: dataOne.title,
  //         time: dataOne.updatedAt,
  //         // offer: dataOne.discount_per,
  //         quantity:  dataOne.quantity,
  //         price: dataOne.price,
  //         price1: dataOne.PTR,
  //         inventory_id: dataOne.inventory_id
  //       })
  //     })
    // }

    return (
      <CustomScrollbars className="messages-list scrollbar container1" style={{ height: height}}>
        <ul className="list-unstyled">
          {notifications && notifications.length > 0 ? notifications.map((notification, index) => {
            return <li className="media p-2">
              <Container>
                <Row>
                  <Col className='1' md={3}>
                    <img
                      alt={notification.image}
                      src={`${helpertFn.productImg(notification.image)}`}
                      className="mr-2 CartImage"
                      onError={this.addDefaultSrc}
                    />
                  </Col>
                  <Col className='2' md={7}>
                    <div className="media-body  pt-1">
                      <p className="h6 mb-0 orderTitle">{notification.title} <i class="zmdi zmdi-close"/> {notification.quantity} Qty
                        <br />

                        {(notification.discount.type === "Discount" || notification.discount.type === "Same" || notification.discount.type === "SameAndDiscount" || notification.discount.type === "DifferentAndDiscount") ?
                        <div>
                        ₹{(notification.ePTR).toFixed(2)}&nbsp;&nbsp;
                              <strike>₹{(notification.PTR).toFixed(2)}</strike>
                        </div>
                              
                              :
                              "₹" + (notification.PTR).toFixed(2)

                        }
                       
                      </p>
                    </div>
                  </Col>
                  <Col className='3' md={2}>
                    <div style={{ float: "right", cursor: "pointer"}}> <i className="zmdi zmdi-close"  onClick={(e) => this.handleRemoveFromCart(e, notification.inventory_id, index)}></i> </div>
                  </Col>
                </Row>
              </Container>
            </li>

          }) : <div className="media-body align-self-center">
              <p className="heading mb-0 text-center"> No Items Found </p>
            </div>
          }
        </ul>
      </CustomScrollbars>
    )
  }
};

const mapStateToProps = ({ auth, seller, buyer }) => {

  const { userDetails } = seller;
  const { user_details } = auth;
  const { cartDetails, removeFromCart } = buyer;

  let users = userDetails ? userDetails : user_details

  return { user_details, users, cartDetails, removeFromCart}
};

export default connect(mapStateToProps, { getUserDetail, getCartDetails, removeFromCart })(CartNotification);

// className="mr-1 text-danger"
// onClick={(e) => this.handleCartValue(e, index, 'decrement')}><i class="zmdi zmdi-minus-circle-outline"></i>
// onClick={(e) => this.handleCartValue(e, index, 'increment')}><i class="zmdi zmdi-plus-circle-o"></i>

//<span  ></span>
//<span className="mb-0 text-primary h4"></span>
//<span className="ml-1 text-success" ></span>
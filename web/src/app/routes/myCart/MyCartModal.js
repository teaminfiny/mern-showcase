import React, { Component } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle'; 


class MyCartModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      RequiredQuantity: '',
      discount: ''
    }
  }


  render() {



    const { dataFromParent, name } = this.props;

    return (
      <React.Fragment>

        <DialogTitle className="mt-5" id="alert-dialog-title" style={{ textAlign: "center" }}>
          
              <span className="text-success">
                <i class="zmdi zmdi-check-circle animated fadeInUp zmdi-hc-5x"></i>
              </span>

        </DialogTitle>

        <h1 className="font-weight-bold"style={{textAlign:"center"}}>All Good!</h1>

      </React.Fragment>
    );
  }
}

export default MyCartModal;
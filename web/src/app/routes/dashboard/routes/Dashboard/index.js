import React from 'react';
import Widget from "components/Widget/index";
import {connect} from 'react-redux';
import {getDashboardCard,getUserDetail} from 'actions'
import ContainerHeader from 'components/ContainerHeader';
import SalesStatistic from 'components/dashboard/eCommerce/SalesStatistic';
import CardMenu from 'components/dashboard/Common/CardMenu';
import IntlMessages from 'util/IntlMessages';
import './../../routes/Misc/test.css'
import axios from '../../../../../constants/axios';
import { NavLink, withRouter } from 'react-router-dom';

class Dashboard extends React.Component {

  onOptionMenuSelect = event => {
    this.setState({ menuState: true, anchorEl: event.currentTarget });
  };

  handleRequestClose = () => {
    this.setState({ menuState: false });
  };

  constructor() {
    super();
    this.state = {
      anchorEl: undefined,
      menuState: false,
      rating : []
    }
  }

  componentDidMount = async () => {
    await axios.get('/users/getAverageRating', {
      headers: {
        'Content-Type': 'application/json',
        token:localStorage.getItem('token')
      }
    }
    ).then(result => {
      let that = this;
      if (result.data.error) {
      } else {
        that.setState({rating : result.data.detail})
      }
    })
      .catch(error => {
      });
    this.props.getDashboardCard({history:this.props.history})
  }

  render() {
    const { anchorEl, menuState, rating } = this.state;
    const {activeSelling,shortExpire,outOfStock, slowMoving, userDetails, expiredProducts ,sellerStats} = this.props
    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">
        <ContainerHeader match={this.props.match} rating={rating} title={userDetails ? userDetails.first_name ? <IntlMessages id={`Hi ${userDetails.first_name}`} /> : '' : ''}>
          {/* <span>Average Rating</span> */}
        </ContainerHeader>
        <div className="row " >

         
            <div className={`col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12`}>
            <NavLink to={{ pathname: "/seller/inventory/", title: 'activeSelling' }} style={{ cursor: "pointer" }}>
              <Widget styleName={`p-3 bg-primary text-white widgetCard`}>
                <div className="media align-items-center flex-nowrap py-lg-2">
                  <div className="mr-3 text-white ">
                    <img src={require('assets/images/dashboard/deals.png')} alt={require('assets/images/dashboard/deals.png')} />
                  </div>
                  <div className="media-body">
                    <h3 style={{ fontSize: '20px' }} className="dashboardCardHead mb-1 text-white">{'Active Selling Products'}</h3>
                    <p style={{ fontSize: '30px' }} className="dashboardCardText mb-0">{sellerStats.activeSelling ? sellerStats.activeSelling : 0}</p>
                  </div>
                </div>
              </Widget>
              </NavLink>
            </div>

          <div className={`col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12`}>
          <NavLink to={{ pathname: "/seller/inventory/", title: 'shortExpiry' }} style={{ cursor: "pointer" }}>
            <Widget styleName={`p-3 bg-warning text-white widgetCard`}>
              <div className="media align-items-center flex-nowrap py-lg-2">
                <div className="mr-3 text-white ">
                  <img src={require('assets/images/dashboard/expiry.png')} alt={require('assets/images/dashboard/expiry.png')} />
                </div>
                <div className="media-body">
                  <h3 style={{fontSize:'20px'}} className="dashboardCardHead mb-1 text-white">{'Short Expire Products'}</h3>
                  <p style={{fontSize:'30px'}}className="dashboardCardText mb-0">{sellerStats&&sellerStats.shortExpireProduct ? sellerStats.shortExpireProduct : 0}</p>
                </div>
              </div>
            </Widget>
            </NavLink>
          </div>

          <div className={`col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 ${'className'}`}>
          <NavLink to={{ pathname: "/seller/inventory/", title: 'slowMoving' }} style={{ cursor: "pointer" }}>
            <Widget styleName={`p-3 bg-grey text-white widgetCard`}>
              <div className="media align-items-center flex-nowrap py-lg-2">
                <div className="mr-3 text-white ">
                  <img src={require('assets/images/dashboard/slow.png')} alt={require('assets/images/dashboard/slow.png')} />
                </div>
                <div className="media-body">
                  <h3 style={{fontSize:'20px'}} className="dashboardCardHead mb-1 text-white">{'Slow Moving Products'}</h3>
                  <p style={{fontSize:'30px'}}className="dashboardCardText mb-0">{sellerStats&&sellerStats.slowMoving ? sellerStats.slowMoving: 0 }</p>
                </div>
              </div>
            </Widget>
            </NavLink>
          </div>



          <div className={`col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12`}>
          <NavLink to={{ pathname: "/seller/inventory/", title: 'outOfStock' }} style={{ cursor: "pointer" }}>
            <Widget styleName={`p-3 bg-red text-white widgetCard`}>
              <div className="media align-items-center flex-nowrap py-lg-2">
                <div className="mr-3 text-white ">
                  <img src={require('assets/images/dashboard/block.png')} alt={require('assets/images/dashboard/block.png')} />
                </div>
                <div className="media-body">
                  <h3 style={{fontSize:'20px'}} className="dashboardCardHead mb-1 text-white">{'Out Of Stock Products'}</h3>
                  <p style={{fontSize:'30px'}}className="dashboardCardText mb-0">{sellerStats&&sellerStats.outOfStock ? sellerStats.outOfStock: 0}</p>
                </div>
              </div>
            </Widget>
            </NavLink>
          </div>
        </div>
        
        <div className="row">
          <div className="col-12">
            <SalesStatistic />
          </div>
        </div>

        <div className="row" style={{ paddingBottom: 25 }}>
          <div className="col-xl-12 col-12">
          </div>
        </div>

        <CardMenu menuState={menuState} anchorEl={anchorEl}
          handleRequestClose={this.handleRequestClose.bind(this)} />
      </div>
    );
  }
}
const mspStateToProps = ({seller})=>{
  const {activeSelling,shortExpire,outOfStock,slowMoving, userDetails, expiredProducts,sellerStats} = seller;
  
  return {activeSelling,shortExpire,outOfStock,slowMoving,userDetails, expiredProducts,sellerStats}
}
export default withRouter(connect(mspStateToProps,{getDashboardCard, getUserDetail}) (Dashboard));
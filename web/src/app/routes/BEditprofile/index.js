import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Widget from "components/Widget/index";
import EditProfileForm from 'components/BEditProfile'
import Password from 'components/BChangePassword'
import CreditCard from 'components/BPayments'
import Button from '@material-ui/core/Button';
import ProfileHeader from "components/profile/ProfileHeader/index";
import { FormGroup, Input, Label } from 'reactstrap';
class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      card: [{
        number: '4242424242424242',
        name: 'Avanish',
        expiry: '0429',
        cvc: '852'
      }, {
        number: '5555555555554444',
        name: 'Avanish',
        expiry: '0429',
        cvc: '852'
      }, {
        number: '378282246310005',
        name: 'Avanish',
        expiry: '0429',
        cvc: '852'
      }]
    }
  }
  handleChange = (event, value) => {
    this.setState({ activeStep: value });
  };
  render() {
    const { activeStep, card } = this.state
    return (
      <div className="app-wrapper">

        <ProfileHeader profile={true} />
        <div className="jr-profile-content">
          <div className="row">
            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
              <Widget styleName="jr-card-full jr-card-tabs-right jr-card-profile">
                <AppBar position="static" color="default" indicatorcolor='primary'>
                  <Tabs textColor='primary' indicatorcolor='primary' centered className="jr-tabs-center" value={activeStep} onChange={this.handleChange} >
                    <Tab textColor='primary' className="profileTab" label="Edit Profile" icon={<i className="zmdi zmdi-account zmdi-hc-2x " />} />
                    <Tab textColor='primary' className="profileTab" label="Change Password" icon={<i className="zmdi zmdi-key zmdi-hc-2x " />} />

                    <Tab textColor='primary' className="profileTab" label="Payments" icon={<i className="zmdi zmdi-card zmdi-hc-2x " />} />


                  </Tabs>
                </AppBar>
                {
                  activeStep === 0 &&
                  <div className="jr-tabs-classic">
                    <div className="jr-tabs-content jr-task-list">
                      <EditProfileForm className='mt-2' />
                    </div>
                  </div>
                }
                {
                  activeStep === 1 &&
                  <div className="jr-tabs-classic">
                    <div className="jr-tabs-content jr-task-list">
                      <Password className='mt-2' />
                    </div>
                  </div>
                }
                {
                  activeStep === 2 &&
                  <div className="jr-tabs-classic">
                    <div className="jr-tabs-content jr-task-list">
                      <div className='row justify-content-center mt-5 mb-5'>

                        {
                          card.map((data, index) =>
                            <div key={index} className='col-sm-4 col-md-4 col-lg-4 mt-2'>
                              <CreditCard cvc={data.cvc} number={data.number} name={data.name}
                                expiry={data.expiry} focus={false} />
                            </div>
                          )
                        }


                      </div>


                      <div className='row justify-content-center'>

                        <FormGroup className={'col-sm-5 col-lg-5 col-md-5 col-xs-5 col-xl-5'}>
                          <Label for="exampleEmail">Card Number</Label>
                          <Input type="text" name="card number" id="exampleEmail" />
                        </FormGroup>
                      </div>
                      <div className='row justify-content-center'>
                        <FormGroup className={'col-sm-5 col-lg-5 col-md-5 col-xs-5 col-xl-5'}>
                          <Label for="exampleEmail">Expiry Date</Label>
                          <Input type="text" name="expiry date" id="exampleEmail" />
                        </FormGroup>
                      </div>
                      <div className='row justify-content-center'>
                        <FormGroup className={'col-sm-5 col-lg-5 col-md-5 col-xs-5 col-xl-5'}>
                          <Label for="exampleEmail">CVC</Label>
                          <Input type="text" name="cvc" id="exampleEmail" />
                        </FormGroup>
                      </div>
                      <div className='row justify-content-center'>
                        <FormGroup className={'col-sm-5 col-lg-5 col-md-5 col-xs-5 col-xl-5'}>
                          <Label for="exampleEmail">Name</Label>
                          <Input type="text" name="Name" id="exampleEmail" />
                        </FormGroup>
                      </div>
                      <div className='row justify-content-center mb-5 '>
                        <FormGroup className={'col-sm-5 col-lg-5 col-md-5 col-xs-5 col-xl-5 buyerEditProfile' }>
                          <Button color="primary" variant="contained" className="jr-btn jr-btn-lg btnPrimary" >


                            <span>Add Card</span>
                          </Button>
                        </FormGroup>
                      </div>
                    </div>
                  </div>





                }
              </Widget>
            </div>



          </div>
        </div>
      </div>
    );
  }
}

export default EditProfile;
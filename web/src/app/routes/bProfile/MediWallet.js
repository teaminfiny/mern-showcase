import React, { Component } from 'react';
import MUIDataTable from "../../../components/DataTable";
import PropTypes from 'prop-types';
import { getUserDetail } from 'actions';
import {getMediWallet} from 'actions/buyer';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import customHead from 'constants/customHead';
import CustomFilter from '../../../components/Filter'
import CustomScrollbars from 'util/CustomScrollbars';
import axios from 'constants/axios';
import { NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';
function TabContainer({children, dir}) {
    return (
      <div dir={dir}>
        {children}
      </div>
    );
  }
  
  TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
  };
class MediWallet extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            value: 0,
            tabValue:0,

            edit: false,
            hide: false,
            delete: false,
            add: false,
            page: 0,
            perPage: 50,
            filter: '',
            // month: 'January',
            // year: 2020,
            month:moment().format('MMMM'),
            year: moment().format('YYYY'),
            wallet:0
         }
}


onFilterChange = (e, key) => {
  this.setState({ [key]: e.target.value });
}

handleResetFilter = (e, filter) => {
  e.preventDefault();
  filter();
  this.setState({
    month: moment().format('MMMM'),
    year: moment().format('YYYY'),
    filter: ''
  });
  let data = {
    page: 1,
    perPage: this.state.perPage,
    filter: this.state.filter,
    month: moment().format('MMMM'),
    year: moment().format('YYYY')
  }
  this.props.getMediWallet({ data, history: this.props.history })
}

componentDidMount(){
  const { page, perPage, filter, month, year } = this.state;
  
  let data = {
    page: 1,
    perPage: 50,
    filter: '',
    // searchText: '',
    month: this.state.month,
    year: this.state.year
  }
  const {users} = this.props;
  this.props.getMediWallet({history:this.props.history, data});
  this.setState({wallet: users && users.wallet_balance });
}

changePage = (page) => {
  let pages = page + 1
  let data = {
    page: pages,
    perPage: this.state.perPage,
    filter: '',
    searchText: '',
    month: this.state.month,
    year: this.state.year
  }
  this.props.getMediWallet({ data, history: this.props.history })
  this.setState({ page })
};

changeRowsPerPage = (perPage) => {
  let data = {
    page: 1,
    perPage: perPage,
    filter: '',
    searchText: '',
    month: this.state.month,
    year: this.state.year
  }
  this.props.getMediWallet({ data, history: this.props.history })
  this.setState({ page: 0, perPage })
}

componentWillReceiveProps = (nextProps) => {
  if(this.props.users != nextProps) {
    this.setState({wallet :nextProps && nextProps.users.wallet_balance })
  }
}
handleDownload = async(e, filter) => {
  e.preventDefault();
  filter();
  let data1 = {
    page: 1,
    perPage: this.state.perPage,
    filter: this.state.filter,
    month: this.state.month,
    year: this.state.year
  }
  let token = localStorage.getItem('buyer_token');
      await axios.request({
        method: 'POST',
        url:'transactions/downloadTransactionReport',
        data:data1,
        headers: {
            'Content-Type': 'application/json',
            'token': token
        },
        responseType:'Blob'
      }).then((result)=> {
        if(result.data.size != 0){
          NotificationManager.success('CSV file downloaded successfully !');
          const downloadUrl = window.URL.createObjectURL(new Blob([result.data]));
          const link = document.createElement('a');
          link.href = downloadUrl;
          link.setAttribute('download', 'report.csv'); 
          document.body.appendChild(link);
          link.click();
          link.remove();
        }
        else{
          NotificationManager.error('Something Went Wrong !')
        }
      })
        .catch(error => {
          NotificationManager.error('Something Went Wrong !')
        });
  this.props.getMediWallet({ data:data1, history: this.props.history })
}


    render() {

      const { mediWallet } = this.props;
      const {wallet} = this.state;


      const options = {
        filterType: 'dropdown',
        print: false,
        download: false,
        selectableRows: false,
        selectableRowsOnClick: false,
        selectableRowsHeader: false,
        responsive: 'scrollMaxHeight',
        search: false,
        viewColumns: false,
        rowHover: false,
        filter: true,
        rowsPerPage: this.state.perPage,
        page: this.state.page,
        serverSide: true,
	      count: mediWallet && mediWallet.details && mediWallet.details.length > 0 && mediWallet.details[0] && 
	      mediWallet.details[0].metadata[0] && mediWallet.details[0].metadata[0].total,
        fixedHeader: false,
        sort: false,
        server: true,
        selectableRows: 'none',
  
        textLabels: {
          filter: {
            all: "",
            title: "FILTERS",
          },
        },
  
        onTableChange: (action, tableState) => {
          switch (action) {
            case 'changePage':
              this.changePage(tableState.page);
              break;
            case 'changeRowsPerPage':
              this.changeRowsPerPage(tableState.rowsPerPage)
              break;
            case 'search':
              this.handleSearch(tableState.searchText)
              break;
          }
        },
        
      };
  

        const statusStyle = (status) => {
          return status.includes("Debit") ? "text-white bg-danger" : status.includes("Credit") ? "text-white bg-success" : "text-white bg-danger";
        }

        const {tabValue} = this.state

        const data = []

        mediWallet && mediWallet.details && mediWallet.details.length > 0 && mediWallet.details[0].data.map((dataOne,index) => {
          data.push({
            date:moment(dataOne.createdAt).format('DD/MM/YY HH:mm'),
            narration:dataOne.narration,
            type:dataOne.type,
            amount:(dataOne.settle_amount).toFixed(2),
            closingBal:(dataOne.closing_bal).toFixed(2),
            status:<div className={dataOne.status.toLowerCase() === 'success' ? 'text-white bg-success' : 'text-white bg-primary'}>{dataOne.status}</div>
          })
        })


        const columns = [
          {
            name: "date",
            label: "Date",
            options: {
              filter: false,
              sort: false,
              customHeadRender: (o, updateDirection) => {
                return customHead(o, updateDirection);
              }
            }
          },
          {
            name: "narration",
            label: "Narration",
            options: {
              filter: false,
              sort: false,
              customHeadRender: (o, updateDirection) => {
                return customHead(o, updateDirection);
              }
            }
          },
          {
            name: "type",
            label: "Type",
            options: {
              filter: false,
              sort: false,
              customHeadRender: (o, updateDirection) => {
                return customHead(o, updateDirection);
              }
            }
          },
          {
            name: "amount",
            label: "Amount",
            options: {
              filter: false,
              sort: false,
              customHeadRender: (o, updateDirection) => {
                return customHead(o, updateDirection);
              }
            }
          },
          {
            name: "closingBal",
            label: "Closing Bal",
            options: {
              filter: false,
              sort: false,
              customHeadRender: (o, updateDirection) => {
                return customHead(o, updateDirection);
              }
            }
          },
          {
            name: "status",
            label: "Status",
            options: {
              filter: true,
              filterType: 'custom',
              customHeadRender: (o, updateDirection) => {
                return customHead(o, updateDirection);
              },
              customFilterListRender: v => {
                return false;
              },
              filterOptions: {
                names: [],
                logic() {
                },
                display: (filterList, handleCustomChange, applyFilter, index, column) => {
                  return (
                  <CustomFilter 
                    onFilterChange={this.onFilterChange} 
                    handleApplyFilter={this.handleApplyFilter} 
                    applyFilter={applyFilter} 
                    handleResetFilter={this.handleResetFilter} 
                    month={this.state.month}
                    year={this.state.year}
                    handleDownload = {this.handleDownload}
                    filterFor='mediWallet'/>)
                },
              }
            }
          }
        ]
        

        
        return (
          <React.Fragment>

            <h1 style={{
              backgroundColor: "white",
              padding: "11px",
              marginBottom: "inherit",
              borderTopLeftRadius: "5px",
              borderTopRightRadius: "5px",
              fontWeight: 500,
              paddingLeft: "24px",
              paddingTop: "20px",
            }}>
              MediWallet
           </h1>
           
           <CustomScrollbars className="messages-list scrollbar" style={{ height: 650 }}>
           {this.props.loading ? 
              <div className="loader-view" style={{ textAlign: "center", marginTop: "30px" }}>
                <CircularProgress />
              </div>
            :
            <MUIDataTable
              title={<h3>{this.props.users === undefined ? '' : 'Wallet Balance: MNY ' + (this.state.wallet).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}) + '/-'}</h3>}
              data={data}
              columns={columns}
              options={options}
            />
           }
        </CustomScrollbars>

          </React.Fragment>
        );
      }
}
 
const mapStateToProps = ({seller,auth,buyer}) => {
  const { mediWallet, loading } = buyer;
  const { userDetails } = seller
  const { user_details } = auth;

  let users = userDetails ? userDetails : user_details
  return { mediWallet , user_details, users, loading };
}
export default withRouter(connect(mapStateToProps, {getUserDetail,getMediWallet})(MediWallet));
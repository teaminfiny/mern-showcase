import React, { Component } from 'react'
import { Col, Row } from 'reactstrap';
import moment from "moment";
import axios from '../../../../constants/axios';
import { NotificationManager } from 'react-notifications';
import { getUserDetail } from '../../../../actions/seller'
import { connect } from 'react-redux'
import CircularProgress from '@material-ui/core/CircularProgress';
import ComplianceFormComponent from './complianceFormComponent';
import ComplianceFormComponentForGst from './complainceFormComponentForGst';

function buildFileSelector() {
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    fileSelector.setAttribute('multiple', 'multiple');
    return fileSelector;
}

class ComplainceForm extends Component {
    constructor(props) {
        super(props);
        this.childRef = React.createRef()
        this.state = {
            fassaiLicExpiry: moment().add(2, 'M'),
            drugLic20BExpiry: moment().add(2, 'M'),
            drugLic20BExpiry: moment().add(2, 'M'),
            loader: false
        }
    }

    handleFileSelect = async (e, key) => {
        e.preventDefault();
        let document = "";
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = () => {
            document = reader.result;
            this.setState({ [key]: document })
        };
        reader.onerror = function (error) {
        };
        this.props = Object.assign({}, this.props, { key: document });
    }

    handledateChange = (value, key) => {
        this.setState({ [key]: value })
    }

    componentDidMount() {
        this.fileSelector = buildFileSelector();
        this.setState({
            drugLic21BExpiry: this.props.userDetails.drugLic21B ? this.props.userDetails.drugLic21B.expires ? moment(this.props.userDetails.drugLic21B.expires) : this.state.drugLic20BExpiry : this.state.drugLic20BExpiry,
            drugLic20BExpiry: this.props.userDetails.drugLic20B ? this.props.userDetails.drugLic20B ? moment(this.props.userDetails.drugLic20B.expires) : this.state.drugLic20BExpiry : this.state.drugLic20BExpiry,
            fassaiLicExpiry: this.props.userDetails.fassaiLic ? this.props.userDetails.fassaiLic ? moment(this.props.userDetails.fassaiLic.expires) : this.state.fassaiLicExpiry : this.state.fassaiLicExpiry,
            drugLic20B: this.props.userDetails.drugLic20B ? this.props.userDetails.drugLic20B.name : '',
            drugLic21B: this.props.userDetails.drugLic21B ? this.props.userDetails.drugLic21B.name : '',
            fassaiLic: this.props.userDetails.fassaiLic ? this.props.userDetails.fassaiLic.name : '',
            gstLic: this.props.userDetails.gstLic ? this.props.userDetails.gstLic.name : '',
        });

    }

    onSubmit = async (formdata) => {
        let data = {};

        data.drugLic20BExpiry = formdata.drugLic20BExpiry ? formdata.drugLic20BExpiry : this.state.drugLic20BExpiry;
        if (formdata.drugLic20B !== '' && formdata.drugLic20B.length > 200) {
            data.drugLic20B = formdata.drugLic20B;
        }

        data.drugLic21BExpiry = formdata.drugLic21BExpiry ? formdata.drugLic21BExpiry : this.state.drugLic21BExpiry;
        if (formdata.drugLic21B !== '' && formdata.drugLic21B.length > 200) {
            data.drugLic21B = formdata.drugLic21B;

        }

        if (formdata.fassaiLic !== undefined && formdata.fassaiLic !== '' && formdata.fassaiLic.length > 200) {
            data.fassaiLic = formdata.fassaiLic;
            data.fassaiLicExpiry = formdata.fassaiLicExpiry ? formdata.fassaiLicExpiry : this.state.fassaiLicExpiry;
        }

        if (formdata.gstLic !== undefined && formdata.gstLic !== '' && formdata.gstLic.length > 200) {
            data.gstLic = formdata.gstLic
        }

        if (this.state.gstLic !== undefined && formdata.gstLic === '') {
            data.gstLic = formdata.gstLic;
        }

        if (this.state.fassaiLic !== undefined && formdata.fassaiLic === '') {
            data.fassaiLic = formdata.fassaiLic;
            if (this.state.fassaiLicExpiry !== undefined && formdata.fassaiLicExpiry === '') {
                data.fassaiLicExpiry = formdata.fassaiLicExpiry;
            }
        }

        data.update = "ComplainceForm"
        if (Object.keys(data).length > 0) {
            this.setState({ loader: true })
            await axios.post('/users/editUserProfile', data, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('buyer_token')
                }
            }
            ).then(result => {
                this.setState({ loader: false })
                if (result.data.error) {
                    NotificationManager.error(result.data.title);
                } else {
                    NotificationManager.success('Complaince details updated successfully.');
                    this.props.getUserDetail({ history: this.props.history })
                }
            })
                .catch(error => {
                    this.setState({ loader: false })
                    NotificationManager.error('Something went wrong, Please try again')
                });
        }
        else {
            // NotificationManager.error('');
        }
    }
    drugLic20B = ()=>{
        // this.childRef.current.ref.current.click()
    }
    render() {
        const { handleSubmit, previousPage } = this.props;
        const { drugLic20BExpiry, drugLic21BExpiry, fassaiLicExpiry, loader } = this.state;
        return (
            <div className="col-xl-12 col-lg-12">
                {/* <ContainerHeader match={this.props.match} title={<IntlMessages id="Complaince Forms" />} /> */}
                <div className="jr-card">
                    {/* <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off"> */}
                        <Row style={{ }}>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                
                                <ComplianceFormComponent fileKey={'drugLic20B'} label2='drugLic20BLicNo' label="Drug Lic 20B" name="drugLic20BExpiry"/>
                                {/* <span> <a download href={``}> View</a></span> */}
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <ComplianceFormComponent fileKey={'drugLic21B'} label2 = 'drugLic21BLicNo' label="Drug Lic 21B" name="drugLic21BExpiry"/>
                                
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-4">
                                <ComplianceFormComponent fileKey={'fassaiLic'}  label2 = 'fassaiLicNo' label="FSSAI Lic (Optional)" name="fassaiLicExpiry"/>
                                
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-4">
                                <ComplianceFormComponentForGst fileKey={'gstLic'}  label2 = 'gstLicNo' label="GSTN Lic (Optional)" name="gstLic"/>
                               
                            </Col>
                        </Row>
                        
                        <div>
                            {
                                loader &&
                                <div className="loader-view">
                                    <CircularProgress />
                                </div>
                            }
                        </div>
                    {/* </form> */}
                </div>
            </div >
        )
    }
}

const mapStateToProps = ({ seller }) => {
    const { userDetails } = seller;
    return { userDetails }
};

export default connect(mapStateToProps, { getUserDetail })(ComplainceForm);

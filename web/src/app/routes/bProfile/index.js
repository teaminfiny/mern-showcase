import React, { Component } from "react";
import UserProfileCard from './userProfileData';
import Interests from "components/wall/Interests/index";
import Orders from 'components/BOrders'
import {  interestList } from "./data";
import Transaction from 'components/BTransactions'
import { connect } from 'react-redux';
import EditProfile from './editProfile';
import MediWallet from './MediWallet';
import './index.css'
import ComplainceForm from "./bCompliance/ComplainceForm";
import CircularProgress from '@material-ui/core/CircularProgress';

import { getUserDetail } from 'actions'
import { getReviewList, getTransactionList, getMediWallet } from 'actions/buyer';
import ProductRequest from 'components/BProductRequests';


class Wall extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: 'Edit Profile',
      avg: ''
    }
    this.myRef = React.createRef()
  }

  changeTab = (value) => {
    this.setState({ title: value })
    this.props.getUserDetail({userType:"buyer"});
  }

  componentDidMount() {
    // const isUserLoggedIn = localStorage.getItem("buyer_token");
    // if (isUserLoggedIn !== null) {
    //   this.props.getUserDetail({ history: this.props.history });
    // }
    if (this.props.location.title && this.props.location.title === 'MediWallet') {
      this.setState({ title: this.props.location.title })
    }
    if (this.props.location.state && this.props.location.state === "/MyCart") {
      this.setState({ title: 'Orders' })
    }
    if(this.props.match.path === "/view-seller/:id"){
      let data = {
        sellerId: this.props.match && this.props.match.params && this.props.match.params.id,
        sellerReviewList: true
      }
    this.props.getReviewList({data})
    }
    else{
    this.props.getReviewList({})
    }
    // let data = {
    //   page: 1,
    //   perPage: 50,
    //   filter: '',
    //   month: moment().format("MMMM"),
    //   year: moment().format("GGGG")
    // }
    // this.props.getTransactionList({ data, history: this.props.history })
    // this.props.getMediWallet({history:this.props.history, data})
    this.myRef.current.scrollIntoView({ behavior: 'smooth' })
  }




    dataFromChild = (avgReview) => {
    this.setState({avg: avgReview})
  }

  render() {
    const { title } = this.state;

    const { users, reviewList, transactionListData, mediWallet } = this.props;

    return (
      <div className="app-wrapper" ref={this.myRef}>
        {users !== "" ? <div className="jr-main-content">
          <div className="row">
            <div className=" col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 p-0 mt-1">
              {/* <CustomScrollbars className="jr-wall-scroll scrollbar BProfile"> */}


              <UserProfileCard
                styleName="pb-4"
                headerStyle="bg-gradient-primary"
                DataFromParent={users}
              />

              <Interests interestList={interestList} changeTab={this.changeTab} title={title} userDataFromParent={users}/>



              {/* </CustomScrollbars> */}
            </div>

            <div className="col-xl-9 col-lg-9 col-md-6 col-sm-6 col-12">

              {/* <CustomScrollbars className="jr-wall-scroll scrollbar"> */}

              <div className="jr-wall-postlist ">
                {
                  title == 'Edit Profile' &&
                  <EditProfile 
                  userDataFromParent={users}/>
                }

                {
                  title === 'Product Requests' &&
                  <ProductRequest />
                }
                

                {
                  title == 'Orders' &&
                  <Orders />
                }
                
                {
                  // transactionListData && transactionListData.details && transactionListData.details[0].data && transactionListData.details[0].data.length > 0 ? 
                                  
                  title == 'Transactions' &&
                  // <div className="jr-card jr-full-card"> 
                  <Transaction />
                //   {/* </div>
                
                // :
                // '' */}
                }



                {
                  // mediWallet && mediWallet.details && mediWallet.details[0].data && mediWallet.details[0].data.length > 0 ?
                    title === 'MediWallet' &&
                    // <div className="jr-card jr-full-card ">
                      <MediWallet
                        userDataFromParent={users} />
                    // {/* </div>
                    // :
                    // '' */}
                }

                {
                  title === 'Compliance' &&
                  <ComplainceForm />
                }
              </div>

              {/* </CustomScrollbars> */}

            </div>
          </div>
        </div> : <div className="loader-view">
            <CircularProgress />
          </div>}
      </div>
    )
  }
}

const mapStateToProps = ({ auth, seller, buyer }) => {

  const { userDetails } = seller
  const { user_details } = auth;
  const { reviewList, transactionListData, mediWallet } = buyer;
  let users = userDetails ? userDetails : user_details
  return { user_details, users, reviewList, transactionListData, mediWallet}
};

export default connect(mapStateToProps, { getUserDetail, getReviewList, getTransactionList, getMediWallet })(Wall);

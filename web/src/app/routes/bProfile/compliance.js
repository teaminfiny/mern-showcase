import React, { Component } from "react";
import Button from '@material-ui/core/Button';
import customHead from 'constants/customHead'
import { withRouter } from 'react-router-dom'
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const columns = [
  {
    name: "title",
    label: "Title",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "action",
    label: "Action",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
];




const options = {
  filterType: 'dropdown',
  filter: false,
  print: false,
  download: false,
  selectableRows: 'none',
  selectableRowsOnClick: false,
  selectableRowsHeader: false,
  responsive: 'scrollMaxHeight',
  search: false,
  viewColumns: false,
  customFooter: (
    count,
    page,
    rowsPerPage,
    changeRowsPerPage,
    changePage
  ) => {
    return <div changePage={changePage} count={count} />;
  },
  textLabels: {
    filter: {
      all: "All",
      title: "FILTERS",
    },
  }
};

class Complaince extends Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  handleTrackOrderClick = (e) => {
  }

  button = (title) => (
    <div className={'action'}>
      <React.Fragment>
        <ButtonGroup color="primary" aria-label="outlined primary button group">
          <Button variant="outlined" className={'text-warning'} onClick={(e) => this.handleTrackOrderClick(e)} >
            View
                </Button>
          <Button variant="outlined" onClick={(e) => this.handleTrackOrderClick(e)} className={'text-primary'}>
            Upload
                </Button>
        </ButtonGroup>
      </React.Fragment>
    </div>
  )

  render() {


    const data = [
      {
        title: '20B', expiry:'22/10/2020', action: this.button('20B')
      },
      { title: '21B', expiry:'22/10/2020', action: this.button('21B') },
      { title: 'GST', expiry:'N/A', action: this.button('GST') },
      { title: 'FSSI', expiry:'22/10/2020', action: this.button('FSSI') },
    ];

    return (
      <React.Fragment>
        <Paper className={'tableRoot'}>
          <Table className={'tableMain1'} aria-label="spanning table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Title</TableCell>
                <TableCell align="left">Expiry</TableCell>
                {/* <TableCell align="left">Quantiy</TableCell> */}
                <TableCell align="left">Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map(data => (
                <TableRow key={data.title}>
                  <TableCell align="left">{data.title}</TableCell>
                  <TableCell align="left">{data.expiry}</TableCell>
                  {/* <TableCell align="left">{data.qty}</TableCell> */}
                  <TableCell align="left">{data.action}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </React.Fragment>
    );
  }
}

export default withRouter(Complaince);
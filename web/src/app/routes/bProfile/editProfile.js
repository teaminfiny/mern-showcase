import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Widget from "components/Widget/index";
import EditProfileForm from 'components/BEditProfile'
import Password from 'components/BChangePassword'
import SwipeableViews from 'react-swipeable-views';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Complaince from './compliance';

function TabContainer({ children, dir }) {
  return (
    <div dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </div>
  );
}
TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};
class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      activeIndex: 0,
      card: [{
        number: '4242424242424242',
        name: 'Avanish',
        expiry: '0429',
        cvc: '852'
      }, {
        number: '5555555555554444',
        name: 'Avanish',
        expiry: '0429',
        cvc: '852'
      }, {
        number: '378282246310005',
        name: 'Avanish',
        expiry: '0429',
        cvc: '852'
      }]
      
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }
  handleChange = (event, value) => {
    this.setState({ activeStep: value });
  };
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.card.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.card.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }
  handleChangeIndex = index => {
    this.setState({ activeStep: index });
  };
  render() {
    const { activeStep, card } = this.state
    const { theme } = this.props;

    const {userDataFromParent} = this.props;

    
    return (

      <div className="jr-profile-content w-100">
        <div className="row">
          <div className="col-xl-12 col-lg-12 col-md-12 col-12">
            <Widget styleName="jr-card-full jr-card-tabs-right jr-card-profile" >
              <AppBar position="static" color="default" indicatorcolor='primary' >
                <Tabs 
                textColor='primary' 
                indicatorcolor='primary' 
                centered className="jr-tabs-center" 
                value={activeStep} 
                onChange={this.handleChange} 
                variant="scrollable" 
                scrollButtons="auto" >

                  
                  <Tab textColor='primary' className="profileTab" label="Profile"  />
                  
                  <Tab textColor='primary' className="profileTab" label="Password" />


                </Tabs>
              </AppBar>


              
              <div className="jr-tabs-classic">
                <div className={activeStep === 3 ? "jr-tabs-content jr-task-list p-0":"jr-tabs-content jr-task-list"}>
                  <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={this.state.activeStep}
                    onChangeIndex={this.handleChangeIndex}
                  >

                    <TabContainer dir={theme.direction} >
                      <EditProfileForm 
                      className='mt-2' 
                      userDataFromParentTwo={userDataFromParent}/>
                    </TabContainer>

                    <TabContainer dir={theme.direction} >
                      <Password className='mt-2' />
                    </TabContainer>

                    <TabContainer dir={theme.direction}>
                      <Complaince/>
                    </TabContainer>
                  </SwipeableViews>
                </div>
              </div>
              
            </Widget>
          </div>



        </div>
      </div>

    );
  }
}

export default withStyles(null, { withTheme: true })(EditProfile);
import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import helperFunction from '../../../constants/helperFunction';

class UserProfileCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      gradient : ''
    }
  }
  componentDidMount = () => {
    this.setState({ gradient : helperFunction.gradientGenerator()})
  }

  render() {
    let { gradient } = this.state;
    return (
      <React.Fragment>
        <div className="jr-card user-detail-card">
          <div className="user-img-container">
            {/* <img className="user-img" alt="AV" src={require('assets/images/Anjali-Sud.jpg')} /> */}
            <Avatar
              alt={''}
              src={''}
              className={`user-avatar`}
              style={{background:gradient}}
            > {helperFunction.getNameInitials(`${this.props.DataFromParent.first_name} ${this.props.DataFromParent.last_name}`) }</Avatar>
          </div>
  
          <div className="jr-card-body d-flex flex-column justify-content-center">
            <h4 className="mb-0"> {`${this.props.DataFromParent.first_name}`} {this.props.DataFromParent.last_name}</h4>
            <span className="d-block jr-fs-13 mb-1">{this.props.DataFromParent.email}</span>
            <span className="d-block jr-fs-sm text-grey">{this.props.DataFromParent.phone}</span>
          </div>
        </div>
      </React.Fragment>
    )
  }
};

export default UserProfileCard;


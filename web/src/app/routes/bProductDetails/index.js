import React, { Component } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Widget from "components/Widget/index";
import CourselProduct from 'components/BCourselProduct'
import { Col, Row } from 'reactstrap'
import { Carousel, CarouselControl, CarouselItem } from 'reactstrap';
import ProductTab from 'components/bproductDetailsTab';
import helpertFn from 'constants/helperFunction';

import { connect } from 'react-redux';

import {
  getProductDetails,
} from 'actions/buyer';

import './index.css'
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';



class BProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: '',
      loading: false,
      activeIndex: 0,
      productData:[],
      tileData:[],
      imageData:[]
    }
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.myRef = React.createRef()
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.imageData.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous(product) {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.imageData.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  componentDidMount = () => this.handleScroll()

  // componentDidUpdate = () => this.handleScroll()

  handleScroll = () => {
    const { index, selected } = this.props
    if (index === selected) {
      this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }

  handleImageChange = (e, product) => {
    this.setState({ image: product.img })
  }

  getImage = () => this.state.imageData.map((product, index) => <div key={index} className='col-sm-3 col-md-3 col-lg-3 sliderImageDiv'>
    <img src={`${helpertFn.productImg(product.img)}`} onClick={(e) => this.handleImageChange(e, product)} style={{ padding: '3.5px 3.5px 0px 0px' }} onError={this.addDefaultSrc} />
  </div>)




  componentDidMount() {
    this.props.getProductDetails({ inventory_id: this.props.match.params.id,history:this.props.history })
  }

  componentDidUpdate = async(prevProps, prevState)=> {
    if(prevProps.productData!==this.props.productData){
      let image = this.props.productData[0].Product.images.length>0?this.props.productData[0].Product.images[0]:logo
      let tileData = []
      let imageData = []
      await this.props.productData[0].Product.images.map((dataOne, index)=>{
        imageData.push({ img: dataOne ,
          title: 'coffee',
          author: 'jill111',
          featured: true,})
      })
      await this.setState({image,productData: this.props.productData,imageData})
    }
    this.handleScroll()
  }
  
  addDefaultSrc(ev){
    ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
  }
  
  render() {

    const { otherSellers, relatedProducts, data } = this.props;
    const{productData} = this.state
    console.log('asdocmeoiugvmae', productData)
    
    const settings = {
      arrows: false,
      dots: true,
      infinite: true,
      speed: 500,
      marginLeft: 10,
      marginRight: 10,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    

    const options1 = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 950,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
          }
        },
        {
          breakpoint: 560,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
          }
        }
      ]
    };

    const { activeIndex, image} = this.state;

    const slides = this.state.imageData.map((product, index) => {
      return (
        <CarouselItem key={index}>
          <div className='d-flex'>

            {this.getImage()}

          </div>

        </CarouselItem>
      );
    });

    return (
      <div className='app-wrapper' ref={this.myRef}>
        
        <Widget styleName="jr-card-full" >
          <div className="row mb-0" >
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 box">             
             
              <div className="productSlider ml-3 mt-3">
                <Carousel
                  autoPlay={false}
                  indicators={true}
                  activeIndex={activeIndex}
                  next={this.next}
                  interval={false}
                  previous={this.previous}
                  className='itemImageProductDetail'
                 
                 >                   

                <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} className="arrows" />
                  {slides}
                  <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} className="arrows" />
                </Carousel>
              </div>

              <div className="jr-slick-slider-lt-thumb content" style={{margin:'auto', width:'60%', height:'auto'}} >
                <img                
                className={this.state.loading ? 'fadeout img-fluid' : 'fadein img-fluid'} src={`${helpertFn.productImg(image)}`}
                alt="..." 
                onError={this.addDefaultSrc}
                />
              </div>
              { productData && productData[0] && productData[0].Product && productData[0].Product.description &&
                <div className="row mb-0 mt-4 w-60" >
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 box">  
                  <div className="jr-slick-slider-lt-thumb content" style={{margin:'auto', width:'60%', height:'auto'}}>
                    <p style={{color:"#202790",marginBottom:"1px"}}>Description :</p>
                    <span>{productData && productData[0] && productData[0].Product && productData[0].Product.description}</span>
                  </div>
                </div>
              </div> }
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
              <ProductTab />
            </div>
          </div>
        </Widget>
        {
          relatedProducts&&relatedProducts.length > 0 ?

            <Row className='caroselColor'>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center">
                  <h2 className="title mb-3 mb-sm-0">Related Products</h2>
                </div>

                <h1 className="mb-3 mt-3 text-left"></h1>

                <CourselProduct
                  key={'relatedProducts'}
                  data={relatedProducts}
                  history={this.props.history}
                />

              </Col>
            </Row>
            :
            null
        }
      </div>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { data, otherSellers, relatedProducts, productData} = buyer;
  return {  data, otherSellers, relatedProducts, productData }
};

export default connect(mapStateToProps, {getProductDetails})(BProductDetails);




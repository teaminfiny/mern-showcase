import React, { Component } from 'react';
import Product from 'components/BProductList';
import { Col, Row } from 'reactstrap'
import ProductData from './productData'
import CourselProduct from 'components/BCourselProduct'
import { Carousel, CarouselControl, CarouselIndicators, CarouselItem } from 'reactstrap';
import CourselProductsForYou from 'components/BCourselProductsForYou'
import './index.css'                        
import './secondaryHeader.css'
import { NavLink, withRouter } from 'react-router-dom';
import FeaturedBCoursel from './FeaturedBCoursel'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import {  getFeaturedProductList } from 'actions/buyer';
import {  getProductsForYou } from 'actions/buyer';
import { getPromotionsListing } from 'actions/buyer'
import {Helmet} from "react-helmet";


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      activeIndex: 0, 
      activeIndex2: 0,
      dashboardModal: false,
      BannerDataAPI: []
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);

    this.next2 = this.next2.bind(this);
    this.previous2 = this.previous2.bind(this);
    this.goToIndex2 = this.goToIndex2.bind(this);
    this.onExiting2 = this.onExiting2.bind(this);
    this.onExited2 = this.onExited2.bind(this);
  }
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.BannerDataAPI.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.BannerDataAPI.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }


  onExiting2() {
    this.animating = true;
  }

  onExited2() {
    this.animating = false;
  }

  next2() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex2 === this.state.BannerDataAPI.length - 1 ? 0 : this.state.activeIndex2 + 1;
    this.setState({ activeIndex2: nextIndex });
  }

  previous2() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex2 === 0 ? this.state.BannerDataAPI.length - 1 : this.state.activeIndex2 - 1;
    this.setState({ activeIndex2: nextIndex });
  }

  goToIndex2(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex2: newIndex });
  }


  componentDidMount(){
    this.props.getFeaturedProductList({ history: this.props.history })
    
    const isUserLoggedIn = localStorage.getItem("buyer_token");
    if (isUserLoggedIn !== null) {
      this.props.getProductsForYou({ history: this.props.history })
    }

    if(isUserLoggedIn == null) {
    this.setState({dashboardModal: true})
    const modalState = setTimeout(() =>{
      this.setState(({dashboardModal: false}))
    },4000)}
    
    this.props.getPromotionsListing({ history: this.props.history })
  }

  handleClose = () => {
    this.setState({dashboardModal:false})
  }

  scrollToDeals = () => {
    this.refs.dealsOfTheDay.scrollIntoView({behavior: "smooth"}); // scroll..
  }

  scrollToFast = () => {
    this.refs.fastMovingProducts.scrollIntoView({behavior: "smooth"}); // scroll..
  }
  toSearch1 = () => {
    this.props.history.push('/search-results/category/search=&company=&seller=&discount=')
  }
  toSearch = (type) => {
    this.props.history.push(`/search-results/category/medicineType=${type}&search=&company=&seller=&discount=`)
  }
  scrollToProductsForYou = () => {
    this.refs.productsForYou.scrollIntoView({behavior: "smooth"}); // scroll..
  }
  
  redirectTo = (e) => {
    if(e !== ''){
      window.open(`${e}`,'_blank');
    }
  }

  componentDidUpdate = async(prevProps, prevState)=> {
    if(prevProps.promotionsList !== this.props.promotionsList){
      const { promotionsList } = this.props;
      this.setState({ BannerDataAPI: promotionsList });  
    }
  }

  render() {

    const { data, featured, bannerArr, dealsArr, fastMovingArr, jumboArr } = this.props;
    const { productsForYou, visitedProduct, promotionsList } = this.props;
    const { activeIndex, activeIndex2, BannerDataAPI} = this.state;

    
    
    const slides = ProductData.map((product, index) => {
      return (
        <CarouselItem
          key={index}
          onExiting={this.onExiting}
          onExited={this.onExited}>
          <Product key={index} showAddToCard={true} history={this.props.history} classValue="col-xl-2 col-md-2 col-sm-2 col-12" />
        </CarouselItem>
      );
    });

    const slides2 = BannerDataAPI && BannerDataAPI.map((banner, i) => {
      return <CarouselItem
        key={i}
        onExiting={this.onExiting2}
        onExited={this.onExited2}>
        <img style={{cursor: "pointer"}} className="app-logo-img" src={banner.image_banner} alt={banner.name} onClick={(e) => this.redirectTo(banner.redirect_url)}/>
      </CarouselItem>
    })



    const productsForYouArray = productsForYou && productsForYou.visitedProduct


    // const shuffled =  featured && featured.sort(() => 0.5 - Math.random());

    return (
      <div className='dashboard animated slideInUpTiny animation-duration-3 scrollBehavior'>
        <Helmet>
          <title>B2B Online Wholesale Medicine Supply For Pharmacy - Medimny</title>
          <meta name="title" content="B2B Online Wholesale Medicine Supply For Pharmacy - Medimny" />
          <meta name="description" content="Medimny is a B2B E-Commerce platform for Pharmaceutical Products for Distributors, Stockists and Retail Drug Stores serving entire nation." />
        </Helmet>
        <div className="secondaryHeaderWrapper">
          <div className="secondaryHeaderMain">
            {/* <h5 className="dealsOfTheDay" style={{cursor: "pointer", paddingLeft: "30px", float: "left"}}>
              <a href='' target='_blank' className="capsuleTwo">Getting Started</a>
            </h5> */}

            <h5 className="dealsOfTheDay" style={{cursor: "pointer", paddingLeft: "30px", float: "left"}}>
              <a onClick={this.scrollToDeals} className="capsuleTwo">Deals Of The Day</a>
            </h5>

            <h5 className="dealsOfTheDay" style={{cursor: "pointer", float: "left"}}>
              <a onClick={this.scrollToFast} className="capsuleTwo">Fast Moving Products   </a>
            </h5>

            <h5 className="dealsOfTheDay" style={{cursor: "pointer", float: "left"}}>
              <a onClick={this.toSearch1} className="capsuleTwo">Show All Deals   </a>
            </h5>

            <h5 className="dealsOfTheDay" style={{cursor: "pointer", float: "left"}}>
              <a onClick={() => this.toSearch('Ethical Branded')} className="capsuleTwo">{'Ethical Branded & PCD   '}</a>
            </h5>

            <h5 className="dealsOfTheDay" style={{cursor: "pointer", float: "left"}}>
              <a onClick={() => this.toSearch('Cool Chain')} className="capsuleTwo">Cool Chain   </a>
            </h5>

            <h5 className="dealsOfTheDay" style={{cursor: "pointer", float: "left"}}>
              <a onClick={() => this.toSearch('Generic')} className="capsuleTwo">{'Generic & Surgical   '}</a>
            </h5>



            <h5 className="sellOnMedideals"  style={{paddingRight: "30px", float: "right"}}>
              <NavLink to={"/signup/"} className="capsule" target="_blank" style={{cursor: "pointer"}}>Sell On Medimny </NavLink>  | <i class="zmdi zmdi-phone ml-1 mr-1"></i>
              <span style={{fontWeight:'bold'}}>+91 9321927004</span></h5>
            <h5 className="dealsOfTheDay" ></h5>
          </div>
        </div>

        {BannerDataAPI && BannerDataAPI.length > 0 ?
          <Carousel
            activeIndex={activeIndex2}
            next={this.next2}
            previous={this.previous2}>

            <CarouselIndicators items={BannerDataAPI && BannerDataAPI} activeIndex={activeIndex2} onClickHandler={this.goToIndex2} />
            {slides2}
            <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous2} />
            <CarouselControl direction="next" directionText="Next" onClickHandler={this.next2} />
          </Carousel>
          :
          null
        }

        <div style={{textAlign:'center'}}>
          <h1 className="title text-primary mb-0 mt-3 font-weight-bold">India's Leading B2B E-Commerce of Pharmaceutical Products Hub</h1>
        </div>
        <div className="app-wrapper pt-3">


          <Row className='caroselColor'>

            <Col xs={12} sm={12} md={12} lg={12} xl={12}>

              {featured && featured.length >= 1 ?
                <div ref="featured">
                  <FeaturedBCoursel
                    key={'featured'}
                    data={featured}
                    history={this.props.history}
                    identifier={'featured'}
                  />
                </div>
                 : 
                null
              } 
              {dealsArr.length >= 1 ?
                <div ref="dealsOfTheDay">
                  <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center" style={{marginBottom :'15px'}}>
                    <h2 className="title mb-0 mb-sm-0">Deals of the day</h2>
                  </div>
                  <CourselProduct
                    key={'dealsArr'}
                    data={dealsArr}
                    history={this.props.history}
                    identifier={'dealsArr'}
                  />
                </div>
                :
                null 
              }
              {fastMovingArr.length >= 1 ?
                <div ref="fastMovingProducts">
                  <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center" style={{margin :'0px'}}>
                    <h2 className="title mb-0 mb-sm-0">Fast moving products</h2>
                  </div>
                  <h1 className="mb-3 mt-3 text-left"></h1>
                  <CourselProduct
                    key={'fastMovingArr'}
                    data={fastMovingArr}
                    history={this.props.history}
                    identifier={'fastMovingArr'}
                  />
                </div>
                : 
                null
              }
              { jumboArr && jumboArr.length >= 1 ?
                <div ref="jumboProducts">
                  <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center" style={{margin :'0px'}}>
                    <h2 className="title mb-0 mb-sm-0">Jumbo deals</h2>
                  </div>
                  <h1 className="mb-3 mt-3 text-left"></h1>
                  <CourselProduct
                    key={'jumboProducts'}
                    data={jumboArr}
                    history={this.props.history}
                    identifier={'jumboArr'}
                  />
                </div>
                : 
                null
              }
              {productsForYouArray && productsForYouArray.length >= 1 ?
                <div ref="productsForYou">
                  <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center" style={{margin :'0px'}}>
                    <h2 className="title" style={{margin:'0px'}}>Products for you</h2>
                  </div>
                  <h1 className="mb-3 mt-3 text-left"></h1>
                  <CourselProductsForYou
                    key={'CourselProductsForYou'}
                    keys={'CourselProductsForYou'}
                    dataFromParent={productsForYouArray}
                    history={this.props.history}
                    identifier={'CourselProductsForYou'}
                  />
                </div>
                : 
                null
              }
            </Col>

          </Row>
        </div>
{/* -------------------------- TrackOrder --------------------------------------- */}

        <Dialog open={this.state.dashboardModal} onClose={this.handleClose} fullWidth={true}>
          <DialogContent style={{ overflow: "hidden" }}>
              <DialogTitle className="" id="alert-dialog-title" style={{ textAlign: "center",paddingBottom:'0px' }}>

                {/* <span className="text-success mb-3">
                  <i class="zmdi zmdi-check-circle animated fadeInUp zmdi-hc-5x"></i>
                </span> */}
                <h1 className="mt-4 font-weight-bold">
                Welcome to India’s Fastest growing B2B marketplace for
                Pharmaceutical Products.
                </h1>
              </DialogTitle>
              <h1  style={{ textAlign: "center" }}>Welcome to <span style={{fontSize:'xx-large', fontWeight:'bold'}}>"Medimny"</span></h1>
            </DialogContent>
           
          <DialogActions>
            <Button onClick={this.handleClose} color="secondary" autoFocus> Close </Button>
          </DialogActions>

        </Dialog>
      </div>
    );
  }
}


const mapStateToProps = ({ buyer }) => {
  const { featured,bannerArr,dealsArr,fastMovingArr, jumboArr, data, productsForYou, visitedProduct, promotionsList } = buyer;
  return { featured,bannerArr,dealsArr,fastMovingArr, jumboArr, data, productsForYou, visitedProduct, promotionsList }
};

export default withRouter(connect(mapStateToProps, {getFeaturedProductList, getProductsForYou, getPromotionsListing})(Dashboard));
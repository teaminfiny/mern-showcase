import React, { Component } from 'react';
import { Row } from 'reactstrap'
import Slider from "react-slick";
import ProductData from './productData'
import BDashboardCategory from 'components/BdashboardCategory'

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0, activeIndex2: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);


  }
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === ProductData.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? ProductData.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }


  onExiting2() {
    this.animating = true;
  }

  onExited2() {
    this.animating = false;
  }




  goToIndex2(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex2: newIndex });
  }

  

  render() {

    
    const options1 = {
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      pauseOnHover: true,
      responsive: [
        {
          breakpoint: 950,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false
          }
        },
        {
          breakpoint: 560,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false
          }
        }
      ]
    };

    const {identifier} = this.props;

    return (

      <div>

        {this.props.data && this.props.data.length < 4 ?
          <Row>
            {
              this.props.data && this.props.data.map((value, index) => {
              return (value && value.data.length >= 3 ?
              <div className="slick-slide-item col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3" key={index}>

                <BDashboardCategory 
                  styleName="pb-4" 
                  headerStyle="bg-gradient-primary" 
                  value={value} 
                  index={index} 
                  data={this.props.data} 
                  identifier={identifier}
                />

              </div> :'')})
            }

          </Row>

          :

          <Slider className="slick-slider-cr" {...options1}>
            {
              this.props.data && this.props.data.map((value, index) => {
              return (value && value.data.length >= 3 ?
              <div className="slick-slide-item " key={index}>
                 
                <BDashboardCategory 
                key = {index}
                styleName="pb-4" 
                headerStyle="bg-gradient-primary" 
                value={value} 
                index={index} 
                data={this.props.data} 
                identifier={identifier}
                /> 

              </div>:'')
            })}
          </Slider> 
         }

      </div>

    );
  }
}

export default Dashboard;

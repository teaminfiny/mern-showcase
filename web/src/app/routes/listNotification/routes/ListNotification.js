import React, { Component } from 'react';
import ListItem from 'app/routes/customViews/routes/Component/ListItem';
import { getNotification } from '../../../../actions/notification';
import { connect } from 'react-redux';
import axios from '../../../../constants/axios'
import InfiniteScroll from 'react-infinite-scroll-component';
import CircularProgress from '@material-ui/core/CircularProgress';
import AxiosRequest from 'sagas/axiosRequest'

class ListNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetch: true,
      page: 1,
      perPage:20,
      items: '',
      dataL:10
    }
  }

  componentDidMount = async () => {
    // this.props.getNotification({ history: this.props.history, data: {} });
    let user = this.props.match.path.includes('seller') ? 'seller' : 'buyer';
    let data = {
      page: this.state.page,
      perPage: 20,
      user_type: user
    }
    if(user =='buyer'){
      let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'users/getNotification', '', data)
      console.log('asdanoifaf',response,data,response && response.data &&response.data)
      if (response.data.error) {
      } else {
        this.setState({ items: response.data.detail });
      }
    }else if(user =='seller'){
      let response = await AxiosRequest.axiosHelperFunc('post', 'users/getNotification', '', data)
      console.log('asdanoifaf',response,data,response && response.data &&response.data)
      if (response.data.error) {
      } else {
        this.setState({ items: response.data.detail });
      }
    }
    
    await axios.post('/users/markNotificationRead', {}, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }
    ).then(result => {
      if (result.data.error) {
      } else {
      }
    })
      .catch(error => {
      });
  }
  fetchData1 = async () => {
    console.log("notificatioasdasdnData-")
    let user = this.props.match.path.includes('seller') ? 'seller' : 'buyer';
    let { total } = this.props.notiData;
    if (this.props.notificationData.length >= total) {
      this.setState({ fetch: false });
      return;
    }
    let data = {
      page: this.state.page,
      perPage: this.state.perPage,
      user_type: user
    }
    if(user =='buyer'){
    let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'users/getNotification', '', data)
    if (response.data.error) {
    } else {
      this.setState({ items: this.state.items.concat(response.data.detail), dataL: this.state.dataL + 10 });
      await this.setState({ page: this.state.page + 1 });
    }}
    else if(user =='seller'){
      let response = await AxiosRequest.axiosHelperFunc('post', 'users/getNotification', '', data)
      if (response.data.error) {
      } else {
        this.setState({ items: this.state.items.concat(response.data.detail), dataL: this.state.dataL + 10 });
        await this.setState({ page: this.state.page + 1 });
      }
    }
  };
  render() {
    let { notificationData, notiData } = this.props;
    let { items, fetch, dataL } = this.state;
    let outOfPage = notiData.total > 0 ? this.state.page > Math.round(notiData.total/this.state.perPage)  : true;
    return (
      <div className="col-xl-12 col-lg-12" style={{height: 500 }}>
        <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center" style={{ marginBottom: '15px' }}>
          <h2 className="title mb-0 mb-sm-0">Notifications</h2>
        </div>
        <div className='jr-card' id='scrollableDiv1' style={{ height: 450, overflow: 'auto', display: 'flex' }}>
        <InfiniteScroll
      dataLength={dataL} 
      next={this.fetchData1}
      hasMore={fetch}
      loader={ !outOfPage && <p style={{ textAlign: 'center', width:'100%' }}>
          <CircularProgress size={40}/>
      </p>}
      scrollableTarget="scrollableDiv1"
      endMessage={
        <p style={{ textAlign: 'center' }}>
          <b>Yay! You have seen it all</b>
       </p> }>
            {items && items.length > 0 ? items.map((data, index) => (
              <ListItem key={index} data={data} styleName="card-strip" />
            )) : <p>No Notifications found</p>}
            </InfiniteScroll>
        </div >
      </div>
    );
  }
}


const mapStateToProps = ({ notification }) => {
  const { notificationData, notiData } = notification;
  return { notificationData, notiData }
};

export default connect(mapStateToProps, { getNotification })(ListNotification);
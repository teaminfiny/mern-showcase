import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from '../../../util/asyncComponent';

const ListNotification = ({match}) => (
  <div className="app-wrapper">
    <Switch>
      <Redirect exact from={`${match.url}/list-Notification`} to={`${match.url}/`}/>
      <Route path={`${match.url}/`} component={asyncComponent(() => import('./routes/ListNotification'))}/>
      <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/>
    </Switch>
  </div>
);

export default ListNotification;

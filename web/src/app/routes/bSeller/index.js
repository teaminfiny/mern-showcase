import React, { Component } from "react";
import About from "components/profile/About/index";
import Contact from "components/profile/Contact/index";
import SalesStatistics from "./salesStatistics";
import ProfileHeader from "components/profile/ProfileHeader/index";
import helperFunction from '../../../constants/helperFunction';
import { getProductsBySeller, getCategories, reviewList } from "actions/buyer";
import { connect } from 'react-redux'
import {getReviewList} from 'actions/buyer';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avg: '',
      perPage: 12,
      categorys_id:'',
      value:''
    }
    this.myRef = React.createRef()
  }
  componentDidMount() {
    this.handleScroll()
    // this.props.getProductsByCompany({company_id: this.props.match.params.id})
    this.props.getProductsBySeller({ seller_id: this.props.match.params.id, page: 1, perPage: this.state.perPage })
    // this.props.getCategories({company_id: this.props.match.params._id})
    const type1 = localStorage.getItem("buyer_token");
    if(this.props.match.path === "/view-seller/:id"){
      let data = {
        sellerId: this.props.match && this.props.match.params && this.props.match.params.id,
        sellerReviewList: true
      }
     if(type1){
    this.props.getReviewList({data})
      }
    }
    else{
    this.props.getReviewList({})
    }
  }

  handleScroll = () => {
    const { index, selected } = this.props
    if (index === selected) {
      this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }

  dataFromChild = (avgReview) => {
    this.setState({ avg: avgReview })
  }


  seeMoreProducts = (e) => {
    // this.setState({value:categorys_id})
    this.setState(prevState => {
      return{
          perPage: Number(prevState.perPage) + 12,
          // category_id:[this.props.category_id] 
      }
  })

  const t = setTimeout(() =>{
    this.props.getProductsBySeller({
      seller_id: this.props.match.params.id,
      page:1,
      perPage: this.state.perPage,
      category_id:e !== '' ? [e] : []
    })
  },1000)  
  };

  render() {
    const { productsBySeller, dataFromParent, categories, reviewList } = this.props;
    
    const type = localStorage.getItem("buyer_token");
    return (
      <div className="app-wrapper" ref={this.myRef}>
        <ProfileHeader history={this.props.history} match={this.props.match} dataFromParent={productsBySeller} dataFromParent1={reviewList}/>
        {/* )} */}
        <div className="jr-profile-content">
          <div className="row">
            <div className="col-xl-8 col-lg-8 col-md-7 col-12">
              <About
                history={this.props.history}
                match={this.props.match}
                showAddToCard={this.props.match.url !== '/view-company'}
                dataFromParent={productsBySeller}
                categoriesFromParent={categories}
                seeMoreProducts={(e)=>this.seeMoreProducts(e)}
                page={this.state.perPage}
              />
            </div>
            <div className="col-xl-4 col-lg-4 col-md-5 col-12 ">
              <div className="jr-card jr-full-card ">
                <div className={`jr-card-header d-flex align-items-center pb-0`}>
                  <div className="mr-auto">
                    <h2>Sales Statistic</h2>
                  </div>
                </div>
                <div className='row p-4 pl-4'>
                  <SalesStatistics className="" match={this.props.match} />
                </div>
              </div>
            { type ?
              <div className="jr-card jr-full-card ">
                <div className={`jr-card-header d-flex align-items-center reviewSellerCard`}>
                  <div className="mr-auto">
                    <h2>Reviews</h2>
                  </div>
                  <span style={{ textAlign: '-webkit-center' }}>
                    <span>
                      {
                        reviewList && reviewList.averageRating > 0 ? <i className={helperFunction.getRatingValueForBuyer(reviewList.averageRating)} set={'google'} style={{fontSize:'25px'}} /> : ''
                      }
                    </span>
                    <br />
                    Average
                  </span>
                </div>
                <Contact
                  history={this.props.history}
                  avgReview={this.dataFromChild} />
              </div>
              :
              null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = ({ buyer }) => {
  const { productsBySeller, categories, reviewList } = buyer
  return { productsBySeller, categories, reviewList }
};
export default connect(mapStateToProps, { getProductsBySeller, getCategories, getReviewList })(Profile);

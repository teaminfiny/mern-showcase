import React, { Component } from 'react';
import InfoCard from 'components/InfoCard';
import AppConfig from 'constants/config'
import axios from 'axios'
export var detailCards = [
  {
    color: '#072791',
    icon: 'card-giftcard',
    title: '0',
    subTitle: 'Products'
  },
  {
    color: '#ff9800',
    icon: 'star-outline',
    title: '0',
    subTitle: 'Reviews'
  }
]

class SalesStatistic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardDetails:[...detailCards]
    }
  }
   componentDidMount=()=>{
    axios({
      method:'post',
      url:`${AppConfig.baseUrl}seller/get_stats`,
      data:{
        seller_id:this.props.match.params.id
      }
    })
    .then((results)=>{
      if(results.data.detail){
        const {detail} = results.data
        let temp = [...this.state.cardDetails]
        temp[0].title = detail.active_products
        temp[1].title = detail.ratings
        this.setState({cardDetails:temp})
      }
    })
  }
  render() {
    return (
      <React.Fragment>
        
        {this.state.cardDetails.map((data, index) => {
          return <div className="col-5 infoCard">
            <InfoCard data={data} />
          </div>
        })
        }
      </React.Fragment>
    )
  }
}

export default SalesStatistic;

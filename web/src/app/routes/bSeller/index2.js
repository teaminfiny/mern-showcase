import React, { Component } from 'react';
import {Col,Row} from 'reactstrap'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Card from '@material-ui/core/Card';

class ViewSeller extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            tabValue:0,

         }
         this.handleChange = this.handleChange.bind(this)
    }
    handleChange = (event, newValue)=>{
        this.setState({tabValue:newValue})
    }
    render() { 
        const {tabValue} = this.state
        return ( 
            <div className= 'animated slideInUpTiny animation-duration-3'>
                <Row>
                    <Col className= 'backgroundCover bg-overlay  ' sm={12} md={12} xs={12} lg={12} xl={12} style= {{background:`linear-gradient(rgba(0,0,0,.7), rgba(0,0,0,.7)),url(${require('../../../assets/images/offerBanners.jpg')})`,backgroundSize:'cover',
                        backgroundRepeat:'none',backgroundPosition:'center center'}}>
                        <Row className='d-flex align-items-end h-100'>
                       <Col className = 'text-white text-center' sm={12} md={12} xs={12} lg={12} xl={12} >
                            <h1>Lotto Pharma</h1>
                            <h3>at one shop</h3>
                       </Col>
                       <Col className = 'text-white text-right' sm={12} md={12} xs={12} lg={12} xl={12} >
                            <span className='d-flex justify-content-end'>
                                <h3 className='ml-2 mr-2 cursor-pointer'><i className = 'zmdi zmdi-thumb-up'></i></h3>
                            
                         
                                <h3 className='ml-2 mr-2 cursor-pointer'><i className = 'zmdi zmdi-thumb-down'></i></h3>
                            </span>
                       </Col>
                       </Row>
                    </Col>
                </Row>
                <Row>
                    <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                        <AppBar position='static' color='primary'>
                        <Tabs
                        value={tabValue}
                        onChange={this.handleChange}
                        // indicatorColor="primary"
                        // textColor="primary"
                        centered
                      >
                        <Tab label="Home" />
                        <Tab label="Products" />
                        <Tab label="Reviews" />
                      </Tabs>
                        </AppBar>
                        {
                            tabValue===0&&
                            <Col sm={5} md={5} xs={5} lg={5} xl={5}>
                                <Card sm={5} md={5} xs={5} lg={5} xl={5}>
                                    <thead>
                                        <tr>
                                            <td>
                                                Seller Name:
                                            </td>
                                            <td>
                                                Lekhraj Pharma
                                            </td>
                                        </tr>
                                        <tr>
                                        <td>
                                            Seller Name:
                                        </td>
                                        <td>
                                            Lekhraj Pharma
                                        </td>
                                    </tr>
                                    <tr>
                                    <td>
                                        Seller Name:
                                    </td>
                                    <td>
                                        Lekhraj Pharma
                                    </td>
                                </tr>
                                <tr>
                                <td>
                                    Seller Name:
                                </td>
                                <td>
                                    Lekhraj Pharma
                                </td>
                            </tr>
                                    </thead>
                                </Card>
                            </Col>
                            

                        }
                    </Col>
                </Row>
            </div>
         );
    }
}
 
export default ViewSeller;
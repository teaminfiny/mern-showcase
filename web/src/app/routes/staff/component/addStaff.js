import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Col, Row } from 'reactstrap';
import { FormGroup } from 'reactstrap';
import Avatar from '@material-ui/core/Avatar';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Icon from '@material-ui/core/Icon';
import DialogContentText from '@material-ui/core/DialogContentText';
import { listGroup, addStaff, listPermissionModules } from '../../../../actions/seller';
import { connect } from 'react-redux';
import ReactStrapTextField from '../../../../components/ReactStrapTextField';
import ReactstrapSelectField from '../../../../components/reactstrapSelectField';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required, emailField, validatePhone } from '../../../../constants/validations';
import Axios from '../../../../sagas/axiosRequest';
import asyncValidate from '../../../../constants/asyncValidate'
import helperFunction from '../../../../constants/helperFunction';

class AddStaff extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      groupId: '',
      permission: [],
      groups: [],
      addGroup: false,
      isAllPermission: false,
      red: 0,
      blue: 0,
      green: 0
    }
  }

  componentDidMount = async () => {
    let groupListData = await Axios.axiosHelperFunc('get', 'seller/getGroupList', '', {});
    this.setState({ groups: groupListData.data.detail });

    let rgbValue = helperFunction.getRGB();
    this.setState({ red: rgbValue[0], green: rgbValue[1], blue: rgbValue[2] })
  }

  handleClick = (key) => {
    this.setState({ [key]: !this.state[key] });
  }

  handleAddGroupClick = (key) => {
    this.props.addGroup('add')
    this.setState({ addGroup: !this.state.addGroup });
  }

  handleRequestClose = () => {
    this.props.handleClick('add');
    this.props.reset();
    this.setState({ permission: [], email: '', name: '', phone: '', groupId: '' });
  };

  handleChange = (event, key) => {
    if (key === 'group') {
      let index = this.state.groups.findIndex((e) => e._id === event.target.value);
      if (index > -1) {
        let tempPermission = this.state.permission;
        tempPermission = this.state.groups[index].permissions;
        let isAdminAccess = tempPermission.length === this.props.listpermissionModulesdata.length ? true : false;
        this.setState({ permission: tempPermission, groupId: event.target.value, isAllPermission: isAdminAccess });
      } else {
        this.setState({ groupId: '' });
      }
    } else {
      this.setState({ [key]: event.target.value });
    }
  }

  onSubmit = () => {
    let data = {
      first_name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      groupId: this.state.groupId,
      isAllPermission: this.state.isAllPermission,
      red: this.state.red,
      green: this.state.green,
      blue: this.state.blue,
    }
    this.props.handleClick('add')
    this.props.addStaff({ history: this.props.history, data, listStaff: { searchText: '', page: 1, perPage: 10 } })
    this.setState({ permission: [], email: '', name: '', phone: '', groupId: '' });
  }

  render() {
    const { email, name, groups, phone, permission, groupId, addGroup } = this.state;
    const { buttonType, add, title, handleSubmit, listpermissionModulesdata, submitting } = this.props;
    return (
      <React.Fragment>
        <Dialog open={add} onClose={this.handleRequestClose}
          fullWidth={true}
          maxWidth={'md'}>
          <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off">
            <DialogTitle>
              {title}
            </DialogTitle>
            <DialogContent>
              <Row>
                <Col xs={12} md={12} sm={12} xl={2} lg={2} className="addStaffAvatar">
                  <Avatar className="size-100" alt="Remy Sharp" src={require('assets/productImage/avatar.png')} />
                </Col>
                <Col xs={12} md={12} sm={12} xl={10} lg={10}>
                  <Row>
                    <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                      <Field id="name" name="name" type="text"
                        component={ReactStrapTextField} label="Name"
                        validate={[required]}
                        value={name}
                        onChange={(event) => this.handleChange(event, 'name')}
                      />
                    </Col>
                    <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                      <Field id="email" name="email" type="text"
                        component={ReactStrapTextField} label="Email ID"
                        validate={[emailField]}
                        value={email}
                        onChange={(event) => this.handleChange(event, 'email')}
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                      <Field id="phone" name="phone" type="text"
                        component={ReactStrapTextField} label="Phone"
                        validate={[validatePhone]}
                        value={phone}
                        onChange={(event) => this.handleChange(event, 'phone')}
                      />
                    </Col>
                    <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                      <Row>
                        <Col xs={10} md={10} sm={10} xl={10} lg={10}>
                          <Field
                            name="group"
                            component={ReactstrapSelectField}
                            label="Group"
                            validate={required}
                            value={groupId}
                            type="select"
                            onChange={(event, newValue, previousValue) => this.handleChange(event, 'group')}
                          >
                            <option value=""> Select Group </option>
                            {
                              this.state.groups !== undefined && this.state.groups.length > 0 && this.state.groups.map((value, key) => {
                                return <option key={'group_' + key} value={value._id}>{value.name}</option>
                              })
                            }
                          </Field>
                        </Col>
                        <Col xs={2} md={2} sm={2} xl={2} lg={2} className="addGroupButton cursor-pointer">
                          <FormGroup>
                            <Icon color="primary" onClick={() => this.handleAddGroupClick('addGroup')} >add_circle</Icon>
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  {
                    groupId !== '' ? <Row>
                      {
                        listpermissionModulesdata !== undefined && listpermissionModulesdata.length === permission.length ? <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                checked={listpermissionModulesdata !== undefined && listpermissionModulesdata.length === permission.length}
                                value={'admin_access'}
                                color="primary"
                                disabled={true}
                              />
                            }
                            label={'Admin Access'}
                          />
                        </Col> : ''
                      }
                      {
                        (permission !== undefined && permission.length > 0) ? permission.map((value, key) => {
                          return <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  checked={true}
                                  value={value.value}
                                  color="primary"
                                  disabled={true}
                                />
                              }
                              label={value.name}
                            />
                          </Col>
                        }) : ''
                      }
                    </Row> : ''
                  }
                </Col>
              </Row>
            </DialogContent>
            <DialogActions className="pr-4">
              <Button onClick={this.handleRequestClose} color='secondary' >
                Cancel
          </Button>
              <Button type='submit' color='primary' disabled={submitting}>
                Add
          </Button>
            </DialogActions>
          </form>
        </Dialog>
        
        
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ seller }) => {
  const { listGroupData, listpermissionModulesdata } = seller;
  return { listGroupData, listpermissionModulesdata }
};

AddStaff = connect(
  mapStateToProps,
  {
    listGroup,
    listPermissionModules,
    addStaff
  }               // bind account loading action creator
)(AddStaff)

export default AddStaff = reduxForm({
  form: 'AddStaff',// a unique identifier for this form
  asyncValidate,
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('AddStaff')();
    dispatch(initialize('AddStaff', newInitialValues));
  }
})(AddStaff);
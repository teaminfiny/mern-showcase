import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Col, Row } from 'reactstrap';
import Avatar from '@material-ui/core/Avatar';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import AddGroup from '../../groups/routes/component/addGroup';
import DialogContentText from '@material-ui/core/DialogContentText';
import { listGroup, addStaff, listPermissionModules } from '../../../../actions/seller';
import { connect } from 'react-redux';
import ReactStrapTextField from '../../../../components/ReactStrapTextField';
import ReactstrapSelectField from '../../../../components/reactstrapSelectField';
import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required, emailField, validatePhone } from '../../../../constants/validations';
import Axios from '../../../../sagas/axiosRequest';
import asyncValidate from '../../../../constants/asyncValidate'

class EditStaff extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            phone: '',
            groupId: '',
            permission: [],
            groups: [],
            addGroup: false,
            isAllPermission: false,
            staffId: ''
        }
    }


    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.selectedData !== prevProps.selectedData) {
            this.props.initialize({
                name: this.props.selectedData ? this.props.selectedData.first_name : this.state.name,
                email: this.props.selectedData ? this.props.selectedData.email : this.state.email,
                phone: this.props.selectedData ? this.props.selectedData.phone : this.state.phone,
                staffId: this.props.selectedData ? this.props.selectedData._id : this.state.staffId,
                groupId: this.props.selectedData ? this.props.selectedData.Groups._id : this.state.groupId,
                isAllPermission: this.props.selectedData ? (this.props.selectedData.Groups.permissions.length === this.props.listpermissionModulesdata.length) ? true : false : false,
                permission: this.props.selectedData ? this.props.selectedData.Groups.permissions : this.state.permission
            });

            this.setState({
                name: this.props.selectedData ? this.props.selectedData.first_name : this.state.name,
                email: this.props.selectedData ? this.props.selectedData.email : this.state.email,
                phone: this.props.selectedData ? this.props.selectedData.phone : this.state.phone,
                staffId: this.props.selectedData ? this.props.selectedData._id : this.state.staffId,
                groupId: this.props.selectedData ? this.props.selectedData.Groups._id : this.state.groupId,
                /* isAllPermission: this.props.selectedData ? (this.props.selectedData.Groups.permissions.length === this.props.listpermissionModulesdata.length) ? true : false : false, */
                permission: this.props.selectedData ? this.props.selectedData.Groups.permissions : this.state.permission
            });
        }
    }

    componentDidMount = async () => {
        this.props.listPermissionModules({ history: this.props.history })
        let groupListData = await Axios.axiosHelperFunc('get', 'seller/getGroupList', '', {});
        this.props.initialize({
            name: this.props.selectedData ? this.props.selectedData.first_name : this.state.name,
            email: this.props.selectedData ? this.props.selectedData.email : this.state.email,
            phone: this.props.selectedData ? this.props.selectedData.phone : this.state.phone,
            staffId: this.props.selectedData ? this.props.selectedData._id : this.state.staffId,
            groupId: this.props.selectedData ? this.props.selectedData.Groups._id : this.state.groupId,
            isAllPermission: this.props.selectedData ? (this.props.selectedData.Groups.permissions.length === this.props.listpermissionModulesdata.length) ? true : false : false,
            permission: this.props.selectedData ? this.props.selectedData.Groups.permissions : this.state.permission
        });
        this.setState({
            name: this.props.selectedData ? this.props.selectedData.first_name : this.state.name,
            email: this.props.selectedData ? this.props.selectedData.email : this.state.email,
            phone: this.props.selectedData ? this.props.selectedData.phone : this.state.phone,
            staffId: this.props.selectedData ? this.props.selectedData._id : this.state.staffId,
            groupId: this.props.selectedData ? this.props.selectedData.Groups._id : this.state.groupId,
            isAllPermission: this.props.selectedData ? (this.props.selectedData.Groups.permissions.length === this.props.listpermissionModulesdata.length) ? true : false : false,
            permission: this.props.selectedData ? this.props.selectedData.Groups.permissions : this.state.permission,
            groups: groupListData.data.detail
        });

    }

    handleClick = (key) => {
        this.setState({ [key]: !this.state[key] });
    }

    handleAddGroupClick = (key) => {
        this.props.handleClick('add')
        this.setState({ [key]: !this.state[key] });
    }

    handleRequestClose = () => {
        this.props.handleClick('edit');
        this.props.reset();
        this.setState({ permission: [], email: '', name: '', phone: '', groupId: '' });
    };

    handleChange = (event, key) => {
        if (key === 'group') {
            let index = this.state.groups.findIndex((e) => e._id === event.target.value);
            if (index > -1) {
                let tempPermission = this.state.permission;
                tempPermission = this.state.groups[index].permissions;
                let isAdminAccess = tempPermission.length === this.props.listpermissionModulesdata.length ? true : false;
                this.setState({ permission: tempPermission, groupId: event.target.value, isAllPermission: isAdminAccess });
            } else {
                this.setState({ groupId: '' });
            }
        } else {
            this.setState({ [key]: event.target.value });
        }
    }

    onSubmit = () => {
        let data = {
            first_name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            groupId: this.state.groupId,
            staffId: this.state.staffId,
            isAllPermission: this.state.isAllPermission
        }
        this.props.handleClick('edit')
        this.props.addStaff({ history: this.props.history, data, listStaff: { searchText: '', page: 1, perPage: 10 } })
        this.setState({ permission: [], email: '', name: '', phone: '', groupId: '' });
    }

    render() {
        const { email, name, groups, phone, permission, groupId, staffId } = this.state;
        const { edit, add, title, handleSubmit, listpermissionModulesdata, submitting } = this.props;
        return (
            <React.Fragment>
                <Dialog open={edit} onClose={this.handleRequestClose}
                    fullWidth={true}
                    maxWidth={'md'}>
                    <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off">
                        <DialogTitle>
                            {title}
                        </DialogTitle>
                        <DialogContent>
                            <Row>
                                <Col xs={12} md={12} sm={12} xl={2} lg={2} className="addStaffAvatar">
                                    <Avatar className="size-100" alt="Remy Sharp" src={require('assets/productImage/avatar.png')} />
                                </Col>
                                <Col xs={12} md={12} sm={12} xl={10} lg={10}>
                                    <Row>
                                        <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                                            <Field id="name" name="name" type="text"
                                                component={ReactStrapTextField} label="Name"
                                                validate={[required]}
                                                value={name}
                                                onChange={(event) => this.handleChange(event, 'name')}
                                            />
                                        </Col>
                                        <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                                            <Field id="email" name="email" type="text"
                                                component={ReactStrapTextField} label="Email ID"
                                                validate={[emailField]}
                                                value={email}
                                                onChange={(event) => this.handleChange(event, 'email')}
                                            // input={{value:email}}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                                            <Field id="phone" name="phone" type="text"
                                                component={ReactStrapTextField} label="Phone"
                                                validate={[validatePhone]}
                                                value={phone}
                                                onChange={(event) => this.handleChange(event, 'phone')}
                                            // input={{value:phone}}
                                            />
                                        </Col>
                                        <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                                            <Row>
                                                <Col xs={12} md={12} sm={12} xl={12} lg={12}>
                                                    <Field
                                                        name="group"
                                                        component={ReactstrapSelectField}
                                                        label="Group"
                                                        // validate={required}
                                                        defaultValue={groupId}
                                                        value={groupId}
                                                        // input={{ value:groupId, name:'group', }}
                                                        type="select"
                                                        onChange={(event) => this.handleChange(event, 'group')}
                                                    >
                                                        {
                                                            this.state.groups !== undefined && this.state.groups.length > 0 && this.state.groups.map((value, key) => {
                                                                return <option key={'group_' + key} value={value._id}>{value.name}</option>
                                                            })
                                                        }
                                                    </Field>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    {
                                        groupId !== '' ? <Row>
                                            {
                                                listpermissionModulesdata !== undefined && listpermissionModulesdata.length === permission.length ? <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                checked={listpermissionModulesdata !== undefined && listpermissionModulesdata.length === permission.length}
                                                                value={'admin_access'}
                                                                color="primary"
                                                                disabled={true}
                                                            />
                                                        }
                                                        label={'Admin Access'}
                                                    />
                                                </Col> : ''
                                            }
                                            {
                                                (listpermissionModulesdata !== undefined && listpermissionModulesdata.length > 0) ? listpermissionModulesdata.map((value, key) => {
                                                    let index;
                                                    if (staffId !== '') {
                                                        if (typeof (permission[0]) === 'string') {
                                                            index = permission.findIndex((e) => e === value._id)
                                                        } else {
                                                            index = permission.findIndex((e) => e._id === value._id)
                                                        }
                                                    } else {
                                                        index = permission.findIndex((e) => e._id === value._id)
                                                    }
                                                    return index > -1 ? <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                                                        <FormControlLabel
                                                            control={
                                                                <Checkbox
                                                                    checked={index > -1 ? true : false}
                                                                    value={value.value}
                                                                    color="primary"
                                                                    disabled={true}
                                                                />
                                                            }
                                                            label={value.name}
                                                        />
                                                    </Col> : ''
                                                }) : ''
                                            }
                                        </Row> : ''
                                    }
                                </Col>
                            </Row>
                        </DialogContent>
                        <DialogActions className="pr-4">
                            <Button onClick={this.handleRequestClose} color='secondary' >
                                Cancel
          </Button>
                            <Button type='submit' color='primary' disabled={submitting}>
                                Edit
           </Button>
                        </DialogActions>
                    </form>
                </Dialog>
                {
                    this.props.permission && this.props.permission.length > 0 ? <AddGroup add_group={this.state.addGroup} permission={this.props.permission} title={'Add Group'} handleClick={this.handleClick} /> : ''
                }
            </React.Fragment>
        );
    }
}

const mapStateToProps = ({ seller }) => {
    const { listGroupData, listpermissionModulesdata } = seller;
    return { listGroupData, listpermissionModulesdata }
};

EditStaff = connect(
    mapStateToProps,
    {
        listGroup,
        listPermissionModules,
        addStaff
    }               // bind account loading action creator
)(EditStaff)

export default EditStaff = reduxForm({
    form: 'EditStaff',// a unique identifier for this form
    asyncValidate,
    onSubmitSuccess: (result, dispatch) => {
        const newInitialValues = getFormInitialValues('EditStaff')();
        dispatch(initialize('EditStaff', newInitialValues));
    }
})(EditStaff);
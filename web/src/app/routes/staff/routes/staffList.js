import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import Button from '@material-ui/core/Button';
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import customHead from 'constants/customHead'
import AddStaff from '../component/addStaff';
import AddGroup from '../../groups/routes/component/addGroup';
import EditStaff from '../component/editStaff';
import DeleteStaff from '../../inventory/component/deleteInventory';
import { listStaff } from '../../../../actions/seller';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from 'constants/axios';
import { NotificationManager } from 'react-notifications';
import AppConfig from 'constants/config'

const columns = [
  {
    name: "name",
    label: "Name",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "email",
    label: "Email ID",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "group",
    label: "Group",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "action",
    label: "Action",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  }
];

class StaffList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      permission: false,
      delete: false,
      add: false,
      addGroup:false,
      searchText : '',
      perPage : 50,
      page: 0,
      selectedData : {},
    }
  }

  componentDidMount = () => {
    this.props.listStaff({ history: this.props.history, searchText: this.state.searchText, page:1, perPage: 50  })
  }

  changePage = (page) => {
    let pages = page + 1
    this.props.listStaff({ userId: '', searchText: this.state.searchedText, page: pages, perPage: this.state.perPage })
    this.setState({ page })
  };

  changeRowsPerPage = (perPage) => {
    this.props.listStaff({ page: 1, perPage })
    this.setState({ page: 0, perPage })
  }

  handleSearch = (searchText) => {
    this.props.listStaff({ userId: '', searchText: searchText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage })
    this.setState({ searchedText: searchText })
  };

  handleClick = (key, value) => {
    this.setState({ [key]: !this.state[key], selectedData : value })
  }


  button = (data) => (
    <ButtonGroup color="primary" aria-label="outlined primary button group">
      <Button className={'text-primary'} onClick={() => this.handleClick('edit',data)} > Edit</Button>
      {/* <Button className={'text-warning'} onClick={() => this.handleClick('permission')} > Permission</Button> */}
      <Button className={'text-danger'} onClick={() => this.handleClick('delete',data)}> Delete</Button>
      <Button className={'text-success'} onClick={() => this.handleReset('reset',data)}> Reset </Button>
    </ButtonGroup>
  )
  addGroup = ()=>{
    this.setState({
      addGroup:true,
      add:false
    })
  }
  handleReset = (key, data) => {
    axios({
      method:'post',
      url:`${AppConfig.baseUrl}seller/addEditStaffMember`,
      headers: {
        'Content-Type': 'application/json',
        token: localStorage.getItem('token')
      },
      data:{
        id:data._id,
        reset:true,
        email:data.email
      }
    }).then((result)=>{
      if(result.data.error){
        NotificationManager.error(result.data.title);
      }else{
        NotificationManager.success(result.data.title);
      }
    })
  }
  render() {
    const { staffList } = this.props;
    let { selectedData } = this.state;
    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      // rowsPerPage: this.state.perPage,
      rowsPerPage: 50,
      rowsPerPageOptions: [],
      page: this.state.page,
      fixedHeader: false,
      print: false,
      download: false,
      filter: false,
      sort: false,
      selectableRows: false,
      count: this.props.staffList.length > 0 ? this.props.staffList[0].metadata.length > 0 ? this.props.staffList[0].metadata[0].total : 0 : 0,
      rowsPerPage: this.state.perPage,
      serverSide: true,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      searchText: this.state.searchedText,
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
        }
      },
    }

    var listData = [];
    staffList.length > 0 && staffList[0].data.length > 0 && staffList[0].data.map((data, index) => {
      listData.push([data.first_name, data.email, data.Groups.name, this.button(data)]);
    });


    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Staff Members" />} />
       

        {
          this.props.loading == false ?
            <MUIDataTable
              title={<Button className={'text-primary'} selectedData={selectedData} onClick={() => this.handleClick('add')}> Add Staff Member+</Button>}
              data={listData}
              columns={columns}
              options={options}
            />

            :
            <div className="loader-view" style={{ textAlign: "center", marginTop: "300px" }}>
              <CircularProgress />
            </div>
        }

        {
          this.state.edit&&
          <EditStaff selectedData={selectedData} edit={this.state.edit} title={'Edit Staff Member'}  handleClick={this.handleClick} />
        }
        {
          this.state.add&&
          <AddStaff addGroup = {this.addGroup} selectedData={selectedData} add={this.state.add} title={'Add Staff Member'} handleClick={this.handleClick} />
        }
        {
          this.state.delete&&
          <DeleteStaff selectedData={selectedData} delete={this.state.delete} deleteFor={'staff'} handleClick={this.handleClick} />
        }
        {
          this.state.addGroup&&
          <AddGroup add_group={this.state.addGroup} permission={this.props.permission} title={'Add Group'} handleClick={this.handleClick} history={this.props.history} />
        }
        
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ seller }) => {
  const { staffList, loading } = seller;
  return { staffList, loading }
};

StaffList = connect(
  mapStateToProps,
  {
    listStaff    
  }               // bind account loading action creator
)(StaffList)

export default StaffList;
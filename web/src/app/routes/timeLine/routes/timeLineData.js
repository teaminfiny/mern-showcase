const timeLineData = [
  {
    image: 'https://via.placeholder.com/128x128',
    time: '16 Jan, 2019',
    title: 'New',
    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.',
  }, {
    image: 'https://via.placeholder.com/208x208',
    time: '16 Jan, 2019',
    title: 'Billed',
    description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
  }, {
    image: 'https://via.placeholder.com/150x150',
    time: '16 Jan, 2019',
    title: 'Ready For Dispatch',
    description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.',
  }, {
    image: 'https://via.placeholder.com/150x150',
    time: '16 Jan, 2019',
    title: 'In-Transit',
    description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. ',
  }, {
    image: 'https://via.placeholder.com/150x150',
    time: '16 Jan, 2019',
    title: 'Delivered',
    description: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.',
  }
];

export default timeLineData;
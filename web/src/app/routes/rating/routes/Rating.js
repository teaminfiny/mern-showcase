import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import Avatar from '@material-ui/core/Avatar';
import moment from 'moment';
import helperFunction from '../../../../constants/helperFunction';
import axios from '../../../../constants/axios';
import AppConfig from 'constants/config'

const customHead = (o, updateDirection) => {
  return <th className="MuiTableCell-root MUIDataTableHeadCell-root-298 MUIDataTableHeadCell-fixedHeader-299 MuiTableCell-head">
    <span style={{ fontSize: '1rem' }}>{o.label}</span>
  </th>;
}

const columns = [
  {
    name: "title",
    label: "Title",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "date",
    label: "Date",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "status",
    label: "Rating",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
];




const options = {
  filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      fixedHeader: false,
      print: false,
      download: false,
      filter: false,
      sort: false,
      selectableRows: false,
      serverSide: true,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
};

class ProductRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratingList: []
    }
  }

  componentWillMount = async () => {
    await axios({
      method:'post',
      url:`${AppConfig.baseUrl}users/getRating`,
      headers: {
        'Content-Type': 'application/json',
        token: localStorage.getItem('token')
      }
    }
    ).then(result => {
      if (result.data.error) {
      } else {
        this.setState({ ratingList: result.data.detail })
      }
    })
      .catch(error => {
      });
  }

  render() {

    let { ratingList } = this.state;
    let ratingData = []
    ratingList && ratingList.length > 0 && ratingList.map((data, index) => {
      ratingData.push([
        <div className="user-profile d-flex flex-row align-items-center">
          <Avatar
            alt='Image'
            style={{ background: helperFunction.getGradient(data.user_id.backGroundColor) }}
            className="user-avatar"
          > {helperFunction.getNameInitials(`${data.user_id.first_name} ${data.user_id.last_name}`)}</Avatar>
          <div className="user-detail-name">
            <h5 className="user-name">{data.user_id.first_name} {data.user_id.last_name} </h5>
          </div>
        </div>
        ,  moment(data.createdAt).format('DD-MM-YYYY'), <i className = {`${helperFunction.getRatingValueForBuyer(Number(data.ratingValue))} fa-2x`} />]);
   });


    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Rating" />} />
        <MUIDataTable
          data={ratingData}
          columns={columns}
          options={options}
        />
      </React.Fragment>
    );
  }
}

export default ProductRequest;
import React from 'react';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import {  withRouter } from 'react-router-dom';
import Radio from '@material-ui/core/Radio';
import ManufacturersPopOver from './ManufacturersPopOver'
import SellerPopOver from './SellerPopOver'
import CategoryPopOver from './CategoryPopOver'
import { Popover } from 'reactstrap';
import './index.css';
import { connect } from 'react-redux';
import { getSearchProduct } from 'actions/buyer';
import {Helmet} from "react-helmet";


class PinnedSubheaderList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checked: [0],
      value: { min: 0, max: 10 },
      value1: { min: 0, max: 500 },
      obj: {},
      popoverOpenCompanies: false,
      popoverOpenCategories: false,
      popoverOpenSellers: false,
      searchText: '',
      categoryName: '',
      companyName: '',
      sellerName: '',
      categories: [],
      cat: props ? props.match.params.keyword ? props.match.params.keyword : 'category' : 'category',
      selectCat:'',
      discount:''
    };
    this.toggleCompaniesPopOver = this.toggleCompaniesPopOver.bind(this);
    this.toggleCategoryPopOver = this.toggleCategoryPopOver.bind(this);
    this.toggleSellersPopOver = this.toggleSellersPopOver.bind(this);
  }

  toggleCompaniesPopOver() {
    this.setState({
      popoverOpenCompanies: !this.state.popoverOpenCompanies
    });
  }

  toggleCategoryPopOver() {
    this.setState({
      popoverOpenCategories: !this.state.popoverOpenCategories
    });
  }

  toggleSellersPopOver() {
    this.setState({
      popoverOpenSellers: !this.state.popoverOpenSellers
    });
  }


  handleToggle = (event, value) => {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });
  };

  handleSigninOnEnter = (e) => {
    if (e.key === 'Enter' && this.state.searchText !== '') {
      let obj = { search: this.state.searchText }
      let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
      let catName =  this.state.selectCat && ((this.state.selectCat).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase();
      // this.props.history.push(`/search-results/${url}`)
      this.props.history.push({ pathname: `/search-results/${catName && catName !== '' ? catName : 'category'}/${url}`, state: 'searchPage' })
    }
  }

  handleClick = (key, e) => {
    if (e !== '') {

      const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
      let obj = { ...this.state.obj }
      obj.search = decodedURL.search ? decodedURL.search : ''
      obj.sortBy = decodedURL.sortBy ? decodedURL.sortBy : 'price'
      obj.category_id = decodedURL.category_id ? decodedURL.category_id.split(',') : []
      obj[key] = e;
      obj.medicineType = decodedURL.medicineType ? decodedURL.medicineType :''

      let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
      this.setState({ obj: obj })
      let catName =  this.state.selectCat ? ((this.state.selectCat).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase() : 'category'
      // this.props.history.push(`/search-results/${url}`)
      this.props.history.push({ pathname: `/search-results/${catName && catName !== '' ? catName : 'category'}/${url}`, state: 'searchPage' })

      const data = {
        key: obj.search ? obj.search : '',
        company_id: obj.company_id ? obj.company_id : '',
        category_id: obj.category_id ? obj.category_id : '',
        seller_id: obj.seller_id ? obj.seller_id : '',
        mediType: obj.medicineType ? obj.medicineType :'',
        discount: obj.discount && obj.discount === '10 or more' ? 10 : obj.discount === '20 or more' ? 20 : obj.discount === '50 or more' ? 50 :'',
        page: 1,
        perPage: 12,
        sortBy: obj.sortBy
      }
      this.props.getSearchProduct({ data })
    }
  }

  componentDidMount = async() => {
    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    let { filterFromParent } = this.props;
    let cateogoryData = await filterFromParent && filterFromParent.detail && filterFromParent.detail.categories.filter((e) => e._id == decodedURL.category_id);
    let catData = cateogoryData && cateogoryData.length > 0 ? cateogoryData[0].name : '' 
    await this.setState({ selectCat: catData})
    let obj = { ...this.state.obj }
    obj.search = decodedURL.search ? decodedURL.search : ''
    obj.company_id = decodedURL.company_id ? decodedURL.company_id : ''
    obj.category_id = decodedURL.category_id ? decodedURL.category_id.split(',') : ''
    obj.seller_id = decodedURL.seller_id ? decodedURL.seller_id : ''
    obj.sortBy= decodedURL.sortBy ? decodedURL.sortBy :'price'
    obj.medicineType = decodedURL.medicineType ? decodedURL.medicineType :''
    obj.discount = decodedURL.discount ? decodedURL.discount : ''
    this.setState({ obj, categories: decodedURL.category_id ? decodedURL.category_id.split(',') : [] })
  }

  reset = () => {
    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    let obj = { search: decodedURL.search ? decodedURL.search : '',sortBy: decodedURL.sortBy ,medicineType: decodedURL.medicineType ? decodedURL.medicineType :'' }
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    // this.props.history.push(`/search-results/${url}`)
    this.props.history.push({ pathname: `/search-results/category/${url}`, state: 'searchPage' })
    const data = {
      key: decodedURL.search ? decodedURL.search : '',
      company_id: '',
      category_id: '',
      seller_id: '',
      page: 1,
      perPage: 12,
      sortBy: decodedURL.sortBy ? decodedURL.sortBy : 'price',
      mediType: decodedURL.medicineType ? decodedURL.medicineType :''
    }
    this.props.getSearchProduct({ data })

    this.setState({
      checked: [0],
      obj: { search: decodedURL.search, company_id: '', category_id: '', seller_id: '', medicineType: decodedURL.medicineType ? decodedURL.medicineType :'',discount:'' },
      categoryName: '',
      companyName: '',
      sellerName: '',
      categories: [],
      discount:''
    })
  }


  handleCategories = async(key, e) => {

    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')

    let obj = { ...this.state.obj }

    obj.search = decodedURL.search ? decodedURL.search : ''
    obj[key] = e;
    obj.sortBy = decodedURL.sortBy ? decodedURL.sortBy : 'price'
    obj.medicineType= decodedURL.medicineType ? decodedURL.medicineType :''
    obj.discount= decodedURL.discount ? decodedURL.discount :''

    let temp = [...this.state.categories]
    if (e.target.checked) {
      temp.push(e.target.value)
      this.setState({ categories: temp })
    } else {
      temp.splice(this.getChecked(e.target.value), 1)
      this.setState({ categories: temp })
    }
    
    let { filterFromParent } = this.props;
    let cateogoryData = await filterFromParent && filterFromParent.detail && filterFromParent.detail.categories.filter((e) => e._id == this.state.categories[0]);
    await this.setState({ selectCat: cateogoryData && cateogoryData.length > 0 ? cateogoryData[0].name : ''})
    const data = {
      key: obj.search ? obj.search : '',
      company_id: obj.company_id ? obj.company_id : '',
      category_id: temp,
      seller_id: obj.seller_id ? obj.seller_id : '',
      mediType: obj.medicineType ? obj.medicineType :'',
      discount: obj.discount && obj.discount === '10 or more' ? 10 : obj.discount === '20 or more' ? 20 : obj.discount === '50 or more' ? 50 :'',
      page: 1,
      perPage: 12,
      sortBy: obj.sortBy
    }
    if (decodedURL.search) {
      obj.search = decodedURL.search
    }
    obj.category_id = temp.length > 0 ? temp.toString() : ''
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    // this.props.history.push(`/search-results/${url}`)
    this.setState({ obj: obj })
    let catName =  this.state.selectCat && ((this.state.selectCat).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()
    this.props.history.push({ pathname: `/search-results/${catName && catName !== '' ? catName : 'category'}/${url}`, state: 'searchPage' })
    this.props.getSearchProduct({ data })
  }

  getChecked = (data) => {
    let index = this.state.categories.findIndex((val) => val == data)
    return index
  }

  render() {

    const { filterFromParent } = this.props
    const categories = filterFromParent && filterFromParent.detail && filterFromParent.detail.categories && filterFromParent.detail.categories;
    const companies = filterFromParent && filterFromParent.detail && filterFromParent.detail.comapnies && filterFromParent.detail.comapnies;
    const sellers = filterFromParent && filterFromParent.detail && filterFromParent.detail.sellers && filterFromParent.detail.sellers;
    let categoryN = this.state.selectCat ? this.state.selectCat : 'category'
    // const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"")) + '"}')
    // Buy Anti Allergy Medicine For Skin Online In India - Medimny

    // Medimny is a B2B E-Commerce platform for Pharmaceutical Products for Distributors, Stockists and Retail Drug Stores serving entire nation. Buy Anti Allergy Medicine For Skin Online In India.
    return (
      <List className="" subheader={<div />}>
        { this.state.selectCat != 'category' ? <Helmet>
          <title>Buy {this.state.selectCat} Medication Online In India - Medimny</title>
          <meta name="title" content={`Buy ${this.state.selectCat} Medication Online In India - Medimny`} />
          <meta name="description" content={`Medimny is a B2B E-Commerce platform for Pharmaceutical Products for Distributors, Stockists and Retail Drug Stores serving entire nation. Buy ${this.state.selectCat} Medication Online In India.` } />
          {/* <meta name="description" content={`Buy ${this.state.selectCat} Medication Online In India. Medimny is a B2B E-Commerce platform for Pharmaceutical Products for Distributors, Stockists and Retail Drug Stores serving entire nation.` } /> */}
        </Helmet> : ''
        }
        <button className="mt-3  mb-3 ml-2"
          style={{
            backgroundColor: "white",
            color: '#f3554c', borderRadius: '5px', border: 'none'
          }} onClick={this.reset} >
          Clear Filters
        </button>


        {/* //------------------------------------------- DISCOUNT ------------------------------------------------------------// */}

        <ListSubheader className="text-muted bg-grey lighten-4">{'Discount'}</ListSubheader>
            <ListItem className="listItemName" button key={'10 or more'} onClick={event => this.handleToggle(event, '10 or more')} style={{ paddingTop: "0px", paddingBottom: "0px" }}>

              <Radio color="primary"
                checked={this.state.obj.discount == '10 or more'}
                onClick={(e) => {
                  this.handleClick('discount', '10 or more');
                }}
                value="a" name="radio button demo"
                aria-label="A" />
              <ListItemText className="" primary={"10% OFF OR MORE"} />

            </ListItem>
            <ListItem className="listItemName" button key={'20 or more'} onClick={event => this.handleToggle(event, '20 or more')} style={{ paddingTop: "0px", paddingBottom: "0px" }}>

              <Radio color="primary"
                checked={this.state.obj.discount == '20 or more'}
                onClick={(e) => {
                  this.handleClick('discount', '20 or more');
                }}
                value="a" name="radio button demo"
                aria-label="A" />
              <ListItemText className="" primary={"20% OFF OR MORE"} />

            </ListItem>
            <ListItem className="listItemName" button key={'50 or more'} onClick={event => this.handleToggle(event, '50 or more')} style={{ paddingTop: "0px", paddingBottom: "0px" }}>

              <Radio color="primary"
                checked={this.state.obj.discount == '50 or more'}
                onClick={(e) => {
                  this.handleClick('discount', '50 or more');
                }}
                value="a" name="radio button demo"
                aria-label="A" />
              <ListItemText className="listItemName" primary={"50% OFF OR MORE"} />

            </ListItem>


        {/* //------------------------------------------- CATEGORY ------------------------------------------------------------// */}

        <ListSubheader className="text-muted bg-grey lighten-4">Categories</ListSubheader>
        {filterFromParent && filterFromParent.detail && filterFromParent.detail.categories.
        slice(0, 5).map((listItem) => (
          <div key={listItem._id}>
            <ListItem className="listItemName" button key={listItem._id} onClick={event => this.handleToggle(event, listItem._id)} style={{ paddingTop: "0px", paddingBottom: "0px" }}>

              <Checkbox

                onClick={(e) => {
                  this.handleCategories('category_id', e);
                }}
                value={listItem._id}
                color="primary"
                checked={this.getChecked(listItem._id) > -1 ? true : false}
                tabIndex="-1" />

              <ListItemText className="" primary={listItem.name} />

            </ListItem>

          </div>
        ))}

        <div className="ml-4 mb-3">
          <button className="moreButton" id="Categories" onClick={this.toggleCategoryPopOver}>
            More
              </button>
          <Popover trigger="legacy" className="manufacturersPopOver" placement="right" isOpen={this.state.popoverOpenCategories} target="Categories" toggle={this.toggleCategoryPopOver}>
            <i class="zmdi zmdi-close zmdi-hc-2x" style={{ float: "right", cursor: "pointer", zIndex: 1, }} onClick={this.toggleCategoryPopOver}></i>
            <CategoryPopOver
              match={this.props.match}
              history={this.props.history}
              handleCategories={this.handleCategories}
              getChecked={this.getChecked}
              category_id={this.state.obj.category_id}
              showAddToCard={this.props.match.url !== '/view-company'}
              dataFromParent={categories} />
          </Popover>
        </div>

        {/* //------------------------------------------- COMAPNY ------------------------------------------------------------// */}

        <ListSubheader className="text-muted bg-grey lighten-4">Companies</ListSubheader>
        {filterFromParent && filterFromParent.detail && filterFromParent.detail.comapnies.
        slice(0, 5).map((listItem) =>
          (
            <div key={listItem._id}>

              <ListItem className="listItemName" button key={listItem._id} onClick={event => this.handleToggle(event, listItem._id)} style={{ paddingTop: "0px", paddingBottom: "0px", fontSize: "10px" }}>

                {/* <Checkbox trigger="legacy" onClick = {(e) => this.handleClick('company_id',listItem._id)} color="primary" checked={this.state.obj.company_id==listItem._id} tabIndex="-1" /> */}

                <Radio color="primary"
                  checked={this.state.obj.company_id == listItem._id}
                  onClick={(e) => {
                    this.handleClick('company_id', listItem._id);
                  }}

                  value="a" name="radio button demo"
                  aria-label="A" />

                <ListItemText primary={listItem.name} />

              </ListItem>

            </div>
          ))}

        <div className="ml-4 mb-3">
          <button className="moreButton" id="Companies" onClick={this.toggleCompaniesPopOver}>
            More
              </button>
          <Popover trigger="legacy" className="manufacturersPopOver" placement="right" isOpen={this.state.popoverOpenCompanies} target="Companies" toggle={this.toggleCompaniesPopOver}>
            <i class="zmdi zmdi-close zmdi-hc-2x" style={{ float: "right", cursor: "pointer", zIndex: 1, }} onClick={this.toggleCompaniesPopOver}></i>
            <ManufacturersPopOver
              match={this.props.match}
              history={this.props.history}
              handleClick={this.handleClick}
              company_id={this.state.obj.company_id}
              showAddToCard={this.props.match.url !== '/view-company'}
              dataFromParent={companies} />
          </Popover>
        </div>

        {/* //------------------------------------------- SELLER ------------------------------------------------------------// */}


        <ListSubheader className="text-muted bg-grey lighten-4">Sellers</ListSubheader>
        {filterFromParent && filterFromParent.detail && filterFromParent.detail.sellers && filterFromParent.detail.sellers.
          slice(0, 5).map((listItem) =>
            (
              <div key={listItem._id}>
                <ListItem className="listItemName" button key={listItem._id} onClick={event => this.handleToggle(event, listItem._id)} style={{ paddingTop: "0px", paddingBottom: "0px" }}>

                  {/* <Checkbox onClick = {(e) => this.handleClick('seller_id',listItem._id)} color="primary" checked={this.state.obj.seller_id==listItem._id} tabIndex="-1" /> */}

                  <Radio color="primary"
                    checked={this.state.obj.seller_id == listItem._id}
                    onClick={(e) => {
                      this.handleClick('seller_id', listItem._id);
                    }}
                    value="a" name="radio button demo"
                    aria-label="A" />

                  <ListItemText primary={listItem.company_name} />

                </ListItem>

              </div>
            ))}

        {filterFromParent && filterFromParent.detail && filterFromParent.detail.sellers && filterFromParent.detail.sellers.length > 5 ?
          <div className="ml-4 mb-3">
            <button className="moreButton" id="Sellers" onClick={this.toggleSellersPopOver} >
              More
              </button>
            <Popover trigger="legacy" className="manufacturersPopOver" placement="right" isOpen={this.state.popoverOpenSellers} target="Sellers" toggle={this.toggleSellersPopOver}>
              <i class="zmdi zmdi-close zmdi-hc-2x" style={{ float: "right", cursor: "pointer", zIndex: 1, }} onClick={this.toggleSellersPopOver}></i>
              <SellerPopOver
                match={this.props.match}
                history={this.props.history}
                handleClick={this.handleClick}
                seller_id={this.state.obj.seller_id}
                showAddToCard={this.props.match.url !== '/view-company'}
                dataFromParent={sellers} />
            </Popover>
          </div>
          :
          null
        }

      </List>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { searchProduct } = buyer;
  return { searchProduct }
};

export default withRouter(connect(mapStateToProps, { getSearchProduct })(PinnedSubheaderList));
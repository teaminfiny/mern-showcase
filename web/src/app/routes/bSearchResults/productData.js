const productData = [
    {
      thumb: require('../../../assets/images/0_std.jpg'),
      name: 'Alarm Clock',
      variant: 'Gold ',
      mrp: '$990 ',
      price: '$699 ',
      offer: '29 %',
      reviews: [
        {
          rating: 5,
          count: 3
        },
        {
          rating: 4,
          count: 5
        },
        {
          rating: 3,
          count: 5
        },
        {
          rating: 2,
          count: 0
        },
        {
          rating: 1,
          count: 3
        },
      ],
      rating: 5,
      seller:'Sony',
      description: "Horo is a home grown brand with utmost emphasis on quality goods to users... ",
    }, {
      thumb: require('../../../assets/images/5f94b8b5e5565b1d9ff9f455083964829845d50e.jpeg'),
      name: 'Bizinto 1 Three Pin',
      variant: 'White',
      mrp: '$490 ',
      price: '$399 ',
      offer: '29 %',
      reviews: [
        {
          rating: 5,
          count: 3
        },
        {
          rating: 4,
          count: 5
        },
        {
          rating: 3,
          count: 5
        },
        {
          rating: 2,
          count: 0
        },
        {
          rating: 1,
          count: 3
        },
      ],
      rating: 4,
      seller:'Sony',
      description: "Bizinto is an indirectly manufacture of Power strip in Delhi and supplying...",
    }, 
  ];
  
  export default productData;
  
import React, { Component } from 'react';
import About from "./About";
import Category from './categoryList'
import './index.css'

import { connect } from 'react-redux';

import {
  getSearchProduct,
  getFilter,
} from '../../../actions/buyer';


class SearchResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      obj: { search: '', company_id: '', category_id: '', seller_id: '' },
      categoryName: '',
      categoryName: '',
      sellername: '',
      sortBy:'price'

    }
    this.myRef = React.createRef()
  }
  handleChange = () => {

  }

  handleScroll = () => {
    const { index, selected } = this.props
    if (index === selected) {
      this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }

  componentDidMount() {
    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    const data = {
      key: decodedURL.search ? decodedURL.search : '',
      company_id: decodedURL.company_id ? decodedURL.company_id : '',
      category_id: decodedURL.category_id ? decodedURL.category_id.split('%2C') : [],
      seller_id: decodedURL.seller_id ? decodedURL.seller_id : '',
      mediType: decodedURL.medicineType ? decodedURL.medicineType : '',
      discount: decodedURL.discount && decodedURL.discount === '10 or more' ? 10 : decodedURL.discount === '20 or more' ? 20 : decodedURL.discount === '50 or more' ? 50 :'',
      page: 1,
      perPage: 12,
      sortBy: decodedURL.sortBy ? decodedURL.sortBy :'price'
    }
    this.setState({ sortBy: decodedURL.sortBy ? decodedURL.sortBy :'price' })
    this.props.getSearchProduct({ data })
    this.handleScroll()
    this.props.getFilter({})
  }

  render() {
    const { searchProduct, filter } = this.props;


    return (
      <div className="app-wrapper" ref={this.myRef}>
        <div className="jr-profile-content">
          <div className="row">
            <div className="col-xl-3 col-lg-3 col-md-3 col-12 d-none d-sm-block">
              <div className="jr-card jr-full-card ">
                <Category
                  // typeOfFilter = {this.filterInfo}
                  filterFromParent={filter} />
              </div>
            </div>
            <div className="col-xl-9 col-lg-9 col-md-9 col-12">
              <About
                location={this.props.location}
                match={this.props.match}
                history={this.props.history}
                sortBy={this.state.sortBy}
                showAddToCard={this.props.match.url !== '/view-company'}
                dataFromParent={searchProduct} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { searchProduct, filter } = buyer;
  return { searchProduct, filter }
};

export default connect(mapStateToProps, { getSearchProduct, getFilter })(SearchResults);
import React from 'react';
import { withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { Col, Row } from 'reactstrap';
import { FormGroup } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { required, number0 } from '../../../constants/validations'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from '../../../constants/axios'
import {NotificationManager} from 'react-notifications';
import { getShortProducts } from '../../../actions/buyer'

class ShortBookPopOver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productId: '',
      productName:'',
      manufacturer:'',
      chemCombination:'',
      quantity: 0,
      error: false,
      errorMessage: {},
      loader: false,
      options:[],
      key:''
    }
  }

  onSubmit = async(e) => {
    e.preventDefault()
    const { productId, productName, quantity } = this.state;
    let data = {
            product:productName.toUpperCase(), 
            id: productId ,
            quantity: Number(quantity)
        }
    let errP = required(productName);
    let errQ = number0(quantity);
    if(errP || errQ){
        this.setState({error: true,errorMessage:{product:errP, quantity:errQ}});
    }
    else{
        this.setState({ loader: true })
        await axios.post('shortBook/addShortBook', data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('buyer_token')
            }
        }
        ).then(result => {
            if (result.data.error) {
                NotificationManager.error(result.data.title);
            } else {
                NotificationManager.success(result.data.title);
            }
            if(this.props.onAdd) { this.props.onAdd() }
            this.onClose()
        })
        .catch(error => {
            NotificationManager.error('Something went wrong, Please try again')
            if(this.props.onAdd) { this.props.onAdd() }
            this.onClose()
        });
    }
  }

  onClose = async() => {
    await  this.setState({
        productId: this.props.product && this.props.product._id ? this.props.product._id : '',
        productName: this.props.product && this.props.product.name ? this.props.product.name : '',
        manufacturer: this.props.product && this.props.product.company_id && this.props.product.company_id.name ? this.props.product.company_id.name : '',
        chemCombination: this.props.product && this.props.product.chem_combination ? this.props.product.chem_combination : '',
        quantity: 0,
        error: false,
        errorMessage: {},
        loader:false,
        key: this.props.product && this.props.product.name ? this.props.product.name : '',
      });
      this.props.toggle();
      let data = {key: this.state.key}
      await this.props.getShortProducts({data, history:this.props.history})
  }

  handleChange = (e, key) => {
      this.setState({[key]: e.target.value})
  }

  handleInputChange = async(e, value) =>{
    if(!this.props.product || this.props.product.name !== value){
      await this.setState({ 
        key:value,
        productId:'',
        productName: '',
        manufacturer:'',
        chemCombination: ''
      })
    }
    let data = {key: value}
    await this.props.getShortProducts({data, history:this.props.history})
  }

  selectChanged = (e, product) =>{
      if(product){
        this.setState({
            productId:product._id,
            productName: product.name,
            manufacturer:product.company_id.name,
            chemCombination: product.chem_combination
          })
      }
      else{
        this.setState({
          productId:'',
          productName: '',
          manufacturer:'',
          chemCombination: ''
        })
      }
  }

  componentDidMount = async() => {
      if(this.props.product){
        await this.setState({
            productId: this.props.product._id,
            productName:this.props.product.name,
            manufacturer:this.props.product.company_id.name,
            chemCombination:this.props.product.chem_combination,
            key:this.props.product.name
        })
      }
      let data = {key: this.state.key}
      await this.props.getShortProducts({data, history:this.props.history})
  }

  render() {
    const { companies, open, shortProducts } = this.props
    const { productId, productName, quantity, error, errorMessage, manufacturer, chemCombination, options} = this.state
    console.log('popoveropen',companies,'open',open)
    let nmae = this.state.productName && shortProducts.find(v => (v.name).toLowerCase() === (this.state.productName).toLowerCase() )
    console.log('aofnmaoslmckas',this.state,this.state.productName ? shortProducts.find(v => (v.name).toLowerCase() === (this.state.productName).toLowerCase() ) : 'Select Product')

    return (
        <React.Fragment>
            <Dialog open={open} onClose={this.onClose} fullWidth={true} maxWidth={'sm'}>
            <DialogTitle>
                Add To Shortbook
            </DialogTitle>
            {this.state.loader ? 
                <div className="loader-view">
                    <CircularProgress />
                </div>
            :
                <form onSubmit={this.onSubmit} autoComplete={false}>
                <DialogContent>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <Autocomplete
                                    options={shortProducts}
                                    id="auto-complete"
                                    onInputChange={(e, val) => this.handleInputChange(e, val)}
                                    getOptionLabel={(option) =>{return option ? option.name : 'Select Product'}}
                                    autoComplete
                                    onChange = {(e, option) => this.selectChanged(e, option)}
                                    value={this.state.productName ? shortProducts.find(v => v.name === this.state.productName ) : 'Select Product'}
                                    includeInputInList
                                    renderInput={(params) => <TextField {...params}
                                                            name="name" 
                                                            fullWidth={true}
                                                            label="Product Name"
                                                            value={this.state.key}
                                                            error={error && errorMessage.product ? error : false}
                                                            helperText={error && errorMessage.product ? errorMessage.product : ''}/>}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <TextField name="manufacturer" 
                                    fullWidth={true}
                                    type="text"
                                    label="Manufacturer"
                                    autoComplete="off"
                                    value={manufacturer}
                                    disabled={true}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <TextField name="chemCombination" 
                                    fullWidth={true}
                                    type="text"
                                    label="Chemical Combination"
                                    autoComplete="off"
                                    value={chemCombination}
                                    disabled={true}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <TextField name="quantity" 
                                    fullWidth={true}
                                    type="text"
                                    label="Quantity"
                                    autoComplete="off"
                                    error={error && errorMessage.quantity ? error : false}
                                    helperText={error && errorMessage.quantity ? errorMessage.quantity : ''}
                                    inputMode="numeric"
                                    value={quantity}
                                    onChange={(event) => this.handleChange(event, 'quantity')}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.onClose} color='secondary' >
                        Cancel
                    </Button>
                    <Button type='submit' color='primary'>
                        Add
                    </Button>

                </DialogActions>
                </form>
            }
            </Dialog>
        </React.Fragment>
    );
  }
}
const mapStateToProps = ({ buyer }) => {
    const { shortProducts} = buyer;
    return { shortProducts }
  };
  
export default withRouter(connect(mapStateToProps, {getShortProducts})(ShortBookPopOver));



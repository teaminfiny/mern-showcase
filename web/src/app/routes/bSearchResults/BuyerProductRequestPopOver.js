import React from 'react';
import Button from '@material-ui/core/Button';
import { Col, Row } from 'reactstrap';
import { FormGroup } from 'reactstrap';
import TextField from '@material-ui/core/TextField';
import { required, number0 } from '../../../constants/validations'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from '../../../constants/axios'
import {NotificationManager} from 'react-notifications';
import Autocomplete from '@material-ui/lab/Autocomplete';

class BuyerProductRequestPopOver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: '',
      manufacturer: '',
      quantity: 0,
      error: false,
      errorMessage: {},
      loader: false,
      otherManufacturer:'',
      hidden:true
    }
  }

  onSubmit = async(e) => {
    e.preventDefault()
    const { product, manufacturer, quantity, otherManufacturer } = this.state;
    let data = {
            product:product.toUpperCase(), 
            manufacturer:manufacturer, 
            quantity: Number(quantity),
            otherManufacturer:otherManufacturer
        }
    let errP = required(product);
    let errM = required(manufacturer);
    let errO = manufacturer === 'others' ? required(otherManufacturer) : null ;
    let errQ = number0(quantity);
    if(errP || errM || errQ || errO){
        this.setState({error: true,errorMessage:{product:errP, manufacturer:errM, quantity:errQ, otherManufacturer:errO}});
    }
    else{
        this.setState({ loader: true })
        await axios.post('/buyerRequest/createBuyerRequest', data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('buyer_token')
            }
        }
        ).then(result => {
            if (result.data.error) {
                NotificationManager.error(result.data.title);
            } else {
                NotificationManager.success(result.data.title);
            }
            this.onClose()
        })
        .catch(error => {
            NotificationManager.error('Something went wrong, Please try again')
            this.onClose()
        });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.product !== nextProps.product) {
      let product = nextProps.product;
      this.setState({ product: product })
    }
  }

  onClose = () => {
      this.setState({
        product: this.props.product ? this.props.product : '',
        manufacturer: '',
        quantity: 0,
        error: false,
        errorMessage: {},
        loader:false,
        otherManufacturer:'',
        hidden:true
      });
      this.props.toggle();
  }

  handleChange = (e, key) => {
      this.setState({[key]: e.target.value})
  }

  selectChanged = (e, company) =>{
    if(company && company._id){
      this.setState({
          manufacturer:company._id,
          hidden:true
        })
    }
    else if(company && company === 'Other'){
        this.setState({
            manufacturer:'others',
            hidden:false
        })
    }
    else{
      this.setState({
        manufacturer:'',
        hidden:true
      })
    }
}

  componentDidMount(){
      this.setState({product: this.props.product ? this.props.product : ''})
  }

  render() {
    const { companies, open } = this.props
    const { product, manufacturer, hidden, otherManufacturer, quantity, error, errorMessage} = this.state

    

    return (
        <React.Fragment>
            <Dialog open={open} onClose={this.onClose} fullWidth={true} maxWidth={'sm'}>
            <DialogTitle>
                Create Product Request
            </DialogTitle>
            {this.state.loader ? 
                <div className="loader-view">
                    <CircularProgress />
                </div>
            :
                <form onSubmit={this.onSubmit} autoComplete={false}>
                <DialogContent>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <TextField name="product" 
                                    fullWidth={true}
                                    type="text"
                                    label="Product Name"
                                    autoComplete="off"
                                    error={error && errorMessage.product ? error : false}
                                    helperText={error && errorMessage.product ? errorMessage.product : ''}
                                    value={product}
                                    onChange={(event) => this.handleChange(event, 'product')}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <Autocomplete
                                    options={['Other',...companies]}
                                    id="auto-complete"
                                    getOptionLabel={(option) =>{return option && option === 'Other' ? 'OTHER' : option ? option.name : 'Select Manufacturer'}}
                                    autoComplete
                                    onChange = {(e, option) => this.selectChanged(e, option)}
                                    includeInputInList
                                    renderInput={(params) => <TextField {...params}
                                                            name="manufacturer" 
                                                            fullWidth={true}
                                                            label="Manufacturer"
                                                            error={error && errorMessage.manufacturer ? error : false}
                                                            helperText={error && errorMessage.manufacturer ? errorMessage.manufacturer : ''}/>}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <TextField name="otherManudacturer" 
                                    fullWidth={true}
                                    type="text"
                                    label="Other Manufacturer"
                                    autoComplete="off"
                                    hidden={hidden}
                                    error={error && errorMessage.otherManufacturer ? error : false}
                                    helperText={error && errorMessage.otherManufacturer ? errorMessage.otherManufacturer : ''}
                                    inputMode="numeric"
                                    value={otherManufacturer}
                                    onChange={(event) => this.handleChange(event, 'otherManufacturer')}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                            <FormGroup>
                                <TextField name="quantity" 
                                    fullWidth={true}
                                    type="text"
                                    label="Quantity"
                                    autoComplete="off"
                                    error={error && errorMessage.quantity ? error : false}
                                    helperText={error && errorMessage.quantity ? errorMessage.quantity : ''}
                                    inputMode="numeric"
                                    value={quantity}
                                    onChange={(event) => this.handleChange(event, 'quantity')}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.onClose} color='secondary' >
                        Cancel
                    </Button>
                    <Button type='submit' color='primary'>
                        Request
                    </Button>

                </DialogActions>
                </form>
            }
            </Dialog>
        </React.Fragment>
    );
  }
}

export default BuyerProductRequestPopOver



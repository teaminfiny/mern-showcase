import React from "react";
import AppBar from '@material-ui/core/AppBar';
import PropTypes from 'prop-types';
import { aboutList } from '../../../app/routes/socialApps/routes/Profile/data'
import { withStyles } from '@material-ui/core/styles';
import UserProfileCard from 'components/dashboard/Common/userProfileCard/UserProfileCard';
import ListCard from 'components/dashboard/Common/userProfileCard/ListCard';
import { Button, Col, Row } from 'reactstrap';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import './index.css'
import { connect } from 'react-redux';
import {
  getSearchProduct,
  getFilter,
} from '../../../actions/buyer';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import BuyerProductRequestPopOver from './BuyerProductRequestPopOver';
import { getUserDetail } from 'actions';
import ShortBookPopOver from './ShortBookPopOver';
import { isMobile } from 'react-device-detect';
function TabContainer({ children, dir }) {
  return (
    <div dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
class About extends React.Component {
  constructor(props) {
    super(props);
  let decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
  this.state = {
    aboutList,
    value: 0,
    loading: false,
    hidden: false,
    perPage: 12,
    sortBy: decodedURL.sortBy ? decodedURL.sortBy : 'price',
    popOverOpen: false,
    list: false
  };
  this.toggle = this.toggle.bind(this);
}

  seeMoreProducts = () => {

    this.setState(prevState => {
      return { perPage: Number(prevState.perPage) + 12 }
    })

      const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
      const data = {
        key: decodedURL.search ? decodedURL.search : '',
        company_id: decodedURL.company_id ? decodedURL.company_id : '',
        category_id: decodedURL.category_id ? decodedURL.category_id.split(',') : [],
        seller_id: decodedURL.seller_id ? decodedURL.seller_id : '',
        mediType: decodedURL.medicineType ? decodedURL.medicineType :'',
        discount: decodedURL.discount && decodedURL.discount === '10 or more' ? 10 : decodedURL.discount === '20 or more' ? 20 : decodedURL.discount === '50 or more' ? 50 :'',
        page: 1,
        perPage: Number(this.state.perPage) + 12,
        sortBy:this.state.sortBy ? this.state.sortBy : 'price'
      }
      this.props.getSearchProduct({ data })

    // this.setState({ loading: true })
    // const a = setTimeout(() => {
    //   this.setState({ loading: false })
    // }, 2000);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  componentDidMount() {
    this.props.getFilter({})
  }

  toggle = () => {
    this.setState({popOverOpen:!this.state.popOverOpen})
  }
  

  handleSortChange = (e,key) => {
    this.setState({ sortBy: e.target.value });
    let url2 = this.props.location.pathname;
    let strip = url2.split('/');
    let catN = strip ? strip[3] ? strip[3] : 'category' : 'category' ;
    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    let obj = {}
    obj.search= decodedURL.search ?  decodedURL.search :'';
    obj.sortBy= e.target.value ;
    obj.company_id=decodedURL.company_id ? decodedURL.company_id : ''; 
    obj.seller_id= decodedURL.seller_id ? decodedURL.seller_id : ''; 
    obj.category_id= decodedURL.category_id ? decodedURL.category_id.split(',') : [];
    obj.medicineType = decodedURL.medicineType ? decodedURL.medicineType :'';
    obj.discount= decodedURL.discount ? decodedURL.discount :'';
    const data = {
      key: obj.search ? obj.search : '',
      company_id: obj.company_id ? obj.company_id : '',
      category_id: obj.category_id,
      seller_id: obj.seller_id ? obj.seller_id : '',
      mediType: obj.medicineType ? obj.medicineType :'',
      discount: obj.discount && obj.discount === '10 or more' ? 10 : obj.discount === '20 or more' ? 20 : obj.discount === '50 or more' ? 50 :'',
      page: 1,
      perPage: 12,
      sortBy: obj.sortBy
    }
    let catName =  catN && ((catN).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase();
    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    this.props.history.push({ pathname: `/search-results/${catName && catName !== '' ? catName : 'category'}/${url}`, state: 'searchPage'})
    this.props.getSearchProduct({ data })
  }
  changeView = () => {
    this.setState({ list: !this.state.list });
  };

  render() {
    const { theme, match } = this.props;
    const { value, aboutList, loading, hidden, perPage,sortBy, list } = this.state;
    const { dataFromParent, filter ,loader, searchProduct} = this.props;
    console.log('asd=-=-=-=dataFromParent',searchProduct)
    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    let searchedText = decodedURL.search

    const categoryName = filter && filter.detail && filter.detail.categories.map(function (e) { return e._id; }).indexOf(decodedURL.category_id);
    const companyName = filter && filter.detail && filter.detail.comapnies.map(function (e) { return e._id; }).indexOf(decodedURL.company_id);
    const SellerName = filter && filter.detail && filter.detail.sellers.map(function (e) { return e._id; }).indexOf(decodedURL.seller_id);


    return (
      <div className="w-100">
        <AppBar position="static" color="default">
          {/* <Tabs variant="fullWidth" textColor='primary' indicatorColor='primary' className="jr-tabs-center" value={value} onChange={this.handleChange}> */}
            <Row>
              <Col sm={8} md={8} lg={8} xl={8} className='pr-0' style={{paddingLeft:'50px', paddingRight:'50px', margin:'auto'}}>
                {
                  decodedURL.medicineType ?
                    <span className='text-primary' style={{marginTop:'20px', padding:0}} > 
                      {`${decodedURL.medicineType} ( ${dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].metadata[0] && dataFromParent.products[0].metadata[0].total ? dataFromParent.products[0].metadata[0].total : 0} )` }
                    </span>
                  :
                  <span className='text-primary' style={{marginTop:'20px', padding:0}} >
                    {`${dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].metadata[0] && dataFromParent.products[0].metadata[0].total ? dataFromParent.products[0].metadata[0].total : 0} 
                    results  
                    ${decodedURL.search ? " for " + decodedURL.search : ''}` } 
                  </span>
                }
            <span className='text-primary' style={{marginBottom:'20px', padding:0}} >
            {`
            ${decodedURL.discount && decodedURL.discount==='10 or more' ? " Of discount 10% off or more " : decodedURL.discount==='20 or more' ? " Of discount 20% off or more " : decodedURL.discount==='50 or more' ? " Of discount 50% off or more " : ''}     
            ${filter && filter.detail && filter.detail.categories[categoryName] && filter.detail.categories[categoryName].name ? " Of category " + [filter.detail.categories[categoryName].name] : ''}         
            ${filter && filter.detail && filter.detail.comapnies[companyName] && filter.detail.comapnies[companyName].name ? " By " + filter.detail.comapnies[companyName].name : ''}      
           ${filter && filter.detail && filter.detail.sellers[SellerName] && filter.detail.sellers[SellerName].company_name ? " Sold by " + filter.detail.sellers[SellerName].company_name : ''}`
              }
            </span></Col>
            {!isMobile && <Col sm={1} md={1} lg={1} xl={1} style={{padding: '10px 0 0 40px'}}>
              { !list ?
                <IconButton className="icon-btn text-primary" onClick={this.changeView}>
                <i class="fas fa-list-alt text-primary" style={{ fontSize: '20px', cursor: 'pointer'}} ></i></IconButton> :
                <IconButton className="icon-btn text-primary" onClick={this.changeView}>
                <i class="fas fa-th text-primary" style={{ fontSize: '20px', cursor: 'pointer'}} ></i></IconButton>
              }
            </Col>}
            <Col sm={3} md={3} lg={3} xl={3} className='pt-2 pb-1 pr-4 m-auto'>
              <Select
                value={decodedURL.sortBy ? decodedURL.sortBy : 'price'}
                // autoWidth={true}
                onChange={(e) => this.handleSortChange(e, 'sortBy')}
                input={<Input disableUnderline={true} className="form-control fullWidth" id="Filters" />}
                MenuProps={{
                  PaperProps: {
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                      width: 200
                    },
                  },
                }}
              >
                <MenuItem value='name'>Name </MenuItem>
                <MenuItem value='price'>Best Price </MenuItem>
                <MenuItem value='discountHighToLow'>Discount (High To Low) </MenuItem>
                <MenuItem value='discountLowToHigh'>Discount (Low To High) </MenuItem>
              </Select>
            </Col>
          </Row>
        </AppBar>

        {/* <div className="jr-tabs-classic"> */}
          <div className="pt-3">
          {/* dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data ? */}
            {
              this.props.loading ?
                <div className="loader-view"
                  style={{ height: this.props.width >= 1200 ? 'calc(100vh - 259px)' : 'calc(100vh - 238px)' }}>
                  <CircularProgress />
                </div> 
            : 

             dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data.length > 0 ?

                <div className="row">
                  {dataFromParent && dataFromParent.products && dataFromParent.products[0] && dataFromParent.products[0].data.map((product, index) =>
                  
                    <React.Fragment key={product._id}>
                    { 
                       list ?
                       <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 pr-4 pl-4">
                          <ListCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} history={this.props.history}/> 
                      </div> :
                      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                        {/* {
                          this.props.history.location.pathname === '/view-seller' ?
                            <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                            : */}
                            <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        {/* } */}
                      </div>
                    }
                      
                    </React.Fragment>
                  )}
                </div>

                :

                dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data.length < 1 ?
                  <React.Fragment>
                    <div style={{ textAlign: "center", marginTop: "100px", marginBottom: "100px" }}>
                      <span className="text-danger" >
                        <i class="zmdi zmdi-alert-circle animated wobble zmdi-hc-5x" style={{ marginBottom: "50px" }}></i>
                      </span>
                      {
                        decodedURL.medicineType && searchedText === '' && !decodedURL.company_id && !decodedURL.seller_id && !decodedURL.category_id && !decodedURL.discount ?
                          <h1>Medicines For Medicine Type {decodedURL.medicineType} Are Coming Soon!</h1>
                        :
                          <h1>Not Available!</h1>
                      }
                    </div>
                    {
                      dataFromParent && dataFromParent.inSystem === false && this.props.users && !decodedURL.medicineType && !decodedURL.company_id && !decodedURL.seller_id && !decodedURL.category_id && !decodedURL.discount ?
                      <React.Fragment>
                        <div style={{ textAlign: "center" }}>
                          <Button color='primary' onClick={this.toggle}>Create Product Request</Button>
                        </div>
                            <BuyerProductRequestPopOver 
                              product = {searchedText}
                              open= {this.state.popOverOpen}
                              toggle = {this.toggle}
                              companies = {filter && filter.detail && filter.detail.comapnies ? filter.detail.comapnies : []}
                            />
                      </React.Fragment>
                      :
                      !loader && dataFromParent && searchProduct && dataFromParent.searchedProduct && Object.keys(searchProduct.searchedProduct).length > 0 &&  dataFromParent.inSystem === true && this.props.users && !decodedURL.medicineType && !decodedURL.company_id && !decodedURL.seller_id && !decodedURL.category_id && !decodedURL.discount ?
                      <React.Fragment>
                        <div style={{ textAlign: "center" }}>
                          <Button color='primary' onClick={this.toggle}>Add To Shortbook</Button>
                        </div>
                            <ShortBookPopOver 
                              open= {this.state.popOverOpen}
                              toggle = {this.toggle}
                              product = {dataFromParent.searchedProduct}
                            />
                      </React.Fragment>
                      :null
                    }
                  </React.Fragment>
                  :
                  null
            }

          </div>
          {
            dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data ?
              !hidden ?
                <div style={{ textAlign: "center", marginBottom: "20px" }}>
                  {(dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].metadata[0] && dataFromParent.products[0].metadata[0].total >= perPage) ?
                    <Button color="primary" className="jr-btn jr-btn-lg btnPrimary" onClick={this.seeMoreProducts}>
                      See More
                    </Button>
                    :
                    dataFromParent.products[0].data.length > 1 ?
                      <Button color="primary" className="jr-btn jr-btn-lg btnPrimary">No More Products</Button>
                      :
                      null
                  }
                </div>
                :
                <div className="loader-view" style={{ textAlign: "center", marginBottom: "20px", marginTop: "-20px" }}>
                  <CircularProgress />
                </div>
              :
              null
          }



        {/* </div> */}
      </div>
    );
  }
}




const mapStateToProps = ({ buyer, seller, auth }) => {
  const { userDetails } = seller
  const { user_details } = auth;
  const { searchProduct, filter,loading,loader } = buyer;
  let users = userDetails ? userDetails : user_details
  return { searchProduct, filter,loading,loader, user_details, users}
};



export default connect(mapStateToProps, { getSearchProduct, getFilter, getUserDetail })(withStyles(null, { withTheme: true })(About));
import React, { Component } from 'react';
import './index.css'
import Radio from '@material-ui/core/Radio';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { getSearchProduct } from 'actions/buyer';

class SellerPopOver extends Component {

  constructor() {
    super()
    this.state = {
      manufacturers: [],
      searchManufacturer: '',

      checked: [0],
      value: { min: 0, max: 10 },
      value1: { min: 0, max: 500 },
      obj: { search: '', company_id: '', category_id: '', seller_id: '' },
      popoverOpenCompanies: false,
      popoverOpenCategories: false,
      popoverOpenSellers: false,
    }
  }

  handleSearch = (e) => {
    this.setState({
      searchManufacturer: e.target.value
    })
  }


  componentDidMount() {
    const { dataFromParent } = this.props;
    this.setState({ manufacturers: dataFromParent })

    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    let obj = { ...this.state.obj }
    obj.search = decodedURL.search ? decodedURL.search : ''
    obj.company_id = decodedURL.company_id ? decodedURL.company_id : ''
    obj.category_id = decodedURL.category_id ? decodedURL.category_id : ''
    obj.seller_id = decodedURL.seller_id ? decodedURL.seller_id : ''
    this.setState({ obj })
  }


  handleClick = (key, e) => {
    if (e !== '') {

      this.props.handleClick('seller_id', e);
    }
  }

  render() {
    const { dataFromParent } = this.props;

    return (


      <div>

        <div className=''
          style={{
            width: "90%",
            position: "sticky", top: 0,
            marginBottom: 0,
            marginLeft: -20,
            marginRight: -20,
            marginTop: -22,
          }}>

        </div>
        <hr style={{ marginTop: 58, marginLeft: -20, width: "105%" }} />


        <ul className="horizontal" style={{ fontSize: 12, marginLeft: -20, marginBottom: 0, }}>

          {dataFromParent && dataFromParent.map(sellers =>{
            let cls = sellers.company_name.length > 32 ? '' :'liCompanies'
            return <li className={cls}>

              <Radio color="primary"
                checked={this.props.seller_id == sellers._id}
                onClick={(e) => this.handleClick('seller_id', sellers._id)}
                value="a" name="radio button demo"
                aria-label="A" />

              { sellers.company_name}
            </li>
         })}

        </ul>
        
      </div>

    );
  }

}


const mapStateToProps = ({ buyer }) => {
  const { searchProduct } = buyer;
  return { searchProduct }
};

export default withRouter(connect(mapStateToProps, { getSearchProduct })(SellerPopOver));
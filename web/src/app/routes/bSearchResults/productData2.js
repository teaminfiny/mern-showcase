const productData = [
    {
     thumb: require('../../../assets/images/sep_0112_azithromycininj.jpg'),
     name: 'iPhone 7',
     variant: 'Black,500Htz',
     mrp: '$400 ',
     price: '$359 ',
     offer: '49 %',
     reviews: [
       {
         rating: 5,
         count: 3
       },
       {
         rating: 4,
         count: 5
       },
       {
         rating: 3,
         count: 5
       },
       {
         rating: 2,
         count: 0
       },
       {
         rating: 1,
         count: 3
       },
     ],
     rating: 4.2,
     seller:'Sachin',
     description: "Bluetooth speaker, Karaoke singing, Car Stereo, instrument recording etc... •",
   }, {
     thumb: require('../../../assets/images/tiganex-injection-500x500.jpeg'),
     name: 'Stonx v2.1',
     variant: 'Black',
     mrp: '$29 ',
     price: '$15 ',
     offer: '49 %',
     reviews: [
       {
         rating: 5,
         count: 3
       },
       {
         rating: 4,
         count: 5
       },
       {
         rating: 3,
         count: 5
       },
       {
         rating: 2,
         count: 0
       },
       {
         rating: 1,
         count: 3
       },
     ],
     rating: 3.1,
     seller:'Lekhraj',
     description: "1 Bluetooth Dongle, 1 Aux Cable, 1 Usb Cable, 1 Manual...",
   }, 
  ];
  
  export default productData;
  
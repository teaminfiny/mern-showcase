import React, { Component } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getSearchProduct } from 'actions/buyer';
import './index.css'


class CategoryPopOver extends Component {

  constructor() {
    super()
    this.state = {
      manufacturers: [],
      searchManufacturer: '',

      checked: [0],
      value: { min: 0, max: 10 },
      value1: { min: 0, max: 500 },
      obj: { search: '', company_id: '', category_id: '', seller_id: '' },
      popoverOpenCompanies: false,
      popoverOpenCategories: false,
      popoverOpenSellers: false,
    }
  }

  handleSearch = (e) => {
    this.setState({
      searchManufacturer: e.target.value
    })
  }


  componentDidMount() {
    const { dataFromParent } = this.props;
    this.setState({ manufacturers: dataFromParent })

    const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
    let obj = { ...this.state.obj }
    obj.search = decodedURL.search ? decodedURL.search : ''
    obj.company_id = decodedURL.company_id ? decodedURL.company_id : ''
    obj.category_id = decodedURL.category_id ? decodedURL.category_id : ''
    obj.seller_id = decodedURL.seller_id ? decodedURL.seller_id : ''
    this.setState({ obj })
  }



  handleClick = (key, e) => {
    if (e !== '') {

      this.props.handleCategories('category_id', e);
    }
  }


  render() {
    const { dataFromParent } = this.props;

    let sorted = dataFromParent && dataFromParent.sort(function (a, b) {
      var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
      if (nameA < nameB) //sort string ascending
        return -1
      if (nameA > nameB)
        return 1
      return 0 //default return value (no sorting)
    })


    return (


      <div>

        <div className=''
          style={{
            width: "90%",
            position: "sticky", top: 0,
            marginBottom: 0,
            marginLeft: -20,
            marginRight: -20,
            marginTop: -22,
          }}>

        </div>
        <hr style={{ marginTop: 58, marginLeft: -20, width: "106%" }} />


        <ul className="horizontal" style={{ fontSize: 12, marginLeft: -20, marginBottom: 0, }}>

          {sorted && sorted && sorted.map(category => <li className="liCompanies">

            <Checkbox
              onClick={(e) => this.handleClick('category_id', e)}
              color="primary"
              checked={this.props.getChecked(category._id) > -1 ? true : false}
              // checked={this.props.category_id==category._id} 
              tabIndex="-1"
              value={category._id} />
            {category.name ? category.name : category.first_name + " " + category.last_name}
          </li>
          )}

        </ul>
      </div>

    );
  }

}


const mapStateToProps = ({ buyer }) => {
  const { searchProduct } = buyer;
  return { searchProduct }
};

export default withRouter(connect(mapStateToProps, { getSearchProduct })(CategoryPopOver));
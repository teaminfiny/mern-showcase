import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import Button from '@material-ui/core/Button';
import { Col, Row} from 'reactstrap';
import customHead from 'constants/customHead'
import AxiosRequest from '../../../../sagas/axiosRequest'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import {connect} from 'react-redux'
import ReactStrapTextField from 'components/ReactStrapTextField';
import ReactStrapSelectField from 'components/reactstrapSelectField';
import {getProductRequest,addRequestProduct} from 'actions'
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { required } from 'constants/validations';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';
import axios from 'constants/axios';
import { NotificationManager } from 'react-notifications';


const columns = [
  {
    name: "title",
    label: "Title",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "company",
    label: "Company",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  
  {
    name: "requested",
    label: "Request Date",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  
  {
    name: "status",
    label: "Status",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  
  {
    name: "action",
    label: "Action",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  }
];



class ProductRequest extends Component {
  constructor(props) {
    super(props)
    this.state = {
      add : false,
      edit:false,
      gstArray:[],
      productCategoryArray:[],
      productTypeArray:[],
      open: false,
    name: '',
    chemicalCombination: '',
    expiryDate: new Date(),
    MRP: '',
    PTR: '',
    minimumOrderQty: '',
    maximumOrderQty: '',
    GST: '',
    type: '',
    productCategory: '',
    discount: '',
    percentOff: 50,
    buy: 1,
    get: 1,
    getProduct: '',
    companyName: '',
    totalQuantity: '',
    page: 0,
      searchedText: '',
      perPage: 50,
    }
  }
  
  componentDidMount=()=> {
   let data = AxiosRequest.axiosHelperFunc('get','seller/getDataforProductRequest','','');
   this.props.getProductRequest({ searchText: this.state.searchedText, page: 1, perPage: this.state.perPage })
   data.then((result)=>{
     this.setState({gstArray:result.data ? result.data.gst : [],productCategoryArray:result.data ? result.data.productCategory : [],productTypeArray:result.data ? result.data.productType : []})
   })
  }

  handleClick = (key)=>{
    this.setState({[key]:!this.state[key]})
  }
  handleRequestClose = ()=>{
    this.setState({
      add : false,
      edit:false,
      open: false,
    name: '',
    chemicalCombination: '',
    expiryDate: new Date(),
    MRP: '',
    PTR: '',
    minimumOrderQty: '',
    maximumOrderQty: '',
    GST: '',
    type: '',
    productCategory: '',
    discount: '',
    percentOff: 50,
    buy: 1,
    get: 1,
    getProduct: '',
    companyName: '',
    totalQuantity: ''
    })
    this.props.reset();
  }
  changePage = (page) => {
    let pages = page + 1
    this.props.getProductRequest({ userId: '', searchText: this.state.searchedText, page: pages, perPage: this.state.perPage })
    this.setState({ page })
  };

  changeRowsPerPage = (perPage) => {
    this.props.getProductRequest({ page: 1, perPage })
    this.setState({ page: 0, perPage })
  }

  handleSearch = (searchText) => {
    this.props.getProductRequest({ userId: '', searchText: searchText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage })
    this.setState({ searchedText: searchText })
  };
 
  onSubmit = (e)=>{
    let data = {
			name: this.state.name,
			companyName: this.state.companyName,
      chemicalCombination: this.state.chemicalCombination,
      gst: this.state.GST,
      mrp: this.state.MRP,
      ptr: this.state.PTR,
      productType: this.state.type,
      productCategory: this.state.productCategory,
      
		}
    this.props.addRequestProduct({ history: this.props.history, data, listProduct: { searchText: '', page: 1, perPage: 50 } });
    this.handleRequestClose()
    

  }
  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  handleDateChange = (date) => {
    this.setState({ expiryDate: date })
  }

  cancelClicked = async(data) => {
    let status = 'cancelled'
    let token = localStorage.getItem('token');
    let changedData={
      id : data._id,
      status : status,
    }
      await axios({
        method: 'POST',
        url:'seller/cancelProductRequest',
        data:changedData,
        headers: {
            'Content-Type': 'application/json',
            'token': token
        }
      }).then(result => {
        if (result.data.error) {
          NotificationManager.error(result.data.title)
        } else {
          NotificationManager.success(`Request cancelled sussessfully !`)
          this.props.getProductRequest({ searchText: this.state.searchedText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage})
        }
      })
        .catch(error => {
          NotificationManager.error('Something Went Wrong !')
        });
  }

  render() {
    const {handleSubmit,requestedProducts} = this.props;
    const statusStyle = (status) => {
      return status.includes("pending") ? "text-primary" : status.includes("approved") ? "text-success" : status.includes("declined") ? "text-danger" : status.includes("cancelled") ? "text-grey" : "text-primary";
    }
    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      rowsPerPage: this.state.perPage,
      page: this.state.page,
      fixedHeader: false,
      print: false,
      download: false,
      filter: false,
      sort: false,
      selectableRows: false,
      count: this.props.requestedProducts.length > 0 ? this.props.requestedProducts[0].metadata.length > 0 ? this.props.requestedProducts[0].metadata[0].total : 0 : 0,
      // rowsPerPage: this.state.perPage,
      rowsPerPage: 50,
      rowsPerPageOptions: [],
      serverSide: true,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      searchText: this.state.searchedText,
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
        }
      },
    }

    let buttonType = 'edit'
    let data = [
      
    ];
    requestedProducts.length > 0 && requestedProducts[0].data.length > 0 && requestedProducts[0].data.map((data1,index)=>{
      data.push([
        data1.name,
        data1.companyName,
        moment(data1.createdAt).format('DD/MM/YYYY HH:mm A'),
        <div className={` badge text-uppercase ${statusStyle(data1.status)}`}>{data1.status}</div>,
        <Button onClick={() => this.cancelClicked(data1)} disabled={data1.status === 'pending' ? false: true} className={data1.status === 'pending' ? 'bg-primary text-white' : 'bg-grey text-white'}>CANCEL</Button>
      ])
    })
    
    let { name, open,gstArray,productTypeArray,productCategoryArray,
      chemicalCombination, expiryDate, MRP, PTR, minimumOrderQty, maximumOrderQty, GST, type, productCategory, discount, percentOff, buy, get, getProduct, companyName, totalQuantity } = this.state;
    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Product Requests" />} />
       
        {
          this.props.loading == false ?
            <MUIDataTable
              title={<Button className={'text-primary'} onClick={() => this.handleClick('add')}> Add Product +</Button>}
              data={data}
              columns={columns}
              options={options}
            />
            :
            <div className="loader-view" style={{ textAlign: "center", marginTop: "300px" }}>
              <CircularProgress />
            </div>
        }

        <Dialog open={this.state.add?this.state.add:this.state.edit} onClose={this.handleRequestClose}
          fullWidth={true}
          maxWidth={'md'}>
          <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off">
          <DialogTitle>
            {this.state.add?'Add product request':'Edit product request'}
            <DialogContentText className='mt-3'>
            </DialogContentText>
          </DialogTitle>
          <DialogContent>
           
              <Row>
                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                  
                  <Field id="name" name="name" type="text"
                      component={ReactStrapTextField} label={"Name"}
                      validate={[required]}
                      value={name}
                      onChange={(event) => this.handleChange(event, 'name')}
                  />
                    
                  
                </Col>
                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                  <Field id="chemicalCombination" name="chemicalCombination" type="text"
                      component={ReactStrapTextField} label={"Chemical Combination"}
                      validate={[required]}
                      value={chemicalCombination}
                      onChange={(event) => this.handleChange(event, 'chemicalCombination')}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                <Field id="companyName" name="companyName" type="text"
                      component={ReactStrapTextField} label={"Company Name"}
                      validate={[required]}
                      value={companyName}
                      onChange={(event) => this.handleChange(event, 'companyName')}
                  />
                  
                </Col>
                <Col xs={12} md={6} sm={12} xl={6} lg={6}>
                  <Field id="GST" name="GST" type="select"
                  component={ReactStrapSelectField} label={"GST"}
                  validate={[required]}
                  value={GST}
                  onChange={(event) => this.handleChange(event, 'GST')}
                  >
                  <option value="">Select GST</option>
                  {
                    gstArray.map((val,index)=>
                    <option value={val._id} key={val._id}>{val.name}</option>
                  )
                  }
                  </Field>
                  
                </Col>
              </Row>
              
             

              
              <Row>
                <Col sm={12} md={6} xs={12} lg={6} xl={6}>
                <Field id="type" name="type" type="select"
                component={ReactStrapSelectField} label={"Type"}
                validate={[required]}
                value={type}
                onChange={(event) => this.handleChange(event, 'type')}
                >
                <option value="">Select Type</option>
                {
                  productTypeArray.map((val,index)=>
                  <option value={val._id} key={val._id}>{val.name}</option>
                )
                }
                </Field>
                  
                </Col>
                <Col sm={12} md={6} xs={12} lg={6} xl={6}>
                <Field id="type" name="productCategory" type="select"
                component={ReactStrapSelectField} label={"Product Category"}
                validate={[required]}
                value={productCategory}
                onChange={(event) => this.handleChange(event, 'productCategory')}
                >
                <option value="">Product Category</option>
                {
                  productCategoryArray.map((val,index)=>
                  <option value={val._id} key={val._id}>{val.name}</option>
                )
                }
                </Field>
                  
                </Col>
              </Row>


              


             
           

          </DialogContent>
          
          <DialogActions className="pr-4">

            <Button onClick={this.handleRequestClose} color='secondary' >
              Cancel
            </Button>
            <React.Fragment>
              
              <Button type = 'submit' color='primary'>
                  Add
            </Button>
             
            </React.Fragment>
            

          </DialogActions>
          </form>
        </Dialog>
         
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ seller }) => {
  const { requestedProducts, loading } = seller;
  return { requestedProducts, loading }
};

ProductRequest = connect(
  mapStateToProps,
  {
    getProductRequest,
    addRequestProduct
  }           // bind account loading action creator
)(ProductRequest)

export default ProductRequest = reduxForm({
  form: 'ProductRequest',// a unique identifier for this form
  enableReinitialize: true,
  onSubmitSuccess: (result, dispatch) => {
      const newInitialValues = getFormInitialValues('ProductRequest')();
      dispatch(initialize('ProductRequest', newInitialValues));
  }
},mapStateToProps)(ProductRequest)

import React, { Component } from "react";
import MUIDataTable from "components/DataTable"
import Button from '@material-ui/core/Button';
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import { Col, Row, Form, Badge, Label } from 'reactstrap';
import AddInventory from '../component/addInventory';
import EditInventory from '../component/editInventory';
import DeleteInventory from '../component/deleteInventory';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import customHead from 'constants/customHead'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import { connect } from 'react-redux';
import { getInventoryList } from 'actions/seller'
import './index.css'
import moment from "moment";
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';

const options = {
  filterType: 'dropdown',
  print: false,
  download: false,
  selectableRows: 'none',
  selectableRowsOnClick: false,
  selectableRowsHeader: false,
  responsive: 'scrollMaxHeight',
  search: true,
  viewColumns: false,
  rowHover: false,
  textLabels: {
    filter: {
      all: "All",
      title: "FILTERS",
    },
  },
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

class Inventory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      hide: false,
      delete: false,
      add: false,
      selectedCategory: ['Category'],
      filter: 'none',
      page: 0,
      searchedText: '',
      perPage: 50,
      editData: '',
      hide: false,
      showFilter: false,
      loader:false,
      visible:'none'
    }
  }

  button = (data) => {
    let status = data.status;
    // let disabled = (status === "Expired") ? true : false;
    // let expDate = new Date(moment(data.expiry_date)).getTime();
    let disabled = (moment(data.expiry_date).diff(moment(), 'days') <= 90) ? true : false;
    let blur = data.hiddenBy && data.hiddenBy === 'admin' ? true : false;
    console.log('aisdnaisunfcainf',disabled && blur,disabled , blur)
    return blur ? <Tooltip className="d-inline-block" id="tooltip-right" title={<h6 className="text-white"
        style={{ marginTop: "10px" }}> {'Hidden by Admin'} </h6> } placement="top">
      <ButtonGroup color="primary" aria-label="outlined primary button group">
      <Button className={blur ? 'text-grey' : 'text-primary'} disabled={blur} onClick={() => this.handleClick('edit', data)} > Edit</Button>
      <Button className={blur ? 'text-grey' : 'text-warning'} disabled={blur} onClick={() => this.handleClick('hide', data)}> {data.isHidden ? 'Unhide' : 'Hide'}</Button>
      <Button className={blur ? 'text-grey' : 'text-danger'} disabled={blur} onClick={() => this.handleClick('delete', data)}> Delete</Button>
    </ButtonGroup></Tooltip> :
      <ButtonGroup color="primary" aria-label="outlined primary button group">
      <Button className={disabled ? 'text-grey' : 'text-primary'} disabled={disabled} onClick={() => this.handleClick('edit', data)} > Edit</Button>
      <Button className={disabled ? 'text-grey' : 'text-warning'} disabled={disabled} onClick={() => this.handleClick('hide', data)}> {data.isHidden ? 'Unhide' : 'Hide'}</Button>
      <Button className={disabled ? 'text-grey' : 'text-danger'} disabled={disabled} onClick={() => this.handleClick('delete', data)}> Delete</Button>
    </ButtonGroup>
  }

  handleClick = (key, data) => {
    this.setState({ [key]: !this.state[key], editData: data })
  }
  
  handleSelectChange = (e) => {
    e.preventDefault();
    let tempSelectedCat = e.target.value;
    let index = tempSelectedCat && tempSelectedCat.length > 0 ? tempSelectedCat.findIndex((e) => e === 'Category') > -1 ? tempSelectedCat.findIndex((e) => e === 'Category') : -1 : -1;
    index > -1 && tempSelectedCat.splice(index, 1);
    this.setState({ selectedCategory: tempSelectedCat });
  }

  handleFiltersChange = (e) => {
    e.preventDefault();
    this.setState({ filter: e.target.value });
  }
  componentDidMount() {
    if(this.props.location.title && this.props.location.title === 'activeSelling'){
      this.setState({ filter: 'Active Selling Products' });
    }
    if(this.props.location.title && this.props.location.title === 'shortExpiry'){
      this.setState({ filter: 'Short expire products' });
    }
    if(this.props.location.title && this.props.location.title === 'outOfStock'){
      this.setState({ filter: 'Out of stock Products' });
    }
    if(this.props.location.title && this.props.location.title === 'slowMoving'){
      this.setState({ filter: 'Slow moving Products' });
    }

    let data = {
      page: 1,
      perPage: 50,
      filter: 
      this.props.location.title && this.props.location.title === 'activeSelling' ?  'Active Selling Products'
       : 
       this.props.location.title === 'shortExpiry' ? 'Short Expire Products' 
       :
       this.props.location.title === 'outOfStock' ? 'Out of stock Products'
       :
       this.props.location.title === 'slowMoving' ? 'Slow moving Products'
       :
       '' ,      
      searchText: '',
      selectedCategory: []
    }
    this.props.getInventoryList({ data, history: this.props.history })   
  }

  changePage = (page) => {
    let pages = page + 1;
    let tempSelectedCat = [...this.state.selectedCategory]
    let index = tempSelectedCat && tempSelectedCat.length > 0 ? tempSelectedCat.findIndex((e) => e === 'Category') > -1 ? tempSelectedCat.findIndex((e) => e === 'Category') : -1 : -1;
    index > -1 && tempSelectedCat.splice(index, 1);
    let data = {
      page: pages,
      perPage: this.state.perPage,
      searchText: this.state.searchedText,
      filter: this.state.filter === 'none' ? '': this.state.filter,
      selectedCategory: tempSelectedCat,
      visible: this.state.visible === 'visible' ? false : this.state.visible === 'hidden' ? true : ''
    }
    this.props.getInventoryList({ data, history: this.props.history })
    this.setState({ page })
    this.setState({ loader: true })
    const t = setTimeout(() => {
      this.setState({ loader: false })
    }, 3000);
  };

  changeRowsPerPage = (perPage) => {

    let tempSelectedCat = this.state.selectedCategory;
    let index = tempSelectedCat && tempSelectedCat.length > 0 ? tempSelectedCat.findIndex((e) => e === 'Category') > -1 ? tempSelectedCat.findIndex((e) => e === 'Category') : -1 : -1;
    index > -1 && tempSelectedCat.splice(index, 1);

    let data = {
      page: 1,
      perPage: perPage,
      searchText: this.state.searchedText,
      filter: this.state.filter === 'none' ? '': this.state.filter,
      selectedCategory: this.state.tempSelectedCat
    }
    this.props.getInventoryList({ data, history: this.props.history })
    this.setState({ page: 0, perPage })
  }
  handleSearch = (searchText) => {
    let data = { searchText: searchText, page: 1, perPage: this.state.perPage, visible: this.state.visible === 'visible' ? false : this.state.visible === 'hidden' ? true : '',
    filter: this.state.filter === 'none' ? '': this.state.filter,selectedCategory: this.state.selectedCategory && this.state.selectedCategory[0] == 'Category' ? [] : this.state.selectedCategory }
    this.props.getInventoryList({ data, history: this.props.history })
    this.setState({ page: 0, searchedText: searchText })
  };
  resetFilter = (filter) => {
    let data = {
      page: 1,
      perPage: 50,
      filter: '',
      visible:'',
      selectedCategory: [],
      searchText: this.state.searchedText ? this.state.searchedText : '',
    }
    this.props.getInventoryList({ data, history: this.props.history })
    filter();
    this.setState({
      page: 0,
      selectedCategory: ['Category'],
      filter: 'none',
      showFilter: false,
      visible:'none'
    })
  }
  applyFilter = (filter) => {
    filter()

    let tempSelectedCat = [...this.state.selectedCategory]
    let index = tempSelectedCat && tempSelectedCat.length > 0 ? tempSelectedCat.findIndex((e) => e === 'Category') > -1 ? tempSelectedCat.findIndex((e) => e === 'Category') : -1 : -1;
    index > -1 && tempSelectedCat.splice(index, 1);

    let data = {
      page: 1,
      perPage: this.state.perPage,
      searchText:this.state.searchedText ? this.state.searchedText : '',
      filter: this.state.filter === 'none' ? '' : this.state.filter,
      selectedCategory: tempSelectedCat,
      visible: this.state.visible === 'visible' ? false : this.state.visible === 'hidden' ? true : ''
    }
    this.props.getInventoryList({ data, history: this.props.history });
    let newSelectedCategory = this.state.selectedCategory;
    this.setState({ page: 0, selectedCategory: newSelectedCategory, showFilter: true })

  }
  handleForHidden = (e,key) => {
    this.setState({ [key]: e.target.value })
  }
  apiCall = ()=> {
    let data = {
      page: Number(this.state.page) + 1,
      perPage: this.state.perPage,
      searchText:this.state.searchedText ? this.state.searchedText : '',
      filter: this.state.filter === 'none' ? '' : this.state.filter,
      selectedCategory: this.state.selectedCategory && this.state.selectedCategory[0] == 'Category' ? [] : this.state.selectedCategory,
      visible: this.state.visible === 'visible' ? false : this.state.visible === 'hidden' ? true : ''
    }
    this.props.getInventoryList({ data, history: this.props.history });
  }
  render() {
    const { listInventory, loading } = this.props;
    const { showFilter, selectedCategory, filter,visible } = this.state;
    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      rowsPerPage: this.state.perPage,
      page: this.state.page,
      fixedHeader: false,
      print: false,
      download: false,
      filter: true,
      search: true,
      sort: false,
      selectableRows: false,
      count: this.props.listInventory.length > 0 ? this.props.listInventory[0].metadata.length > 0 ? this.props.listInventory[0].metadata[0].total : 0 : 0,
      rowsPerPage: this.state.perPage,
      serverSide: true,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      searchText: this.state.searchedText,
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
          case 'onFilterDialogClose':
            break
          case 'onFilterDialogOpen':
            break;
        }
      },
    }

    const statusStyle = (status) => {
      return status.includes("Out of Stock") ? "text-white bg-danger" : status.includes("In stock") ? "text-white bg-success" : status.includes("Expired") ? "text-white bg-grey" : status.includes("Active") ? "text-white bg-info" : "text-white bg-grey";
    }

  

    let data = [];
    listInventory.length > 0 && listInventory[0].data.length > 0 && listInventory[0].data.map((data1, index) => {
      
      data.push([
        data1.Products ? data1.Products.name : 'N.A.', "₹" + (Number(data1.MRP)).toFixed(2), 
        data1.Discounts ? data1.Discounts.type === 'Discount'
        ? <div>
        <span className={'originalPrice '}>₹{(Number(data1.PTR).toFixed(2))}</span>
        <span >₹{((Number(data1.PTR) - (Number(data1.PTR) / 100 * Number(data1.Discounts.discount_per)))).toFixed(2)}</span></div> 
        : <div>
        <span >₹{(Number(data1.PTR)).toFixed(2)}</span>
          </div> 
        : "₹" + (Number(data1.PTR)).toFixed(2),
          <div>{(data1.quantity < data1.min_order_quantity) ? <span className={` badge text-uppercase ${statusStyle("Out of Stock")}`}>Out of Stock</span> : data1.quantity}</div>, 
          <div key={'recent'} >
            {(moment(data1.expiry_date).diff(moment(), 'months') >= 9) ? 
            <span className={` badge text-uppercase ${statusStyle("In stock")}`}>Usable</span> :
            (moment(data1.expiry_date).diff(moment(), 'months') >= 3)&& (moment(data1.expiry_date).diff(moment(), 'months') <= 9) ?
            <span className={` badge text-uppercase ${statusStyle("Out of Stock")}`}>Short Expiry</span>  :
            (moment(data1.expiry_date).diff(moment(), 'months') <= 3) ?
            <span className={` badge text-uppercase ${statusStyle("Expired")}`}>Expired</span>:''
            } </div>,
          this.button(data1)])
    })
    // className={` badge text-uppercase ${statusStyle(data1.status)}`} {data1.status} :

    const columns = [
      {
        name: "title",
        label: "Title",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "MRP",
        label: "MRP",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "PTR",
        label: "PTR",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "quantity",
        label: "Quantity",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "action",
        label: "Action",
        options: {
          filter: true,
          filterType: 'custom',
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          },
          customFilterListRender: v => {
            return false;
          },
          filterOptions: {
            names: [],
            logic() {
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => (
              <React.Fragment>
                <Row form style={{ maxWidth: 300 }}>
                  <Col md={12} xl={12} xs={12} lg={12}>
                    <FormControl className="w-100 mb-2 categoryFilter">
                      <Label for="Filters">Status</Label>
                      <Select
                        value={this.state.filter}
                        onChange={(e) => this.handleFiltersChange(e)}
                        input={<Input disableUnderline={true} className="form-control" id="Filters" />}
                        MenuProps={{
                          PaperProps: {
                            style: {
                              maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                              width: 200
                            },
                          },
                        }}
                      >
                        <MenuItem value="none">
                          All
                        </MenuItem>
                        <MenuItem value='Active Selling Products'>Active Selling </MenuItem>
                        <MenuItem value='Slow moving Products'>Slow Moving </MenuItem>
                        <MenuItem value='Short expire products'>Short Expire </MenuItem>
                        <MenuItem value='Out of stock Products'>Out Of Stock </MenuItem>
                        <MenuItem value='Expired'>Expired </MenuItem>
                      </Select>
                    </FormControl>
                  </Col>
                  <Col md={12} xl={12} xs={12} lg={12}>
                    <FormControl className="w-100 categoryFilter mt-2">
                      <Label for="category">Category</Label>
                      <Select
                        multiple
                        id="selectCategory"
                        value={this.state.selectedCategory}
                        onChange={(e) => this.handleSelectChange(e)}
                        input={<Input disableUnderline={true} className="form-control " id="name-multiple" />}
                        MenuProps={{
                          PaperProps: {
                            style: {
                              maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                              width: 200
                            },
                          },
                        }}
                      >
                        <MenuItem
                          key={'select Category'}
                          value={'Category'}
                          style={{
                            fontWeight: '500',
                          }}
                        >
                          Select Category
                        </MenuItem>
                        {this.props.productCategories.map(name => (
                          <MenuItem
                            key={name.name}
                            value={name.name}
                            className='putIconAfter '
                            style={{
                              fontWeight: this.state.selectedCategory.indexOf(name.name) !== -1 ? '500' : '400',
                            }}
                          >
                            {name.name}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Col>
                  <Col md={12} xl={12} xs={12} lg={12}>
                    <FormControl className="w-100 mt-2 categoryFilter">
                      <Label for="Filters">Hidden</Label>
                      <Select
                        value={this.state.visible}
                        onChange={(e) => this.handleForHidden(e,'visible')}
                        input={<Input disableUnderline={true} className="form-control" id="Filters" />}
                        MenuProps={{
                          PaperProps: {
                            style: {
                              maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                              width: 200
                            },
                          },
                        }}
                      >
                        <MenuItem value="none">
                          All
                        </MenuItem>
                        <MenuItem value='visible'>Visible </MenuItem>
                        <MenuItem value='hidden'>Hidden </MenuItem>
                      </Select>
                    </FormControl>
                  </Col>
                </Row>
                <div style={{ paddingTop: 15 }} >
                  <Button variant="contained" onClick={() => this.applyFilter(applyFilter)} className='filterButton' color='primary'>Apply Filter</Button>
                  <Button variant="contained" onClick={() => this.resetFilter(applyFilter)} className='filterButton' color='primary'>Reset Filter</Button>
                </div>
              </React.Fragment>
            ),
            onFilterChange: () => {
            }
          },
        },
      }
    ];
    const { loader } = this.state;
    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id={showFilter ? (selectedCategory.findIndex((e) => e === 'Category') > -1 && filter === 'none') ? "Inventory" : (selectedCategory.findIndex((e) => e === 'Category') > -1 && filter !== 'none') ? `Inventory (Showing ${filter.toLowerCase()})` : (selectedCategory.findIndex((e) => e === 'Category') < 0 && filter === 'none') ? `Inventory (Showing results for ${selectedCategory.join(', ').toLowerCase()})` :
        ( selectedCategory.length == 0 && filter )?
         `Inventory (Showing ${filter.toLowerCase()}) ` :
         `Inventory (Showing ${filter.toLowerCase()} for ${selectedCategory.join(', ').toLowerCase()})`  :  'Inventory'} />} />
             
        {loader ? <div className="loader-view"
          style={{ height: this.props.width >= 1200 ? 'calc(100vh - 259px)' : 'calc(100vh - 238px)' }}>
          <CircularProgress />
        </div> : null}
      {
          loader == true ?
            null :
            <MUIDataTable
              title={<Button className={'text-primary'} onClick={() => this.handleClick('add')}> Add Inventory +</Button>}
              data={data}
              columns={columns}
              options={options}
            />
        } 

        {
          this.state.edit &&
          <EditInventory editData={this.state.editData} page={this.state.page} perPage={this.state.perPage} buttonType={'edit'} edit={this.state.edit} title={'Edit Inventory'} handleClick={this.handleClick} callMount={this.apiCall}/>
        }

        {
          this.state.add &&
          <AddInventory  page={this.state.page} perPage={this.state.perPage} buttonType={'add'} add={this.state.add} title={'Inventory'} handleClick={this.handleClick} callMount={this.apiCall}/>
        }
        {
          this.state.delete &&
          <DeleteInventory page={this.state.page} perPage={this.state.perPage} delete={this.state.delete} editData={this.state.editData} deleteFor={'inventory'} handleClick={this.handleClick} callMount={this.apiCall}/>
        }

        {
          this.state.hide &&
          <DeleteInventory page={this.state.page} perPage={this.state.perPage} delete={this.state.hide} editData={this.state.editData} hideFor={'inventory'} handleClick={this.handleClick} callMount={this.apiCall}/>
        }

      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ seller }) => {
  const { listInventory, productCategories, loading } = seller;
  return { listInventory, productCategories, loading }
};

export default connect(mapStateToProps, { getInventoryList })(Inventory);
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Col, Row } from 'reactstrap';
import { FormGroup, Label, Input, FormText } from 'reactstrap';
import moment from 'moment';
import DatePicker from "react-datepicker";
import AxiosRequest from 'sagas/axiosRequest'
import ReactStrapTextField from 'components/ReactStrapTextField';
import { NotificationManager } from 'react-notifications';
import { Field, reduxForm } from 'redux-form'
import { getInventoryList } from 'actions'
import { connect } from 'react-redux'
import { noValidations, required, minLength6, confirmPasswordMatch, validatePassword, number, number0, maxPurchase, percentageValidation, discountValidation, maxLength4, minLessAvail,maxLessAvail, noDecimal, maxGreaterMin,maxLessMin, twoDecimal, lessThenMrp} from 'constants/validations';
import AutoCompleteSearch from 'components/asyncAutocmplete';
import TextBox from 'components/textBox';
class AddInventory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      name: '',
      chemicalCombination: '',
      expiryDate: new Date(moment().add(6, 'months')),
      MRP: '',
      PTR: '',
      minimumOrderQty: '',
      maximumOrderQty: '',
      GST: '',
      GSTValue:'',
      type: '',
      productCategory: '',
      discount: 'nodiscount',
      percentOff: 1,
      buy: 1,
      get: 1,
      getProduct: '',
      companyName: '',
      totalQuantity: '',
      searchItem: '',
      options: [],
      selectedData: '',
      options2: [],
      otherSelectedProduct: '',
      otherName: '',
      searchOtherProdcut: '',
      buyMx: 35,
      getMx: 35,
      percentOffMx: 35,
      bestOffer:'',
      openDailog:false,
      ePtr:'',
      countryOfO:'',
      medicineType:'',
      isPrepaid:'',
      surCharge:''
    };
  }

  handleRequestClose = () => {
    this.setState({ open: false });

    this.handleClose(this.props.editData ? 'edit' : 'add');
  };
  closeDialog = () => {
    this.setState({ openDailog: false });
    this.handleClose()
  }
  addAnotherProduct = () => {
    const {MRP,PTR,maximumOrderQty,minimumOrderQty,totalQuantity} = this.props.form.AddInventory.values
    this.props.initialize({
      MRP: MRP,
      PTR: PTR,
      totalQuantity:totalQuantity,
      minimumOrderQty: minimumOrderQty,
      maximumOrderQty: maximumOrderQty,

      discount:'nodiscount',
      discount_name: '',
      discount_per: '',
      purchase:'',
      bonus: '',
      otherInventoryId:  '',
      discountId: '',
      percentOff: 1,
      buy: '',
      get: '',
      getProduct: ''
    })
    this.setState({
      MRP: MRP,
      PTR: PTR,
      totalQuantity:totalQuantity,
      minimumOrderQty: minimumOrderQty,
      maximumOrderQty: maximumOrderQty,

      discount:'nodiscount',
      openDailog: false,
      discount_name: '',
      discount_per: '',
      purchase:'',
      bonus: '',
      otherInventoryId:  '',
      discountId:  '',
      discountId: '',
      percentOff: 1,
      buy: '',
      get: '',
      getProduct: '',
      searchItem: '',
      options: [],
      options2: [], otherSelectedProduct: '', otherName:''
    })
  }
  onSubmit = async (e) => {
    const {MRP,PTR,maximumOrderQty,minimumOrderQty,totalQuantity} = this.props.form.AddInventory.values
    let data = {
      Id: this.props.editData ? this.props.editData._id : '',
      product_id: this.state.selectData._id,
      company_id: this.state.selectData.company_id._id,
      chemicalCombination: this.state.chemicalCombination,
      MRP: MRP,
      PTR: PTR,
      quantity:totalQuantity,
      min_order_quantity: minimumOrderQty,
      max_order_quantity: maximumOrderQty,
      expiry_date: moment(this.state.expiryDate).endOf('month').format(),
      discount_name: this.state.discount ? this.state.discount !== 'nodiscount' ? this.state.discount : "" : "",
      discount_per: this.state.percentOff,
      purchase: (this.state.discount == 'Same' || this.state.discount == 'SameAndDiscount' || this.state.discount == 'Different' || this.state.discount == 'DifferentAndDiscount') ? Number(minimumOrderQty) : Number(this.state.buy),
      bonus: Number(this.state.get),
      otherInventoryId: this.state.otherSelectedProduct ? this.state.otherSelectedProduct._id : '',
      discountId: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts._id : '' : ''

    }
    let response = await AxiosRequest.axiosHelperFunc('post', 'inventory/addInventoryItem', '', data)
    // this.handleClose()
    if (response.data.error) {
      NotificationManager.error(response.data.title)
    } else {
      NotificationManager.success(response.data.title)
      let data2 = {
        page: this.props.page === 0 ? 1 : this.props.page,
        perPage: this.props.perPage
      }
      this.bestOfferApi(response.data.detail.product_id)
      this.setState({openDailog:true})
      this.props.callMount();
      // this.props.getInventoryList({ data: data2 })
    }
  }
  handleChange = async (e, key) => {
    let data = {}
    this.setState({ [key]: e.target.value });

  }
  handleChange2 = (event, key) => {
    this.setState({ [key]: event.target.value });
  }
  handleChange3 = (event, key) => {
    this.setState({ [key]: event.target.value });
  }

  hamdleSerchAnotherProduct = async (e, val) => {
    if (val) {
      let body = { name: val }
      let data = await AxiosRequest.axiosHelperFunc('post', 'inventory/searchInventory', '', body)
      let temp = await data.data.detail
      await this.setState({ options2: temp, otherName: val })
    } else {
      this.setState({ options2: [], otherSelectedProduct: '', otherName: val })
    }
  }
  selectAnother = (data) => {

    this.setState({ otherSelectedProduct: data, otherName: data ? data.name : '' })
  }
  shouldComponentUpdate(nextProps, nextState) {
    return JSON.stringify(this.props) !== JSON.stringify(nextProps) || JSON.stringify(this.state) !== JSON.stringify(nextState);
  }
  handleSearchChange = async (e, val) => {

    if (val) {
      let body = { name: val }
      this.setState({name:val})
      let data = await AxiosRequest.axiosHelperFunc('post', 'product/searchProduct', '', body)
      let temp = []
      temp = data.data.detail
      this.setState({ options: temp, selectedData: '' })
    } else {
      this.setState({ options: [], selectedData: '',name:'' })
    }
  }
  selectData = async(data) => {
    if (data) {
      this.setState({
        selectData: data, chemicalCombination: data.chem_combination, companyName: data.company_id.name,
        GST: data.GST.name, productCategory: data.product_cat_id.name, type: data.Type.name, name: data.name,
        GSTValue:data.GST.value, countryOfO: data.country_of_origin, medicineType: data.medicine_type_id.name,
        isPrepaid:data.isPrepaid, surCharge: data.surcharge
      })
    } else {
      this.setState({
        selectData: data, chemicalCombination: '', companyName: '',
        GST: '', productCategory: '', type: '', name: '',GSTValue:'',countryOfO:'', medicineType:'',isPrepaid:''
      })
    }
    this.bestOfferApi(data && data._id)
  }

  bestOfferApi = async(data) => {
    let data1 ={Id:data && data}
    let response1 = await AxiosRequest.axiosHelperFunc('post', 'product/bestOffer', '', data1)
    let offer = response1 && response1.data && response1.data.data.slice(0, 3);
    this.setState({ bestOffer: offer })
  }

  handleDateChange = (data) => {
    this.setState({ expiryDate: data })
  }
  handleClose = () => {
    this.setState({
      open: false,
      name: '',
      chemicalCombination: '',
      expiryDate:  new Date(moment().add(6, 'months')),
      MRP: '',
      PTR: '',
      minimumOrderQty: '',
      maximumOrderQty: '',
      GST: '',
      type: '',
      productCategory: '',
      discount: 'nodiscount',
      percentOff: '',
      buy: 1,
      get: 1,
      getProduct: '',
      companyName: '',
      totalQuantity: '',
      searchItem: '',
      options: [],
      selectedData: '',
      selectData: '',
      medicineType:'',
      buyMx: 10,
      getMx: 10,
      GSTValue:'',
      isPrepaid:''
    })
    this.props.handleClick(this.props.editData ? 'edit' : 'add')
    this.props.reset()
  }
  setDiv = (div) => {
    this.div = div;
    this.setMaxWidth();
  };

  setMaxWidth = () => {
    // Make sure mobile devices don't overflow
    if (this.div) {
      this.setState({ buyMx: Math.min(340, this.div.offsetWidth) });
    }
  };



  render() {
    const { handleSubmit } = this.props;
    let { name, open, options, options2, otherSelectedProduct, otherName,
      chemicalCombination, expiryDate, GST, type, productCategory, getProduct,discount, companyName,GSTValue,bestOffer,countryOfO, surCharge } = this.state;
      let valueData = this.props.form?this.props.form.AddInventory?this.props.form.AddInventory.values?this.props.form.AddInventory.values:'':'':''
    let MRP = valueData.MRP?valueData.MRP:''
    let PTR = valueData.PTR?valueData.PTR:''
    let maximumOrderQty = valueData.maximumOrderQty?valueData.maximumOrderQty:''
    let minimumOrderQty=valueData.minimumOrderQty?valueData.minimumOrderQty:''
    let totalQuantity = valueData.totalQuantity?valueData.totalQuantity:''
    let percentOff=valueData.percentOff?valueData.percentOff:''
    let buy =valueData.buy?valueData.buy:''
    let get=valueData.get?valueData.get:''
    let { title, buttonType } = this.props;

      let samePrice = (discount === 'Same' && PTR !== "") ? Number((Number(PTR) * Number(minimumOrderQty))/(Number(minimumOrderQty) + (Number(get) ? Number(get) : ''))):0;

      let discPrice = (discount === 'Discount' && PTR !== "") ? percentOff !== '' ? 
      ( Number(PTR) - (( Number(PTR)/100) * Number(percentOff))) : Number(PTR) : 0 ;

      let noDiscPrice = ((discount === 'nodiscount'|| discount ==='Different') && PTR !== "") ? Number(PTR) :Number(PTR) ;

      let sameAndD = (discount === 'SameAndDiscount' && PTR !== "") ? Number((Number(PTR) * Number(minimumOrderQty))/(Number(minimumOrderQty) + (Number(get) ? Number(get) : ''))).toFixed(2) :0;

      let sameAndDiscount = (Number(sameAndD) - (Number(sameAndD)/100 * Number(percentOff))).toFixed(2);

      let diffrentAndDiscount = (Number(noDiscPrice) - ( Number(noDiscPrice)/100 * Number(percentOff) )).toFixed(2);

      let samePriceGst = Number(samePrice)/100 * Number(GSTValue);
      let discPriceGst = Number(discPrice)/100 * Number(GSTValue);
      let noDiscPriceGst = Number(noDiscPrice)/100 * Number(GSTValue?GSTValue:0);
      let sameAndDiscountGst = Number(sameAndDiscount)/100 * Number(GSTValue);
      let diffrentAndDiscountGst = Number(diffrentAndDiscount)/100 * Number(GSTValue);

      let totalSamePrice = Number(samePrice + samePriceGst);
      let totalDiscPrice = Number(discPrice + discPriceGst);
      let totalNoDiscPrice = Number(noDiscPrice + noDiscPriceGst) > 0 ? Number(noDiscPrice + noDiscPriceGst) :0;
      let totalSameAndDiscount = Number(Number(sameAndDiscount) + sameAndDiscountGst);
      let totalDiffrentAndDiscount = Number(Number(diffrentAndDiscount) + diffrentAndDiscountGst);
      console.log('dawiucmnaoiwsfv',bestOffer)
    return (
      <React.Fragment>
        {/* <Button className="jr-btn" onClick={() => this.setState({ open: true })}>Add Inventory + </Button> */}
        <Dialog open={this.props.buttonType === 'edit' ? this.props.edit : this.props.add} onClose={this.handleRequestClose}
          fullWidth={true}
          maxWidth={'md'}>
          <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off">
            <DialogTitle className='pb-0'>
              {this.props.title}
            </DialogTitle>
            <DialogContent>

              <Row>
                <Col xs={12} md={12} sm={12} xl={12} lg={12}>
                  <FormGroup>
              { bestOffer && bestOffer.map((data,index)=>{
               return <React.Fragment> {data ? <Label for="name">{data ? `${index+1}. ( Best Offered Price: Rs. ${(data.price).toFixed(2)}
              ${data.disc && data.disc.name == 'Same'?', Offer: Buy '+data.disc.discount_on_product.purchase+' and get '+data.disc.discount_on_product.bonus+' free': 
              data.disc && data.disc.name == 'Discount'?
              ', Offer: '+data.disc.discount_per+'% discount':
              data.disc && data.disc.name == 'Different'?
              ', Offer: '+'Buy '+data.disc.discount_on_product.purchase+' and get '+data.disc.discount_on_product.bonus+' '+data.disc.discount_on_product.inventory_id.product_id.name+' free' : data.disc && data.disc.type == ''? 
              '':''
              }, Min Quantity: ${data.minQ} )`:''}</Label>:''}<br/></React.Fragment>
              })}
              <div className='mt-2'></div>
                    <AutoCompleteSearch value={this.state.name} options={options} handleDataChange={this.selectData} handleChange={this.handleSearchChange} key='name' label={'Name'} />
                    {
                      !name &&
                      <Field id="name" name="name" type="text"
                        component={ReactStrapTextField} label={""}
                        validate={[required]}
                        value={name}
                        hidden={true}
                        disabled={true}
                      />}
                  </FormGroup>
                </Col>

                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                  {
                    
                      <FormGroup>
                        <Label for="chemicalCombination">Chemical Combination</Label>
                        <Input disabled type="text" name="chemicalCombination" value={chemicalCombination} id="chemicalCombination" />
                      </FormGroup>
                  }


                </Col>


                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                  {
                    
                      <FormGroup>
                        <Label for="companyName">Company Name</Label>
                        <Input disabled type="text" name="companyName" id="companyName" value={companyName} />
                      </FormGroup>
                  }


                </Col>
                <Col xs={12} md={12} sm={12} xl={4} lg={4}>

                  <Field id="totalQuantity" name="totalQuantity" type="text"
                    component={ReactStrapTextField} label={"Total Available Quantity"}
                    validate={[required, number,number0]}
                    // value={totalQuantity}
                    // onChange={(event) => this.handleChange(event, 'totalQuantity')}
                  />

                </Col>

                <Col xs={12} md={6} sm={12} xl={4} lg={4}>
                  <FormGroup>
                    <Label for="expiryDate">Expiry Date</Label>
                    <DatePicker
                      selected={expiryDate}
                      onChange={date => this.handleDateChange(date)}
                      dateFormat="MM/yyyy"
                      showMonthYearPicker
                      className="form-control"
                      value={expiryDate}
                      minDate={moment(expiryDate).toDate()}
                    />
                  </FormGroup>
                </Col>
                <Col xs={12} md={12} sm={12} xl={4} lg={4}>
                <FormGroup>
                        <Label for="countryOfO">Country Of Origin</Label>
                        <Input disabled type="text" name="countryOfO" value={countryOfO} id="countryOfO" />
                      </FormGroup>

                </Col>
                <Col xs={12} md={4} sm={12} xl={4} lg={4}>
                  {
                    
                      <FormGroup>
                        <Label for="GST">GST</Label>
                        <Input disabled type="text" name="GST" id="GST" value={GST}
                        />
                          
                      </FormGroup>
                  }


                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  {
                    <FormGroup>
                      <Label for="type">Type</Label>
                      <Input disabled type="text" name="type" id="text" value={type}
                      />

                    </FormGroup>
                  }


                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  {/* { valueData.medicine_type_id && valueData.medicine_type_id.name ? */}
                    <FormGroup>
                      <Label for="type">Medicine type</Label>
                      <Input disabled type="text" name="Medicine type" id="text" value={this.state.medicineType}
                      />

                    </FormGroup> 


                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  <Field id="MRP" name="MRP" type="text"
                    component={ReactStrapTextField} label={"MRP"}
                    validate={[required, number, twoDecimal]}
                    // value={MRP}
                    // onChange={(event) => this.handleChange(event, 'MRP')}
                  />

                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  <Field id="PTR" name="PTR" type="number"
                    component={ReactStrapTextField} label={"PTR"}
                    validate={[required, number, twoDecimal]}
                    // value={PTR}
                    // onChange={(event) => this.handleChange(event, 'PTR')}
                  />

                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                    <FormGroup>
                      <Label for="type">Prepaid Inventory</Label>
                      <Input disabled type="text" name="type" id="text" value={'FALSE'}
                      />
                    </FormGroup>
                </Col>
               

                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                  <Field id="minimumOrderQty" name="minimumOrderQty" type="text"
                    component={ReactStrapTextField} label={"Minimum order quantity"}
                    validate={[required, number,number0, minLessAvail, noDecimal]}
                    // value={minimumOrderQty}
                    // onChange={(event) => this.handleChange(event, 'minimumOrderQty')}
                  />

                </Col>
                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                  <Field id="maximumOrderQty" name="maximumOrderQty" type="text"
                    component={ReactStrapTextField} label={"Maximum order quantity"}
                    validate={[required, number,number0, maxLength4, maxPurchase, maxLessAvail, noDecimal, maxGreaterMin,maxLessMin]}
                    // value={maximumOrderQty}
                    // onChange={(event) => this.handleChange(event, 'maximumOrderQty')}
                  />

                </Col>
                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                    <FormGroup>
                     <Label for="Surcharge">Product Surcharge</Label>
                     <Input disabled type="text" name="Surcharge" value={surCharge && surCharge ? `${surCharge} %` : '0 %'} id="Surcharge" />
                    </FormGroup>
                  </Col>

                
                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                    <FormGroup>
                      <Label for="isPrepaid">Prepaid Product</Label>
                      <Input disabled type="text" name="isPrepaid" id="isPrepaid" value={this.state.isPrepaid === true ? 'TRUE' : this.state.isPrepaid === false ? 'FALSE' : ''}
                      />
                    </FormGroup>
                </Col>
                
                <Col sm={12} md={6} xs={12} lg={6} xl={6}>
                  {
                      <FormGroup>
                        <Label for="productCategory">Product Category</Label>
                        <Input disabled type="text" name="productCategory" id="productCategory" value={productCategory}
                         />
                      </FormGroup>

                  }


                </Col>


                <Col sm={12} md={6} xs={12} lg={6} xl={6}>
                  <FormGroup>
                    <Label>Discount</Label>
                    <Input type = 'select' value={discount}  onChange={(event) => this.handleChange(event, 'discount')}>
                      <option selected={discount === 'nodiscount'} value="nodiscount">Select discount</option>
                      <option selected={discount === 'Discount'} value='Discount'>Discount on PTR</option>
                      <option selected={discount === 'Same'} value='Same'>Same product bonus</option>
                      <option selected={discount === 'Different'} value='Different'>Different product bonus</option>
                      <option selected={discount === 'SameAndDiscount'} value='SameAndDiscount'>Same product bonus and Discount</option>
                      <option selected={discount === 'DifferentAndDiscount'} value='DifferentAndDiscount'>Different product bonus and Discount</option>
                    </Input>

                  </FormGroup>
                </Col>
              </Row>


              {
                discount === 'Discount' ?
                  <React.Fragment>
                    <Row>
                      <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                        <FormGroup>
                          <span className='row'>
                            <span className='ml-3 mr-3 mt-3'>Get flat  </span>
                            <Field id="percentOff" name="percentOff" type="number"
                              component={TextBox} label={""}
                              validate={[required, number, number0, percentageValidation, discountValidation]}
                              value={percentOff}
                              fullWidth={false}
                              width={(parseInt(percentOff) < 0 || parseInt(percentOff) > 100) ? 100 : this.state.percentOffMx}
                              setDiv={this.setDiv}
                              margin='dense'
                              onChange={(event) => this.handleChange2(event, 'percentOff')}
                            />
                            <span className='ml-3 mr-3 mt-3'>% Off </span>
                          </span>
                        </FormGroup>
                      </Col>
                    </Row>
                  </React.Fragment>
                  : discount === 'Different' ? <React.Fragment>
                    <Col sm={12} md={12} xs={12} lg={12} xl={12} className='row ml-1'>

                      <FormGroup className='sm-7 md-7 lg-7 xl-7'>
                        <div className='row  pr-4'>
                          <p className='mt-3 mr-2'>Buy </p>
                          <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>

                          <p className='mt-3 ml-2 mr-2'> {`${name} and get`}</p>
                          <Field id="get" name="get" type="number"
                            component={TextBox} label={''}
                            validate={[required, number, number0, noDecimal]}
                            value={get}
                            width={(buy < 0) ? 100 : this.state.getMx}
                            margin='dense'
                            min={1}
                            onChange={(event) => this.handleChange3(event, 'get')}
                          />


                        </div>
                      </FormGroup>



                      <FormGroup className=' mt-2 sm-4 md-4 lg-4 xl-4' width={100} >
                        <AutoCompleteSearch standard={true} value={otherName} placeholder={'Select product'} options={options2} handleDataChange={this.selectAnother} handleChange={this.hamdleSerchAnotherProduct} key='getProduct' />

                        {
                          !otherName &&
                          <Field id="name" name="name" type="text"
                            component={ReactStrapTextField} label={""}
                            validate={[required]}
                            value={otherName}
                            hidden={true}
                            disabled={true}
                          />}
                      </FormGroup>
                      <FormGroup className=' ml-2 sm-1 md-1 lg-1 xl-1 pt-2' >
                        <p>Free</p>
                      </FormGroup>

                    </Col>
                  </React.Fragment> : discount === 'Same' ? <React.Fragment>
                    <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                      <FormGroup>
                        <div className='row  pr-4'>
                          <p className='mt-3 mr-2'>Buy </p>
                          <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
                          <p className='mt-3 ml-2 mr-2'> {`${name} and get`}</p>
                          <Field id="get" name="get" type="number"
                            component={TextBox} label={''}
                            validate={[required, number, number0, noDecimal]}
                            value={get}
                            width={(buy < 0) ? 100 : this.state.getMx}
                            margin='dense'
                            onChange={(event) => this.handleChange3(event, 'get')}
                          />
                          <p className='mt-3 ml-2'> {`free`}</p>
                        </div>

                      </FormGroup>
                    </Col>
                  </React.Fragment> : 
                  discount === 'SameAndDiscount' ? <React.Fragment>
                  <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                    <FormGroup>
                      <div className='row'>
                        <p className='mt-3 mr-2'>Buy </p>
                        <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
                        <p className='mt-3 ml-2 mr-2'> {`${name} and get`}</p>
                        <Field id="get" name="get" type="number"
                          component={TextBox} label={''}
                          validate={[required, number, number0, noDecimal]}
                          value={get}
                          width={(buy < 0) ? 100 : this.state.getMx}
                          margin='dense'
                          onChange={(event) => this.handleChange3(event, 'get')}
                        />
                        <p className='mt-3 ml-2'> {`free`}</p>
                        <span className='ml-1 mr-1 mt-3'>and add-on </span>
                            <Field id="percentOff" name="percentOff" type="number"
                              component={TextBox} label={""}
                              validate={[required, number, number0, percentageValidation, discountValidation]}
                              value={percentOff}
                              fullWidth={false}
                              width={(parseInt(percentOff) < 0 || parseInt(percentOff) > 100) ? 100 : this.state.percentOffMx}
                              setDiv={this.setDiv}
                              margin='dense'
                              onChange={(event) => this.handleChange2(event, 'percentOff')}
                            />
                            <span className='ml-1 mr-1 mt-3'>% Off </span>
                      </div>

                    </FormGroup>
                  </Col>
                </React.Fragment> :
                discount === 'DifferentAndDiscount' ? <React.Fragment>
                  <Row>
                <Col sm={12} md={12} xs={12} lg={12} xl={12} className='row ml-1'>

                  <FormGroup className='sm-7 md-7 lg-7 xl-7'>
                    <div className='row  pr-4'>
                      <p className='mt-3 mr-2'>Buy </p>
                      <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
                      <p className='mt-3 ml-2 mr-2'> {`${name} and get`}</p>
                      <Field id="get" name="get" type="number"
                        component={TextBox} label={''}
                        validate={[required, number, number0, noDecimal]}
                        value={get}
                        width={(buy < 0) ? 100 : this.state.getMx}
                        margin='dense'
                        min={1}
                        onChange={(event) => this.handleChange3(event, 'get')}
                      />


                    </div>
                  </FormGroup>
                          </Col>
                          </Row>
                  <Row>
                  <Col sm={4} md={4} lg={4} xl={4} className='pr-0'>

                  <FormGroup className=' mt-2 ' width={100} >
                    <AutoCompleteSearch standard={true} value={otherName} placeholder={'Select product'} options={options2} handleDataChange={this.selectAnother} handleChange={this.hamdleSerchAnotherProduct} key='getProduct' />
                    
                    {
                      !otherName &&
                      <Field id="name" name="name" type="text"
                        component={ReactStrapTextField} label={""}
                        validate={[required]}
                        value={otherName}
                        hidden={true}
                        disabled={true}
                      />}
                      
                  </FormGroup>
                  </Col>
                  <Col sm={3} md={3} lg={3} xl={3} className='pl-0'>
                  <FormGroup  >
                  <span className='row'>
                            <span className='ml-3 mr-3 mt-2'>Free and add-on </span>
                            <Field id="percentOff" name="percentOff" type="number"
                              component={TextBox} label={""}
                              validate={[required, number, number0, percentageValidation, discountValidation]}
                              value={percentOff}
                              fullWidth={false}
                              width={(parseInt(percentOff) < 0 || parseInt(percentOff) > 100) ? 100 : this.state.percentOffMx}
                              setDiv={this.setDiv}
                              margin='dense'
                              onChange={(event) => this.handleChange2(event, 'percentOff')}
                            />
                            <span className='ml-3 mr-3 mt-2'>% Off </span>
                          </span>
                  </FormGroup>

                </Col>
                </Row>
              </React.Fragment>: ''
              }

              <div>
              <span className="text-success float-left pt-2" style={{ fontSize: 15 }}>Est. retailer margin:&nbsp;
              { <span>
                 { discount == 'Same' && PTR !== "" && MRP !== ""?
                 Number(MRP) > Number(totalSamePrice) ?
                  (((Number(MRP) - Number(totalSamePrice)) / Number(MRP)) * 100).toFixed(2) :
                  (((Number(totalSamePrice) - Number(MRP)) / Number(MRP)) * 100).toFixed(2) :

                  (discount == 'SameAndDiscount' && PTR !== "" && MRP !== "" && percentOff !="")?
                  Number(MRP) > Number(totalSameAndDiscount) ?
                  (((Number(MRP) - Number(totalSameAndDiscount)) / Number(MRP)) * 100).toFixed(2) :
                  (((Number(totalSameAndDiscount) - Number(MRP)) / Number(MRP)) * 100).toFixed(2) :

                  (discount == 'DifferentAndDiscount' && PTR !== "" && MRP !== "" && percentOff !="")?
                  Number(MRP) > Number(totalDiffrentAndDiscount) ?
                  (((Number(MRP) - Number(totalDiffrentAndDiscount)) / Number(MRP)) * 100).toFixed(2) :
                  (((Number(totalDiffrentAndDiscount) - Number(MRP)) / Number(MRP)) * 100).toFixed(2) :

                  (discount == 'Discount' && PTR !== "" && MRP !== "") ?
                  Number(MRP) > Number(totalDiscPrice) ?
                  (((Number(MRP) - Number(totalDiscPrice)) / Number(MRP)) * 100).toFixed(2):
                  (((Number(totalDiscPrice) - Number(MRP)) / Number(MRP)) * 100).toFixed(2):
                  
                  ((discount == 'nodiscount' || discount == 'Different')&&PTR !== "" && MRP !== "") ?
                  Number(MRP) > Number(totalNoDiscPrice) ?
                  (((Number(MRP) - Number(totalNoDiscPrice)) / Number(MRP)) * 100).toFixed(2):
                  (((Number(totalNoDiscPrice) - Number(MRP?MRP:0)) / Number(MRP?MRP:0)) * 100).toFixed(2):
                  PTR?Number(PTR):0
                 }
                {/* Est. retailer margin : ((MRP- (offered price+gst))/MRP)*100 */}
                %</span>
              }
                  
                 </span>
                <span className="text-success float-right pr-4 pt-2" style={{ fontSize: 15 }}>Effective PTR price is {<span>&#8377;{
                 (discount === 'SameAndDiscount' && PTR !== "") ? sameAndDiscount : 
                 (discount === 'DifferentAndDiscount' && PTR !=='') ? diffrentAndDiscount :
                 (discount === 'Same' && PTR !== "") ? ((Number(PTR) * Number(minimumOrderQty))/(Number(minimumOrderQty) + (Number(get) ? Number(get) : ''))).toFixed(2) :
                 (discount === 'Discount' && PTR !== "") ? percentOff !== '' ? ( Number(PTR) - (((percentOff !== '' ? Number(percentOff) : 100) / 100) * Number(PTR))).toFixed(2) < 0 ? 0 : (Number(PTR) - (((percentOff !== '' ? Number(percentOff) : 100) / 100) * Number(PTR))).toFixed(2) : (((percentOff !== '' ? Number(percentOff) : 100) / 100) * Number(PTR)).toFixed(2) : PTR !== "" ? Number(PTR).toFixed(2) : PTR?Number(PTR):0}</span>}</span>
              </div>
            </DialogContent>

            <DialogActions className="pr-4">

              <Button onClick={this.handleRequestClose} color='secondary' >
                Cancel
            </Button>
              {
                buttonType === 'edit' ? <Button type='submit' color='primary'>
                  Submit
             </Button> : <Button type='submit' color='primary'>
                    Add
            </Button>
              }

            </DialogActions>
          </form>
        </Dialog>

        {/* --------------------------=-=Add same prod again-=-=-------------------- */}
        <Dialog fullWidth={true} maxWidth={'sm'} open={this.state.openDailog} onClose={this.closeDialog}>
        {/* <CircularDeterminate closeDialog={this.closeDialog}/> */}
          <DialogTitle>
            {'Recreate another scheme ?'}
          </DialogTitle>
          <DialogActions className="pr-4">

            <Button onClick={this.closeDialog} color='secondary' >
              No
            </Button>
             <Button onClick={this.addAnotherProduct} color='primary'>
                Yes
            </Button>

          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}
const mapStateProps = ({seller,form})=>{
  // const {}
  return{seller,form}
}
AddInventory = connect(
  mapStateProps,
  {
    getInventoryList
  }           // bind account loading action creator
)(AddInventory)

export default AddInventory = reduxForm({
  form: 'AddInventory',// a unique identifier for this form
  // onSubmitSuccess: (result, dispatch) => {
  //   const newInitialValues = getFormInitialValues('AddInventory')();
  // }
})(AddInventory)

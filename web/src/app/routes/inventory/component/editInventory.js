import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Col, Row } from 'reactstrap';
import { FormGroup, Label, Input } from 'reactstrap';
import moment from 'moment';
import DatePicker from "react-datepicker";
import AxiosRequest from 'sagas/axiosRequest'
import ReactStrapTextField from 'components/ReactStrapTextField';
import ReactStrapSelectField from 'components/reactstrapSelectField';
import { NotificationManager } from 'react-notifications';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { getInventoryList } from 'actions'
import { connect } from 'react-redux'
import { required, number, number0, maxPurchase, percentageValidation, discountValidation, maxLength4, noDecimal, minLessAvail, maxLessAvail, maxGreaterMin, twoDecimal, lessThenMrp,maxLessMin} from 'constants/validations';
import AutoCompleteSearch from 'components/asyncAutocmplete'
import TextBox from 'components/textBox';
class EditInventory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      name: '',
      chemicalCombination: '',
      expiryDate:  new Date(moment().add(6, 'months')),
      MRP: '',
      PTR: '',
      minimumOrderQty: '',
      maximumOrderQty: '',
      GST: '',
      type: '',
      productCategory: '',
      discount: 'nodiscount',
      percentOff: 1,
      buy: 1,
      get: 1,
      getProduct: '',
      companyName: '',
      totalQuantity: '',
      searchItem: '',
      options: [],
      selectedData: '',
      options2: [],
      otherSelectedProduct: '',
      otherName: '',
      searchOtherProdcut: '',
      buyMx: 35,
      getMx: 35,
      percentOffMx: 35,
      ePtr:'',
      countryOfO:''
    };
  }

  componentDidMount() {
    this.props.initialize({
      name: this.props.editData ? this.props.editData.Products.name : this.state.name,
      countryOfO: this.props.editData ? this.props.editData.Products.country_of_origin : this.state.country_of_origin,
      chemicalCombination: this.props.editData ? this.props.editData.Products.chem_combination : this.state.chemicalCombination,
      MRP: this.props.editData ? this.props.editData.MRP : this.state.MRP,
      PTR: this.props.editData ? this.props.editData.PTR : this.state.PTR,
      GST: this.props.editData ? this.props.editData.GST.name : this.state.GST,
      type: this.props.editData ? this.props.editData.ProductTypes.name : this.state.type,
      productCategory: this.props.editData ? this.props.editData.ProductCategory ? this.props.editData.ProductCategory.name : this.state.productCategory : this.state.productCategory,
      discount: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts.name : this.state.discount : this.state.discount,
      percentOff: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts.discount_per : this.state.percentOff : this.state.percentOff,
      buy: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.purchase : this.state.buy : this.state.buy : this.state.buy,
      get: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.bonus : this.state.get : this.state.get : this.state.get,
      totalQuantity: this.props.editData ? Number(this.props.editData.quantity) : Number(this.props.editData.quantity),
      companyName: this.props.editData ? this.props.editData.Company.name : this.state.companyName,
      minimumOrderQty: this.props.editData ? this.props.editData.min_order_quantity : this.state.minimumOrderQty,
      maximumOrderQty: this.props.editData ? this.props.editData.max_order_quantity : this.state.maximumOrderQty,
      otherName: this.props.editData ? this.props.editData.OtherProducts ? this.props.editData.OtherProducts.name : this.state.otherName : this.state.otherName,
      otherSelectedProduct: this.props.editData ? this.props.editData.OtherProducts : '',
      expiryDate: this.props.editData ? new Date(this.props.editData.expiry_date) : ''
    });
    this.setState({

      selectData: this.props.editData ? this.props.editData.Products : '',
      name: this.props.editData ? this.props.editData.Products.name : this.state.name,
      countryOfO: this.props.editData ? this.props.editData.Products.country_of_origin : this.state.country_of_origin,
      chemicalCombination: this.props.editData ? this.props.editData.Products.chem_combination : this.state.chemicalCombination,
      MRP: this.props.editData ? this.props.editData.MRP : this.state.MRP,
      PTR: this.props.editData ? this.props.editData.PTR : this.state.PTR,
      GST: this.props.editData ? this.props.editData.GST.name : this.state.GST,
      type: this.props.editData ? this.props.editData.ProductTypes.name : this.state.type,
      productCategory: this.props.editData ? this.props.editData.ProductCategory.name : this.state.productCategory,
      discount: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts.name : this.state.discount : this.state.discount,
      percentOff: this.props.editData ?this.props.editData.Discounts? this.props.editData.Discounts.discount_per : this.state.percentOff:this.state.percentOff,
      buy: this.props.editData ?this.props.editData.Discounts? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.purchase : this.state.buy : this.state.buy:this.state.buy,
      get: this.props.editData ?this.props.editData.Discounts? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.bonus : this.state.get : this.state.get:this.state.buy,
      totalQuantity: this.props.editData ? this.props.editData.quantity : this.props.editData.quantity,
      companyName: this.props.editData ? this.props.editData.Company.name : this.state.companyName,
      minimumOrderQty: this.props.editData ? this.props.editData.min_order_quantity : this.state.minimumOrderQty,
      maximumOrderQty: this.props.editData ? this.props.editData.max_order_quantity : this.state.maximumOrderQty,
      otherName: this.props.editData ? this.props.editData.OtherProducts ? this.props.editData.OtherProducts.name : this.state.otherName : this.state.otherName,
      otherSelectedProduct: this.props.editData ? this.props.editData.Inventories : '',
      expiryDate: this.props.editData ? new Date(this.props.editData.expiry_date) : ''
    });
  }
  handleRequestClose = () => {
    this.setState({ open: false });

    this.handleClose(this.props.editData ? 'edit' : 'add');
  };
  onSubmit = async (e) => {

    let data = {
      Id: this.props.editData ? this.props.editData._id : '',
      product_id: this.state.selectData._id,
      company_id: this.state.selectData.company_id._id,
      chemicalCombination: this.state.chemicalCombination,
      MRP: this.state.MRP,
      PTR: this.state.PTR,
      quantity: this.state.totalQuantity,
      min_order_quantity: this.state.minimumOrderQty,
      max_order_quantity: this.state.maximumOrderQty,
      expiry_date: moment(this.state.expiryDate).endOf('month').format(),
      discount_name: this.state.discount ? this.state.discount !== 'nodiscount' ? this.state.discount : "" : "",
      discount_per: this.state.percentOff,
      purchase: this.state.minimumOrderQty,
      bonus: this.state.get,
      otherInventoryId: this.state.otherSelectedProduct ? this.state.otherSelectedProduct._id : '',
      discountId: this.props.editData ? this.props.editData.Discounts ? this.props.editData.Discounts._id : '' : ''

    }
    let url = this.prop
    let response = await AxiosRequest.axiosHelperFunc('post', 'inventory/editInventoryItem', '', data)
    this.handleClose()
    if (response.data.error) {
      NotificationManager.error(response.data.title)
    } else {
      NotificationManager.success(response.data.title)
      let data2 = {
        page: this.props.page === 0 ? 1 : this.props.page,
        perPage: this.props.perPage
      }
      this.props.callMount();
      // this.props.getInventoryList({ data: data2 })
    }
  }
  handleChange = async (e, key) => {
    let data = {}
    this.props.dispatch(change('EditInventory', key, e.target.value));
    this.setState({ [key]: e.target.value });

  }
  handleChange2 = (event, key) => {
    this.setState({ [key]: event.target.value });
  }
  handleChange3 = (event, key) => {
    this.setState({ [key]: event.target.value });
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.editData && this.props.editData !== prevProps.editData) {
      this.props.initialize({
        name: this.props.editData ? this.props.editData.Products.name : this.state.name,
        countryOfO: this.props.editData ? this.props.editData.Products.country_of_origin : this.state.country_of_origin,
        chemicalCombination: this.props.editData ? this.props.editData.Products.chem_combination : this.state.chemicalCombination,
        MRP: this.props.editData ? this.props.editData.MRP : this.state.MRP,
        PTR: this.props.editData ? this.props.editData.PTR : this.state.PTR,
        GST: this.props.editData ? this.props.editData.GST.name : this.state.GST,
        type: this.props.editData ? this.props.editData.ProductTypes.name : this.state.type,
        productCategory: this.props.editData ? this.props.editData.ProductCategory.name : this.state.productCategory,
        discount: this.props.editData ? this.props.editData.Discounts.name : this.state.discount,
        percentOff: this.props.editData ? this.props.editData.Discounts.discount_per : this.state.percentOff,
        buy: this.props.editData ? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.purchase : this.state.buy : this.state.buy,
        get: this.props.editData ? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.bonus : this.state.get : this.state.get,
        totalQuantity: this.props.editData ? this.props.editData.quantity : this.props.editData.quantity,
        companyName: this.props.editData ? this.props.editData.Company.name : this.state.companyName,
        minimumOrderQty: this.props.editData ? this.props.editData.min_order_quantity : this.state.minimumOrderQty,
        maximumOrderQty: this.props.editData ? this.props.editData.max_order_quantity : this.state.maximumOrderQty,
        otherName: this.props.editData ? this.props.editData.OtherProducts ? this.props.editData.OtherProducts.name : this.state.otherName : this.state.otherName,
        otherSelectedProduct: this.props.editData ? this.props.editData.OtherProducts : '',
        expiryDate: this.props.editData ? new Date(this.props.editData.expiry_date) : ''
      });
      this.setState({

        selectData: this.props.editData ? this.props.editData.Products : '',
        countryOfO: this.props.editData ? this.props.editData.Products.country_of_origin : this.state.country_of_origin,
        name: this.props.editData ? this.props.editData.Products.name : this.state.name,
        chemicalCombination: this.props.editData ? this.props.editData.Products.chem_combination : this.state.chemicalCombination,
        MRP: this.props.editData ? this.props.editData.MRP : this.state.MRP,
        PTR: this.props.editData ? this.props.editData.PTR : this.state.PTR,
        GST: this.props.editData ? this.props.editData.GST.name : this.state.GST,
        type: this.props.editData ? this.props.editData.ProductTypes.name : this.state.type,
        productCategory: this.props.editData ? this.props.editData.ProductCategory.name : this.state.productCategory,
        discount: this.props.editData ? this.props.editData.Discounts.name : this.state.discount,
        percentOff: this.props.editData ? this.props.editData.Discounts.discount_per : this.state.percentOff,
        buy: this.props.editData ? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.purchase : this.state.buy : this.state.buy,
        get: this.props.editData ? this.props.editData.Discounts.discount_on_product ? this.props.editData.Discounts.discount_on_product.bonus : this.state.get : this.state.get,
        totalQuantity: this.props.editData ? Number(this.props.editData.quantity) : Number(this.props.editData.quantity),
        companyName: this.props.editData ? this.props.editData.Company.name : this.state.companyName,
        minimumOrderQty: this.props.editData ? this.props.editData.min_order_quantity : this.state.minimumOrderQty,
        maximumOrderQty: this.props.editData ? this.props.editData.max_order_quantity : this.state.maximumOrderQty,
        otherName: this.props.editData ? this.props.editData.OtherProducts ? this.props.editData.OtherProducts.name : this.state.otherName : this.state.otherName,
        otherSelectedProduct: this.props.editData ? this.props.editData.Inventories : '',
        expiryDate: this.props.editData ? new Date(this.props.editData.expiry_date) : ''
      });
    }
  }

  hamdleSerchAnotherProduct = async (e, val) => {
    if(this.state.name && this.state.otherName !== '' && val !== '') {
      this.props.dispatch(change("EditInventory","otherName",''))
      this.props.dispatch(change("EditInventory","name",''))
    } else if(this.state.name && this.state.otherName === this.state.name && val === '') {
      this.props.dispatch(change("EditInventory","otherName",''))
      this.props.dispatch(change("EditInventory","name",''))
    } else if(this.state.name && this.state.otherName !== this.state.name && val === '') {
      this.props.dispatch(change("EditInventory","otherName",''))
      this.props.dispatch(change("EditInventory","name",''))
    }
    if (val) {
      let body = { name: val }
      let data = await AxiosRequest.axiosHelperFunc('post', 'inventory/searchInventory', '', body)
      let temp = []
      temp = data.data.detail
      this.setState({ options2: temp, otherName: val })
    } else {
      this.setState({ options2: [], otherName: val })
    }
  }
  selectAnother = (data) => {

    this.setState({ otherSelectedProduct: data, otherName: data ? data.name : '' })
  }
  handleSearchChange = async (e, val) => {

    if (val) {
      let body = { name: val }
      let data = await AxiosRequest.axiosHelperFunc('post', 'product/searchProduct', '', body)
      let temp = []
      temp = data.data.detail
      this.setState({ options: temp, selectedData: '' })
    } else {
      this.setState({ options: [], selectedData: '' })
    }
  }
  selectData = (data) => {
    if (data) {
      this.setState({
        selectData: data, chemicalCombination: data.chem_combination, companyName: data.company_id.name,
        GST: data.GST.name, productCategory: data.product_cat_id.name, type: data.Type.name, name: data.name
      })
    } else {
      this.setState({
        selectData: data, chemicalCombination: '', companyName: '',
        GST: '', productCategory: '', type: '', name: ''
      })
    }

  }
  handleDateChange = (data) => {
    this.setState({ expiryDate: data })
  }
  handleClose = () => {
    this.setState({
      open: false,
      name: '',
      chemicalCombination: '',
      expiryDate: new Date(),
      MRP: '',
      PTR: '',
      minimumOrderQty: '',
      maximumOrderQty: '',
      GST: '',
      type: '',
      productCategory: '',
      discount: '',
      percentOff: '',
      buy: 1,
      get: 1,
      getProduct: '',
      companyName: '',
      totalQuantity: '',
      searchItem: '',
      options: [],
      selectedData: '',
      selectData: '',
      buyMx: 10,
      getMx: 10
    })
    this.props.handleClick(this.props.editData ? 'edit' : 'add')
    this.props.reset()
  }
  render() {
    let { title, buttonType, pristine, invalid, handleSubmit } = this.props;
    let { name, open, options, options2, otherSelectedProduct, otherName,
      chemicalCombination, expiryDate, MRP, PTR, minimumOrderQty, maximumOrderQty, GST, type, productCategory, discount, percentOff, buy, get, getProduct, companyName, totalQuantity,countryOfO } = this.state;

    let noDiscPrice = ((discount === 'nodiscount'|| discount ==='Different') && PTR !== "") ? Number(PTR) :Number(PTR) ;
    let sameAndD = (discount === 'SameAndDiscount' && PTR !== "") ? Number((Number(PTR) * Number(minimumOrderQty))/(Number(minimumOrderQty) + (Number(get) ? Number(get) : ''))).toFixed(2) :0;

    let sameAndDiscount = (Number(sameAndD) - (Number(sameAndD)/100 * Number(percentOff))).toFixed(2);

    let diffrentAndDiscount = (Number(noDiscPrice) - ( Number(noDiscPrice)/100 * Number(percentOff) )).toFixed(2);

    return (
      <React.Fragment>
        {/* <Button className="jr-btn" onClick={() => this.setState({ open: true })}>Add Inventory + </Button> */}
        <Dialog open={this.props.buttonType === 'edit' ? this.props.edit : this.props.add} onClose={this.handleRequestClose}
          fullWidth={true}
          maxWidth={'md'}>
          <form noValidate onSubmit={handleSubmit(this.onSubmit)} autoComplete="off">
            <DialogTitle className='pb-0'>
              {this.props.title}
            </DialogTitle>
            <DialogContent>

              <Row>
                <Col xs={12} md={12} sm={12} xl={12} lg={12}>
                  <FormGroup>
                    <Label for="name">Name</Label>
                    <AutoCompleteSearch disabled={true} value={this.state.name} options={options} handleDataChange={this.selectData} handleChange={this.handleSearchChange} key='name' />
                    {
                      !name &&
                      <Field id="name" name="name" type="text"
                        component={ReactStrapTextField} label={""}
                        validate={[required]}
                        value={name}
                        hidden={true}
                        disabled={true}
                      />}
                  </FormGroup>
                </Col>

                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                  {
                    !chemicalCombination ?
                      <Field id="chemicalCombination" name="chemicalCombination" type="text"
                        component={ReactStrapTextField} label={"Chemical Combination"}
                        validate={[required]}
                        value={chemicalCombination}
                        disabled={true}
                      />
                      :
                      <FormGroup>
                        <Label for="chemicalCombination">Chemical Combination</Label>
                        <Input disabled type="text" name="chemicalCombination" value={chemicalCombination} id="chemicalCombination" />
                      </FormGroup>
                  }


                </Col>


                <Col xs={12} md={12} sm={12} xl={6} lg={6}>
                  {
                    !companyName ?
                      <Field id="companyName" name="companyName" type="text"
                        component={ReactStrapTextField} label={"Company Name"}
                        validate={[required]}
                        value={companyName}
                        disabled={true}
                      /> :
                      <FormGroup>
                        <Label for="companyName">Company Name</Label>
                        <Input disabled type="text" name="companyName" id="companyName" value={companyName} />
                      </FormGroup>
                  }


                </Col>
                <Col xs={12} md={12} sm={12} xl={4} lg={4}>

                  <Field id="totalQuantity" name="totalQuantity" type="number"
                    component={ReactStrapTextField} label={"Total Available Quantity"}
                    validate={[required, number, noDecimal,number0]}
                    value={totalQuantity}
                    onChange={(event) => this.handleChange(event, 'totalQuantity')}
                  />

                </Col>

                <Col xs={12} md={6} sm={12} xl={4} lg={4}>
                  <FormGroup>
                    <Label for="expiryDate">Expiry Date</Label>
                    <DatePicker
                      selected={expiryDate}
                      onChange={date => this.handleDateChange(date)}
                      dateFormat="MM/yyyy"
                      showMonthYearPicker
                      className="form-control"
                      value={expiryDate}
                      minDate={moment(expiryDate).toDate()}
                    />
                  </FormGroup>
                </Col>
                <Col xs={12} md={6} sm={12} xl={4} lg={4}>
                <Field id="countryOfO" name="countryOfO" type="text"
                    component={ReactStrapTextField} label={"Country Of Origin"}
                    validate={[required]}
                    value={countryOfO} disabled={true}
                    // onChange={(event) => this.handleChange(event, 'countryOfO')}
                  />
                </Col>
                <Col xs={12} md={4} sm={12} xl={4} lg={4}>
                  {
                    !GST ?
                      <Field id="GST" name="GST" type="text"
                        component={ReactStrapTextField} label={"GST"}
                        validate={[required]}
                        value={GST}
                        disabled={true}
                      /> :
                      <FormGroup>
                        <Label for="GST">GST</Label>
                        <Input disabled type="text" name="GST" id="GST" value={GST}
                        >
                          <option value=" ">Select GST</option>
                          <option value='GST_0'>GST_0</option>
                          <option value='GST_5'>GST_5</option>
                          <option value='GST_12'>GST_12</option>
                          <option value='GST_18'>GST_18</option>
                          <option value='GST_28'>GST_28</option>
                        </Input>
                      </FormGroup>
                  }


                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  {
                    !type ?
                      <Field id="type" name="type" type="text"
                        component={ReactStrapTextField} label={"Type"}
                        validate={[required]}
                        value={type}
                        disabled={true}
                      />
                      :
                      <FormGroup>
                        <Label for="type">Type</Label>
                        <Input disabled type="text" name="type" id="text" value={type}
                        >
                          <option value="">Select type</option>
                          <option value='Bottle'>Bottle</option>
                          <option value='Tuber'>Tuber</option>
                          <option value='Box'>Box</option>
                        </Input>
                      </FormGroup>
                  }


                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  { this.props.editData.medicineTypeName &&
                    <FormGroup>
                      <Label for="type">Medicine type</Label>
                      <Input disabled type="text" name="type" id="text" value={this.props.editData.medicineTypeName}
                      />
                    </FormGroup>
                  }
                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  <Field id="MRP" name="MRP" type="text"
                    component={ReactStrapTextField} label={"MRP"}
                    validate={[required, number, twoDecimal]}
                    value={MRP}
                    onChange={(event) => this.handleChange(event, 'MRP')}
                  />

                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                  <Field id="PTR" name="PTR" type="text"
                    component={ReactStrapTextField} label={"PTR"}
                    validate={[required, number, twoDecimal]}
                    value={PTR}
                    onChange={(event) => this.handleChange(event, 'PTR')}
                  />

                </Col>
                <Col sm={12} md={4} xs={12} lg={4} xl={4}>
                    <FormGroup>
                      <Label for="type">Prepaid Inventory</Label>
                      <Input disabled type="text" name="type" id="text" value={this.props.editData.prepaidInven && this.props.editData.prepaidInven === true ? 'TRUE' : 'FALSE'}
                      />
                    </FormGroup>
                </Col>
                

                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                  <Field id="minimumOrderQty" name="minimumOrderQty" type="number"
                    component={ReactStrapTextField} label={"Minimum order quantity"}
                    validate={[required, number, noDecimal, minLessAvail,number0]}
                    value={minimumOrderQty}
                    onChange={(event) => this.handleChange(event, 'minimumOrderQty')}
                  />

                </Col>
                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                  <Field id="maximumOrderQty" name="maximumOrderQty" type="number"
                    component={ReactStrapTextField} label={"Maximum order quantity"}
                    validate={[required, number,number0, maxLength4, noDecimal, maxPurchase, maxLessAvail, maxGreaterMin,maxLessMin]}
                    value={maximumOrderQty}
                    pattern="[0-9]"
                    onChange={(event) => this.handleChange(event, 'maximumOrderQty')}
                  />

                </Col>
                <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                    <FormGroup>
                     <Label for="Surcharge">Product Surcharge</Label>
                     <Input disabled type="text" name="Surcharge" value={this.props.editData.Products.surcharge && this.props.editData.Products.surcharge ? `${this.props.editData.Products.surcharge} %` : '0 %'} id="Surcharge" />
                    </FormGroup>
                  </Col>
                  <Col sm={12} md={3} xs={12} lg={3} xl={3}>
                    <FormGroup>
                      <Label for="isPrepaid">Prepaid Product</Label>
                      <Input disabled type="text" name="isPrepaid" id="isPrepaid" value={this.props.editData.Products.isPrepaid && this.props.editData.Products.isPrepaid === true ? 'TRUE' : 'FALSE'}
                      />
                    </FormGroup>
                </Col>

               
                <Col sm={12} md={6} xs={12} lg={6} xl={6}>
                  {
                    !productCategory ?
                      <Field id="productCategory" name="productCategory" type="text"
                        component={ReactStrapTextField} label={"Product Category"}
                        validate={[required]}
                        value={productCategory}
                        disabled={true}
                      /> :
                      <FormGroup>
                        <Label for="productCategory">Product Category</Label>
                        <Input disabled type="text" name="productCategory" id="productCategory" value={productCategory}
                          onChange={(e) => this.handleChange(e, 'productCategory')}>
                          <option value="">Select category</option>
                          <option value='Medicine'>Medicine</option>
                          <option value='Cosmetic'>Cosmetic</option>
                        </Input>
                      </FormGroup>

                  }


                </Col>

                <Col sm={12} md={6} xs={12} lg={6} xl={6}>
                  <FormGroup>
                    <Field id="discount" name="discount" type="select"
                      component={ReactStrapSelectField} label={"Discount"}
                      validate={[required]}
                      value={discount}
                      onChange={(event) => this.handleChange(event, 'discount')}
                    >
                      <option selected={discount === 'nodiscount'} value="nodiscount">Select discount</option>
                      <option selected={discount === 'Discount'} value='Discount'>Discount on PTR</option>
                      <option selected={discount === 'Same'} value='Same'>Same product bonus</option>
                      <option selected={discount === 'Different'} value='Different'>Different product bonus</option>
                      <option selected={discount === 'SameAndDiscount'} value='SameAndDiscount'>Same product bonus and Discount</option>
                      <option selected={discount === 'DifferentAndDiscount'} value='DifferentAndDiscount'>Different product bonus and Discount</option>
                    </Field>

                  </FormGroup>
                </Col>
              </Row>


              {
                discount === 'Discount' ?
                  <React.Fragment>
                    <Row>
                      <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                        <FormGroup>
                          <span className='row'>
                            <span className='ml-3 mr-3 mt-3'>Get flat </span>
                            <Field id="percentOff" name="percentOff" type="number"
                              component={TextBox} label={""}
                              validate={[required, number, number0, percentageValidation, discountValidation]}
                              value={percentOff}
                              fullWidth={false}
                              width={(parseInt(percentOff) < 0 || parseInt(percentOff) > 100) ? 100 : this.state.percentOffMx}
                              setDiv={this.setDiv}
                              margin='dense'
                              onChange={(event) => this.handleChange2(event, 'percentOff')}
                            />
                            <span className='ml-3 mr-3 mt-3'>% Off </span>
                          </span>
                        </FormGroup>
                      </Col>
                    </Row>
                  </React.Fragment>
                  : discount === 'Different' ? <React.Fragment>
                    <Col sm={12} md={12} xs={12} lg={12} xl={12} className='row ml-1'>

                      <FormGroup className='sm-7 md-7 lg-7 xl-7'>
                        <div className='row  pr-4'>
                          <p style={{marginTop : 18}} className=' mr-2'>Buy </p>
                          <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
                          <p style={{marginTop : 18}} className='ml-2 mr-2'> {`${name} and get`}</p>
                          <Field id="get" name="get" type="number"
                            component={TextBox} label={''}
                            validate={[required, number, number0, noDecimal]}
                            value={get}
                            width={(buy < 0) ? 100 : this.state.getMx}
                            margin='dense'
                            min={1}
                            onChange={(event) => this.handleChange3(event, 'get')}
                          />


                        </div>
                      </FormGroup>



                      <FormGroup className=' mt-2 sm-4 md-4 lg-4 xl-4' width={100} >
                        <AutoCompleteSearch standard={true} value={otherName} placeholder={'Select product'} options={options2} handleDataChange={this.selectAnother} handleChange={this.hamdleSerchAnotherProduct} key='getProduct' />

                        {
                          !otherName &&
                          <Field id="name" name="name" type="text"
                            component={ReactStrapTextField} label={""}
                            validate={[required]}
                            value={otherName}
                            hidden={true}
                            disabled={true}
                          />}
                      </FormGroup>
                      <FormGroup className='ml-2 sm-1 md-1 lg-1 xl-1' style={{marginTop : 18}}>
                        <p>Free</p>
                      </FormGroup>

                    </Col>
                  </React.Fragment> : discount === 'Same' ? <React.Fragment>
                    <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                      <FormGroup>
                        <div className='row pr-4'>
                          <p className='mt-3 mr-2'>Buy </p>
                          <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
                          <p className='mt-3 ml-2 mr-2'> {`"${name}" and get`}</p>
                          <Field id="get" name="get" type="number"
                            component={TextBox} label={''}
                            validate={[required, number, number0, noDecimal]}
                            value={get}
                            width={(get < 0) ? 100 : this.state.getMx}
                            margin='dense'
                            normalize={0}
                            onChange={(event) => this.handleChange(event, 'get')}
                          />
                          <p className='mt-3 ml-2'> {`free`}</p>
                        </div>

                      </FormGroup>
                    </Col>

                  </React.Fragment> : 
                   discount === 'SameAndDiscount' ? <React.Fragment>
                   <Col sm={12} md={12} xs={12} lg={12} xl={12}>
                     <FormGroup>
                       <div className='row'>
                         <p className='mt-3 mr-2'>Buy </p>
                         <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
                         <p className='mt-3 ml-2 mr-2'> {`${name} and get`}</p>
                         <Field id="get" name="get" type="number"
                           component={TextBox} label={''}
                           validate={[required, number, number0, noDecimal]}
                           value={get}
                           width={(buy < 0) ? 100 : this.state.getMx}
                           margin='dense'
                           onChange={(event) => this.handleChange3(event, 'get')}
                         />
                         <p className='mt-3 ml-2'> {`free`}</p>
                         <span className='ml-1 mr-1 mt-3'>and add-on </span>
                             <Field id="percentOff" name="percentOff" type="number"
                               component={TextBox} label={""}
                               validate={[required, number, number0, percentageValidation, discountValidation]}
                               value={percentOff}
                               fullWidth={false}
                               width={(parseInt(percentOff) < 0 || parseInt(percentOff) > 100) ? 100 : this.state.percentOffMx}
                               setDiv={this.setDiv}
                               margin='dense'
                               onChange={(event) => this.handleChange2(event, 'percentOff')}
                             />
                             <span className='ml-1 mr-1 mt-3'>% Off </span>
                       </div>
 
                     </FormGroup>
                   </Col>
                 </React.Fragment> :
                 discount === 'DifferentAndDiscount' ? <React.Fragment>
                   <Row>
                 <Col sm={12} md={12} xs={12} lg={12} xl={12} className='row ml-1'>
 
                   <FormGroup className='sm-7 md-7 lg-7 xl-7'>
                     <div className='row  pr-4'>
                       <p className='mt-3 mr-2'>Buy </p>
                       <p className='mt-3 ml-2 mr-2'> {`${minimumOrderQty}`}</p>
 
                       <p className='mt-3 ml-2 mr-2'> {`${name} and get`}</p>
                       <Field id="get" name="get" type="number"
                         component={TextBox} label={''}
                         validate={[required, number, number0, noDecimal]}
                         value={get}
                         width={(buy < 0) ? 100 : this.state.getMx}
                         margin='dense'
                         min={1}
                         onChange={(event) => this.handleChange3(event, 'get')}
                       />
 
 
                     </div>
                   </FormGroup>
                           </Col>
                           </Row>
                   <Row>
                   <Col sm={4} md={4} lg={4} xl={4} className='pr-0'>
 
                   <FormGroup className=' mt-2 ' width={100} >
                     <AutoCompleteSearch standard={true} value={otherName} placeholder={'Select product'} options={options2} handleDataChange={this.selectAnother} handleChange={this.hamdleSerchAnotherProduct} key='getProduct' />
                     
                     {
                       !otherName &&
                       <Field id="name" name="name" type="text"
                         component={ReactStrapTextField} label={""}
                         validate={[required]}
                         value={otherName}
                         hidden={true}
                         disabled={true}
                       />}
                       
                   </FormGroup>
                   </Col>
                   <Col sm={3} md={3} lg={3} xl={3} className='pl-0'>
                   <FormGroup  >
                   <span className='row'>
                             <span className='ml-3 mr-3 mt-2'>Free and add-on </span>
                             <Field id="percentOff" name="percentOff" type="number"
                               component={TextBox} label={""}
                               validate={[required, number, number0, percentageValidation, discountValidation]}
                               value={percentOff}
                               fullWidth={false}
                               width={(parseInt(percentOff) < 0 || parseInt(percentOff) > 100) ? 100 : this.state.percentOffMx}
                               setDiv={this.setDiv}
                               margin='dense'
                               onChange={(event) => this.handleChange2(event, 'percentOff')}
                             />
                             <span className='ml-3 mr-3 mt-2'>% Off </span>
                           </span>
                   </FormGroup>
 
                 </Col>
                 </Row>
               </React.Fragment>:''
              }

              <div>
                <span className="text-success float-right pr-4 pt-2" style={{ fontSize: 15 }}>Effective PTR price is {<span>&#8377;{
                (discount === 'SameAndDiscount' && PTR !== "") ? sameAndDiscount : 
                (discount === 'DifferentAndDiscount' && PTR !=='') ? diffrentAndDiscount :
                (discount === 'Same' && PTR !== "") ? ((Number(PTR) * Number(minimumOrderQty))/(Number(minimumOrderQty) + (Number(get) ? Number(get) : ''))).toFixed(2) :
                (discount === 'Discount' && PTR !== "") ? percentOff !== '' ? ( Number(PTR) - (((percentOff !== '' ? Number(percentOff) : 100) / 100) * Number(PTR))).toFixed(2) < 0 ? 0 : (Number(PTR) - (((percentOff !== '' ? Number(percentOff) : 100) / 100) * Number(PTR))).toFixed(2) : (((percentOff !== '' ? Number(percentOff) : 100) / 100) * Number(PTR)).toFixed(2) : PTR !== "" ? Number(PTR).toFixed(2) : 0}</span>}</span>
              </div>
            </DialogContent>

            <DialogActions className="pr-4">

              <Button onClick={this.handleRequestClose} color='secondary' >
                Cancel
            </Button>
              {
                buttonType === 'edit' ? <Button type='submit' color='primary'>
                  Submit
             </Button> : <Button type='submit' color='primary'>
                    Add
            </Button>
              }

            </DialogActions>
          </form>
        </Dialog>
      </React.Fragment>
    );
  }
}

EditInventory = connect(
  null,
  {
    getInventoryList
  }           // bind account loading action creator
)(EditInventory)

export default EditInventory = reduxForm({
  form: 'EditInventory',// a unique identifier for this form
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  initialValues: getFormInitialValues('EditInventory')(),
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('EditInventory')();
    dispatch(initialize('EditInventory', newInitialValues));
  }
})(EditInventory)

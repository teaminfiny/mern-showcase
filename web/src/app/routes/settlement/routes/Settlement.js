import React, { Component } from "react";
import MUIDataTable from "../../../../components/DataTable";
import Button from '@material-ui/core/Button';
import { Col, Row, Form, Badge, Label, Input } from 'reactstrap';
import { FormControl } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import moment from 'moment';
import customHead from 'constants/customHead';
import { connect } from 'react-redux';
import { getSettlements } from '../../../../actions/seller';
import CircularProgress from '@material-ui/core/CircularProgress';
import { DatePicker } from 'material-ui-pickers';

const customStyles = {
  pendingSettlementRow: {
    '& td': { backgroundColor: "#00000012" }
  },
  pendingSettlementRow: {
    '& td': { backgroundColor: "#00000012" }
  }
};


class Settlement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      hide: false,
      delete: false,
      add: false,
      page: 0,
      perPage: 50,
      searchedText: '',
      month:moment().format('MMMM'),
      year: moment().format('YYYY'),
      loading: true,
      fromDate: new Date(),
      toDate: new Date(),
      to:moment().format(),
      from: moment().format(),
      // selectedDate: moment().format('MM/YYYY'),
    }
  }

  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  componentDidMount = () => {
    let data = {
      page:1,
      perPage:50,
      from_date:this.state.from ? moment(this.state.from).format('YYYY-MM-DD'):  moment(this.state.from).format('YYYY-MM-DD'),
      to_date: this.state.to ? moment(this.state.to).add(1,'days').format('YYYY-MM-DD'):moment(this.state.to).add(1,'days').format('YYYY-MM-DD'),
    }
    this.props.getSettlements({ data, history: this.props.history })
    setTimeout(() => {
      this.setState({ loading: false })
    }, 1300)
  }

  changePage = (page) => {
    let pages = page + 1
    let data = {
      page: pages,
      perPage: this.state.perPage,
      filter: '',
      // searchText: '',
      from_date:this.state.from ? moment(this.state.from).format('YYYY-MM-DD'): moment().format(),
      to_date: this.state.to ? moment(this.state.to).add(1,'days').format('YYYY-MM-DD'): moment().format(),
    }
    this.props.getSettlements({ data, history: this.props.history })
    this.setState({ page ,loading:true})
    setTimeout(() => {
      this.setState({ loading: false })
    }, 1300)
  };

  changeRowsPerPage = (perPage) => {
    let data = {
      page: 1,
      perPage: perPage,
      filter: '',
      // searchText: '',
      month: this.state.month,
      year: this.state.year
    }
    this.props.getSettlements({ data, history: this.props.history })
    this.setState({ page: 0, perPage })
  }


  resetFilter = (filter) => {
    let data = {
      page: 1,
      perPage: this.state.perPage,
      filter: '',
      from_date: moment().format('YYYY-MM-DD'),
      to_date:  moment().add(1,'days').format('YYYY-MM-DD')
    }
    this.props.getSettlements({ data, history: this.props.history })
    // e.preventDefault();
    filter();  
    this.setState({
      filter: '',
      toDate:'',
      fromDate:'',
      month:'',
      year:'',
      to:moment().format(),
      from: moment().format()
    });

  }
  applyFilter = (filter) => {
    filter()
    
    let data = {
      page: 1,
      perPage: this.state.perPage,
      // searchText:this.state.searchedText ? this.state.searchedText : '',
      filter: this.state.filter === 'none' ? '' : this.state.filter,
      // to_date:this.state.toDate ? this.state.toDate : '',
      // from_date:this.state.fromDate ? this.state.fromDate : '',
      from_date:this.state.from ? moment(this.state.from).format('YYYY-MM-DD'):  moment(this.state.from).format('YYYY-MM-DD'),
      to_date: this.state.to ? moment(this.state.to).add(1,'days').format('YYYY-MM-DD'):moment(this.state.to).add(1,'days').format('YYYY-MM-DD'),
    }
    this.props.getSettlements({ data, history: this.props.history })
    this.setState({
      // month:this.state.to ? moment(this.state.to).format('MMMM'):'',
      // year:this.state.to ? moment(this.state.to).format('YYYY'):'',
      // to_date:this.state.toDate ? this.state.toDate : '',
      // from_date:this.state.fromDate ? this.state.fromDate : '',
      to:moment(this.state.to).format('MM/DD/YYYY') ,
      from: moment(this.state.from).format('MM/DD/YYYY') 
    })
  }
  handleChange = (key, event) => {
    this.setState({ [key]: event.target.value });
  }
  handleDateChange = (date,key) => {
    this.setState({ [key] :  moment(date).format() })
  }
  handletoDateChange = (data) => {
    // moment(data).format('mm/DD/YYYY')
    this.setState({
      toDate: data
      // month: moment(data).format('MMMM'),
      // year: moment(data).format('YYYY') 
    })
  }
  render() {

    let { sellerSettlements } = this.props;
    let { from, to } = this.state;
    const columns = [
      {
        name: "orderId",
        label: "Order ID",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "orderDate",
        label: "Order Date",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "orderValue",
        label: "Order Value",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "tds",
        label: "TDS",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "tcs",
        label: "TCS",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "surg",
        label: "Surchrg",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "commission",
        label: "Commission",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "netAmount",
        label: "Net Amount",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "paymentDueDate",
        label: "Payment Due Date",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "paymentDate",
        label: "Payment Date",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "transactionId",
        label: "Transaction ID",
        options: {
          filter: true,
          filterType: 'custom',
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          },
          customFilterListRender: v => {
            return false;
          },
          filterOptions: {
            names: [],
            logic() {
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => (
              <React.Fragment >
                
                <Row form style={{ maxWidth: 300,
                  //  minHeight:260
                   }}>
                 
                <Col md={5} xl={5} xs={5} sm={5} lg={5}>
                
                  {/* <FormControl className="w-100 mb-2 "> */}
                  <Label for="fromDate">FROM</Label>
                      <DatePicker
                        onChange={(date) => this.handleDateChange(date, 'from')}
                        name='from'
                        id="from"
                        value={from}
                        // autoOk
                        fullWidth
                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                        format="DD/MM/YYYY"
                      />
                  {/* </FormControl> */}
                </Col>
                 {/* </Row>  */}
               
               {/* <Row form> */}
                <Col md={5} xl={5} xs={5} sm={5} lg={5}>
                
                  <FormControl >
                  <Label for="toDate">TO</Label>
                  <DatePicker
                        onChange={(date) => this.handleDateChange(date, 'to')}
                        name='to'
                        id="to"
                        value={to}
                        // autoOk
                        fullWidth
                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                        format="DD/MM/YYYY"
                      />
                  </FormControl>
                </Col>
                
                {/* </Row> */}

                {/* <Row form style={{ maxWidth: 150 }}> */}
                  
                </Row>
                <div style={{ paddingTop: 15 }} >
                  <Button variant="contained" onClick={() => this.applyFilter(applyFilter)} className='filterButton' color='primary'>Apply Filter</Button>
                  <Button variant="contained" onClick={() => this.resetFilter(applyFilter)} className='filterButton' color='primary'>Reset Filter</Button>
                </div>
              </React.Fragment>
            ),
            onFilterChange: () => {
            }
          },
        },
      }

    ];

    const statusStyle = (status) => {
      return status.includes("Pending") ? "text-white bg-grey" : status.includes("Settled") ? "text-white bg-success" : "text-white bg-grey";
    }


    let data=[]
    
     sellerSettlements && sellerSettlements.settlements && sellerSettlements.settlements[0] && sellerSettlements.settlements[0].data.map((dataOne) => {
      data.push({
        
            orderId:dataOne.order.order_id,
            orderDate:dataOne.order.createdAt,
            sellerName: <div >{dataOne.seller && dataOne.seller.company_name}</div>,
            orderValue:"₹" + (dataOne.order_value).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}),
            tds:"₹" + (dataOne.tds).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}),
            tcs:"₹" + (dataOne.tcs).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}),
            commission:"₹" + (dataOne.commission).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}),
            surg: dataOne.commission_comp && dataOne.commission_comp.prod_surge_comm ? (dataOne.commission_comp.prod_surge_comm).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}) : 'N/A',
            netAmount:"₹" + (dataOne.net_amt).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}),
            paymentDueDate:dataOne.payment_due_date_ft,
            status: dataOne.status=='Pending' ? <div key={'pending'} className={` badge text-uppercase ${statusStyle('Pending')}`}>Pending</div>:<div  className={` badge text-uppercase ${statusStyle('Settled')}`}>Settled</div>,
            paymentDate:moment(dataOne.payment_date).format('DD/MM/YYYY'),
            transactionId: dataOne.transaction_id,
            
      })
    })

    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      rowsPerPage: this.state.perPage,
      page: this.state.page ,
      fixedHeader: false,
      print: false,
      download: false,
      filter: true,
      sort: false,
      selectableRows: false,
      count: sellerSettlements && sellerSettlements.settlements[0] && sellerSettlements.settlements[0].metadata && sellerSettlements.settlements[0].metadata[0] && sellerSettlements.settlements[0].metadata[0].total,
      serverSide: true,
      rowsPerPage: this.state.perPage,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      search: false,
      searchText: this.state.searchedText,
      setRowProps: (row) => {
        return {
          className: classnames(
            {
              [this.props.classes.pendingSettlementRow]: row[4].key === 'Pending'
            }),
        };
      },
      textLabels: {
        filter: {
          all: "All",
          title: "FILTERS",
        },
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
        }
      },
    }

    return (
      <React.Fragment>
        {/* <ContainerHeader match={this.props.match} title={<IntlMessages id={ `Settlements `} />} /> */}
        {
          this.state.loading == false ?
            <MUIDataTable
              // title={'Earnings this month: ' + `₹${totalEarning}`}
              data={data}
              columns={columns}
              options={options}
            />
            :
            <div className="loader-view" style={{ textAlign: "center", marginTop: "300px" }}>
              <CircularProgress />
            </div>
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ seller }) => {
  let { sellerSettlements, totalEarning } = seller;
  return { sellerSettlements, totalEarning }
}
export default connect(mapStateToProps, { getSettlements })(withStyles(customStyles)(Settlement));
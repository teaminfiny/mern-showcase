import React, { Component } from 'react';
import moment from 'moment'
import { connect } from 'react-redux';
import { getListGroupSettlement } from 'actions/seller'
import AppConfig from 'constants/config'
import MUIDataTable from "components/DataTable";
import Button from '@material-ui/core/Button';
import RenderDatePicker from 'components/RenderDatePicker/new_index'
import customHead from 'constants/customHead'
import {Label, Col, Row, } from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import { Field } from 'redux-form'
import ButtonGroup from '@material-ui/core/ButtonGroup';
import CircularProgress from '@material-ui/core/CircularProgress';

class SettlementsReports extends Component {
    constructor(props) {
        super(props);
        let date = new Date()
        this.state = {
            detailCards: '',
            month: date.getMonth(),
            year: date.getFullYear(),
            tempMonth: date.getMonth(),
            tempYear: date.getFullYear(),
            page: 0,
            perPage:50,
            from_date:'',
            to_date:''
        }
    }
    componentDidMount = () => {
        let data ={sellerId: this.props.match.params.id }
        this.props.getListGroupSettlement({ data })
    }
    handleDateChange = (date,key) => {
        this.setState({ [key] :  date })
        // moment(date).format('MM/DD/YYYY')
    }
    applyFilter = (filter) => {
        filter()
        let data = {
          page: 1,
          perPage: this.state.perPage,
          sellerId: this.props.match.params.id,
          searchText:this.state.searchedText ? this.state.searchedText : '',
          from_date:this.state.from ? this.state.from: moment(),
          to_date: this.state.to ? this.state.to: moment()
        //   filter: !this.state.status  ? {user_type:'seller', mainUser:null,user_status:{'$ne':'denied'}} : {user_type:'seller', mainUser:null,user_status:this.state.status},
        //   name:this.state.searchedText ? this.state.searchedText :''
        }
        this.props.getListGroupSettlement({ data, history: this.props.history });
        this.setState({
          page:0
        })
    }
    resetFilter = (filter) => {
        let data = {
          page: 1,
          perPage: 50,
          sellerId: this.props.match.params.id,
        //   filter: !this.state.status === '' ? '' : {user_type:'seller', mainUser:null,user_status:{'$ne':'denied'}},
        //   name:this.state.searchedText ? this.state.searchedText :''
        }
        this.props.getListGroupSettlement({ data, history: this.props.history })
        filter();
        this.setState({
          filter: '',
          status: '',
          showFilter: false,
          page:0
    
        })
      }
      changePage = (page) => {
        let pages = page + 1;
        let data = {
          page: pages,
          perPage: this.state.perPage,
          sellerId: this.props.match.params.id,
          searchText:this.state.searchedText ? this.state.searchedText : '',
          from_date:this.state.from ? this.state.from: moment(),
          to_date: this.state.to ? this.state.to: moment()
        //   filter:  !this.state.status ? {user_type:'seller', mainUser:null,user_status:{'$ne':'denied'}} : {user_type:'seller', mainUser:null,user_status:this.state.status},
        //   name:this.state.searchedText ? this.state.searchedText :''
        }
        this.props.getListGroupSettlement({ data, history: this.props.history })
        this.setState({ page })
      };
      handleSearch = (searchText) => { 
        let data = { page: 1,
          perPage: this.state.perPage,
          searchText:searchText
        //   filter: !this.state.status  ? {user_type:'seller', mainUser:null,user_status:{'$ne':'denied'}} : {user_type:'seller', mainUser:null,user_status:this.state.status},
        //   name:searchText
        }
    
        this.props.getListGroupSettlement({ data, history: this.props.history })
        this.setState({ searchedText: searchText, page:0 })
      };   
      report = (data) =>{
        return <ButtonGroup color="primary" aria-label="outlined primary button group">
        {/* <Button variant="outlined" className={'text-warning'} onClick={(e)=>this.send(data)}>Send</Button> */}
        <Button variant="outlined" className={'text-primary'} onClick={(e)=>this.redirect(data)}>Report</Button>
        </ButtonGroup>
      }
      
      redirect = (data) =>{
        window.open(`${AppConfig.baseUrl}groupSettlements/generateRemittance?Id=${data._id}`, '_blank');
      }
    render() {
        const { selectedDate } = this.state
        const { listGroupSettlement } = this.props
        const options = {
            filterType: 'dropdown',
            responsive: 'scroll',
            viewColumns: false,
            selectableRows: false,
            // resizableColumns:true,
            page: this.state.page,
            perPage: this.state.perPage,
            fixedHeader: false,
            print: false,
            download: false,
            filter: false,
            sort: false,
            selectableRows: false,
            serverSide: true,
            count: this.props.consumerTotal,
            server: true,
            selectableRowsOnClick: false,
            selectableRows: 'none',
            fixedHeader: false,
            search: false,
            onTableChange: (action, tableState) => {
                switch (action) {
                    case 'changePage':
                        this.changePage(tableState.page);
                        break;
                    case 'search':
                        this.handleSearch(tableState.searchText)
                        break;    

                }
            },
        }
        const columns = [
            {
              name: "utr",
              label: "UTR",
              options: {
                filter: false,
                sort: false,
                customHeadRender: (o, updateDirection) => {
                  return customHead(o, updateDirection);
                }
              }
            },
            {
              name: "orderValue",
              label: "Order Value",
              options: {
                filter: false,
                sort: false,
                customHeadRender: (o, updateDirection) => {
                  return customHead(o, updateDirection);
                }
              }
            }, 
            {
              name: "netAmt",
              label: "Net Amt",
              options: {
                filter: false,
                sort: false,
                customHeadRender: (o, updateDirection) => {
                  return customHead(o, updateDirection);
                }
              }
            },
            {
              name: "paymentDate",
              label: "Payment Date",
              options: {
                filter: false,
                sort: false,
                customHeadRender: (o, updateDirection) => {
                  return customHead(o, updateDirection);
                }
              }
            },          
            {
              name: "report",
              label: "Report",
              options: {
                filter: true,
                filterType: 'custom',
                customHeadRender: (o, updateDirection) => {
                  return customHead(o, updateDirection);
                },
                customFilterListRender: v => {
                  return false;
                },
                filterOptions: {
                  names: [],
                  logic() {
                  },
                  display: (filterList, handleCustomChange, applyFilter, index, column) => (
                    <React.Fragment>
                      <Row form style={{ maxWidth: 300 }}>
                      <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                  <FormControl className="w-100 mb-2 ">
                  <Label for="fromDate">FROM</Label>
                    <Field className="form-control" placeholder='MM/DD/YYYY'  autoComplete='off'
                    name="from"  id="from" onChange={(date) => this.handleDateChange(date, 'from')}
                    value={moment(selectedDate).format()} 
                    component={RenderDatePicker}
                    />
                  </FormControl>
                </Col>
               
                <Col md={6} xl={6} xs={6} sm={6} lg={6}>
                  <FormControl >
                  <Label for="toDate">TO</Label>
                  <Field className="form-control" placeholder='MM/DD/YYYY' autoComplete='off' 
                  name="to"  id="to" onChange={(date) => this.handleDateChange(date, 'to')}
                  value={moment(selectedDate).format()}
                  component={RenderDatePicker}/>
                  </FormControl>
                </Col>
                </Row> 
                      <div style={{ paddingTop: 15 }} >
                        <Button variant="contained" onClick={() => this.applyFilter(applyFilter)} className='filterButton' color='primary'>Apply Filter</Button>
                        <Button variant="contained" onClick={() => this.resetFilter(applyFilter)} className='filterButton' color='primary'>Reset Filter</Button>
                      </div>
                    </React.Fragment>
                  ),
                  onFilterChange: () => {
                  }
                },
              },
            },
          ];
        let data1=[]
        listGroupSettlement && listGroupSettlement.data[0] && listGroupSettlement.data[0].data.map((dataOne,index) => {
            data1.push({
              utr:dataOne.utrNo ? dataOne.utrNo : 'N/A',
              orderValue:<div>₹{(dataOne.order_value).toLocaleString('en-IN',{maximumFractionDigits:2})}</div>,
              netAmt:<div>₹{(dataOne.net_amt).toLocaleString('en-IN',{maximumFractionDigits:2})}</div>,
              paymentDate:moment(dataOne.payment_date).format('DD/MM/YYYY'),
              report:this.report(dataOne)
            })
        })
        

        return (
            <div className="dashboard animated slideInUpTiny animation-duration-3">
        { this.props.loading == false ?
          <MUIDataTable
            title=''
            data={data1}
            columns={columns}
            options={options}
          />:
        <div className="loader-view" style={{ textAlign: "center", marginTop: "150px" }}>
          <CircularProgress />
        </div>
        }
            </div>
        );
    }
}

const mapStateToProps = ({ seller }) => {
    const { listGroupSettlement, loading } = seller

    return { listGroupSettlement, loading }
}
export default SettlementsReports = connect(mapStateToProps,{getListGroupSettlement})(SettlementsReports)
import React, { Component } from "react";
import FullWidthTabs from '../../components/routes/tabs/fixed/FullWidthTabsSettlement';
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
// import SettlementHeader from './SettlementHeader'

class SettlemetsTab extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Settlements"/>} /> 
        {/* <SettlementHeader /> */}

        <FullWidthTabs history={this.props.history} match={this.props.match} tabFor={'settlement'}  />
      </React.Fragment>
    );
  }
}

export default SettlemetsTab;
import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from '../../../util/asyncComponent';

const Settlement = ({match}) => (
  <div className="app-wrapper">
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/settlementList`}/>
      <Route path={`${match.url}/settlementList`} component={asyncComponent(() => import('./routes/SettlementTab'))}/>
      {/* <Route path={`${match.url}/`} component={asyncComponent(() => import('./routes/Settlement'))}/> */}
      <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/>
    </Switch>
  </div>
);

export default Settlement;

import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import ContainerHeader from '../../../../components/ContainerHeader';
import IntlMessages from '../../../../util/IntlMessages';
import Button from '@material-ui/core/Button';
import { Col, Row, Label, Input } from 'reactstrap';
import { FormGroup } from '@material-ui/core';
import customHead from 'constants/customHead'


const columns = [
  {
    name: "narration",
    label: "Narration",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "amount",
    label: "Amount",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
  {
    name: "date",
    label: "Date",
    options: {
      filter: true,
      filterType: 'custom',
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      },
      customFilterListRender: v => {
        return false;
      },
      filterOptions: {
        names: [],
        logic() {
        },
        display: () => (
          <React.Fragment>
            <Row form>
              <Col md={6} xl={6} xs={6} lg={6}>
                <FormGroup>
                  <Label for="exampleSelect">Select Month</Label>
                  <Input type="select" name="select" id="exampleSelect">
                    <option>January</option>
                    <option>February</option>
                    <option>March</option>
                    <option>April</option>
                    <option>May</option>
                    <option>June</option>
                    <option>July</option>
                    <option>August</option>
                    <option>September</option>
                    <option>October</option>
                    <option>November</option>
                    <option>December</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col md={6} xl={6} xs={6} lg={6}>
                <FormGroup>
                  <Label for="exampleSelect">Select Month</Label>
                  <Input type="select" name="select" id="exampleSelect">
                    <option>2015</option>
                    <option>2016</option>
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <div style={{ paddingTop: 15 }} >
              <Button variant="contained" className='filterButton' color='primary'>Apply Filter</Button>
              <Button variant="contained" className='filterButton' color='primary'>Reset Filter</Button>
            </div>
          </React.Fragment>
        ),
      },
    }
  },
  {
    name: "status",
    label: "Status",
    options: {
      filter: false,
      sort: false,
      customHeadRender: (o, updateDirection) => {
        return customHead(o, updateDirection);
      }
    }
  },
];




const options = {
  filterType: 'dropdown',
  print: false,
  download: false,
  selectableRows: 'none',
  selectableRowsOnClick: false,
  selectableRowsHeader: false,
  responsive: 'scrollMaxHeight',
  search: true,
  viewColumns: false,
  rowHover: false,
  textLabels: {
    filter: {
      all: "All",
      title: "FILTERS",
    },
  }
};

class Transaction extends Component {
  render() {

    const statusStyle = (status) => {
      return status.includes("Pending") ? "text-white bg-grey" : status.includes("Success") ? "text-white bg-success" : "text-white bg-grey";
    }

    const data = [
      {
        narration: 'Settlement against 4 orders', amount: "₹10", status: <div className={` badge text-uppercase ${statusStyle('Success')}`}>Success</div>, date: '22/08/2019'
      },
      { narration: 'Settlement against 4 orders', amount: "₹20", status: <div className={` badge text-uppercase ${statusStyle('Success')}`}>Success</div>, date: '22/08/2019' },
      { narration: 'Settlement against 4 orders', amount: "₹5", status: <div className={` badge text-uppercase ${statusStyle('Success')}`}>Success</div>, date: '22/08/2019' },
      { narration: 'Settlement against 4 orders', amount: "₹50", status: <div className={` badge text-uppercase ${statusStyle('Success')}`}>Success</div>, date: '22/08/2019' },
      { narration: 'Settlement against 4 orders', amount: "₹21", status: <div className={` badge text-uppercase ${statusStyle('Pending')}`}>Pending</div>, date: '22/08/2019' },
      { narration: 'Settlement against 4 orders', amount: "₹15", status: <div className={` badge text-uppercase ${statusStyle('Pending')}`}>Pending</div>, date: '22/08/2019' },
      { narration: 'Settlement against 4 orders', amount: "₹05", status: <div className={` badge text-uppercase ${statusStyle('Pending')}`}>Pending</div>, date: '22/08/2019' }
    ];

    return (
      <React.Fragment>
        <ContainerHeader match={this.props.match} title={<IntlMessages id="Transactions (Sept 28, 2019)" />} />
        <MUIDataTable
          data={data}
          columns={columns}
          options={options}
        />
      </React.Fragment>
    );
  }
}

export default Transaction;
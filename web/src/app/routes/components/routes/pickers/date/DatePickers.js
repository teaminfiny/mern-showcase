import React, { Component } from 'react';
import moment from 'moment';
// import {DatePicker} from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const ref = React.createRef();

export default class DatePickers extends Component {
  constructor(props){
    super(props)
    this.state = {
      selectedDate : moment()
    };
  }
  

  handleDateChange = (date) => {
    this.props.handledateChange(date, this.props.id)
  };

  render() {
    const { selectedDate } = this.state;
    return (
      <KeyboardDatePicker
        // margin="normal"
        id="date-picker-dialog"
        label=""
        format="MM/dd/yyyy"
        minDate={selectedDate}
        value={this.props.date}
        onChange={this.handleDateChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
      />
    )

  }
}
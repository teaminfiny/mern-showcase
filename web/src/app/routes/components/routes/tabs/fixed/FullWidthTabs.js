import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { changeTab } from '../../../../../../actions/tabAction';
import {connect} from 'react-redux';
import OrderList from '../../../../orders/routes/OrderList';
import OrderHistory from '../../../../orders/routes/OrderHistory';
import OldManifest from '../../../../orders/routes/OldManifest';
import { withRouter } from 'react-router-dom';

function TabContainer({children, dir}) {
  return (
    <div dir={dir}>
      {children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class FullWidthTabs extends Component {
  constructor(props){
  super(props);
    this.state = {
    value: 0,
  };
}

  handleChange = (event, value) => {
    this.props.changeTab(value)
    this.props.history.replace(`/seller/orders/orderList`)
  };

  handleChangeIndex = index => {
    this.props.changeTab(index)
  };

  render() {
    const {theme, tabValue, tabFor} = this.props;
    return (
      <div className="w-100" >
        <AppBar position="static" color="default">
          <Tabs
            value={tabValue}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            scrollButtons="on"
          >
            <Tab className="tab" label="Open"/>
            <Tab className="tab" label="History"/>
            <Tab className="tab" label="Old Manifest"/>
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={tabValue}
          onChangeIndex={this.handleChangeIndex}
        >
          <TabContainer dir={theme.direction}>
            {
              tabFor === 'order' ? <OrderList history={this.props.history} tab={tabValue} location={this.props.location}/> : ''
            }
          </TabContainer>
          <TabContainer dir={theme.direction}>
            {
              tabFor === 'order' ? <OrderHistory history={this.props.history} tab={tabValue} location={this.props.location}/> : ''
            }
          </TabContainer>
          <TabContainer dir={theme.direction}>
            {
              tabFor === 'order' ? <OldManifest history={this.props.history} tab={tabValue} location={this.props.location}/> : ''
            }
          </TabContainer>
        </SwipeableViews>
      </div>
    );
  }
}

FullWidthTabs.propTypes = {
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = ({tabAction}) => {
  const {tabValue} = tabAction;
  return { tabValue}
};

export default connect(mapStateToProps, {changeTab}) ( withStyles(null, {withTheme: true})(withRouter(FullWidthTabs)));
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { changeTab } from '../../../../../../actions/tabAction';
import {connect} from 'react-redux';
import Settlement from 'app/routes/settlement/routes/Settlement';
import Report from 'app/routes/settlement/routes/SettlementReport';
import { withRouter } from 'react-router-dom';


function TabContainer({children, dir}) {
  return (
    <div dir={dir}>
      {children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class FullWidthTabsSettlement extends Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.props.changeTab(value)
  };

  handleChangeIndex = index => {
    this.props.changeTab(index)
  };

  render() {
    const {theme, tabValue, tabFor} = this.props;
    return (
      // style={{boxShadow :'0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)'}}
      <div className="w-100" >
        <AppBar position="static" color="default">
          <Tabs
            value={tabValue}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            scrollButtons="on"
          >
            <Tab className="tab" label="All"/>
            <Tab className="tab" label="Remittance advice"/>
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={tabValue}
          onChangeIndex={this.handleChangeIndex}
        >
          <TabContainer dir={theme.direction} index={tabValue}>
            {
             tabFor === 'settlement' ? <Settlement history={this.props.history} match={this.props.match}/> : ''
            }
          </TabContainer>
          <TabContainer dir={theme.direction} index={tabValue}>
            {
              tabFor === 'settlement' ? <Report history={this.props.history} match={this.props.match}/> : ''
            }
          </TabContainer>
        </SwipeableViews>
      </div>
    );
  }
}

FullWidthTabsSettlement.propTypes = {
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = ({tabAction}) => {
  const {tabValue} = tabAction;
  return { tabValue}
};

export default connect(mapStateToProps, {changeTab}) ( withStyles(null, {withTheme: true})(withRouter(FullWidthTabsSettlement)));
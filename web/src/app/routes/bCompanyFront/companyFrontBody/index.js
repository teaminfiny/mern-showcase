import React from "react";
import Widget from "components/Widget";
import PropTypes from 'prop-types';
import { aboutList } from 'app/routes/socialApps/routes/Profile/data';
import { withStyles } from '@material-ui/core/styles';
import CompanyProductCard from 'app/routes/bCompanyFront/companyProductCard';
import{FormGroup,Label,Input, Button} from 'reactstrap';
import 'components/profile/About/index.css'
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import {
  getProductsByCompany,
  getCategories
} from 'actions/buyer';
function TabContainer({ children, dir }) {
  return (
    <div dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </div>
  );
}
TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};
class CompanyFrontBody extends React.Component {
  constructor(props) {
    super(props);
  this.state = {
    aboutList,
    value: '',
    loader:'false',
    fetch: true,
    page: 1,
    perPage:16,
    compItems: '',
    dataL:10,
    thisPagecount:16,
    hidden: false,
  }
}
  handleChange = (event) => {
    this.setState({ value:  event.target.value });  
    this.props.getProductsByCompany({company_id: this.props.match.params.id,category_id:event.target.value})
  };
  // handleChangeIndex = index => {
  //   this.setState({ value: index });
  // };
componentDidMount = () => {
  this.props.getProductsByCompany({company_id: this.props.match.params.id})
  this.props.getCategories({company_id: this.props.match.params._id,page: this.state.page,
    perPage: this.state.perPage})
}

  thisPagecountFn = async() => {
   await this.setState({hidden:true})
   this.props.getProductsByCompany({page: this.state.page,
    perPage: this.state.perPage + 16,
    company_id: this.props.match.params.id,
    category_id: this.state.value ? this.state.value :''})
    await this.setState({hidden:true,})
    const t = setTimeout(() =>{
      this.setState({perPage: this.state.perPage + 16, hidden:false})
    }, 2000)
  }
  render() {
    const {dataFromParent,productsByCompany,categories} = this.props;
    const { value, hidden } = this.state;
    return (
      <Widget styleName="jr-card-full jr-card-tabs-right jr-card-profile "  >
        <div position="static" color="default" >
           <FormGroup style={{marginRight:'15px',float:'right',width:'25%'}} >
           <Label for="exampleSelect"></Label>
        <Input type="select" name="select" 
        onChange={this.handleChange} 
        value={value}
        >
          <option value={''}>All</option>
          {
            categories && categories.detail && categories.detail.map((val,i)=>
            <option value={val._id} key = {i}>{val.name}</option>
            )
          }
          </Input>
            </FormGroup>
        </div>

        <div className="jr-tabs-classic">
          <div className="jr-tabs-content jr-task-list">
               
                <TabContainer>
                <div className="row">
                  {productsByCompany && productsByCompany.results ? null :
                  <React.Fragment>
                      <div className="loader-view" style={{margin:'auto'}}    >
                  <CircularProgress />
                </div>
                    </React.Fragment>
                  }
                  
               { productsByCompany && productsByCompany.results.length > 0 ?
               <React.Fragment>
                  { productsByCompany && productsByCompany.results.map((product, index) =>
                      <div
                        className="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12" key={product._id}>
                        <CompanyProductCard
                        dataFromParentTwo={dataFromParent}
                        styleName="pb-4" headerStyle="bg-gradient-primary" product={product}  />
                      </div>)}
                </React.Fragment>  
                   
                    :

                    productsByCompany && productsByCompany.results.length < 1 ?
                    
                    
                    <React.Fragment>
                    <div style={{ margin: 'auto', width: '50%',textAlign:' center' }}>
                    <span className="text-danger" >
                      <i class="zmdi zmdi-alert-circle animated wobble zmdi-hc-5x" style={{fontSize:'90px'}} ></i>
                    </span><br/><br/>
                    <h1>Sorry, no results found !</h1>
                  </div>
                  </React.Fragment>
                  : null
                 
              }
                </div>
                { !hidden ? 
                <div style={{textAlign: "center"}}>
                {(productsByCompany && productsByCompany.count > this.state.thisPagecount) &&
                   <Button color="primary" className="jr-btn jr-btn-lg btnPrimary"
                  onClick={this.thisPagecountFn}>
                  See More
                  </Button>
                }</div> :              
                <div className="loader-view">
                <CircularProgress />
                </div>
                }
              </TabContainer>
              {/* //new// */}
          </div>
        </div>
      </Widget>
    );
  }
}
const mapStateToProps = ({ buyer }) => {
  const { productsByCompany, categories} = buyer;
  return { productsByCompany, categories }
};
CompanyFrontBody = connect(
  mapStateToProps,
  {
    getProductsByCompany,
    getCategories
  }         
)(CompanyFrontBody)
export default withStyles(null, { withTheme: true })(CompanyFrontBody);
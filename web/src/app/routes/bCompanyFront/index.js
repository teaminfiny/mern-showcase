import React, { Component } from "react";
import CompanyFrontBody from "./companyFrontBody";
import CompanyeHeader from "./companyHeader";
import {getProductsByCompany} from "actions/buyer";
import { connect } from 'react-redux'
class BCompanyFront extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.myRef = React.createRef()
  }
  // componentDidMount = () => this.handleScroll()
  componentDidMount() {
    this.handleScroll()
    this.props.getProductsByCompany({company_id: this.props.match.params.id})
    const {productsByCompany} = this.props;
  }
  handleScroll = () => {
    const { index, selected } = this.props
    if (index === selected) {
      this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }
  render() {
    const {productsByCompany} = this.props;
    return (
      <div className="app-wrapper" ref={this.myRef}>
        <CompanyeHeader match={this.props.match}
        dataFromParent={productsByCompany} />
        <div className="jr-profile-content">
          <div className="row">
            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
              <CompanyFrontBody 
              history={this.props.history} match={this.props.match}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = ({ buyer}) => {
  const { productsByCompany } = buyer
  return { productsByCompany }
};
export default connect(mapStateToProps, { getProductsByCompany })(BCompanyFront);
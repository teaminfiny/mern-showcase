import React, { Component } from 'react';
import { Carousel, CarouselItem } from 'reactstrap';
import { NavLink, withRouter } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import './index.css'
import helpertFn from 'constants/helperFunction';
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';
class CompanyProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }
  onExiting() {
    this.animating = true;
  }
  onExited() {
    this.animating = false;
  }

  next(size) {
    if (this.animating) return;
    // const nextIndex = this.state.activeIndex === ProductData.length - 1 ? 0 : this.state.activeIndex + 1;
    if(size === 1){
      this.setState({ activeIndex: 0});
    }
    else if(this.state.activeIndex === 0){
      this.setState({ activeIndex: this.state.activeIndex + 1 });
    }
  }


  previous(size) {
    if (this.animating) return;
    // const nextIndex = this.state.activeIndex === 0 ? ProductData.length - 1 : this.state.activeIndex - 1;
    if(size === 1){
      this.setState({ activeIndex: 0});
    }
    else if(this.state.activeIndex === 1){
    this.setState({ activeIndex: this.state.activeIndex - 1 });
    }
  }
  
  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }
  handleDelete = () => {
  }
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  handleClick = () => {
    this.props.history.push('/view-seller')
    // window.open("/view-seller", "_blank")
  }

  addDefaultSrc(ev){
    ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
  }

  render() {
    
    const { product } = this.props
    const { activeIndex } = this.state
    const chemCombination = product && product && product && product.chem_combination
    const type = product && product.medicine_type_id && product.medicine_type_id.name;
    const bgColor = type === 'Ethical branded' || type === 'Others' ? '#ff7000' :
                    type === 'Cool chain' ? '#0b68a8' :
                    type === 'Surgical' || type ==='OTC' || type ==='Generic' ? '#038d0e' :'#072791'
    const slides = product.images.length>0?product.images.map((image, index) => {
      return (
        <CarouselItem
        key={index}
      >
        <NavLink className="buyerRedirectNavlink" to={`/search-results/category/search=${encodeURIComponent(product.name)}`}>
        <img src =  { product.images.length === 0 ? logo :`${helpertFn.productImg(image)}`  }  onError={this.addDefaultSrc} />
        </NavLink>
      </CarouselItem>
    );
  }):[<CarouselItem
    key={'images'}
  >
    <NavLink className="buyerRedirectNavlink" to={`/search-results/category/search=${encodeURIComponent(product.name)}`}>
    <img src =  { logo   }   />
    </NavLink>
  </CarouselItem>]
    const chipData = [
      { key: 0, class: 'originalPrice font', offer: 200, price: 400, by: 'Medideals', seller : 'Ajay' },
      { key: 1, class: 'priceColor', offer: 'Buy 1 Crocin get 2 Free', price: 50, by: 'Anacin', seller : 'Praful' },
      { key: 2, class: 'priceColor', offer: 'Buy 1 Crocin get 2 Combiflame Free', price: 100, by: 'Cipla', seller : 'Lekhraj' },
      { key: 3, class: 'originalPrice font', offer: 500, price: 1000, by: 'Wellness 24', seller : 'Avanish' },
      { key: 5, class: 'priceColor', offer: 'Buy 5 Vicks get 2 Saradon Free', price: 100, by: 'Cipla', seller : 'Eldhose' },
      { key: 4, class: 'originalPrice font', offer: 1000, price: 2000, by: 'Medideals', seller : 'Alex' },
    ]

    
    return (
      <React.Fragment>
        {
          chipData[this.props.index] !== undefined ?
            <div className={`jr-card text-left ${this.props.match.url === '/seller/dashboard' ? 'pb-0 ':''}`}>
              <div className={`jr-card-header-color ${(this.props.match.url === '/view-seller') ? 'intranetCardViewSeller': this.props.match.url === '/seller/dashboard' ? 'intranetCardSellerDashboard' : 'intranetCard'}`} style={{position:'relative', display:'inline-flex', padding: '0px'}}>
                <Carousel
                  autoPlay={false}
                  indicators={true}
                  activeIndex={activeIndex}
                  next={() => this.next(slides.length)}
                  interval={false}
                  previous={() => this.previous(slides.length)}
                  className='itemImage'>
                  {slides}
                  {/* <CarouselControl cssModule='primary' direction="prev" directionText="Previous" onClickHandler={() => this.previous(slides.length)} />
                  <CarouselControl direction="next" directionText="Next" onClickHandler={() => this.next(slides.length)} /> */}
                </Carousel>
                <span style={{ padding:'2px', fontWeight:'bold', position: 'absolute', zIndex: 1, backgroundColor:`${bgColor}`, color:'white', width:'auto'}} className='itemImage' >{type === 'Others' ? 'PCD' :type}</span>
                {/* {
                  this.props.match.url === '/view-company' ? null : this.props.match.url === "/seller/dashboard" ? null : <Fab className="jr-badge-up bg-primary"><i className="zmdi zmdi-shopping-cart" /></Fab>
                } */}
              </div>
              <div className="jr-card-body mb-2 pt-2" style={{height:"150px",width:"180px"}}>
                <div className="product-details">
                  <NavLink className="buyerRedirectNavlink" to={`/search-results/category/search=${encodeURIComponent(product.name)}`}>
                    <h4 className='mb-1'>{product.name}</h4>
                  </NavLink>
                  <p style={{color:"#202790",marginBottom:"1px"}}>Description :</p>
                  {
                    (this.props.history.location.pathname === '/seller/dashboard') ? null :
                      <p className={'pt-4 mb-0'}>NO DATA</p>
                  }
                  {
                    this.props.match.url !== '/seller/dashboard' ? null : <p className="d-flex  mb-0 align-items-baseline">Sold by <h4 className="text-primary ml-1">{chipData[this.props.index].seller}</h4> </p>
                  }
                </div>
              </div>
            </div>
            :
            <div className="jr-card text-left pb-2" style={{overflow:'hidden'}}>
              <div className={`jr-card-header-color ${this.props.match.url === '/view-seller' ? 'intranetCardViewSeller' : 'intranetCard'}`} style={{minHeight:'244px', position:'relative', display:'inline-flex', padding: '0px'}}>
                <Carousel
                  autoPlay={false}
                  indicators={true}
                  activeIndex={activeIndex}
                  next={() => this.next(slides.length)}
                  interval={false}
                  previous={() => this.previous(slides.length)}>
                  {slides}
                  {/* <CarouselControl cssModule='primary' direction="prev" directionText="Previous" onClickHandler={() => this.previous(slides.length)} />
                  <CarouselControl direction="next" directionText="Next" onClickHandler={() => this.next(slides.length)} /> */}
                </Carousel>
                <span style={{ padding:'5px', fontWeight:'bold', position: 'absolute', zIndex: 1, backgroundColor:`${bgColor}`, color:'white', width:'auto'}} className='itemImage' >{type === 'Others' ? 'PCD' : type}</span>
                {/* {
                  this.props.match.url === '/view-company' ? null : this.props.match.url === "/seller/dashboard" ? null : <Fab className="jr-badge-up bg-primary"><i className="zmdi zmdi-shopping-cart" /></Fab>
                } */}
              </div>
              <div className="jr-card-body mb-2 pt-2" style={{height:"150px",width:"180px"}}  >
                <div className="product-details">
                  
              

                   <Tooltip
                   className="d-inline-block"
                   id="tooltip-right"
                   title={
                    <span  className='mb-1'>{product.name}</span>
                   }
                   placement="right"
                 >
                    <NavLink className="buyerRedirectNavlink" to={`/search-results/category/search=${encodeURIComponent(product.name)}`}>
                   <h4 style={{minHeight: "31px",maxHeight: "31px"}} >
                     {(product.name).slice(0, 25) + ((product.name).length > 25 ? "..." : '')}
                   </h4></NavLink>
                  </Tooltip>
                   
                  
                  
                <div style={{minHeight: "15px", maxHeight: "15px", marginBottom:"5px", padding:"0px"}}>
                  {
                  helpertFn.showPrepaid(product.medicine_type_id.name, product.isPrepaid && product.isPrepaid, false) &&
                  <span className='text-white bg-danger' style={{margin:"0px", padding:"0px 5px"}}>Only Prepaid</span>
                  }

                  {
                    chemCombination.length > 50 ?
                      <React.Fragment >
                        <p className="ellipsis" />
                        <p style={{ color: "#202790", marginBottom: "1px" }}>Description :</p>
                        <h5 className={'pt-1 mb-0'}>{(product && product.chem_combination).slice(0, 65) + "..."}</h5>
                      </React.Fragment> :
                      <React.Fragment >
                        <p />
                        <p style={{ color: "#202790", marginBottom: "1px" }}>Description :</p>
                        <h5 className={'pt-1 mb-0'}>{product && product.chem_combination}</h5>
                      </React.Fragment>
                  }

                </div>
              </div>
            </div>
            </div>
        }
      </React.Fragment>
    );
  }
}
export default withRouter(CompanyProductCard);
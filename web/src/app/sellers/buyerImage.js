import React, { Component } from "react";
import Avatar from '@material-ui/core/Avatar';

const buyer = (buyerName, buyerImage) => {
  // const buyer = (buyerName) => {
  return <div className="user-detail">
    <div className="user-profile d-flex flex-row align-items-center">
      {
        buyerImage === undefined ? < Avatar
          alt={buyerName}
          src={require(`assets/productImage/avatar.png`)}
          className="user-avatar"
        /> : <Avatar
            alt='Image'
            src={require(`assets/productImage/${buyerImage}`)}
            className="user-avatar"
          />
      }

      <div className="user-detail-name">
        <h5 className="user-name">{buyerName} </h5>
      </div>
    </div>
  </div>
}

export default buyer;

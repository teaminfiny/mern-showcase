import React from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from 'components/Header/index';
import Sidebar from 'containers/SideNav/index';
import Dashboard from '../routes/dashboard';
import Orders from '../routes/orders';
import Inventory from '../routes/inventory';
import Settlement from '../routes/settlement';
import Transaction from '../routes/transaction';
import EditProfile from '../routes/editProfile';
import ChangePassword from '../routes/changePassword';
import ProductRequest from '../routes/productRequest';
import ComplainceForm from '../routes/complainceForm';
import ListNotification from '../routes/listNotification';
import Rating from '../routes/rating';
import Staff from '../routes/staff';
import Groups from '../routes/groups';
import {
  ABOVE_THE_HEADER,
  BELOW_THE_HEADER,
  COLLAPSED_DRAWER,
  FIXED_DRAWER,
  HORIZONTAL_NAVIGATION,
} from 'constants/ActionTypes';
import { getSidebar } from 'actions/seller'
import ColorOption from 'containers/Customizer/ColorOption';
import { isIOS, isMobile } from 'react-device-detect';
import asyncComponent from '../../util/asyncComponent';
import TopNav from 'components/TopNav';
import { NotificationContainer, NotificationManager } from 'react-notifications';
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  componentDidMount() {
    this.props.getSidebar({ history: this.props.history })
  }

  render() {
    const { match, location, drawerType, navigationStyle, horizontalNavPosition, sidebar } = this.props;
    const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'fixed-drawer' : drawerType.includes(COLLAPSED_DRAWER) ? 'collapsible-drawer' : 'mini-drawer';

    //set default height and overflow for iOS mobile Safari 10+ support.
    if (isIOS && isMobile) {
      document.body.classList.add('ios-mobile-view-height')
    }
    else if (document.body.classList.contains('ios-mobile-view-height')) {
      document.body.classList.remove('ios-mobile-view-height')
    }

    return (
      <div className={`app-container ${drawerStyle}`}>
        <NotificationContainer />
        <Sidebar />
        <div className="app-main-container">
          <div
            className={`app-header ${navigationStyle === HORIZONTAL_NAVIGATION ? 'app-header-horizontal' : ''}`}>
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === ABOVE_THE_HEADER) &&
              <TopNav styleName="app-top-header" />}
            <Header />
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === BELOW_THE_HEADER) &&
              <TopNav />}
          </div>

          <main className="app-main-content-wrapper">
            <div className="app-main-content">
              {
                location.pathname.includes('/edit-Profile') || location.pathname.includes('/list-Notification') || location.pathname.includes('/change-Password') || location.pathname.includes('/ratings') ? 
                <Switch>
                  <Route path={`${match.url}/edit-Profile`} component={EditProfile} />
                  <Route path={`${match.url}/change-Password`} component={ChangePassword} />
                  <Route path={`${match.url}/list-Notification`} component={ListNotification} />
                  <Route path={`${match.url}/ratings`} component={Rating} />
                </Switch> : sidebar && sidebar.length > 0 ? sidebar.findIndex((e) => location.pathname.includes(e.pathname)) > -1 ?
                  <Switch>
                    <Route path={`${match.url}/dashboard`} component={Dashboard} />
                    <Route path={`${match.url}/orders`} component={Orders} />
                    <Route path={`${match.url}/inventory`} component={Inventory} />
                    <Route path={`${match.url}/settlement`} component={Settlement} />
                    <Route path={`${match.url}/transaction`} component={Transaction} />
                    <Route path={`${match.url}/product-Request`} component={ProductRequest} />
                    <Route path={`${match.url}/complaince-form`} component={ComplainceForm} />
                    <Route path={`${match.url}/staffs`} component={Staff} />
                    <Route path={`${match.url}/groups`} component={Groups} />
                  </Switch> : <Switch> <Route component={asyncComponent(() => import('app/routes/extraPages/routes/404'))} /> </Switch> : ''
              }
            </div>
            {/* <Footer/> */}
          </main>
        </div>
        <ColorOption />
      </div>
    );
  }
}


const mapStateToProps = ({ settings, seller }) => {
  const { drawerType, navigationStyle, horizontalNavPosition } = settings;
  const { sidebar } = seller;
  return { drawerType, navigationStyle, horizontalNavPosition, sidebar }
};
export default withRouter(connect(mapStateToProps, { getSidebar })(App));
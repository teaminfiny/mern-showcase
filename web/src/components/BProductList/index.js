import React from 'react';
import ProductGridItem from 'components/eCommerce/ProductGridItem'
import productData from "./productData";

function ProductsGrid(props) {
  console.log('props',props)
  return (
    <div className="row animated slideInUpTiny animation-duration-3">
      {productData.map((product, index) => (
        <ProductGridItem key={index} showAddToCard = {props.showAddToCard} product={product} history = {props.history} classValue={props.classValue}/>
      ))}
    </div>
  );
}

export default ProductsGrid;
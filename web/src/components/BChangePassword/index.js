import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import {FormGroup} from 'reactstrap';


import { Field, reduxForm, getFormInitialValues, initialize } from 'redux-form'
import { required, passwordMatch, minLength6 } from '../BEditProfile/BEditValidation';
import { NotificationManager } from 'react-notifications';
import ReactStrapTextField from 'components/ReactStrapTextField';
import axios from '../../constants/axios';




class BuyerChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            oldPassword:'',
            password:'',
            confirmPassword:'',
            loading:false,
            showNotification: true
         }
    }
    componentWillUnmount() {
        clearTimeout(this.timer);
      }
      handleButtonClick = () => {
        if (!this.state.loading) {
          this.setState(
            {
              loading: true,
              oldPassword:'',
              password:'',
            confirmPassword:'',
            },
            () => {
              this.timer = setTimeout(() => {
                this.setState({
                  loading: false,
                });
              }, 2000);
            },
          );
        }
      };
      timer = undefined;
    handleChange =(key,event) => {
    this.setState({ [key]: event.target.value });
    };
    
  onSubmit = async () => {

    let data = {
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
    }

    await axios.post('/users/changePassword', data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('buyer_token')
      }
    }
    ).then(result => {
      let that = this;
      if (this.state.showNotification) {
        if (result.data.error) {
          NotificationManager.error(result.data.title);
        } else {
          NotificationManager.success(result.data.title);
        }
        that.setState({ showNotification: false });
      }
    })
      .catch(error => {
        NotificationManager.error('Something went wrong, Please try again')
      });
  }


    render() {
        const {oldPassword,password,confirmPassword,} = this.state; 
        const {handleSubmit} = this.props;
        // const Gender = ['Male','Female','Other']
        return ( 
            
                <div className='mt-5 mb-5'>
              <form onSubmit={handleSubmit(this.onSubmit)}  autoComplete="off">
                 
                    <div className = 'row justify-content-center'>
              
                      <Field name="password" id="password" type="password"
                      classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                      label='New Password'
                       value={password}
                      component={ReactStrapTextField}
                      validate={[required, minLength6]}
                        onChange={(e)=>this.handleChange('password',e)}
                        autoComplete="off" />
                    
                      </div> 
                      <div className = 'row justify-content-center '>
                    
                    <Field name="confirmPassword" id="confirmPassword" type="password" 
                    classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                    label='Confirm Password'
                    value={confirmPassword}
                    component={ReactStrapTextField}
                    validate={[passwordMatch]}                   
                      onChange={(e)=>this.handleChange('confirmPassword',e)}
                      autoComplete="off" />
                    </div>
                    <div className = 'row justify-content-center mt-2 '>
                  <FormGroup  className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10 buyerEditProfile'}>
                        <Button color="primary" type='submit' variant="contained" className="jr-btn jr-btn-lg btnPrimary" >
                            
                            
                            Save
                        </Button>
                    </FormGroup>
                  </div>
                  </form>
                </div>
           
         );
    }
}


export default BuyerChangePassword = reduxForm({
  form: 'BuyerChangePassword',// a unique identifier for this form
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  initialValues: getFormInitialValues('BuyerChangePassword')(),
  onSubmitSuccess: (result, dispatch) => {
    const newInitialValues = getFormInitialValues('BuyerChangePassword')();
    dispatch(initialize('BuyerChangePassword', newInitialValues));
  }
})(BuyerChangePassword)


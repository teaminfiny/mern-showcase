import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { changeTab } from '../../actions/tabAction';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import CustomScrollbars from 'util/CustomScrollbars';
import { getProductRequestList } from 'actions/buyer';
import ProductRequest from './ProductRequest'

function TabContainer({children, dir,index,value}) {
  return (
    <div dir={dir}>
      {value==index&&children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class FullWidthProductRequestTab extends Component {
  state = {
    value: 0,
    tabValue:0,
    page: 1,
    rowsPerPage: 50,
    month:moment().format("MMMM"),
    year: moment().format("GGGG")
  };

  componentDidMount(){ 

    const {page, rowsPerPage, month, year} = this.state;

    let data = {page, rowsPerPage, month, year, tab:'open'};

    this.props.getProductRequestList({history:this.props.history, data});

  }

  handleChange = (event, value) => {
    this.setState({tabValue:value})
  };

  handleChangeIndex = index => {
    this.setState({tabValue:index})
  };

  render() {


    const {theme, tabFor, productRequestList} = this.props;
    const { tabValue } = this.state
    return (
      <div className="w-100" >
        <AppBar position="static" color="default">
          <Tabs
            value={tabValue}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            scrollButtons="on"
          >
            <Tab className="tab" label="Open"/>
            <Tab className="tab" label="History"/>
          </Tabs>

        </AppBar>       

          <TabContainer dir={theme.direction} value = {tabValue} index= {0}>

            <CustomScrollbars className="messages-list scrollbar" style={{ height: 650 }}>
              <ProductRequest
                history={this.props.history}
                requestFromParent={productRequestList}
                value={this.state}
                identifier={'productRequestList'}
              />
            </CustomScrollbars>
          </TabContainer>

          <TabContainer dir={theme.direction} value = {tabValue} index= {1}>

            <CustomScrollbars className="messages-list scrollbar" style={{ height: 650 }}>

              <ProductRequest
                history={this.props.history}
                requestFromParent={productRequestList}
                value={this.state}
                identifier={'productRequestHistory'}
              />

            </CustomScrollbars>

          </TabContainer>

      
      </div>
    );
  }
}

FullWidthProductRequestTab.propTypes = {
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = ({tabAction,buyer}) => {
  const {tabValue} = tabAction;
  const { productRequestList } = buyer;
  return { tabValue, productRequestList}
};

export default withRouter(connect(mapStateToProps, {changeTab, getProductRequestList})( withStyles(null, {withTheme: true})(FullWidthProductRequestTab)));
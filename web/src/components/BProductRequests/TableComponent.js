import React, { Component } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import './index.css'
import momemt from 'moment';
import { Link } from 'react-router-dom'

class TableComponent extends Component{

    constructor(props) {
        super(props)
    }

    render() {

        console.log('rows in table', this.props.row)
        return (
            <React.Fragment>
                
                <TableRow
                    hover
                    className='cursor-pointer'
                    key={this.props.row.productName}
                    align={'left'}
                    component={this.props.row.stock && this.props.row.stock ===true ? Link : 'row'} 
                    to={`/search-results/category/search=${encodeURIComponent(this.props.row.productName)}`}
                >

                    <TableCell align={'left'} >{this.props.row.productName}</TableCell>

                    <TableCell align="left">{this.props.row.manufacturer}</TableCell>
                    
                    <TableCell align="left">{this.props.row.quantity}</TableCell>
                    <TableCell align="left">
                        <span>{momemt(this.props.row.requestedDate).format('DD/MM/YYYY')}</span>
                        <p style={{margin:0, padding:0}}>{momemt(this.props.row.requestedDate).format('HH:mm A')}</p>
                    </TableCell>
                    <TableCell align="left">{this.props.row.status}</TableCell>
                    {this.props.identifier === 'productRequestList' ? <TableCell align="left">{this.props.row.action}</TableCell> : <TableCell align="left">{this.props.row.stock && this.props.row.stock===true ? <div className="badge text-primary" style={{margin:0, padding:0}}>IN-STOCK</div>: <div className="badge text-danger" style={{margin:0, padding:0}}>NOT AVAILABLE</div>}</TableCell>}
                </TableRow>
            </React.Fragment>
        )
    }

}

export default TableComponent;
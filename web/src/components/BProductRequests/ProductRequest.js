import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';
import {Button} from '@material-ui/core'
import TableComponent from './TableComponent';

import { withStyles } from '@material-ui/core/styles';

import EnhancedTableHead from './EnhancedTableHead'
import EnhancedTableToolbar from './EnhancedTableToolbar'
import EnhancedTableToolbarOpen from './EnhancedTableToolbarOpen'
import CircularProgress from '@material-ui/core/CircularProgress';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import axios from 'constants/axios';
import { getProductRequestList } from 'actions/buyer';
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let date = new Date();

const useStyles = theme => ({
  root: {
    width: '100%',
    marginTop: 0,
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 400,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  list: {
    maxWidth: 239
  },
  fontSize: {
    fontSize: '1rem'
  }
});

class ProductRequest extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      rowsPerPage: 50,
      month: moment().format("MMMM"),
      year: moment().format("GGGG"),
      popOverOpen : false,
      cancelData : {}
    }
  }

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage })
  };

  handleChangeRowsPerPage = event => {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    })
  };

  componentWillReceiveProps =(nextProps) => {
    if(this.props.requestFromParent && this.props.requestFromParent.data && this.props.requestFromParent.data.length > 0 && nextProps.requestFromParent.data && nextProps.requestFromParent.data.length > 0 ){
      this.setState({
        month : moment(nextProps.requestFromParent.data[0].createdAt).format("MMMM"),
        year : moment(nextProps.requestFromParent.data[0].createdAt).format("GGGG")
      })
    }
  }

  cancelClicked = (data) =>{
    this.setState({popOverOpen:true, cancelData:data})
  }

  closeCancel = () =>{
    this.setState({popOverOpen:false, cancelData:{}})
  }

  confirmCancel = async() => {
    let status = 'CANCELLED'
    let token = localStorage.getItem('buyer_token');
    let changedData={
      id : this.state.cancelData._id,
      status : status
    }
      await axios({
        method: 'POST',
        url:'buyerRequest/updateBuyerRequest',
        data:changedData,
        headers: {
            'Content-Type': 'application/json',
            'token': token
        }
      }).then(result => {
        if (result.data.error) {
          NotificationManager.error(result.data.title)
        } else {
          NotificationManager.success(result.data.title)
          let data = {
              page: 1,
              perPage: 50,
              tab:'open',
              month: this.state.month,
              year: this.state.year,
          }
          this.props.getProductRequestList({ data, history: this.props.history })
        }
      })
        .catch(error => {
          NotificationManager.error('Something Went Wrong !')
        });
    this.setState({popOverOpen:false, cancelData:{}})
  }

  render() {
    const { classes, identifier } = this.props;


    const statusStyle = (status) => {
      return status.includes("PENDING") ? "text-white bg-primary" : status.includes("DENIED") ? "text-white bg-danger" : status.includes("APPROVED") ? "text-white bg-warning": status.includes("FULFILLED") ? "text-white bg-success" : "text-white bg-primary";
    }

    const { requestFromParent } = this.props;

    let rows = []

    requestFromParent && requestFromParent.data && requestFromParent.data.result && requestFromParent.data.result.map((dataOne, index) => {
      let manufacturer = dataOne.manufacturer && dataOne.manufacturer.name ? dataOne.manufacturer.name : dataOne.otherManufacturer;
      rows.push({
        productName: dataOne.product_name,
        manufacturer: manufacturer,
        quantity: Number(dataOne.quantity),
        status: <div key={'recent'}
          className={` badge text-uppercase 
                     ${statusStyle(
            dataOne.isFulfilled === true ? "FULFILLED"
            :
            dataOne.status === "PENDING" ?
            "PENDING"
            :
            dataOne.status === "APPROVED" ?
              "APPROVED"
              :
              dataOne.status === "DENIED" ?
                "DENIED":''
            )}`}>

          {dataOne.isFulfilled === true ? "FULFILLED" : dataOne.status}
        </div>,
        stock:dataOne.inStock ? dataOne.inStock : false ,
        requestedDate: (dataOne.createdAt),
        action: <div>{identifier === "productRequestList" ? <Button variant="outlined" className="text-danger" onClick={() => this.cancelClicked(dataOne)}>CANCEL</Button> : ''}</div>
      })
    })

    let productData = requestFromParent ? requestFromParent.data ? requestFromParent.data : [] : []
    let counts = requestFromParent && requestFromParent.data.metadata[0] && requestFromParent.data.metadata[0].total ? requestFromParent.data.metadata[0].total : 0;
    return (
      <React.Fragment>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          {identifier === "productRequestHistory" ?
            <EnhancedTableToolbar identifier={identifier}/>
            :
            identifier === "productRequestList" ?
            <EnhancedTableToolbarOpen identifier={identifier}/>
            :
            null
          }
          {
            this.props.loading ?   
              <div className="loader-view">
                <CircularProgress />
              </div>
            :
          <div className={classes.tableWrapper}>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                identifier = {identifier}
                rowCount={rows.length}
              />

              <TableBody>
                {rows.length < 1 ? <TableRow><TableCell colSpan={6} style={{textAlign:'center'}}>Sorry, no matching records found !</TableCell></TableRow> :
                rows.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                  .map((row, index) => {
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (

                      <TableComponent
                        row={row}
                        labelId={labelId}
                        value={this.props.value}
                        identifier = {identifier}
                        itemListFromParent={productData} />

                    )
                  })}

              </TableBody>
            </Table>
          </div>
          }
          <TablePagination
            rowsPerPageOptions={[]}
            component="div"
            count={counts}
            rowsPerPage={this.state.rowsPerPage}
            page={this.state.page}
            backIconButtonProps={{
              'aria-label': 'previous page',
            }}
            nextIconButtonProps={{
              'aria-label': 'next page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>

      </div>
      {/*------------------------- Confirm Cancel Dialog ------------------------------------------ */}
      <Dialog open={this.state.popOverOpen} onClose={this.closeCancel} fullWidth={true}
          maxWidth={'sm'}>
          <DialogTitle>
            Cancel Product Request
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              {'Are you sure you want to cancel this product request ?'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() =>this.closeCancel()} color='secondary' >
              No
            </Button>
            <Button color='primary' onClick={() =>this.confirmCancel()} >
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { loading, productRequestList } = buyer;
  return { loading, productRequestList }
};

export default withRouter(connect(mapStateToProps, { getProductRequestList })(withStyles(useStyles)(ProductRequest)))
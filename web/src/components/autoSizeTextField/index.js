import React, { PureComponent } from 'react';
import { TextField } from 'react-md';
// class AutoResizing extends PureComponent {
//     state = {
//       value: 'This is some default text to place',
//       max: 2,
//     };
  
//     setDiv = (div) => {
//       this.div = div;
//       this.setMaxWidth();
//     };
  
//     setMaxWidth = () => {
//       // Make sure mobile devices don't overflow
//       if (this.div) {
//         this.setState({ max: Math.min(340, this.div.offsetWidth) });
//       }
//     };
  
//     handleChange = (value) => {
//       this.setState({ value });
//       this.props.input.onChange()
//     };
  
//     render() {
//       const { value, max } = this.state;
//       let {input,margin, label, type, fullWidth,meta} = this.props;
//       let { asyncValidate, touched, error, warning } = meta;
//       let val = (touched && error) || (warning ? true : false)
//       let err = '';
//       if (typeof error == 'object') {
//           console.log('value... inside')
//           console.log('value........', error.exception)
//           err = error.exception
//       } else {
//           console.log('value..... else')
//           err = error
//       }
//       console.log('autoResize',input)
//       return (
//         <div className="md-grid">
//           <div className="md-cell md-cell--12" ref={this.setDiv}>
//             <TextField
//               id="autoresizing-1"
//               label={label}
//             name={input.name}
//             type={type}
//             margin={margin?margin:'normal'}
//             helperText={(touched && err)}
//             {...input}
//             onChange = {this.handleChange}
//             fullWidth={fullWidth?fullWidth:true}
//             error={val ? true : false}
//             minDate = {new Date()}
//               resize={{ max }}
//             />
//           </div>
//           </div>
//       )
//     }
//     }

const renderField = ({ input, label,setDiv, type,max, meta: { asyncValidate, touched, error, warning } }) => {
    let val = (touched && error) || (warning ? true : false)
    let err = '';
    if (typeof error == 'object') {
        console.log('value... inside')
        console.log('value........', error.exception)
        err = error.exception
    } else {
        console.log('value..... else')
        err = error
    }

    console.log("touched value........",input)
    return <div className="md-grid">
    <div className="md-cell md-cell--12" ref={setDiv}>
        <TextField
            label={label}
            name={input.name}
            type={type}
            margin="normal"
            helperText={(touched && err)}
            {...input}
            error={val ? true : false}
            minDate={new Date()}
            resize={{ max }}
        />
        </div>
    </div>

}

export default renderField
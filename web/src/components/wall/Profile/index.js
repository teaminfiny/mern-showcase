import React, {Component} from "react";
import Button from '@material-ui/core/Button';
import {connect} from "react-redux";
import Auxiliary from "util/Auxiliary";
import InfoCard from 'components/InfoCard';
import {Col} from 'reactstrap'

const cardData = {
  icon: 'mic-outline',
  title: '23',
  subTitle: 'Orders',
  color: '#ff9800',
};
const orderData = {
  icon: 'collection-folder-image',
  title: '₹ 387',
  subTitle: 'Spent',
  color: '#f44336',
}
class Profile extends Component {

  state = {
    isFollow: false
  }

  handleToggle = () => {
    this.setState((previousState) => ({
      isFollow: !previousState.isFollow
    }));
  }

  render() {
    const {user, userInfo} = this.props;
    const {id, name, image, address} = user;
    const {followers, following, frinds} = userInfo;
    return (
      <Auxiliary>
        <div className="jr-profileon">
          <div className="jr-profileon-thumb jr-profileon-thumb-htctrcrop">
            <img src={image} alt=''/>
          </div>
          <div className="jr-profileon-content">
            <p className="jr-profileon-title">{name}</p>
            <span className="jr-fs-sm">{address}</span>
          </div>
        </div>

        <div className="jr-follower text-center">
          <div className='row'>
            <Col sm={6} xs={6} md={6} lg={6} xl={6}>
              <InfoCard data={cardData}/>
            </Col>
            <Col  sm={6} xs={6} md={6} lg={6} xl={6}>
            <InfoCard className='NetChart' data={orderData}/>
            </Col>
          </div>
          
        </div>
        
      </Auxiliary>
    )
  }
}

const mapStateToProps = ({auth}) => {
  const {authUser} = auth;
  return {authUser}
};

export default connect(mapStateToProps)(Profile)

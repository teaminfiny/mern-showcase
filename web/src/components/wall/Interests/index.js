import React, { cloneElement, Component } from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
import { getUserDetail } from 'actions';
import { getMediWallet } from 'actions/buyer';
import { withRouter } from 'react-router-dom';

const lists = [
  { id: 1, icon: 'edit', name: 'Edit Profile', color: 'warning' },
  { id: 2, icon: 'shopping-basket', name: 'Product Requests', color: 'secondary' },
  { id: 3, icon: 'truck', name: 'Orders', color: 'info' },
  { id: 4, icon: 'card', name: 'Transactions', color: 'success' },
  { id: 5, icon: 'balance-wallet', name: 'MediWallet', color: 'danger' },
  { id: 6, icon: 'receipt', name: 'Compliance', color: 'primary' }];


class Interests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      tabValue:0,
      wallet:0

    }
  }

  handleClick = (value) => {
    this.props.changeTab(value)
  }
  componentWillReceiveProps = (nextProps) => {
    if(this.props.users != nextProps) {
      this.setState({wallet :nextProps && nextProps.users.wallet_balance })
    }
  }
  componentDidMount=()=>{
    const {users} = this.props;
  this.setState({wallet: users && users.wallet_balance });
  }
  render() {

    // const { users } = this.props;

    return (
      <div className="jr-entry-sec">
        <div className="jr-card p-0">
          <List dense={true}>
            {lists.map((value) =>
              <ListItem selected={value.name === this.props.title} button onClick={() => this.handleClick(value.name)}>
                <IconButton>
                    <i className={`zmdi zmdi-${value.icon} text-${value.color}`} />
                </IconButton>
                <ListItemText
                  primary={<div>
                    <span>
                      {
                        value.name
                      }
                    </span>
                    <span className="float-right">
                    {value.name === 'MediWallet' ? this.props.users === undefined ? '' : 'MNY ' + (this.state.wallet).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2})  + '/-' : ''}
                    </span>
                  </div>}
                  // secondary={value.name === 'MediMoney' ? `Bal :  ₹220` : ''}
                >
                </ListItemText>
              </ListItem>
            )}
          </List>
        </div>
      </div>
    )
  }
}


// export default Interests;
const mapStateToProps = ({ auth, seller, buyer }) => {
  const { userDetails } = seller
  const { user_details } = auth;

  let users = userDetails ? userDetails : user_details

  return {  user_details, users }
};

export default connect(mapStateToProps, { getUserDetail, getMediWallet})(withRouter(Interests));
import React, { Component } from 'react';
import {
  Col,
  Row
} from 'reactstrap';
import EnlargedImageContainerDimensions from './EnlargedImageContainerDimensions';

import './index.css';

export default class Magnify extends Component {
    constructor(props){

        super(props);
        // this.handleMouseHover = this.handleMouseHover.bind(this);
        this.state = {
          users:null,
          galleryImages: null,
          isHovering: false
        }
      }
    render() {
        console.log('magnify value=',this.props.value)
        return (
            <div>
               
                  
              
             
                    <Row>
                        <Col >
                            <EnlargedImageContainerDimensions value = {this.props.value}/>
                        </Col>
                    </Row>

            </div>
        );
    }
}

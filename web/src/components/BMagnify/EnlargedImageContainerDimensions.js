import React, { Component } from 'react';
import ReactImageMagnify from 'react-image-magnify';
import './index.css';


export default class EnlargedImageContainerDimensions extends Component {
    
    render() {
        
        return (
            <div className="fluid">
                <div className="fluid__image-container">
                    <ReactImageMagnify {...{
                         smallImage: {
                            alt: 'Wristwatch by Ted Baker London',
                            isFluidWidth: false,
                            src: this.props.value.galleryImages.photo,
                            width:300,
                            height:500,
                            zIndex:'99',
                            sizes: '(max-width: 500px) 100vw, (max-width: 1200px) 30vw, 400px'
                        },
                        largeImage: {
                            src: this.props.value.galleryImages.photo,
                            width: 1000,
                            height: 1000,
                            zIndex:'99'
                        },
                        // enlargedImageContainerDimensions: {
                        //     width: '100%',
                        //     height: '100%',
                            
                        // }
                        shouldUsePositiveSpaceLens: true,
                        lensStyle: {
                            background: 'hsla(600,100%,50%,0.3)',
                            border: '0px solid #ccc'
                          }
                    }} />
                </div>
                
                <div style={{height: '500px'}} />
            </div>
        );
    }
}

import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import renderTextField from '../../../components/textBox';
import { accountNumber, ifscCode, required } from '../../../constants/validations';
import { Col, Row } from 'reactstrap';
import FieldFileInput from '../../../components/FieldFileInput';
import FieldCheckBox from '../../../components/checkBoxField'
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import './style.css'

class Form3 extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        const { handleSubmit, previousPage,submitting  } = this.props
        return (
            <form onSubmit={handleSubmit} autocomplete="off">
                <Grow in={true}>
                    <React.Fragment>
                        <Row>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field name="account_no" label="Account Number" component={renderTextField} validate={accountNumber} />
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field name="ifsc_code" label="IFSC Code" component={renderTextField} validate={ifscCode} />
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field name="recipient_name" label={'Recipient Name'} component={renderTextField} validate={required} />
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field name="cancelledCheck" component={FieldFileInput} label="Cancelled Check" validate={(value)=> (value === undefined ? 'Please Upload Cancelled Check' : '')} />
                            </Col>
                            <Col sm={12} md={12} xs={12} lg={12} xs={12} xl={12} className="mt-2">
                                <Field name="policy" component={FieldCheckBox} label="Accept Terms and Conditions" validate={required}/>
                            </Col>
                        </Row>
                        <div className='row'>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
                            {
                                !submitting&&
                                <div className='list-action mt-3'>
                                    <Button variant='contained' color='primary' className='m-2 signin' size='medium' type="button" onClick={previousPage}>Previous</Button>
                                </div>
                            }
                                
                                <div className='list-action mt-3'>
                                    <Button style={{color:'white'}} type='submit' variant='contained' disabled = {submitting} color='primary' className='m-2 signin' size='medium'>{submitting?'Processing':'Finish'}</Button>
                                </div>
                            </Col>
                        </div>
                    </React.Fragment>
                </Grow>
            </form>
        )
    }
}
Form3 = reduxForm({
    form: 'wizard', //Form name is same
    destroyOnUnmount: false,
})(Form3)

export default Form3;
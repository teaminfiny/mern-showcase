const validate = values => {
    const errors = {}
    if (!values.firstName) {
      errors.firstName = 'This field is required'
    }
    if (!values.lastName) {
      errors.lastName = 'This field is required'
    }
    if (!values.email) {
      errors.email = 'This field is required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Please enter valid email address'
    }
    if (!values.user_state) {
      errors.user_state = 'This field is required'
    }
    if (!values.user_state) {
      errors.user_state = 'This field is required'
    }
    if (!values.user_type) {
      errors.user_type = 'This field is required'
    }
    return errors
  }
  
  export default validate
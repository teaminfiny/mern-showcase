import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Col, Row } from 'reactstrap';
import Grow from '@material-ui/core/Grow';
import TextField from '@material-ui/core/TextField';
import renderTextField from '../../../components/textBox';
import RenderSelectField from '../../../components/RenderSelectField';
import { required, emailField, passwordMatch, minValue10, validatePassword,specailChar, phone } from '../../../constants/validations';
import asyncValidate from '../../../constants/asyncValidate';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
// import asyncValidate from '../../../constants/asyncValidate';
import axios from '../../../constants/axios';
import { Input } from '@material-ui/core';

const renderField = ({ input, label, type, meta: { asyncValidating, touched, error, warning } }) => {
	let val = (touched && error) || (warning ? true : false) || asyncValidating
	if (asyncValidating) {
		error = "Email already exists"
	}
	return <div>
		<TextField
			className={asyncValidating ? 'async-validating' : ''}
			label={label}
			name={input.name}
			type={type}
			margin="normal"
			helperText={(touched && error)}
			{...input}
			fullWidth={true}
			error={val ? true : false}
			minDate={new Date()}
		/>
	</div>

}


class Form1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			emailExist: '',
			states: []
		}
	}


	goToSignIN = () => {
		this.props.history.push('/signin')
	}

	handleChange = (e) => {
		return this.props.touch('email')
	}

	componentDidMount = async () => {
		await axios.get('/admin/getStates', {}, {
			headers: {
				'Content-Type': 'application/json',
				'token': localStorage.getItem('token')
			}
		}
		).then(result => {
			let that = this;
			if (result.data.error) {
			} else {
				that.setState({ states: result.data.detail })
			}
		})
			.catch(error => {
			});
	}

	render() {
		const { handleSubmit } = this.props;
		const { emailExist, states } = this.state;
		return (
			<form onSubmit={handleSubmit} autocomplete="off">
				<Grow in={true}>
					<React.Fragment>
						<Row>
							{/* <Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Input
									name="user_type"
									label="User Type"
									fullWidth={true}
									margin="normal"
									readOnly
									value="Seller"
								>
								</Input>
							</Col> */}
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="company_name" component={renderTextField} label="Company" validate={required,specailChar} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="email" component={renderTextField} onChange={(e) => this.handleChange(e)} validate={emailField} label="Email" />
							</Col>
							
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="first_name" component={renderTextField} label="First Name" validate={required,specailChar} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="last_name" component={renderTextField} label="Last Name" validate={required,specailChar} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="user_address" component={renderTextField} label="Address Line 1" validate={required} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="user_address1" component={renderTextField} label="Address Line 2" />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="user_city" component={renderTextField} label="City" validate={required,specailChar} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								{/* <Field name="user_state" component={renderTextField} label="State" validate={required} /> */}
								<Field
									name="user_state"
									component={RenderSelectField}
									label="State"
									validate={required}
									fullWidth={true}
									margin="normal"
								>
									<option value="" />
									{
										states && states.length > 0 ? states.sort(function(a, b){
											var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
											if (nameA < nameB) //sort string ascending
												return -1 
											if (nameA > nameB)
												return 1
											return 0 //default return value (no sorting)
										}).map((state, key) => {
											return <option value={state._id}>{state.name}</option>
										}) : ''
									}
								</Field>
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="user_pincode" component={renderTextField} label="Pin Code" validate={(value) => (value === undefined ? 'This field is required' : isNaN(value) ? 'Please enter valid pin code number': '')} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="phone" component={renderTextField} label="Contact No" validate={phone} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field id="password" type="password" name="password" component={renderTextField} label="Password" validate={[validatePassword]} />
							</Col>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<Field name="confirmPassword" type="password" component={renderTextField} label="Confirm Password" validate={passwordMatch('password')} />
							</Col>
							{/* {(value) => (value === undefined ? 'This field is required' : isNaN(value) ? 'Please enter valid phone number' : value.length < 10 ? 'Please enter valid phone number' : '')} */}
							

						</Row>
						<div className='row'>
							<Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
								<div className='list-action mt-3'>
									<Button style={{color:'white'}} type='submit' variant='contained'  color='primary' className='m-2 signin' size='medium'> Next </Button>
								</div>
							</Col>
							<Col className='signInFooterend'>
								<span style={{ textAlign: 'end' }}>
									<p>Already have an account? <a href='javascript:void(0);' onClick={this.goToSignIN}>Sign in</a></p>
								</span>
							</Col>
						</div>
					</React.Fragment>
				</Grow >
			</form >
		)
	}
}

Form1 = reduxForm({
	form: 'wizard',     // <------ same form name
	asyncValidate,
	destroyOnUnmount: false,
})(Form1)

export default withRouter(Form1)
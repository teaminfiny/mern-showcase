import React, { Component } from 'react';
import { Row } from 'reactstrap'
import Slider from "react-slick";
import ProductData from './productData'
import UserProfileCard from 'components/dashboard/Common/userProfileCard/UserProfileCard';
import './index.css';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      activeIndex: 0, 
      activeIndex2: 0,
      stack: 0,
      speed:500
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.toPrev = this.toPrev.bind(this);

  }
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === ProductData.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? ProductData.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }


  onExiting2() {
    this.animating = true;
  }

  onExited2() {
    this.animating = false;
  }




  goToIndex2(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex2: newIndex });
  }

  toPrev = () => {
    if(this.slider){
      this.slider.slickGoTo(0, 1);
    }
   
  }

  componentDidMount() {
    this.checkLength()
  }

    checkLength = () => {    
      const { data } = this.props;
      const { stack } = this.state;
      let arr = data.length;
      if(arr%2 === 0){
        this.setState({ stack: (((arr/2) - 2 ) * 2) })            
      }
      else{
        this.setState({ stack: ((((arr/2) - 2 ) * 2) + 1) })       
      }
    }

  render() { 
    const { stack } = this.state;

    const options1 = {
      dots: true,
      arrows: false,
      infinite: false,
      speed: this.state.speed,
      slidesToShow: 4,
      slidesToScroll: 2,
      autoplay: true,
      autoplaySpeed: 3000,
      pauseOnHover: true,
      beforeChange:(currentSlide, nextSlide) => {
        if(nextSlide == stack-2){
          this.setState({speed:0})
        }
        else{
          this.setState({speed:500})
        }
      },
      afterChange: (slickCurrentSlide) => {
        if(slickCurrentSlide == stack){
          setTimeout(() => {
            this.toPrev()
          }, 0);          
        }
      },
      
      responsive: [
        {
          breakpoint: 950,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false
          }
        },
        {
          breakpoint: 560,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false
          }
        }
      ]
    };
    // const { activeIndex, activeIndex2 } = this.state;
    // const slides = ProductData.map((product, index) => {
    //   return (
    //     <CarouselItem
    //       key={index}
    //       onExiting={this.onExiting}
    //       onExited={this.onExited}>
    //       <div className="col-lg-3 col-sm-6 col-12">
    //         <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" />
    //       </div>
    //       {/*<Product key={index} showAddToCard={true} history={this.props.history} classValue="col-xl-2 col-md-2 col-sm-2 col-12"/>*/}
    //     </CarouselItem>
    //   );
    // });

    const {identifier} = this.props;

    return (

      <div>

        {this.props.data && this.props.data.length < 4 ?

          // <Col xs={12} sm={12} md={12} lg={3} xl={3}> 
          <Row>
            {
              this.props.data && this.props.data.map((product, index) => 
              <div className="slick-slide-item col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3" key={index}>

                <UserProfileCard 
                  styleName="pb-4" 
                  headerStyle="bg-gradient-primary" 
                  product={product} 
                  index={index} 
                  data={this.props.data} 
                  identifier={identifier}
                />

              </div>)
            }
          {/* // </Col> */}
          </Row>

          :

          <Slider ref={c => (this.slider = c)} className="slick-slider-cr" {...options1}>
            {
              this.props.data && this.props.data.map((product, index) => <div className="slick-slide-item " key={index}>

                <UserProfileCard 
                key = {index}
                styleName="pb-4" 
                headerStyle="bg-gradient-primary" 
                product={product} 
                index={index} 
                data={this.props.data} 
                identifier={identifier}
                />

              </div>)
            }
          </Slider> 
        }

      </div>

    );
  }
}

export default Dashboard;




{/* <Slider className="slick-slider-cr" {...options1}>
{
 this.props.data && this.props.data.map((product, index) => <div className="slick-slide-item " key={index}>

    <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} data={this.props.data} />

  </div>)
}
</Slider> */}
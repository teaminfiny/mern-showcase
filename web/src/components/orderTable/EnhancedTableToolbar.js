import React, { Component } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import CustomFilter from '../../../src/components/Filter/index';
import { Popover, PopoverBody, PopoverHeader } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import './index.css'
import { getOrderHistoryList, getOrderList } from 'actions/buyer';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap'

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
let date = new Date();


class EnhancedTableToolbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            show: false
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen
        });
    }

    handleResetFilter = (e, filter) => {
        e.preventDefault();
        // filter();
        this.setState({
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            filter: '',
            show:true
        });
        let data = {
            page: 1,
            perPage: 50,
            filter: this.state.filter,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        }
        this.props.getOrderHistoryList({ data, history: this.props.history })
        this.toggle()
    }

    componentDidMount(){
        this.setState({
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        });
        let data = {
            page: 1,
            perPage: 50,
            // month: moment().format("MMMM"),
            // year: moment().format("GGGG"),
        }
        this.props.getOrderHistoryList({ data, history: this.props.history })
    }


    onFilterChange = (e, key) => {
        this.setState({ [key]: e.target.value });
    }

    applyFilter = () => {
        this.setState({ show: true });
    }
    render() {

        // const classes = useToolbarStyles();
        // const { numSelected } = props;

        const { identifier } = this.props;

        return (

            <Toolbar
            // className={clsx(classes.root, {
            //   [classes.highlight]: numSelected > 0,
            // })}
            >

                {/* <Typography className={classes.title} variant="h6" id="tableTitle">
            Nutrition
          </Typography> */}

                {
                    identifier === "orderHistoryList" ?

                        <React.Fragment>
                            <Row className='w-100'>
                                <Col sm={12} md={11} lg={11} xs={12} xl={11}>
                                    {(this.state.show && this.props.show) ? <h3>{`Showing Order History for ${this.state.month} ${this.state.year}`}</h3>: <h3>&nbsp;</h3>}
                                </Col>
                                <Col sm={12} md={1} lg={1} xs={12} xl={1}>
                                    <div style={{marginTop: "-10px"}}>
                                        <IconButton aria-label="filter list">
                                            <FilterListIcon
                                                onClick={this.toggle}
                                                id="filter"
                                            />
                                        </IconButton>
                                    </div>
                                </Col>
                            </Row>


                            <Popover
                                trigger="legacy" className="SignInPopOver" placement="bottom" isOpen={this.state.popoverOpen} target="filter" toggle={this.toggle}
                            >
                                <h5  className="font-weight-bold" style={{color: '#000000de'}}>FILTERS</h5>

                                <CustomFilter
                                    onFilterChange={this.onFilterChange}
                                    handleApplyFilter={this.toggle}
                                    applyFilter={this.applyFilter}
                                    handleResetFilter={this.handleResetFilter}
                                    month={this.state.month}
                                    year={this.state.year}
                                    filterFor='buyerOrderHistory' />

                            </Popover>
                        </React.Fragment>

                        :
                        null

                }



            </Toolbar>
        );
    }
};


//   export default EnhancedTableToolbar;

const mapStateToProps = ({ buyer }) => {
    const { orderHistoryList } = buyer;
    return { orderHistoryList }
};


export default withRouter(connect(mapStateToProps, { getOrderHistoryList })(EnhancedTableToolbar));
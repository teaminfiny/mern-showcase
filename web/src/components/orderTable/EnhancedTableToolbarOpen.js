import React, { Component } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import Grow from '@material-ui/core/Grow';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import CustomFilter from '../../../src/components/Filter/index';
import { Popover, PopoverBody, PopoverHeader } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import './index.css'
import { getOrderList } from 'actions/buyer';
import { connect } from 'react-redux';

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
let date = new Date();


class EnhancedTableToolbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
            hide: true,
            show: false
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
            hide: true,
            searchText:''
        });
    }

    handleResetFilter = (e, filter) => {
        e.preventDefault();
        // filter();
        this.setState({
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
            show: true
        });
        let data = {
            page: 1,
            perPage: 50,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        }
        this.props.getOrderList({ data, history: this.props.history })
        this.toggle()
    }

    componentDidMount(){
        this.setState({
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
            hide: true,
            show: false
        });
        let data = {
            page: 1,
            perPage: 50,
            // month: moment().format("MMMM"),
            // year: moment().format("GGGG"),
        }
        this.props.getOrderList({ data, history: this.props.history })
    }


    onFilterChange = async(e, key) => {
        await this.setState({ [key]: e.target.value , searchText:''});
        if( [key] == 'month' || [key] == 'year' ){
            this.props.filterData({month:this.state.month,year:this.state.year})
        }
    }

    applyFilter = () => {
        this.toggle();
        this.setState({ show: true});
    }
    
    handleTextChange = (e) => {
        this.setState({searchText: e.target.value, month: '', year: ''})
    }

    clearSearch = () => {
        this.setState({searchText:'', month: moment().format("MMMM"), year: moment().format("GGGG"), hide:true})
        let data = {
          page: 1,
          perPage: 50,
          filter: '',
          searchText:'',
          month: moment().format("MMMM"),
          year: moment().format("GGGG")
        }
        this.props.getOrderList({ data, history: this.props.history })
    }

    applySearch = () => {
        let data = {
          page: 1,
          perPage: 50,
          filter: '',
          searchText:this.state.searchText,
          month: this.state.month,
          year: this.state.year
        }
        this.props.getOrderList({ data, history: this.props.history })
    }

    handleKeyPress = (e) => {
        if(e.keyCode === 13){
            this.applySearch()
        }
    }

    render() {

        // const classes = useToolbarStyles();
        // const { numSelected } = props;

        const { identifier } = this.props;

        return (

            <Toolbar
            // className={clsx(classes.root, {
            //   [classes.highlight]: numSelected > 0,
            // })}
            >

                {/* <Typography className={classes.title} variant="h6" id="tableTitle">
            Nutrition
          </Typography> */}

                {
                    identifier === "orderList" ?

                        <React.Fragment>
                            {(this.state.hide && this.props.show && this.state.show) ? <h3>Showing Orders for {this.state.month} {this.state.year}</h3>:
                            <div style={{marginTop: "-10px", marginRight: "0px", marginLeft:"0px", width:"80%"}}>
                                <TextField
                                className='search'
                                autoFocus={true}
                                fullWidth ={true}
                                hidden = {this.state.hide}
                                value={this.state.searchText}
                                onChange={this.handleTextChange}
                                placeholder='Search Order'
                                onKeyUp={this.handleKeyPress}
                                />
                            </div>
                            }
                            <Grow appear in={true} timeout={300}>
                            <div className='searchBox' style={{marginTop: "-10px", marginRight: "0px", marginLeft:"auto"}}>
                                <IconButton className='clear'>
                                    { this.state.hide ? <SearchIcon onClick={() => {this.setState({hide: false})}}/> : <ClearIcon onClick={this.clearSearch}/> }
                                </IconButton>
                            </div>
                            </Grow>
                            <div style={{marginTop: "-10px", marginRight: "0px", marginLeft:"0px"}}>
                                <IconButton aria-label="filter list">
                                    <FilterListIcon
                                        onClick={this.toggle}
                                        id="filter"
                                    />
                                </IconButton>
                            </div>


                            <Popover
                                trigger="legacy" className="SignInPopOver" placement="bottom" isOpen={this.state.popoverOpen} target="filter" toggle={this.toggle}
                            >
                                <h5  className="font-weight-bold" style={{color: '#000000de'}}>FILTERS</h5>

                                <CustomFilter
                                    onFilterChange={this.onFilterChange}
                                    handleResetFilter={this.handleResetFilter}
                                    applyFilter={this.applyFilter}
                                    month={this.state.month}
                                    year={this.state.year}
                                    filterFor='buyerOpenOrders' />

                            </Popover>
                        </React.Fragment>

                        :
                        null

                }



            </Toolbar>
        );
    }
};


//   export default EnhancedTableToolbar;

const mapStateToProps = ({ buyer }) => {
    const { getOrderList } = buyer;
    return { getOrderList }
};


export default withRouter(connect(mapStateToProps, { getOrderList })(EnhancedTableToolbar));
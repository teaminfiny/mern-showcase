import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TrackOrder from './trackOrder'
import './index.css'
import {Popover, PopoverBody, PopoverHeader} from 'reactstrap';

import { Link, withRouter, NavLink } from 'react-router-dom';
import { connect } from 'react-redux'
import { getUpdateOrder, getOrderList, getOrderHistoryList } from 'actions/buyer';

import moment from 'moment';
import axios from 'axios';
import AppConfig from 'constants/config';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';

const useStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#137cbd',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#106ba3',
    },
  },
});

function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

class CollapseComponent extends React.Component{
    constructor(props){
      super(props)

      this.toggle = this.toggle.bind(this);

      this.state = {
        openModal:false,
        popoverOpen: false,
        details:'',

        acceptModal: false,
        acceptDetails: '',

        cancelModal: false,
        cancelDetails: '',

        page: 1,
        perPage: 50,
        filter: "",
        month: moment().format("MMMM"),
        year: moment().format("GGGG"),
        selectedValue:'Reason not listed',
        products: [],
        api:1
      }
    }

    handleClose = ()=>{
      this.setState({openModal:false,details:''})
    }

    handleOpenModal = (details)=>{
      const {mainOrderData,value} = this.props;
      let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
      if(index>-1){
        this.setState({openModal:true,details:mainOrderData[index]})
      }
    }

    toggle() {
      this.setState({
        popoverOpen: !this.state.popoverOpen
      });
    }

//---------------------------------------------------------------------------------------//

    handleAcceptModal = (details) => {
      const {mainOrderData, value} = this.props;
      let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
      if(index>-1){
        this.setState({
          acceptModal:true,
          acceptDetails: mainOrderData[index]})
      }
    }

    handleAccept = (details) => {
      const {mainOrderData, value, filterObj} = this.props;
      const {page, perPage, filter, month, year} = this.state;

      let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
      if(index>-1){
       const data =  {
          orderId: mainOrderData[index].order_id,
          status: "accept",
          acceptFromBuyer:'accept',
          page, perPage, filter,  
          month:this.props.show === true ?  filterObj && filterObj.month ? filterObj.month : this.state.month:'',
          year:this.props.show === true ?  filterObj && filterObj.year ? Number(filterObj.year) : this.state.year :''         
        }
        this.props.getUpdateOrder({data})
        const {updateOrder} = this.props;
      }
      this.handleAcceptClose()
      
      // const {page, perPage, filter, month, year} = this.state;
      let data = {page, perPage, filter, month, year};
      // this.props.getOrderList({
      //   history:this.props.history,
      //   page:1,
      //   perPage:10,
      //   filter: "",
      //   month: moment().format("MMMM"),
      //   year: moment().format("GGGG")});
    }

    handleAcceptClose = () => {
      this.setState({acceptModal:false, acceptDetails:''})
    }

// ---------------------------------------------------------------------------------------//

    handleCancelModal = (details) => {
      const {mainOrderData, value} = this.props;
      let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
      if(index>-1){
        this.setState({
         cancelModal:true,
         cancelDetails: mainOrderData[index]})
      }
    }

    handleCancel = (details) => {
      const {mainOrderData, value, filterObj} = this.props;
      const {page, perPage, filter, month, year} = this.state;
      let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
      if(index>-1){
       const data =  {
          orderId: mainOrderData[index].order_id,
          status: "Cancelled",
          page, perPage, filter,
          month: this.props.show === true ? filterObj && filterObj.month ? filterObj.month : this.state.month:'',
          year: this.props.show === true ? filterObj && filterObj.year ? Number(filterObj.year) : this.state.year:'',
          reason: this.state.selectedValue,
        }
        this.props.getUpdateOrder({data})
        const {updateOrder} = this.props;
      }
      this.handleCancelClose()

      
      let data = {page, perPage, filter, month, year};
      // this.props.getOrderList({history:this.props.history, data});
      // this.props.getOrderHistoryList({history:this.props.history, data});
    }

    handleCancelClose = () => {
      this.setState({cancelModal:false, cancelDetails:''})
    }

//-----------------------------------------------------------------------------------------//

openInvoice = (details) => {
  const {mainOrderData, value} = this.props;
  let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);
  if(index>-1){
   const data =  {
      orderId: mainOrderData[index].order_id,
      status: "Cancelled"
    }
    this.props.getUpdateOrder({data})
    const {updateOrder} = this.props;
  }
}
  handleChange = (event) => {
    event.preventDefault();
    this.setState({ selectedValue: event.target.value })
  }
  
  componentDidUpdate = async(prevProps,prevState) => {
    if(this.props.collapse && this.state.api == 1){
      await axios({
        method: 'post',
        url: `${AppConfig.baseUrl}order/getBuyerOrderDetails`,
        data: { orderId: this.props.value.orderId },
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('buyer_token')
        }
    }).then((result) => {
        const { detail } = result.data
        this.setState({ products: detail.products, api:0 })
    }).catch(error=>error)
    }
  }

    render(){
      let { selectedValue } = this.state;
      const { collapse, value, orderedItemListFromParent, updateOrder,mainOrderData } = this.props;

      const { openModal, acceptModal, cancelModal, products } = this.state;

      // let totalCost = orderedItemListFromParent.map((val) => Number(val.paid)).reduce((acc, valu) => acc + valu, 0)
      

      let index = mainOrderData.findIndex((val)=>val.order_id===value.orderId);

      // let data = orderedItemListFromParent;
      let finalPerUnitCost = 0;
      let finalPerUnitCostAndGST = 0
      
      // = data.discount_per > 1 ?  (data.PTR - (data.PTR / 100 * data.discount_per)).toFixed(2) : (data.PTR).toFixed(2)
      

          return(
            <React.Fragment>
                {
                  collapse === true &&
                <React.Fragment >
                
                  <TableRow className='w-100 h-50'>
                    <TableCell colSpan={4} align="right" className='w-100 h-50' >
                    <div className='row ml-3'>
                    
                    <div className = 'col-12 d-flex justify-content-center'>
                      <List className='col-sm-12 col-xs-12 col-md-10 col-lg-10 col-xl-10'>
                      <h5 className="" style={{textAlign: "left", marginBottom: "-20px", marginLeft: "11px"}}>{moment(value.date).format("MMM Do YYYY, hh:mm A")}</h5>
                      {
                         products.map((data,index) => {
                          if(data.discount_per > 1 && data.discount_on_product && data.discount_on_product == data.inventory_id ){
                            // finalPerUnitCost = (((data.PTR * data.discount_on_product.purchase) / (data.discount_on_product.purchase + data.discount_on_product.bonus))
                            // - ((data.PTR * data.discount_on_product.purchase) / (data.discount_on_product.purchase + data.discount_on_product.bonus)/100)*data.discount_per).toFixed(2);
                            finalPerUnitCost = data.ePTR ? data.ePTR : data.PTR
                          }else if (data.discount_per > 1) {
                            //  finalPerUnitCost = (data.PTR - (data.PTR / 100 * data.discount_per)).toFixed(2)
                            finalPerUnitCost = data.ePTR ? data.ePTR : data.PTR
                           }
                           else {
                            //  finalPerUnitCost = (data.PTR).toFixed(2)
                            finalPerUnitCost = data.ePTR ? data.ePTR : data.PTR
                           }
                           finalPerUnitCostAndGST = Number(finalPerUnitCost) + Number(finalPerUnitCost)/100*Number(data.GST)
                        return(
                        <ListItem   key={index} alignItems={'flex-start'}> 
                        
                        {/* <ListItemText
                        className={'listMaxWidth'}
                        primary={data.productName}
                        secondary={ `Qty: ${data.quantity}`}
                        /> */}

                        <ListItemText
                          className={'listMaxWidth'}
                          primary={data.productName}
                            secondary={

                              (data.discount_name && data.discount_name == 'DifferentAndDiscount' ||
                               data.discount_name && data.discount_name == 'SameAndDiscount')?

                              <React.Fragment>
                                PTR: ₹{(data.PTR).toFixed(2)}
                                <br />
                                GST({data.GST}%): ₹{(finalPerUnitCost / 100 * Number(data.GST)).toFixed(2)}
                                <br/>
                                {data.discount_name == 'SameAndDiscount' ?
                                  <React.Fragment>
                                    Buy {data.discount_on_product.purchase} Get {data.discount_on_product.bonus} Free and {data.discount_per}% Off
                                    <br/>
                                    Effective Price: ₹
                                    {/* {(((data.PTR * data.discount_on_product.purchase) / (data.discount_on_product.purchase + data.discount_on_product.bonus))
                                    - ((data.PTR * data.discount_on_product.purchase) / (data.discount_on_product.purchase + data.discount_on_product.bonus)/100)*data.discount_per).toFixed(2)} */}
                                    { data.ePTR ? data.ePTR : data.PTR }
                                  </React.Fragment>
                                  :
                                  data.discount_name == 'DifferentAndDiscount'  ?
                                  <React.Fragment>
                                    Buy {data.discount_on_product.purchase} Get {data.discount_on_product.bonus} {data.discount_on_product.name} Free and {data.discount_per}% Off
                                    <br/>
                                    Effective Price: ₹{ data.ePTR ? data.ePTR : data.PTR }
                                  </React.Fragment>:null
                                }
                                <br /> 
                                Final Unit Price: ₹{(finalPerUnitCostAndGST).toFixed(2)}
                                <br />
                                {data.expiry_date ? <React.Fragment>Expiry: {moment(data.expiry_date).format('MM/YYYY')}
                                    <br/></React.Fragment> : null}
                                Qty: {data.quantity}
                                <br />                                
                              </React.Fragment>

                              :
                              data.discount_name && data.discount_name == 'Discount'  ?

                                <React.Fragment>
                                  PTR: ₹{(data.PTR).toFixed(2)}
                                  <br />
                                  Offer: {data.discount_per}% discount on PTR
                                  <br />
                                  Offer Price: ₹{(data.PTR - (data.PTR / 100 * data.discount_per)).toFixed(2)}
                                  <br />
                                  GST({data.GST}%): ₹{(finalPerUnitCost / 100 * Number(data.GST)).toFixed(2)}
                                  <br/>
                                  Final Unit Price: ₹{(finalPerUnitCostAndGST).toFixed(2)}
                                  <br />
                                  {data.expiry_date ? <React.Fragment>Expiry: {moment(data.expiry_date).format('MM/YYYY')}
                                    <br/></React.Fragment> : null}
                                  Qty: {data.quantity}
                                  <br />                                
                                </React.Fragment>

                                :

                                data.discount_on_product ?

                                  <React.Fragment>                                    
                                    PTR: ₹{(data.PTR).toFixed(2)}
                                    <br />
                                    GST({data.GST}%): ₹{(finalPerUnitCost / 100 * Number(data.GST)).toFixed(2)}
                                    <br />    

                                    {data.discount_name == 'Same'  ?
                                      <React.Fragment>
                                        Buy {data.discount_on_product.purchase} Get {data.discount_on_product.bonus} Free
                                        <br/>
                                        Effective Price: ₹
                                        {(data.ePTR ? data.ePTR : data.PTR ).toFixed(2)}
                                      </React.Fragment>
                                      :
                                      <React.Fragment>
                                        Buy {data.discount_on_product.purchase} Get {data.discount_on_product.bonus} {data.discount_on_product.name + " Free"}
                                      </React.Fragment>
                                    }
                                
                                    <br />  
                                    Final Unit Price: ₹:{(finalPerUnitCostAndGST).toFixed(2)}
                                    <br/>   
                                    {data.expiry_date ? <React.Fragment>Expiry: {moment(data.expiry_date).format('MM/YYYY')}
                                    <br/></React.Fragment> : null}     
                                    Qty: {data.quantity}
                                    <br />
                                  </React.Fragment>

                                  :

                                  <React.Fragment>
                                    PTR: ₹{(data.PTR).toFixed(2)}
                                    <br />
                                    GST({data.GST}%): ₹{(finalPerUnitCost / 100 * Number(data.GST)).toFixed(2)}
                                    <br/>
                                    Final Unit Price: ₹:{(finalPerUnitCostAndGST).toFixed(2)}
                                    <br/>
                                    {data.expiry_date ? <React.Fragment>Expiry: {moment(data.expiry_date).format('MM/YYYY')}
                                    <br/></React.Fragment> : null}
                                    Qty: {data.quantity}
                                    <br />                                   
                                  </React.Fragment>
                            } 
                        />


                        <ListItemText
                        className={'finalPrice pull-right'}

                        primary = 
                        {
                          ( data.discount_name && data.discount_name == 'SameAndDiscount' || data.discount_name && data.discount_name == 'Same') ?
                          // data.discount_per  &&  data.discount_on_product && data.discount_on_product.inventory_id === data.inventory_id ?
                          <React.Fragment>
                            ₹{(finalPerUnitCostAndGST * (((data.quantity/data.discount_on_product.purchase )  * data.discount_on_product.bonus) + Number(data.quantity))).toFixed(2)}
                          </React.Fragment>
                          :
                          <React.Fragment>
                            ₹{(data.quantity * finalPerUnitCostAndGST).toFixed(2)}
                          </React.Fragment>
  //                         <React.Fragment>
  //  ₹{((finalPerUnitCostAndGST * data.quantity) + (finalPerUnitCostAndGST * data.quantity) / 100 * Number(data.GST)).toFixed(2)}
  //                         </React.Fragment>
  //                         :
  //                         <React.Fragment>
  //                           ₹{(Number(finalPerUnitCostAndGST * Number(data.quantity)) + (Number(finalPerUnitCostAndGST / 100 * Number(data.GST)) * Number(data.quantity))).toFixed(2)}
  //                         </React.Fragment>
                          // (finalPerUnitCostAndGST * data.quantity).toFixed(2)
                        }

                        // secondary = 
                        // {  + (Number(data.PTR/100 * Number(data.GST)) * Number(data.quantity))
                        //   data.discount_per > 1 ?
                        //     <strike>{`₹ ${(data.PTR).toFixed(2)}`}</strike>
                        //     :
                        //     null
                        // }
                        />
                        </ListItem>
                        )
                        }
                        )
                      }
                        
                      </List>
                      </div>
                      </div>
                      <div className='row ml-3'>
                        <div className = 'col-12 d-flex justify-content-center'>          
                          <List className='col-sm-12 col-xs-12 col-md-10 col-lg-10 col-xl-10'>


                            {/* <React.Fragment>
                              <ListItem alignItems={'flex-start'} >
                                <ListItemText
                                  className={'listMaxWidth'}
                                  secondary={'GST'}
                                />

                                <ListItemText
                                  secondary={}
                                />
                              </ListItem>
                            </React.Fragment> */}

{/* --------------------------------Delivery charge 50 ------------------------------*/}

{Number(value.delivery_charges) === 50 ?
                              <React.Fragment>
                                <ListItem alignItems={'flex-start'} >
                                  <ListItemText
                                    className={'listMaxWidth'}
                                    secondary={'Delivery Charges:'}
                                  />

                                  <ListItemText
                                    secondary={'₹50'}
                                  />
                                </ListItem>
                              </React.Fragment>
                            :
                            null
                            }


                          <Divider />

                            <ListItem  alignItems={'flex-start'} >
                              <ListItemText
                              className={'listMaxWidth'}
                              primary={'Total'}
                              />
                              <ListItemText
                              primary={
                                Number(value.delivery_charges) === 50 ?
                                `₹${(Number(value.amount.props.children[1]) + 50).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits:2}) }`
                                :
                                `₹${(Number(value.amount.props.children[1])).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits:2})}`
                              }
                              />
                            </ListItem>
                          </List>
                        </div>
                      </div>
                      <Divider />
                      
                      {/* col-sm-12 col-xs-12 col-md-7 col-lg-6 col-xl-6 mt-3 */}
                      <div className='row mt-3 mr-2 flex-row-reverse'>
                        <div className=''>
                          <div className='list-action mx-auto'>
                            {mainOrderData[index].requested === "Requested" ?
                              <div>
                                <Button className="jr-btn text-uppercase text-primary" onClick={() => this.handleAcceptModal(value)}>
                                  Accept
                                </Button>
                                <Button className="jr-btn text-uppercase text-danger" onClick={() => this.handleCancelModal(value)}>
                                  Cancel
                                </Button>
                                <Button className="jr-btn text-uppercase text-success" onClick={() => this.handleOpenModal(value)}>
                                  Track
                                </Button>
                              </div>

                              :  

                              mainOrderData[index].order_status[mainOrderData[index].order_status.length - 1].status === "New" ? 
                              <div>
                                <Button className="jr-btn text-uppercase text-danger" onClick={() => this.handleCancelModal(value)}>
                                  Cancel
                                </Button>

                                <Button className="jr-btn text-uppercase text-success" onClick={() => this.handleOpenModal(value)}>
                                  Track
                                </Button>
                              </div>

                              :

                              // mainOrderData[index].order_status[mainOrderData[index].order_status.length - 1].status === "Processed" ? 
                              (mainOrderData[index].requested === "Delivered" || mainOrderData[index].requested === "Processed") ?
                              <div>
                                
                              <a href={`${AppConfig.baseUrl}invoice/${mainOrderData[index].uploadedInvoice}`} target='_blank'>
                                <Button className="jr-btn text-uppercase text-info">
                                  View Invoice
                                </Button>
                              </a>

                              <Button className="jr-btn text-uppercase text-success" onClick={() => this.handleOpenModal(value)}>
                                Track
                              </Button>
                              </div>
                              
                              :

                              <Button className="jr-btn text-uppercase text-success" onClick={() => this.handleOpenModal(value)}>
                                Track
                              </Button>
                            }

                          </div>
                        </div>
                      </div>       
                     
                    </TableCell>
                  </TableRow>
                </React.Fragment>
                }

{/* -------------------------- TrackOrder --------------------------------------- */}

              <Dialog open = {openModal} onClose = {this.handleClose} fullWidth = {true}>
                <DialogTitle id="alert-dialog-title">{"Order tracking details"}</DialogTitle>
                <DialogContent>
                  <TrackOrder orderDetails={this.state.details}/>
                </DialogContent>

                <DialogActions>
                  <Button onClick={this.handleClose} color="secondary" autoFocus> Close </Button>
                </DialogActions>

              </Dialog>

{/* -------------------------- Accept --------------------------------------- */}

              <Dialog open = {acceptModal} onClose = {this.handleAcceptClose} fullWidth = {true}>

                <DialogTitle id="alert-dialog-title">Accept Changes</DialogTitle>
                
                <DialogContent>
                  Are you sure you want to accept changes?
                </DialogContent>

                <DialogActions>
                  <Button onClick={this.handleAcceptClose} color="secondary" autoFocus> Close </Button>
                  <Button className="jr-btn text-uppercase text-primary" onClick={() => this.handleAccept(value)}> Accept </Button>                 
                </DialogActions>

              </Dialog>

{/* -------------------------- Cancel --------------------------------------- */}

              <Dialog open = {cancelModal} onClose = {this.handleCancelClose} fullWidth = {true}>

                <DialogTitle>
                  Cancel Order
              <DialogContentText className="mt-1 mb-0">
                    Are you sure you want to cancel the order ?
            </DialogContentText>
                </DialogTitle>
                <DialogContent>
            <FormControl component="fieldset">
              <RadioGroup aria-label="reason" name="customized-radios" value={selectedValue} onChange={(e) => this.handleChange(e)}>
              <FormControlLabel value="Late delivery" control={<StyledRadio />} label="Late delivery" />
                <FormControlLabel value="Better price somewhere" control={<StyledRadio />} label="Better price somewhere" />
                <FormControlLabel value="Order by mistake" control={<StyledRadio />} label="Order by mistake" />
                <FormControlLabel value="Reason not listed" control={<StyledRadio />} label="Reason not listed" />
              </RadioGroup>
            </FormControl>
          </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleCancelClose} color="secondary" autoFocus> Close </Button>
                  <Button className="jr-btn text-uppercase text-primary" onClick={() => this.handleCancel(value)}> Cancel Order </Button>                  
                </DialogActions>

              </Dialog>
            </React.Fragment>
            
        )
    }
}
 
const mapStateToProps = ({ buyer }) => {
  const { updateOrder } = buyer;
  return {  updateOrder }
};

export default withRouter(connect(mapStateToProps, { getUpdateOrder, getOrderList, getOrderHistoryList }) (CollapseComponent));


import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import moment from 'moment';
import TableComponent from './TableComponent';
import ComponentTable from './ComponentTable';
import CollapseComponent from './CollapseComponent';

import { lighten, withStyles, makeStyles } from '@material-ui/core/styles';
import helper from 'constants/helperFunction';
import EnhancedTableHead from './EnhancedTableHead'
import EnhancedTableToolbar from './EnhancedTableToolbar'
import EnhancedTableToolbarOpen from './EnhancedTableToolbarOpen'
import CircularProgress from '@material-ui/core/CircularProgress';
import { NavLink,withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let date = new Date();
// const rows = [
//     createData(
//         'ODI12584',
//         <a className='text-reset text-primary' href='/view-seller'>Avanish</a>,
//         <div key={'recent'} className={` badge text-uppercase ${statusStyle('Placed')}`}>Placed</div>,
//         <span>&#8377; 1380</span>,
//         require('assets/productImage/user.jpg'),
//         'Crocin X 20',
//         67
//     ),

//     createData('ODI84646',<a className='text-reset text-primary' href='/view-seller'>Sachin</a>, 
//     <div key={'recent'} className={` badge text-uppercase ${statusStyle('Ready for dispatch')}`}>Ready for dispatch</div>, 
//     <span>&#8377; 51</span>,
//     require('assets/productImage/user.jpg'), "D'Cold Total X 60", 67,),

//     createData('ODI79797', <a className='text-reset text-primary' href='/view-seller'>Abhidnya</a>,
//     <div key={'recent'} className={` badge text-uppercase ${statusStyle('Processed')}`}>Processed</div>, 
//     <span>&#8377; 2400</span>,
//     require('assets/productImage/user.jpg'),'Colgate X 60', 67,),

//     createData('ODI314564', <a className='text-reset text-primary' href='/view-seller'>Prafful</a>,
//     <div key={'recent'} className={` badge text-uppercase ${statusStyle('Processed')}`}>Processed</div>, 
//     <span>&#8377; 1100</span>,
//     require('assets/productImage/user.jpg'),'Cough Syrup X 15', 67,),
//   ]


// const {orderFromParent} = this.props;

// const rows = []

// orderFromParent && orderFromParent.details && orderFromParent.details.length > 0 && orderFromParent.details[0].data.map((dataOne, index) => {
//     rows.push([
//         createData(dataOne.order_id, <a className='text-reset text-primary' href='/view-seller'>{dataOne.seller_id.first_name}</a>,
//             <div key={'recent'} className={` badge text-uppercase ${statusStyle('Processed')}`}>{dataOne.order_status[0].status}</div>,
//             <span>&#8377; {dataOne.delivery_charges}</span>,
//             require('assets/productImage/user.jpg'), 'Cough Syrup X 15', 67),
//     ])
// })


const useStyles = theme => ({
  root: {
    width: '100%',
    marginTop: 0,
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 400,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  list: {
    maxWidth: 239
  },
  fontSize: {
    fontSize: '1rem'
  }
});



const useToolbarStyles = theme => ({
  root: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '1 1 100%',
  },
});

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}


class Order extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: [],
      page: 0,
      dense: false,
      rowsPerPage: 50,
      collapse: false, 
      month: moment().format("MMMM"),
      year: moment().format("GGGG"),
      filterObj: ''
    }
  }


  handleRequestSort = (event, property) => {
    const isDesc = this.state.orderBy === property && this.state.order === 'desc';
    this.setState({
      order: isDesc ? 'asc' : 'desc',
      OrderBy: property
    });
  };


  OnCollapseProject = () => {
    this.setState({
      collapse: !this.state.collapse
    })
  }

  handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = this.rows.map(n => n.name);
      this.setState({ selected: newSelecteds })
      return;
    }
    this.setState({ selected: [] })
  };

  handleClick = (event, name) => {
    const selectedIndex = this.state.selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(this.state.selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(this.state.selected.slice(1));
    } else if (selectedIndex === this.state.selected.length - 1) {
      newSelected = newSelected.concat(this.state.selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        this.state.selected.slice(0, selectedIndex),
        this.state.selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected })

  };

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage })
  };

  handleChangeRowsPerPage = event => {
    this.setState({
      rowsPerPage: parseInt(event.target.value, 10),
      page: 0
    })
  };

  handleChangeDense = event => {
    this.setState({
      dense: event.target.checked
    })
  };
  applyFilter = () => {
    // 
  }


  handleResetFilter = (e, filter) => {
    e.preventDefault();
    filter();
    this.setState({
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
      filter: ''
    });
    let data = {
      page: 1,
      perPage: this.state.perPage,
      filter: this.state.filter,
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
    }
    this.props.getOrderHistoryList({ data, history: this.props.history })
  }


  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  isSelected = name => this.state.selected.indexOf(name) !== -1;

  filterData = (data) => {
    this.setState({ filterObj: data });
  }

  render() {
    const { classes, identifier } = this.props;


    function createData(orderId, seller, status, amount, product, description, value) {
      return { orderId, seller, status, amount, product, description, value };
    }

    const statusStyle = (status) => {
      return status.includes("Processed") ? "text-white bg-primary" : status.includes("pending") ? 'text-white bg-dark' : status.includes("Ready For Dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-yellow" : status.includes("Cancelled") ? "text-white bg-grey" : status.includes("Delivered") ? "text-white bg-green" : status.includes("Requested") ? "text-white bg-info" : status.includes("New") ? "text-white bg-danger" : "text-white bg-info";
    }

    const { orderFromParent } = this.props;

    //     createData(
    //         'ODI12584',
    //         <a className='text-reset text-primary' href='/view-seller'>Avanish</a>,
    //         <div key={'recent'} className={` badge text-uppercase ${statusStyle('Placed')}`}>Placed</div>,
    //         <span>&#8377; 1380</span>,
    //         require('assets/productImage/user.jpg'),
    //         'Crocin X 20',
    //         67
    //     ),

    let rows = []

    orderFromParent && orderFromParent.detail && orderFromParent.detail[0] && orderFromParent.detail[0] && orderFromParent.detail[0].data.length > 0 && orderFromParent.detail[0].data.map((dataOne, index) => {

      rows.push({
        orderId: dataOne.order_id,
        orderType:<div key={'recent1'} className={'badge text-uppercase text-white'} style={{backgroundColor:`${helper.getProdColor(dataOne.orderType)}`}}>{dataOne.orderType ? dataOne.orderType : 'N/A'}</div> , 
        seller: <NavLink className="text-reset text-primary text-capitalize" to={`/view-seller/${dataOne.seller_id && dataOne.seller_id._id}`}>
          {dataOne.seller_id ? dataOne.seller_id.company_name : ''}
        </NavLink>,
        status: 
        <div key={'recent'} className={` badge text-uppercase ${statusStyle(dataOne.requested !== "Requested" ? dataOne.order_status[dataOne.order_status.length - 1].status : 'Requested')}`}>

          {dataOne.requested === "Requested" ?
            dataOne.requested
            :
            dataOne.order_status[dataOne.order_status.length - 1].status === 'New' ? 'Placed' : dataOne.order_status[dataOne.order_status.length - 1].status}
        </div>,
        amount: <span>&#8377;{(Number(dataOne.total_amount)).toFixed(2)}</span>,
        delivery_charges: dataOne.delivery_charges,
        product: 'assets/productImage/user.jpg',
        description: dataOne.paymentType,
        value: dataOne.breadth,
        date: dataOne.createdAt,
        tableAmout: <span>&#8377;{(Number(dataOne.delivery_charges)).toFixed(2) == 50 ? (50 + Number(dataOne.total_amount)).toFixed(2) : (Number(dataOne.total_amount)).toFixed(2)}</span>
      })
    })

    let orderData = orderFromParent ? orderFromParent.detail ? orderFromParent.detail.length > 0 ? orderFromParent.detail[0].data : [] : [] : []
    const isSelected = name => this.state.selected.indexOf(name) !== -1;

    const emptyRows = this.state.rowsPerPage - Math.min(this.state.rowsPerPage, rows.length - this.state.page * this.state.rowsPerPage);

    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          {identifier === "orderHistoryList" ?
            <EnhancedTableToolbar identifier={identifier} show={this.props.show}/>
            :
            identifier === "orderList" ?
            <EnhancedTableToolbarOpen identifier={identifier} filterData={this.filterData} show={this.props.show}/>
            :
            null
          }
          {
            this.props.loading ?   
              <div className="loader-view">
                <CircularProgress />
              </div>
            :
          <div className={classes.tableWrapper}>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={this.state.dense ? 'small' : 'medium'}
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={this.state.selected.length}
                order={this.state.order}
                orderBy={this.state.orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={rows.length}
              />

              <TableBody>
                {rows.length < 1 ? <TableRow><TableCell colSpan={6} style={{textAlign:'center'}}>{identifier === "orderList" ? `You don't have open orders !` : identifier === "orderHistoryList" ? `You don't have orders !` : 'Sorry no records found'}</TableCell></TableRow> :
                stableSort(rows, getSorting(this.state.order, this.state.orderBy))
                  .slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(row.name);
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (

                      <TableComponent
                        row={row}
                        labelId={labelId}
                        value={this.props.value}
                        itemListFromParent={orderData} 
                        show={this.props.show}
                        filterObj={this.state.filterObj}/>

                    )
                  })}

              </TableBody>
            </Table>
          </div>
          }
          <TablePagination
            rowsPerPageOptions={[]}
            component="div"
            count={rows.length}
            rowsPerPage={this.state.rowsPerPage}
            page={this.state.page}
            backIconButtonProps={{
              'aria-label': 'previous page',
            }}
            nextIconButtonProps={{
              'aria-label': 'next page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>

      </div>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { loading } = buyer;
  return { loading }
};

export default withRouter(connect(mapStateToProps)(withStyles(useStyles)(Order)))
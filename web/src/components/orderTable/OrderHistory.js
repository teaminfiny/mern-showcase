import React, { Component } from "react";
import MUIDataTable from "../../components/DataTable";
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import FilterList from '@material-ui/icons/FilterList';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import buyerImage from 'app/sellers/buyerImage';
import moment from 'moment';
import customHead from 'constants/customHead'
import CustomFilter from '../../../src/components/Filter/index';
import { withRouter } from 'react-router-dom'
import { getOrderHistoryList, getOrderList } from 'actions/buyer';
import { connect } from 'react-redux';

const customStyles = {
  BusinessAnalystRow: {
    '& td': { backgroundColor: "#00000012" }
  }
};

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

let date = new Date();

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
        searchedText: '',

        page:1,
        perPage:10,
        filter: "",
        month: moment().format("MMMM"),
        year: moment().format("GGGG")
    };

  }

  statusStyle = (status) => {
    return status.includes("Processed") ? "text-white bg-primary" : status.includes("Ready for dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-danger" : status.includes("Cancelled") ? "text-white bg-grey" : status.includes("Delivered") ? "text-white bg-green" : "text-white bg-danger";
  }

  button = () => (
    <ButtonGroup color="primary" aria-label="outlined primary button group">
      <Button variant="outlined">
        Invoice
      </Button>
    </ButtonGroup>
  )

  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  componentDidMount(){ 

    const {page, perPage, filter, month, year} = this.state;

    let data = {page, perPage, filter, month, year};

    this.props.getOrderHistoryList({history:this.props.history, data});

  }

  changePage = (page) => {
    let pages = page + 1
    let data = {
      page: pages,
      perPage: this.state.perPage,
      filter: '',
      searchText: '',
      month: this.state.month,
      year: this.state.year
    }
    this.props.getOrderHistoryList({ data, history: this.props.history })
    this.setState({ page })
  };

  changeRowsPerPage = (perPage) => {
    let data = {
      page: 1,
      perPage: perPage,
      filter: '',
      searchText: '',
      month: this.state.month,
      year: this.state.year
    }
    this.props.getOrderHistoryList({ data, history: this.props.history })
    this.setState({ page: 0, perPage })
  }


  handleSearch = (searchText) => {
    let data = { searchText: searchText, page: this.state.page > 0 ? this.state.page : 1, perPage: this.state.perPage, month : '',year :'' }
    this.props.getOrderHistoryList({ data, history: this.props.history })
    this.setState({ searchedText: searchText })
  };

  handleResetFilter = (e, filter) => {
    e.preventDefault();
    filter();
    this.setState({
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
      filter: ''
    });
    let data = {
      page: 1,
      perPage: this.state.perPage,
      filter: this.state.filter,
      month: monthNames[date.getMonth()],
      year: date.getFullYear(),
    }
    this.props.getOrderHistoryList({ data, history: this.props.history })
  }

  render() {
    let { orderHistoryList } = this.props;

    const {orderFromParent} = this.props;
    
    const options = {
      filterType: 'dropdown',
      viewColumns: false,
      selectableRows: false,
      // resizableColumns:true,
      rowsPerPage: this.state.perPage,
      page: this.state.page - 1,
      fixedHeader: false,
      print: false,
      download: false,
      filter: true,
      sort: false,
      selectableRows: false,
      count: this.props.getOrderHistoryList.length > 0 ? this.props.getOrderHistoryList[0].metadata.length > 0 ? this.props.getOrderHistoryList[0].metadata[0].total : 0 : 0,
      serverSide: true,
      rowsPerPage: this.state.perPage,
      server: true,
      selectableRowsOnClick: false,
      selectableRows: 'none',
      fixedHeader: false,
      search : true,
      searchText: this.state.searchedText,
      textLabels: {
        filter: {
          all: "",
          title: "FILTERS",
        },
      },
      onTableChange: (action, tableState) => {
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
        }
      },
    }

    const columns = [

      {
        name: "orderId",
        label: "Order ID",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "amount",
        label: "Amount",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "date",
        label: "Date",
        options: {
          filter: true,
          filterType: 'custom',
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          },
          customFilterListRender: v => {
            return false;
          },
          filterOptions: {
            names: [],
            logic() {
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => {
              return (<CustomFilter 
                onFilterChange={this.onFilterChange} 
                handleApplyFilter={this.handleApplyFilter} 
                applyFilter={applyFilter} 
                handleResetFilter={this.handleResetFilter}
                month={this.state.month}
                year = {this.state.year} 
                filterFor='buyerOrderHistory'/>)
            },
          },
        }
      },
    //   {
    //     name: "action",
    //     label: "Action",
    //     options: {
    //       filter: false,
    //       sort: false,
    //       customHeadRender: (o, updateDirection) => {
    //         return customHead(o, updateDirection);
    //       }
    //     }
    //   }
    ];

    let data = []
    orderFromParent && orderFromParent.detail && orderFromParent.detail.length > 0 && orderFromParent.detail[0].data.map((data1, index) => {
      data.push([data1.order_id,
      <div><p className={'m-0'}>&#8377;{data1.total_amount}</p></div>,
      <div key={'recent'} className={` badge text-uppercase ${this.statusStyle(data1.order_status[data1.order_status.length - 1].status)}`}>{data1.order_status[data1.order_status.length - 1].status}</div>,
      moment(data1.order_status[data1.order_status.length - 1].date).format(' D MMM, YYYY h:mm a')
      ])
    })

    return (
      <div style={{ padding: '0px 1px 0 1px', boxShadow: '0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)' }}>
        <MUIDataTable
          title={<h3>{`Showing orders for ${this.state.month} ${this.state.year}`}</h3>}
          data={data}
          columns={columns}
          options={options}
          style={{ borderRadius: '0px !important' }}
        />
      </div>
    );
  }
}

const mapStateToProps = ({buyer}) => {
    const { orderHistoryList } = buyer;
    return { orderHistoryList}
  };
  

export default connect(mapStateToProps, { getOrderHistoryList })(withStyles(customStyles)(OrderHistory));

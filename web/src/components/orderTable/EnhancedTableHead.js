import React from 'react'
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';


const headCells = [
    { id: 'orderId', numeric: false, disablePadding: true, label: 'OrderID' },
    { id: 'orderType', numeric: false, disablePadding: true, label: 'OrderType' },
    { id: 'seller', numeric: false, disablePadding: false, label: 'Seller' },
    { id: 'amount', numeric: false, disablePadding: false, label: 'Amount' },
    { id: 'payment', numeric: false, disablePadding: false, label: 'Payment' },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
    
  ];

function EnhancedTableHead(props) {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = property => event => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          
          {headCells.map(headCell => (
            <TableCell
              key={headCell.id}
              align={'left'}
              padding={'default'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={order}
                onClick={createSortHandler(headCell.id)}
                className={classes.fontSize}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }


export default EnhancedTableHead;
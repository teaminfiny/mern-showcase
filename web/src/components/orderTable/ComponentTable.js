import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TrackOrder from './trackOrder'
import './index.css'

class ComponentTable extends React.Component{
    constructor(props){
        super(props)
        this.state={
            collapse:false
        }
    }
    OnCollapseProject=()=>{
        this.setState({collapse:!this.state.collapse})
        this.props.handleCollapse()
    }
    
    render(){
        const {collapse} = this.state;
        
        return(
            <React.Fragment>
                
                    {collapse ?
                    <span className='mr-1 cursor-pointer' onClick={() => this.OnCollapseProject()}>
                    <i className="zmdi zmdi-chevron-down"></i>
                        
                    </span>
                        :
                        <span className='mr-1 cursor-pointer' onClick={() => this.OnCollapseProject()}>
                    
                        <i className="zmdi zmdi-chevron-right"></i>
                        </span>
                    }
               
            </React.Fragment>
        )
    }
    
}
 
export default ComponentTable;
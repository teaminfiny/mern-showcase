import React, { Component } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import './index.css'
import ComponentTable from './ComponentTable';
import CollapseComponent from './CollapseComponent';

class TableComponent extends Component{

    constructor(props) {
        super(props)
        this.state = {
            collapse: false
        }
    }

    render() {

        const OnCollapseProject = () => {
            // setcollapse(!collapse)
            this.setState({collapse: !this.state.collapse})
        }

        
        const {itemListFromParent, row} = this.props;
        
        
        let index = itemListFromParent.findIndex((val)=>val.order_id===row.orderId)
        
        let products = index>-1? itemListFromParent[index].products:[]

        return (
            <React.Fragment>
                
                <TableRow
                    hover
                    className='cursor-pointer'
                    key={this.props.row.name}
                    align={'left'}
                >

                    <TableCell onClick={OnCollapseProject} align={'left'} component="th" id={this.props.labelId} scope="row" >
                        <div className="d-contents">
                            <ComponentTable value={this.props.row.status} handleCollapse={OnCollapseProject} />
                            {this.props.row.orderId}
                        </div>

                    </TableCell>
                    <TableCell align="left">{this.props.row.orderType}</TableCell>
                    <TableCell align="left">{this.props.row.seller}</TableCell>
                    
                    <TableCell onClick={OnCollapseProject} align="left">
                        {this.props.row.tableAmout}
                    </TableCell>
                    <TableCell onClick={OnCollapseProject} align="left">{this.props.row.description}</TableCell>
                    <TableCell onClick={OnCollapseProject} align="left">{this.props.row.status}</TableCell>


                </TableRow>

                <CollapseComponent 
                collapse={this.state.collapse} 
                value={this.props.row} 
                stateValue = {this.props.value}
                show={this.props.show}
                orderedItemListFromParent={products}
                mainOrderData = {itemListFromParent}
                filterObj={this.props.filterObj}
                />
            </React.Fragment>
        )
    }

}

export default TableComponent;
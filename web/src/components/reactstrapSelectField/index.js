import React, { Component } from 'react'
import { Form, FormGroup, Label, Input, FormFeedback } from 'reactstrap';

const renderSelectField = ({ input: { onChange, value }, props, label, defaultValue, meta: { touched, error, warning }, children, ...custom }) => {
    console.log("input..........",value,props, defaultValue)
    return <FormGroup>
        <Label for={label}>{label}</Label>
        <Input onChange={onChange}
            value={defaultValue} {...props} type="select" invalid={(touched && error) || (warning)}  {...custom}>
            {children}
        </Input>
        <FormFeedback>{error}</FormFeedback>
    </FormGroup>
}

export default renderSelectField
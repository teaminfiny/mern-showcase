import React from 'react';
import Avatar from '@material-ui/core/Avatar'
import { connect } from 'react-redux'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { userSignOut } from 'actions/Auth';
import IntlMessages from 'util/IntlMessages';
import { NavLink, withRouter } from 'react-router-dom';
import helperFunction from '../../constants/helperFunction';

class UserInfo extends React.Component {

  state = {
    anchorEl: null,
    open: false,
  };

  handleClick = event => {
    this.setState({ open: true, anchorEl: event.currentTarget });
  };

  handleRequestClose = () => {
    this.setState({ open: false });
  };

  render() {
    let { userDetails } = this.props;
    return (
      <React.Fragment>
        {
          userDetails !== undefined ? <div className="user-profile d-flex flex-row align-items-center">
            <Avatar
              alt='Andrew'
              // src={require('assets/images/medical-logo.png')}
              style={{ background: helperFunction.getGradient(userDetails.backGroundColor) }}
              className="user-avatar "
            >
              {
                userDetails !== undefined ? helperFunction.getNameInitials(`${userDetails.first_name ? userDetails.first_name : ''} ${userDetails.last_name ? userDetails.last_name : ''}`) : ''
              }
            </Avatar>
            <div className="user-detail">
            <h4 className="user-name" onClick={this.handleClick}>{userDetails.first_name} {helperFunction.getNameInitials(userDetails.last_name)} <i
                className="zmdi zmdi-caret-down zmdi-hc-fw align-middle" />
              </h4>
            </div>
            <Menu className="user-info"
              id="simple-menu"
              anchorEl={this.state.anchorEl}
              open={this.state.open}
              onClose={this.handleRequestClose}
              PaperProps={{
                style: {
                  minWidth: 120,
                  paddingTop: 0,
                  paddingBottom: 0
                }
              }}
            >
              <MenuItem onClick={this.handleRequestClose}>
                <NavLink to="/seller/edit-Profile" className={'text-black NavLink'}>
                  <i className="zmdi zmdi-account zmdi-hc-fw mr-2" />
                  <IntlMessages id="Edit Profile" />
                </NavLink>
              </MenuItem>
              <MenuItem onClick={this.handleRequestClose}>
                <NavLink to="/seller/change-Password" className={'text-black NavLink'}>
                  <i className="zmdi zmdi-settings zmdi-hc-fw mr-2" />
                  <IntlMessages id="Change Password" />
                </NavLink>
              </MenuItem>
              {/* <MenuItem onClick={this.handleRequestClose}>
            <NavLink to="/seller/product-Request" className={'text-black NavLink'}>
              <i className="zmdi zmdi-swap zmdi-hc-fw mr-2" />
              <IntlMessages id="Product Request" />
            </NavLink>
          </MenuItem> */}
              <MenuItem onClick={() => {
                this.handleRequestClose();
                localStorage.clear();
                this.props.userSignOut({history:this.props.history})
              //   setTimeout(function(){
              //     window.location.reload();
              // }, 500) 
              this.props.history.push('/signin')
              }}>
                <i className="zmdi zmdi-sign-in zmdi-hc-fw mr-2" />

                <IntlMessages id="popup.logout" />
              </MenuItem>
            </Menu>
          </div> : ''
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ settings, seller }) => {
  const { locale } = settings;
  const { userDetails } = seller;
  return { locale, userDetails }
};
export default connect(mapStateToProps, { userSignOut })(withRouter(UserInfo));



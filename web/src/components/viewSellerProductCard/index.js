import React, { Component } from 'react';
import Fab from '@material-ui/core/Fab';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { Carousel, CarouselItem } from 'reactstrap';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import { NavLink, withRouter } from 'react-router-dom';
import { Popover, PopoverBody, PopoverHeader } from 'reactstrap';
import CartPopOver from '../../components/dashboard/Common/userProfileCard/CartPopOver'
import Tooltip from '@material-ui/core/Tooltip';
import helpertFn from 'constants/helperFunction';
import './index.css'
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';

class SellerProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0, popoverOpen: false };
    this.toggle = this.toggle.bind(this);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }
  onExited() {
    this.animating = false;
  }
  next(size) {
    if (this.animating) return;
    // const nextIndex = this.state.activeIndex === ProductData.length - 1 ? 0 : this.state.activeIndex + 1;
    if(size === 1){
      this.setState({ activeIndex: 0});
    }
    else if(this.state.activeIndex == 0){
      this.setState({ activeIndex: this.state.activeIndex + 1 });
    }
  }
  previous(size) {
    if (this.animating) return;
    // const nextIndex = this.state.activeIndex === 0 ? ProductData.length - 1 : this.state.activeIndex - 1;
    if(size === 1){
      this.setState({ activeIndex: 0});
    }
    else if(this.state.activeIndex == 1){
    this.setState({ activeIndex: this.state.activeIndex - 1 });
    }
  }
  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }
  handleDelete = () => {
  }
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  handleClick = () => {
    this.props.history.push('/view-seller')
    // window.open("/view-seller", "_blank")
  }
  handleNavClicked = (e, link) => {
    this.props.history.push(link)
  }
  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  addDefaultSrc(ev){
    ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
  }
  closepop = () => {
    this.setState({ popoverOpen: false })
  }
  render() {
    const { value } = this.props
    const { activeIndex } = this.state
    const inventoryID = value && value && value.inventory_id;
    
    const productName =  value && value.Product && value.Product.name && ((value.Product.name).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase();
    const bgColor = value && value.medi_type === 'Ethical branded' || value.medi_type === 'Others' ? '#ff7000' :
                    value.medi_type === 'Cool chain' ? '#0b68a8' :
                    value.medi_type === 'Surgical' || value.medi_type ==='OTC' || value.medi_type ==='Generic' ? '#038d0e' :'#072791'

    const slides = value && value.Product && value.Product.images && value.Product.images.length > 0 ? value.Product.images.map((image, index) => {
      return (
        <CarouselItem
          key={index}
        >
          {/* <NavLink className="buyerRedirectNavlink" to="/product-details">
            <img src={product.thumb} />
          </NavLink> */}
          {/* <NavLink className="buyerRedirectNavlink" to={`/view-seller/${value.Product.data._id}`}  onClick={(e) => this.handleNavClick(e, `/view-seller/${product.data._id}`)}> */}
          <img onError={this.addDefaultSrc}  src={value.Product.images.length === 0 ? logo : `${helpertFn.productImg(image)}`} />
          {/* </NavLink> */}
        </CarouselItem>
      );
    }): [
      <CarouselItem
        key='images'
      >
        <img src={logo} onError={this.addDefaultSrc}/>
      </CarouselItem>
    ]
    const chipData = [
      { key: 0, class: 'originalPrice font', offer: 200, price: 400, by: 'Medideals' },
    ]
    let a = this.getRandomInt(3)
    let icon = this.props.index % 2 === 0 ? <Avatar className="bg-danger iconAvtarColor text-white">
      <ThumbDownIcon style={{ fontSize: '0.6rem' }} /></Avatar> : <Avatar className="bg-success text-white iconAvtarColor">
        <ThumbUpIcon style={{ fontSize: '0.6rem' }} /></Avatar>
      //    let effects = Number(value.PTR) * Number(value && value.Discount && value.Discount.discount_on_product.purchase);
      //    let effects1 = Number(effects) /(Number(value && value.Discount && value.Discount.discount_on_product.purchase)+ Number(value && value.Discount && value.Discount.discount_on_product.bonus));
    return (
      <React.Fragment>
        <div className="jr-card text-left" >
          <div className={`jr-card-header-color ${this.props.match.url === '/view-seller' ? 'intranetCardViewSeller' : 'intranetCard'} ${this.props.match.url === '/view-seller' ? 'over' : ''}`} style={{minHeight:'200px', position:'relative', display:'inline-flex', padding: '0px',maxHeight: '100%'}}>
            <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${value.inventory_id}`} >
              <Carousel
                autoPlay={false}
                indicators={true}
                activeIndex={activeIndex}
                next={() => this.next(slides.length)}
                interval={false}
                previous={() => this.previous(slides.length)}>
                {slides}
                {/* <CarouselControl cssModule='primary' direction="prev" directionText="Previous" onClickHandler={() => this.previous(slides.length)} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={() => this.next(slides.length)} /> */}
              </Carousel>
            </NavLink>
                <span style={{ padding:'2px', fontWeight:'bold', position: 'absolute', zIndex: 1, backgroundColor:`${bgColor}`, color:'white', width:'auto'}} >{value && value.medi_type === 'Others' ? 'PCD' : value.medi_type}</span>
            {
              this.props.match.url === '/view-company' ? null : 
                <Fab className="jr-badge-up" style={{backgroundColor:`${bgColor}`, color:'white'}} >
                  <i className="zmdi zmdi-shopping-cart" style={{ float: "left" }} onClick={this.toggle} id={`upc1${inventoryID}`} />
                </Fab>
            }
            <div>
              {/* boxShadow: "2px 2px 2px 2px  #d9d3d2", border: "none", */}
              <Popover style={{ padding: "5px", paddingBottom: "0px", textAlign: "center", }} trigger="legacy" placement="right" isOpen={this.state.popoverOpen} target={`upc1${inventoryID}`} toggle={this.toggle} >
                <PopoverHeader style={{ padding: "4px 4px", textAlign: "center" }}>Select Quantity</PopoverHeader>
                <br />
                <CartPopOver closepop={(e)=>this.closepop(e)}
                  dataFromParent={value}
                />
                <PopoverBody style={{ paddingBottom: 0, textAlign: "center" }}>
                </PopoverBody>
              </Popover>

            </div>
          </div>
                    {
                      this.props.value && this.props.value.medi_attribute.includes('Jumbo Deal') ?
                        <Chip
                          label={'Jumbo Deal'}
                          size='small'
                          style={{backgroundColor:`${bgColor}`, padding:'17px 10px', color:'white', float:'left', position:'relative', zIndex:1, marginTop:'-28px', marginLeft:'-17px'}}
                        />
                      :''
                    }
          <div className="jr-card-body pt-2" style={{marginLeft:'-17px'}}>
            <div className="product-details" >
              <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${value.inventory_id}`}>
                <h6 className="card-title card-titleIntranet" style={{minHeight: "45px", maxHeight: "45px", overflow: "hidden", marginTop: "0px"}}>{value.Product.name}</h6>
              </NavLink>
              {/* {
                    this.props.history.location.pathname === '/view-seller' ? null : <a href="/view-seller">
                      <Chip className='mb-2'
                        avatar={<Avatar src={require('assets/images/Anjali-Sud.jpg')} />}
                        label={value.Seller.first_name + " " + value.Seller.last_name}
                        deleteIcon={icon}
                        onDelete={this.handleDelete}
                        onClick={this.handleClick}
                        size='small'
                      />
                    </a>
                  } */}
              <p className="d-flex align-items-baseline manufacturerTag" style={{minHeight: "62px"}}>By&nbsp;<NavLink className="buyerRedirectNavlink " to={`/view-company/${value.Company._id}`} onClick={(e) => this.handleNavClicked(e, `/view-company/${value.Company._id}`)} >
                  <h6 className="text-primary ">{value.Company.name}</h6>
                </NavLink>
              </p>
                <div style={{minHeight: "15px", maxHeight: "15px", marginBottom:"5px", padding:"0px"}}>
                  {
                    helpertFn.showPrepaid(this.props.value.medi_type,this.props.value.Product.isPrepaid && this.props.value.Product.isPrepaid, this.props.value.prepaidInven &&this.props.value.prepaidInven) &&
                  <span className='text-white bg-danger' style={{margin:"0px", padding:"0px 5px"}}>Only Prepaid</span>
                  }
              </div>
            </div>
          </div>
          <div className='jr-card-footer intranetFooterSeller mt-0 pl-0' style={{ overflow: 'hidden', 
        minHeight:'90px' }}>
            {/* <table>
                  <tbody>
                    <tr>
                      <td>
                        <span>asd</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span className="discountPrice"> </span>
                      </td>
                    </tr>
                  </tbody>
                </table> */}
                {                          
                  value.Discount && value.Discount.discount_type === "Same" ?
                    <React.Fragment>
                        {/* <span className={'priceColor'}>
                          ₹{(value.Inventory.PTR).toFixed(2)}
                        </span>
                        &nbsp; */}
                      <span className={'originalPriceSeller'} >
                        {("Buy " + value.Discount.discount_on_product.purchase + " Get " + value.Discount.discount_on_product.bonus + " Free ")}
                      </span><br/>
                      <span style={{fontSize: 'small'}} >
                        Min: {value.min_order_quantity}, Max: {value.max_order_quantity}
                      </span>
                      <br/>
                      <span className={'priceColor'}>&#x20B9;{(value.ePTR).toFixed(2)}</span>&nbsp;
                      <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>&nbsp;
                      {/* <Tooltip
                        className="d-inline-block"
                        id="tooltip-right"
                        title={
                          <span style={{ marginTop: "13px" }}>
                            Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} Free
                                  </span>
                        }
                        placement="right"
                      > */}
                      {/* </Tooltip> */}
                    </React.Fragment>

                  :
                  value.Discount && value.Discount.discount_type === "SameAndDiscount" ?
                    <React.Fragment>
                                  {/* <span className={'priceColor'}>
                                    ₹{(value.Inventory.PTR).toFixed(2)}
                                  </span>
                                  &nbsp; */}
                                <Tooltip
                                className="d-inline-block"
                                id="tooltip-right"
                                title={
                                  <span style={{ marginTop: "13px" }}>
                                    Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} Free, and {(value.Discount.discount_per).toFixed(2)}% Off
                                          </span>
                                }
                                placement="right"
                                >
                                    <span className={'originalPriceSeller'} >
                                      {("Buy " + value.Discount.discount_on_product.purchase + " Get " + value.Discount.discount_on_product.bonus + " Free, and "+(value.Discount.discount_per).toFixed(2)+"% Off")}
                                    </span>
                                </Tooltip>
                                <br/>
                                <span style={{fontSize: 'small'}} >
                                  Min: {value.min_order_quantity}, Max: {value.max_order_quantity}
                                </span><br/>
                                <span className={'priceColor'}>&#x20B9;{(value.ePTR).toFixed(2)}</span>&nbsp;
                                <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>&nbsp;
                                
                              </React.Fragment> :

                            value.Discount && value.Discount.discount_type === "Different" ? 

                              <React.Fragment>
                                <Tooltip
                                  className="d-inline-block"
                                  id="tooltip-right"
                                  title={
                                    <span className="text-white" style={{ marginTop: "13px" }}>
                                      Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} {value.OtherProducts.name} Free
                                    </span>
                                  }
                                  placement="right"
                                  >
                                    <span className={'originalPriceSeller'} > 
                                      {("Buy " + value.Discount.discount_on_product.purchase + " Get " + value.Discount.discount_on_product.bonus + " " + value.OtherProducts.name + " Free").slice(0,25)+ "..."}
                                    </span> 
                                </Tooltip>
                                <br />
                                <span style={{fontSize: 'small'}} >
                                  Min: {value.min_order_quantity}, Max: {value.max_order_quantity}
                                </span>
                                <br/>

                                <span className={'priceColor'}>₹{(value.ePTR).toFixed(2)}</span>
                                &nbsp;
                                <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>&nbsp;
                            </React.Fragment>
                            :
                              value.Discount && value.Discount.discount_type === "DifferentAndDiscount" ? 

                              <React.Fragment>
                              <Tooltip
                                  className="d-inline-block"
                                  id="tooltip-right"
                                  title={
                                    <span className="text-white" style={{ marginTop: "13px" }}>
                                      Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} {value.OtherProducts.name} Free, and {(value.Discount.discount_per).toFixed(2)}% Off
                                    </span>
                                  }
                                  placement="right"
                              >
                                  <span className={'originalPriceSeller'} > 
                                    {("Buy " + value.Discount.discount_on_product.purchase + " Get " + value.Discount.discount_on_product.bonus + " " + value.OtherProducts.name + " Free, and "+(value.Discount.discount_per).toFixed(2)+"% Off").slice(0,25)+ "..."}
                                  </span> 
                              </Tooltip>
                              <br/> 
                              <span style={{fontSize: 'small'}} >
                                Min: {value.min_order_quantity}, Max: {value.max_order_quantity}
                              </span><br />

                                <span className={'priceColor'}>₹{(value.ePTR).toFixed(2)}</span>
                                &nbsp;
                              <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>&nbsp;
                        </React.Fragment> :
                        value.Discount && value.Discount.discount_type === "Discount" ?
                        <React.Fragment>
                              <span className={'originalPriceSeller'} style={{ marginTop: "13px" }}>
                            {   (value.Discount.discount_per).toFixed(2)}% Discount
                              </span>
                              <br/>
                              <span style={{fontSize: 'small'}} >
                                Min: {value.min_order_quantity}, Max: {value.max_order_quantity}
                              </span><br />
                                <span className={'priceColor'}>₹{(value.ePTR).toFixed(2)}</span> 
                                &nbsp;
                                <span className={'originalPrice font'}>₹{(value.PTR).toFixed(2)}</span>
                                &nbsp;
                              {/* <Tooltip
                                className="d-inline-block"
                                id="tooltip-right"
                                title={
                              <span  style={{ marginTop: "13px" }}>
                            {   value.Discount.discount_per}% Discount
                              </span>} placement="right" > */}
                              {/* </Tooltip> */}
                              </React.Fragment>
                                :                   
                    value.Discount  == undefined ?
                      <React.Fragment>
                        {/* <span className={'priceColor'}>
                        ₹{(value.Inventory.PTR).toFixed(2)}
                      </span>
                      &nbsp; */}
                              <span style={{fontSize: 'small'}} >
                                Min: {value.min_order_quantity}, Max: {value.max_order_quantity}
                              </span>
                              <br/>
                              <span className={'priceColor'}>&#x20B9;{(value.ePTR).toFixed(2)}</span>&nbsp;
                              <span className={'originalPrice font'}>&#x20B9;{value.MRP.toFixed(2)}
                              </span>&nbsp;
                      </React.Fragment>
                      :
                      ''
                        }
                        
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default withRouter(SellerProductCard);
import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import {Input} from 'reactstrap'

const suggestions = [
  { label: 'Medicine' },
  { label: 'Toothpaste' },
  { label: 'Toothbrush' },
  { label: 'Crocin' },
  { label: 'D-Cold Total' },
  { label: 'Paracetamol' },
  { label: 'Honey' },
];
function renderSuggestion(suggestionProps) {
  const { suggestion, index, itemProps, highlightedIndex, selectedItem } = suggestionProps;
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.label}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion.label}
    </MenuItem>
  );
}

const SearchBox = ({styleName, placeholder, onChange, value,handleClick,handleSigninOnEnter}) => {

  return (
    <div className={`search-bar right-side-icon bg-transparent ${styleName}`}>
      <div className="form-group search-container" >
      <Input bsSize="lg" className="form-control border-0 lg" type="search" placeholder={placeholder} onChange={onChange}
      onKeyPress={ handleSigninOnEnter}   value={value} autoComplete="off"/>
     <button onClick = {handleClick} className="search-icon"><i className="zmdi zmdi-search zmdi-hc-lg"/></button>
     
        
      </div>
    </div>
  )
};

export default SearchBox;

SearchBox.defaultProps = {
  styleName: "",
  value: "",
};
import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import renderTextField from '../../../components/textBox';
import FieldFileInput from '../../../components/FieldFileInput';
import { required, emailField, passwordMatch,fassaiLicNoRequired, gstLicNoRequired } from '../../../constants/validations';
import { Col, Row } from 'reactstrap';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom'
import RenderDatePicker from 'components/RenderDatePicker'
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import './style.css'

class Form2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drugLic20BLicNo:'',
            drugLic21BLicNo:'',
            drugLic20B:"",
            drugLic21B:'',
            fassaiLic:'',
            fassaiLicNo:'',
            gstLic:'',
            gstLicNo:''
            
        }
    }

    handleFileSelect = async (e, key) => {
        e.preventDefault();
        let document = "";
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = () => {
            document = reader.result;
            this.setState({ [key]: document })
        };
        reader.onerror = function (error) {
        };
        this.props = Object.assign({}, this.props, { key: document });
    }

    
    render() {
        const { handleSubmit, previousPage,submitting  } = this.props
        const {drugLic20BLicNo,drugLic21BLicNo,drugLic20B,drugLic21B,fassaiLic,fassaiLicNo,gstLic,gstLicNo} = this.state
        return (
            <form onSubmit={handleSubmit} autocomplete="off">
                <Grow in={true}>
                    <React.Fragment>
                        <Row>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field value={drugLic20B} name="drugLic20B" component={FieldFileInput} label="Drug Lic 20B" validate={(value)=> (value === undefined ? 'Please Upload Drug Lic 20B' : '')} />
                                <Field name="drugLic20BLicNo" validate={[required]} label = 'Drug Lic20B no.' component={renderTextField}  />
                                <Field name="drugLic20BExpiry"  component={RenderDatePicker}  />
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field name="drugLic21B" component={FieldFileInput} label="21B  Drug Lic" validate={(value)=> (value === undefined ? 'Please Upload Drug Lic 21B' : '')} />
                                <Field name="drugLic21BLicNo" validate={[required]} label = 'Drug Lic21B no.' component={renderTextField}  />
                                <Field name="drugLic21BExpiry"  component={RenderDatePicker}  />
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field value={fassaiLic} name="fassaiLic" component={FieldFileInput} label="FSSAI Lic (Optional)" />
                                <Field value={fassaiLicNo} name="fassaiLicNo" validate={[fassaiLicNoRequired('fassaiLic')]} label = 'FSSAI Lic no.' component={renderTextField}  />
                                <Field name="fassaiLicExpiry" value={new Date()} component={RenderDatePicker}  />
                            </Col>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="mt-2">
                                <Field value={gstLic} name="gstLic" component={FieldFileInput} label="GSTN Lic (Optional)"  />
                                
                                <Field value={gstLicNo} name="gstLicNo" validate={[gstLicNoRequired('gstLic')]} label = 'GSTN Lic no.' component={renderTextField}  />

                            </Col>
                        </Row>
                        <div className='row'>
                            <Col sm={12} md={6} lg={6} xs={12} xl={6} className="text-left">
                                {
                                    !submitting&&
                                    <div className='list-action mt-3'>
                                    <Button type="button" variant='contained' color='primary' className='m-2 signin' size='medium'onClick={previousPage}>Previous</Button>
                                </div>
                                }
                                
                                <div className='list-action mt-3'>
                                    <Button style={{color:'white'}} type='submit' disabled={submitting} variant='contained' color='primary' className='m-2 signin' size='medium'>{submitting ?'Processing':'Finish'}</Button>
                                </div>
                            </Col>
                        </div>
                    </React.Fragment>
                </Grow>
            </form>
        )
    }
}

Form2 = reduxForm({
    form: 'wizardbuyer',  //Form name is same
    destroyOnUnmount: false,
})(Form2)

export default withRouter(Form2) 
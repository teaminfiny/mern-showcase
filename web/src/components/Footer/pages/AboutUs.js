import React, { Component } from 'react';
import logo from '../../../assets/images/medimny.png'
import {Helmet} from "react-helmet";


class AboutUs extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
        this.myRef = React.createRef()
    }
    componentDidMount = () => {
        this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }
    render() { 
        return ( 
            <React.Fragment>
                <Helmet>
                    <title>About Us | Pharmaceutical Products Wholesale Distributors In India - Medimny</title>
                    <meta name="title" content="About Us | Pharmaceutical Products Wholesale Distributors In India - Medimny" />
                    <meta name="description" content={`Incorporated under corporate identity "Felicitas Digitech Private Limited" and brand name "Medimny", we aim to bring-in efficiencies in the supply chain in the sector by providing a common platform to buyers and sellers across the nation.`} />
                </Helmet>
                <div className="app-wrapper" ref={this.myRef}>
                    <div className="row">

                        <div className="col-xl-8 col-lg-8 col-sm-8 col-xs-12 signinContainer mt-4 mb-5" >
                    <div className="jr-card AboutUs">
                        <div className="animated slideInUpTiny animation-duration-3 ">
                        
                        <h1 style={{textAlign:'center', fontWeight:'bolder'}}>
                            About Us
                            {/* <ContainerHeader match={this.props.match} title={<IntlMessages id="About Us" />} /> */}
                        </h1>

                            <div className="login-header mb-0">
                            <img src={logo} width="40%" style={{marginBottom: "60px", marginTop: "35px"}}/>
                            </div>
                            <h3 style={{ color: "#5b5b5b"}}>
                                We are a new age technology-driven B2B<br/> ecommerce marketplace in pharma and healthcare space.
                                </h3> 

                                <br/>
                            <h4 style={{ color: "#5b5b5b" }} className="mb-4">
                                Incorporated under corporate identity "Felicitas Digitech Private Limited" and brand name "Medimny",
                                we aim to bring-in efficiencies in the supply chain in the sector by providing a common platform to buyers
                                and sellers across the nation.
                            </h4> 
                        </div>
                    </div>
                </div>


                    </div>
                </div>
            </React.Fragment>
         );
    }
}
 
export default AboutUs;





/* <div className="col-xl-7 col-lg-7">
<Widget styleName="jr-card-profile">
  <div className="mb-3">
       <h3 className="card-title mb-2 mb-md-3">Medideals</h3>
     <p className="text-grey jr-fs-sm mb-0">A little flash back of Kiley Brown</p>
  </div>
  <h3 className="jr-font-weight-bold">
      We are a new age technology-driven B2B ecommerce marketplace in pharma and healthcare space.
  </h3>
  <h3>Incorporated under corporate identity "Felicitas Digitech Private Limited" and brand name "Medideals",
we aim to bring-in efficiencies in the supply chain in the sector by providing a common platform to buyers
and sellers across the nation.
  </h3>


</Widget>
</div> */
import React, { Component } from 'react';
import IntlMessages from '../../../util/IntlMessages';
import ContainerHeader from 'components/ContainerHeader';
import { Field, reduxForm, getFormInitialValues } from 'redux-form'
import { required, validatePhone, emailField } from '../../../components/BEditProfile/BEditValidation';
import ReactStrapTextField from 'components/ReactStrapTextField';
import { Row, Col, FormGroup } from 'reactstrap'
import Button from '@material-ui/core/Button';
import AxiosRequest from 'sagas/axiosRequest'
import { NotificationManager } from 'react-notifications';
class ContactUs extends Component {
    constructor(props) {
        super(props);
        this.state = { firstName: '', lastName: '',  email: '', phone: '', msg:'', companyName:'', city: '' }
        this.myRef = React.createRef()
    }  

    componentDidMount = () => {
      this.props.initialize({ firstName: '', lastName: '',  email: '', phone: '', msg:'', companyName:'', city: ''  });
      this.setState({ firstName: '', lastName: '',  email: '', phone: '', msg:'', companyName:'', city: ''  });
      this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }

    submit = async(e) => {
      let data = {
        shop_name: this.state.companyName,
        name: this.state.firstName,
        phone: this.state.phone,
        city: this.state.city,
        email: this.state.email,
        msg: this.state.msg
      }
      let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'users/sendEmailToAdmin', '', data)
      if (response.data.error) {
        NotificationManager.error(response.data.title)
      } else {
        NotificationManager.success(response.data.title);
        this.componentDidMount();
      }
    }

    handleChange = (key, event) => {
      this.setState({ [key]: event.target.value });
    }

    render() { 
      const { handleSubmit } = this.props;
      let { firstName,  email, phone, msg, companyName, city } = this.state;
        return ( 
      <React.Fragment>
                <div className="app-wrapper" ref={this.myRef}>

              <div className="row">
                <div className="col-xl-12 col-lg-12">
                  <ContainerHeader match={this.props.match} title={<IntlMessages id="Contact Us" />} />
                </div>
                <div className="col-lg-9 col-md-8 col-sm-7 col-12">
                  <form onSubmit={handleSubmit(this.submit)} className="contact-form jr-card">
                    <Row>
                      <Col sm={12} md={6} lg={6} xs={12} xl={6}>

                        <Field id="firstName"
                          name="firstName" type="text"
                          component={ReactStrapTextField}
                          label='Name'
                          value={firstName}
                          onChange={(e) => this.handleChange('firstName', e)}
                        />
                      </Col>
                      <Col sm={12} md={6} lg={6} xs={12} xl={6}>
                        <Field id="companyName" name="companyName" type="text"
                          component={ReactStrapTextField}
                          validate={[required]}
                          label='Company Name'
                          value={companyName}
                          onChange={(e) => this.handleChange('companyName', e)}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col sm={12} md={4} lg={4} xs={12} xl={4}>
                        <Field id="email" name="email" type="text"
                          component={ReactStrapTextField}
                          validate={[emailField]}
                          label='Email'
                          value={email}
                          onChange={(e) => this.handleChange('email', e)}
                        />
                      </Col>
                      <Col sm={12} md={4} lg={4} xs={12} xl={4}>
                        <Field id="phone" name="phone" type="text"
                          component={ReactStrapTextField}
                          validate={[validatePhone]}
                          label='Phone'
                          value={phone}
                          onChange={(e) => this.handleChange('phone', e)}
                        />
                      </Col>
                      <Col sm={12} md={4} lg={4} xs={12} xl={4}>
                        <Field id="city" name="city" type="text"
                          component={ReactStrapTextField}
                          validate={[required]}
                          label='City'
                          value={city}
                          onChange={(e) => this.handleChange('city', e)}
                        />
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <FormGroup>
                          <Field id="msg" name="msg" type='textarea'
                            component={ReactStrapTextField}
                            rows="6"
                            label='How can we help you ?'
                            value={msg}
                            onChange={(e) => this.handleChange('msg', e)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <div className="row">
                      <div className="col-12">
                        <div className="form-group mb-0">
                          <Button type='submit' color="primary" variant="contained"
                            className="jr-btn jr-btn-lg btnPrimary" >
                            Send
                          </Button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>

        <div className="col-lg-3 col-md-4 col-sm-5 col-12">
          <div className="contact-block jr-card py-5 px-4">
            <ul className="contact-info vertical">
              <li>
                <i className="zmdi zmdi-pin zmdi-hc-fw"/>
                <div className="contact-body">
                  <h4 className="text-uppercase">ADDRESS</h4>
                  <address className="mb-0">
                    FELICITAS DIGITECH PRIVATE LIMITED
                    <br/>
                    Unit 511, Opal Square, Wagle Industrial Estate,
                    <br/>
                    Near Mulund Toll Naka,
                    <br/>
                    Thane (W) - 400604 INDIA
                  </address>
                </div>
              </li>

              <li>
                <i className="zmdi zmdi-phone zmdi-hc-fw"/>
                <div className="contact-body">
                  <h4 className="text-uppercase">Phone</h4>
                  <div><span className="jr-link text-primary disable-link">
                        +91 9321927004
                      </span>
                  </div>
                  {/* <div><span className="jr-link text-primary disable-link">+91 9321927004</span>
                  </div> */}
                </div>
              </li>

              <li>
                <i className="zmdi zmdi-email zmdi-hc-fw"/>
                <div className="contact-body">
                  <h4 className="text-uppercase">E-mail</h4>
                  <div><span className="text-primary jr-link">helpdesk@medimny.com</span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      </div>

    </React.Fragment>
         );
    }
}
 
ContactUs = reduxForm({
  form: 'ContactUs',// a unique identifier for this form
  initialValues: getFormInitialValues('ContactUs')()
})(ContactUs)
export default ContactUs;
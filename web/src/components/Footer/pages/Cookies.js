import React, { Component } from 'react';
import IntlMessages from '../../../util/IntlMessages';

import ContainerHeader from 'components/ContainerHeader';



class Cookies extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <React.Fragment>
                <div className="app-wrapper">
                    <div className="row">

                        <div className="col-xl-12 col-lg-12">
                            <ContainerHeader match={this.props.match} title={<IntlMessages id="Cookies" />} />
                        </div>


                    </div>
                </div>
            </React.Fragment>
         );
    }
}
 
export default Cookies;
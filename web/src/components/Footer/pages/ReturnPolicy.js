import React, { Component } from 'react';
import IntlMessages from '../../../util/IntlMessages';

import ContainerHeader from 'components/ContainerHeader';



class ReturnPolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
        this.myRef = React.createRef()
    }
    componentDidMount = () => {
        this.myRef.current.scrollIntoView({ behavior: 'smooth' })
    }
    render() { 
        return ( 
            <React.Fragment>
                <div className="app-wrapper" ref={this.myRef}>
                    <div className="row">

                        <div className="col-xl-12 col-lg-12">
                            <ContainerHeader match={this.props.match} title={<IntlMessages id="Return Policy" />} />
                        </div>


                    </div>
                </div>
            </React.Fragment>
         );
    }
}
 
export default ReturnPolicy;
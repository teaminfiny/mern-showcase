import React from 'react';
import { Col, Row } from 'reactstrap'
import './index.css'
import ios from '../../assets/images/iosAppStore.png'
import android from '../../assets/images/androidPlayStore.png'
import { NavLink, withRouter } from 'react-router-dom';

const Footer = () => {
  return (

    <footer className="app-footer newfooter">

      <div className="mt-3">


<div>

        <Row className="rowstyle">
          <Col md="6">
            <h5 className=""><strong>About</strong></h5>
            <ul style={{ paddingInlineStart: "0px" }}>

              <NavLink className="buyerRedirectNavlink" to={`/about-us`}>
                <li className="liststyle mt-1 text-primary">
                  {/* <a href="#!">About Us</a> */}
                  About Us
                </li>
              </NavLink>
            
              {/* <NavLink className="buyerRedirectNavlink" to={`/Services`}>
              <li className="liststyle mt-1">
                <a href="#!">Services</a>
              </li>
              </NavLink> */}

              <NavLink className="buyerRedirectNavlink" to={`/ContactUs`}>
              <li className="liststyle mt-1 text-primary">
                {/* <a href="#!">Contact Us</a> */}
                Contact Us
              </li>
              </NavLink>

              {/* <NavLink className="buyerRedirectNavlink" to={`/Careers`}>
              <li className="liststyle mt-1">
                <a href="#!">Careers</a>
              </li>
              </NavLink> */}

            </ul>
          </Col>

          <Col md="6">
            <h5 className=""><strong>Policy</strong></h5>
            <ul style={{ paddingInlineStart: "0px" }}>

              {/* <NavLink className="buyerRedirectNavlink" to={`/ReturnPolicy`}>
                <li className="liststyle mt-1">
                  <a href="#!">Return Policy</a>
                </li>
              </NavLink> */}

              <NavLink className="buyerRedirectNavlink" to={`/TermsOfUse`}>
                <li className="liststyle mt-1 text-primary">
                  {/* <a href="#!"></a> */}
                  Terms Of Use
                </li>
              </NavLink>

              <NavLink className="buyerRedirectNavlink" to={`/Privacy`}>
                <li className="liststyle mt-1 text-primary">
                  {/* <a href="#!"></a> */}
                  Privacy & Cookies
                </li>
              </NavLink>

              <NavLink className="buyerRedirectNavlink" to={`/ShippingAndReturnPolicy`}>
                <li className="liststyle mt-1 text-primary">
                  {/* <a href="#!"> </a> */}
                  Shipping And Return Policy
                </li>
              </NavLink>

              {/* <NavLink className="buyerRedirectNavlink" to={`/Cookies`}>
                <li className="liststyle mt-1">
                  <a href="#!">Cookies</a>
                </li>
              </NavLink> */}

            </ul>
          </Col>

        </Row>




        <div className="approw">
        <Row className="rowstyle pull-right">
          <Col md="12" className="text-right appStyle">
            {/* <h5 className="textLeft"><strong style={{color: "black"}}>Coming Soon</strong></h5> */}
            <ul style={{ paddingInlineStart: "0px" }}>
              <li className="liststyle">
                <a href="https://apps.apple.com/in/app/medimny/id1560601461"  target="_blank">
                    <img src={ios} style={{ width: "55%" }} />
                </a>
              </li>

              <li className="liststyle mt-2">
                <a href="https://play.google.com/store/apps/details?id=com.medimny"  target="_blank">
                    <img src={android} style={{ width: "55%" }} />
                </a>
              </li>
            </ul>
          </Col>

        </Row>
      
      </div>

      </div>





        <div className="footText">
          <p className="">  &copy; Felicitas Digitech Private Limited 2021. All Rights Reserved.</p>
          <p>The trademarks, logos and service marks displayed on the Website (&quot;Marks&quot;) are the property of Felicitas
Digitech Private Limited or their seller or respective pharmaceutical companies or third parties. You are not
permitted to use the Marks without the prior consent of Felicitas Digitech Private Limited or their seller or
respective pharmaceutical companies or third parties that may own the Marks.</p>
        </div>
      
      </div>






      {/* <div className="pull-right" style={{textAlign: "end"}}>
        <Row className="rowstyle">
          <Col md="6">
            <h5 className="">Our Apps</h5>
            <ul style={{ paddingInlineStart: "0px" }}>
              <li className="liststyle pull-right">
                <a href="#!"> <img src={ios} style={{ width: "100%" }} /></a>
              </li>
              <li className="liststyle pull-right">
                <a href="#!"><img src={android} style={{ width: "120%" }} /></a>
              </li>
            </ul>
          </Col>
        </Row>
      </div> */}

      {/* <div className="">
      <img src={ios} style={{ width: "15%" }} />
      </div>    

      <div className="">
      <img src={android} style={{ width: "7%" }} />
      </div> */}






    </footer>
  );
}
  

export default withRouter(Footer);

      
     /*  <Button
        href="https://codecanyon.net/cart/configure_before_adding/20978545?license=regular&ref=phpbits&size=source&support=bundle_12month&_ga=2.172338659.1340179557.1515677375-467259501.1481606413"
        target="_blank"
        size="small"
        color="primary"
      >
        <IntlMessages id="eCommerce.buyNow" />
      </Button> */      
      /* <span className="d-inline-block">  &copy; Felicitas Digitech Private Limited 2020. All Rights Reserved.</span> */
    
      /* <Container fluid className="text-center text-md-left"> */

import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { Col, Row, Form, Badge, Label, Input } from 'reactstrap';
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import { connect } from 'react-redux';
import helperFunction from '../.../../../constants/helperFunction'
import {getTransactionList} from 'actions/buyer';
import { withRouter } from 'react-router-dom'

const monthArray = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'August',
  'September',
  'October',
  'November',
  'December',
];

let date = new Date();

class CustomBuyerFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      month: '',
      year: 0,
      yearList: []
    }
  }

  /*  componentWillReceiveProps = (nextProps, nextState) => {
     console.log('nextProps ',nextProps.month, nextProps.year, this.props.month, this.props.month !== nextProps.month)
    if(nextProps.month && (this.props.month !== nextProps.month) || (nextProps.year && (this.props.year !== nextProps.year) )) {
      this.setState({month :nextProps.month, year : nextProps.year })
    }
   } */

  componentDidMount = () => {
    this.setState({ month: this.props.month ? this.props.month : monthArray[date.getMonth() - 1], year: this.props.year ? this.props.year : date.getFullYear() })

    let years = helperFunction.yearList();
    this.setState({ yearList: years })
  }

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
    this.props.onFilterChange(e, key);
  }

  handleClose = e => {
    e.preventDefault();
    this.props.handleResetFilter(e, this.props.applyFilter)
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.applyFilter();
    let data = {
      page: 1,
      perPage: 10,
      filter: '',
      month: this.state.month,
      year: parseInt(this.state.year)
    }
      this.props.getTransactionList({ data, history: this.props.history })
  }

  render() {
    let { month, year, yearList } = this.state;
    return (
      <React.Fragment>
        <Row form>
          <Col md={6} xl={6} xs={12} sm={12} lg={6}>
            <FormGroup>
              <Label for="exampleSelect">Select Month</Label>
              <Input type="select" name="month" value={month} onChange={(e) => this.handleChange(e, 'month')} id="month">
                {
                  monthArray.map((value, key) => {
                    return <option value={value} >{value}</option>;
                  })
                }
              </Input>
            </FormGroup>
          </Col>
          <Col md={6} xl={6} xs={12} sm={12} lg={6}>
            <FormGroup>
              <Label for="exampleSelect">Select Month</Label>
              <Input type="select" value={year} onChange={(e) => this.handleChange(e, 'year')} name="year" id="year">
                {
                  yearList.map((value, key) => {
                    return <option value={parseInt(value)} >{value}</option>
                  })
                }
                {/* <option value="2015" >2015</option>
                <option value="2016" >2016</option>
                <option value="2017" >2017</option>
                <option value="2018" >2018</option>
                <option value="2019" >2019</option> */}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <div style={{ paddingTop: 15 }} className="row" >
          <Col md={6} xl={6} xs={12} sm={12} lg={6}>
            <Button variant="contained" className='filterButton' onClick={(e) => this.handleSubmit(e)} color='primary'>Apply Filter</Button>
          </Col>
          <Col md={6} xl={6} xs={12} sm={12} lg={6} className="text-nowrap">
            <Button variant="contained" onClick={(e) => this.handleClose(e)} className='filterButton' color='primary'>Reset Filter</Button>
          </Col>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = ({ buyer }) => {
  const { transactionListData } = buyer;
  return { transactionListData };
}
export default withRouter(connect(mapStateToProps, {getTransactionList})(CustomBuyerFilter));
import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';


let counter = 0;

function createData(name, sellerName, quantity, image) {
  counter += 1;
  return { id: counter, name, sellerName, quantity, image };
}

class ProductTable extends Component {
  state = {
    data: [
      
    ],
  };

  render() {
    let data2 = this.props.value?this.props.value:this.state.data
    return (
      <div className="table-responsive-material">
        <table className="default-table table-unbordered table table-sm table-hover">
          <thead className="th-border-b">
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
            {data2.length > 0 && data2.map(data => {
              return (
                <tr
                  tabIndex={-1}
                // key={data.id}
                >
                  <td>
                    <div className="user-profile d-flex flex-row align-items-center">
                      {/* <Avatar
                        alt={data.name}
                        src={data.image}
                        className="user-avatar"
                      /> */}
                      <div className="user-detail">
                        <h5 className="user-name">{data.Product.name} </h5>
                      </div>
                    </div>
                  </td>
                  <td>{data.Company.name}</td>
                  <td>{data.quantity}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ProductTable;
import React from 'react';
import PropTypes from 'prop-types';
import { lighten, makeStyles } from '@material-ui/core/styles';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TrackOrder from './trackOrder'
import './index.css'

import {Popover, PopoverBody, PopoverHeader} from 'reactstrap';


function createData(orderId, seller, status, amount,product,description, value) {
  return { orderId, seller, status, amount,product,description, value };
}

const statusStyle = (status) => {
    return status.includes("Processed") ? "text-white bg-primary" : status.includes("Ready for dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-danger" : "text-white bg-danger";
  }

const rows = [
  createData('ODI12584', <a className='text-reset text-primary' href='/view-seller'>Avanish</a>, 
  <div key={'recent'} className={` badge text-uppercase ${statusStyle('Placed')}`}>Placed</div>, <span>&#8377; 1380</span>,
  require('assets/productImage/user.jpg'),'Crocin X 20', 67,),
  createData('ODI84646',<a className='text-reset text-primary' href='/view-seller'>Sachin</a>, 
  <div key={'recent'} className={` badge text-uppercase ${statusStyle('Ready for dispatch')}`}>Ready for dispatch</div>, <span>&#8377; 51</span>,
  require('assets/productImage/user.jpg'), "D'Cold Total X 60", 67,),
  createData('ODI79797', <a className='text-reset text-primary' href='/view-seller'>Abhidnya</a>,
  <div key={'recent'} className={` badge text-uppercase ${statusStyle('Processed')}`}>Processed</div>, <span>&#8377; 2400</span>,
  require('assets/productImage/user.jpg'),'Colgate X 60', 67,),
  createData('ODI314564', <a className='text-reset text-primary' href='/view-seller'>Prafful</a>,
  <div key={'recent'} className={` badge text-uppercase ${statusStyle('Processed')}`}>Processed</div>, <span>&#8377; 1100</span>,
  require('assets/productImage/user.jpg'),'Cough Syrup X 15', 67,),
]

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headCells = [
  { id: 'orderId', numeric: false, disablePadding: true, label: 'OrderID' },
  { id: 'seller', numeric: false, disablePadding: false, label: 'Seller' },
  { id: 'amount', numeric: false, disablePadding: false, label: 'Amount' },
  { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
  
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={'left'}
            padding={'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={order}
              onClick={createSortHandler(headCell.id)}
              className={classes.fontSize}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));



const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: 0,
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 400,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  list:{
    maxWidth:239
  },
  fontSize:{
    fontSize:'1rem'
  }
}));


class CollapseComponent extends React.Component{
    constructor(props){
      super(props)

      this.toggle = this.toggle.bind(this);

      this.state = {
        openModal:false,
        popoverOpen: false
      }
    }

    handleClose = ()=>{
      this.setState({openModal:false})
    }

    handleOpenModal = ()=>{
      this.setState({openModal:true})
    }

    toggle() {
      this.setState({
        popoverOpen: !this.state.popoverOpen
      });
    }


    render(){



      
        const{collapse,value} = this.props

        const {openModal} = this.state
        
        const productData = [
          {title:'Fair N Lovely',qty:3,actual:400,finalPrice:300, info: true},
          {title:'Crocin tablets',qty:4,actual:500,finalPrice:200, info: false},
          {title:'Yakut',qty:6,actual:100,finalPrice:80, info: false},
          {title:'Vicks Tablets',qty:10,actual:1000,finalPrice:400, info: false},
          {title:'Relief Pain Spray',qty:20,actual:1200,finalPrice:800, info: false}
        ]
        
        
          return(
            <React.Fragment>
                {
                    collapse===true &&
                <React.Fragment>
                
                  <TableRow className='w-100 h-50'>
                    <TableCell colSpan={4} align="right" className='w-100 h-50' >
                    <div className='row ml-3'>
                    <div className = 'col-12 d-flex justify-content-center'>
                      <List className='col-sm-12 col-xs-12 col-md-8 col-lg-8 col-xl-8'>
                      {
                        productData.map((data,index)=>
                        <ListItem   key={index}> 
                        {/* alignItems={'flex-start'} */}
                        <ListItemText
                        className={'listMaxWidth'}
                        primary={data.title}
                        secondary={ `Qty: ${data.qty}`}
                        />
                        <ListItemText
                        className={'finalPrice'}
                        primary={`₹ ${data.finalPrice}`}
                        secondary={ <strike>{`₹ ${data.actual}`}</strike>}
                        />
                        <ListItemText>
                        {
                          data.info === true? 
                          <div>
                          <i class="zmdi zmdi-info-outline" style={{float: "left"}} onClick={this.toggle} onMouseOut = {this.toggle} id="infoPopOver"></i> 
                          <div>
                          <Popover placement="right" isOpen={this.state.popoverOpen} target="infoPopOver" toggle={this.toggle}>
                            <PopoverHeader>Info</PopoverHeader>
                            <PopoverBody>
                              The quantity you requested is not available.
                              Kindly review your order again.
                              </PopoverBody>
                          </Popover>
                        </div>
                          </div>

                          
                          : null
                        }
                        </ListItemText>

                        </ListItem>
                        )
                      }
                        
                      </List>
                      </div>
                      </div>
                      <div className='row ml-3'>
                        <div className = 'col-12 d-flex justify-content-center'>
                          
                          <List className='col-sm-12 col-xs-12 col-md-8 col-lg-8 col-xl-8'>
                          <Divider />
                            <ListItem  alignItems={'flex-start'} >
                              <ListItemText
                              className={'listMaxWidth'}
                              primary={'Total'}
                              />
                              <ListItemText
                              primary={`₹ 1780`}
                              
                              />
                            </ListItem>
                          </List>
                        </div>
                      </div>
                      <Divider />
                      
                    
                        
                        <div className='row ml-3  flex-row-reverse'>
                    <div className = 'col-sm-12 col-xs-12 col-md-5 col-lg-6 col-xl-6 mt-3'>
                        <div className='list-action mx-auto'>
                        <Button className="jr-btn text-uppercase text-danger">
                          Cancel
                        </Button>

                        {/* <Button className="jr-btn text-uppercase text-primary">
                              Edit
                        </Button> */}

                        <Button className="jr-btn text-uppercase text-success" onClick = {this.handleOpenModal}>
                          Track
                        </Button>
                      </div>
                      </div>
                      </div>
                       
                          
                     
                    </TableCell>
                  </TableRow>
                </React.Fragment>
                }

              <Dialog open = {openModal}
              onClose={this.handleClose}
              fullWidth = {true}
              >
                <DialogTitle id="alert-dialog-title">{"Order tracking details"}</DialogTitle>
                <DialogContent>
                  <TrackOrder/>
                </DialogContent>
                <DialogActions>
                  
                  <Button onClick={this.handleClose} color="primary" autoFocus>
                    Ok
                  </Button>
                </DialogActions>
              </Dialog>
            </React.Fragment>
            
        )
    }
}
class Componenttable extends React.Component{
    constructor(props){
        super(props)
        this.state={
            collapse:false
        }
    }
    OnCollapseProject=()=>{
        this.setState({collapse:!this.state.collapse})
        this.props.handleCollapse()
    }
    
    render(){
        const {collapse} = this.state;
        
        return(
            <React.Fragment>
                
                    {collapse ?
                    <span className='mr-1 cursor-pointer' onClick={() => this.OnCollapseProject()}>
                    <i className="zmdi zmdi-chevron-down"></i>
                        
                    </span>
                        :
                        <span className='mr-1 cursor-pointer' onClick={() => this.OnCollapseProject()}>
                    
                        <i className="zmdi zmdi-chevron-right"></i>
                        </span>
                    }
               
            </React.Fragment>
        )
    }
    
}
function TableComponent({row,labelId}) {
    const [collapse,setcollapse] = React.useState(false);
    const OnCollapseProject=()=>{
       setcollapse(!collapse)
    }
        return (
            <React.Fragment>
              <TableRow
              hover
              className = 'cursor-pointer'
              key={row.name}
              align={'left'}
              >
              
              <TableCell onClick = {OnCollapseProject}   align={'left'} component="th" id={labelId} scope="row" >
              <div className="d-contents">
              <Componenttable value = {row.status} handleCollapse = {OnCollapseProject}/>  
                  {row.orderId}
                  
              
              </div>
                  
              </TableCell>
              <TableCell align="left">{row.seller}</TableCell>
              <TableCell onClick = {OnCollapseProject} align="left">
              {row.amount}
              </TableCell>
              <TableCell onClick = {OnCollapseProject} align="left">{row.status}</TableCell>
              

              </TableRow>
              <CollapseComponent collapse={collapse} value={row} />
          </React.Fragment>
        );
    
}



export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [collapse,setcollapse] = React.useState(false);

  const handleRequestSort = (event, property) => {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  };
 const OnCollapseProject=()=>{
    setcollapse(!collapse)
 }
  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = name => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);


  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        
        <div className={classes.tableWrapper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return<TableComponent row={row} labelId={labelId}/>
                })}
              
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'previous page',
          }}
          nextIconButtonProps={{
            'aria-label': 'next page',
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      
    </div>
  );
}

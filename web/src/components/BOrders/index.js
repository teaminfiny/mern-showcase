import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { changeTab } from '../../actions/tabAction';
import {connect} from 'react-redux';
// import OrderHistory from './openOrder';
import { withRouter } from 'react-router-dom'
import moment from 'moment';

import { getOrderList, getOrderHistoryList } from 'actions/buyer';

import Order from '../../components/orderTable'
import CustomScrollbars from 'util/CustomScrollbars';

function TabContainer({children, dir,index,value}) {
  return (
    <div dir={dir}>
      {value==index&&children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class FullWidthTabs extends Component {
  state = {
    value: 0,
    tabValue:0,

    page:1,
    perPage: 50,
    filter: "",
    month:moment().format("MMMM"),
    year: moment().format("GGGG"),
    show: false
  };

  handleChange = (event, value) => {
    this.setState({tabValue:value, show:true})
  };

  handleChangeIndex = index => {
    this.setState({tabValue:index})
  };

  componentDidMount(){ 

    const {page, perPage, filter, month, year} = this.state;

    let data = {page, perPage, filter};

    this.props.getOrderList({history:this.props.history, data});

    this.props.getOrderHistoryList({history:this.props.history, data});

  }


  render() {


    const {orderList, orderHistoryList} = this.props;


    // console.log("NEWWWWWWWW orderList", orderList);


    const {theme,tabFor} = this.props;
    const { tabValue } = this.state
    return (
      // style={{boxShadow :'0px 2px 4px -1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)'}}
      <div className="w-100" >
        <AppBar position="static" color="default">
          <Tabs
            value={tabValue}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            scrollButtons="on"
          >
            <Tab className="tab" label="Open"/>
            <Tab className="tab" label="History"/>
          </Tabs>

        </AppBar>       

        

          {/* <div className="jr-card jr-full-card ">
            <div className={`jr-card-header d-flex align-items-center reviewSellerCard`}>
              <CustomFilter
                onFilterChange={this.onFilterChange}
                handleApplyFilter={this.handleApplyFilter}
                applyFilter={applyFilter}
                handleResetFilter={this.handleResetFilter}
                month={this.state.month}
                year={this.state.year}
                filterFor='buyerOrderHistory' />
            </div>
          </div> */}



          <TabContainer dir={theme.direction} value = {tabValue} index= {0}>

            <CustomScrollbars className="messages-list scrollbar" style={{ height: 650 }}>
              <Order
                history={this.props.history}
                orderFromParent={orderList}
                value={this.state}
                identifier={'orderList'}
                show={this.state.show}
              />
            </CustomScrollbars>
          </TabContainer>

          <TabContainer dir={theme.direction} value = {tabValue} index= {1}>

            <CustomScrollbars className="messages-list scrollbar" style={{ height: 650 }}>

              {/* <OrderHistory
                history={this.props.history}
                orderFromParent={orderHistoryList}
              /> */}

              <Order
                history={this.props.history}
                orderFromParent={orderHistoryList}
                value={this.state}
                identifier={'orderHistoryList'}
                show={this.state.show}
              />

            </CustomScrollbars>

          </TabContainer>

      
      </div>
    );
  }
}

FullWidthTabs.propTypes = {
  theme: PropTypes.object.isRequired,
};

// const mapStateToProps = ({tabAction}) => {
//   const {tabValue} = tabAction;
//   return { tabValue}
// };

// export default connect(mapStateToProps, {changeTab})( withStyles(null, {withTheme: true})(FullWidthTabs));

const mapStateToProps = ({tabAction,buyer}) => {
  const {tabValue} = tabAction;
  const { orderList, orderHistoryList } = buyer;
  return { tabValue, orderList, orderHistoryList}
};

export default withRouter(connect(mapStateToProps, {changeTab, getOrderList, getOrderHistoryList})( withStyles(null, {withTheme: true})(FullWidthTabs)));
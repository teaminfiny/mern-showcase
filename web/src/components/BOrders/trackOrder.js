import React, { Component } from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import WithIconTimeLineItem from 'components/timeline/WithIconTimeLineItem';
import { Accessible, NewReleases, DirectionsTransit, CreditCard } from '@material-ui/icons';
import timeLineData from 'app/routes/timeLine/routes/timeLineData';


function getSteps() {
    return ['Pending', 'Confirmed', 'Processing', 'Shipped', 'Delivered'];
  }
class TrackOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
          activeStep: 3
        }
      }
      render() {
        const steps = getSteps();
        const { activeStep } = this.state;
        return ( 
            <React.Fragment>
                <Stepper style={{ padding: '24px 0' }} activeStep={activeStep} alternativeLabel className="horizontal-stepper-linear">
                    {steps.map((label, index) => {
                    return (
                        <Step style={{ padding: 0 }} key={label} className={`horizontal-stepper ${index === activeStep ? 'active' : ''}`}>
                        <StepLabel className="stepperlabel">{label}</StepLabel>
                        </Step>
                    );
                    })}
                </Stepper>
                <div className="timeline-section timeline-center clearfix animated slideInUpTiny animation-duration-3" >
                    <WithIconTimeLineItem timeLine={timeLineData[0]} color="red">
                    <NewReleases />
                    </WithIconTimeLineItem>
                    <WithIconTimeLineItem styleName="timeline-inverted" timeLine={timeLineData[1]} color="blue">
                    <CreditCard />
                    </WithIconTimeLineItem>
                    <WithIconTimeLineItem timeLine={timeLineData[2]} color="yellow">
                    <Accessible />
                    </WithIconTimeLineItem>
                    <WithIconTimeLineItem styleName="timeline-inverted" timeLine={timeLineData[3]} color="amber">
                    <DirectionsTransit />
                    </WithIconTimeLineItem>
                    <WithIconTimeLineItem timeLine={timeLineData[4]} color="green">
                    {/* <DirectionsBus /> */}
                    <div>
                        <i style={{ fontSize: '1.5rem',paddingTop:14 }} class="zmdi zmdi-truck"></i>
                    </div>
                    </WithIconTimeLineItem>
                </div>
            </React.Fragment>
         );
    }
}
 
export default TrackOrder;
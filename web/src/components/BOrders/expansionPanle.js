import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Button from '@material-ui/core/Button';
import {Table} from 'reactstrap'
import Avatar from '@material-ui/core/Avatar';
let counter = 0;

function createData(name, image, shoppedNumber) {
  counter += 1;
  return { id: counter, name, image, shoppedNumber };
}
const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
  },
  column: {
    flexBasis: '33.3%',
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.text.lightDivider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  link: {
    color: theme.palette.primary[500],
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
});

function DetailedExpansionPanel(props) {
  const {classes} = props;
  const data =  [
    createData('John Smith', require('assets/productImage/user.jpg'), '3'),
    createData('Priti', require('assets/productImage/user2.jpg'), '1'),
    createData('Kshitija ', require('assets/productImage/user3.jpg'), '0'),
    createData('Praful', require('assets/productImage/user4.jpg'), '5'),
    createData('Avanish', require('assets/productImage/images.png'), '0'),
    createData('Abhidnya', require('assets/productImage/user2.jpg'), '12'),
    createData('Lekhraj', require('assets/productImage/user.jpg'), '6'),
    createData('Ajay', require('assets/productImage/images.png'), '9'),
    createData('Eldhose', require('assets/productImage/user4.jpg'), '3'),
  ]
  const teamList = [
    createData('John Smith', require('assets/productImage/user.jpg'), '3'),
    createData('Priti', require('assets/productImage/user2.jpg'), '1'),
    createData('Kshitija ', require('assets/productImage/user3.jpg'), '0'),
    createData('Praful', require('assets/productImage/user4.jpg'), '5'),
  ]
  return (
    <div className={classes.root}>
      <Table>
        <tbody>
        {data.map(data => {
            return (
              <tr
                tabIndex={-1}
                key={data.id}>
                <td>
                  
                    <Avatar
                      alt={data.name}
                      src={data.image}
                      className="user-avatar"
                    />
                    </td>
                    <td>
                    <div className="user-detail">
                      <h5 className="user-name pb-1">{data.name} </h5>
                      
                      
                    </div>
                  
                </td>
                <td>
                Test Products
                </td>
                <td>
                &#8377; 1000
                </td>
               
              </tr>
            );
          })}
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan='3' className='justify-content-end'>
            <ExpansionPanelActions>
          <Button size="small">Cancel</Button>
          <Button size="small" color="primary">
            Save
          </Button>
        </ExpansionPanelActions>
            </td>
            </tr>
        </tbody>
        </Table>
        
      
    </div>
  );
}

DetailedExpansionPanel.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DetailedExpansionPanel);
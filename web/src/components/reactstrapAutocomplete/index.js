import React, { Component } from 'react';
import {Typeahead,AsyncTypeahead} from 'react-bootstrap-typeahead';
class AutoComplete extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            emptyLabel:true,
            selectHintOnEnter:true,
            minLength:2
         }
    }
    render() {
        const {
          disabled,
          dropup,
          emptyLabel,
          flip,
          highlightOnlyResult,
          minLength,
          open,
          selectHintOnEnter,
        } = this.state;
    
        const checkboxes = [
          /* eslint-disable max-len */
          {checked: disabled, children: 'Disable the input', name: 'disabled'},
          {checked: dropup, children: 'Dropup menu', name: 'dropup'},
          {checked: flip, children: 'Flip the menu position when it reaches the viewport bounds', name: 'flip'},
          {checked: !!minLength, children: 'Require minimum input before showing results (2 chars)', name: 'minLength'},
          {checked: emptyLabel, children: 'Hide the menu when there are no results', name: 'emptyLabel'},
          {checked: selectHintOnEnter, children: 'Select the hinted result by pressing enter', name: 'selectHintOnEnter'},
          {checked: highlightOnlyResult, children: 'Highlight the only result', name: 'highlightOnlyResult'},
          {checked: !!open, children: 'Force the menu to stay open', name: 'open'},
          /* eslint-enable max-len */
        ];
        const options = [{name:'Data'},{name:'Data1'},{name:'Data2'},{name:'Data3'}]
        return (
          <React.Fragment>
            <Typeahead
              {...this.state}
              emptyLabel={emptyLabel ? 'No search result found' : undefined}
              labelKey="name"
              useCache= {true}
              options={options}
              onChange = {(e)=>this.handleChange(e)}
              onInputChange = {(e)=>this. onInputChange(e)}
              placeholder=""
            />
            
          </React.Fragment>
        );
      }
      onInputChange = (e)=>{
        console.log('AutoComplete onInputChange',e)
      }
      handleChange = (e)=>{
        console.log('AutoComplete',e)
        this.props.handleChange(e,this.props.key)
      }
      _handleChange = (e) => {
        const {checked, name} = e.target;
        const newState = {[name]: checked};
    
        switch (name) {
          case 'minLength':
            newState[name] = checked ? 2 : 0;
            break;
          case 'open':
            newState[name] = checked ? true : undefined;
            break;
          default:
            break;
        }
    
        this.setState(newState);
      }
}
 
export default AutoComplete;
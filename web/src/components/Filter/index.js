import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { Col, Row, Label, Input } from 'reactstrap';
import { FormGroup } from '@material-ui/core';
import { getOrderHistory } from '../../actions/order'
import { getSettlementList } from '../../actions/seller'
import {getTransactionList} from '../../actions/buyer'
import {getMediWallet} from '../../actions/buyer'
import {getOrderHistoryList, getOrderList, getProductRequestList, getShortbook} from '../../actions/buyer'
import { withRouter } from 'react-router-dom'
import { getOrder } from '../../actions/order';

import { connect } from 'react-redux';
import helperFunction from '../../constants/helperFunction'
const monthArray = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

let date = new Date();

class CustomFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      month: '',
      year: 0,
      yearList: []
    }
  }

  /*  componentWillReceiveProps = (nextProps, nextState) => {
     console.log('nextProps ',nextProps.month, nextProps.year, this.props.month, this.props.month !== nextProps.month)
    if(nextProps.month && (this.props.month !== nextProps.month) || (nextProps.year && (this.props.year !== nextProps.year) )) {
      this.setState({month :nextProps.month, year : nextProps.year })
    }
   } */

  componentDidMount = () => {
    this.setState({ month: this.props.month ? this.props.month : monthArray[date.getMonth() - 1], year: this.props.year ? this.props.year : date.getFullYear() })

    let years = helperFunction.yearList();
    this.setState({ yearList: years })
  }

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value });
    this.props.onFilterChange(e, key);
  }

  handleClose = e => {
    e.preventDefault();
    this.props.handleResetFilter(e, this.props.applyFilter)
  }
  handleDownload = e => {
    e.preventDefault();
    this.props.handleDownload(e, this.props.applyFilter)
  }
  handleSubmit = e => {
    e.preventDefault();
    if (this.props.filterFor !== 'buyerOrderHistory') {
    this.props.applyFilter();
    }
    if (this.props.filterFor === 'buyerOrderHistory'){
      this.props.handleApplyFilter();
      this.props.applyFilter();
    }
    let data = {
      page: 1,
      perPage: 50,
      filter: '',
      searchText:this.props.searchText ? this.props.searchText :'',
      month: this.state.month,
      year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
    }
    if (this.props.filterFor === 'buyerProductRequestHistory' || this.props.filterFor === 'shortbookFulfilled') {
      data.tab = 'history'
    }
    if (this.props.filterFor === 'buyerOpenProductRequest' || this.props.filterFor === 'shortbookList') {
      data.tab = 'open'
    }
    if (this.props.filterFor === 'settlement') {
      this.props.getSettlementList({ data, history: this.props.history })
    } 
    else if (this.props.filterFor === 'transaction') {
      this.props.getTransactionList({ data, history: this.props.history })
    }
    else if (this.props.filterFor === 'mediWallet') {
      this.props.getMediWallet({ data, history: this.props.history })
    }
    else if (this.props.filterFor === 'buyerOrderHistory') {
      this.props.getOrderHistoryList({ data, history: this.props.history })
    }
    else if (this.props.filterFor === 'buyerOpenOrders') {
      this.props.getOrderList({ data, history: this.props.history })
    }
    else if (this.props.filterFor === 'sellerOpenOrder') {
      this.props.getOrder({ data, history: this.props.history })
    }
    else if (this.props.filterFor === 'buyerOpenProductRequest' || this.props.filterFor === 'buyerProductRequestHistory') {
      this.props.getProductRequestList({ data, history: this.props.history })
    }
    else if (this.props.filterFor === 'shortbookFulfilled' || this.props.filterFor === 'shortbookList') {
      this.props.getShortbook({ data, history: this.props.history })
    }
    else {
      this.props.getOrderHistory({ data, history: this.props.history })
    }
  }

  render() {
    let { month, year, yearList } = this.state;
    return (
      <React.Fragment>
        <Row form>
          <Col md={6} xl={6} xs={12} sm={12} lg={6}>
            <FormGroup>
              <Label for="exampleSelect">Select Month</Label>
              <Input type="select" name="month" value={month} onChange={(e) => this.handleChange(e, 'month')} id="month">
                {(this.props.filterFor == 'sellerOpenHistoryOrder' || this.props.filterFor == 'sellerOpenOrder') ? <option value= ''>All</option> : ''}
                {
                  monthArray.map((value, key) => {
                    return <option value={value} >{value}</option>;
                  })
                }
              </Input>
            </FormGroup>
          </Col>
          <Col md={6} xl={6} xs={12} sm={12} lg={6}>
            <FormGroup>
              <Label for="exampleSelect">Select Year</Label>
              <Input type="select" value={year} onChange={(e) => this.handleChange(e, 'year')} name="year" id="year">
              {(this.props.filterFor == 'sellerOpenHistoryOrder' || this.props.filterFor == 'sellerOpenOrder') ? <option value= ''>All</option> : ''}
                {
                  yearList.map((value, key) => {
                    return <option value={parseInt(value)} >{value}</option>
                  })
                }
                {/* <option value="2015" >2015</option>
                <option value="2016" >2016</option>
                <option value="2017" >2017</option>
                <option value="2018" >2018</option>
                <option value="2019" >2019</option> */}
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <div style={{ paddingTop: 15 }} className="row" >
          <Col md={6} xl={6} xs={12} sm={12} lg={6}>
            <Button variant="contained" className='filterButton' onClick={(e) => this.handleSubmit(e)} color='primary'>Apply</Button>
          </Col>
          <Col md={6} xl={6} xs={12} sm={12} lg={6} className="text-nowrap">
            <Button variant="contained" onClick={(e) => this.handleClose(e)} className='filterButton' color='primary'>Reset</Button>
          </Col>
        </div>
        {this.props.filterFor === 'mediWallet' ?
        <div style={{ paddingTop: 15 }} className="row" >
          <Col md={12} xl={12} xs={12} sm={12} lg={12} className="text-nowrap">
            <Button variant="contained" onClick={(e) => this.handleDownload(e)} className='filterButton' color='primary'>Download CSV</Button>
          </Col>
        </div>
        : null }
      </React.Fragment>
    );
  }
}

export default  withRouter(connect(null, { getOrderHistory, getSettlementList, getTransactionList, getMediWallet, getOrderHistoryList, getOrder, getOrderList, getProductRequestList, getShortbook })(CustomFilter));
import React from 'react';
import Button from '@material-ui/core/Button';


const handleSeller = (history)=>{
  history.push('/view-seller')
}
const ProductGridItem = ({product,history,classValue,showAddToCard}) => {
  const {thumb, name, price, mrp, offer,seller} = product;
  return (
    <div className={classValue}>
      <div className="card product-item">
        <div className="card-header border-0 p-0">
          <div className="card-image" >
            <div className="grid-thumb-equal">
              <span className="grid-thumb-cover jr-link">
                <img alt="Remy Sharp" src={thumb}/>
              </span>
            </div>
          </div>
        </div>
        <div className="card-body">
          <div className="product-details">
            <h2 className="card-title font-weight-bold">{name}
            {
              showAddToCard&&
              <small className="text-grey text-darken-2">{", by " }<a href='javascript:void(0);' onClick={()=>handleSeller(history)}>{seller}</a></small>
            }
            </h2>
            {
              showAddToCard&&
            <div className="d-flex ">
              <h3 className="card-title">{price} </h3>
              <h5 className="text-muted px-2">
                <del>{mrp}</del>
              </h5>
              <h5 className="text-success">{offer} off</h5>
            </div>
            }
            
           
          </div>
          {
            showAddToCard&&
            <div>
            <Button  color="primary" variant="contained" className="jr-btn jr-btn-sm btnPrimary">
              <i className="zmdi zmdi-shopping-cart"/>
              <span>Add to Cart</span>
            </Button>
          </div>
          }
          
        </div>
      </div>
    </div>
  )
};

export default ProductGridItem;


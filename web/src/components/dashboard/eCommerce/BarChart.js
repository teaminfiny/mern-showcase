import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import AxiosRequest from 'sagas/axiosRequest'
import {connect} from 'react-redux';
import {showAuthMessage} from 'actions'
am4core.useTheme(am4themes_animated);

class BarChart extends Component {
  componentDidMount=async()=> {
    var chart = am4core.create("chartdiv1", am4charts.XYChart);
    chart.paddingRight = 20;
    chart.responsive.enabled = true;
    let response = await AxiosRequest.axiosHelperFunc('get','seller/getGraphForDahboard','','')
    if(response.data && response.data.error){
      this.props.showAuthMessage(response.data.title)
      
    }else{
    // Add data
    chart.data = response.data ? response.data.detail : []
  }
    // Create axes

    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "month";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;

    categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
      if (target.dataItem && target.dataItem.index & 2 == 2) {
        return dy + 25;
      }
      return dy;
    });

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
   /*  valueAxis.renderer.grid.template.strokeOpacity = 0;
    valueAxis.min = 0;
    valueAxis.max = 100;
    valueAxis.strictMinMax = true; */
    // Create series
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "orders";
    series.dataFields.categoryX = "month";
    series.name = "orders";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fillOpacity = .8;

    let columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;

    // Add cursor
    chart.cursor = new am4charts.RadarCursor();

  }

  /* componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  } */

  render() {
    return (
      <div id="chartdiv1" style={{ width: "100%", height: "300px" }} />
    );
  }
}

export default connect(null,{showAuthMessage}) (BarChart);
import React from 'react';
import Avatar from '@material-ui/core/Avatar';

class OrderTableCell extends React.Component {

  onOptionMenuSelect = event => {
    this.setState({ menuState: true, anchorEl: event.currentTarget });
  };
  handleRequestClose = () => {
    this.setState({ menuState: false });
  };

  constructor() {
    super();
    this.state = {
      anchorEl: undefined,
      menuState: false,
    }
  }

  render() {
    const { id, buyerName, orders, amount, date, status } = this.props.data;
    const statusStyle = status.includes("delivered") ? "text-white bg-success" : status.includes("Ready for dispatch") ? "text-white bg-amber" : status.includes("Placed") ? "text-white bg-primary" : "text-white bg-danger";
    return (
      <tr
        tabIndex={-1}
        key={id}
      >
        <td>
          <div className="user-profile d-flex flex-row align-items-center">
            <Avatar
              alt={buyerName}
              src={require('assets/productImage/avatar.png')}
              className="user-avatar"
            />
            <div className="user-detail">
              <h5 className="user-name">{buyerName} </h5>
            </div>
          </div>
        </td>
        <td>{orders}</td>
        <td>{amount}</td>
        <td>{date}</td>
        <td className="status-cell text-left">
          <div className={` badge text-uppercase ${statusStyle}`}>{status}</div>
        </td>
      </tr>

    );
  }
}

export default OrderTableCell;

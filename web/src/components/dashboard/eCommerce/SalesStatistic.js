import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getDashboardSalesStats } from '../../../actions/order'
import {withRouter} from 'react-router-dom'
import {
  BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';




class SalesStatistic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      graphData: []
    };
  }

  componentDidMount () {
    this.props.getDashboardSalesStats({history : this.props.history});
  }

  componentDidUpdate = async(prevProps, prevState)=> {
    if(prevProps.statsData !== this.props.statsData){
      const { statsData } = this.props;
      this.setState({ graphData: statsData.montlyRevenueOrders });  
    }
  }

  render() {
    let { statsData } = this.props;
    const {graphData} = this.state;

    let data = [
      { Month: 'Apr', Orders: 0, },
      { Month: 'May', Orders: 0, },
      { Month: 'Jun', Orders: 0, },
      { Month: 'Jul', Orders: 0, },
      { Month: 'Aug', Orders: 0, },
      { Month: 'Sep', Orders: 0, },
      { Month: 'Oct', Orders: 0, },
      { Month: 'Nov', Orders: 0, },
      { Month: 'Dec', Orders: 0, },
      { Month: 'Jan', Orders: 0, },
      { Month: 'Feb', Orders: 0, },
      { Month: 'Mar', Orders: 0, },
    ];

    graphData && graphData.sort(function(a, b) {
      return parseFloat(a._id) - parseFloat(b._id);
    }).map((dataOne, index) => {
    if (dataOne._id == '04') { 
    data[0] = { Month: 'Apr', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '05') { 
    data[1] = { Month: 'May', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '06') { 
    data[2] = { Month: 'Jun', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '07') { 
    data[3] = { Month: 'Jul', Orders: dataOne.orders, Amount: ((dataOne.amount).toFixed(2)) } }
    if (dataOne._id == '08') { 
    data[4] = { Month: 'Aug', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '09') { 
    data[5] = { Month: 'Sep', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '10') { 
    data[6] = { Month: 'Oct', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '11') { 
    data[7] = { Month: 'Nov', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '12') { 
    data[8] = { Month: 'Dec', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if(dataOne._id == '01'){ 
    data[9] = { Month: 'Jan', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2)} }
    if (dataOne._id == '02') { 
    data[10] = { Month: 'Feb', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    if (dataOne._id == '03') { 
    data[11] = { Month: 'Mar', Orders: dataOne.orders, Amount: (dataOne.amount).toFixed(2) } }
    })

    let n1;
    let maxValue;
    graphData && graphData.sort(function (a, b) {
      return parseFloat(a.amount) - parseFloat(b.amount);
    }).map((dataOne, index) => {
      n1 = dataOne.amount / 2;
      maxValue = parseInt(dataOne.amount + n1);
    })

    return (
      <div className="jr-card">
        <div className="row">

          <div className="jr-card-header d-flex align-items-center col-6 col-sm-6 col-md-6 col-lg-6">
            <h3 className="mb-0">Sales Statistic</h3>
          </div>

          {/* <div className="jr-card-header dashboardInvoice d-flex  col-6 col-sm-6 col-md-6 col-lg-6">
            <Button className={'text-primary mb-0'} > Last Invoice </Button>
          </div>
 onClick={() => this.handleClick('add')}  */}
        </div>

        <div className="row mb-3">

          <div className="col-6 col-sm-4 col-md-3 col-lg-2">
            <span className="d-flex align-items-center mb-2">
              <i className="zmdi zmdi-alert-triangle text-muted chart-f20" />
              <span className="ml-3 text-dark">{statsData ? statsData.pendingOrders : 0}</span>
            </span>
            <p className="text-muted">Pending Orders</p>
          </div>

          <div className="col-6 col-sm-4 col-md-3 col-lg-2">
            <span className="d-flex align-items-center mb-2">
              <i className="zmdi zmdi-calendar-note text-muted chart-f20" />
              <span className="ml-3 text-dark">{statsData ? statsData.monthlyOrders : 0}</span>
            </span>
            <p className="text-muted">Monthly Orders</p>
          </div>

          <div className="col-6 col-sm-4 col-md-3 col-lg-2">
            <span className="d-flex align-items-center mb-2">
              <i className="zmdi zmdi-shopping-cart text-muted chart-f20" />
              <span className="ml-3 text-dark">₹{statsData ? parseInt(statsData.monthlySales).toLocaleString('en-IN') : 0}</span>
            </span>
            <p className="text-muted">Monthly Sales</p>
          </div>

          <div className="col-6 col-sm-4 col-md-3 col-lg-2">
            <span className="d-flex align-items-center mb-2">
              <i className="zmdi zmdi-truck text-muted chart-f20" />
              <span className="ml-3 text-dark">{statsData ? statsData.totalOrders : 0}</span>
            </span>
            <p className="text-muted">Total Orders</p>
          </div>

          <div className="col-6 col-sm-4 col-md-3 col-lg-2">
            <span className="d-flex align-items-center mb-2">
              <i className="zmdi zmdi-chart text-muted chart-f20" />
              <span className="ml-3 text-dark">₹{statsData ? parseInt(statsData.totalSales).toLocaleString('en-IN') : 0}</span>
            </span>
            <p className="text-muted">Total Sales</p>
          </div>

          {/* <div className="col-6 col-sm-4 col-md-3 col-lg-2">
            <span className="d-flex align-items-center mb-2">
              <i className="zmdi zmdi-money-box text-muted chart-f20" />
              <span className="ml-3 text-dark">&#x20B9;{statsData ? statsData.monthlyOrders : 0}pan>
            </span>
            <p className="text-muted">Total Revenue</p>
          </div> */}

        </div>

        <div className="row">
          <div className="col-lg-12 col-12 mb-12 mb-lg-12">
            <ResponsiveContainer width="100%" height={350}>
            
            {/* <BarChart
              width="100%"
              height={300}
              data={data}
              margin={{
                top: 5, right: 65, left: 20, bottom: 5,
              }}              
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="Month" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="Orders" fill="#193298" />
            </BarChart> */}
            <BarChart
                width={500}
                height={400}
                data={data}
                margin={{
                  top: 20, right: 30, left: 20, bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Month" />
                <YAxis yAxisId="left" domain={[0, 1000]}orientation="left" stroke="#193298" />
                <YAxis yAxisId="right" domain={[0, maxValue]} orientation="right" stroke="#f0a500" />
                <Tooltip formatter={(value) => new Intl.NumberFormat('en-IN').format(value)}/>
                <Legend />
                <Bar yAxisId="left" dataKey="Orders" fill="#193298" />
                <Bar yAxisId="right" dataKey="Amount" fill="#f0a500" />
              </BarChart>

              {/* <BarChart /> */}
              {/* <AreaChart data={salesStatisticData}
                margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
                <XAxis dataKey="name" />
                <YAxis type="number" domain={[0, 26000]} />
                <CartesianGrid strokeDasharray="0" stroke="#DCDEDE" />
  
                <Tooltip />
                <defs>
                  <linearGradient id="salesStatistic" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#4258BC" stopOpacity={1} />
                    <stop offset="95%" stopColor="#FFF" stopOpacity={0.8} />
                  </linearGradient>
                </defs>
  
                <Area type='monotone' dataKey='uv' strokeWidth={2} stroke='#6F82E5' fill="url(#salesStatistic)" />
              </AreaChart> */}
            </ResponsiveContainer>
          </div>
          {/* <div className="col-lg-5 col-12">
            <ResponsiveContainer width="100%">
              <SalesGauge />
            </ResponsiveContainer>
          </div> */}
        </div>
      </div>
    );
  }
};

const mapStateToProps = ({ order }) => {
  let { lastWeekRevenueOrders,
    lastThirtyDaysRevenueOrders,
    totalRevenueOrders,
    montlyRevenueOrders,
    statsData,
   } = order;
  return {
    lastWeekRevenueOrders,
    lastThirtyDaysRevenueOrders,
    totalRevenueOrders,
    montlyRevenueOrders,
    statsData
  }
}

export default connect(mapStateToProps, {getDashboardSalesStats})(withRouter(SalesStatistic));

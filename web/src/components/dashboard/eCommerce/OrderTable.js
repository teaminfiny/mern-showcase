import React, {Component} from 'react';
import OrderTableCell from './OrderTableCell';

let counter = 0;

function createData( user, buyerName, orders, amount, date, status) {
  counter += 1;
  return {id: counter, user, buyerName, orders, amount, date, status};
}

class OrderTable extends Component {
  state = {
    data: [
      createData( 'A' , 'Avanish', 'Combiflame', '10', '22/08/2019','Placed'),
      createData( 'A' , 'Abhidnya', 'Crocine', '20', '22/08/2019', 'Ready for dispatch'),
      createData( 'A' , 'Priti', 'Saradon', '30', '22/08/2019', 'delivered'),
      createData( 'A' , 'Lekhraj', 'Zandu Balm', '50', '22/08/2019', 'Ready for dispatch'),
    ],
  };

  render() {
    const {data} = this.state;
    return (
      <div className="table-responsive-material">
        <table className="default-table table-unbordered table table-sm table-hover">
          <thead className="th-border-b">
          <tr>
            <th>User</th>
            <th>Order</th>
            <th>Amount($)</th>
            <th>Date</th>
            <th className="status-cell text-left">Status</th>
            <th/>
          </tr>
          </thead>
          <tbody>
          {data.map(data => {
            return (
              <OrderTableCell key={data.id} data={data}/>
            );
          })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default OrderTable;
import React, { Component } from 'react';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { Carousel, CarouselItem, Col, Row } from 'reactstrap';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import { NavLink, withRouter } from 'react-router-dom';
import AppConfig from 'constants/config'
import './index.css'
import moment from 'moment'
import {
  getProductDetails,
} from 'actions/buyer';
import { connect } from 'react-redux';
import Tooltip from '@material-ui/core/Tooltip';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import CartPopOver from './CartPopOver'
import helpertFn from 'constants/helperFunction';
import Fab from '@material-ui/core/Fab';
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';

class ListCard extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeIndex: 0,
      popoverOpen: false
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next(size) {
    if (this.animating) return;
    if (size === 1) {
      this.setState({ activeIndex: 0 });
    }
    else if (this.state.activeIndex == 0) {
      this.setState({ activeIndex: this.state.activeIndex + 1 });
    }
  }

  previous(size) {
    if (this.animating) return;
    if (size === 1) {
      this.setState({ activeIndex: 0 });
    }
    else if (this.state.activeIndex == 1) {
      this.setState({ activeIndex: this.state.activeIndex - 1 });
    }
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  handleDelete = () => {

  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  handleClick = () => {
  }

  handleNavClick = (e, link) => {
    this.props.history.push(link)
    this.props.getProductDetails({ inventory_id: this.props.product.inventory_id })
  }

  addDefaultSrc(ev) {
    ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
  }
  closepop = () => {
    this.setState({ popoverOpen: false })
  }
  render() {

    const { product, identifier } = this.props

    const { activeIndex } = this.state

    const discount = this.props.product && this.props.product.Discount && this.props.product.Discount;

    const otherProduct = this.props.product ? this.props.product.OtherProducts ? this.props.product.OtherProducts : '' : '';

    const inventoryID = product && product && product.inventory_id;

    const chemCombination = this.props.product && this.props.product.Product && this.props.product.Product.chem_combination

    const productMRP = this.props.product && this.props.product.MRP

    const productName = this.props.product && this.props.product.Product && this.props.product.Product.name && ((this.props.product.Product.name).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()
    const bgColor = product && product.medi_type === 'Ethical branded' || product.medi_type === 'Others' ? '#ff7000' : product.medi_type === 'Cool chain' ? '#0b68a8' :
      product.medi_type === 'Surgical' || product.medi_type === 'OTC' || product.medi_type === 'Generic' ? '#038d0e' : '#072791'

    const slides = product ? product ? product.Product ? product.Product.images.length > 0 ? product.Product.images.map((image, index) => {
      return (
        <CarouselItem
          key={index}
        >
          <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`}
            onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
            <img src={product.Product.images.length === 0 ? logo : `${helpertFn.productImg(image)}`}
              onError={this.addDefaultSrc} />
            {/* <CarouselCaption captionText={image} imageHeader={image} />
           */}
          </NavLink>
        </CarouselItem>
      )
    })
      :
      [<CarouselItem
        key={"index"}
      >
        <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
          <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
        </NavLink>
      </CarouselItem>]
      :
      [<CarouselItem
        key={"index"}
      >
        <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
          <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
        </NavLink>
      </CarouselItem>]
      :
      [<CarouselItem
        key={"index"}
      >
        <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
          <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
        </NavLink>
      </CarouselItem>]
      :
      [<CarouselItem
        key={"index"}
      >
        <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
          <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
        </NavLink>
      </CarouselItem>]


    let a = this.getRandomInt(3)
    let icon = this.props.index % 2 === 0 ? <Avatar className="bg-danger iconAvtarColor text-white">
      <ThumbDownIcon style={{ fontSize: '0.6rem' }} /></Avatar> : <Avatar className="bg-success text-white iconAvtarColor">
      <ThumbUpIcon style={{ fontSize: '0.6rem' }} /></Avatar>

    return (
      <React.Fragment>
      {/* { isMobile === false ?  */}
        <div class='flex-con jr-card p-0' style={{maxHeight:'170px'}}>
          <div style={{ maxWidth: '20%' }}>
            <div style={{ position: 'relative', display: 'inline-flex', overflow: 'hidden',maxHeight: '100%' }}>
              <Carousel
                autoPlay={false}
                indicators={true}
                activeIndex={activeIndex}
                next={() => this.next(slides.length)}
                interval={false}
                previous={() => this.previous(slides.length)}
                className='itemImage'>
                {slides}

              </Carousel>

              <span style={{ padding: '2px', fontWeight: 'bold', position: 'absolute', zIndex: 1, backgroundColor: `${bgColor}`, color: 'white', width: 'auto', maxHeight:'40px' }} className='itemImage' >{product && product.medi_type === 'Others' ? 'PCD' : product.medi_type}</span>

            </div>
          </div>
          <div style={{ width: '80%' }} className='pl-4'>
            <Row>
              <Col md={6} xl={6} lg={6} sm={12} xs={6}>
                <div className='pt-2'>
                {
                this.props.product && this.props.product.Product && this.props.product.Product.name.length > 40 ?
                  <Tooltip
                    className="d-inline-block"
                    id="tooltip-right"
                    title={
                      <h6 className="text-white"
                        style={{ marginTop: "5px" }}>
                        {this.props.product && this.props.product.Product && this.props.product.Product.name}
                      </h6>
                    }
                    placement="right"
                    className="ellipsis">

                    <NavLink className="ellipsis" className="buyerRedirectNavlink" to={`/product-details/${productName}/${inventoryID}`}
                      onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${inventoryID}`)}>
                      <h2 className="card-title card-titleIntranet" style={{ minHeight: "20px", maxHeight: "20px", overflow: "hidden", marginTop: "5px" }}>
                        {(this.props.product && this.props.product.Product && this.props.product.Product.name) + "..."}
                      </h2>
                    </NavLink>
                  </Tooltip>
                  :
                  <NavLink className="" className="buyerRedirectNavlink" to={`/product-details/${productName}/${inventoryID}`}
                    onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${inventoryID}`)}>
                    <h2 className="card-title card-titleIntranet" style={{ minHeight: "20px", maxHeight: "20px", overflow: "hidden", marginTop: "5px" }}>
                      {this.props.product && this.props.product.Product && this.props.product.Product.name}
                    </h2>
                  </NavLink>
              }
            </div>
            </Col>
            <Col md={6} xl={6} lg={6} sm={12} xs={6}> 
            </Col>
            </Row>
            <Row>
              <Col md={6} xl={6} lg={6} sm={12} xs={6}>
            <div className='pb-2'>
              <NavLink className="buyerRedirectNavlink" to={`/view-seller/${product && product.Seller && product.Seller._id}`}
                style={{ minHeight: "43px" }}>
                <Chip
                  label={(product && product.Seller && product.Seller.company_name.length) > 25 ?
                    (product && product.Seller && product.Seller.company_name).slice(0, 24) + "..." : product && product.Seller && product.Seller.company_name}
                  onClick={this.handleClick}
                  size='small'
                />
              </NavLink>
            </div>
            </Col>
            <Col md={6} xl={6} lg={6} sm={12} xs={6} className='pl-0 pt-1 pr-0'>
            {
              this.props.product && this.props.product.medi_attribute.includes('Jumbo Deal') &&
                <span size='small' className='text-white'
                  style={{backgroundColor:`${bgColor}`, margin: "0px", padding: "3px 5px"}}>
                  Jumbo Deal
                </span>
            }&nbsp;
            {
              helpertFn.showPrepaid(this.props.product.medi_type,this.props.product.Product.isPrepaid && this.props.product.Product.isPrepaid, product.prepaidInven &&product.prepaidInven) && 
                <span className='text-white bg-danger' style={{ margin: "0px", padding: "3px 5px" }}>Only Prepaid</span>
            }
            
            </Col>
            </Row>
            <Row>
              <Col md={6} xl={6} lg={6} sm={12} xs={6}>
                <div>
              <p className={this.props.history.location.pathname === '/view-seller' ? 'pt-1' : ''} style={{ minHeight: "10px", marginBottom: "0px" }}>
                MRP: ₹{productMRP.toFixed(2)}
                
              </p>
            </div>
              </Col>
              <Col md={6} xl={6} lg={6} sm={12} xs={6} className='pl-0'>
              <div className=''>
              Expiry Date: {this.props.product && this.props.product.expiry_date && moment(this.props.product.expiry_date).format('D MMM, YYYY')}
            </div>
              </Col>
            </Row>
            <Row>
              <Col md={6} xl={6} lg={6} sm={12} xs={6}>
                <div>
              {
                chemCombination.length > 35 ?
                  <React.Fragment>

                    <Tooltip
                      className="d-inline-block"
                      id="tooltip-right"
                      title={
                        <h6 className="text-white"
                          style={{ marginTop: "5px" }}>
                          {this.props.product && this.props.product.Product && this.props.product.Product.chem_combination}
                        </h6>
                      }
                      placement="right"
                      className="ellipsis">

                      <p className="ellipsis" className={this.props.history.location.pathname === '/view-seller' ? 'pt-1' : 'pt-1'}
                        style={{ minHeight: "25px", marginBottom: "5px" }}>
                        {chemCombination + "..."}
                      </p>

                    </Tooltip>

                  </React.Fragment>
                  :
                  <p className={this.props.history.location.pathname === '/view-seller' ? 'pt-1' : 'pt-1'} style={{ minHeight: "46px", marginBottom: "5px" }}>
                    {chemCombination}
                  </p>
              }
            </div>
              </Col>
              <Col md={6} xl={6} lg={6} sm={12} xs={6} className='pl-0'>
{/* <div className='mb-1 row'>
              <div className="col-xl-8 col-lg-8 col-md-8 col-sm-8"> */}
                {
                  discount && discount.discount_type === "Same" ?
                    <React.Fragment>
                      <div className='row pl-3'>
                        <span className={'discountPrice'}> Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} Free</span>
                      </div>
                      <div className='row pl-3'>
                        <span className={'priceColor'}>&#x20B9;{(product.ePTR).toFixed(2)}</span>
                        <span style={{ marginTop: '2px' }}>&nbsp;
                          <span className={'originalPrice font'}>&#x20B9;{product.PTR.toFixed(2)}</span>
                          &nbsp;
                          <span className={'discountPrice'} >Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                        </span>

                      </div>

                    </React.Fragment>

                    :
                    discount && discount.discount_type === "SameAndDiscount" ?
                      <React.Fragment>
                        <div className='row pl-3'>
                          <Tooltip
                            className="d-inline-block"
                            id="tooltip-right"
                            title={
                              <span style={{ marginTop: "13px" }}>
                                Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} Free, and {(product.Discount.discount_per).toFixed(2)}% Off
                              </span>
                            }
                            placement="right"
                          >
                            <span className={'discountPrice'}> Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} Free, and {(product.Discount.discount_per).toFixed(2)}% Off</span>
                          </Tooltip>
                        </div>
                        <div className='row pl-3'>
                          <span className={'priceColor'}>&#x20B9;{(product.ePTR).toFixed(2)}</span>&nbsp;
                          <span style={{ marginTop: '2px' }}>&nbsp;
                            <span className={'originalPrice font'}>&#x20B9;{product.PTR.toFixed(2)}</span>&nbsp;
                            <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                          </span>
                        </div>


                      </React.Fragment> :

                      discount && discount.discount_type === "Discount" ?

                        <React.Fragment>
                          <div className='row pl-3'>
                            <span>Get {(product.Discount.discount_per).toFixed(2)}% discount</span>
                          </div>
                          <div className='row pl-3'>
                            <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span>&nbsp;
                            <span style={{ marginTop: '2px' }}>&nbsp;
                              <span className={'originalPrice font'}>₹{(product.PTR).toFixed(2)}</span>&nbsp;
                              <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                            </span>
                          </div>


                        </React.Fragment>

                        :

                        discount && discount.discount_type === "Different" ?
                          <React.Fragment>
                            <div className='row pl-3'>
                              <Tooltip
                                className="d-inline-block"
                                id="tooltip-right"
                                title={
                                  <h6 className="text-white" style={{ marginTop: "13px" }}>
                                    Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} {otherProduct.name} Free
                                  </h6>
                                }
                                placement="right"
                                className="ellipsis">

                                <span className="ellipsis" className={'discountPrice'}>
                                  {("Buy " + discount.discount_on_product.purchase + " Get " + discount.discount_on_product.bonus + " " + otherProduct.name + " Free").slice(0, 25) + "..."}
                                </span>
                              </Tooltip>
                            </div>
                            <div className='row pl-3'>
                              <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                              <span style={{ marginTop: '2px' }}>
                                <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                              </span>
                              {/* </div> */}
                            </div>



                          </React.Fragment>

                          :
                          discount && discount.discount_type === "DifferentAndDiscount" ?

                            <React.Fragment>
                              <div className='row pl-3'>
                                <Tooltip
                                  className="d-inline-block"
                                  id="tooltip-right"
                                  title={
                                    <h6 className="text-white" style={{ marginTop: "13px" }}>
                                      Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} {otherProduct.name} Free, and {(product.Discount.discount_per).toFixed(2)}% Off
                                    </h6>
                                  }
                                  placement="right"
                                  className="ellipsis">

                                  <span className="ellipsis" className={'discountPrice'}>
                                    {("Buy " + discount.discount_on_product.purchase + " Get " + discount.discount_on_product.bonus + " " + otherProduct.name + " Free, and " + (product.Discount.discount_per).toFixed(2) + "% Off").slice(0, 25) + "..."}
                                  </span>
                                </Tooltip>
                              </div>
                              <div className='row pl-3'>
                                <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                                <span className={'discountPrice'} style={{ marginTop: '2px' }}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>

                              </div>

                            </React.Fragment> :

                            discount && discount.discount_type === "" ?

                              <React.Fragment>
                                <div className='row pl-3 mt-3'></div>
                                <div className='row pl-3'>
                                  <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                                  <span style={{ marginTop: '2px' }}>&nbsp;
                                    <span className={'originalPrice font'}>{product.MRP === product.PTR ? null : "₹" + (product.MRP).toFixed(2)}</span>&nbsp;
                                    <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                                  </span>
                                </div>
                              </React.Fragment>

                              :

                              <React.Fragment>
                                <div className='row pl-3 mt-3'></div>
                                <div className='row pl-3'>
                                  <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                                  <span style={{ marginTop: '2px' }}>
                                    &nbsp;
                                    <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                                  </span>
                                </div>
                              </React.Fragment>
                }
              {/* </div>
              </div> */}
              </Col>
              </Row>
            <Row>
            <Col md={8} xl={8} lg={8} sm={12} xs={8} >
              {
              this.props.match.url === `/view-company` ? null : <div><p className="d-flex mb-0 align-items-baseline">
                By
                <NavLink className="buyerRedirectNavlink " to={`/view-company/${product.Company._id}`}>
                  <h5 className="text-primary ml-1" style={{ minHeight: "25px", maxHeight: "25px", overflow: "hidden", marginBottom: "5px" }}>{this.props.product && this.props.product.Company && this.props.product.Company.name}</h5>
                </NavLink>
              </p></div>
            }
            </Col>
            <Col md={4} xl={4} lg={4} sm={12} xs={4} style={{textAlign:'end', marginTop:'-15px'}}>
              <div className='pr-3 pb-2'>
            <Fab className="jr-badge-up" style={{ backgroundColor: `${bgColor}`, color: 'white',width:'40px',height:'40px' }} >
                  <i class="zmdi zmdi-shopping-cart"
                    style={{ float: "right", fontSize: '20px' }}
                    onClick={this.toggle}
                    id={"lc1" + inventoryID + identifier}
                  />
                </Fab>
                <Popover placement="left" isOpen={this.state.popoverOpen} target={"lc1" + inventoryID + identifier} toggle={this.toggle} style={{ padding: "5px", paddingBottom: "0px", textAlign: "center" }}>
                  <PopoverHeader style={{ padding: "4px 4px", textAlign: "center" }}>
                    Select Quantity</PopoverHeader>
                  <CartPopOver closepop={this.closepop}
                    dataFromParent={product}
                  />
                  <PopoverBody style={{ paddingBottom: 0, textAlign: "center" }}>
                  </PopoverBody>
                </Popover></div>
            </Col>
            </Row>
            
            
            
            
            
              {/* <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4" style={{ textAlign: 'end' }}>
                
              </div>
            </div> */}
          

        </div></div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { data, otherSellers, relatedProducts, productData, } = buyer;
  return { data, otherSellers, relatedProducts, productData }
};

export default withRouter(connect(mapStateToProps, { getProductDetails })(ListCard));
const productData = [
  {
    thumb: require('assets/images/0_std.jpg'),
    name: 'Alarm Clock',
    variant: 'Gold ',
    mrp: '$990 ',
    price: '$699 ',
    offer: '29 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 5,
    seller: 'Sony',
    description: "Horo is a home grown brand with utmost emphasis on quality goods to users... ",
  },
  {
    thumb: require('assets/images/5f94b8b5e5565b1d9ff9f455083964829845d50e.jpeg'),
    name: 'Bizinto 1 Three Pin',
    variant: 'White',
    mrp: '$490 ',
    price: '$399 ',
    offer: '29 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 4,
    seller: 'Sony',
    description: "Bizinto is an indirectly manufacture of Power strip in Delhi and supplying...",
  },
  {
    thumb: require('assets/images/030858__30194.1563546477.jpg'),
    name: 'Samons Flameless',
    variant: 'Black',
    mrp: '$49 ',
    price: '$39 ',
    offer: '30 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    seller: 'JBL',
    rating: 3.3,
    description: "Now light your cigarette buds with ease by using this USB Rechargeable...",
  },
  {
    thumb: require('assets/images/tiganex-injection-500x500.jpeg'),
    name: 'Sony MDR-ZX110',
    variant: 'White',
    mrp: '$29 ',
    price: '$15 ',
    offer: '49 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 3.5,
    seller: 'Sachin',
    description: "Experience great sound quality with weight and foldable headphones...",
  },
  {
    thumb: require('assets/images/sep_0112_azithromycininj.jpg'),
    name: 'iPhone 7',
    variant: 'Black,500Htz',
    mrp: '$400 ',
    price: '$359 ',
    offer: '49 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 4.2,
    seller: 'Sachin',
    description: "Bluetooth speaker, Karaoke singing, Car Stereo, instrument recording etc... •",
  },
  {
    thumb: require('assets/images/tiganex-injection-500x500.jpeg'),
    name: 'Stonx v2.1',
    variant: 'Black',
    mrp: '$29 ',
    price: '$15 ',
    offer: '49 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 3.1,
    seller: 'Lekhraj',
    description: "1 Bluetooth Dongle, 1 Aux Cable, 1 Usb Cable, 1 Manual...",
  },
  {
    thumb: require('assets/images/images10.jpeg'),
    name: 'Aqua check',
    variant: 'Black',
    mrp: '$29 ',
    price: '$15 ',
    offer: '49 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 3.1,
    seller: 'Lekhraj',
    description: "1 Bluetooth Dongle, 1 Aux Cable, 1 Usb Cable, 1 Manual...",
  },
  {
    thumb: require('assets/images/image8.jpeg'),
    name: 'Active one',
    variant: 'Black',
    mrp: '$29 ',
    price: '$15 ',
    offer: '49 %',
    reviews: [
      {
        rating: 5,
        count: 3
      },
      {
        rating: 4,
        count: 5
      },
      {
        rating: 3,
        count: 5
      },
      {
        rating: 2,
        count: 0
      },
      {
        rating: 1,
        count: 3
      },
    ],
    rating: 3.1,
    seller: 'Lekhraj',
    description: "1 Bluetooth Dongle, 1 Aux Cable, 1 Usb Cable, 1 Manual...",
  },
];

export default productData;

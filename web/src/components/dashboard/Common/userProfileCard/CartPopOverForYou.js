import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import Button from '@material-ui/core/Button';
// import { Input } from '@material-ui/core';
import { FormGroup, Input, Label } from 'reactstrap';
import ReactStrapTextField from 'components/ReactStrapTextField';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { connect } from 'react-redux';
import { addToCart, getCartDetails } from '../../../../actions/buyer'
import axios from '../../../../constants/axios';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import AxiosRequest from 'sagas/axiosRequest'

import './index.css'

class CartPopOverForYou extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // quantity: '',
            Inventory:'',
            quantity:0,
            updatedCount:0,
        }
        this.handleIncrement = this.handleIncrement.bind(this)
        this.handleDecrement = this.handleDecrement.bind(this)
        this.handleIncremented = this.handleIncremented.bind(this)
    this.handleDecremented = this.handleDecremented.bind(this)
    }
    // handleChange(evt) {
    //     const quantity = (evt.target.validity.valid) ? evt.target.value : this.state.quantity;
    //     this.setState({ quantity });
    //   }
    handleIncrement = () => {
        this.setState(prevState => {
            return{
                quantity: Number(prevState.quantity) + 1
            }
        })
    }
    handleDecrement = () => {
      if(this.state.quantity >= 1){  
      this.setState(prevState => {
            return{
                quantity: prevState.quantity - 1
            }
        })
    }
  }

  handleChange = (key, event) => {
    if(event.target.value.length <=4){
        this.setState({ quantity:parseInt(event.target.value) });
        }
  };


handleIncremented = (key, event) => {
 
    if (this.state.updatedCount + this.props.dataFromParent.min_order_quantity > this.props.dataFromParent.max_order_quantity){
        
      // self.view.makeToast("Quantity should be a multiple of minimum quantity and cannot exceed max quantity.", duration: Config.defaultToastDuration, position: .center)
  }
  else if(this.state.updatedCount <=  Number(this.props.dataFromParent.max_order_quantity)){
        this.setState({
          updatedCount : Number(this.state.updatedCount) + Number(this.props.dataFromParent.min_order_quantity)
        })
  }
  
  }
  
  handleDecremented = (key, event) => {
    
    if (this.state.updatedCount != this.props.dataFromParent.min_order_quantity && this.state.updatedCount - this.props.dataFromParent.min_order_quantity >= this.props.dataFromParent.min_order_quantity){
        this.setState({
          
          updatedCount : Number(this.state.updatedCount) - Number(this.props.dataFromParent.min_order_quantity)
        })
  
    }
      // (this.state.updatedCount >=  Number(this.props.productData[0].min_order_quantity))
        
  }
  handleQuantityChanged = (key, event) => {
    NotificationManager.warning('Please use buttons for increment and decrement')
    // this.setState({ quantity:parseInt(event.target.value) * 2 ,       
  // });
  };
  maxLengthCheck = (event) => {
    if (event.target.value > this.props.dataFromParent.max_order_quantity)
   { 
    event.target.value =  this.props.dataFromParent.min_order_quantity
    // this.setState({invalid: true})
  }
}
  

      onSubmit = async (e) => {
        this.props.closepop()
          let data
        if (this.props.dataFromParent && this.props.dataFromParent && this.props.dataFromParent.Discount && (this.props.dataFromParent.Discount.discount_type === "Same" || this.props.dataFromParent.Discount.discount_type === "SameAndDiscount" || this.props.dataFromParent.Discount.discount_type === "Different" || this.props.dataFromParent.Discount.discount_type === "DifferentAndDiscount") ){
     if (this.state.updatedCount >= this.props.dataFromParent.min_order_quantity && this.state.updatedCount <= this.props.dataFromParent.max_order_quantity && Number.isInteger(this.state.updatedCount) === true) {
             data = {
                cart_details: [{
                    inventory_id: this.state.Inventory,
                    quantity: this.state.updatedCount
                }]
            }
            let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'cart/addtoCart', '', data)
            if (response.data.error) {
                NotificationManager.error(response.data.title)
            } else {
                NotificationManager.success(response.data.title)
                const isLogin = localStorage.getItem("buyer_token");
                if (isLogin) {
                  this.props.getCartDetails({ history: this.props.history })
                }
            }
        }
      }

          
          else if (this.state.quantity >= this.props.dataFromParent.min_order_quantity && this.state.quantity <= this.props.dataFromParent.max_order_quantity && Number.isInteger(this.state.quantity) === true) {
               data = {
                  cart_details: [{
                      inventory_id: this.state.Inventory,
                      quantity: this.state.quantity
                  }]
              }
              let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'cart/addtoCart', '', data)
              if (response.data.error) {
                  NotificationManager.error(response.data.title)
              } else {
                  NotificationManager.success(response.data.title)
                  const isLogin = localStorage.getItem("buyer_token");
                  if (isLogin) {
                    this.props.getCartDetails({ history: this.props.history })
                  }
              }
          }
        else {
            NotificationManager.error("Please check Minimum & Maximum order quantity.")
          }
      }
      // closepop = () => {
      //   let ea=true
      //   this.props.closepop({ea})
      // }

componentDidMount(){
        const { dataFromParent } = this.props;
        const { min_order_quantity,inventory_id } = this.props.dataFromParent;
        // this.props.initialize({quantity: min_order_quantity})
        this.setState({
          quantity: dataFromParent.min_order_quantity,
          updatedCount:  dataFromParent.min_order_quantity,
          // quantity: takeLatest(min_order_quantity),
          Inventory: dataFromParent.inventory_id
        })
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.dataFromParent !== this.props.dataFromParent){
          this.setState({
            quantity:  this.props.dataFromParent.min_order_quantity, 
            updatedCount: this.props.dataFromParent && this.props.dataFromParent && this.props.dataFromParent.Discount && this.props.dataFromParent.Discount.discount_type === "Same" ?
             this.props.dataFromParent.min_order_quantity : '',
            invalid: false
          })
        }
      }

    render() {
        const { dataFromParent } = this.props;
        let {quantity} = this.state;    
        // const { quantity } = quantity;    
        return (
            <div >
                <div className="media-body " style={{display: "flex"}} >
                    {/* <h1>{this.state.quantity}</h1> */}
{this.props.dataFromParent&&this.props.dataFromParent&&this.props.dataFromParent.Discount &&(this.props.dataFromParent.Discount.discount_type == "Same" || this.props.dataFromParent.Discount.discount_type === "SameAndDiscount" || this.props.dataFromParent.Discount.discount_type === "Different" || this.props.dataFromParent.Discount.discount_type === "DifferentAndDiscount" ) ?
<React.Fragment> 
                    <span className="mt-1 text-grey" 
                    // onClick={this.handleDecrement}
                    onClick={(e) => this.handleDecremented('minOrdntity', e)}
                    ><i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span>
                    <Input  
                        id="quantity"
                        name="quantity" 
                        type="number" 
                        textAlign="center"
                        // placeholder={this.props.dataFromParent.min_order_quantity}
                        // updatedCount={this.props.dataFromParent.min_order_quantity}
                        value={this.state.updatedCount} 
                        // component={ReactStrapTextField}
                        max={this.props.dataFromParent.max_order_quantity}
                         min={this.props.dataFromParent.min_order_quantity}
                         onInput={this.maxLengthCheck.bind(this)}
                        style={{ width: "70px", fontSize: "17px", marginLeft: "5px",  marginRight: "1px",textAlign:'center', height: "50%", }}
                        onChange={(e) => this.handleQuantityChanged('quantity',e)}
                        />
                    <span className="ml-1 mt-1 text-grey" 
                    // onClick={this.handleIncrement}
                    onClick={(e) => this.handleIncremented('minOasdrdntity', e)}
                    ><i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span>
                    </React.Fragment>
                    :
                    <React.Fragment>

                    <span className="mt-1 text-grey" onClick={this.handleDecrement}><i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span>
                    <Input  
                        id="quantity"
                        name="quantity" 
                        type="number" 
                        textAlign="center"
                        value={this.state.quantity} 
                        max={this.props.dataFromParent.max_order_quantity}
                        min={this.props.dataFromParent.min_order_quantity}
                        // component={ReactStrapTextField}
                        style={{ width: "70px", fontSize: "17px", marginLeft: "5px",  marginRight: "1px",textAlign:'center', height: "50%", }}
                        onChange={(e) => this.handleChange('quantity',e)}
                        />
                    <span className="ml-1 mt-1 text-grey" 
                    onClick={this.handleIncrement}><i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span></React.Fragment>

                    }
                </div>
            <div> 
            <span className="mt-5" style={{paddingTop:"3px"}}>Min Order: {dataFromParent.min_order_quantity }</span><br/> 
            <span  style={{paddingBottom:"2px"}}>Max Order: {dataFromParent.max_order_quantity }</span>
            </div>
            {/* <span>Min Max Order({dataFromParent.min_order_quantity},{dataFromParent.max_order_quantity})</span> */}
                <button  
                style={{ padding: '7px', backgroundColor: '#072791', color: 'white', borderRadius: '7px', border: 'none',width:"80%"}} 
                onClick={this.onSubmit}
                variant="contained" color="primary">Add To Cart</button>
            </div>
        );
    }
}
const mapStateToProps = ({ buyer }) => {
    const { addToCart, cartDetails } = buyer
    return { addToCart, cartDetails };
  };
  export default connect(mapStateToProps, { addToCart, getCartDetails })(CartPopOverForYou);
                {/* <Row> */}
                    {/* <Col xs={12} md={12} sm={12} xl={12} lg={12}> */}
{/* 
                    <div  style={{ backgroundColor: "#5093ef", borderRadius: "100%", float: "left", width: "60px", height: "60px",verticalAlign: "middle" }}>
                        <h1 style={{ color: "white", textAlign: "center" }}>-</h1>
                    </div> */}
{/* 
                    <img src="https://img.icons8.com/cotton/64/000000/plus--v2.png" style={{width: "60px", height: "60px"}}></img>
                    <div style={{ backgroundColor: "#ef5350", borderRadius: "100%", float: "left", width: "60px", height: "60px" }}>
                        <h1 style={{ color: "white", textAlign: "center" }}>10</h1>
                    </div>
                    <img src="https://img.icons8.com/cotton/100/000000/minus--v3.png" style={{width: "60px", height: "60px"}}></img>
 */}
                    {/* <div style={{ backgroundColor: "#5093ef", borderRadius: "100%", float: "left", width: "60px", height: "60px" }}>
                        <h1 style={{ color: "white", textAlign: "center" }}>+</h1>
                    </div> */}
                    {/* </Col> */}
                    {/* </Row> */}
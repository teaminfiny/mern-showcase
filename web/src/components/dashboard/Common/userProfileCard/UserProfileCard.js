import React, { Component } from 'react';
import Fab from '@material-ui/core/Fab';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import { Carousel, CarouselItem } from 'reactstrap';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import { NavLink, withRouter } from 'react-router-dom';
import AppConfig from 'constants/config'


import {
  getProductDetails,
} from 'actions/buyer';
import { connect } from 'react-redux';

import Tooltip from '@material-ui/core/Tooltip';

import {Popover, PopoverBody, PopoverHeader} from 'reactstrap';

import CartPopOver from './CartPopOver'
import helpertFn from 'constants/helperFunction';
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';

class UserProfileCard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = { 
      activeIndex: 0,
      popoverOpen: false
    };

    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }



  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next(size) {
    if (this.animating) return;
    // const nextIndex = this.state.activeIndex === ProductData.length - 1 ? 0 : this.state.activeIndex + 1;
    if(size === 1){
      this.setState({ activeIndex: 0});
    }
    else if(this.state.activeIndex == 0){
      this.setState({ activeIndex: this.state.activeIndex + 1 });
    }
  }

  previous(size) {
    if (this.animating) return;
    // const nextIndex = this.state.activeIndex === 0 ? ProductData.length - 1 : this.state.activeIndex - 1;
    if(size === 1){
      this.setState({ activeIndex: 0});
    }
    else if(this.state.activeIndex == 1){
    this.setState({ activeIndex: this.state.activeIndex - 1 });
    }
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }
  
  handleDelete = () => {

  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  
  handleClick = () => {
  }

  handleNavClick = (e, link) => {
    this.props.history.push(link)
    this.props.getProductDetails({inventory_id: this.props.product.inventory_id})
  }
  
  addDefaultSrc(ev){
    ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
  }
  closepop = () => {
    this.setState({ popoverOpen: false })
  }   
  render() {
    
    const {  product, identifier } = this.props

    const { activeIndex } = this.state
    
    const discount = this.props.product && this.props.product.Discount && this.props.product.Discount;


    const otherProduct = this.props.product ? this.props.product.OtherProducts ? this.props.product.OtherProducts : '' : '';

    const inventoryID = product && product && product.inventory_id;

    const chemCombination = this.props.product && this.props.product.Product && this.props.product.Product.chem_combination
    
    const productMRP = this.props.product && this.props.product.MRP
    const productName =  this.props.product && this.props.product.Product && this.props.product.Product.name && ((this.props.product.Product.name).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()
    const bgColor = product && product.medi_type === 'Ethical branded' || product.medi_type === 'Others' ? '#ff7000' :
                    product.medi_type === 'Cool chain' ? '#0b68a8' :
                    product.medi_type === 'Surgical' || product.medi_type ==='OTC' || product.medi_type ==='Generic' ? '#038d0e' :'#072791'

    // const pos = product && product.map(function(e) { return e.Product.name; }).indexOf(inventoryID);

    

   
  const slides = product ? product ? product.Product ? product.Product.images.length >  0 ? product.Product.images.map((image, index) => {
     return (
        <CarouselItem
          key={index}
        >
          <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`}  
            onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
          <img src =  { product.Product.images.length === 0 ? logo :`${helpertFn.productImg(image)}` } 
          onError={this.addDefaultSrc} />
          {/* <CarouselCaption captionText={image} imageHeader={image} />
           */}
          </NavLink>
        </CarouselItem>
      )
    })
    :
    [<CarouselItem
      key={"index"}
    >
      <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
        <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
      </NavLink>
    </CarouselItem>]
     :
    [<CarouselItem
      key={"index"}
    >
      <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
        <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
      </NavLink>
    </CarouselItem>]
     : 
     [<CarouselItem
      key={"index"}
    >
      <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
        <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
      </NavLink>
    </CarouselItem>]
     :
      [<CarouselItem
      key={"index"}
    >
      <NavLink className="buyerRedirectNavlink" to={`/product-details/${productName}/${product.inventory_id}`} onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${product.inventory_id}`)}>
        <img src={product.Product.images.length === 0 ? logo : `${AppConfig.productImageUrl}/}`} />
      </NavLink>
    </CarouselItem>]


    let a = this.getRandomInt(3)
    let icon = this.props.index % 2 === 0 ? <Avatar className="bg-danger iconAvtarColor text-white">
      <ThumbDownIcon style={{ fontSize: '0.6rem' }} /></Avatar> : <Avatar className="bg-success text-white iconAvtarColor">
        <ThumbUpIcon style={{ fontSize: '0.6rem' }} /></Avatar>
    //  const eff = this.props.product && this.props.product.Discount && this.props.product.Discount && this.props.product.Discount.discount_type == 'Same' && this.props.product.Discount.map((data) => {
    //       return
    // if(this.props.product && this.props.product.Discount && this.props.product.Discount && this.props.product.Discount.discount_type == "Same"){
      //  let effects = Number(product.PTR) * Number(this.props.product && this.props.product.Discount && this.props.product.Discount.discount_on_product.purchase);
        // let effects1 = Number(effects) /(Number(this.props.product && this.props.product.Discount && this.props.product.Discount.discount_on_product.purchase)+ Number(this.props.product && this.props.product.Discount && this.props.product.Discount.discount_on_product.bonus));
    // }  
      // })
        
    return (

      <React.Fragment>
          
            <div className="jr-card text-left">
              <div className={`jr-card-header-color ${this.props.match.url === '/view-seller' ? 'intranetCardViewSeller' : 'intranetCard'}`} style={{minHeight: "260px", position:'relative', display:'inline-flex', padding: '0px'}}>

                <Carousel
                  autoPlay={false}
                  indicators={true}
                  activeIndex={activeIndex}
                  next={() => this.next(slides.length)}
                  interval={false}
                  previous={() => this.previous(slides.length)}
                  className='itemImage'>
              {/* <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} /> */}
                  {slides}
                  
                  {/* <CarouselControl className="arrows" cssModule='primary' direction="prev" directionText="Previous" onClickHandler={() => this.previous(slides.length)} />
                  <CarouselControl className="arrows" direction="next" directionText="Next" onClickHandler={() => this.next(slides.length)} /> */}
                </Carousel>
                
                <span style={{ padding:'2px', fontWeight:'bold', position: 'absolute', zIndex: 1, backgroundColor:`${bgColor}`, color:'white', width:'auto'}} >{product && product.medi_type === 'Others' ? 'PCD' : product.medi_type }</span>

                {
                  this.props.match.url === '/view-company' ? 
                  null 

                  : 
                  
                  // <Fab className="jr-badge-up bg-primary">
                  //   <i className="zmdi zmdi-shopping-cart" />
                  // </Fab>

                  <div>
                  <Fab className="jr-badge-up" style={{backgroundColor:`${bgColor}`, color:'white'}} >
                    <i class="zmdi zmdi-shopping-cart" 
                    style={{ float: "left" }} 
                    onClick={this.toggle} 
                    id= {"upc1" + inventoryID + identifier}
                    />
                  </Fab>

                  <div>
                  {/* boxShadow: "2px 2px 2px 2px  #d9d3d2", border: "none", */}
                    <Popover style={{ padding: "5px", paddingBottom: "0px", textAlign: "center", }} trigger="legacy" placement="right"
                      isOpen={this.state.popoverOpen}
                      target={"upc1" + inventoryID + identifier}
                      toggle={this.toggle} >

                      <PopoverHeader style={{ padding: "4px 4px", textAlign: "center" }}>Select Quantity</PopoverHeader>
                      <br />
                      <CartPopOver closepop={(e)=>this.closepop(e)}
                        dataFromParent={product}
                      />
                      <PopoverBody style={{ paddingBottom: 0, textAlign: "center" }}>
                      </PopoverBody>

                    </Popover>

                  </div>
                </div>
                }

              </div>
                    {
                      this.props.product && this.props.product.medi_attribute.includes('Jumbo Deal') ?
                        <Chip
                          label={'Jumbo Deal'}
                          size='small'
                          style={{backgroundColor:`${bgColor}`, padding:'17px 10px', color:'white', float:'left', position:'relative', zIndex:1, marginTop:'-28px'}}
                        />
                      :''
                    }
              <div className="jr-card-body pt-2" >
                <div className="product-details">
                  
                {
                    this.props.product && this.props.product.Product && this.props.product.Product.name.length > 40 ?
                      <Tooltip
                        className="d-inline-block"
                        id="tooltip-right"
                        title={
                          <h6 className="text-white"
                            style={{ marginTop: "2px" }}>
                            {this.props.product && this.props.product.Product && this.props.product.Product.name}
                          </h6>
                        }
                        placement="right"
                        className="ellipsis">

                        <NavLink className="ellipsis" className="buyerRedirectNavlink" to={`/product-details/${productName}/${inventoryID}`}
                          onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${inventoryID}`)}>
                          <h2 className="card-title card-titleIntranet"  style={{minHeight: "45px", maxHeight: "45px", overflow: "hidden", marginTop: "2px"}}>
                            {(this.props.product && this.props.product.Product && this.props.product.Product.name) + "..."}
                          </h2>
                        </NavLink>
                      </Tooltip>
                  :
                  <NavLink className="" className="buyerRedirectNavlink" to={`/product-details/${productName}/${inventoryID}`}
                    onClick={(e) => this.handleNavClick(e, `/product-details/${productName}/${inventoryID}`)}>
                    <h2 className="card-title card-titleIntranet" style={{minHeight: "45px", maxHeight: "45px", overflow: "hidden", marginTop: "2px"}}>
                      {this.props.product && this.props.product.Product && this.props.product.Product.name}
                    </h2>
                  </NavLink>
                }


                <p className={this.props.history.location.pathname === '/view-seller' ? 'pt-1' : ''} style={{minHeight: "10px",marginBottom:"2px"}}>
                  MRP: ₹{productMRP.toFixed(2)}
                  {
                        helpertFn.showPrepaid(this.props.product.medi_type,this.props.product.Product.isPrepaid && this.props.product.Product.isPrepaid, product.prepaidInven &&product.prepaidInven) && 
                          <span className='text-white bg-danger' style={{margin:"0px", padding:"0px 5px", float:"right"}}>Only Prepaid</span>
                  }
                </p>

                  <NavLink className="buyerRedirectNavlink" to={`/view-seller/${product && product.Seller && product.Seller._id}`} 
                  style={{minHeight: "43px"}}>
                      <Chip 
                        // avatar={<Avatar src={require('assets/images/Anjali-Sud.jpg')} />}
                        label={(product && product.Seller && product.Seller.company_name.length) > 25 ?
                         ( product && product.Seller && product.Seller.company_name).slice(0,24) + "..." : product && product.Seller && product.Seller.company_name} //REMAINING FOR SELLER NAME (API NOT COMPLETED AT THE MOMENT)
                        // deleteIcon={icon}
                        // onDelete={this.handleDelete}
                        onClick={this.handleClick}
                        size='small'
                      />
                  </NavLink>



                  {
                    chemCombination.length > 35 ?
                      <React.Fragment>
                        
                         <Tooltip 
                        className="d-inline-block" 
                        id="tooltip-right" 
                          title={
                            <h6 className="text-white"
                              style={{ marginTop: "2px" }}>
                              {this.props.product && this.props.product.Product && this.props.product.Product.chem_combination}
                            </h6>
                          }
                        placement="right" 
                        className="ellipsis">

                        <p className="ellipsis" className={this.props.history.location.pathname === '/view-seller' ? 'pt-1' : 'pt-1'} 
                        style={{minHeight: "46px",marginBottom:"2px"}}>
                          {chemCombination + "..."}
                        </p>

                        </Tooltip>

                      </React.Fragment> 
                      :
                      <p className={this.props.history.location.pathname === '/view-seller' ? 'pt-1' : 'pt-1'} style={{minHeight: "46px",marginBottom:"2px"}}>
                        {chemCombination}
                      </p>
                  }

                  {
                    this.props.match.url === `/view-company` ? null : <p className="d-flex mb-0 align-items-baseline">
                      By  
                      <NavLink className="buyerRedirectNavlink " to={`/view-company/${product.Company._id}`}>
                        <h5 className="text-primary ml-1" style={{minHeight: "30px", maxHeight: "30px", overflow: "hidden",marginBottom:"2px"}}>{this.props.product && this.props.product.Company && this.props.product.Company.name}</h5>
                      </NavLink> 
                    </p>
                  }

                </div>
              </div>

              <div className='jr-card-footer intranetFooter ellipsis' >
                {
                  discount && discount.discount_type === "Same" ?
                    <React.Fragment>
                    {/* <span className={'priceColor'}>₹{(product.Inventory.PTR).toFixed(2)}</span>&nbsp;  */}
                    <div className='row pl-3'>
                      <span className={'discountPrice'}> Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} Free</span>
                      </div>
                      <div className='row pl-3'>
                        {/* <div className="col-lg-5 col-md-5 col-sm-5 col-5 " > */}
                          <span className={'priceColor'}>&#x20B9;{(product.ePTR).toFixed(2)}</span>
                          <span style={{marginTop:'2px'}}>
                          <span className={'originalPrice font'}>&#x20B9;{product.PTR.toFixed(2)}</span>
                        {/* </div>
                        <div className="col-lg-7 col-md-7 col-sm-7 col-7" style={{marginTop:'2px'}}> */}
                        &nbsp;
                        <span className={'discountPrice'} >Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                        </span>
                        {/* </div> */}
                      
                        
                      </div>
                    
                 
                    </React.Fragment>

                      :
                      discount && discount.discount_type === "SameAndDiscount" ?
                      <React.Fragment>
                      <div className='row pl-3'>
                        <Tooltip
                                className="d-inline-block"
                                id="tooltip-right"
                                title={
                                  <span style={{ marginTop: "5px" }}>
                                    Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} Free, and {(product.Discount.discount_per).toFixed(2)}% Off
                                  </span>
                                }
                                placement="right"
                              >
                      <span className={'discountPrice'}> Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} Free, and {(product.Discount.discount_per).toFixed(2)}% Off</span>
                            </Tooltip>
                            </div>
                            <div className='row pl-3'>
                              {/* <div className="col-lg-5 col-md-5 col-sm-5 col-5" > */}
                            <span className={'priceColor'}>&#x20B9;{(product.ePTR).toFixed(2)}</span>&nbsp;
                            <span style={{marginTop:'2px'}}>
                            <span className={'originalPrice font'}>&#x20B9;{product.PTR.toFixed(2)}</span>&nbsp;
                          {/* </div> */}
                          {/* <div className="col-lg-7 col-md-7 col-sm-7 col-7" > */}
                          <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                          </span>
                          {/* </div> */}
                            </div>
                      
                   
                      </React.Fragment> :

                      discount &&  discount.discount_type === "Discount" ?

                    <React.Fragment>
                      {/* <div className='row'> */}
                        <div className='row pl-3'>
                          <span>Get {(product.Discount.discount_per).toFixed(2)}% discount</span>
                        </div>
                        <div className='row pl-3'>
                          {/* <div className="col-lg-5 col-md-5 col-sm-5 col-5 " > */}
                            <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span>&nbsp;
                            <span style={{marginTop:'2px'}}>
                            <span className={'originalPrice font'}>₹{(product.PTR).toFixed(2)}</span>&nbsp;
                          {/* </div> */}
                          {/* <div className="col-lg-7 col-md-7 col-sm-7 col-7" > */}
                              <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                              </span>
                          {/* </div> */}
                        </div>
                      {/* </div> */}
                    
                   
                    </React.Fragment>

                      :

                      discount &&  discount.discount_type === "Different" ? //REMAINING FOR DIFFERENT (API NOT COMPLETED AT THE MOMENT)

                    <React.Fragment>
                      <div className='row pl-3'>
                        <Tooltip
                            className="d-inline-block"
                            id="tooltip-right"
                            title={
                              <h6 className="text-white" style={{ marginTop: "5px" }}>
                                Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} {otherProduct.name} Free
                            </h6>
                            }
                            placement="right"
                            className="ellipsis">

                            <span className="ellipsis" className={'discountPrice'}>
                              {("Buy " + discount.discount_on_product.purchase + " Get " + discount.discount_on_product.bonus + " " + otherProduct.name + " Free").slice(0, 25) + "..."}
                            </span>
                          </Tooltip>
                          </div>
                          <div className='row pl-3'>
                            {/* <div className="col-lg-5 col-md-5 col-sm-5 col-5" > */}
                          <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                          <span style={{marginTop:'2px'}}>
                        {/* </div> */}
                        {/* <div className="col-lg-7 col-md-7 col-sm-7 col-7" > */}
                        <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                        </span>
                        {/* </div> */}
                          </div>
                        
                    
                    
                    </React.Fragment>

                    :   
                    discount &&  discount.discount_type === "DifferentAndDiscount" ? //REMAINING FOR DIFFERENT (API NOT COMPLETED AT THE MOMENT)

                    <React.Fragment>
                      <div className='row pl-3'>
                        <Tooltip
                            className="d-inline-block"
                            id="tooltip-right"
                            title={
                              <h6 className="text-white" style={{ marginTop: "5px" }}>
                                Buy {discount.discount_on_product.purchase} Get {discount.discount_on_product.bonus} {otherProduct.name} Free, and {(product.Discount.discount_per).toFixed(2)}% Off
                            </h6>
                            }
                            placement="right"
                            className="ellipsis">

                            <span className="ellipsis" className={'discountPrice'}>
                              {("Buy " + discount.discount_on_product.purchase + " Get " + discount.discount_on_product.bonus + " " + otherProduct.name + " Free, and "+(product.Discount.discount_per).toFixed(2)+"% Off").slice(0, 25) + "..."}
                            </span>
                          </Tooltip>
                          </div>
                          <div className='row pl-3'>
                            {/* <div className="col-lg-5 col-md-5 col-sm-5 col-5" > */}
                          <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                        {/* </div> */}
                        {/* <div className="col-lg-7 col-md-7 col-sm-7 col-7" > */}
                        <span className={'discountPrice'} style={{marginTop:'2px'}}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                          
                        {/* </div> */}
                          </div>
                        
                    
                    
                    </React.Fragment> :                

                      discount &&  discount.discount_type === "" ? //FOR DEALS ARR

                  <React.Fragment>
                    <div className='row pl-3 mt-3'></div>
                    <div className='row pl-3'>
                        {/* <div className="col-lg-4 col-md-4 col-sm-4 col-4" > */}
                          <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                          <span style={{marginTop:'2px'}}>
                          <span className={'originalPrice font'}>{product.MRP === product.PTR? null : "₹" +  (product.MRP).toFixed(2)}</span>&nbsp;
                        {/* </div> */}
                        {/* <div className="col-lg-8 col-md-8 col-sm-8 col-8" > */}
                        <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                        </span>
                        {/* </div> */}
                      </div>
                  </React.Fragment>

                    : 

                  <React.Fragment>
                    <div className='row pl-3 mt-3'></div>
                  <div className='row pl-3'>
                        {/* <div className="col-lg-4 col-md-4 col-sm-4 col-4" > */}
                          <span className={'priceColor'}>₹{(product.ePTR).toFixed(2)}</span> &nbsp;
                          <span style={{marginTop:'2px'}}>
                          &nbsp;
                        {/* </div> */}
                        {/* <div className="col-lg-8 col-md-8 col-sm-8 col-8" > */}
                        <span className={'discountPrice'}>Min: {product.min_order_quantity}, Max: {product.max_order_quantity}</span>
                        </span>
                        {/* </div> */}
                      </div> 
                  </React.Fragment>
              }
              </div>

            </div>
            
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { data, otherSellers, relatedProducts, productData,} = buyer;
  return {  data, otherSellers, relatedProducts, productData }
};

export default withRouter(connect(mapStateToProps, {getProductDetails}) (UserProfileCard));
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ProductTable from 'components/productTable';
import IntlMessages from 'util/IntlMessages';
import { FormGroup } from '@material-ui/core';
import { Col, Row, Label, Input } from 'reactstrap';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import { connect } from 'react-redux';
import { getDashboardTopSellingProducts, getDashboardLeastSellingProducts } from 'actions/seller'
import helperFunction from '../../constants/helperFunction';

let monthArray = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
let date = new Date();
function TabContainer({ children, dir }) {
  return (
    <div dir={dir}>
      {children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class SellingProductTab extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      anchorEl: null,
      filterValueTop: monthArray[date.getMonth()] + ' ' + date.getFullYear(),
      filterValueLeast: monthArray[date.getMonth()] + ' ' + date.getFullYear(),
      month: monthArray[date.getMonth()],
      year: date.getFullYear(),
      month1: monthArray[date.getMonth()],
      year1: date.getFullYear(),
      selectedTab: '',
      yearList: []
    }
  }

  componentDidMount() {
    let dateDetails = this.state.filterValueTop.split(' ')
    let dateDetails1 = this.state.filterValueLeast.split(' ')
    let years = helperFunction.yearList();
    this.setState({ yearList: years })
    let data = {
      month: dateDetails[0],
      year: Number(dateDetails[1])
    }

    let data1 = {
      month: dateDetails1[0],
      year: Number(dateDetails1[1]),
      limit: 5
    }
    this.props.getDashboardTopSellingProducts({ data })
    this.props.getDashboardLeastSellingProducts({ data1 })
  }
  handleClick = (event, key) => {
    this.setState({ anchorEl: event.currentTarget, selectedTab: key })
  }

  handleClose = (e) => {
    e.preventDefault();

    if (this.state.selectedTab === 'top') {

      this.setState({ anchorEl: null, filterValueTop: monthArray[date.getMonth()] + ' ' + date.getFullYear(), month: monthArray[date.getMonth()], year: date.getFullYear() })
    } else if (this.state.selectedTab === 'least') {
      this.setState({ anchorEl: null, filterValueLeast: monthArray[date.getMonth()] + ' ' + date.getFullYear(), month1: monthArray[date.getMonth()], year1: date.getFullYear() })
    }
  }

  handleApplyFilter = (e) => {
    e.preventDefault();
    if (this.state.selectedTab === 'top') {
      let filter = this.state.month + ' ' + this.state.year;
      let data = {
        month: this.state.month,
        year: Number(this.state.year)
      }
      this.props.getDashboardTopSellingProducts({ data })
      this.setState({ anchorEl: null, filterValueTop: filter })
    } else if (this.state.selectedTab === 'least') {
      let filter = this.state.month1 + ' ' + this.state.year1;
      let data1 = {
        month: this.state.month1,
        year: Number(this.state.year1),
        limit: 5
      }
      this.props.getDashboardLeastSellingProducts({ data1 })
      this.setState({ anchorEl: null, filterValueLeast: filter })
    }

  }

  handleChange = (e, key) => {
    this.setState({ [key]: e.target.value })
  }

  render() {
    const { theme } = this.props;
    const { filterValueTop, anchorEl, filterValueLeast, month, year, month1, year1, selectedTab, yearList } = this.state;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-xl-6 col-12">
            <div className="jr-card">
              <div className="jr-card-header d-flex align-items-center">
                <div className="mr-auto">
                  <h3 className="d-inline-block mb-0">Top 5 Selling Products</h3>
                  {
                    filterValueTop === '' ? null : <span className="text-white badge badge-success">{filterValueTop}</span>
                  }
                </div>
                {/* <IconButton className="icon-btn" onClick={(e) => this.handleClick(e, 'top')}>
                  <i className="zmdi  zmdi-filter-list" />
                </IconButton> */}
              </div>
              <ProductTable value={this.props.topSellingProducts} />
            </div>
          </div>

          <div className="col-xl-6 col-12">
            <div className="jr-card">
              <div className="jr-card-header d-flex align-items-center">
                <div className="mr-auto">
                  <h3 className="d-inline-block mb-0">Least 5 Selling Products</h3>
                  {
                    filterValueLeast === '' ? null : <span className="text-white badge badge-success">{filterValueLeast}</span>
                  }
                </div>
                {/* <IconButton className="icon-btn" onClick={(e) => this.handleClick(e, 'least')}>
                  <i className="zmdi  zmdi-filter-list" />
                </IconButton> */}
              </div>
              <ProductTable value={this.props.leastSellingProducts} />
            </div>
          </div>
        </div>

        <Menu
          id="lock-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={(e) => this.handleClose(e)}
        >
          <MenuItem className="filterBody">
            <div className="flex">
              <Row form>
                <Col md={6} xl={6} xs={6} lg={6}>
                  <FormGroup>
                    <Label for="exampleSelect">Select Month</Label>
                    <Input type="select" value={selectedTab === 'top' ? month : month1} name="select" onChange={(e) => this.handleChange(e, selectedTab === 'top' ? 'month' : 'month1')} id="exampleSelect">
                      {
                        monthArray.map((value, key) => {
                          return <option key={key} value={value}>{value}</option>
                        })
                      }
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={6} xl={6} xs={6} lg={6}>
                  <FormGroup>
                    <Label for="exampleSelect">Select Year</Label>
                    <Input type="select" value={selectedTab === 'top' ? year : year1} onChange={(e) => this.handleChange(e, selectedTab === 'top' ? 'year' : 'year1')} name="select" id="exampleSelect">
                      {
                        yearList.map((year, key) => {
                          return <option key={key}  value={year}>{year}</option>
                        })
                      }
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              <div style={{ paddingTop: 15 }} >
                <Button variant="contained" className='filterButton' onClick={(e) => this.handleApplyFilter(e)} color='primary'>Apply Filter</Button>
                <Button variant="contained" className='filterButton' onClick={(e) => this.handleClose(e)} color='primary'>Reset Filter</Button>
              </div>
            </div>
          </MenuItem>
        </Menu>
      </React.Fragment>
    );
  }
}

SellingProductTab.propTypes = {
  theme: PropTypes.object.isRequired,
};
const mapStateToProps = ({ seller }) => {
  const { topSellingProducts, leastSellingProducts } = seller
  return { topSellingProducts, leastSellingProducts }
}
export default connect(mapStateToProps, { getDashboardTopSellingProducts, getDashboardLeastSellingProducts })(withStyles(null, { withTheme: true })(SellingProductTab));
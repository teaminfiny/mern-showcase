import React from 'react';

import NotificationItem from './NotificationItem';
import { getBuyerNotification } from 'actions/notification';
import InfiniteScroll from 'react-infinite-scroll-component';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import AxiosRequest from 'sagas/axiosRequest'
class AppNotification extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      fetch: true,
      page: 2,
      perPage:20,
      items: '',
      dataL:20
    };
  }
  componentDidMount = () => {
    this.setState({ items: this.props.notificationData && this.props.notificationData});
  }
  
  fetchData = async () => {
    console.log('asdoimasciomaec-', this.props)
    let { total } = this.props.notiData;
    if (this.props.notificationData.length >= total) {
      console.log('asdoimasciomaec-', this.props.notiData)
      this.setState({ fetch: false });
      return;
    }
    let data = {
      page: this.state.page,
      perPage: this.state.perPage,
      user_type: this.props.type
    }
    let response = await AxiosRequest.axiosTokenSwitch('post', 'users/getNotification', '', data)
    if (response.data.error) {
    } else {
      this.setState({ items: this.state.items.concat(response.data.detail), dataL: this.state.dataL + 10 });
      this.setState({ page: this.state.page + 1 });
    }
  };
  render(){

  let { notificationData, notiData } = this.props;
  let { items, fetch, dataL } = this.state;
  let outOfPage = notiData.total > 0 ? this.state.page > Math.round(notiData.total/this.state.perPage)  : true;
  console.log('asdoimasciomaec', items,outOfPage,notificationData.length && notiData)
  return (
    <div id="scrollableDiv" style={{ height: 300, overflow: 'auto', display: 'flex' }}>
      <InfiniteScroll
      dataLength={dataL} 
      next={this.fetchData}
      hasMore={fetch}
      loader={ !outOfPage && <div style={{ textAlign: 'center' }}>
          <CircularProgress size={25}/>
      </div>}
      scrollableTarget="scrollableDiv"
      endMessage={
        <p style={{ textAlign: 'center' }}>
          <b>Yay! You have seen it all</b>
       </p> }> 
      <ul className="list-unstyled">
        { (notificationData.length > 0 && notiData.total > 0) ?
        items && items.length > 0 && items.map((notification, index) => <NotificationItem key={notification._id} notification={notification} />) : <li><p className="sub-heading mb-0">No Notification found</p> </li>
        }
      </ul>
    
    </InfiniteScroll>
    </div>
  )
}
};

const mapStateToProps = ({  notification }) => {
  const { unreadNotificationCount, notificationData, unReadNotification, notiData} = notification;
  return { unreadNotificationCount, notificationData, unReadNotification, notiData }
};

export default (connect(mapStateToProps, { getBuyerNotification })(AppNotification));


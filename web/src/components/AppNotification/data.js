export const notifications = [

  {
    image: require('assets/images/alisha3.jpg'),
    title: "Stella Johnson has recently posted an album",
    time: "4:10 PM",
    icon: "12-10-2019",
  }, {
    image: require('assets/images/alisha.jpeg'),
    title: "Alex Brown has shared Martin Guptil's post",
    time: "5:18 PM",
    icon: "12-10-2019",
  }, {
    image: require('assets/images/Anjali-Sud.jpg'),
    title: "Domnic Brown has sent you a group invitation for Global Health",
    time: "5:36 PM",
    icon: "12-10-2019",
  }, {
    image: require('assets/images/74sByqd.jpg'),
    title: "John Smith has birthday today",
    time: "5:54 PM",
    icon: "12-10-2019",
  }, {
    image: require('assets/images/profile_user.jpg'),
    title: "Chris has updated his profile picture",
    time: "5:25 PM",
    icon: "12-10-2019",
  }
];
import React from 'react';
import Avatar from '@material-ui/core/Avatar'
import { withRouter } from 'react-router-dom'
import helperFunction from '../../constants/helperFunction';
import moment from 'moment'
const NotificationItem = ({ notification }) => {
  const { createdAt, message, buyerId, sellerId, flag } = notification;
  let user = localStorage.user_type
  console.log("notification...",localStorage)
  const initials = (data) => {
    switch (data) {
      case "Order Placed":
      case "Order Accepted / Declined": 
      case "Order Received": 
        return 'OP'
      case "Order Declined":
        return 'OD'
      case "Shortbook Available In Stock":
      case "Shortbook Notification":
      case "Product Added To ShortBook":
        return 'SB'
      case "MEDIMNY PRESENTS":
        return 'D'
      case "Product Request Generated":
      case "Product Notification":
      case "Product Request Fulfilled":
      case "Product Request Notification":
        return 'PR'
      default:
        return false
    }
  }
  const initialColor = (data) => {
    switch (data) {
      case "Order Placed":
      case "Order Accepted / Declined":
      case "Order Received": 
        return 'blue'
      case "Order Declined":
        return 'red'
      case "Shortbook Available In Stock":
      case "Shortbook Notification":
      case "Product Added To ShortBook":
        return '#078492'
      case "MEDIMNY PRESENTS":
        return 'orange'
      case "Product Request Generated":
      case "Product Notification":
      case "Product Request Fulfilled":
      case "Product Request Notification":
        return '#430792'
      default:
        return false
    }
  }
  let userType = user == 'buyer' ? buyerId : sellerId;
  return (
    // user == 'buyer' ?
    <li className="media">
      <Avatar
        alt={'image'}
        style={{ background: initialColor(notification.flag) ? initialColor(notification.flag) : helperFunction.getGradient(userType && userType.backGroundColor) }}
        className=" mr-2"
      >
        {
          initials(notification.flag) ? initials(notification.flag) :
          userType !== undefined && userType ? helperFunction.getNameInitials(`${userType.first_name} ${userType.last_name}`) : ''
        }
      </Avatar>
      <div className="media-body align-self-center">
        <h6 className={` mb-0 ${flag=='Shortbook Notification' && 'text-primary'}`}>{message}</h6>
        <span className="mb-0 text-grey"><small>{moment(createdAt).format('DD-MM-YYYY')}</small></span>
        <span className="meta-date ml-1"><small>{moment(createdAt).format('HH:mm A')}</small></span>
      </div>
    </li> 
  //   <li className="media">
  //   <Avatar
  //     alt={'image'}
  //     style={{ background: helperFunction.getGradient(sellerId && sellerId.backGroundColor) }}
  //     className=" mr-2"
  //   >
  //     {
  //       sellerId !== undefined && sellerId ? helperFunction.getNameInitials(`${sellerId.first_name} ${sellerId.last_name}`) : ''
  //     }
  //   </Avatar>
  //   <div className="media-body align-self-center">
  //     <h6 className=" mb-0">{message}</h6>
  //     <span className="mb-0 text-grey"><small>{moment(createdAt).format('DD-MM-YYYY')}</small></span>
  //     <span className="meta-date ml-1"><small>{moment(createdAt).format('HH:mm A')}</small></span>
  //   </div>
  // </li>
  );
};

export default withRouter(NotificationItem);

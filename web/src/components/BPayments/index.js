import React, { Component } from 'react';
import Cards from 'react-credit-cards';
class CreditCard extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        const {cvc,expiry,focus,name,number} = this.props
        return ( 
            <Cards
          cvc={cvc}
          expiry={expiry}
          focus={focus}
          name={name}
          number={number}
        />
         );
    }
}
 
export default CreditCard;
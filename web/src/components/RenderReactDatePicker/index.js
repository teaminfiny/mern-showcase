import moment from 'moment';
import DatePicker from "react-datepicker";
import React from 'react';
import PropTypes from 'prop-types';

const RenderReactDatePicker = ({ input, meta: { touched, error } }) => {

    const onChange = event => {
        console.log("input event", event, event)
        const pickedDate = event;
        input.onChange(pickedDate);
    }
    console.log("input", input)
    return (
        <div>
            <DatePicker
                /* dateFormat="MM-DD-YYYY"
                selected={input.value ? moment(input.value) : null}
                onChange={onChange}
                className="form-control" */

                selected={input.value ? moment(input.value).toDate() : moment().toDate()}
                onChange={onChange}
                dateFormat="dd/MM/yyyy"
                // showMonthYearPicker
                className="form-control"
                value={input.value ? moment(input.value).toDate() : moment().toDate()}
                minDate={moment().toDate()}
            />
            {
                touched && error &&
                <span className="error">
                    {error}
                </span>
            }
        </div>
    );
}

RenderReactDatePicker.propTypes = {
    input: PropTypes.shape().isRequired,
    meta: PropTypes.shape().isRequired
};

export default RenderReactDatePicker;
import React, { Component } from 'react'
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
        return
    } else {
        return <FormHelperText>{touched && error}</FormHelperText>
    }
}

const RenderSelectField = ({
    input,
    props,
    label,
    meta: { touched, error },
    children,
    ...custom
}) => {
        console.log('cherckbox==',input)
       return <FormControl required error={error} component="fieldset" >
       <FormControlLabel
            control={
              <Checkbox {...input} {...props}color="primary"/>
            }
            label={label}
          />
            {renderFromHelper({ touched, error })}
        </FormControl>
        }

export default RenderSelectField;
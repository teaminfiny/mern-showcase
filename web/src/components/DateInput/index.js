import React, { Component } from 'react'
import FormHelperText from '@material-ui/core/FormHelperText'
const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
        return
    } else {
        return <FormHelperText style={{ color: '#f44336' }}>{touched && error}</FormHelperText>
    }
}

export default class DateInput extends Component {
    constructor(props) {
        super(props)
    }

    handleFileSelect(e) {
        const { input: { onChange } } = this.props
        let document = "";
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = () => {
            document = reader.result;
            onChange(document)
        };
        reader.onerror = function (error) {
        };
    }

    uplaodNew(e) {
        const { input: { onChange } } = this.props;
        onChange('')
    }

    render() {
        const { input: { value } } = this.props
        const {  label,  meta: { touched, error, warning } } = this.props  //whatever props you send to the component from redux-form Field
        return (
            <div className='text-center'>
                <div className="p-2">
                    <input
                        type='file'
                        accept='.jpg, .png, .jpeg, .pdf'
                        style={{ display: 'none' }}
                        onChange={(e) => this.handleFileSelect(e)} ref={(ref) => this.drugLic20B = ref}
                    />
                    <div class="img-wrap">
                        {/* {value !== '' ? <React.Fragment>
                            <span class="close" onClick={(e) => this.uplaodNew()}>&times;</span>
                            {value.length > 200 ? <React.Fragment>
                                {value.substring(value.indexOf('/') + 1, value.indexOf(';base64')) === "pdf" ? <iframe src={value} style={{ width: '150px', height: '150px' }} /> : <img src={value} style={{ width: '150px', height: '150px' }} className='d-block mx-auto' onClick={(e) => this.drugLic20B.click()} />}
                            </React.Fragment> : value.includes('.pdf') ? <iframe src={`${AppConfig.baseUrl}users/${value}`} style={{ width: '150px', height: '150px' }} /> : <img src={`${AppConfig.baseUrl}users/${value}`} style={{ width: '150px', height: '150px' }} className='d-block mx-auto' onClick={(e) => this.drugLic20B.click()} />}
                        </React.Fragment> : <img src={require('../../assets/img/pdf1.png')} style={{ width: '150px', height: '150px' }} className='d-block mx-auto' onClick={(e) => this.drugLic20B.click()} />} */}

                        <img src={value !== '' ? require('../../assets/img/pdf1.png') : require('../../assets/img/pdf2.png')} style={{ width: '150px', height: '150px' }} className='d-block mx-auto' onClick={(e) => this.drugLic20B.click()} />

                    </div>
                    {/* <img src={value !== '' ? require('../../assets/img/camera_3.png') : require('../../assets/img/pdf.png')} className='d-block mx-auto' onClick={(e) => this.drugLic20B.click()} /> */}
                </div>
                <label className="text-dark" style={{ fontSize: 17 }}>{label}</label>
                {renderFromHelper({ touched, error })}
            </div>
        )
    }
}
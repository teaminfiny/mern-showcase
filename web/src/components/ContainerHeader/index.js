import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import IntlMessages from 'util/IntlMessages';
import Avatar from '@material-ui/core/Avatar';
import { Emoji } from 'emoji-mart'
import helperFunction from '../../constants/helperFunction';
import Tooltip from '@material-ui/core/Tooltip';

const getDisplayString = (sub) => {
  const arr = sub.split("-");
  if (arr.length > 1) {
    return arr[0].charAt(0).toUpperCase() + arr[0].slice(1) + " " + arr[1].charAt(0).toUpperCase() + arr[1].slice(1)
  } else {
    return sub.charAt(0).toUpperCase() + sub.slice(1)
  }

};
const getUrlString = (path, sub, index) => {
  if (index === 0) {
    return '#/';
  } else {
    return '#/' + path.split(sub)[0] + sub;
  }
};

const ContainerHeader = ({ title, match, rating }) => {
  const path = match.path.substr(1);
  const subPath = path.split('/');
  return (
    <div className="page-heading d-sm-flex justify-content-sm-between align-items-sm-center">
     <Tooltip className='d-inline-block' id='tooltip-right' placement='left' title={
                      <h6 className="text-white"
                        style={{ marginTop: "13px" }}>
                        {title}
                      </h6>
                    }> 
                    <h2 className="title mb-3 mb-sm-0"style={{overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    maxWidth: '100%'}} >{title}</h2>
    </Tooltip>

      <Breadcrumb className="mb-0" tag="nav">
        {subPath.map((sub, index) => {
          if (sub === 'dashboard') {
            return <NavLink to="/seller/ratings">
              {/* <Avatar style={{backgroundColor:"#4caf50"}}><i className="zmdi zmdi-thumb-up zmdi-hc-fw rating text-white" /></Avatar> */}
              <div className="ratingSmileEmoji" >
              {/* <span class=" text-warning">😠</span> */}
              {
                rating && rating.length > 0 ? <i className = {helperFunction.getRatingValueForBuyer(Math.round(Number(rating[0].average)))} set={'google'} size={30} /> : ''
              }
           
              </div>
            </NavLink>
          }

          /* return <BreadcrumbItem active={subPath.length === index + 1}
                                 tag={subPath.length === index + 1 ? "span" : "a"} key={index}
                                 href={getUrlString(path, sub, index)}>{getDisplayString(sub)}</BreadcrumbItem> */
        }
        )}
      </Breadcrumb>
    </div>
  )
};

export default ContainerHeader;


import React, { Component } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import Grow from '@material-ui/core/Grow';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import CustomFilter from '../../../src/components/Filter/index';
import { Popover, PopoverBody, PopoverHeader } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import './index.css'
import { connect } from 'react-redux';
import { getShortbook } from 'actions/buyer';


const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
let date = new Date();


class EnhancedTableToolbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
            hide: true,
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
            searchText: '',
            hide: true
        });
    }

    handleResetFilter = (e, filter) => {
        e.preventDefault();
        // filter();
        this.setState({
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
        });
        let data = {
            page: 1,
            perPage: 50,
            tab: 'history',
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        }
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
        this.toggle()
    }

    componentDidMount(){
        this.setState({
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
        });
        let data = {
            page: 1,
            perPage: 50,
            tab: 'history',
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        }
        // this.props.getShortbook({ data, history: this.props.history })
    }


    onFilterChange = (e, key) => {
        console.log('onFilterChange', e.target, key)
        this.setState({ [key]: e.target.value, searchText: '' });
    }

    handleTextChange = (e) => {
        this.setState({searchText: e.target.value })
    }

    clearSearch = () => {
        this.setState({searchText:'', month: moment().format("MMMM"), year: moment().format("GGGG"), hide:true})
        let data = {
          page: 1,
          perPage: 50,
          searchText:'',
          tab:'history',
          month: this.state.month,
          year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
        }
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
    }
    applyFilter = () => {
        this.toggle();
        let data = {
            page: 1,
            perPage: 50,
            tab:'history',
            month: this.state.month,
            year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
        }
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
    }
    handleKeyPress = (e) => {
        if(e.keyCode === 13){
            this.applySearch()
        }
    }
    applySearch = () => {
        let data = {
          page: 1,
          perPage: 50,
          tab:'history',
          searchText:this.state.searchText,
          month: this.state.month,
          year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
        }
        console.log('shortbookListdatasaa',this.state.month)
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
    }
    render() {

        const { identifier } = this.props;

        return (

            <Toolbar>

                {
                    identifier === "shortbookFulfilled" ?

                        <React.Fragment>
                            {this.state.hide ? <h3  style={{marginRight: "0px", marginLeft:"-8px"}}>Showing Fulfilled Shortbook for {this.state.month} {this.state.year}</h3>:
                            <div style={{marginTop: "-10px", marginRight: "0px", marginLeft:"0px", width:"80%"}}>
                                <TextField
                                className='search'
                                autoFocus={true}
                                fullWidth ={true}
                                hidden = {this.state.hide}
                                value={this.state.searchText}
                                onChange={this.handleTextChange}
                                placeholder='Search Product'
                                onKeyUp={this.handleKeyPress}
                                />
                            </div>
                            }
                            <Grow appear in={true} timeout={300}>
                            <div className='searchBox' style={{marginTop: "-10px", marginRight: "0px", marginLeft:"auto"}}>
                                <IconButton className='clear'>
                                    { this.state.hide ? <SearchIcon onClick={() => {this.setState({hide: false})}}/> : <ClearIcon onClick={this.clearSearch}/> }
                                </IconButton>
                            </div>
                            </Grow>
                            <div style={{marginTop: "-10px"}}>
                                <IconButton aria-label="filter list">
                                    <FilterListIcon
                                        onClick={this.toggle}
                                        id="filter"
                                    />
                                </IconButton>
                            </div>


                            <Popover
                                trigger="legacy" className="SignInPopOver" placement="bottom" isOpen={this.state.popoverOpen} target="filter" toggle={this.toggle}
                            >
                                <h5  className="font-weight-bold" style={{color: '#000000de'}}>FILTERS</h5>

                                <CustomFilter
                                    onFilterChange={this.onFilterChange}
                                    handleResetFilter={this.handleResetFilter}
                                    month={this.state.month}
                                    applyFilter={this.applyFilter}
                                    year={this.state.year}
                                    filterFor='shortbookFulfilled' />

                            </Popover>
                        </React.Fragment>

                        :
                        null
                }
            </Toolbar>
        );
    }
};


const mapStateToProps = ({ buyer }) => {
    const { shortbookList } = buyer;
    return { shortbookList }
};


export default withRouter(connect(mapStateToProps, { getShortbook })(EnhancedTableToolbar));
import React, { Component } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grow from '@material-ui/core/Grow';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import './index.css'
import { connect } from 'react-redux';
import { getShortbook } from 'actions/buyer';
import AddIcon from '@material-ui/icons/Add';

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
let date = new Date();


class EnhancedTableToolbarOpen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
            hide: true,
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
            hide: true,
            searchText:''
        });
    }

    handleResetFilter = (e, filter) => {
        e.preventDefault();
        // filter();
        this.setState({
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: ''
        });
        let data = {
            page: 1,
            perPage: 50,
            tab:'open',
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        }
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
        this.toggle()
    }

    componentDidMount(){
        this.setState({
            popoverOpen: false,
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
            searchText: '',
            hide: true
        });
        let data = {
            page: 1,
            perPage: 50,
            tab:'open',
            month: moment().format("MMMM"),
            year: moment().format("GGGG"),
        }
        // this.props.getShortbook({ data, history: this.props.history })
    }


    onFilterChange = (e, key) => {
        this.setState({ [key]: e.target.value , searchText:''});
    }

    applyFilter = () => {
        this.toggle();
        let data = {
            page: 1,
            perPage: 50,
            tab:'open',
            month: this.state.month,
            year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
        }
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
    }
    
    handleTextChange = (e) => {
        this.setState({searchText: e.target.value })
    }

    clearSearch = () => {
        this.setState({searchText:'', month: moment().format("MMMM"), year: moment().format("GGGG"), hide:true})
        let data = {
          page: 1,
          perPage: 50,
          searchText:'',
          tab:'open',
          month: this.state.month,
          year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
        }
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
    }

    applySearch = () => {
        let data = {
          page: 1,
          perPage: 50,
          tab:'open',
          searchText:this.state.searchText,
          month: this.state.month,
          year: parseInt(this.state.year) ? parseInt(this.state.year) : ''
        }
        console.log('shortbookListdatasaa',this.state.month)
        this.props.filter(data)
        // this.props.getShortbook({ data, history: this.props.history })
    }

    handleKeyPress = (e) => {
        if(e.keyCode === 13){
            this.applySearch()
        }
    }

    render() {

        const { identifier } = this.props;

        return (

            <Toolbar>

                {
                    identifier === "shortbookList" ?

                        <React.Fragment>
                            {this.state.hide ? <h3  style={{marginRight: "0px", marginLeft:"-8px"}}>Showing Shortbook
                             {/* for {this.state.month} {this.state.year} */}
                             </h3>:
                            <div style={{marginTop: "-10px", marginRight: "0px", marginLeft:"0px", width:"80%"}}>
                                <TextField
                                className='search'
                                autoFocus={true}
                                fullWidth ={true}
                                hidden = {this.state.hide}
                                value={this.state.searchText}
                                onChange={this.handleTextChange}
                                placeholder='Search Product'
                                onKeyUp={this.handleKeyPress}
                                />
                            </div>
                            }
                            <Grow appear in={true} timeout={300}>
                            <div className='searchBox' style={{marginTop: "-10px", marginRight: "0px", marginLeft:"auto"}}>
                                <IconButton className='clear'>
                                    { this.state.hide ? <SearchIcon onClick={() => {this.setState({hide: false})}}/> : <ClearIcon onClick={this.clearSearch}/> }
                                </IconButton>
                            </div>
                            </Grow>
                            <div style={{marginTop: "-10px", marginRight: "0px", marginLeft:"0px"}}>
                                <IconButton aria-label="Add Icon">
                                    <AddIcon onClick={this.props.addClicked} />
                                </IconButton>
                            </div>
                        </React.Fragment>

                        :
                        null
                }

            </Toolbar>
        );
    }
};

const mapStateToProps = ({ buyer }) => {
    const { shortbookList } = buyer;
    return { shortbookList }
};

export default withRouter(connect(mapStateToProps, { getShortbook })(EnhancedTableToolbarOpen));
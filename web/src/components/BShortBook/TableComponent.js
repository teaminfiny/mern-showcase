import React, { Component } from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import './index.css'
import momemt from 'moment';
import { Link } from 'react-router-dom'

class TableComponent extends Component{

    constructor(props) {
        super(props)
    }

    render() {

        console.log('rows in table', this.props.row)
        return (
            <React.Fragment>
                
                <TableRow
                    hover
                    className='cursor-pointer'
                    key={this.props.row.productName}
                    align={'left'}
                >

                    <TableCell align={'left'} component={Link} to={`/search-results/category/search=${encodeURIComponent(this.props.row.productName)}`}>
                        {this.props.row.productName}
                    </TableCell>

                    <TableCell align={'left'} component={Link} to={`/search-results/category/search=${encodeURIComponent(this.props.row.productName)}`}>
                        {this.props.row.manufacturer}
                    </TableCell>
                    
                    <TableCell align={'left'} component={Link} to={`/search-results/category/search=${encodeURIComponent(this.props.row.productName)}`}>
                        {this.props.row.quantity}
                    </TableCell>
                    
                    <TableCell align={'left'} component={Link} to={`/search-results/category/search=${encodeURIComponent(this.props.row.productName)}`}>
                        <span>{momemt(this.props.row.requestedDate).format('DD/MM/YYYY')}</span>
                        <p style={{margin:0, padding:0}}>{momemt(this.props.row.requestedDate).format('HH:mm A')}</p>
                    </TableCell>
                    
                    <TableCell align={'left'} component={Link} to={`/search-results/category/search=${encodeURIComponent(this.props.row.productName)}`}>
                        {this.props.row.status}
                    </TableCell>
                    {this.props.identifier === 'shortbookList' ? <TableCell align="left">{this.props.row.action}</TableCell> : null}
                </TableRow>
            </React.Fragment>
        )
    }

}

export default TableComponent;
import React from 'react'
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';


const headCells = [
    { id: 'productName', numeric: false, disablePadding: true, label: 'Product Name' },
    { id: 'manufacturer', numeric: false, disablePadding: false, label: 'Manufacturer' },
    { id: 'quantity', numeric: true, disablePadding: false, label: 'Quantity' },
    { id: 'requestedDate', numeric: false, disablePadding: false, label: 'Date' },
    { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
    
  ];


function EnhancedTableHead(props) {
    const { classes, identifier} = props;
    return (
      <TableHead>
        <TableRow>
          
          {headCells.map(headCell => (
            <TableCell
              key={headCell.id}
              align={'left'}
              padding={'default'}
            >
                {headCell.label}
            </TableCell>
          ))}
          {identifier === 'shortbookList' ? <TableCell key='action' align={'left'} padding={'default'} >Action</TableCell> : null }
        </TableRow>
      </TableHead>
    );
  }


export default EnhancedTableHead;
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import { changeTab } from '../../actions/tabAction';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'
import moment from 'moment';
import CustomScrollbars from 'util/CustomScrollbars';
import { getShortbook } from 'actions/buyer';
import ShortBook from './ShortBook';
import IntlMessages from 'util/IntlMessages';
import ContainerHeader from 'components/ContainerHeader';

function TabContainer({children, dir,index,value}) {
  return (
    <div dir={dir}>
      {value==index&&children}
    </div>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

class FullWidthShortbookTab extends Component {
  constructor(props){
    super(props);
    this.state = {
    value: 0,
    tabValue:0,
    page: 1,
    perPage:50,
    rowsPerPage: 50,
    month:moment().format("MMMM"),
    year: moment().format("GGGG")
    };
    this.myRef = React.createRef()
  }
  
  componentDidMount(){ 
    this.myRef.current.scrollIntoView({ behavior: 'smooth' })

  }

  handleChange = (event, value) => {
    this.setState({tabValue:value})
  };

  handleChangeIndex = index => {
    this.setState({tabValue:index})
  };

  render() {


    const {theme, tabFor, shortbookList} = this.props;
    const { tabValue } = this.state
    return (
      <div className="app-wrapper" ref={this.myRef}>
        <div className="row">

          <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <ContainerHeader match={this.props.match} title={<IntlMessages id="My Shortbook" />} />
          </div>

      <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">

          <TabContainer dir={theme.direction} value = {0} index= {0}>

            <CustomScrollbars className="messages-list scrollbar" style={{ height: 500 }}>
              <ShortBook
                history={this.props.history}
                // shortbookFromParent={shortbookList}
                value={this.state}
                identifier={'shortbookList'}
              />
            </CustomScrollbars>
          </TabContainer>
      </div>
      </div>
      </div>
    );
  }
}

FullWidthShortbookTab.propTypes = {
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = ({tabAction,buyer}) => {
  const {tabValue} = tabAction;
  const { shortbookList } = buyer;
  return { tabValue, shortbookList}
};

export default withRouter(connect(mapStateToProps, {changeTab, getShortbook})( withStyles(null, {withTheme: true})(FullWidthShortbookTab)));
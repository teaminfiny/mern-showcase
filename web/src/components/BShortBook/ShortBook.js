import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';
import {Button} from '@material-ui/core'
import TableComponent from './TableComponent';

import { withStyles } from '@material-ui/core/styles';

import EnhancedTableHead from './EnhancedTableHead'
import EnhancedTableToolbar from './EnhancedTableToolbar'
import EnhancedTableToolbarOpen from './EnhancedTableToolbarOpen'
import CircularProgress from '@material-ui/core/CircularProgress';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import axios from 'constants/axios';
import { getShortbook } from 'actions/buyer';
import ShortBookPopOver from 'app/routes/bSearchResults/ShortBookPopOver'
import { NotificationManager } from 'react-notifications';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const useStyles = theme => ({
  root: {
    width: '100%',
    marginTop: 0,
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 400,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  list: {
    maxWidth: 239
  },
  fontSize: {
    fontSize: '1rem'
  }
});
let tabs ='';
class ShortBook extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      rowsPerPage: 50,
      perPage:50,
      month: moment().format("MMMM"),
      year: moment().format("GGGG"),
      popOverOpen: false,
      cancelPopOver: false,
      cancelData : {}
    }
    tabs = this.props.identifier == 'shortbookFulfilled' ? 'history' :'open';
  }

  handleChangePage = async(event, newPage) => {
    console.log('safomaeovmac',event, newPage)
    await this.setState({ page: newPage  });
    let { month, year} = this.state;
    let page = this.state.page == 0 ? 1 : this.state.page + 1
    let data = {page, perPage: 5, month, year, tab: tabs};

    this.props.getShortbook({history:this.props.history, data});
  };

  // handleChangeRowsPerPage = event => {
  //   this.setState({
  //     rowsPerPage: parseInt(event.target.value, 10),
  //     page: 0
  //   })
  // };

  addClicked = () => {
    this.setState({popOverOpen:!this.state.popOverOpen})
  }

  toggle = () => {
    this.setState({popOverOpen:!this.state.popOverOpen})
  }

  onPopOverClosed = () =>{
    let data = {
        page: 1,
        perPage: 50,
        tab: tabs,
        month: moment().format("MMMM"),
        year: moment().format("GGGG"),
    }
    this.props.getShortbook({ data, history: this.props.history })
  }

  cancelClicked = (data) =>{
    this.setState({cancelPopOver:true, cancelData:data})
  }

  closeCancel = () =>{
    this.setState({cancelPopOver:false, cancelData:{}})
  }

  componentDidMount(){ 
    let {  month, year, perPage} = this.state;
    let data = {page: 1, perPage, month, year, tab: tabs};
    console.log('asdnadfesgsg',data)
    this.props.getShortbook({history:this.props.history, data});

  }

  confirmCancel = async() => {
    let token = localStorage.getItem('buyer_token');
    let changedData={
      id : this.state.cancelData._id,
    }
      await axios({
        method: 'POST',
        url:'shortBook/removeShortBook',
        data:changedData,
        headers: {
            'Content-Type': 'application/json',
            'token': token
        }
      }).then(result => {
        if (result.data.error) {
          NotificationManager.error(result.data.title)
        } else {
          NotificationManager.success(result.data.title)
          let data = {
              page: this.state.page == 0 ? 1 : this.state.page,
              perPage: 50,
              tab:'open',
              month: this.state.month,
              year: this.state.year,
          }
          this.props.getShortbook({ data, history: this.props.history })
        }
      })
        .catch(error => {
          NotificationManager.error('Something Went Wrong !')
        });
    this.setState({cancelPopOver:false, cancelData:{}})
  }

  filter = async(data)=> {
    console.log('shortbookListdata',data)
    this.setState({page:0, month:data.month, year:data.year})
    this.props.getShortbook({ data, history: this.props.history })
  }
  render() {
    const { classes, identifier } = this.props;


    const statusStyle = (status) => {
      return status.includes('OUT OF STOCK') ? "text-white bg-danger" : status.includes("IN-STOCK") ? "text-white bg-primary" : status.includes("FULFILLED") ? "text-white bg-success": "text-white bg-primary";
    }

    const { shortbookList } = this.props;
    let counts = shortbookList && shortbookList.data.metadata[0] && shortbookList.data.metadata[0].total ? shortbookList.data.metadata[0].total : 0;
    let rows = []

    shortbookList && shortbookList.data && shortbookList.data.result && shortbookList.data.result.map((dataOne, index) => {
      rows.push({
        productName: dataOne.product_name,
        manufacturer: dataOne.manufacturer ? dataOne.manufacturer.name : 'N/A',
        quantity: Number(dataOne.quantity),
        status: <div key={'recent'}
          className={` badge text-uppercase 
                     ${statusStyle(
            dataOne.isFulfilled ? "FULFILLED"
            :
            dataOne.inStock ?
            "IN-STOCK"
            :
            'OUT OF STOCK'
            )}`}>
          {
          dataOne.isFulfilled ? "FULFILLED" : dataOne.inStock ? "ADD TO CART" : 'OUT OF STOCK'
          }
        </div>,
        requestedDate: (dataOne.createdAt),
        action: <div>{identifier === "shortbookList" ? <Button variant="outlined" className="text-danger" onClick={() => this.cancelClicked(dataOne)}>REMOVE</Button> : ''}</div>
      })
    })

    let productData = shortbookList ? shortbookList.data ? shortbookList.data : [] : []

    return (
      <React.Fragment>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          {identifier === "shortbookFulfilled" ?
            <EnhancedTableToolbar identifier={identifier} filter={(e)=>this.filter(e)}/>
            :
            identifier === "shortbookList" ?
            <div>
                <ShortBookPopOver 
                  open= {this.state.popOverOpen}
                  toggle = {this.toggle}
                  onAdd = {this.onPopOverClosed}
                />
              <EnhancedTableToolbarOpen identifier={identifier} addClicked={this.addClicked} filter={(e)=>this.filter(e)}/>
            </div>
            :
            null
          }
          {
            this.props.loading ?   
              <div className="loader-view">
                <CircularProgress />
              </div>
            :
          <div className={classes.tableWrapper}>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                identifier = {identifier}
                rowCount={rows.length}
              />

              <TableBody>
                {rows.length < 1 ? <TableRow><TableCell colSpan={6} style={{textAlign:'center'}}>Sorry, no matching records found !</TableCell></TableRow> :
                rows.map((row, index) => {
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (

                      <TableComponent
                        row={row}
                        labelId={labelId}
                        value={this.props.value}
                        identifier = {identifier}
                        itemListFromParent={productData} />

                    )
                  })}

              </TableBody>
            </Table>
          </div>
          }
          <TablePagination
            rowsPerPageOptions={[]}
            component="div"
            count={counts}
            rowsPerPage={this.state.perPage}
            page={this.state.page}
            backIconButtonProps={{
              'aria-label': 'previous page',
            }}
            nextIconButtonProps={{
              'aria-label': 'next page',
            }}
            onChangePage={this.handleChangePage}
            // onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>

      </div>
      {/*------------------------- Confirm Cancel Dialog ------------------------------------------ */}
      <Dialog open={this.state.cancelPopOver} onClose={this.closeCancel} fullWidth={true}
          maxWidth={'sm'}>
          <DialogTitle>
            Remove Shortbook Product
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              {'Are you sure you want to remove this product from shortbook ?'}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() =>this.closeCancel()} color='secondary' >
              No
            </Button>
            <Button color='primary' onClick={() =>this.confirmCancel()} >
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { loading, shortbookList } = buyer;
  return { loading, shortbookList }
};

export default withRouter(connect(mapStateToProps, { getShortbook })(withStyles(useStyles)(ShortBook)))
import React, { Component } from 'react';

import { NavLink, withRouter } from 'react-router-dom';


class SecondaryHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div className="secondaryHeaderWrapper">
                <div className="secondaryHeaderMain">
                    <h5 className="dealsOfTheDay" style={{ cursor: "pointer", paddingLeft: "30px", float: "left" }}><a onClick={this.scrollToDeals}>Deals Of The Day   </a></h5>
                    <h5 className="dealsOfTheDay" style={{ cursor: "pointer", float: "left" }}><a onClick={this.scrollToFast}>Fast Moving Products   </a></h5>
                    <h5 className="dealsOfTheDay" style={{ cursor: "pointer", float: "left" }}><a onClick={this.scrollToProductsForYou}>Products For You </a></h5>
                    <h5 className="sellOnMedideals" style={{ float: 'right' }}> <NavLink to={"/signup/"}>Sell On Medimny </NavLink> <i class="zmdi zmdi-phone ml-1"></i> +91 9321927004</h5>
                </div>
            </div>
         );
    }
}
 
export default withRouter(SecondaryHeader);
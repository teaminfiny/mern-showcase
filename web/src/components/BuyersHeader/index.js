import React from 'react';
import { Link, withRouter, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

import { Dropdown, DropdownMenu, DropdownToggle, Row, Col } from 'reactstrap';
import {
  BELOW_THE_HEADER,
  COLLAPSED_DRAWER,
  FIXED_DRAWER,
  HORIZONTAL_NAVIGATION,
  INSIDE_THE_HEADER,
  GET_MARK_READ_LIST_FOR_BUYER
} from 'constants/ActionTypes';
import { userSignOut } from 'actions/Auth';
import SearchBox from 'components/SearchBox';
import MailNotification from '../MailNotification/index';
import AppNotification from '../AppNotification/index';
import CardHeader from 'components/dashboard/Common/CardHeader/index';
import Badge from '@material-ui/core/Badge';
import { switchLanguage, toggleCollapsedNav } from 'actions/Setting';
import IntlMessages from 'util/IntlMessages';
import LanguageSwitcher from 'components/LanguageSwitcher/index';
import Menu from 'components/TopNav/Menu';
import UserInfoPopup from 'components/UserInfo/UserInfoPopup';
import './index.css'
import { isMobile } from 'react-device-detect';

import CartNotification from 'app/routes/myCart/CartNotification';
import { getUserDetail } from 'actions';
import { getMarkReadForBuyer } from 'actions/buyer';
import { Popover, PopoverBody, PopoverHeader } from 'reactstrap';
import SignInPopOver from './SignInPopOver';
import { getMediWallet, getShortProducts } from 'actions/buyer';
import { getAutoFill } from 'actions/buyer';
import { getSearchProduct } from 'actions/buyer';
import { getCartDetails } from 'actions/buyer';
import IntegrationAutosuggest from './IntegrationAutosuggest';
import CardBox from 'components/CardBox/index';

import {
  addToCart,
  getCartDetailsSuccess,
} from 'actions/buyer'
import { showAuthMessage, userSignInSuccess, userSignOutSuccess, userSignUpSuccess } from "actions/Auth";
import AxiosRequest from 'sagas/axiosRequest'
import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import AppConfig from 'constants/config'
import axios from 'axios';
import {  getProductsForYou } from 'actions/buyer';

import moment from 'moment';

import Chip from '@material-ui/core/Chip';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import { withStyles } from "@material-ui/styles";

import './secondaryHeader.css'
import helperFunction from '../../constants/helperFunction';

import { getBuyerNotification } from '../../actions/notification';


import { createFilterOptions } from '@material-ui/lab/Autocomplete';
import MenuBookIcon from '@material-ui/icons/MenuBook';

const stylesFn = () => ({
  input: {
    color: "blue"
  },
  focused: {
    background: "red"
  }
});

class Header extends React.Component {


  onAppNotificationSelect = () => {
    this.props.getUserDetail({userType:"buyer"})
    this.setState({
      appNotification: !this.state.appNotification
    })
  };

  onAppShoppingCartSelect = () => {
    this.props.getUserDetail({userType:"buyer"})
    this.setState({
      appShoppingCart: !this.state.appShoppingCart
    })
  };

  onMailNotificationSelect = () => {
    this.setState({
      mailNotification: !this.state.mailNotification
    })
  };
  onLangSwitcherSelect = (event) => {
    this.setState({
      langSwitcher: !this.state.langSwitcher, anchorEl: event.currentTarget
    })
  };
  onSearchBoxSelect = () => {
    this.setState({
      searchBox: !this.state.searchBox
    })
  };
  onAppsSelect = () => {
    this.props.getUserDetail({userType:"buyer"})
    this.setState({
      apps: !this.state.apps
    })
  };
  onUserInfoSelect = () => {
    this.setState({
      userInfo: !this.state.userInfo
    })
  };
  handleRequestClose = () => {
    this.setState({
      langSwitcher: false,
      userInfo: false,
      mailNotification: false,
      appNotification: false,
      appShoppingCart: false,
      searchBox: false,
      apps: false
    });
  };
  onToggleCollapsedNav = (e) => {
    const val = !this.props.navCollapsed;
    this.props.toggleCollapsedNav(val);
  };
  handleLogout = () => {
    this.onAppsSelect()
    this.props.history.push('/')
    this.props.userSignOut()
    localStorage.clear();
    setTimeout(function () {
      window.location.reload();
    }, 500)
  }


  toggle = () => {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  toggleWallet = () => {
    this.setState({
      walletPopOverOpen: !this.state.walletPopOverOpen
    });
  }

  Apps = () => {
    return (
      <React.Fragment>
        { !localStorage.getItem('asAdmin') ? 
        <ul className="jr-list w-100 mx-auto mb-1">
          <li className="jr-list-item w-100 ">
            <Link className="jr-list-link buyerHeaderLink" to="/profile" onClick={this.onAppsSelect.bind(this)}>
              <i className="zmdi zmdi-account zmdi-hc-fw" />
              <span className="jr-list-text">Profile</span>
            </Link>
          </li>
        </ul>:''}
        { !localStorage.getItem('asAdmin') ? 
        <ul className="jr-list w-100 mx-auto mb-1">
          <li className="jr-list-item w-100 ">
            <Link className="jr-list-link buyerHeaderLink" to={{pathname: "/profile",title: 'MediWallet'}} onClick={this.onAppsSelect.bind(this)}>
              <i className="zmdi zmdi-balance-wallet zmdi-hc-fw" />
              <span className="jr-list-text">MNY {this.props.users ?  (this.props.users && this.props.users.wallet_balance).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}) + "/-" : null}</span>
            </Link>
          </li>
        </ul> :
        <ul className="jr-list w-100 mx-auto mb-1">
          <li className="jr-list-item w-100 ">
            <Link className="jr-list-link buyerHeaderLink">
            <i className="zmdi zmdi-balance-wallet zmdi-hc-fw" />
            <span className="jr-list-text">MNY {this.props.users ?  (this.props.users && this.props.users.wallet_balance).toLocaleString('en-IN',{maximumFractionDigits:2,minimumFractionDigits:2}) + "/-" : null}</span>
            </Link>
          </li>
        </ul>}
         
        <ul className="jr-list w-100 mx-auto">
          <li className="jr-list-item w-100 ">
            <a href='' className="jr-list-link buyerHeaderLink" onClick={this.handleLogout}>
              <i className="zmdi zmdi-sign-in  zmdi-hc-fw" />
              <span className="jr-list-text">Logout</span>
            </a>
          </li>
        </ul>
      </React.Fragment>)
  };

  constructor(props) {
    super(props);
    let url = props.location.search
    let json = url ? JSON.parse('{"' + decodeURIComponent(url.substring(1).replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}') : ''
    this.state = {
      anchorEl: undefined,
      searchBox: false,
      searchText: '',
      mailNotification: false,
      userInfo: false,
      langSwitcher: false,
      appNotification: false,
      appShoppingCart: false,

      popoverOpen: false,

      walletPopOverOpen: false,

      value: 0,
      tabValue: 0,

      edit: false,
      hide: false,
      delete: false,
      add: false,
      page: 1,
      perPage: 12,
      filter: '',
      month: moment().format('MMMM'),
      year: moment().format('YYYY'),

      searchClicked: false,

      data: null,
      isLoaded: false,
      itemSelected: false,
      inputVal: "",
      options: [],
      selectedOption: "",
      id: json ? json.Id ? json.Id : '':'',
      admin: json ? Boolean(json.admin) ? Boolean(json.admin) : false:false,
    }
    this.updateState = this.updateState.bind(this);
    this.onChangeAutoCompleteSearch = this.onChangeAutoCompleteSearch.bind(this);
    this.updateSearchText = this.updateSearchText.bind(this);
  }

  handleClose = (e) => {
    this.setState({ appNotification: false, appShoppingCart: false, })
  }

  onChangeAutoCompleteSearch = (event, value) => {

    if (event.type === "keydown" || event.type === "click") {
      let url2 = this.props.location.pathname;
      let strip = url2.split('/');
      let catN = strip ? strip[3] ? strip[3] : 'category' : 'category' ;
      let obj = { search: value, company: '', seller: '', discount: '',sortBy:'price' }
      let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
      let catName =  catN && ((catN).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()
      // this.props.history.push(`/search-results/${url}`) 
      this.props.history.push({ pathname: `/search-results/${catName}/${url}`, state: 'searchPage' })
      const data = {
        key: obj.search ? obj.search : '',
        company_id: obj.company_id ? obj.company_id : '',
        category_id: obj.category_id ? obj.category_id : [],
        seller_id: obj.seller_id ? obj.seller_id : '',
        page: 1,
        perPage: 12,sortBy:'price'
      }
      this.props.getSearchProduct({ data })
    }
  }

  updateSearchText = (evt, value) => {
    let search2 = (value).replace(/[^a-zA-Z0-9]/g,'')
    this.props.getAutoFill({ key: search2 })
    const { autoFill, searchOptions } = this.props;
    this.setState({
      searchText: value,
    }
      // , () => {
      //   this.onChangeAutoCompleteSearch(evt.type)
      // }
    );
  }


  handleSigninOnEnter = (e) => {
    let url2 = this.props.location.pathname;
    let strip = url2.split('/');
    let catN = strip ? strip[3] ? strip[3] : 'category' : 'category' ;
    let catName =  catN && ((catN).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()
    if (e.key === 'Enter' && this.state.searchText !== '') {
      let obj = { search: this.state.searchText, company: '', seller: '', discount: '' ,sortBy:'price'}
      let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
      // this.props.history.push(`/search-results/${url}`) 
      this.props.history.push({ pathname: `/search-results/${catName}/${url}`, state: 'searchPage' })
      const data = {
        key: obj.search ? obj.search : '',
        company_id: obj.company_id ? obj.company_id : '',
        category_id: obj.category_id ? obj.category_id : [],
        seller_id: obj.seller_id ? obj.seller_id : '',
        page: 1,
        perPage: 12,sortBy:'price'
      }
      this.props.getSearchProduct({ data })
    }

    if (e.key === 'Enter' && this.state.searchText === '') {
      
      let obj = { search: '', company: '', seller: '', discount: '',sortBy:'price' }
      let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
      this.props.history.push({ pathname: `/search-results/${catName}/${url}`, state: 'searchPage' })
      const data = {
        key: '',
        company_id: obj.company_id ? obj.company_id : '',
        category_id: obj.category_id ? obj.category_id : [],
        seller_id: obj.seller_id ? obj.seller_id : '',
        page: 1,
        perPage: 12,sortBy:'price'
      }
      this.props.getSearchProduct({ data })
    }
  }

  updateState(e) {
    this.setState({ searchText: e.target.textContent, itemSelected: true });
    let url2 = this.props.location.pathname;
    let strip = url2.split('/');
    let catN = strip ? strip[3] ? strip[3] : 'category' : 'category' ;
    if (this.state.searchText !== '') {
      let obj = { search: e.target.textContent }
      let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
      // this.props.history.push(`/search-results/${url}`)  
      let catName =  catN && ((catN).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()
      this.props.history.push({ pathname: `/search-results/${catName}/${url}`, state: 'searchPage' })
      const data = {
        key: e.target.textContent ? e.target.textContent : '',
        company_id: obj.company_id ? obj.company_id : '',
        category_id: obj.category_id ? obj.category_id : [],
        seller_id: obj.seller_id ? obj.seller_id : '',
        page: 1,
        perPage: 12
      }
      this.props.getSearchProduct({ data })
    }
  }

  encodeUrl = (url) => new URLSearchParams(url).toString()

  componentDidMount=async()=> {
    let authBuyer = localStorage.getItem('buyer_token')
    if (authBuyer) {
      this.props.getUserDetail({ userType: 'buyer' })
      let data = {
        page: 1,
        perPage: 10,
        filter: '',
        month: this.state.month,
        year: this.state.year
      }
      this.props.getMediWallet({ history: this.props.history, data })
      this.props.getBuyerNotification({ email: '', password: '', user_type: 'buyer', page: 1, perPage: 10 })
    }
  }

  componentDidUpdate(prevProps, prevState) {    
    if(prevProps.location.state !== ''){
      if(prevProps.location.state !== this.props.location.state){
        if(this.props.location.state !== 'searchPage'){
          this.clearSearch()
        }      
      }
    }
  }

  callClear = () => {
    if(this.props.location.state !== 'searchPage'){
      this.clearSearch()
    }
  }


  clearSearch = () => {
    this.setState({ searchText: '' });
  }
  handleChange = (e) => {
    this.props.getMarkReadForBuyer({})
  }

  render() {
    const { getMarkReadListForBuyer } = this.props;
    const { mediWallet, cartDetails, unreadNotificationCount, notificationData, unReadNotification  } = this.props;
    let totalBalance = 0;
    const totalWalletBalance = mediWallet && mediWallet.details && mediWallet.details.length > 0 && mediWallet.details[0].data.map((total, value) => {
      return (totalBalance = totalBalance + total.settle_amount)
    })
    const walletBalance = totalWalletBalance && totalWalletBalance[totalWalletBalance.length - 1]

    let authBuyer = localStorage.getItem('buyer_token')
    // let  checkUser = localStorage.getItem('user_type')
    const { drawerType, locale, navigationStyle, horizontalNavPosition } = this.props;
    const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'd-block d-xl-none' : drawerType.includes(COLLAPSED_DRAWER) ? 'd-block' : 'd-none';
    const { autoFill, searchOptions } = this.props;
    const inputProps = {
      paddingTop: 0,
    };

    if(!authBuyer){
      localStorage.clear();
    }
    const filter = createFilterOptions();

    let filtered = []

    return (
      <AppBar
        color='default'
        className={`app-main-header app-main-headerB noShadow ${(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === BELOW_THE_HEADER) ? 'app-main-header-top' : ''}`}>
       <marquee attribute_name = "attribute_value" scrollamount="3%" attributes className='text-white bg-primary'>
       Medimny is open for orders and we are accepting orders in all serviceable locations in India. There might be a delay due to the COVID-19 epidemic and further delay can be expected for areas under containment or lockdown. Contact us for any issues – helpdesk@medimny.com 
       </marquee>
        {isMobile === false ?
          <div className="app-toolbarB" disableGutters={false}>
            <Row className='d-flex'>

              <Col sm={3} md={3} lg={3} xl={3}>
                <Link className="app-logo app-logoB mr-2 " to="/">
                  <img src={require("assets/images/medimny.png")} alt="Medimny" title="Medimny"/>
                </Link>
              </Col>

              <Col sm={5} md={5} lg={5} xl={5}>
                {/* <SearchBox styleName="d-lg-block" placeholder=""
                  onChange={this.updateSearchText.bind(this)}
                  handleSigninOnEnter = {this.handleSigninOnEnter.bind(this)}
                  value={this.state.searchText} handleClick = {this.handleClick} /> */}

                <div className="search-bar right-side-icon bg-transparent">
                  <div className="form-group search-container">

                    <Autocomplete
                      style={{ marginTop: "2px", paddingTop: "0px !important", borderStyle:'solid' ,borderColor: "#072791",borderWidth: "2px" }}
                      variant='outlined'
                      className="autoCompleteSearch"
                      id="free-solo-demo"
                      filterSelectedOptions
                      filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                
                        if (params.inputValue !== '') {
                          filtered.unshift(params.inputValue);
                        }
                
                        return filtered;
                      }}
                      

                      autoHighlight
                      disableClearable
                      clearOnEscape
                      freeSolo
                      // disableOpenOnFocus
                      inputValue={this.state.searchText} //---------- USE inputValue ----------------- 
                      
                      onInputChange={(event, val) => this.updateSearchText(event, val)}
                      onKeyPress={this.handleSigninOnEnter.bind(this)}
                      onChange={(event, val) => this.onChangeAutoCompleteSearch(event, val)}                                   

                      options={ 'asdsd', searchOptions && searchOptions.map(option => option.word)}
                      
                      getOptionLabel={(option) => {
                        // e.g value selected with enter, right from the input
                        if (typeof option === 'string') {
                          return option;
                        }
                        if (option.inputValue) {
                          return option.inputValue;
                        }
                        return option.inputValue;
                      }}
                      
                      renderInput={params => (
                        <div>
                          <TextField
                            name="searchTextBoxField"
                            className="searchTextField"
                            {...params}
                            fullWidth
                            variant='outlined'
                            placeholder="Search Medicine"
                          />

                        </div>
                      )}
                    />

                  </div>
                </div>

              </Col>
              <div style={{ textAlign: "right", color: "#072791", marginLeft: "-46px", zIndex: "5000", marginTop: "8px" }}>
                <i className="zmdi zmdi-search  zmdi-hc-2x"></i>
              </div>

              <Col sm={4} md={4} lg={4} xl={4}>
                <Row >
                  <Col md={5} lg={5} xl={7} className="cartIcon pr-0">
                    {/* <Badge className={'icon-btn text-primary '} badgeContent={4} color="secondary">
                      <i className="zmdi zmdi-shopping-cart   shopping-cart " />
                    </Badge> */}
                    <Row className='justify-content-center ml-2'>
                      {/*<Col md={4} lg={4} xl={4}>
                        <span>
                          <i className="zmdi zmdi-balance zmdi-hc-fw" />
                        </span>
                    </Col>*/}


                      {!this.props.users ? null :

                        <Col md={4} lg={4} xl={4} style={{textAlign:'center'}}>
                              <Dropdown
                                className="quick-menu"
                                isOpen={this.state.appNotification}
                                toggle={this.onAppNotificationSelect.bind(this)}>

                                <DropdownToggle
                                  className="d-inline-block"
                                  tag="span"
                                  data-toggle="dropdown">

                                  {unReadNotification && unReadNotification.length > 0 ?
                                    <IconButton className="icon-btn text-primary" onClick={(e) => this.handleChange()}>
                                      <i className="zmdi zmdi-notifications icon-alert" />
                                    </IconButton>
                                    :
                                    <IconButton className="icon-btn text-primary ">
                                      <i className="zmdi zmdi-notifications" />
                                      
                                    </IconButton>
                                  }
                                <span className='text-primary' style={{fontSize:'12px',fontWeight:'bold'}}>Notification</span>
                                </DropdownToggle>

                                <DropdownMenu right>
                                  <CardHeader styleName="align-items-center"
                                    heading={<IntlMessages id="appNotification.title" />} />
                                  <AppNotification
                                    notificationData={notificationData}
                                    type='buyer'
                                  />
                                </DropdownMenu>
                              </Dropdown>
                        </Col>
                      }
                      { !this.props.users ?
                        null
                        :
                        <Col md={4} lg={4} xl={4} style={{textAlign:'center'}}>
                                    <NavLink to={{
                                      pathname: "/shortbook",
                                      title: 'My Shortbook'
                                    }} >
                                      <IconButton className="icon-btn text-primary" style={{ padding: '4px'}}>
                                        <MenuBookIcon />
                                      </IconButton>
                                    </NavLink><br/>
                                    <span className='text-primary' style={{fontSize:'12px',fontWeight:'bold'}}>ShortBook</span>
                        </Col>

                      } 

                      {!this.props.users ? null :
                        <Col md={4} lg={4} xl={4} style={{textAlign:'center'}}>
                              <Dropdown
                                className="quick-menu"
                                isOpen={this.state.appShoppingCart}
                                toggle={this.onAppShoppingCartSelect.bind(this)}>

                                <DropdownToggle
                                  className="d-inline-block"
                                  tag="span"
                                  data-toggle="dropdown">
                                  {cartDetails && cartDetails.detail && cartDetails.detail.length > 0 ?
                                    <IconButton className="icon-btn text-primary">
                                      <i className="zmdi zmdi-shopping-cart icon-alert" />
                                    </IconButton>
                                    :
                                    <IconButton className="icon-btn text-primary">
                                      <i className="zmdi zmdi-shopping-cart" />
                                    </IconButton>
                                  }<br/>
                                  <span className='text-primary' style={{fontSize:'12px',fontWeight:'bold'}}>
                                    MyCart</span>
                                </DropdownToggle>

                                <DropdownMenu right className="pb-0">
                                  <CardHeader styleName="align-items-center"
                                    heading={<IntlMessages id="My Cart" />} />
                                  <CartNotification  history={this.props.history} />
                                  {cartDetails && cartDetails.detail && cartDetails.detail.length > 0 ?
                                    <NavLink tag={Link} to="/MyCart" className={'text-white NavLink'} onClick={(e) => this.handleClose(e)}>
                                      <div className="orderFooter">

                                        <span>Go To Cart</span>

                                      </div></NavLink>
                                    :
                                    <NavLink className="buyerRedirectNavlink" to={{ pathname: '/login', state: '/' }}>
                                      <div className="orderFooter">
                                        <span style={{ color: 'white', cursor: 'default' }}>Add some items</span>
                                      </div></NavLink>
                                  }

                                </DropdownMenu>
                              </Dropdown>
                          {/* <Link to="/seller/list-Notification">
                          <IconButton className="icon-btn text-primary text-white quick-menu">
                            <i className="zmdi zmdi-shopping-cart-plus  icon-alert animated infinite  shopping-cart " />
                          </IconButton>
                        </Link> */}
                        </Col>
                      }
                    </Row>
                  </Col>



                  {!this.props.users ?
                    <Col md={7} lg={6} xl={5} className="header-notifications">
                      <ul className="header-notifications list-inline pull-right mr-1">
                        <li className="list-inline-item">

                          <div className="">
                            <button id="signInPopOver" onClick={this.toggle}
                              style={{
                                marginTop: 3,
                                backgroundColor: "#072791",
                                color: "#ffffff",
                                border: "none",
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 7,
                                paddingBottom: 7,
                                borderRadius: "5px",
                              }}>
                              LOGIN
                              </button>

                            <Popover trigger="legacy" className="SignInPopOver" placement="bottom" isOpen={this.state.popoverOpen} target="signInPopOver" toggle={this.toggle}>
                              <i class="zmdi zmdi-close zmdi-hc-2x" style={{ float: "right", cursor: "pointer" }} onClick={this.toggle}></i>
                              <SignInPopOver />
                            </Popover>
                          </div>
                        </li>
                      </ul>
                    </Col>
                    :
                    null}


                  {!this.props.users ?
                    null
                    :
                    <Col md={7} lg={6} xl={5} className="header-notifications ">
                      <ul className="header-notifications list-inline ">
                        <li className="list-inline-item">
                          <Dropdown
                            className="quick-menu app-notification text-white ml-4"
                            isOpen={this.state.apps}
                            toggle={this.onAppsSelect.bind(this)}>
                            <DropdownToggle
                              className="d-inline-block"
                              tag="span"
                              data-toggle="dropdown">
                              <span className="app-notification-menu bg-primary">
                                <i className="zmdi zmdi-apps zmdi-hc-fw zmdi-hc-lg icon" />
                                <span className='mt-1'>
                                  {/* {`${this.props.users.first_name}`} */}
                                  {helperFunction.getNameInitials(`${this.props.users.first_name} ${this.props.users.last_name}`) }

                                </span>
                              </span>
                            </DropdownToggle>
                            <DropdownMenu>
                              {this.Apps()}
                            </DropdownMenu>
                          </Dropdown>
                        </li>
                      </ul>
                    </Col>
                  }


                </Row>
              </Col>
            </Row>
          </div>

          :  //IF MOBILE TRUE--------------------------------------------------------------------------------

          <div className="mobileview-app-toolbar" disableGutters={false}>
            <Row className='d-flex'>

              <Col sm={1} xs={1} >
              </Col>

              <Col sm={3} xs={3} className="pt-1" >
                <Link className="app-logo app-logoB mr-2 " to="/">
                  <img src={require("assets/images/medimny.png")} alt="Medimny" title="Medimny" />
                </Link>
              </Col>

              <Col sm={8} xs={8}>
                <Row style={{ float: "right" }}>

                  {!this.props.users ? null :
                    <Col sm={2} xs={2} className="cartIcon pl-5 pr-4">
                      <ul className='header-notifications list-inline '>
                        <li className="list-inline-item app-tour">
                          <Dropdown
                            className="quick-menu"
                            isOpen={this.state.appNotification}
                            toggle={this.onAppNotificationSelect.bind(this)}>

                            <DropdownToggle
                              className="d-inline-block"
                              tag="span"
                              data-toggle="dropdown">
                              {unReadNotification && unReadNotification.length > 0 ?
                                <NavLink to="/list-Notification" className={'text-black NavLink'} onClick={(e) => this.handleClose(e)}>
                                  <IconButton className="icon-btn text-primary" onClick={(e) => this.handleChange()}>
                                    <i className="zmdi zmdi-notifications icon-alert" />
                                  </IconButton>
                                </NavLink>
                                    :
                                <NavLink to="/list-Notification" className={'text-black NavLink'} onClick={(e) => this.handleClose(e)}>
                                
                                    <IconButton className="icon-btn text-primary">
                                      <i className="zmdi zmdi-notifications" />
                                    </IconButton>
                                </NavLink>                                
                              }
                            </DropdownToggle>
                            {/* onClick={(e)=>this.handleChange()} */}


                            {/* <DropdownMenu positionFixed >
                              <CardHeader styleName="align-items-center"
                                heading={<IntlMessages id="appNotification.title" />} />
                              <AppNotification
                                notificationData={notificationData}
                              />
                            </DropdownMenu> */}

                            {/* <DropdownMenu>
                              <CardHeader styleName="align-items-center"
                                heading={<IntlMessages id="appNotification.title" />} />
                              <AppNotification /> */}
                              {/* <div style={{ padding: 10, borderTop: '1px solid #dee2e6', textAlign: 'center', zIndex: 10, margin: '0 -10px' }}>
                                <NavLink to="/list-Notification" className={'text-black NavLink'} onClick={(e) => this.handleClose(e)}>
                                  <span>See all notifications</span>
                                </NavLink>
                              </div> */}
                            {/* </DropdownMenu> */}
                          </Dropdown>
                        </li>
                      </ul>
                    </Col>
                  }

                      { !this.props.users ?
                        null
                        :
                        <Col sm={2} xs={2} className="cartIcon p-0 ml-2">
                                    <NavLink to={{
                                      pathname: "/shortbook",
                                      title: 'My Shortbook'
                                    }} >
                                      <IconButton className="icon-btn text-primary" style={{ padding: '4px'}}>
                                        <MenuBookIcon />
                                      </IconButton>
                                    </NavLink>
                        </Col>

                      } 
                  {/* {!this.props.users ? null :

                    <Col sm={3} xs={3} className="cartIcon pl-3 pr-0">
                      <ul className='header-notifications list-inline ml-auto'>
                        <li className="list-inline-item app-tour">
                          <Dropdown className="quick-menu">
                            <DropdownToggle
                              className="d-inline-block"
                              tag="span"
                              data-toggle="dropdown">

                              <div style={{ textAlign: 'center' }}>
                             { this.props.users && this.props.users.wallet_balance > 0 ? 
                             <NavLink to={{ pathname: "/profile", title: 'MediWallet' }}>
                                  <IconButton className="icon-btn text-primary" id="walletPopOver" onMouseOver={this.toggleWallet} onMouseOut={this.toggleWallet}>
                                    <i class="zmdi zmdi-balance-wallet icon-alert"></i>
                                  </IconButton>
                                </NavLink> :
                                <NavLink to={{ pathname: "/profile", title: 'MediWallet' }}>
                                <IconButton className="icon-btn text-primary" id="walletPopOver" onMouseOver={this.toggleWallet} onMouseOut={this.toggleWallet}>
                                  <i class="zmdi zmdi-balance-wallet"></i>
                                </IconButton>
                              </NavLink>
                            }</div> */}
                                {/* <span style={{
                                  fontSize: '13px',
                                  //textAlign: 'center',
                                  marginTop: '-4px',
                                  display: 'block',
                                }}> {this.props.users ? "₹" + (this.props.users && this.props.users.wallet_balance).toFixed(2) + "/-" : null}
                                </span> */}
                              

                              {/* <Popover placement="bottom" isOpen={this.state.walletPopOverOpen} target="walletPopOver" toggleWallet={this.toggleWallet}>
                                <h3>MediWallet Balance</h3>
                                <hr />
                                <center><h2>₹2500/-</h2></center>
                              </Popover> */}
                            {/* </DropdownToggle>
                          </Dropdown>
                        </li>
                      </ul>
                    </Col>
                  } */}

                  {!this.props.users ? null :

                    <Col sm={2} xs={2} className="cartIcon p-0" >
                      <ul className='header-notifications list-inline ml-auto pull-right'>
                        <li className="list-inline-item app-tour">
                          <Dropdown
                            className="quick-menu"
                            isOpen={this.state.appShoppingCart}
                            toggle={this.onAppShoppingCartSelect.bind(this)}>

                            <DropdownToggle
                              className="d-inline-block"
                              tag="span"
                              data-toggle="dropdown">
                              <NavLink to="/MyCart" className={'text-white NavLink'} onClick={(e) => this.handleClose(e)}>
                                {cartDetails && cartDetails.detail && cartDetails.detail.length > 0 ?
                                    <IconButton className="icon-btn text-primary">
                                      <i className="zmdi zmdi-shopping-cart icon-alert" />
                                    </IconButton>
                                    :
                                    <IconButton className="icon-btn text-primary">
                                      <i className="zmdi zmdi-shopping-cart" />
                                    </IconButton>
                                  }
                              </NavLink>
                            </DropdownToggle>

                            {/* <DropdownMenu right className="pb-0">
                              <CardHeader styleName="align-items-center"
                                heading={<IntlMessages id="My Cart" />} />
                              <CartNotification />
                              {cartDetails && cartDetails.detail.length > 0 ?
                                <NavLink to="/MyCart" className={'text-white NavLink'} onClick={(e) => this.handleClose(e)}>
                                  <div className="orderFooter">

                                    <span>Go To Cart</span>

                                  </div></NavLink>
                                :
                                <div className="orderFooter">
                                  <div className="orderFooter">
                                    <span style={{ color: 'white', cursor: 'default' }}>Add some items</span>
                                  </div>
                                </div>
                              }
                            </DropdownMenu> */}
                          </Dropdown>
                        </li>
                      </ul>
                    </Col>
                  }

                  <Col sm={2} xs={2} className="header-notifications dropDown">
                    <ul className="header-notifications list-inline ">
                      <li className="list-inline-item">
                        <Dropdown
                          className="quick-menu app-notification"
                          isOpen={this.state.apps}
                          toggle={this.onAppsSelect.bind(this)}>

                          {!this.props.users ?

                            <div>
                              <button id="signInPopOver" onClick={this.toggle}
                                style={{
                                  marginTop: 3,
                                  marginBottom: 5,
                                  backgroundColor: "#072791",
                                  color: "#ffffff",
                                  border: "none",
                                  paddingLeft: 10,
                                  paddingRight: 10,
                                  paddingTop: 5,
                                  paddingBottom: 5,
                                  borderRadius: "3px",
                                  fontSize: 10
                                }}>
                                LOGIN
                              </button>

                              <Popover trigger="legacy" className="SignInPopOver" placement="bottom" isOpen={this.state.popoverOpen} target="signInPopOver" toggle={this.toggle}>
                                <h2><strong></strong></h2>

                                <SignInPopOver />

                              </Popover>
                            </div>

                            :

                            <DropdownToggle
                              className="d-inline-block text-white"
                              tag="span"
                              data-toggle="dropdown">
                              <span className="app-notification-menu bg-primary">
                                <i className="zmdi zmdi-apps zmdi-hc-fw zmdi-hc-lg icon" />
                                {/* <span className='mt-1'> */}
                                {/* {helperFunction.getNameInitials(`${this.props.DataFromParent.first_name} ${this.props.DataFromParent.last_name}`) } */}
                                {/* {helperFunction.getNameInitials(`${this.props.users.first_name} ${this.props.users.last_name}`) } */}
                                {/* </span> */}
                              </span>
                            </DropdownToggle>
                          }

                          <DropdownMenu>
                            {this.Apps()}
                          </DropdownMenu>
                        </Dropdown>
                      </li>
                    </ul>
                  </Col>

                </Row>
              </Col>
            </Row>

            <Row>
              <Col>
                <div className="search-bar right-side-icon bg-transparent">
                  <div className="form-group search-container">
                    <Autocomplete
                      style={{ marginTop: "2px", paddingTop: "0px !important", borderStyle:'solid' ,borderColor: "#072791",borderWidth: "2px" }}
                      variant='outlined'
                      className="autoCompleteSearch"
                      id="free-solo-demo"
                      filterSelectedOptions
                      filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                        if (params.inputValue !== '') {
                          filtered.unshift(params.inputValue);
                        }
                        return filtered;
                      }}
                      autoHighlight
                      disableClearable
                      clearOnEscape
                      freeSolo
                      // disableOpenOnFocus
                      inputValue={this.state.searchText} //---------- USE inputValue ----------------- 

                      onInputChange={(event, val) => this.updateSearchText(event, val)}
                      onKeyPress={this.handleSigninOnEnter.bind(this)}
                      onChange={(event, val) => this.onChangeAutoCompleteSearch(event, val)}

                      options={'asdsd', searchOptions && searchOptions.map(option => option.word)}

                      getOptionLabel={(option) => {
                        // e.g value selected with enter, right from the input
                        if (typeof option === 'string') {
                          return option;
                        }
                        if (option.inputValue) {
                          return option.inputValue;
                        }
                        return option.inputValue;
                      }}

                      renderInput={params => (
                        <div>
                          <TextField
                            name="searchTextBoxField"
                            className="searchTextField"
                            {...params}
                            fullWidth
                            variant='outlined'
                            placeholder="Search Medicine"
                          />
                        </div>
                      )}
                    />
                  </div>
                </div>
              </Col>
              <div style={{ textAlign: "right", color: "#072791", marginLeft: "-46px", zIndex: "100", marginTop: "8px" }}>
                <i className="zmdi zmdi-search  zmdi-hc-2x"></i>
              </div>
            </Row>
          </div>
        }
      </AppBar>
    );
  }

}


const mapStateToProps = ({ settings, auth, seller, buyer, notification }) => {
  const { drawerType, locale, navigationStyle, horizontalNavPosition } = settings;
  const { userDetails } = seller
  const { user_details } = auth;
  const { mediWallet, autoFill, searchOptions, searchProduct, cartDetails, getMarkReadListForBuyer } = buyer;
  const { unreadNotificationCount, notificationData, unReadNotification} = notification;

  let users = userDetails ? userDetails : user_details

  return { drawerType, locale, navigationStyle, horizontalNavPosition, user_details, users, mediWallet, autoFill, searchOptions, searchProduct, cartDetails,
    unreadNotificationCount, notificationData, unReadNotification, getMarkReadListForBuyer }
};

export default withStyles(stylesFn)(
  connect(mapStateToProps, { toggleCollapsedNav, switchLanguage, userSignOut, getUserDetail, getMediWallet, getAutoFill, getSearchProduct, getCartDetails, getBuyerNotification,getMarkReadForBuyer, getShortProducts })(withRouter(Header)));
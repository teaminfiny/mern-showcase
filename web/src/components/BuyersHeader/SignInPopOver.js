import React from 'react';
import { Link, withRouter, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { Col, Row, Card } from 'reactstrap';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextBox from 'components/textBox/index.js'
import { Field, reduxForm } from 'redux-form'
import { validateData, required, maxLength, number, minValue, emailField, aol, minValue18, tooOld, maxLength15, minValue4, passwordMatch } from '../../constants/validations'
import {
  hideMessage,
  // showAuthLoader,
  userFacebookSignIn,
  userGithubSignIn,
  userGoogleSignIn,
  userSignIn,
  userTwitterSignIn
} from 'actions/Auth';

import './index.css';

class SignInPopOver extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      user_type: 'buyer',
      // user:'',
      showNotification: true
    }
  }



  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }

    // if (this.props.authUser !== null) {
    //   this.props.history.push('/');
    // }
  }

  // gotoSignup = () => {
  //   this.props.history.push('/signup')
  // }

  // gotoForgotPassword = () => {
  //   this.props.history.push('/forgotPassword')
  // }

  handleSigninOnEnter = (e) => {
    let email = this.state.email;
    let password = this.state.password;
    if (e.key === 'Enter' && email !== '' && password !== '') {

    }
  }

  handleEmailChange = (e, newValue, previousValue, key) => {
    if (this.state.showNotification === false) {
      this.setState({showNotification:true});
    }
    this.setState({ [key]: (e.target.value).toLowerCase() })
  }

  handlePasswordChange = (e, newValue, previousValue, key) => {
    if (this.state.showNotification === false) {
      this.setState({showNotification:true});
    }
    this.setState({ [key]: e.target.value })
  }

  onSubmit = () => {
    const { email, password, user_type } = this.state;
    if (this.state.showNotification) {
      // this.props.showAuthLoader();
      localStorage.clear();
      this.props.userSignIn({ email, password, user_type, history: this.props.history });
      this.setState({showNotification:false});
  
    }    
  }



  local = () => {
    localStorage.setItem( 'user','buyer')
  }






  render() {


    const {
      email,
      password,
      user_type,
      // user
    } = this.state;
    const { showMessage, loader, alertMessage } = this.props;
    const { handleSubmit, pristine, reset, submitting } = this.props;

    return (
      <div className="col-xl-12 col-lg-12 mt-3">
        <div className="">
          <div className="">
            <div className="">
            </div>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <Row>
                <Col sm={12} md={12} lg={12} xs={12} xl={12}>


                </Col>
                <Col sm={12} md={12} lg={12} xs={12} xl={12}>
                  <Field name="email" type="text"
                    component={TextBox} label="Email"
                    validate={emailField}
                    value={this.state.email}
                    onChange={(event, newValue, previousValue) => this.handleEmailChange(event, newValue, previousValue, 'email')}
                  />


                </Col>
                <Col sm={12} md={12} lg={12} xs={12} xl={12}>

                  <Field name="password" type="password"
                    component={TextBox} label="Password"
                    validate={required}
                    value={this.state.password}
                    onChange={(event, newValue, previousValue) => this.handlePasswordChange(event, newValue, previousValue, 'password')}
                  />

                </Col>
                
                <Col sm={6} md={6} lg={6} xs={6} xl={6} >
                  <Button type='submit' variant='contained' color='primary' className='m-2 signin' size='small'>LOGIN</Button>
                </Col>

                <Col sm={6} md={6} lg={6} xs={6} xl={6} className='pull-right pt-3' style={{textAlign: "right",fontSize: '13px', fontWeight: '500'}}>                
                  <NavLink to="/buyersignup"><span>Sign Up as Retailer</span></NavLink>
                  <br/>
                  
                </Col>




                <Col sm={6} md={4} lg={6} xs={6} xl={6} className='d-flex  align-items-center'>
                  {/* <a href='javascript:void(0);' onClick={this.gotoSignup}>
                    <span>
                      Create an account
                      </span>
                    </a> */}
                  <NavLink to="/forgotPassword"  >
                    {/* <a href='javascript:void(0);' onClick={this.gotoForgotPassword}> */}
                    <span color='primary' onClick={this.local}>Forgot your password?</span>
                    {/* </a> */}
                  </NavLink>


                </Col>
                <Col sm={6} md={4} lg={6} xs={6} xl={6} className='pull-right ' style={{textAlign: "right",fontSize: '13px', fontWeight: '500'}}>
                  <NavLink to="/signin"><span>Sign In as Stockist</span></NavLink>
                </Col>

              </Row>
              <div>
              </div>
            </form>
          </div>

        </div>
      </div>

    );
  }
}
// loader,
// loader,
const mapStateToProps = ({ auth }) => {
  const { alertMessage, showMessage, authUser } = auth;
  return { alertMessage, showMessage, authUser }
};
SignInPopOver = reduxForm({
  form: 'fieldLevelValidation',// a unique identifier for this form
  validateData
})(SignInPopOver)
SignInPopOver = connect(
  mapStateToProps,
  {
    userSignIn,
    hideMessage,
    // showAuthLoader,
  }               // bind account loading action creator
)(SignInPopOver)
export default withRouter(SignInPopOver)

// export default connect(mapStateToProps, {
//   userSignIn,
//   hideMessage,
//   showAuthLoader,
//   userFacebookSignIn,
//   userGoogleSignIn,
//   userGithubSignIn,
//   userTwitterSignIn
// })(SignIn);


import React, { Component } from 'react';
import Widget from "components/Widget/index";
import { Row, Col } from 'reactstrap'
import Slider from "react-slick";
import Button from '@material-ui/core/Button';
import AppConfig from 'constants/config'
import './index.css'
import { NavLink, withRouter } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import helpertFn from 'constants/helperFunction';
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';
class BDashboardCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      obj: { search: '', company_id: '', category_id: '', seller_id: '' },
      cat:'category'

    }
  }

  handleCategories = async(key, e) => {

    // const decodedURL = JSON.parse('{"' + decodeURIComponent(this.props.match.params.search.replace(/&/g, "\",\"").replace(/=/g, "\":\"")) + '"}')

    let obj = { ...this.state.categories }

    // obj.search = this.state.search?this.state.search : ''   
    // console.log('handleCategories', e.target.value, e.target.checked)

    let temp = [...this.state.categories]

    temp.push(e)
    this.setState({ categories: temp })

    const data = {
      key: obj.search ? obj.search : '',
      company_id: obj.company_id ? obj.company_id : '',
      category_id: temp,
      seller_id: obj.seller_id ? obj.seller_id : '',
      page: 1,
      perPage: 10
    }

    // obj.search = decodedURL.search
    obj.category_id = temp.length > 0 ? temp.toString() : ''

    let url = Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
    let catName =  this.props.value && this.props.value.name ? ((this.props.value.name).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase() : 'category';
    await this.setState({cat: catName})
    this.props.history.push(`/search-results/${catName}/${url}`)

    // this.props.getSearchProduct({ data })
  }

  addDefaultSrc(ev) {
    ev.target.src = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG'
  }

  render() {
    const { data, name } = this.props.value;
    return (
      <Widget title={name.length > 24 ? <Tooltip className='ellipsis' title={<h4 className="text-white"
        style={{ marginTop: "13px" }}>{name.slice(0, 26) + '...'}</h4>}><h4 className='font-weight-bold mb-0'>{name}</h4></Tooltip> : <h4 className='font-weight-bold mb-0'>{name}</h4>} styleName='mavHeight'>
        <Row >
          {
            data.map((val) =>
              <Col sm={3} md={3} lg={6} xl={6} xs={6} >
                <div className="card-image" >
                  <div className="grid-thumb-equal">
                    <NavLink className="buyerRedirectNavlink" to={`/product-details/${((val.Product.name).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()}/${val.inventory_id}`}>
                      <span className="grid-thumb-cover jr-link">
                        <img onError={this.addDefaultSrc} alt="image" src={val.Product.images.length > 0 ? `${helpertFn.productImg(val.Product.images[0])}` : logo} />
                      </span>
                    </NavLink>
                  </div>
                  <NavLink className="buyerRedirectNavlink" to={`/product-details/${((val.Product.name).replace(/ /g, '-')).replace(/[\/,  @#%\^;?]/g, ' ').toLowerCase()}/${val.inventory_id}`}>

                    {val.Product.name.length > 15 ?

                      <React.Fragment>

                        <Tooltip
                          className="d-inline-block"
                          id="tooltip-right"
                          title={<h6 className="text-white"
                            style={{ marginTop: "13px" }}>{val.Product.name}</h6>}
                          placement="right"
                          className="ellipsis">
                          <h5 className="greyText"> {val.Product.name} </h5>
                        </Tooltip>

                        <h6 className="greyText">
                          PTR: ₹{(val.PTR).toFixed(2)}
                        </h6>

                      </React.Fragment>

                      :

                      <Tooltip className="d-inline-block" id="tooltip-right" title="Add" placement="right">
                        <h5 className="greyText"> {val.Product.name} </h5>
                      </Tooltip>
                    }
                  </NavLink>


                  {

                    val.Discount && val.Discount.discount_type === "Same" ?

                      <React.Fragment>
                        <Tooltip
                          className="d-inline-block"
                          id="tooltip-right"
                          title={
                            <h6 className="text-white" style={{ marginTop: "13px" }}>
                              Buy {val.Discount.discount_on_product.purchase} Get {val.Discount.discount_on_product.bonus} Free
                      </h6>
                          }
                          placement="right"
                          className="ellipsis">

                          <h6 className="ellipsis" className="greyText">
                            {("Buy " + val.Discount.discount_on_product.purchase + " Get " + val.Discount.discount_on_product.bonus + " Free").slice(0, 25) + "..."}
                          </h6>
                        </Tooltip>

                        {/* <h6 style={{marginTop: "13px"}} className="greyText">
                    Buy { val.Discount.discount_on_product.purchase} Get { val.Discount.discount_on_product.bonus} Free
                  </h6> */}
                      </React.Fragment>

                      :
                      val.Discount && val.Discount.discount_type === "Different" ?

                        <React.Fragment>
                          <Tooltip
                            className="d-inline-block"
                            id="tooltip-right"
                            title={
                              <h6 className="text-white" style={{ marginTop: "13px" }}>
                                Buy {val.Discount.discount_on_product.purchase} Get {val.Discount.discount_on_product.bonus} {val.OtherProducts.name} Free
                              </h6>
                            }
                            placement="right"
                            className="ellipsis">

                            <h6 className="ellipsis" className="greyText">
                              {("Buy " + val.Discount.discount_on_product.purchase + " Get " + val.Discount.discount_on_product.bonus + " " + val.OtherProducts.name + " Free").slice(0, 25) + "..."}
                            </h6>
                          </Tooltip>

                          {/* <h6 style={{marginTop: "13px"}}>
                        Buy { val.Discount.discount_on_product.purchase} Get {val.Discount.discount_on_product.bonus} {val.OtherProducts.name} Free
                      </h6> */}
                        </React.Fragment>

                        :

                        val.Discount && val.Discount.discount_type === "Discount" ?

                          <React.Fragment>
                            <h6 style={{ marginTop: "13px" }} className="greyText">
                              {" " + (val.Discount.discount_per).toFixed(2)}% discount
                        </h6>
                          </React.Fragment>

                          :

                          val.Discount && val.Discount.discount_type === "" ? //FOR DEALS ARR

                            <React.Fragment>

                            </React.Fragment>

                            :

                            <React.Fragment>

                            </React.Fragment>
                  }

                </div>
              </Col>)
          }
        </Row>
        <Row className='d-flex' style={{alignSelf: 'center'}}>
          {/* <NavLink className="buyerRedirectNavlink" to={`/search-results/category/category_id=${data[0].ProductCategory._id}`}>           */}
          <Button className="jr-btn text-uppercase text-primary"
            onClick={(e) => { this.handleCategories('category_id', data[0].Product_Category._id) }} >See More</Button>
          {/* </NavLink> */}

        </Row>
      </Widget>
    );
  }
}
export default withRouter(BDashboardCategory);
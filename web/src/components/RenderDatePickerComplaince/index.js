import React from 'react';
import { DatePicker } from 'material-ui-pickers';
import FormHelperText from '@material-ui/core/FormHelperText'
import moment from 'moment'

const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
        return
    } else {
        return <FormHelperText style={{ color: '#f44336' }}>{touched && error}</FormHelperText>
    }
}

const RenderDatePickerComplaince = ({ label, input: { onChange, value, name }, meta: { touched, error } }) => {
    console.log('input.value', value)
    const selectedDate = value ?
        moment(value) :
        moment().add(2, "M");
    return (
        <React.Fragment>
            <div className="pt-2 text-center">
                <span>Expiry date : </span>
                <DatePicker
                    onChange={onChange}
                    name={name}
                    value={selectedDate}
                    minDate={selectedDate}
                    // autoOk
                    // fullWidth
                    id="datePickerStyle"
                    className={'datePickerStyle'}
                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                    format="MM/DD/YYYY"
                />
                {renderFromHelper({ touched, error })}
            </div>
        </React.Fragment>
    );
}

export default RenderDatePickerComplaince;
import React, { Component } from "react";
import MUIDataTable from "../../components/DataTable";
import customHead from 'constants/customHead';

import {connect} from 'react-redux'

import {getTransactionList} from 'actions/buyer';

import moment from 'moment';
import { withRouter } from 'react-router-dom'
// import CustomBuyerFilter from '../buyerFilter'
import CustomFilter from '../../components/Filter'
import CustomScrollbars from 'util/CustomScrollbars';




class Transaction extends Component {

  constructor(props){
    super(props)
    this.state = {
      edit: false,
      hide: false,
      delete: false,
      add: false,
      page: 1,
      perPage: 50,
      filter: '',
      // month: 'January',
      // year: 2020,
      month: moment().format("MMMM"),
      year: moment().format("GGGG")
      
    }
  }

  onFilterChange = (e, key) => {
    this.setState({ [key]: e.target.value });
  }

  handleResetFilter = (e, filter) => {
    e.preventDefault();
    filter();
    this.setState({
      month: moment().format("MMMM"),
      year: moment().format("GGGG"),
      filter: ''
    });
    let data = {
      page: 1,
      perPage: this.state.perPage,
      filter: this.state.filter,
      month: moment().format("MMMM"),
      year: moment().format("GGGG")
    }
    this.props.getTransactionList({ data, history: this.props.history })
  }
  
  componentDidMount(){

    let data = {
      page: 1,
      perPage: 50,
      filter: '',
      // searchText: '',
      month: this.state.month,
      year: this.state.year
    }
    this.props.getTransactionList({history:this.props.history, data})
  }

  changePage = (page) => {
    let pages = page + 1
    let data = {
      page: pages,
      perPage: this.state.perPage,
      filter: '',
      searchText: '',
      month: this.state.month,
      year: this.state.year
    }
    this.props.getTransactionList({ data, history: this.props.history })
    this.setState({ page })
  };

  changeRowsPerPage = (perPage) => {
    let data = {
      page: 1,
      perPage: perPage,
      filter: '',
      searchText: '',
      month: this.state.month,
      year: this.state.year
    }
    this.props.getTransactionList({ data, history: this.props.history })
    this.setState({ page: 0, perPage })
  }



  render() {

    const { transactionListData } = this.props;


    const statusStyle = (status) => {
      return status.includes("Pending") ? "text-white bg-grey" : status.includes("Success") ? "text-white bg-success" : "text-white bg-grey";
    }

    const columns = [
      {
        name: "narration",
        label: "Narration",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "amount",
        label: "Amount",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "status",
        label: "Status",
        options: {
          filter: false,
          sort: false,
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          }
        }
      },
      {
        name: "date",
        label: "Date",
        options: {
          filter: true,
          filterType: 'custom',
          customHeadRender: (o, updateDirection) => {
            return customHead(o, updateDirection);
          },
          customFilterListRender: v => {
            return false;
          },
          filterOptions: {
            names: [],
            logic() {
              console.log('call filter api here..');
            },
            display: (filterList, handleCustomChange, applyFilter, index, column) => {
              return (
              <CustomFilter 
                onFilterChange={this.onFilterChange} 
                handleApplyFilter={this.handleApplyFilter} 
                applyFilter={applyFilter} 
                handleResetFilter={this.handleResetFilter} 
                month={this.state.month}
                year={this.state.year}
                filterFor='transaction'/>)
            },
          }
        }
      }
    ];

    const options = {
      filterType: 'dropdown',
      print: false,
      download: false,
      selectableRows: false,
      selectableRowsOnClick: false,
      selectableRowsHeader: false,
      search: false,
      viewColumns: false,
      rowHover: false,
      filter: true,
      rowsPerPage: this.state.perPage,
      page: this.state.page - 1,
      fixedHeader: false,
      sort: false,
      server: true,
      selectableRows: 'none',

      textLabels: {
        filter: {
          all: "",
          title: "FILTERS",
        },
      },

      onTableChange: (action, tableState) => {
        console.log('action=', action, tableState)
        switch (action) {
          case 'changePage':
            this.changePage(tableState.page);
            break;
          case 'changeRowsPerPage':
            this.changeRowsPerPage(tableState.rowsPerPage)
            break;
          case 'search':
            this.handleSearch(tableState.searchText)
            break;
        }
      },
      
    };


const data = []

transactionListData && transactionListData.details && transactionListData.details.length > 0 && transactionListData.details[0] && transactionListData.details[0].data.map((dataOne,index) => {
  data.push({
    narration:dataOne.narration,
    amount: "₹" +(dataOne.settle_amount.toFixed(2)),
    status: <div className={` badge text-uppercase ${statusStyle(dataOne.status === "Settled" ? 'Success' : dataOne.status)}`}>{dataOne.status}</div>,
    date: moment(dataOne.createdAt).format('MMM Do YYYY')
  })
})
    


    return (
      <React.Fragment>
        <h1 style={{backgroundColor: "white",
    padding: "11px",
    marginBottom: "inherit",
    borderTopLeftRadius: "5px",
    borderTopRightRadius: "5px",
    fontWeight: 500,
    paddingLeft: "24px",
    paddingTop: "20px",
    }}>
      Transactions
    </h1>
        <CustomScrollbars className="messages-list scrollbar" style={{ height: 650 }}>

          <MUIDataTable
            // title={'Transactions'}
            title={<h3>{`Showing Transactions for ${this.state.month} ${this.state.year}`}</h3>}
            data={data}
            columns={columns}
            options={options}
          />
        </CustomScrollbars>

      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ buyer }) => {
  const { transactionListData } = buyer;
  return { transactionListData };
}
export default withRouter(connect(mapStateToProps, {getTransactionList})(Transaction));
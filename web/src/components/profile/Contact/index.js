import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CustomScrollbars from 'util/CustomScrollbars';
import {withRouter} from 'react-router-dom'
import helperFunction from '../../../constants/helperFunction';
import {connect} from 'react-redux';
import {getReviewList} from 'actions/buyer';
import moment from "moment";
let counter = 0;
function createData(name, image, shoppedNumber) {
  counter += 1;
  return { id: counter, name, image, shoppedNumber };
}
let avatar = [<Avatar className="ratingSmallIcon1"><i className="zmdi zmdi-thumb-up zmdi-hc-fw rating1 text-white" /></Avatar>, <Avatar className="ratingSmallIconDanger1" ><i className="zmdi zmdi-thumb-down zmdi-hc-fw rating1 text-white" /></Avatar>];
class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        createData('John Smith', require('assets/productImage/user.jpg'), '3'),
        createData('Priti', require('assets/productImage/user2.jpg'), '1'),
        createData('Kshitija ', require('assets/productImage/user3.jpg'), '0'),
        createData('Praful', require('assets/productImage/user4.jpg'), '5'),
        createData('Avanish', require('assets/productImage/images.png'), '0'),
        createData('Abhidnya', require('assets/productImage/user2.jpg'), '12'),
        createData('Lekhraj', require('assets/productImage/user.jpg'), '6'),
        createData('Ajay', require('assets/productImage/images.png'), '9'),
        createData('Eldhose', require('assets/productImage/user4.jpg'), '3'),
      ],
      teamList: [
        createData('John Smith', require('assets/productImage/user.jpg'), '3'),
        createData('Priti', require('assets/productImage/user2.jpg'), '1'),
        createData('Kshitija ', require('assets/productImage/user3.jpg'), '0'),
        createData('Praful', require('assets/productImage/user4.jpg'), '5'),
      ],
    }
  }
  componentDidMount(){
    if(this.props.match.path === "/view-seller/:id"){
      let data = {
        sellerId: this.props.match && this.props.match.params && this.props.match.params.id,
        sellerReviewList: true
      }
    this.props.getReviewList({data})
    }
    else{
    this.props.getReviewList({})
    }
  }
  toSeller = (e) => {
    this.props.history.push(`/view-seller/${e._id}`)
  }
  render() {
    const { reviewList } = this.props;
    // this.props.avgReview(reviewList && reviewList.averageRating)
    //default-table table table-sm table-responsive-sm table-hover
    // const arr =  reviewList.detail;
    // const sellerName = arr && arr.map((name, index) => {
    //   return(
    //              name.seller_id.first_name
    //     )}) 
    return (
      // <div className="table-responsive-material table-userdetail-mmin " style={{ maxHeight: 350, overflow: 'auto' }}>
      this.props.match.path === "/view-seller/:id" ?
      <CustomScrollbars className="messages-list scrollbar" style={{  height: this.props.history.location.pathname === '/profile' ? 360 : 350 }}>
        <table className="default-table table-sm table full-table mb-0">
          <tbody>
            {reviewList && reviewList.detail.length > 0 ? reviewList.detail.map(data => {
              return (
                <tr
                  tabIndex={-1}
                  key={data.id}>
                  <td>
                    <div className="user-profile d-flex flex-row align-items-center">
                      <Avatar
                        alt={''}
                        src={''}
                        className="user-avatar"
                        style={{background:helperFunction.gradientGenerator()}}
                      >{helperFunction.getNameInitials(data.user_id ? data.user_id.first_name : '')}</Avatar>
                      <div className="user-detail">
                        <h4 className="user-name">{data.user_id ? data.user_id.company_name : ''}</h4>
                        <p className="user-description">{moment(data.createdAt).format("MMM Do YY")}</p>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div className="ratingSmileEmoji" >
              {/* <span class=" text-warning">😠</span> */}
              <span className='mr-lg-4 mr-md-4 mr-sm-4 mr-xs-4 mr-xl-4 float-lg-right float-md-right float-sm-right'>
              {
                // <i className = {${helperFunction.getRatingForBuyer(2)}}></i>
                data.ratingValue && data.ratingValue > 0 ? <i className = {helperFunction.getRatingValueForBuyer(data.ratingValue)} set={'google'} size={30} />
                // data.ratingValue && data.ratingValue > 0 ? <i class1={helperFunction.getRatingValueForBuyer(data.ratingValue)} set={'google'} size={30} />
                 : ''
              }
              </span>
              </div>
                  </td>
                </tr>
              ); 
            })
            : 
            <td>
            <div className="user-profile d-flex flex-row align-items-center">
              {/* <Avatar
                alt={''}
                src={''}
                className="user-avatar"
                ></Avatar> */}
              <div className="user-detail">
                <h4 className="user-name">No reviews found</h4>
                <p className="user-description"></p>
              </div>
            </div>
          </td> }
          </tbody>
        </table>
        {/* <ul>{sellerName}</ul> */}
      </CustomScrollbars>
      :
      <CustomScrollbars className="messages-list scrollbar" style={{  height: this.props.history.location.pathname === '/profile' ? 360 : 350 }}>
      <table className="default-table table-sm table full-table mb-0">
        <tbody>
          {reviewList && reviewList.detail.length > 0 ? reviewList.detail.map(data => {
            return (
              <tr
                tabIndex={-1}
                key={data.id}>
                <td>
                  <div className="user-profile d-flex flex-row align-items-center" onClick={(e)=>this.toSeller(data.seller_id)}>
                    <Avatar
                      alt={''}
                      src={''}
                      className="user-avatar"
                      style={{background:helperFunction.gradientGenerator()}}
                    >{helperFunction.getNameInitials(data.seller_id ? data.seller_id.first_name : '')}</Avatar>
                    <div className="user-detail">
                      <h4 className="user-name">{data.seller_id ? data.seller_id.company_name : ''}</h4>
                      <p className="user-description">{moment(data.createdAt).format("MMM Do YY")}</p>
                    </div>
                  </div>
                </td>
                <td>
          <div className="ratingSmileEmoji" onClick={(e)=>this.toSeller(data.seller_id)}>
            {/* <span class=" text-warning">😠</span> */}
            <span className='mr-lg-4 mr-md-4 mr-sm-4 mr-xs-4 mr-xl-4 float-lg-right float-md-right float-sm-right'>
            {
              // <i className = {${helperFunction.getRatingForBuyer(2)}}></i>
              data.ratingValue && data.ratingValue > 0 ? <i className = {helperFunction.getRatingValueForBuyer(data.ratingValue)} set={'google'} size={30} />
              // data.ratingValue && data.ratingValue > 0 ? <i class1={helperFunction.getRatingValueForBuyer(data.ratingValue)} set={'google'} size={30} />
               : ''
            }
            </span>
          </div>
                </td>
              </tr>
            ); 
          })
          : 
          <td>
          <div className="user-profile d-flex flex-row align-items-center">
            {/* <Avatar
              alt={''}
              src={''}
              className="user-avatar"
              ></Avatar> */}
            <div className="user-detail">
              <h4 className="user-name">No reviews found</h4>
              <p className="user-description"></p>
            </div>
          </div>
        </td> }
        </tbody>
      </table>
      {/* <ul>{sellerName}</ul> */}
    </CustomScrollbars>
    )
  }
}
const mapStateToProps = ({ buyer }) => {
  const { reviewList } = buyer;
  return { reviewList };
}
export default withRouter(connect(mapStateToProps, { getReviewList } )(Contact));
{/* <div className='col-sm-3 col-md-3 col-lg-3 ml-1' style={{ background: linear - gradient(rgba(0, 0, 0, .7), rgba(0, 0, 0, .7)), url(${ require('../../../assets/images/tablet.jpeg')}), backgroundSize:'cover',
  backgroundRepeat: 'none', backgroundPosition: 'center center', height: '42px'}}>
    <div className='d-flex mt-3 mr-2'>
      <h6 className='text-white'>+3</h6>
    </div>
</div > */}
import React, { Component } from "react";
import './index.css';
import AxiosRequest from 'sagas/axiosRequest'
import { NotificationManager } from 'react-notifications';
class ProfileHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value:'',
      rate:0,
    }
  }
  handleEmoji = async(e)=>{
    
    this.setState({rate: e.target.id})
    let data = {
      seller_id :this.props.match.params.id,
      ratingValue :e.target.id,
    }
    let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'users/addRating', '', data)
    if (response.data.error) {
      NotificationManager.error("Please Sign in to peform this action.")
    } else {
     
      NotificationManager.success(response.data.title)
    }
  }
  onSaved = async (e) => {
  }
  // componentDidMount(){
  //   const { dataFromParent } = this.props;
  // }
  componentDidUpdate(prevProps, prevState){
    if(prevProps.dataFromParent1 !== this.props.dataFromParent1){
      this.setState({
        rate: this.props.dataFromParent1.userRating, 
        
      })
    }
  }
  render() {
    const {dataFromParent} = this.props;
    const login = localStorage.getItem("buyer_token");
    return (
      <div className="jr-profile-banner" style={{
        background: `linear-gradient(rgba(0,0,0,.7), rgba(0,0,0,.7)),url(${require('../../../assets/images/banners5.jpeg')})`, backgroundSize: 'cover',
        backgroundRepeat: 'none', backgroundPosition: 'center center'
      }}>
        <div className="jr-profile-container">
          <div className="jr-profile-banner-top">
            <div className="jr-profile-banner-top" style={{marginBottom:'20px'}}>
                {/* <div className="jr-profile-banner-avatar">
                  <Avatar className="size-90" alt="..." src={require("assets/images/Anjali-Sud.jpg")} />
                </div> */}
              <div className="jr-profile-banner-avatar-info">
                <h2 className="mb-2 jr-mb-sm-3 jr-fs-xxl jr-font-weight-light">
                  {dataFromParent&&dataFromParent.products[0]&&dataFromParent.products[0].data[0]&&dataFromParent.products[0].data[0].Seller&&dataFromParent.products[0].data[0].Seller.company_name }
                </h2>
                {/* <p className="mb-0 jr-fs-lg">Florida, USA</p> */}
              </div>
            </div>
            <div className="jr-profile-banner-bottom-left" style={{width:'100%'}}>
              {/* <ul className="jr-follower-list">
                  <li>
                    <span className="jr-follower-title jr-fs-lg jr-font-weight-medium">35</span>
                    <span className="jr-fs-sm">Orders</span></li>
                  <li>
                    <span className="jr-follower-title jr-fs-lg jr-font-weight-medium">10</span>
                    <span className="jr-fs-sm">Liked</span></li>
                </ul> */}
                  {/* <ul className=""> */}
                  { login ? 
                  <span>
                    {/* {dataFromParent1 &&dataFromParent1.userRatingA} */}
                <i id={1} onClick={(e) => this.handleEmoji(e)} 
                  className={`${ this.state.rate  == 1 ? 'fas' : 'far'} fa-angry warning`} 
                  style={{ fontSize: '30px' }} ></i> &nbsp;
                <i id={2} onClick={(e) => this.handleEmoji(e)} 
                  className={`${ this.state.rate  == 2 ? 'fas' : 'far'} fa-sad-tear `}  
                  style={{ fontSize: '30px' }}></i> &nbsp;
                <i id={3} onClick={(e) => this.handleEmoji(e)} 
                  className={`${ this.state.rate  == 3 ? 'fas' : 'far'} fa-meh `} 
                  style={{ fontSize: '30px' }}></i> &nbsp;
                <i id={4} onClick={(e) => this.handleEmoji(e)} 
                  className={`${ this.state.rate  == 4 ? 'fas' : 'far'} fa-smile-beam }`} 
                  style={{ fontSize: '30px' }}></i> &nbsp;
                <i id={5} onClick={(e) => this.handleEmoji(e)}
                  className={`${ this.state.rate == 5 ? 'fas' : 'far'} fa-grin-hearts }`} 
                  style={{ fontSize: '30px' }}></i> &nbsp;
                    {/* <i id={5} onClick = {(e)=>this.handleEmoji(e)}  className={`${value==5?'fas':'far'} fa-grin-hearts`} style={{fontSize:'30px'}}></i> &nbsp; */}
                      {/* <Emoji emoji={helperFunction.getRatingValue(2)} set={'google'} size={30} />
                      <Emoji emoji={helperFunction.getRatingValue(3)} set={'google'} size={30} />
                      <Emoji emoji={helperFunction.getRatingValue(4)} set={'google'} size={30} />
                      <Emoji emoji={helperFunction.getRatingValue(5)} set={'google'} size={30} /> */}
                    </span> : '' }
                  {/* </ul> */}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ProfileHeader;
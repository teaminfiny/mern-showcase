import React from "react";
import Widget from "components/Widget/index";
import PropTypes from 'prop-types';
import { aboutList } from '../../../app/routes/socialApps/routes/Profile/data'
import { withStyles } from '@material-ui/core/styles';
import SellerProductCard from 'components/viewSellerProductCard';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import CircularProgress from '@material-ui/core/CircularProgress';

import './index.css'
import {
  getProductsBySeller,
  getCategories
} from 'actions/buyer';
import { connect } from 'react-redux';
function TabContainer({ children, dir }) {
  return (
    <div dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </div>
  );
}
TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};
class About extends React.Component {
  state = {
    aboutList,
    value: '',
    perPage: 12,
    hidden: false,
    loader:false,
    categorys_id:''
  };


  handleChange = (event) => {
    this.setState({value:  event.target.value, perPage:12});   
    
    this.props.getProductsBySeller({
      seller_id: this.props.match.params.id,
      category_id:event.target.value !=='' ? [event.target.value] : [],
      page:1,
      perPage:12})
      // this.props.seeMoreProducts(this.state.value)
  };

seeMoreProducts = () => {
  
  this.props.getProductsBySeller({
    seller_id: this.props.match.params.id,
    category_id:this.state.value !=='' ? [this.state.value] : [],
    page:1,
    perPage:Number(this.state.perPage) + 12})
  this.setState({hidden:true, perPage:Number(this.state.perPage) + 12 })

  const t = setTimeout(() =>{
  this.setState({hidden:false})
}, 2000)

  this.setState({loader: true})
  const a = setTimeout(() => {
    this.setState({loader: false})                
  }, 2000);
  
}

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  componentDidMount() {
   this.props.getCategories({seller_id: this.props.match.params._id})
}
  render() {
    const {  product } = this.props;
    const { value } = this.state;
    const {dataFromParent, categoriesFromParent} = this.props;
    const {hidden} = this.state;


    return (
      <Widget styleName="jr-card-full jr-card-tabs-right jr-card-profile">
        <div position="static" color="default" >
          {/* <Tabs textColor='primary' indicatorColor='primary' className="jr-tabs-center" 
          value={value}  
          onChange={this.handleChange}
           variant="scrollable" 
            scrollButtons="auto">
            <Tab className='text-green' label="Cosmetics" icon={<i className="zmdi zmdi-face zmdi-hc-2x " />} />
            <Tab className='text-orange' label="Chronic" icon={<i className="zmdi  zmdi-plus-square zmdi-hc-2x " />} />
            <Tab className='text-red' label="Diabetic" icon={<i className="zmdi zmdi-eyedropper zmdi-hc-2x " />} />
            <Tab className='text-primary' label="Others" icon={<i className="zmdi zmdi-memory zmdi-hc-2x " />} />
          </Tabs> */}
          <FormGroup style={{marginRight:'15px',float:'right',width:'28%'}} >
           <Label for="exampleSelect"></Label>
        <Input type="select" name="select" 
        onChange={this.handleChange} 
            // onChange={(e) => this.handleRemoveFromCart(e, value)} 
        value={value}
        >
          <option value={''}>All</option>
          {
            categoriesFromParent && categoriesFromParent.detail && categoriesFromParent.detail.map((val,i)=>
            <option value={val._id} key = {i}>{val.name}</option>
            )
          }
          </Input>
            </FormGroup>
        </div>
        <div className="jr-tabs-classic">
          <div className="jr-tabs-content jr-task-list">
              <TabContainer>
                <div className="row">
                {
              dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data ?
                null
                :
                <div className="loader-view" style={{margin:'auto',marginBottom:'10px'}}>
                  <CircularProgress />
                </div>
            }

                  { dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data && dataFromParent.products[0].data.length  > 0 ?

                  dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data.map((value,index)=>
                    <React.Fragment>
                      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                      {/* style={{display:"flex",overflow:'hidden'}} */}
                          {/* this.props.history.location.pathname === '/view-seller' ? */}
                          <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product= {product}  dataFromParentTwo={dataFromParent} value={value}/> 
                       {/* : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product}  /> */}
                      </div>
                    </React.Fragment>
                 )
                  :
                  dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].data < 1 ?
                  <div style={{ margin: 'auto', width: '50%',textAlign:' center', fontSize:'25px'}}>
                    <span className="text-danger" >
                      <i class="zmdi zmdi-alert-circle animated wobble zmdi-hc-5x" ></i>
                    </span>
                    <h1>Not Available !</h1>
                  </div>
                  :
                  null
                  
                  }
                
                
                </div> 
                  { !hidden ? 
                <div style={{textAlign: "center"}}>
                  
                 
                  {(dataFromParent && dataFromParent.products[0] && dataFromParent.products[0].metadata[0] && dataFromParent.products[0].metadata[0].total > this.state.perPage) ?
                   <Button color="primary" className="jr-btn jr-btn-lg btnPrimary"
                  onClick={this.seeMoreProducts}>
                  See More
                  </Button>
                :''
              //   <Button color="primary" className="jr-btn jr-btn-lg btnPrimary">
              //  No More Products
              //  </Button>
                }
                
  
                </div>
                //  :
                //  <div style={{textAlign: "center"}}>
                //   <Button color="primary" style={{cursor:'default'}} className="jr-btn jr-btn-lg btnPrimary">
                //       No More Products
                      
                //     </Button>
                // </div>

                :              
                <div className="loader-view">
                <CircularProgress />
              </div>
                }

              </TabContainer>
              {/*
              <TabContainer dir={theme.direction} >
                <div className="row">
                  {ProductData.map((product, index) =>
                    <React.Fragment>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                    </React.Fragment>
                  )}
                </div>
              </TabContainer>
              <TabContainer dir={theme.direction} >
                <div className="row">
                  {ProductData.map((product, index) =>
                    <React.Fragment>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                    </React.Fragment>
                  )}
                </div>
              </TabContainer>
              <TabContainer dir={theme.direction} >
                <div className="row">
                  {ProductData.map((product, index) =>
                    <React.Fragment>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                      <div
                        className="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        {
                          this.props.history.location.pathname === '/view-seller' ? <SellerProductCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} /> : <UserProfileCard styleName="pb-4" headerStyle="bg-gradient-primary" product={product} index={index} />
                        }
                      </div>
                    </React.Fragment>
                  )}
                </div>
              </TabContainer>
            </SwipeableViews>  */}
          </div>
        </div>
      </Widget>
    );
  }
}
const mapStateToProps = ({ buyer }) => {
  const { productsByCompany, categories} = buyer;
  return { productsByCompany, categories }
};
About = connect(
  mapStateToProps,
  {
    getProductsBySeller,
    getCategories
  }         
)(About)
export default withStyles(null, { withTheme: true })(About);
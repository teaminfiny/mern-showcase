import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CustomScrollbars from 'util/CustomScrollbars';

import { connect } from 'react-redux';

import {
  getProductDetails
} from 'actions/buyer';

import OtherSellerInfo from './OtherSellerInfo'

let counter = 0;

let avatar = [<Avatar style={{ backgroundColor: "#2caf50" }}><i className="zmdi zmdi-thumb-up zmdi-hc-fw rating text-white" /></Avatar>, <Avatar style={{ backgroundColor: "#f44336" }}><i className="zmdi zmdi-thumb-down zmdi-hc-fw rating text-white" /></Avatar>];

class OtherSellers extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        popoverOpen: false
       }
    }

  

  toggle = () => {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  render() {

    const { otherSellers } = this.props;

    return (

    <CustomScrollbars className="jr-comments scrollbar" style={{height: 660}}>
        {otherSellers.length < 1 ? <div style={{textAlign:'center', marginTop:'100px', fontSize:20}}>Sorry, no other sellers found.</div>:
        otherSellers&&otherSellers.map(data => {
          return (
            <OtherSellerInfo data={data}/>
            );
        })}
      </CustomScrollbars>
    );
  }
}


const mapStateToProps = ({ buyer }) => {
  const {  otherSellers, relatedProducts, productData} = buyer;
  return {  otherSellers, relatedProducts, productData }
};

export default connect(mapStateToProps, {getProductDetails})(OtherSellers);


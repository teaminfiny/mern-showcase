import React, { Component } from 'react';
import { FormGroup, Input, Label } from 'reactstrap';


class BulkOrderModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      RequiredQuantity: '',
      discount: ''
    }
  }

  handleChange(evt) {
    const RequiredQuantity = (evt.target.validity.valid) ? evt.target.value : this.state.RequiredQuantity;
    
    this.setState({ RequiredQuantity });
  }

  handleChangeDiscount(evt) {
    const discount = (evt.target.validity.valid) ? evt.target.value : this.state.discount;
    
    this.setState({ discount });
  }


  render() {

    
    const { dataFromParent } = this.props;

    return (
      <React.Fragment>

        {/* <h1 style={{ textAlign: 'center' }}><strong>Bulk Order Request</strong></h1> */}

        <div className=''>

          {
            dataFromParent.map((value, key) => {
              return (
                <div className='row justify-content-center '>
                  <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                    <Label for="exampleEmail">Product Name</Label>
                    <Input type="text" value={value.Product.name} readonly style={{backgroundColor: "#e1e1e1"}}/>
                  </FormGroup>
                </div>
              )
            })
          }

          {
            dataFromParent.map((value, key) => {
              return (
                <div className='row justify-content-center '>
                  <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                    <Label for="exampleEmail">PTR</Label>
                    <Input type="text" value ={value.PTR} readonly style={{backgroundColor: "#e1e1e1"}}/>
                  </FormGroup>
                </div>
              )
            })
          }

          {
            dataFromParent.map((value, key) => {
              return (
                <div className='row justify-content-center '>
                  <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                    <Label for="exampleEmail">Required Quantity</Label>
                    <Input type="text" pattern="[0-9]*" onInput={this.handleChange.bind(this)} value={this.state.RequiredQuantity} />
                  </FormGroup>
                </div>
              )
            })
          }

        <div className='row justify-content-center '>
          <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
            <Label for="exampleEmail">Discount (%)</Label>
            <Input type="text" pattern="[0-9]*" onInput={this.handleChangeDiscount.bind(this)} value={this.state.discount} />
          </FormGroup>
        </div>


        </div>


      </React.Fragment>
    );
  }
}

export default BulkOrderModal;
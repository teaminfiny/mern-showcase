import React, { Component } from 'react';
import Chip from '@material-ui/core/Chip';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import Avatar from '@material-ui/core/Avatar';
import { Col, Row } from 'reactstrap';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';

import BulkOrderModal from './BulkOrderModal';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

// import { Input } from '@material-ui/core';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'

import * as moment from 'moment';

import { connect } from 'react-redux';

import {
  getProductDetails,
  getVisitedCategory,
  bulkRequest,
  addToCart,
  getCartDetails
} from 'actions/buyer';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import AxiosRequest from 'sagas/axiosRequest'

import { FormGroup, Label, Input } from 'reactstrap';
import helpertFn from 'constants/helperFunction';

import { NavLink } from 'react-router-dom';

import { Popover, PopoverBody, PopoverHeader } from 'reactstrap';
import CartPopOverForYou from '../../../components/dashboard/Common/userProfileCard/CartPopOverForYou'
import { required, minLength6, number, accountNumber, maxLength4 } from 'constants/validations';
import {Helmet} from "react-helmet";
import ReactStrapTextField from 'components/ReactStrapTextField';
import { getUserDetail } from 'actions'
import {CircularProgress} from '@material-ui/core'
const logo = 'https://s3.ap-south-1.amazonaws.com/medideals.assets/PLACEHOLER.JPG';



// let colors = ['text-red', 'text-green', 'text-blue', 'text-orange', 'text-navy', 'text-maroon', 'text-fuchsia', 'text-olive'];
// function maxLengthCheck(object) {
//   if (object.value.length > object.max.length)
//     object.value = object.value.slice(0, object.max.length)
// }

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invalid: false,

      openModal: false,

      Inventory: '',
      quantity: 0,

      requestSuccess: false,

      ptr: '',
      RequiredQuantity: '',
      discount: '',
      updatedCount: 0,
    };
    this.handleRequestDelete = this.handleRequestDelete.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleIncrement = this.handleIncrement.bind(this)
    this.handleDecrement = this.handleDecrement.bind(this)
    this.handleIncremented = this.handleIncremented.bind(this)
    this.handleDecremented = this.handleDecremented.bind(this)
  }

  handleChange(evt) {
    const RequiredQuantity = (evt.target.validity.valid) ? evt.target.value : this.state.RequiredQuantity;
    if (evt.target.value.length <= 7) {
      this.setState({ RequiredQuantity });
    }
  }

  handleChangeDiscount(evt) {
    const discount = (evt.target.validity.valid) ? evt.target.value : this.state.discount;
    this.setState({ discount });
  }

  handleRequestDelete = () => {
  };

  handleClick = () => {
    this.props.history.push('/view-seller')
  }

  componentDidMount() {
    this.props.getProductDetails({ inventory_id: this.props.match.params.id })
    const user = localStorage.getItem("buyer_token")
  }

  handleOpenModal = () => {
    this.setState({
      openModal: true,
      RequiredQuantity: '',
      discount: '',
    })
  }

  handleClose = () => {
    this.setState({ openModal: !this.state.openModal })
    this.setState({ requestSuccess: false })
  }

  handleBulkRequest = () => {
    if (this.state.RequiredQuantity.length >= 1 && this.state.RequiredQuantity.length <= 7) {
      const data = {
        inventory_id: this.props.match.params.id,
        ptr: this.props.productData[0].PTR,
        qty: this.state.RequiredQuantity,
        discount: this.state.discount
      }
      const user = localStorage.getItem("buyer_token")
      if (user !== null) {
        this.props.bulkRequest({ data })
      }
      this.setState({ requestSuccess: !this.state.requestSuccess })
    }
    else {
      NotificationManager.error("Please enter required quantity.")
    }
  }


  handleIncrement = () => {
    if (this.state.quantity < this.props.productData[0].max_order_quantity) {
      this.setState(prevState => {
        return {
          quantity: Number(prevState.quantity) + 1
        }
      })
    }
    else if (this.state.quantity <= this.props.productData[0].max_order_quantity) {
      this.setState({ invalid: true })
    }
  }

  handleDecrement = () => {
    if (this.state.quantity > this.props.productData[0].min_order_quantity) {
      this.setState(prevState => {
        return {
          quantity: prevState.quantity - 1
        }
      })
    }
    else if (this.state.quantity < this.props.productData[0].min_order_quantity) {
      this.setState({ invalid: true })
    }
  }


  handleIncremented = (key, event) => {

    if (this.state.updatedCount + this.props.productData[0].min_order_quantity > this.props.productData[0].max_order_quantity) {

      // self.view.makeToast("Quantity should be a multiple of minimum quantity and cannot exceed max quantity.", duration: Config.defaultToastDuration, position: .center)
    }
    else if (this.state.updatedCount <= Number(this.props.productData[0].max_order_quantity)) {
      this.setState({
        updatedCount: Number(this.state.updatedCount) + Number(this.props.productData[0].min_order_quantity)
      })

    }

  }

  handleDecremented = (key, event) => {

    if (this.state.updatedCount != this.props.productData[0].min_order_quantity && this.state.updatedCount - this.props.productData[0].min_order_quantity >= this.props.productData[0].min_order_quantity) {
      this.setState({

        updatedCount: Number(this.state.updatedCount) - Number(this.props.productData[0].min_order_quantity)
      })

    }
    // (this.state.updatedCount >=  Number(this.props.productData[0].min_order_quantity))


  }



  // handleQuantityChange = (key, event) => {
  //   // if(event.target.value < this.props.productData.max_order_quantity && event.target.value > this.props.productData.min_order_quantity){
  //       this.setState({ quantity: event.target.value });
  //   // }
  // };

  handleQuantityChange = (key, event) => {

    this.setState({
      quantity: parseInt(event.target.value),
    });
  };

  handleQuantityChanged = (key, event) => {
    NotificationManager.warning('Please use buttons for increment and decrement')
  };

  onSubmit = async (e) => {
    // const isLogin = localStorage.getItem("buyer_token");
    // if (isLogin) {

    let data

    if (this.props.productData[0].Discount && this.props.productData[0].Discount.discount_type === "Same") {
      if (this.state.updatedCount >= this.props.productData[0].min_order_quantity && this.state.updatedCount <= this.props.productData[0].max_order_quantity && Number.isInteger(this.state.updatedCount) === true) {
        data = {
          cart_details: [{
            inventory_id: this.props.productData[0].inventory_id,
            quantity: this.state.updatedCount
          }]
        }
        let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'cart/addtoCart', '', data)
        if (response.data.error) {
          NotificationManager.error("Please Sign In.")
        } else {
          NotificationManager.success(response.data.title)
          const isLogin = localStorage.getItem("buyer_token");
          if (isLogin) {
            this.props.getCartDetails({ history: this.props.history })
          }
        }
      }
    }

    else if (this.state.quantity >= this.props.productData[0].min_order_quantity && this.state.quantity <= this.props.productData[0].max_order_quantity && Number.isInteger(this.state.quantity) === true) {
      data = {
        cart_details: [{
          inventory_id: this.props.productData[0].inventory_id,
          quantity: this.state.quantity
        }]
      }
      let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'cart/addtoCart', '', data)
      if (response.data.error) {
        NotificationManager.error("Please Sign In.")
      } else {

        NotificationManager.success(response.data.title)
        const isLogin = localStorage.getItem("buyer_token");
        if (isLogin) {
          this.props.getCartDetails({ history: this.props.history })
        }
      }
    }

    else {
      NotificationManager.error(!this.state.quantity ? 'Please enter quantity' : "Please check Minimum & Maximum order quantity.")
    }
  }

  //   else if ((this.state.quantity >= this.props.productData[0].min_order_quantity && this.state.quantity <= this.props.productData[0].max_order_quantity)) {
  //      let obj = JSON.parse(localStorage.getItem("obj")?localStorage.getItem("obj"):"[]");
  //      let data;
  //      obj.push(
  //       data = {
  //         // cart_details: [{
  //       "inventory_id": this.props.match.params.id,
  //       "quantity": this.state.quantity,
  //       "title":this.props.productData[0].Product.name,
  //       "image":this.props.productData[0].Product.images[0],
  //       "seller_name":this.props.productData[0].Seller.first_name,
  //       "price":this.props.productData[0].PTR,
  //       "gstPercentage":this.props.productData[0].GST.value,
  //       "user_id":this.props.productData[0].user_id,
  //       "Discount":this.props.productData[0].Discount,
  //       "discountPercentage":this.props.productData[0].Discount && this.props.productData[0].Discount.discount_per,
  //       "min_order_quantity":this.props.productData[0].min_order_quantity,
  //       "max_order_quantity":this.props.productData[0].max_order_quantity,
  //       "time":this.props.productData[0].updatedAt,
  //       "MRP":this.props.productData[0].MRP,
  //       "PTR":this.props.productData[0].PTR,
  //     // }]
  //   })
  //     // let val = localStorage.setItem("task", obj);
  //     localStorage.setItem('obj', JSON.stringify(obj))
  //     NotificationManager.success("Item added successfully")
  //   }
  //   else {
  //     NotificationManager.error("Please check Minimum & Maximum order quantity.")
  //   }
  // }

  maxLengthCheck = (event) => {
    if (event.target.value > this.props.productData[0].max_order_quantity) {
      event.target.value = this.props.productData[0].min_order_quantity
      this.setState({ invalid: true })
    }
    //  let t = setTimeout(() => {
    //     this.setState({ invalid: false });
    //   }, 3000);
    //   clearInterval(t);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.productData !== this.props.productData) {
      this.setState({
        quantity: this.props.productData[0].min_order_quantity,
  updatedCount: this.props.productData[0].Discount && this.props.productData[0].Discount.discount_type === "Same" ? this.props.productData[0].min_order_quantity : '',
        invalid: false
      })
    }
  }


  render() {
    const user = localStorage.getItem("buyer_token")
    // let updatedCount = this.props.productData[0].min_order_quantity;
    const {  productData } = this.props;
    let effects;
    let effects1;
    let propsTitle = this.props.match.params.keyword;
    let title = this.props.match.params.keyword && this.props.match.params.keyword.toUpperCase().split('-').join(' ');
    const bgColor = productData && productData.length > 0 && productData[0].medi_type === 'Ethical branded' || productData && productData.length > 0 && productData[0].medi_type === 'Others' ? '#ff7000' :
    productData && productData.length > 0 &&productData[0].medi_type === 'Cool chain' ? '#0b68a8' :
    productData && productData.length > 0 &&productData[0].medi_type === 'Surgical' || productData && productData.length > 0 && productData[0].medi_type ==='OTC' || productData && productData.length > 0 && productData[0].medi_type ==='Generic' ? '#038d0e' :'#072791'


    return (
      <React.Fragment>
        {this.props.loading ? 
                <div className="loader-view"
                  style={{ height: this.props.width >= 1200 ? 'calc(100vh - 259px)' : 'calc(100vh - 238px)' }}>
                  <CircularProgress />
                </div>
        :
      <div className='pt-3'>
        <Helmet>
            <title>Buy {title} Online In India - Medimny</title>
            <meta name="title" content={`Buy ${title} Online In India - Medimny`} />
            <meta name="description" content={`Medimny is a B2B E-Commerce platform for Pharmaceutical Products for Distributors, Stockists and Retail Drug Stores serving entire nation. Buy ${title} Online In India.`} />
        </Helmet>
        {
          productData.map((value, key) => {
            return (
              <React.Fragment>
              {/* <span className='mr-3' style={{padding:'3px', fontWeight:'bold', backgroundColor:`${bgColor}`, color:'white', borderRadius:'50%'}}>{value.medi_type}</span> */}
                <Chip
                  label={value.medi_type === 'Others' ? 'PCD' :value.medi_type}
                  size='small'
                  style={{backgroundColor:`${bgColor}`, color:'white', marginBottom:'10px'}}
                />
                {
                  value && value.medi_attribute.includes('Jumbo Deal') ?
                    <Chip
                      label='Jumbo Deal'
                      size='small'
                      style={{backgroundColor:`${bgColor}`, color:'white', marginBottom:'10px', marginLeft:'10px'}}
                    />
                  :""
                }
                <h1><strong>{value.Product.name}</strong></h1>
              </React.Fragment>
            )
          })
        }

        {
          productData.map((value, key) => {
            return (
              <React.Fragment>
              <NavLink className="buyerRedirectNavlink" to={`/view-seller/${value && value.Seller && value.Seller._id}`}>
                <Chip
                  label={value.Seller.company_name}
                  deleteIcon={
                    <Avatar className="bg-success iconAvtarColor text-white">
                      <ThumbUpIcon style={{ fontSize: '0.6rem' }} /></Avatar>
                  }
                  onClick={this.handleClick}
                  size='small'
                />
              </NavLink>
              </React.Fragment>
          )})
        }

        {/* <Row>
          {
            productDataOne.map((value, key) => {
              return <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-5">
                <div className="media flex-nowrap  mb-2">
                  <div className="mr-3">
                    {
                      value === 'expiry_date' ? 
                      <i className={`zmdi zmdi-${value.icon} jr-fs-xlxl text-red`} /> 
                      :  
                      value.title === 'Discount on PTR' ?
                      <i className={`zmdi zmdi-${value.icon} jr-fs-xlxl text-green`} /> 
                      : 
                      <i className={`zmdi zmdi-${value.icon} jr-fs-xlxl text-primary`} />
                    } */}
        {/* <i className={`zmdi zmdi-${value.icon} jr-fs-xlxl text-primary`} /> */}
        {/* </div>

                  <div className="media-body">
                    <h6 className="mb-1 text-grey">{value.title}</h6>
                    <p className="mb-0">{value.details}</p>
                  </div>

                </div>

              </Col>
            })
          } 
  </Row>  */}

        <Row>

          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-5">
                  <NavLink className="buyerRedirectNavlink" to={`/view-company/${value.Company._id}`}>
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-city-alt jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Manufacturer</h5>
                      <p className="mb-0 text-primary">{value.Company.name}</p>
                    </div>
                  </div>
              </NavLink> 
                </Col>
              )
            })
          }

          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-5">
                  <div className="media flex-nowrap  mb-2">
                    {(moment(value.expiry_date).diff(moment(), 'months') <= 6) ?
                      <React.Fragment>
                        <div className="mr-3">
                          <i className={`zmdi zmdi-calendar-close animated infinite wobble jr-fs-xlxl text-danger`} />
                        </div>
                        <div className="media-body">
                          <h5 className="mb-1 text-danger">Expiry Date</h5>
                          <p className="mb-0 text-danger">{moment(value.expiry_date).format('ll')}</p>
                        </div>
                      </React.Fragment>
                      :
                      <React.Fragment>
                        <div className="mr-3">
                          <i className={`zmdi zmdi-calendar-close jr-fs-xlxl text-primary`} />
                        </div>
                        <div className="media-body">
                          <h5 className="mb-1 text-grey">Expiry Date</h5>
                          <p className="mb-0">{moment(value.expiry_date).format('ll')}</p>
                        </div>
                      </React.Fragment>
                    }
                  </div>
                </Col>
              )
            })
          }

          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-5">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-collection-text jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Type</h5>
                      <p className="mb-0">{value.Product.product_type}</p>
                    </div>
                  </div>
                </Col>
              )
            })
          }

          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-long-arrow-down jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Minimum Order Quantity </h5>
                      <p className="mb-0">{value.min_order_quantity}</p>
                    </div>
                  </div>
                </Col>)
            })
          }

          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-long-arrow-up jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Maximum Order Quantity</h5>
                      <p className="mb-0">{value.max_order_quantity}</p>
                    </div>
                  </div>
                </Col>
              )
            })
          }


          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-folder jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Product Category</h5>
                      <p className="mb-0">{value.Product_Category.name}</p>
                    </div>
                  </div>
                </Col>
              )
            })
          }
        
          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <h1 style={{ fontSize: '37px', color: '#072791' }}>₹</h1>
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">MRP</h5>
                      <p className="mb-0">₹{(value.MRP).toFixed(2)}</p>
                    </div>
                  </div>
                </Col>
              )
            })
          }

          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-shopping-basket jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">GST</h5>
                      <p className="mb-0">{value.Product.GST}%</p>
                    </div>
                  </div>
                </Col>
              )
            })
          }
          {
            productData.map((value, key) => {
              return (
                <Col xs={6} sm={6} md={6} lg={4} xl={4} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-pin jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Country Of Origin</h5>
                      <p className="mb-0">{value.Product.country_of_origin}</p>
                    </div>
                  </div>
                </Col>
              )
            })
          }

           {
            productData.map((value, key) => {
              return (
                <Col xs={8} sm={8} md={8} lg={6} xl={6} className="pt-4">
                  <div className="media flex-nowrap  mb-2">
                    <div className="mr-3">
                      <i className={`zmdi zmdi-library jr-fs-xlxl text-primary`} />
                    </div>
                    <div className="media-body">
                      <h5 className="mb-1 text-grey">Chemical Combination</h5>
                      <p className="mb-0">{value.Product.chem_combination}</p>
                    </div>
                  </div>
                </Col>
              )
            })
          } 
          {
            productData.map((value, key) => {

              return (
                value.Discount && value.Discount !== undefined ?
                  <Col xs={8} sm={8} md={8} lg={6} xl={6} className="pt-4">
                    <div className="media flex-nowrap  mb-2">
                      {value.Discount == undefined ?
                        <React.Fragment style={{ display: 'none' }} ></React.Fragment>
                        :
                        <React.Fragment>
                          <div className="mr-3">
                            <i className={`zmdi zmdi-label jr-fs-xlxl text-primary`} />
                          </div>
                        </React.Fragment>}
                      <div className="media-body">
                        {value.Discount && value.Discount.discount_type === "Discount" ?
                          <React.Fragment>
                            <h5 className="mb-1 text-grey">Discount on PTR</h5>
                            <p className="mb-0">{value.Discount && (value.Discount.discount_per).toFixed(2)}%</p>
                          </React.Fragment>
                          :
                          <React.Fragment>
                            {value.Discount && value.Discount.discount_type === "Same" ?
                              <React.Fragment>
                                <h5 className="mb-1 text-grey">Offer</h5>
                                <span className={'DiscountPrice'}>Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} Free</span>
                              </React.Fragment>
                              :
                              value.Discount && value.Discount.discount_type === "SameAndDiscount" ?
                              <React.Fragment>
                                <h5 className="mb-1 text-grey">Offer</h5>
                                <span className={'DiscountPrice'}>Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} Free, and {value.Discount && (value.Discount.discount_per).toFixed(2)}% Off</span>
                              </React.Fragment>
                              :
                              value.Discount && value.Discount.discount_type === "DifferentAndDiscount" ?
                              <React.Fragment>
                                <h5 className="mb-1 text-grey">Offer</h5>
                                <span className={'DiscountPrice'}>Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} {value.OtherProducts.name} Free, and {value.Discount && (value.Discount.discount_per).toFixed(2)}% Off</span>
                              </React.Fragment>
                              :
                              value.Discount && value.Discount.discount_type === "Different" ?
                                <React.Fragment>
                                  <h5 className="mb-1 text-grey">Offer</h5>
                                  <span className={'DiscountPrice'}>Buy {value.Discount.discount_on_product.purchase} Get {value.Discount.discount_on_product.bonus} {value.OtherProducts.name} Free</span>
                                </React.Fragment>
                                :
                                value.Discount && value.Discount.type === "" ? //FOR NO DISCOUNT
                                  <React.Fragment style={{ display: 'none' }}>
                                  </React.Fragment>
                                  :
                                  <React.Fragment style={{ display: 'none' }}>
                                  </React.Fragment>
                            } </React.Fragment>
                        }
                      </div>
                    </div>
                  </Col>
                  :
                  <React.Fragment></React.Fragment>
              )
            })
          }
        </Row>



        {/* <div className="media flex-nowrap mb-3 mt-1 ">
          <div className="mr-3">
            <i className={`zmdi zmdi-library jr-fs-xlxl text-primary`} />
          </div>    
          {
            productData.map((value, key) => {
              return (
                <div className="media-body">
                  <h5 className="mb-1 text-grey">Chemical Combination</h5>
                  <p className="mb-0">{value.Product.chem_combination}</p>
                </div>
              )
            })
          }
       </div> */}

        {
          productData.map((value, key) => {
            if (value.Discount && value.Discount.discount_type === "Same") {
              effects = Number((value.PTR.toFixed(2) * value.Discount.discount_on_product.purchase));
              effects1 = Number(effects) / (Number(value.Discount.discount_on_product.purchase) + Number(value.Discount.discount_on_product.bonus));
            }
            return (
              <div   >
                {value.Discount && value.Discount.discount_type === "Discount" ?
                  <div>
                    <span className='priceColorProduct'> &#x20B9;{(value.ePTR).toFixed(2)}</span>
                    <span className='originalPriceProduct ml-2'> &#x20B9;{value.PTR.toFixed(2)}</span>
                    <br/>
                    <span className='text-danger'>*Excluding GST</span>
                  </div>
                  :
                  value.Discount && value.Discount.discount_type === "Same" ?
                    <div><h5 className="mb-1 text-grey">Offered Price :</h5>&nbsp;
                <span className={'priceColor'}>&#x20B9;{(value.ePTR).toFixed(2)}</span>&nbsp;
                <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>
                <br/>
                    <span className='text-danger'>*Excluding GST</span>
                    </div>
                     :
                     value.Discount && value.Discount.discount_type === "SameAndDiscount" ?
                       <div><h5 className="mb-1 text-grey">Offered Price :</h5>&nbsp;
                   <span className={'priceColor'}>&#x20B9;{(value.ePTR).toFixed(2)}</span>&nbsp;
                   <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>
                   <br/>
                       <span className='text-danger'>*Excluding GST</span>
                       </div> 
                       :
                       value.Discount && value.Discount.discount_type === "DifferentAndDiscount" ?
                         <div><h5 className="mb-1 text-grey">Offered Price :</h5>&nbsp;
                     <span className={'priceColor'}>&#x20B9;{(value.ePTR).toFixed(2)}</span>&nbsp;
                     <span className={'originalPrice font'}>&#x20B9;{value.PTR.toFixed(2)}</span>
                     <br/>
                         <span className='text-danger'>*Excluding GST</span>
                         </div>:

                    <span className='priceColorProduct'> &#x20B9;{(value.ePTR).toFixed(2)}</span>

                }
                {value.Discount && value.Discount.discount_type === "Different" ?
                  <div>
                  <span className='text-danger'>*Excluding GST</span>
                  </div> : ''
                }
                {value.Discount  === undefined ?
                  <div>
                  <span className='text-danger'>*Excluding GST</span>
                  </div>
                  : ''
                }

                  {
                    helpertFn.showPrepaid(value.medi_type, value.Product.isPrepaid && value.Product.isPrepaid, value.prepaidInven &&value.prepaidInven) &&
                      <div>
                        <Chip
                          label={'Only Prepaid'}
                          size='small'
                          style={{backgroundColor:'red', color:'white'}}
                        />
                      </div>
                  }
                <br />
                {value.Discount && (value.Discount.discount_type === "Same" || value.Discount.discount_type === "SameAndDiscount" || value.Discount.discount_type === "Different" || value.Discount.discount_type === "DifferentAndDiscount") ?
                  <div style={{ display: "flex", marginTop: "-7px" }} >
                    <span className="mt-1 text-grey"
                      // onClick={this.handleDecremented}
                      onClick={(e) => this.handleDecremented('minOrdntity', e)}>
                      <i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span>
                    <Input
                      id="minOrderQuantity"
                      name="minOrderQuantity"
                      type="number"
                      updatedCount={this.props.productData[0].min_order_quantity}
                      value={this.state.updatedCount}
                      placeholder="Quantity"
                      max={this.props.productData[0].max_order_quantity}
                      min={this.props.productData[0].min_order_quantity}
                      onChange={(e) => this.handleQuantityChanged('minOrderQuantity', e)}
                      onInput={this.maxLengthCheck.bind(this)}
                      style={{ width: "100px", fontSize: "17px", marginLeft: "5px", marginRight: "1px", textAlign: 'center' }} />

                    <span className="ml-1 mt-1 text-grey"
                      // onClick={this.handleIncremented}
                      onClick={(e) => this.handleIncremented('minOrderQuantity', e)}>
                      <i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span>
                  </div>
                  :
                  <div style={{ display: "flex" }} >
                    <span className=" mt-1 text-grey" onClick={this.handleDecrement}><i class="zmdi zmdi-minus-circle-outline  zmdi-hc-2x"></i></span>
                    <Input
                      id="minOrderQuantity"
                      name="minOrderQuantity"
                      type="number"
                      value={this.state.quantity}
                      max={this.props.productData[0].max_order_quantity}
                      min={this.props.productData[0].min_order_quantity}
                      onChange={(e) => this.handleQuantityChange('minOrderQuantity', e)}
                      onInput={this.maxLengthCheck.bind(this)}
                      style={{ width: "100px", fontSize: "17px", marginLeft: "5px", marginRight: "1px", textAlign: 'center' }} />

                    <span className="ml-1 mt-1 text-grey" onClick={this.handleIncrement}><i class="zmdi zmdi-plus-circle-o  zmdi-hc-2x"></i></span>
                  </div>

                }
              </div>
            )
          })
        }

        <Row style={{ visibility: this.state.invalid === true ? "" : "hidden" }}>
          <h5 className="mt-2 ml-3 text-danger">Quantity should be between Minimum & Maximum order quantity.</h5>
        </Row>

        {/* <React.Fragment> 
          
          {this.state.quantity <= this.props.productData[0]&&this.props.productData[0].max_order_quantity  || this.state.quantity >= this.props.productData[0]&&this.props.productData[0].min_order_quantity ? 
           <h5 className="mt-2 ml-3 text-danger"></h5>
            : 
            Number.isInteger(this.state.quantity) === false  && Number.isInteger(this.state.quantity) ?
            <h5 className="mt-2 ml-3 text-danger">
              Please enter valid quantity</h5>
            :
            <React.Fragment> </React.Fragment>
            }
          
        </React.Fragment> */}
        { productData.map((value, key) => { 
       if(value && value.Seller ){
       return <div className='row ml-1'>
          <button
            style={{ padding: '10px 15px 10px 15px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none', marginRight: "30px" }}
            onClick={this.onSubmit}
          >
            Add To Cart
            </button>
          {user !== null ?
            <button style={{ padding: '10px 15px 10px 15px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} onClick={this.handleOpenModal}>Bulk Order</button>
            :
            null
          }
        </div>}
        else{
         return <div className='row ml-1'>
          <button
            style={{ padding: '10px 15px 10px 15px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none', marginRight: "30px",cursor: 'default' }}
            
          >
            Product Not Available
            </button>
          {/* {user !== null ?
            <button style={{ padding: '10px 15px 10px 15px', backgroundColor: '#072791', color: 'white', borderRadius: '5px', border: 'none' }} onClick={this.handleOpenModal}>Bulk Order</button>
            :
            null
          } */}
        </div>
        } 
           })}

        <Dialog open={this.state.openModal == true}
          onClose={this.handleClose}
          fullWidth={true}>
          {
            this.state.requestSuccess === true ? null :
              <DialogTitle id="alert-dialog-title" ><h1>Bulk Order Request</h1></DialogTitle>
          }

          <DialogContent style={{ overflow: "hidden" }}>
            {
              this.state.requestSuccess === true ?

                <DialogTitle className="mt-4" id="alert-dialog-title" style={{ textAlign: "center" }}>
                  <span className="text-success mb-3">
                    <i class="zmdi zmdi-check-circle animated fadeInUp zmdi-hc-5x"></i>
                  </span>
                  <h1 className="mt-4 font-weight-bold">Bulk Order Request Sent Successfully</h1>
                </DialogTitle>
                :
                <div className=''>
                  <DialogContentText>
                    To request a larger quantity, please fill in the below details.
						      </DialogContentText>

                  {
                    productData.map((value, key) => {
                      return (
                        <div className='row justify-content-center '>
                          <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                            <Label for="exampleEmail">Product Name</Label>
                            <Input type="text" value={value.Product.name} readonly style={{ backgroundColor: "#e1e1e1" }} />
                          </FormGroup>
                          <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                            <Label for="exampleEmail">Prepaid Only</Label>
                            <Input type="text" value={value.Product.isPrepaid && value.Product.isPrepaid === true ? 'TRUE' :'FALSE' } readonly style={{ backgroundColor: "#e1e1e1" }} />
                          </FormGroup>
                        </div>
                      )
                    })
                  }

                  {
                    productData.map((value, key) => {
                      return (
                        <div className='row justify-content-center '>
                          <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                            <Label for="exampleEmail">PTR</Label>
                            <Input type="text" value={value.PTR} readonly style={{ backgroundColor: "#e1e1e1" }} />
                          </FormGroup>
                        </div>
                      )
                    })
                  }

                  {
                    productData.map((value, key) => {
                      return (
                        <div className='row justify-content-center '>
                          <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                            <Label for="exampleEmail">Required Quantity</Label>
                            <Input
                              type="number"
                              onInput={this.handleChange.bind(this)}
                              value={this.state.RequiredQuantity}
                              validate={[required]} />
                          </FormGroup>
                        </div>
                      )
                    })
                  }

                  <div className='row justify-content-center '>
                    <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}>
                      <Label for="exampleEmail">Expected Discount</Label>
                      <Input type="text" onInput={this.handleChangeDiscount.bind(this)} value={this.state.discount} />
                    </FormGroup>
                  </div>
                </div>

            }

          </DialogContent>

          <DialogActions className="pr-4">
            {
              this.state.requestSuccess === true ?
                <Button onClick={this.handleClose} color='secondary' >	Close </Button>
                :
                <div>
                  <Button onClick={this.handleClose} color='secondary' >	Cancel </Button>
                  <Button type="submit" color='primary' onClick={this.handleBulkRequest}> Place Bulk Order </Button>
                </div>
            }
          </DialogActions>
        </Dialog>

      </div>
      }
      </React.Fragment>
    );
  }
}


const mapStateToProps = ({ buyer }) => {
  const { data, otherSellers, relatedProducts, productData, visitedCategory, bulkRequest, addToCart, cartDetails, loading } = buyer;
  return { data, otherSellers, relatedProducts, productData, visitedCategory, bulkRequest, addToCart, cartDetails, loading }
};

export default withRouter(connect(mapStateToProps, { getProductDetails, getVisitedCategory, bulkRequest, addToCart, getCartDetails })(ProductDetail));


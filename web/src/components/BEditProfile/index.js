import React, { Component } from 'react';
import { Row, Col } from 'reactstrap'
import Button from '@material-ui/core/Button';
import { FormGroup, Input, Label } from 'reactstrap'
import ReactStrapTextField from 'components/ReactStrapTextField';
import asyncValidate from 'constants/asyncValidate';
import { Field, reduxForm, getFormInitialValues, initialize, change } from 'redux-form'
import { required, validatePincode, validatePhone, emailField,specailChar } from './BEditValidation';
import { connect } from 'react-redux'
import AxiosRequest from 'sagas/axiosRequest'
import { NotificationManager } from 'react-notifications';
import AppConfig from 'constants/config'
import axios from 'axios'
import ReactStrapSelectField from 'components/reactstrapSelectField';
import {withRouter} from 'react-router-dom'
import { getUserDetail,logoutUser } from 'actions'






class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      gender: '',
      loading: false,
      success: true,
      address: '',
      fileName: 'GST.png',
      pincode: '',
      detail:'',
      openModal: false,
      stateList: [],
      // userState:'',
      // value: ''
    }
  }



  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  handleButtonClick = () => {
    if (!this.state.loading) {
      this.setState(
        {
          loading: true,
        },
        () => {
          this.timer = setTimeout(() => {
            this.setState({
              loading: false,
            });
          }, 2000);
        },
      );
    }
  };

  timer = undefined;
 

  handleFileSelect = (e) => {
    // console.log('e.....', e.target.files["0"]);
    e.preventDefault();
    let document = "";
    let reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function () {
      document = reader.result;
    };
    reader.onerror = function (error) {
    };
    this.setState({ fileName: e.target.files["0"].name })
  }


  handleOpenModal = () => {
    this.setState({ openModal: true })
  }

  handleClose = () => {
    this.setState({ openModal: false })
  }

  onSave = async (e) => {
    let data = {
      // Id: this.props.editData ? this.props.editData._id : '',
      first_name: this.state.firstName,
      last_name: this.state.lastName,
      // company_name: this.state.companyName,
      email: this.state.email,
      phone: this.state.phone,
      user_address: this.state.userAddress,
      user_city: this.state.userCity,
      user_state:this.state.value ? this.state.value : this.state.userState,
      user_pincode: this.state.pincode,
 

    }
    let url = this.prop
    let response = await AxiosRequest.axiosBuyerHelperFunc('post', 'users/updateProfile', '', data)
    this.handleClose()
    if (response.data.error) {
      NotificationManager.error(response.data.title)
      this.props.logoutUser(response.data,this.props.history)
    } else {
      NotificationManager.success(response.data.message)
      this.props.getUserDetail({userType:"buyer"})
      // axios.get(`${AppConfig.baseUrl}users/getuserDetails`,{
      //   token:localStorage.getItem('buyer_token')
      // })
    
      let data2 = {
        page: this.props.page === 0 ? 1 : this.props.page,
        perPage: this.props.perPage
      }

    
    }
  }


  componentDidMount() {
    // this.props.getUserDetail();
    let data1 = localStorage.getItem('buyer_token');
    // this.props.getUserdetailDispatcher({data1});

    axios({
      method:'get',
      url:`${AppConfig.baseUrl}admin/getStates`
    }).then((result)=>{
      const {detail} = result.data
      // let temp = [...this.state.states]
      this.setState({
        detail,
        stateList: detail
      })
    })

   
    const { first_name, last_name, company_name, email, phone, user_address, user_pincode,_id,user_city,user_state } = this.props.userDataFromParentTwo;
    this.props.initialize({ firstName: first_name, lastName: last_name,  email: email, phone: phone, userAddress: user_address, pincode: user_pincode,staffId:_id ,
    userCity: user_city, userState:user_state?user_state._id:this.state.value,
    // companyName: company_name,
  })
   
    this.setState({
      firstName: first_name, lastName: last_name, companyName: company_name, email: email,
      phone: phone, userAddress: user_address, pincode: user_pincode,staffId:_id, userCity: user_city,  userState:user_state?user_state._id:this.state.value,
      // companyName: company_name,
    })
   
  }

  handleChanged = (event) => {
    this.setState({ value:  event.target.value });   
    }
   
  handleChange = (key, event) => {
      this.setState({ [key]: event.target.value });
    }




  render() {
    
    const { userDataFromParentTwo } = this.props;
    const { detail } = this.props;

    const {stateList} = this.state;

    const { email, phone1, gender, loading, success, userAddress1, fileName, pincode1, firstName, lastName, companyName, Email, phone, userAddress, pincode,userCity,userState } = this.state;
    let { buttonType, pristine, invalid, handleSubmit } = this.props;
    const Gender = ['Male', 'Female', 'Other']

    const { openModal } = this.state

  
    
    return (

      <div className='mt-5 mb-5'>
        <form onSubmit={handleSubmit(this.onSave)} autoComplete="off">
          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>

              <Field id="firstName"
                name="firstName" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[required,specailChar]}
                label='First Name'
                value={firstName}
                onChange={(e) => this.handleChange('firstName', e)}
              />
            </Col>
          </Row>

          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="lastName" name="lastName" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[required,specailChar]}
                label='Last Name'
                value={lastName}
                onChange={(e) => this.handleChange('lastName', e)}
              />
            </Col>
          </Row>


          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="email" name="email" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[emailField]}
                label='Email'
                value={email}
                onChange={(e) => this.handleChange('email', e)}
              />
            </Col>
          </Row>


          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="phone" name="phone" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[validatePhone]}
                label='Phone'
                value={phone}
                onChange={(e) => this.handleChange('phone', e)}
              />
            </Col>
          </Row>

          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="userAddress" name="userAddress" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[required]}
                label='Address'
                value={userAddress}
                onChange={(e) => this.handleChange('userAddress', e)}
              />
            </Col>
          </Row>

          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="userCity" name="userCity" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[required,specailChar]}
                label='City'
                value={userCity}
                onChange={(e) => this.handleChange('userCity', e)}
              />
            </Col>
          </Row>

          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="userState" name="userState" type="select"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapSelectField}
                validate={[required]}
                value={userState}
                onChange={this.handleChanged} label='State' >
                <option selected={userState==''} value={''}></option>
                {
                  stateList && stateList.map((val) => {
                    return (<option selected={val._id==userState} value={val._id}>{val.name}</option>)
                  })
                }
              </Field>
            </Col>
          </Row>

          <Row className='row justify-content-center '>
            <Col sm={10} md={10} lg={10} xs={10} xl={10}>
              <Field id="pincode" name="pincode" type="text"
                // classNameField={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10'}
                component={ReactStrapTextField}
                validate={[validatePincode]}
                label='Pincode'
                value={pincode}
                onChange={(e) => this.handleChange('pincode', e)}
              />
            </Col>
          </Row>


          <Row className='row justify-content-center mt-2'>
            <FormGroup className={'col-sm-10 col-lg-10 col-md-10 col-xs-10 col-xl-10 buyerEditProfile'}>
              <Button 
              // onClick={this.handleOpenModal}
              // onClick={this.onSave} 
              type='submit'
               color="primary" variant="contained" className="jr-btn jr-btn-lg btnPrimary" >
              Save
              </Button>
            </FormGroup>
          </Row>



        </form>


      </div>

    );
  }
}

  const mapStateToProps = ({seller})=>{
    return{seller}
  }
 EditProfile = reduxForm({
  form: 'EditProfile',// a unique identifier for this form
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  asyncValidate,
  initialValues: getFormInitialValues('EditProfile')(),
  // onSubmitSuccess: (result, dispatch) => {
  //   const newInitialValues = getFormInitialValues('EditProfile')();
  //   dispatch(initialize('EditProfile', newInitialValues));
  // }
})(EditProfile)
export default withRouter(connect(mapStateToProps,{getUserDetail,logoutUser})(EditProfile))
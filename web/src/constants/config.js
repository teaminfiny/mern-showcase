const AppConfig = {
  // baseUrl:'https://medideals.infiny.dev/',
  // baseUrl: 'http://localhost:4002/',
  baseUrl: 'https://api.medideals.in/',
  // productImageUrl: 'https://medideals.infiny.dev/products/'
  productImageUrl: 'https://s3.ap-south-1.amazonaws.com/medideals.assets/',

  //feature
  // baseUrl:'https://medimny-dev.infiny.dev/'
}

export default AppConfig;
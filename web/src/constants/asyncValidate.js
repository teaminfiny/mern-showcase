import AxiosFunction from '../sagas/axiosRequest'

const asyncValidate = async (values) => {
    console.log("Axios values",values)
    let Id = values.staffId?values.staffId:''
    let errors= {}
    if(values.email){
        let data = await AxiosFunction.axiosHelperFunc('post', 'users/validateEmail', '', {email:values.email,Id})
        console.log("Axios", data)
        if (data.data.isExist) {
            errors.email ='Email already taken.' 
        }
    }
    if(values.phone){
        let data = await AxiosFunction.axiosHelperFunc('post', 'users/validateEmail', '', {phone:values.phone,Id})
        console.log("Axios", data)
        if (data.data.isExist) {
            errors.phone='This contact number already taken'
        }
    }
    if(errors.phone||errors.email){
        throw errors
    }
   
    
   
};

export default asyncValidate;
import React, { Component } from 'react';

const customHead = (o, updateDirection) => {
  return <th key={o.label} className="MuiTableCell-root MUIDataTableHeadCell-root-298 MUIDataTableHeadCell-fixedHeader-299 MuiTableCell-head">
    <span style={{ fontSize: '1rem' }}>{o.label}</span>
  </th>;
}

export default customHead;
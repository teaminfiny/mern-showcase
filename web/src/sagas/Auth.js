import { all, call, fork, put, takeEvery,takeLatest } from "redux-saga/effects";
import {
  auth,
  facebookAuthProvider,
  githubAuthProvider,
  googleAuthProvider,
  twitterAuthProvider
} from "../firebase/firebase";

import {
  SIGNIN_FACEBOOK_USER,
  SIGNIN_GITHUB_USER,
  SIGNIN_GOOGLE_USER,
  SIGNIN_TWITTER_USER,
  SIGNIN_USER,
  SIGNOUT_USER,
  SIGNUP_USER
} from "constants/ActionTypes";

import { showAuthMessage, userSignInSuccess, userSignOutSuccess, userSignUpSuccess } from "actions/Auth";

import {
  userFacebookSignInSuccess,
  userGithubSignInSuccess,
  userGoogleSignInSuccess,
  userTwitterSignInSuccess,

} from "../actions/Auth";

import {
  addToCart,
  getCartDetails,
} from 'actions/buyer'

import {getProductsForYou} from '../actions/buyer'
import {getMediWallet} from '../actions/buyer'

import { getBuyerNotification } from '../actions/notification';

import { getUserDetail } from 'actions'
import { getSidebar, apiFailed } from 'actions/seller'
import { NotificationManager } from 'react-notifications';
import AxiosRequest from './axiosRequest'


const createUserWithEmailPasswordRequest = async (email, password) =>
  await auth.createUserWithEmailAndPassword(email, password)
    .then(authUser => authUser)
    .catch(error => error);


const signInUserWithGoogleRequest = async () =>
  await auth.signInWithPopup(googleAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithFacebookRequest = async () =>
  await auth.signInWithPopup(facebookAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithGithubRequest = async () =>
  await auth.signInWithPopup(githubAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithTwitterRequest = async () =>
  await auth.signInWithPopup(twitterAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);



function* createUserWithEmailPassword({ payload }) {
  const { email, password } = payload;
  try {
    const signUpUser = yield call(createUserWithEmailPasswordRequest, email, password);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem('user_id', signUpUser.user.uid);
      yield put(userSignUpSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}



function* signInUserWithGoogle() {
  try {
    const signUpUser = yield call(signInUserWithGoogleRequest);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem('user_id', signUpUser.user.uid);
      yield put(userGoogleSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}


function* signInUserWithFacebook() {
  try {
    const signUpUser = yield call(signInUserWithFacebookRequest);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem('user_id', signUpUser.user.uid);
      yield put(userFacebookSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}


function* signInUserWithGithub() {
  try {
    const signUpUser = yield call(signInUserWithGithubRequest);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem('user_id', signUpUser.user.uid);
      yield put(userGithubSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}


function* signInUserWithTwitter() {
  try {
    const signUpUser = yield call(signInUserWithTwitterRequest);
    if (signUpUser.message) {
      if (signUpUser.message.length > 100) {
        yield put(showAuthMessage('Your request has been canceled.'));
      } else {
        yield put(showAuthMessage(signUpUser.message));
      }
    } else {
      localStorage.setItem('user_id', signUpUser.user.uid);
      yield put(userTwitterSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}



function* signInUserWithEmailPassword({ payload }) {
  const { history, user_type } = payload;
  try {
    let signInUser
    if (user_type === "seller") {
      signInUser = yield call(AxiosRequest.axiosHelperFunc, 'post', 'users/login', '', payload);
    } else {
      signInUser = yield call(AxiosRequest.axiosBuyerHelperFunc, 'post', 'users/signin', '', payload);
    }
    if(signInUser.data.detail&&signInUser.data.detail.user_type=='admin'){
      NotificationManager.error('Invalid credentials');
    }
    if (signInUser.data.error===true) {
      if (signInUser.data.detail && !signInUser.data.detail.isVerified) { 
        NotificationManager.success(signInUser.data.title)
        localStorage.setItem('verifyToken', signInUser.data.token);
        history.push('/verifyToken');
      } else {
       
        yield put(apiFailed(signInUser.data.title));
      }
    } else if (signInUser.data.isDocumentExpired !== undefined && signInUser.data.isDocumentExpired === true && signInUser.data.detail.user_type === 'seller') {
      // yield put(apiFailed(signInUser.data.title));
      if (signInUser.data.detail && !signInUser.data.detail.isVerified) {
        if (signInUser.data.detail.user_type === "seller") {
          localStorage.setItem('token', signInUser.data.token);
        }
        else {
          localStorage.setItem('buyer_token', signInUser.data.token);
        }
        localStorage.setItem('user_type', signInUser.data.detail.user_type);
        localStorage.setItem('isDocumentExpired', signInUser.data.isDocumentExpired);
        yield put(userSignInSuccess(signInUser.data));
        yield put(getSidebar({ history }))
      }
    } else {
      localStorage.setItem('isDocumentExpired', false);
      localStorage.setItem('user_type', signInUser.data.detail.user_type)
      yield put(userSignInSuccess(signInUser.data));
      if (signInUser.data.detail.user_type === 'buyer') {
        localStorage.setItem('buyer_token', signInUser.data.token);
        yield put(getUserDetail({ userType: 'buyer' }))
        yield put(getProductsForYou({history}))
        yield put(getMediWallet({history}))        
        yield put(getBuyerNotification({email: '', password: '', user_type: 'buyer'}))
        yield put(getCartDetails({history}))
        let inventoryData=localStorage.getItem('obj');
        let data = JSON.parse( inventoryData);
        if(data !== null){
        yield put(addToCart({cart_details:data}))
        yield put(getCartDetails({cart_details:data}))
        }
        yield put(userSignInSuccess(signInUser.data));
      }
      if (signInUser.data.detail.user_type === 'seller') {
        localStorage.setItem('token', signInUser.data.token);
        yield put(userSignInSuccess(signInUser.data));
        yield put(getSidebar({ history }))
      }
    }
  } catch (error) {
    // yield put(showAuthMessage(error));
    history.push('/underMaintenance')
  }
}

function* signOut({ payload }) {
  let { history } = payload
  try {
      localStorage.removeItem('token');
      localStorage.removeItem('forgotPassword');
      localStorage.removeItem('verifyToken');
      localStorage.removeItem('user_type');
      localStorage.removeItem('buyer_token');
      localStorage.removeItem('seller_token');
      localStorage.removeItem('asAdmin');
      // localStorage.clear();
      yield put(userSignOutSuccess(signOutUser));
      history.push('/signin')
    
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

export function* createUserAccount() {
  yield takeLatest(SIGNUP_USER, createUserWithEmailPassword);
}

export function* signInWithGoogle() {
  yield takeEvery(SIGNIN_GOOGLE_USER, signInUserWithGoogle);
}

export function* signInWithFacebook() {
  yield takeEvery(SIGNIN_FACEBOOK_USER, signInUserWithFacebook);
}

export function* signInWithTwitter() {
  yield takeEvery(SIGNIN_TWITTER_USER, signInUserWithTwitter);
}

export function* signInWithGithub() {
  yield takeEvery(SIGNIN_GITHUB_USER, signInUserWithGithub);
}

export function* signInUser() {
  yield takeLatest(SIGNIN_USER, signInUserWithEmailPassword);
}

export function* signOutUser() {
  yield takeLatest(SIGNOUT_USER, signOut);
}

export default function* rootSaga() {
  yield all([fork(signInUser),
  fork(createUserAccount),
  fork(signInWithGoogle),
  fork(signInWithFacebook),
  fork(signInWithTwitter),
  fork(signInWithGithub),
  fork(signOutUser)]);
}
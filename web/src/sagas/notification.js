import { all, call, fork, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
    GET_NOTIFICATIONS,
    GET_NOTIFICATIONS_SUCCESS,

    GET_BUYER_NOTIFICATIONS,
    GET_BUYER_NOTIFICATIONS_SUCCESS
} from "constants/ActionTypes";

import {
    getNotification,
    getNotificationSuccess,
    getBuyerNotification,
    getBuyerNotificationSuccess,

} from "actions/notification";

import { showAuthMessage } from "actions/Auth";
import { Redirect, Route, Switch } from 'react-router-dom';
import Axios from './axiosRequest'

function* getNotificationFunction({ payload }) {
  const { history } = payload
  try {
    const getNotificationResponse = yield call(Axios.axiosRequest, 'post', 'users/getNotification', '', payload);

    if (getNotificationResponse.data.error) {
      yield put(showAuthMessage(getNotificationResponse.data.title));

    } else {
      yield put(getNotificationSuccess(getNotificationResponse.data))
    }
  } catch (error) {
    // yield put(showAuthMessage(error));
    console.log('error9833405',error)
    history.push('/underMaintenance')
    // return(<Redirect to={'/underMaintenance'} />)
  }
}
export function* getNotificationDispatcher() {
  yield takeLatest(GET_NOTIFICATIONS, getNotificationFunction);
}

//----------------------------------------------------------------------------------

function* getBuyerNotificationFunction({ payload }) {
  const { history } = payload
  console.log('insaganotification');
  
  try {
    const getBuyerNotificationResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'users/getNotification', '', payload);

    if (getBuyerNotificationResponse.data.error) {
      // yield put(showAuthMessage(getBuyerNotificationResponse.data.title));

    } else {
      yield put(getBuyerNotificationSuccess(getBuyerNotificationResponse.data))
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}
export function* getBuyerNotificationDispatcher() {
  yield takeLatest(GET_BUYER_NOTIFICATIONS, getBuyerNotificationFunction);
}



export default function* rootSaga() {
    yield all([
      fork(getNotificationDispatcher),
      fork(getBuyerNotificationDispatcher)
    ]);
  }
import { all, call, fork, put, takeEvery, takeLatest, takeLeading } from "redux-saga/effects";
import {
  GET_SIDEBAR,
  LIST_PERMISSION_MODULES,
  ADD_GROUP,
  LIST_GROUP,
  DELETE_GROUP,
  ADD_STAFF,
  LIST_STAFF,
  DELETE_STAFF,
  REQUEST_PRODUCT,
  GET_REQUEST_PRODUCT,
  GET_USERDETAILS,
  GET_INVENTORY,
  GET_DASHBOARD_CARD,
  GET_DASHBOARD_TOP_SELLING_PRODUCTS,
  GET_SETTLEMENT,
  GET_DASHBOARD_LEAST_SELLING_PRODUCTS,
  GET_MARK_READ_LIST,
   // ------------- SETTLEMENTS-----------------

   GET_SETTLEMENTS,
   GET_LIST_GROUP_SETTLEMENT,
} from "constants/ActionTypes";

import {
  apiFailed,
  getSidebarSuccess,
  listPermissionModulesSuccess,
  addGroupSuccess,
  deleteGroupSuccess,
  listGroup,
  listGroupSuccess,
  addStaffSuccess,
  deleteStaffSuccess,
  listStaff,
  listStaffSuccess,
  getProductRequest,
  getProductRequestSuccess,
  addRequestProductSuccess,
  getUserDetailSuccess,
  getInventoryListSuccess,
  getDashboardCardSuccess,
  getDashboardTopSellingProductsSuccess,
  getDashboardLeastSellingProductsSuccess,
  getSettlementListSuccess,
  getMarkReadSuccess,


  getSettlementsSuccess,
  getListGroupSettlementSuccess,
} from "actions/seller";

import {getNotification} from 'actions/notification'
import Axios from './axiosRequest'

function* getSideFunction({ payload }) {
  const { history } = payload
  try {
    const getSidebarResponse = yield call(Axios.axiosRequest, 'post', 'users/getSidbarContent', '', payload);
    if (getSidebarResponse.data.error) {
      yield put(apiFailed(getSidebarResponse.data.title));

    } else {

      if (getSidebarResponse.data.detail.length > 0) {
        let path = history.location.pathname.split('/')
        let index = path[2] ? getSidebarResponse.data.detail.findIndex((val) => val.pathname.includes(path[2])) : -1
        console.log('asdasdsgvbrthndezvr',path,index)
        if (index === -1) {
          history.push(`${getSidebarResponse.data.detail[0].pathname}`);
        }

      }
      yield put(getSidebarSuccess(getSidebarResponse.data))
    }
  } catch (error) {
    // yield put(apiFailed(error));
    history.push('/underMaintenance')
  }
}
export function* getSideBarDispatcher() {
  yield takeLeading(GET_SIDEBAR, getSideFunction);
}

function* listPermissionModules({ payload }) {
  const { history } = payload
  try {
    const listPermissionModuleResponse = yield call(Axios.axiosRequest, 'post', 'seller/getPermissionModule', '', payload);
    if (listPermissionModuleResponse.data.error) {
      yield put(apiFailed(listPermissionModuleResponse.data.title));

    } else {
      yield put(listPermissionModulesSuccess(listPermissionModuleResponse.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getPermissionModuleDispatcher() {
  yield takeEvery(LIST_PERMISSION_MODULES, listPermissionModules);
}

function* addGroupFunction({ payload }) {
  const { history } = payload
  try {
    const addGroupResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/addEditGroupModule', '', payload.data);
    if (addGroupResponse.data.error) {
      yield put(apiFailed(addGroupResponse.data.title));

    } else {
      yield put(addGroupSuccess(addGroupResponse.data));
      yield put(listGroup(payload.listGroup))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* addGroupDispatcher() {
  yield takeLatest(ADD_GROUP, addGroupFunction);
}

function* listGroupFunction({ payload }) {
  const { history } = payload
  try {
    const listGroupResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/getGroupModule', '', payload);
    if (listGroupResponse.data.error) {
      yield put(apiFailed(listGroupResponse.data.title));

    } else {
      yield put(listGroupSuccess(listGroupResponse.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* listGroupDispatcher() {
  yield takeEvery(LIST_GROUP, listGroupFunction);
}

function* deleteGroupFunction({ payload }) {
  const { history } = payload
  try {
    const deleteGroupResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/deleteGroupModule', '', payload);
    if (deleteGroupResponse.data.error) {
      yield put(apiFailed(deleteGroupResponse.data.title));

    } else {
      yield put(deleteGroupSuccess(deleteGroupResponse.data))
      yield put(listGroup(payload.listGroup))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* deleteGroupDispatcher() {
  yield takeLatest(DELETE_GROUP, deleteGroupFunction);
}

function* addStaffFunction({ payload }) {
  const { history } = payload
  try {
    const addStaffResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/addEditStaffMember', '', payload.data);
    if (addStaffResponse.data.error) {
      yield put(apiFailed(addStaffResponse.data.title));

    } else {
      yield put(addStaffSuccess(addStaffResponse.data));
      yield put(listStaff(payload.listStaff))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* addStaffDispatcher() {
  yield takeLatest(ADD_STAFF, addStaffFunction);
}

function* listStaffFunction({ payload }) {
  const { history } = payload
  try {
    const listStaffResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/getStaffList', '', payload);
    if (listStaffResponse.data.error) {
      yield put(apiFailed(listStaffResponse.data.title));

    } else {
      yield put(listStaffSuccess(listStaffResponse.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* listStaffDispatcher() {
  yield takeEvery(LIST_STAFF, listStaffFunction);
}

function* deleteStaffFunction({ payload }) {
  const { history } = payload
  try {
    const deleteStaffResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/deleteStaff', '', payload);
    if (deleteStaffResponse.data.error) {
      yield put(apiFailed(deleteStaffResponse.data.title));

    } else {
      yield put(deleteStaffSuccess(deleteStaffResponse.data))
      yield put(listStaff(payload.listStaff))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* deleteStaffDispatcher() {
  yield takeLatest(DELETE_STAFF, deleteStaffFunction);
}


function* addProductRequestFunction({ payload }) {
  const { history } = payload
  try {
    const addProductRequestResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/addProductRequest', '', payload.data);
    if (addProductRequestResponse.data.error) {
      yield put(apiFailed(addProductRequestResponse.data.title));

    } else {
      yield put(addRequestProductSuccess(addProductRequestResponse.data))
      yield put(getProductRequest(payload.listProduct))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* addProductRequestDispatcher() {
  yield takeLatest(REQUEST_PRODUCT, addProductRequestFunction);
}

function* listRequestedProductsFunction({ payload }) {
  const { history } = payload
  try {
    const listRequestedProductsResponse = yield call(Axios.axiosHelperFunc, 'post', 'seller/getRequestedProductList', '', payload);
    if (listRequestedProductsResponse.data.error) {
      yield put(apiFailed(listRequestedProductsResponse.data.title));

    } else {
      yield put(getProductRequestSuccess(listRequestedProductsResponse.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* listRequestedProductsDispatcher() {
  yield takeEvery(GET_REQUEST_PRODUCT, listRequestedProductsFunction);
}


function* getUserDetailFunction({ payload }) {
  const { history } = payload
  try {
    const getUserDetailRequest = payload.userType === "buyer" ? yield call(Axios.axiosBuyerHelperFunc, 'get', 'users/getuserDetails', '', payload) : yield call(Axios.axiosHelperFunc, 'get', 'users/getuserDetails', '', payload);

    if (getUserDetailRequest.data.error) {
      yield put(apiFailed(getUserDetailRequest.data.title));

    } else {
      yield put(getUserDetailSuccess(getUserDetailRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getUserdetailDispatcher() {
  yield takeEvery(GET_USERDETAILS, getUserDetailFunction);
}

function* getInventoryFunction({ payload }) {
  const { history } = payload
  try {
    const getInventoryRequest = yield call(Axios.axiosHelperFunc, 'post', 'inventory/listingInventory', '', payload.data);
    if (getInventoryRequest.data.error) {
      yield put(apiFailed(getInventoryRequest.data.title));

    } else {
      yield put(getInventoryListSuccess(getInventoryRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getInventoryDispatcher() {
  yield takeLatest(GET_INVENTORY, getInventoryFunction);
}

function* getDashboardCardFunction({ payload }) {
  const { history } = payload
  try {
    const getDashboardCardRequest = yield call(Axios.axiosHelperFunc, 'get', 'seller/getDashBoardCardDetails', '', '');
    if (getDashboardCardRequest.data.error) {
      yield put(apiFailed(getDashboardCardRequest.data.title));

    } else {
      yield put(getDashboardCardSuccess(getDashboardCardRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getDashboardCardDispatcher() {
  yield takeEvery(GET_DASHBOARD_CARD, getDashboardCardFunction);
}

function* getDashboardTopSellingProductsFunction({ payload }) {
  const { history } = payload
  try {
    const getDashboardTopSellingProductsRequest = yield call(Axios.axiosHelperFunc, 'post', 'seller/getDashboardTopSellingProducts', '', payload.data);
    if (getDashboardTopSellingProductsRequest.data.error) {
      yield put(apiFailed(getDashboardTopSellingProductsRequest.data.title));

    } else {
      yield put(getDashboardTopSellingProductsSuccess(getDashboardTopSellingProductsRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getDashboardTopSellingProductsDispatcher() {
  yield takeEvery(GET_DASHBOARD_TOP_SELLING_PRODUCTS, getDashboardTopSellingProductsFunction);
}

function* getSettlementListFunction({ payload }) {
  const { history } = payload
  try {
    const getSettlementListRequest = yield call(Axios.axiosHelperFunc, 'post', 'seller/getSettlements', '', payload.data);
    if (getSettlementListRequest.data.error) {
      yield put(apiFailed(getSettlementListRequest.data.title));

    } else {
      yield put(getSettlementListSuccess(getSettlementListRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getSettlementListDispatcher() {
  yield takeEvery(GET_SETTLEMENT, getSettlementListFunction);
}

function* getDashboardLeastSellingProductsFunction({ payload }) {
  const { history } = payload
  try {
    const getDashboardLeastSellingProductsRequest = yield call(Axios.axiosHelperFunc, 'post', 'seller/getDashboardLeastSellingProducts', '', payload.data1);
    if (getDashboardLeastSellingProductsRequest.data.error) {
      yield put(apiFailed(getDashboardLeastSellingProductsRequest.data.title));

    } else {
      yield put(getDashboardLeastSellingProductsSuccess(getDashboardLeastSellingProductsRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getDashboardLeastSellingProductsDispatcher() {
  yield takeEvery(GET_DASHBOARD_LEAST_SELLING_PRODUCTS, getDashboardLeastSellingProductsFunction);
}

// ----------------------------------------------------------------------------

function* getMarkReadFunction({ payload }) {
  const { history } = payload
  try {
    const getMarkReadRequest = yield call(Axios.axiosHelperFunc, 'post', 'users/markRead', '', payload.data);
    if (getMarkReadRequest.data.error) {
      yield put(apiFailed(getMarkReadRequest.data));

    } else {
      yield put(getMarkReadSuccess(getMarkReadRequest.data))
      yield put(getNotification(getMarkReadRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}
export function* getMarkReadDispatcher() {
  yield takeLatest(GET_MARK_READ_LIST, getMarkReadFunction);
}

// ----------------------------------------------------------------------------------

function* getSettlementsFunction({ payload }) {
  const { history } = payload
  try {
      const getSettlementsResponse = yield call(Axios.axiosHelperFunc, 'post', 'settlements/list', '', payload.data);

      
      if (getSettlementsResponse.data.error) {
          yield put(apiFailed(getSettlementsResponse.data.title));
      } else {
          yield put(getSettlementsSuccess(getSettlementsResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')  
    // yield put(apiFailed(error));
  }
}

export function* getSettlementsDispacher() {
  yield takeLatest(GET_SETTLEMENTS, getSettlementsFunction);
}
// -----------------------------------------------------
function* getListGroupSettlementFunction({ payload }) {
  const { history } = payload;
  try {
    const getListGroupSettlementResponse = yield call(Axios.axiosHelperFunc, 'post', 'groupSettlements/listGroupSettlement', '', payload.data);

    if (getListGroupSettlementResponse.data.error) {
      yield put(apiFailed(getListGroupSettlementResponse.data.title));
    } else {
      yield put(getListGroupSettlementSuccess(getListGroupSettlementResponse.data))

    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}

export function* getListGroupSettlementDispatcher() {
  yield takeLatest(GET_LIST_GROUP_SETTLEMENT, getListGroupSettlementFunction);
}
export default function* rootSaga() {
  yield all([
    fork(getSideBarDispatcher),
    fork(getPermissionModuleDispatcher),
    fork(addGroupDispatcher),
    fork(listGroupDispatcher),
    fork(deleteGroupDispatcher),
    fork(addStaffDispatcher),
    fork(listStaffDispatcher),
    fork(deleteStaffDispatcher),
    fork(addProductRequestDispatcher),
    fork(listRequestedProductsDispatcher),
    fork(getUserdetailDispatcher),
    fork(getInventoryDispatcher),
    fork(getDashboardCardDispatcher),
    fork(getDashboardTopSellingProductsDispatcher),
    fork(getSettlementListDispatcher),
    fork(getDashboardLeastSellingProductsDispatcher),
    fork(getMarkReadDispatcher),
    fork(getSettlementsDispacher),
    fork(getListGroupSettlementDispatcher),
  ]);
}
import { all, call, fork, put, takeEvery, takeLatest, takeLeading } from "redux-saga/effects";
import React from 'react';

import {
  API_FAILED,

  LOGOUT_USER,

  GET_FEATURED_PRODUCT_LIST,

  GET_PRODUCT_DETAILS,

  GET_TRANSACTION_LIST,

  GET_MEDIWALLET,
  
  GET_PRODUCTS_FOR_YOU,

  GET_ORDER_LIST,

  GET_REVIEW_LIST,

  GET_ORDER_HISTORY_LIST,

  ADD_TO_CART,
  
  GET_CART_DETAILS,

  REMOVE_FROM_CART,

  GET_VISITED_CATEGORY,

  GET_PRODUCTS_BY_COMPANY,

  BULK_REQUEST,

  GET_SEARCH_PRODUCT,


  GET_PRODUCTS_BY_SELLER,

  GET_CATEGORIES,
  GET_AUTO_FILL,

  GET_FILTER,

  EMPTY_CART,

  GET_UPDATE_ORDER,

  GET_PROMOTIONS_LISTING,

  GET_MARK_READ_LIST_FOR_BUYER,

  GET_PRODUCT_REQUEST_LIST,
  
  GET_SHORTBOOK,

  GET_SHORT_PRODUCTS

} from "constants/ActionTypes";



import {

  getFeaturedProductListSuccess,

  getProductDetailsSuccess,

  getTransactionListSuccess,

  getMediWalletSuccess,

  getProductsForYouSuccess,

  getOrderList,

  getOrderListSuccess,

  getReviewListSuccess,

  getOrderHistoryList,
  getOrderHistoryListSuccess,

  addToCartSuccess,

  getCartDetails,
  getCartDetailsSuccess,

  removeFromCartSuccess,

  getVisitedCategorySuccess,
  getVisitedCategory,

  getProductsByCompanySuccess,

  bulkRequestSuccess,

  getSearchProductSuccess,

  getProductsBySellerSuccess,

  getCategoriesSuccess,

  getAutoFillSuccess,

  getFilterSuccess,

  emptyCartSuccess,

  getUpdateOrderSuccess,

  getPromotionsListingSuccess,

  getMarkReadForBuyerSuccess,

  logoutUser,

  getProductRequestListSuccess,

  getShortbookSuccess,

  getShortProductsSuccess

} from "actions/buyer";
import {apiFailed,getUserDetailSuccess,
  removeUser, getUserDetail} from 'actions/seller'
import{removeAuth,userSignInSuccess} from 'actions/Auth'
import {getBuyerNotification} from 'actions/notification'
import Axios from './axiosRequest'
import { Redirect, Route, Switch } from 'react-router-dom';


// const logoutUser = (data,history)=>{

//   if((data.isDenied==true)||(data.isBlocked==true)){
    
//     history.push('/buyers/home')
//     removeAuth();
//     removeUser();
    
//   }
// }

function* getFeaturedProductListFunction({ payload }) {
  const {history} = payload
  try {

    const getFeaturedProductListRequest = yield call(Axios.axiosBuyerHelperFunc, 'post', 'productBuyer/v1/featuredProductList', '', payload.data);


    if (getFeaturedProductListRequest.data.error) {
      yield put(apiFailed(getFeaturedProductListRequest.data.title));
      yield put(logoutUser(getFeaturedProductListRequest.data,history))
    } else {
      yield put(getFeaturedProductListSuccess(getFeaturedProductListRequest.data))
    }
  } catch (error) {
    // yield put(apiFailed(error));
    // throw error
    history.push('/underMaintenance')
  }
}

export function* getFeaturedProductList() {
  yield takeLatest(GET_FEATURED_PRODUCT_LIST, getFeaturedProductListFunction);
}

//--------------------------------------------------------------------------------------------------------------------//

function* getProductDetailsFunction({ payload }) {
  const { history } = payload
  try {

    const getProductDetailsRequest = yield call(Axios.axiosBuyerHelperFunc, 'post', 'productBuyer/v1/productDetails', '', payload);


    if (getProductDetailsRequest.data.error) {
      yield put(apiFailed(getProductDetailsRequest.data.title));
      // yield put(logoutUser,getProductDetailsRequest.data,history)
    } else {
      yield put(getProductDetailsSuccess(getProductDetailsRequest.data))
      const user = localStorage.getItem("buyer_token")
      if(user !== null){
        yield put(getVisitedCategory({visited_category: getProductDetailsRequest.data.productData[0].Product_Category._id}))
      }
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}


export function* getProductDetails() {
  yield takeLatest(GET_PRODUCT_DETAILS, getProductDetailsFunction);
}

//--------------------------------------------------------------------------------------------------------------------//


function* getTransactionListFunction({ payload }) {
  const { history } = payload
  try {
      const getTransactionListResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'transactions/listingTransactions', '', payload.data);

      
      if (getTransactionListResponse.data.error) {
          yield put(apiFailed(getTransactionListResponse.data.title));
          yield put(logoutUser(getTransactionListResponse.data,history))
      } else {
          yield put(getTransactionListSuccess(getTransactionListResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getTransactionList() {
  yield takeLatest(GET_TRANSACTION_LIST, getTransactionListFunction);
}

//--------------------------------------------------------------------------------------------------------------------//


function* getMediWalletFunction({ payload }) {
  const {history} = payload
  try {
      const getMediWalletResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'transactions/listingWalletTransaction', '', payload.data);
     
     
      if (getMediWalletResponse.data.error) {
          yield put(apiFailed(getMediWalletResponse.data.title));
          yield put(logoutUser(getMediWalletResponse.data,history))
      } else {
          yield put(getMediWalletSuccess(getMediWalletResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getMediWallet() {
  yield takeLatest(GET_MEDIWALLET, getMediWalletFunction);
}

//--------------------------------------------------------------------------------------------------------------------//


function* getProductsForYouFunction({ payload }) {
  const{history} = payload
  try {

    const getProductsForYouRequest = yield call(Axios.axiosBuyerHelperFunc, 'post', 'productBuyer/v1/productForYou', '', payload);


    if (getProductsForYouRequest.data.error) {
      yield put(apiFailed(getProductsForYouRequest.data.title));
      yield put(logoutUser(getProductsForYouRequest.data,history))

    } else {
      yield put(getProductsForYouSuccess(getProductsForYouRequest.data))
    }
  } catch (error) {
    history.push('/underMaintenance')
    // yield put(apiFailed(error));
  }
}

export function* getProductsForYou() {
  yield takeLatest(GET_PRODUCTS_FOR_YOU, getProductsForYouFunction);
}


//--------------------------------------------------------------------------------------------------------------------//


function* getOrderListFunction({ payload }) {
  const { history } = payload
  try {
      const getOrderListResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'order/orderListingBuyers', '', payload.data);
      

      if (getOrderListResponse.data.error) {
          yield put(apiFailed(getOrderListResponse.data.title));
          yield put(logoutUser(getOrderListResponse.data,history))
      } else {
          yield put(getOrderListSuccess(getOrderListResponse.data))
         
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getOrderListDispatcher() {
  yield takeLatest(GET_ORDER_LIST, getOrderListFunction);
}


//--------------------------------------------------------------------------------------------------------------------//


function* getOrderHistoryListFunction({ payload }) {
  const { history } = payload
  try {
      const getOrderHistoryListResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'order/orderHistoryBuyers', '', payload.data);


      if (getOrderHistoryListResponse.data.error) {
          yield put(apiFailed(getOrderHistoryListResponse.data.title));
          yield put(logoutUser(getOrderHistoryListResponse.data,history))
      } else {
          yield put(getOrderHistoryListSuccess(getOrderHistoryListResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getOrderHistoryListDispatcher() {
  yield takeLatest(GET_ORDER_HISTORY_LIST, getOrderHistoryListFunction);
}

//--------------------------------------------------------------------------------------------------------------------//


function* getReviewListFunction({ payload }) {
  const { history } = payload
  try {
      const getReviewListResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'users/getRating', '', payload.data);
      

      if (getReviewListResponse.data.error) {
          yield put(apiFailed(
            getReviewListResponse.data.title
            ));
            yield put(logoutUser(getReviewListResponse.data,history))
      } else {
          yield put(getReviewListSuccess(getReviewListResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getReviewList() {
  yield takeLatest(GET_REVIEW_LIST, getReviewListFunction);
}


//----------------------------------------------------------------------------------------------------------------- //



function* addToCartFunction({ payload }) {
  const { history } = payload
  try {
      const addToCartResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'cart/addtoCart', '', payload.data);
      

      if (addToCartResponse.data.error) {
          yield put(apiFailed(addToCartResponse.data.title));
          yield put(logoutUser(addToCartResponse.data,history))
      } else {
          // yield put(addToCartSuccess(addToCartResponse.data)) 
          yield put(getCartDetails({}))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* addToCart() {
  yield takeLatest(ADD_TO_CART, addToCartFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getCartDetailsFunction({ payload }) {
  const{history} = payload
  try {
      const getCartDetailsResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'cart/cartDetails', '', payload);
      

      if (getCartDetailsResponse.data.error) {
          yield put(apiFailed(getCartDetailsResponse.data.title));
          yield put(logoutUser(getCartDetailsResponse.data,history))
      } else {
          yield put(getCartDetailsSuccess(getCartDetailsResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getCartDetailsDispatcher() {
  yield takeLatest(GET_CART_DETAILS, getCartDetailsFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* removeFromCartFunction({ payload }) {
  const {history} = payload
  try {
      const removeFromCartResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'cart/removeFromCart', '', payload);
      

      if (removeFromCartResponse.data.error) {
          // yield put(apiFailed(removeFromCartResponse.data.title));
        
      } else {
          yield put(removeFromCartSuccess(removeFromCartResponse.data))
          yield put(getCartDetails({}))         

      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* removeFromCart() {
  yield takeLatest(REMOVE_FROM_CART, removeFromCartFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getVisitedCategoryFunction({ payload }) {
  const { history } = payload
  try {
      const getVisitedCategoryResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'buyer/visitedCategory', '', payload);
      

      if (getVisitedCategoryResponse.data.error) {
          yield put(apiFailed(getVisitedCategoryResponse.data.title));
          yield put(logoutUser(getVisitedCategoryResponse.data,history))
      } else {
          yield put(getVisitedCategorySuccess(getVisitedCategoryResponse.data))
          
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getVisitedCategoryDispatcher() {
  yield takeLatest(GET_VISITED_CATEGORY, getVisitedCategoryFunction);
}


//----------------------------------------------------------------------------------------------------------------- //


function* getProductsByCompanyFunction({ payload }) {
  const { history } = payload
  try {
      const getProductsByCompanyResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'product/by_company', '', payload);
      if (getProductsByCompanyResponse.data.error) {
          yield put(apiFailed(getProductsByCompanyResponse.data.title));
          yield put(logoutUser(getProductsByCompanyResponse.data,history))
      } else {
          yield put(getProductsByCompanySuccess(getProductsByCompanyResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}
export function* getProductsByCompany() {
  yield takeLatest(GET_PRODUCTS_BY_COMPANY, getProductsByCompanyFunction);
}


//----------------------------------------------------------------------------------------------------------------- //

function* bulkRequestFunction({ payload }) {
  const { history } = payload
  try {
      const bulkRequestResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'buyer/bulkRequest', '', payload.data);
      

      if (bulkRequestResponse.data.error) {
          yield put(apiFailed(bulkRequestResponse.data.title));
          // yield put(logoutUser,bulkRequestResponse.data,history)

      } else {
          yield put(bulkRequestSuccess(bulkRequestResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* bulkRequestResponseDispatcher() {
  yield takeLatest(BULK_REQUEST, bulkRequestFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getSearchProductFunction({ payload }) {
  const { history } = payload
  try {
      const getSearchProductResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'inventory/v1/search', '', payload.data);
      

      if (getSearchProductResponse.data.error) {
          yield put(apiFailed(getSearchProductResponse.data.title));
          // yield put(logoutUser,getSearchProductResponse.data,history)
      } else {
          yield put(getSearchProductSuccess(getSearchProductResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getSearchProductDispacher() {
  yield takeLatest(GET_SEARCH_PRODUCT, getSearchProductFunction);
}

//----------------------------------------------------------------------------------------------------------------- //
function* getProductsBySellerFunction({ payload }) {
  const { history } = payload
  try {
      const getProductsBySellerResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'inventory/v1/search', '', payload);
      if (getProductsBySellerResponse.data.error) {
          yield put(apiFailed(getProductsBySellerResponse.data.title));
          yield put(logoutUser(getProductsBySellerResponse.data,history))
      } else {
          yield put(getProductsBySellerSuccess(getProductsBySellerResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}
export function* getProductsBySeller() {
  yield takeLatest(GET_PRODUCTS_BY_SELLER, getProductsBySellerFunction);
}
//----------------------------------------------------------------------------------------------------------------- //
function* getCategoriesFunction({ payload }) {
  const { history } = payload
  try {
      const getCategoriesResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'inventory/get_categories', '', payload);
      if (getCategoriesResponse.data.error) {
          yield put(apiFailed(getCategoriesResponse.data.title));
          yield put(logoutUser(getCategoriesResponse.data,history))
      } else {
          yield put(getCategoriesSuccess(getCategoriesResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}
export function* getCategories() {
  yield takeLatest(GET_CATEGORIES, getCategoriesFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getAutoFillFunction({ payload }) {
  const { history,key } = payload
  try {
      const getAutoFillResponse = yield call(Axios.axiosBuyerHelperFunc, 'get', `product/autofill?key=${key}`, '', payload);
      if (getAutoFillResponse.data.error) {
          // yield put(apiFailed(getAutoFillResponse.data.title));
          yield put(logoutUser(getAutoFillResponse.data,history))
      } else {
          yield put(getAutoFillSuccess(getAutoFillResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}
export function* getAutoFill() {
  yield takeLatest(GET_AUTO_FILL, getAutoFillFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getFilterFunction({ payload }) {
  const { history,key } = payload
  try {
      const getFilterResponse = yield call(Axios.axiosBuyerHelperFunc, 'get', 'inventory/get_filters', '', payload);
      if (getFilterResponse.data.error) {
          yield put(apiFailed(getFilterResponse.data.title));
          yield put(logoutUser(getFilterResponse.data,history))
      } else {
          yield put(getFilterSuccess(getFilterResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}
export function* getFilter() {
  yield takeLatest(GET_FILTER, getFilterFunction);
}

/***************Empty Cart**************** *****************************************************/
function* emptyCartFunction({ payload }) {
  const {history} = payload
    
    try {
        const emptyCartResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'cart/removeAll', '', payload);
        if (emptyCartResponse.data.error) {
            yield put(apiFailed(emptyCartResponse.data.title));
            // yield put(logoutUser,emptyCartResponse.data,history)
        } else {
            yield put(emptyCartSuccess(emptyCartResponse.data))
            yield put(getCartDetails({}))
        }
    } catch (error) {
      history.push('/underMaintenance')
        // yield put(apiFailed(error));
    }
  }
  export function* emptyCartDispatcher() {
    yield takeLatest(EMPTY_CART, emptyCartFunction);
  }
//----------------------------------------------------------------------------------------------------------------- //

function* getUpdateOrderFunction({ payload }) {
  const { history,key } = payload
  try {
      const getUpdateOrderResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'order/updateOrder', '', payload.data);
      if (getUpdateOrderResponse.data.error) {
          yield put(apiFailed(getUpdateOrderResponse.data.title));
          yield put(logoutUser(getUpdateOrderResponse.data,history))
      } else {
          yield put(getUpdateOrderSuccess(getUpdateOrderResponse.data.title))
          yield put(getOrderList({history,data:payload.data}))
          yield put(getOrderHistoryList({history,data:payload.data}))
          yield put(getUserDetail({userType:"buyer"}))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}
export function* getUpdateOrder() {
  yield takeLatest(GET_UPDATE_ORDER, getUpdateOrderFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getPromotionsListingFunction({ payload }) {
  const { history } = payload
  try {
      const getPromotionsListingResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'promotions/listing', '', payload);
      if (getPromotionsListingResponse.data.error) {
          yield put(apiFailed(getPromotionsListingResponse.data.title));
          yield put(logoutUser(getPromotionsListingResponse.data,history))
      } 
      
      else {
          yield put(getPromotionsListingSuccess(getPromotionsListingResponse.data))
      }
  } 
  catch (error) {
      // yield put(apiFailed(error));
  }
}
export function* getPromotionsListing() {
  yield takeLatest(GET_PROMOTIONS_LISTING, getPromotionsListingFunction);
}

//----------------------------------------------------------------------------------------------------------------- //

function* getMarkReadForBuyerFunction({ payload }) {
  const { history } = payload
  try {
      const getMarkReadForBuyerResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'users/markRead', '', payload);
      if (getMarkReadForBuyerResponse.data.error) {
          yield put(apiFailed(getMarkReadForBuyerResponse.data));
          yield put(logoutUser(getMarkReadForBuyerResponse.data,history))
      } 
      else {
          yield put(getMarkReadForBuyerSuccess(getMarkReadForBuyerResponse.data))
          yield put(getBuyerNotification(getMarkReadForBuyerResponse.data))
      }
  } 
  catch (error) {
      // yield put(apiFailed(error));
  }
}
export function* getMarkReadForBuyerDispatcher() {
  yield takeLatest(GET_MARK_READ_LIST_FOR_BUYER, getMarkReadForBuyerFunction);
}

function* logoutFunction({payload}){
  const{data,history} = payload
  try {
  if((data.isDenied==true)||(data.isBlocked==true)){
    
    history.push('/')
    let detail = ''
    yield put(userSignInSuccess(detail))
    yield put(getUserDetailSuccess(detail))
    localStorage.removeItem('buyer_token');
    localStorage.removeItem('isDocumentExpired');
    localStorage.removeItem('user_type');
  }
    
  } catch (error) {
    history.push('/underMaintenance')
  }
  
  }

export function* logoutDispatcher(){
  yield takeLatest(LOGOUT_USER,logoutFunction)
}


//--------------------------------------------------------------------------------------------------------------------//


function* getProductRequestListFunction({ payload }) {
  const { history } = payload
  try {
      const getProductRequestListResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'buyerRequest/listBuyerRequest', '', payload.data);


      if (getProductRequestListResponse.data.error) {
          yield put(apiFailed(getProductRequestListResponse.data.title));
          yield put(logoutUser(getProductRequestListResponse.data,history))
      } else {
          yield put(getProductRequestListSuccess(getProductRequestListResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getProductRequestListDispatcher() {
  yield takeLatest(GET_PRODUCT_REQUEST_LIST, getProductRequestListFunction);
}


//--------------------------------------------------------------------------------------------------------------------//


function* getShortbookListFunction({ payload }) {
  const { history } = payload
  try {
      const getShortbookListResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'shortBook/listShortBook', '', payload.data);


      if (getShortbookListResponse.data.error) {
          yield put(apiFailed(getShortbookListResponse.data.title));
          yield put(logoutUser(getShortbookListResponse.data,history))
      } else {
          yield put(getShortbookSuccess(getShortbookListResponse.data))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getShortbookListDispatcher() {
  yield takeLatest(GET_SHORTBOOK, getShortbookListFunction);
}


//--------------------------------------------------------------------------------------------------------------------//


function* getShortProductsFunction({ payload }) {
  const { history } = payload
  try {
      const getShortProductsResponse = yield call(Axios.axiosBuyerHelperFunc, 'post', 'shortBook/getShortProducts', '', payload.data);


      if (getShortProductsResponse.data.error) {
          yield put(apiFailed(getShortProductsResponse.data.title));
          yield put(logoutUser(getShortProductsResponse.data,history))
      } else {
          yield put(getShortProductsSuccess(getShortProductsResponse.data.products))
      }
  } catch (error) {
    history.push('/underMaintenance')
      // yield put(apiFailed(error));
  }
}

export function* getShortProductsDispatcher() {
  yield takeLatest(GET_SHORT_PRODUCTS, getShortProductsFunction);
}

export default function* rootSaga() {
  yield all([
    fork(getFeaturedProductList),
    fork(getProductDetails),
    fork(getTransactionList),
    fork(getMediWallet),
    fork(getProductsForYou),
    fork(getOrderListDispatcher),
    fork(getOrderHistoryListDispatcher),
    fork(getReviewList),
    fork(addToCart),
    fork(getCartDetailsDispatcher),
    fork(removeFromCart),
    fork(getVisitedCategoryDispatcher),
    fork(getProductsByCompany),
    fork(bulkRequestResponseDispatcher),
    fork(getSearchProductDispacher),
    fork(getProductsBySeller),
    fork(getCategories),
    fork(getAutoFill),
    fork(getFilter),
    fork(emptyCartDispatcher),
    fork(getUpdateOrder),
    fork(getPromotionsListing),
    fork(getMarkReadForBuyerDispatcher),
    fork(logoutDispatcher),
    fork(getProductRequestListDispatcher),
    fork(getShortbookListDispatcher),
    fork(getShortProductsDispatcher)
  ]);
}

import { all, call, fork, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
    GET_ORDER,
    GET_ORDER_SUCCESS,
    GET_ORDER_DETAILS,
    GET_ORDER_DETAILS_SUCCESS,
    UPDATE_ORDER,
    UPDATE_ORDER_SUCCESS,
    GET_ORDER_HISTORY,
    GET_ORDER_HISTORY_SUCCESS,
    GET_DASHBOARD_SALES_STATS,
    GET_DASHBOARD_SALES_STATS_SUCCESS,
} from "constants/ActionTypes";

import {
    getOrder,
    getOrderSuccess,
    getOrderDetails,
    getOrderDetailsSuccess,
    updateOrder,
    updateOrderSuccess,
    getOrderHistory,
    getOrderHistorySuccess,
    getDashboardSalesStatsSuccess
} from "actions/order";

import { showAuthMessage } from "actions/Auth";

import Axios from './axiosRequest'

function* getOrderFunction({ payload }) {
    const { history } = payload
    try {
        const getOrderResponse = yield call(Axios.axiosHelperFunc, 'post', 'order/allOrderListing', '', payload.data);
        if (getOrderResponse.data.error) {
            yield put(showAuthMessage(getOrderResponse.data.title));

        } else {
            yield put(getOrderSuccess(getOrderResponse.data))
        }
    } catch (error) {
        history.push('/underMaintenance')
        // yield put(showAuthMessage(error));
    }
}
export function* getOrderDispatcher() {
    yield takeLatest(GET_ORDER, getOrderFunction);
}

function* getOrderDetailsFunction({ payload }) {
    const { history } = payload

    try {
        const getOrderDetailsResponse = yield call(Axios.axiosHelperFunc, 'post', 'order/getOrderDetails', '', payload.data);
        if (getOrderDetailsResponse.data.error) {
            yield put(showAuthMessage(getOrderDetailsResponse.data.title));

        } else {
            yield put(getOrderDetailsSuccess(getOrderDetailsResponse.data))
        }
    } catch (error) {
        history.push('/underMaintenance')
        // yield put(showAuthMessage(error));
    }
}
export function* getOrderDetailsDispatcher() {
    yield takeLatest(GET_ORDER_DETAILS, getOrderDetailsFunction);
}

function* updateOrderFunction({ payload }) {
    const { history } = payload

    try {
        const updateOrderResponse = yield call(Axios.axiosHelperFunc, 'post', 'order/updateOrder', '', payload.data);
        if (updateOrderResponse.data.error) {
            yield put(showAuthMessage(updateOrderResponse.data.title));
            yield put(getOrderDetails({ history: history, data: { orderId: payload.data.orderId } }))
        } else {
            yield put(updateOrderSuccess(updateOrderResponse.data))
            yield put(getOrderDetails({ history: history, data: { orderId: payload.data.orderId } }))
        }
    } catch (error) {
        history.push('/underMaintenance')
        // yield put(showAuthMessage(error));
    }
}
export function* updateOrderDispatcher() {
    yield takeLatest(UPDATE_ORDER, updateOrderFunction);
}

function* getOrderHistoryFunction({ payload }) {
    const { history } = payload

    try {
        const getOrderHistoryResponse = yield call(Axios.axiosHelperFunc, 'post', 'order/allOrderListing', '', payload.data);
        if (getOrderHistoryResponse.data.error) {
            yield put(showAuthMessage(getOrderHistoryResponse.data.title));

        } else {
            yield put(getOrderHistorySuccess(getOrderHistoryResponse.data));
        }
    } catch (error) {
        history.push('/underMaintenance')
        // yield put(showAuthMessage(error));
    }
}
export function* getOrderHistoryDispatcher() {
    yield takeLatest(GET_ORDER_HISTORY, getOrderHistoryFunction);
}

function* getDashboardSalesStatsFunction({ payload }) {
    const { history } = payload

    try {
        const getDashboardSalesStatsResponse = yield call(Axios.axiosHelperFunc, 'post', 'order/dashboardSalesStats', '', payload.data);
        if (getDashboardSalesStatsResponse.data.error) {
            yield put(showAuthMessage(getDashboardSalesStatsResponse.data.title));

        } else {
            yield put(getDashboardSalesStatsSuccess(getDashboardSalesStatsResponse.data));
        }
    } catch (error) {
        history.push('/underMaintenance')
        // yield put(showAuthMessage(error));
    }
}
export function* getDashboardSalesStatsDispatcher() {
    yield takeLatest(GET_DASHBOARD_SALES_STATS, getDashboardSalesStatsFunction);
}

export default function* rootSaga() {
    yield all([
        fork(getOrderDispatcher),
        fork(getOrderDetailsDispatcher),
        fork(updateOrderDispatcher),
        fork(getOrderHistoryDispatcher),
        fork(getDashboardSalesStatsDispatcher),
    ]);
}
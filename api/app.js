var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var dotenv = require('dotenv');
var multer = require('multer');
const request = require("request");
var cors = require('cors');
const moment = require('moment');
dotenv.config();
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');
var bankAccountRouter = require('./routes/bankAccount');
var buyerRouter = require('./routes/buyer');
var cartRouter = require('./routes/cart');
var companyRouter = require('./routes/company');
var indexRouter = require('./routes/index');
var inventoryRouter = require('./routes/inventory');
var medicineBankRouter = require('./routes/medicineBank');
var orderRouter = require('./routes/order');
var productCategoryRouter = require('./routes/productCategory');
var productRouter = require('./routes/product');
var promotionsRouter = require('./routes/promotions');
var sellerRouter = require('./routes/seller');
var transactionsRouter = require('./routes/transactions');
var settlementRouter = require('./routes/settlements');
var systemConfig = require('./routes/systemConfig');
var systemActivity = require('./routes/systemActivity');
var productAuto = require('./routes/productAuto');
var settlement = require('./controllers/settlements')
const helper = require('./lib/helper');
const notification = require('./models/notification');
var async = require('async')
var orders = require('./models/order')
var CronJob = require('cron').CronJob;
//for buyer
var productBuyerRouter = require('./routes/productBuyer');
var groupSettlementRouter = require('./routes/groupSettlement');
var shippingManifestRouter = require('./routes/shippingManifest');
var reportRouter = require('./routes/report');
var mediCategoryRouter = require('./routes/medicine_type');
var buyerRequestRouter = require('./routes/buyerRequest');
var {getActiveInventory} = require('./controllers/activeInventory');
var shippingPartnerRouter = require('./routes/shippingPatner');
var shortBookRouter = require('./routes/shortBook');
var ticketRouter = require('./routes/ticket');
var app = express();

mongoose.set('debug', true);
mongoose.Promise = global.Promise;
console.log('process.env.db', process.env.db)
mongoose.connect(process.env.db, { useNewUrlParser: true, useCreateIndex: true });
app.use(cors())
// mongoose.connect('https://localhost:27017', { useNewUrlParser: true, useCreateIndex: true });

// app.use(function (req, res, next) {
//   var allowedOrigins = ['http://localhost:3000', 'http://localhost:3001', 'https://medideals-admin.infiny.dev', 'https://medideals-web.infiny.dev', 'http://admin.medideals.in', 'https://admin.medideals.in', 'https://medideals.in', 'http://medideals.in', 'https://www.medideals.in', 'https://medideals.infiny.dev', 'http://medideals.infiny.dev'];
//   var origin = req.headers.origin ? req.headers.origin : req.headers;
//   if (allowedOrigins.indexOf(origin) > -1) {
//     res.setHeader('Access-Control-Allow-Origin', origin);
//   }

//   // res.header("Access-Control-Allow-Origin", "https://medideals-web.infiny.dev");

//   res.header("Access-Control-Allow-Credentials", "true");
//   res.header("Access-Control-Allow-Headers", "Origin,Content-Type, token, x-id, Content-Length, X-Requested-With");

//   res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//   next();
// });

app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.json({ limit: '500mb', extended: true }));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads')));
app.use(express.static(path.join(__dirname, 'assets')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/admin', adminRouter);
app.use('/bankAccount', bankAccountRouter);
app.use('/buyer', buyerRouter);
app.use('/cart', cartRouter);
app.use('/company', companyRouter);
app.use('/inventory', inventoryRouter);
app.use('/medicineBank', medicineBankRouter);
app.use('/order', orderRouter);
app.use('/productCategory', productCategoryRouter);
app.use('/product', productRouter);
app.use('/promotions', promotionsRouter);
app.use('/seller', sellerRouter);
app.use('/transactions', transactionsRouter);
app.use('/settlements', settlementRouter);
app.use('/productBuyer', productBuyerRouter);
app.use('/systemConfig', systemConfig);
app.use('/systemActivity', systemActivity);
app.use('/productAuto', productAuto);
app.use('/groupSettlements', groupSettlementRouter);
app.use('/shippingManifest', shippingManifestRouter);
app.use('/reports', reportRouter);
app.use('/medicine', mediCategoryRouter);
app.use('/buyerRequest',buyerRequestRouter);
app.use('/shipping',shippingPartnerRouter);
app.use('/shortBook', shortBookRouter);
app.use('/ticket', ticketRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


var job = new CronJob({
  cronTime: '0 55 12 * * *',
  onTick: async () => {
    await helper.delhivaeryUpdateStatus();
    await helper.shipRocketUpdateStatus();
  },
  start: true,
  timeZone: 'Asia/Kolkata'
});
job.start();

var activeInvenJob = new CronJob({
  cronTime: '0 */3 * * *',
  onTick: async () => {
    getActiveInventory();
  },
  start: true,
  timeZone: 'Asia/Kolkata'
});
activeInvenJob.start();
 
module.exports = app;

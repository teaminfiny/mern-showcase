const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const shortBookController = require('../controllers/shortBook')

var expressValidator = require('express-validator');
router.use(expressValidator())

router.post('/listShortBook', [auth.authenticateUser], (req, res, next) => {
    shortBookController.listShortBook(req, res);
});

router.post('/addShortBook', [auth.authenticateUser], (req, res, next) => {
    shortBookController.addShortBook(req, res);
});

router.post('/removeShortBook', [auth.authenticateUser], (req, res, next) => {
    shortBookController.removeShortBook(req, res);
});

router.post('/getShortProducts', [auth.authenticateUser], (req, res, next) => {
    shortBookController.getShortProducts(req, res);
});

router.post('/getShortbookFilters', [auth.authenticateUser], (req, res, next) => {
    shortBookController.getShortbookFilters(req, res);
});

router.post('/notifyUsers', [auth.authenticateUser], (req, res, next) => {
    shortBookController.notifyUsers(req, res);
});

module.exports = router;
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var productCategoryController = require('../controllers/productCategory');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var productCategory = require("../models/product_category");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


// router.post('/listingProductCategory', [auth.authenticateUser, auth.isAuthenticated('listingProductCategory')], (req, res, next) => {
//     productCategoryController.listingProductCategory(req, res);
// });
router.post('/listingProductCategory', (req, res, next) => {
    productCategoryController.listingProductCategory(req, res);
});

router.post('/addProductCategory',(req, res, next) => {
    productCategoryController.addProductCategory(req, res);
});

router.post('/editProductCategory', [auth.authenticateUser, auth.isAuthenticated('editProductCategory')], (req, res, next) => {
    productCategoryController.editProductCategory(req, res);
});

router.post('/deleteProductCategory', [auth.authenticateUser, auth.isAuthenticated('deleteProductCategory')], (req, res, next) => {
    productCategoryController.deleteProductCategory(req, res);
});


module.exports = router;

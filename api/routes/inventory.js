var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var inventoryController = require('../controllers/inventory');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var inventory = require("../models/inventory");
const randomstring = require("randomstring");
var {getActiveInventory} = require('../controllers/activeInventory');

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingInventory', [auth.authenticateUser, auth.isAuthenticated('listingInventory')], (req, res, next) => {
  inventoryController.listingInventory(req, res);
});

router.post('/oldlistingInventory', [auth.authenticateUser, auth.isAuthenticated('listingInventory')], (req, res, next) => {
  inventoryController.oldlistingInventory(req, res);
});

router.post('/inventoryDetails', [auth.authenticateUser, auth.isAuthenticated('inventoryDetails')], (req, res, next) => {
  inventoryController.inventoryDetails(req, res);
});

router.post('/addInventoryItem', [auth.authenticateUser, auth.isAuthenticated('addInventoryItem')], (req, res, next) => {
  inventoryController.addInventoryItem(req, res);
});

router.post('/editInventoryItem', [auth.authenticateUser, auth.isAuthenticated('editInventoryItem')], (req, res, next) => {
  inventoryController.editInventoryItem(req, res);
});

router.post('/updateStockQuantity', [auth.authenticateUser, auth.isAuthenticated('updateStockQuantity')], (req, res, next) => {
  inventoryController.updateStockQuantity(req, res);
});

router.post('/deleteInventoryItem', [auth.authenticateUser, auth.isAuthenticated('deleteInventoryItem')], (req, res, next) => {
  inventoryController.deleteInventoryItem(req, res);
});

router.post('/hideInventoryItem', [auth.authenticateUser, auth.isAuthenticated('hideInventoryItem')], (req, res, next) => {
  inventoryController.hideInventoryItem(req, res);
});

router.post('/requestAddProduct', [auth.authenticateUser, auth.isAuthenticated('requestAddProduct')], (req, res, next) => {
  inventoryController.requestAddProduct(req, res);
});
router.post('/searchInventory', [auth.authenticateUser, auth.isAuthenticated('searchInventory')], (req, res, next) => {
  inventoryController.searchInventory(req, res);
});
router.post('/outOfStockFive', [auth.authenticateUser, auth.isAuthenticated('outOfStockFive')], (req, res, next) => {
  inventoryController.outOfStockFive(req, res);
});

router.post('/search', (req, res, next) => {
  inventoryController.search(req, res);
});

router.post('/v1/search', (req, res, next) => {
  inventoryController.v1search(req, res);
});

router.post('/get_categories', (req, res, next) => {
  inventoryController.getCategory(req, res);
});

router.get('/get_filters', (req, res, next) => {
  inventoryController.getFilters(req, res);
});

router.post('/list', [auth.authenticateUser, auth.isAuthenticated('getList')], (req, res, next) => {
  inventoryController.getList(req, res);
});

router.post('/dealOfTheDay', [auth.authenticateUser, auth.isAuthenticated('dealOfTheDay')], (req, res, next) => {
  inventoryController.dealOfTheDay(req, res);
});
router.post('/Service', [auth.authenticateUser], (req, res) => {
  inventoryController.Service(req, res)
})

router.post('/addEptr', (req, res, next) => {
  inventoryController.addEptr(req, res);
});

router.get('/addMedicineType',(req, res) => {
  inventoryController.addParameters(req, res);
})

router.post('/updateActiveInventory', (req, res, next) => {
  inventoryController.updateActiveInventory(req, res);
});

router.get('/getActiveInventory',(req, res) => {
  getActiveInventory();
});
router.get('/changeInventory',(req, res) => {
  inventoryController.changeInventory(req, res);
});

router.post('/jumboDeal', [auth.authenticateUser, auth.isAuthenticated('jumboDeal')], (req, res) => {
  inventoryController.jumboDeal(req, res);
});

module.exports = router;

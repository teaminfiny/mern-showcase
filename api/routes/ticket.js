var express = require('express');
var router = express.Router();
var ticketController = require('../controllers/ticket');
var auth = require("../lib/auth");
const { check, validationResult, body } = require('express-validator/check');
var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/addTicket', [auth.authenticateUser], (req, res) => {
  ticketController.addTickets(req, res)
})

module.exports = router;
var express = require('express');
var router = express.Router();
var groupSettlementController = require('../controllers/groupSettlements');
var auth = require("../lib/auth");

router.post('/newData',[auth.authenticateUser, auth.isAuthenticated('newData')],(req,res) => {
  groupSettlementController.newData(req, res);
})

router.post('/listGroupSettlement',[auth.authenticateUser, auth.isAuthenticated('listGroupSettlement')],(req,res) => {
  groupSettlementController.listGroupSettlement(req, res);
})

router.get('/generateRemittance',(req,res) => {
  groupSettlementController.generateRemittance(req, res);
})
router.get('/sendReportEmail',[auth.authenticateUser],(req,res) => {
  groupSettlementController.sendReportEmail(req, res);
})

router.get('/sellerGraphData',[auth.authenticateUser, auth.isAuthenticated('sellerGraphData')],(req, res) => {
  groupSettlementController.sellerGraphData(req, res);
})

router.get('/getSettledCsv',[auth.authenticateUser],(req, res) => {
  groupSettlementController.getSettledCsv(req, res);
})

router.post('/fixGrpSettlement',[auth.authenticateUser],(req, res) => {
  groupSettlementController.fixGrpSettlement(req, res);
})
module.exports = router;
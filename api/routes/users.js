var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var userController = require('../controllers/user');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var help = require("../lib/helper");
const randomstring = require("randomstring");
const { check, validationResult, body } = require('express-validator/check');
var expressValidator = require('express-validator');
router.use(expressValidator())


// Signup with normal image upload 
/* router.post('/signup', (req, res) => {
    var dir = './assets/users';
    var fileName = randomstring.generate(8);
    // upload image
    helper.fileUpload(req, res, dir, fileName, function (status, fileName, req, res) {
      if (!req.file) {
        req.body.profileImage = '';
      }
      else {
        req.body.compliance_form = fileName;
      }
      userController.signup(req, res);
    });
}); */

// Signup for seller
router.post('/signup', [
  check('email').custom(value => {
    return user.findOne({ email: value.trim().toLowerCase() }).then((data) => {
      if (data) {
        throw new Error('Email already exists')
      }
      return true
    })
  }),
  check('phone').custom(value => {
    return user.findOne({ phone: value }).then((data) => {
      if (data) {
        throw new Error('Phone already exists')
      }
      return true
    })
  }),
  check('first_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for first name')
    }
    return true
  }),
  check('last_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for last name')
    }
    return true
  }),
  check('company_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for company name')
    }
    return true
  }),
  check('user_city').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for city')
    }
    return true
  })

], (req, res) => {
  userController.signup(req, res);
});

//signup buyers
router.post('/register', [
  check('email').custom(value => {
    return user.findOne({ email: value.trim().toLowerCase() }).then((data) => {
      if (data) {
        throw new Error('Email already exists')
      }
      return true
    })
  }),
  check('phone').custom(value => {
    return user.findOne({ phone: value }).then((data) => {
      if (data) {
        throw new Error('Phone already exists')
      }
      return true
    })
  }),
  check('first_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for first name')
    }
    return true
  }),
  check('last_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for last name')
    }
    return true
  }),
  check('company_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for company name')
    }
    return true
  }),
  check('user_city').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for city')
    }
    return true
  })
], (req, res) => {
  userController.registerBuyer(req, res);
});

// signin for seller
router.post('/login', (req, res, next) => {
  userController.login(req, res);
});

// signin for buyers
router.post('/signin', (req, res, next) => {
  userController.signin(req, res);
});

router.post('/changePassword', [auth.authenticateUser], (req, res, next) => {
  userController.changePassword(req, res);
});

router.post('/forgotPassword', (req, res, next) => {
  userController.forgotPassword(req, res);
});


router.post('/resendOTP', [auth.authenticateUser], (req, res, next) => {

  userController.resendOTP(req, res);
});


router.post('/verifyOTP', (req, res, next) => {
  userController.verifyOTP(req, res);
});

/* logout user */
router.post('/userLogout', [auth.authenticateUser, auth.isAuthenticated('userLogout')], (req, res, next) => {
  userController.userLogout(req, res);
});

router.post('/editUserProfile', [auth.authenticateUser], [
  check('first_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for first name')
    }
    return true
  }),
  check('last_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for last name')
    }
    return true
  }),
  check('company_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for company name')
    }
    return true
  }),
  check('user_city').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for city')
    }
    return true
  })
], (req, res, next) => {
  userController.editUserProfile(req, res);
});

router.get('/getuserDetails', [auth.authenticateUser], (req, res, next) => {
  userController.getuserDetails(req, res)
});

router.post('/signupVerificationOtp', (req, res, next) => {
  userController.signupVerificationOtp(req, res);
});
router.post('/mobileVerification', (req, res, next) => {
  userController.mobileVerification(req, res);
});

router.post('/getSidbarContent', [auth.authenticateUser], (req, res, next) => {
  userController.getSidbarContent(req, res);
});

router.post('/validateEmail', (req, res, next) => {
  userController.validateEmail(req, res);
});

router.post('/getNotification', [auth.authenticateUser], (req, res, next) => {
  userController.getNotification(req, res);
});

router.post('/markNotificationRead', [auth.authenticateUser], (req, res, next) => {
  userController.markNotificationRead(req, res);
});

router.post('/addRating', [auth.authenticateUser], (req, res, next) => {
  userController.addRating(req, res);
});

router.post('/getRating', [auth.authenticateUser], (req, res, next) => {
  userController.getRating(req, res);
});

router.get('/getAverageRating', [auth.authenticateUser], (req, res, next) => {
  userController.getAverageRating(req, res);
});

router.post('/updateComplainceForm', [auth.authenticateUser], (req, res, next) => {
  userController.updateComplainceForm(req, res);
});
router.post('/updateBankDetails', [auth.authenticateUser], (req, res) => {
  userController.updateBankDetails(req, res)
})

router.post('/updateProfile', [auth.authenticateUser], [
  check('first_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for first name')
    }
    return true
  }),
  check('last_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for last name')
    }
    return true
  }),
  check('company_name').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for company name')
    }
    return true
  }),
  check('user_city').custom(value => {
    let regex = /^[a-zA-Z0-9 ]+$/;
    let isValid = regex.test(value)
    if (isValid == false) {
      throw new Error('Special characters are not allowed for city')
    }
    return true
  })
], (req, res, next) => {
  userController.updateProfile(req, res)
});

/* 
router.get('/getAllUsers', [auth.authenticateUser, auth.isAuthenticated('getAllUsers')], (req, res, next) => {
  userController.getAllUser(req, res)
});

router.get('/profile', [auth.authenticateUser, auth.isAuthenticated('profile')], (req, res, next) => {
  userController.getProfile(req, res)
}); 
*/

router.post('/markRead', [auth.authenticateUser], [], (req, res) => {
  userController.markRead(req, res);
});

// Signup for seller
router.get('/sms', (req, res) => {
  userController.sms(req, res);
});

router.get('/email', (req, res) => {
  userController.email(req, res);
});

router.get('/sendBroadcast', (req, res) => {
  userController.sendBroadcast(req, res);
});


router.post('/codOnOff', [auth.authenticateUser, auth.isAuthenticated('codOnOff')], (req, res) => {
  userController.codOnOff(req, res);
});

router.post('/deliveryChargeOnOff', [auth.authenticateUser, auth.isAuthenticated('deliveryChargeOnOff')], (req, res) => {
  userController.deliveryChargeOnOff(req, res);
})

router.post('/resetPassAdmin', [auth.authenticateUser, auth.isAuthenticated('resetPassAdmin')], (req, res) => {
  userController.resetPassAdmin(req, res);
});
router.get('/getCodService', [auth.authenticateUser], (req, res) => {
  userController.getCodService(req, res)
})
router.post('/rtoCharge', [auth.authenticateUser], (req, res) => {
  userController.rtoCharge(req, res)
})
router.post('/addRemark', [auth.authenticateUser,auth.isAuthenticated('addRemark')], (req, res, next) => {
  userController.addRemark(req, res);
});
router.post('/sellerConfig', [auth.authenticateUser,auth.isAuthenticated('sellerConfig')], (req, res, next) => {
  userController.sellerConfig(req, res);
});
router.get('/getBuyerslessthan90', (req, res) => {
  userController.getBuyerslessthan90(req, res)
})
router.post('/sendEmailToAdmin', (req, res, next) => {
  userController.sendEmailToAdmin(req, res);
});
router.get('/generateLabel1', (req, res, next) => {
  help.generateLabel1(req, res);
});
module.exports = router;

var express = require('express');
var router = express.Router();
var productController = require('../controllers/product');
var auth = require("../lib/auth");

var expressValidator = require('express-validator');
router.use(expressValidator())

var productControllerBuyers = require('../controllers/productBuyer');

router.post('/featuredProductList', (req, res, next) => {
    productControllerBuyers.featuredProductList(req, res);
});

router.post('/productDetails', (req, res, next) => {
    productControllerBuyers.productDetails(req, res);
});

router.post('/productForYou', [auth.authenticateUser], (req, res, next) => {
    productControllerBuyers.productForYou(req, res);
});

router.post('/v1/featuredProductList', (req, res, next) => {
    productControllerBuyers.v1featuredProductList(req, res);
});

router.post('/v1/productDetails', (req, res, next) => {
    productControllerBuyers.v1productDetails(req, res);
});

router.post('/v1/productForYou', [auth.authenticateUser], (req, res, next) => {
    productControllerBuyers.v1productForYou(req, res);
});

module.exports = router;
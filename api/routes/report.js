const express = require("express");

const router = express.Router();

const generateReportController = require("../controllers/report");

router.post("/generateReports",(req, res) => {
    generateReportController.generateReport(req, res);
});

router.post("/getSellerPerformance",(req, res) => {
    generateReportController.getSellerPerformance(req, res);
});

module.exports = router;
const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const buyerRequestController = require('../controllers/buyerRequest');

var expressValidator = require('express-validator');
router.use(expressValidator())

router.post('/listBuyerRequest', [auth.authenticateUser], (req, res, next) => {
    buyerRequestController.listBuyerRequest(req, res);
});

router.post('/createBuyerRequest', [auth.authenticateUser], (req, res, next) => {
    buyerRequestController.createBuyerRequest(req, res);
});

router.post('/updateBuyerRequest', [auth.authenticateUser], (req, res, next) => {
    buyerRequestController.updateBuyerRequest(req, res);
});

router.post('/deleteBuyerRequest', [auth.authenticateUser], (req, res, next) => {
    buyerRequestController.deleteBuyerRequest(req, res);
});

router.post('/notifyUser', [auth.authenticateUser], (req, res, next) => {
    buyerRequestController.notifyUser(req, res);
});

router.get('/changeRequestStatus', (req, res) => {
    buyerRequestController.changeRequestStatus(req, res);
});

module.exports = router;
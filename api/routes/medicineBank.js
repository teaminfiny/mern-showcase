var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var medicineBankController = require('../controllers/medicineBank');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var product = require("../models/product");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingMedicineBank', [auth.authenticateUser, auth.isAuthenticated('listingMedicineBank')], (req, res, next) => {
    medicineBankController.listingMedicineBank(req, res);
});

router.post('/addProduct', [auth.authenticateUser, auth.isAuthenticated('addProduct')], (req, res, next) => {
    medicineBankController.addProduct(req, res);
});

router.post('/editProduct', [auth.authenticateUser, auth.isAuthenticated('editProduct')], (req, res, next) => {
    medicineBankController.editProduct(req, res);
});

router.post('/removeProduct', [auth.authenticateUser, auth.isAuthenticated('removeProduct')], (req, res, next) => {
    medicineBankController.removeProduct(req, res);
});

router.post('/approveProduct', [auth.authenticateUser, auth.isAuthenticated('approveProduct')], (req, res, next) => {
    medicineBankController.approveProduct(req, res);
});

router.post('/getAllProducts', [auth.authenticateUser, auth.isAuthenticated('getAllProducts')], (req, res, next) => {
    medicineBankController.getAllProducts(req, res);
});

module.exports = router;

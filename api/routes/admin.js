var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var adminController = require('../controllers/admin');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var config = require("../models/config");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())



router.get('/bulkProduct', (req, res, next) => {
  adminController.bulkProduct(req, res);

});


router.get('/getStates', (req, res) => {
  adminController.getStates(req, res);
});

router.post('/addStates', (req, res, next) => {
  adminController.addStates(req, res);
});

router.post('/changeUserStatus', [auth.authenticateUser, auth.isAuthenticated('changeUserStatus')], (req, res, next) => {
  adminController.changeUserStatus(req, res);
});

router.post('/approveOrdenyUser', [auth.authenticateUser, auth.isAuthenticated('banOrsuspendUSer')], (req, res, next) => {
  adminController.approveOrdenyUser(req, res);
});

router.post('/addConfigData', [auth.authenticateUser, auth.isAuthenticated('addConfigData')], (req, res, next) => {
  adminController.addConfigData(req, res);
});

router.post('/listingConfigData', [auth.authenticateUser, auth.isAuthenticated('listingConfigData')], (req, res, next) => {
  adminController.listingConfigData(req, res);
});

router.post('/listingStaff', [auth.authenticateUser, auth.isAuthenticated('listingStaff')], (req, res, next) => {
  adminController.listingStaff(req, res);
});

router.post('/addStaff', [auth.authenticateUser, auth.isAuthenticated('addStaff')], (req, res, next) => {
  adminController.addStaff(req, res);
});

router.post('/editStaff', [auth.authenticateUser, auth.isAuthenticated('editStaff')], (req, res, next) => {
  adminController.editStaff(req, res);
});

router.post('/staffDetails', [auth.authenticateUser, auth.isAuthenticated('staffDetails')], (req, res, next) => {
  adminController.staffDetails(req, res);
});

router.post('/deleteStaff', [auth.authenticateUser, auth.isAuthenticated('deleteStaff')], (req, res, next) => {
  adminController.deleteStaff(req, res);
});

router.post('/sellerBuyerListing', [auth.authenticateUser, auth.isAuthenticated('sellerBuyerListing')], (req, res, next) => {
  adminController.sellerBuyerListing(req, res);
});

router.post('/sellerBuyerDetails', [auth.authenticateUser, auth.isAuthenticated('sellerBuyerDetails')], (req, res, next) => {
  adminController.sellerBuyerDetails(req, res);
});

router.post('/createSettlements', [auth.authenticateUser, auth.isAuthenticated('createSettlements')], (req, res, next) => {
  adminController.createSettlements(req, res);
});

router.post('/getUsers', [auth.authenticateUser, auth.isAuthenticated('getUsers')], (req, res, next) => {
  adminController.getUsers(req, res);
});

router.post('/approve', [auth.authenticateUser, auth.isAuthenticated('approve')], (req, res, next) => {
  adminController.approve(req, res);
});

router.post('/makeCategoryFeatured', (req, res, next) => {
  adminController.makeCategoryFeatured(req, res);
});

router.post('/makeInventoryDealsOfDay', (req, res, next) => {
  adminController.makeInventoryDealsOfDay(req, res);
});

router.post('/getMetadata', (req, res, next) => {
  adminController.getMetadata(req, res);
});

router.post('/processProductRequest', [auth.authenticateUser, auth.isAuthenticated('processProductRequest')], (req, res, next) => {
  adminController.processProductRequest(req, res);
});

router.post('/addGST', [auth.authenticateUser, auth.isAuthenticated('addGST')], (req, res, next) => {
  adminController.addGST(req, res);
});

router.post('/deleteGST', [auth.authenticateUser, auth.isAuthenticated('deleteGST')], (req, res, next) => {
  adminController.deleteGST(req, res);
});

router.post('/addCategory', [auth.authenticateUser, auth.isAuthenticated('addCategory')], (req, res, next) => {
  adminController.addCategory(req, res);
});

router.post('/deleteCategory', [auth.authenticateUser, auth.isAuthenticated('deleteCategory')], (req, res, next) => {
  adminController.deleteCategory(req, res);
});

router.post('/addProductType', [auth.authenticateUser, auth.isAuthenticated('addProductType')], (req, res, next) => {
  adminController.addProductType(req, res);
});

router.post('/deleteProductType', [auth.authenticateUser, auth.isAuthenticated('deleteProductType')], (req, res, next) => {
  adminController.deleteProductType(req, res);
});

router.post('/addCompany', [auth.authenticateUser, auth.isAuthenticated('addCompany')], (req, res, next) => {
  adminController.addCompany(req, res);
});

router.post('/deleteCompany', [auth.authenticateUser, auth.isAuthenticated('deleteCompany')], (req, res, next) => {
  adminController.deleteCompany(req, res);
});

router.post('/getStats', (req, res, next) => {
  adminController.getStats(req, res);
});
router.post('/getTopBuyer', (req, res, next) => {
  adminController.getTopBuyer(req, res);
});
router.post('/getTopSeller', (req, res, next) => {
  adminController.getTopSeller(req, res);
});
router.post('/addbulkZipcodes', (req, res) => {
  adminController.addbulkZipcodes(req, res)
})
router.post('/fixSearch', (req, res, next) => {
  adminController.fixSearch(req, res);
});
router.post('/broadcastMessage', [auth.authenticateUser, auth.isAuthenticated('broadcastMessage')], (req, res) => {
  adminController.broadcastMessage(req, res)
})
router.post('/verifyPhoneUser', [auth.authenticateUser, auth.isAuthenticated('verifyPhoneUser')], (req, res) => {
  adminController.verifyPhoneUser(req, res)
})
router.post('/updateAddress', [auth.authenticateUser, auth.isAuthenticated('updateAddress')], (req, res) => {
  adminController.updateAddress(req, res)
})
router.post('/generateLabelUser', [auth.authenticateUser], (req, res) => {
  adminController.generateLabelUser(req, res)
})
router.post('/replaceSpecailChar', (req, res) => {
  adminController.replaceSpecailChar(req, res)
})
router.post('/creditDebitWallet', [auth.authenticateUser, auth.isAuthenticated('creditDebitWallet')], (req, res) => {
  adminController.creditDebitWallet(req, res);
})
router.post('/searchBuyer', (req, res) => {
  adminController.searchBuyer(req, res);
});
router.post('/getBuyers', [auth.authenticateUser], (req, res)=> {
  adminController.getBuyers(req, res);
})
router.get('/getBuyer', [auth.authenticateUser], (req, res)=> {
  adminController.getBuyer(req, res);
})
router.post('/adminToUser', (req, res)=> {
  adminController.adminToUser(req, res);
})
module.exports = router;

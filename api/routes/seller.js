var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var sellerController = require('../controllers/seller');
var courierResponse = require('../models/courierResponse');
var orderController = require('../controllers/order');
const { vacationMode, getGroupModule, addGroupModule, editGroupModule, deleteGroupModule, getPermissionModule } = require('../controllers/seller')
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingSellers', [auth.authenticateUser, auth.isAuthenticated('listingSellers')], (req, res, next) => {
  sellerController.listingSellers(req, res);
});

router.post('/getSellerDetails', [auth.authenticateUser, auth.isAuthenticated('getSellerDetails')], (req, res, next) => {
  sellerController.getSellerDetails(req, res);
});

router.post('/enableDisableVaccation', [auth.authenticateUser, auth.isAuthenticated('enableDisableVaccation')], (req, res) => {
  vacationMode(req, res)
})

router.post('/getGroupModule', [auth.authenticateUser, auth.isAuthenticated('getGroupModule')], (req, res) => {
  getGroupModule(req, res)
})

router.post('/addEditGroupModule', [auth.authenticateUser, auth.isAuthenticated('addGroupModule')], (req, res) => {
  if (req.body.groupId) {
    editGroupModule(req, res)
  } else {
    addGroupModule(req, res)
  }

})

router.post('/deleteGroupModule', [auth.authenticateUser, auth.isAuthenticated('deleteGroupModule')], (req, res) => {
  deleteGroupModule(req, res)
})

router.post('/getPermissionModule', [auth.authenticateUser, auth.isAuthenticated('getPermissionModule')], (req, res) => {
  getPermissionModule(req, res)
})

router.post('/getStaffList', [auth.authenticateUser, auth.isAuthenticated('getStaffList')], (req, res) => {
  sellerController.getStaffList(req, res)
})

router.post('/addEditStaffMember', [auth.authenticateUser, auth.isAuthenticated('addEditStaffMember')], (req, res) => {
  if (req.body.staffId) {
    sellerController.editStaffMember(req, res)
  } else if ( req.body.reset === true ) {
    sellerController.resetPassStaffMember(req, res)
  }else {
    sellerController.addStaffMember(req, res)
  }
})

router.post('/deleteStaff', [auth.authenticateUser, auth.isAuthenticated('deleteStaff')], (req, res) => {
  sellerController.deleteStaff(req, res)
})

router.get('/getGroupList', [auth.authenticateUser, auth.isAuthenticated('getGroupList')], (req, res) => {
  sellerController.getGroupList(req, res)
})
router.get('/getDataforProductRequest', [auth.authenticateUser, auth.isAuthenticated('getDataforProductRequest')], (req, res) => {
  sellerController.getDataforProductRequest(req, res)
})
router.post('/addProductRequest', [auth.authenticateUser, auth.isAuthenticated('addProductRequest')], (req, res) => {
  sellerController.addProductRequest(req, res)
})
router.post('/getRequestedProductList', [auth.authenticateUser, auth.isAuthenticated('getRequestedProductList')], (req, res) => {
  sellerController.getRequestedProductList(req, res)
})
router.get('/getDashBoardCardDetails', [auth.authenticateUser, auth.isAuthenticated('getDashBoardCardDetails')], (req, res) => {
  sellerController.getDashBoardCardDetails(req, res)
})
router.post('/getDashboardTopSellingProducts', [auth.authenticateUser, auth.isAuthenticated('getDashboardTopSellingProducts')], (req, res) => {
  sellerController.getDashboardTopSellingProducts(req, res)
})
router.get('/getDataForChart', [auth.authenticateUser, auth.isAuthenticated('getDataForChart')], (req, res) => {
  sellerController.getDataForChart(req, res)
})
router.get('/getGraphForDahboard', [auth.authenticateUser, auth.isAuthenticated('getGraphForDahboard')], (req, res) => {
  sellerController.getGraphForDahboard(req, res)
})

router.post('/getSettlements', [auth.authenticateUser, auth.isAuthenticated('getSettlements')], (req, res) => {
  sellerController.getSettlements(req, res)
})

router.get('/getDispatcedOrders', [auth.authenticateUser, auth.isAuthenticated('getDispatcedOrders')], (req, res) => {
  sellerController.getDispatcedOrders(req, res)
})

router.post('/getDashboardLeastSellingProducts', [auth.authenticateUser, auth.isAuthenticated('getDashboardLeastSellingProducts')], (req, res) => {
  sellerController.getDashboardLeastSellingProducts(req, res)
})

router.post('/get_stats', (req, res) => {
  sellerController.getStats(req, res)
})

router.post('/courier-webhook', (req, res) => {
  orderController.addCourierResponse(req, res)
})
router.post('/cancelProductRequest', [auth.authenticateUser], (req, res) => {
  sellerController.cancelProductRequest(req, res)
})

module.exports = router;

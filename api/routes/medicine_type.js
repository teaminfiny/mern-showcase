var express = require('express');
var router = express.Router();
var auth = require("../lib/auth");
var mediController = require('../controllers/medicine_type');

router.post('/addMediCategory',  [auth.authenticateUser] ,(req, res)=>{
  mediController.addMedCategory(req, res);
})

router.get('/listMediCategory',  [auth.authenticateUser] ,(req, res)=>{
  mediController.listMedCategory(req, res);
})

module.exports = router;
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var transactionsController = require('../controllers/transactions');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var transactions = require("../models/transactions");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingTransactions', [auth.authenticateUser], (req, res, next) => {
    transactionsController.listingTransactions(req, res);
});

router.post('/TransactionDetails', [auth.authenticateUser], (req, res, next) => {
    transactionsController.TransactionDetails(req, res);
});

router.post('/listingWalletTransaction', [auth.authenticateUser], (req, res, next) => {
    transactionsController.listingWalletTransaction(req, res);
});

router.post('/walletTransactionDetails', [auth.authenticateUser], (req, res, next) => {
    transactionsController.walletTransactionDetails(req, res);
});

router.post('/addTransaction', [auth.authenticateUser], (req, res, next) => {
    transactionsController.addTransaction(req, res);
});

router.get('/getTransactionDetails', [auth.authenticateUser], (req, res, next) => {
    transactionsController.getTransactionDetails(req, res);
});

router.post('/downloadTransactionReport', [auth.authenticateUser], (req, res, next) => {
    transactionsController.downloadTransactionReport(req, res);
});

router.get('/test', (req, res) => {
    transactionsController.test(req, res);
});

module.exports = router;

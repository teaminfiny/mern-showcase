var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var promotionController = require('../controllers/promotions');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var promotions = require("../models/promotions");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listing', (req, res, next) => {
    promotionController.listingPromotions(req, res);
});

router.post('/add', [auth.authenticateUser,auth.isAuthenticated('addPromotion')], (req, res, next) => {
    promotionController.addPromotion(req, res);
});

router.post('/edit', [auth.authenticateUser, auth.isAuthenticated('editPromotion')], (req, res, next) => {
    promotionController.editPromotion(req, res);
});

router.post('/delete', [auth.authenticateUser, auth.isAuthenticated('delPromotion')], (req, res, next) => {
    promotionController.deletePromotion(req, res);
});

router.post('/active', [auth.authenticateUser, auth.isAuthenticated('activatePromotion')], (req, res, next) => {
    promotionController.activatePromotion(req, res);
});


module.exports = router;

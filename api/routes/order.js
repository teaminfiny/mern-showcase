var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var orderController = require('../controllers/order');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var order = require("../models/order");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/orderListing', [auth.authenticateUser, auth.isAuthenticated('orderListing')], (req, res, next) => {
    orderController.orderListing(req, res);
});

router.post('/ChangeStatus', [auth.authenticateUser, auth.isAuthenticated('ChangeStatus')], (req, res, next) => {
    orderController.ChangeStatus(req, res);
});

router.post('/getBuyerOrderDetails', [auth.authenticateUser], (req, res, next) => {
    orderController.getOrderDetails(req, res);
});

router.post('/getOrderDetails', [auth.authenticateUser, auth.isAuthenticated('getOrderDetails')], (req, res, next) => {
    orderController.getOrderDetails(req, res);
});

router.post('/updateOrder', [auth.authenticateUser], (req, res, next) => {
    orderController.updateOrder(req, res);
});

router.post('/proccessCancelOrder', [auth.authenticateUser], (req, res, next) => {
    if (req.body.status === 'Processed') {
        orderController.processOrder(req, res);
    } else {
        orderController.cancelOrder(req, res);
    }

});

router.post('/orderHistory', [auth.authenticateUser], (req, res, next) => {
    orderController.orderHistory(req, res);
});

router.post('/orderHistoryBuyers', [auth.authenticateUser], (req, res, next) => {
    orderController.orderHistoryBuyers(req, res);
});

router.post('/orderListingBuyers', [auth.authenticateUser], (req, res, next) => {
    orderController.orderListingBuyers(req, res);
});

router.post('/dashboardSalesStats', [auth.authenticateUser], (req, res, next) => {
    orderController.dashboardSalesStats(req, res);
});
router.post('/cancelOrder', [auth.authenticateUser, auth.isAuthenticated('cancelOrder')], (req, res, next) => {
    orderController.cancelOrder(req, res);
});

router.post('/createOrder', [auth.authenticateUser], (req, res, next) => {
    orderController.createOrder(req, res);
});

router.post('/confirm', [auth.authenticateUser], (req, res, next) => {
    orderController.confirm(req, res);
});
router.post('/getReponse', (req, res) => {
    orderController.getReponse(req, res);
    // res.redirect("https://medideals-web.infiny.dev")

})
router.get('/redirect', (req, res) => {
    res.render('redirect')
})

router.get('/export', [auth.authenticateUser], (req, res, next) => {
    orderController.exportCsv(req, res);
});

router.get('/generateLabel', (req, res, next) => {
    orderController.generateLabel(req, res);
});

router.get('/generateOrderMail', (req, res, next) => {
    orderController.generateOrderMail(req, res);
});

router.post('/dotZotDummy', (req, res) => {
    orderController.dotZotDummy(req, res)
})

router.post('/adminProcessOrder', [auth.authenticateUser, auth.isAuthenticated('adminProcessOrder')], (req, res) => {
    orderController.adminProcessOrder(req, res)
})
router.get('/generatePdf', (req, res) => {
    orderController.generatePdf(req, res)
})
router.post('/dashboardSalesStatsAdmin', [auth.authenticateUser, auth.isAuthenticated('dashboardSalesStatsAdmin')], (req, res, next) => {
    orderController.dashboardSalesStatsAdmin(req, res);
});

router.post('/allOrderListing', [auth.authenticateUser, auth.isAuthenticated('allOrderListing')], (req, res, next) => {
    orderController.allOrderListing(req, res);
});

router.post('/changeOrder', [auth.authenticateUser, auth.isAuthenticated('changeOrder')], (req, res) => {
    orderController.changeOrder(req, res);
});
router.post('/delhiveryWebhook', (req, res) => {
    orderController.delhiveryWebhook(req, res)
})
router.post('/markDelivered', (req, res) => {
    orderController.markDelivered(req, res)
})
router.get('/sellerDashGraph',[auth.authenticateUser], (req, res) => {
    orderController.sellerDashGraph(req, res)
})
router.get('/addBuyerSellerCompName',[auth.authenticateUser], (req, res) => {
    orderController.addBuyerSellerCompName(req, res)
})
router.get('/markOrderNew',[auth.authenticateUser], (req, res) => {
    orderController.markOrderNew(req, res)
})
router.get('/checkProcess',[auth.authenticateUser], (req, res) => {
    orderController.checkThreshold(req, res)
})
router.get('/checkOrder', (req, res) => {
    orderController.checkOrder(req, res)
})
router.get('/changeOrder2', (req, res) => {
    orderController.changeOrder2(req, res)
})
router.get('/findCancelledOrders', (req, res) => {
    orderController.findCancelledOrders(req, res)
})
router.post('/getSellerPerformanceOrders', (req, res) => {
    orderController.getSellerPerformanceOrders(req, res)
})
router.post('/getBuyerSellerByRevenue',[auth.authenticateUser], (req, res) => {
    orderController.getBuyerSellerByRevenue(req, res)
})
router.post('/markOrderAsEthical', (req, res) => {
    orderController.markOrderAsEthical(req, res)
})
router.post('/orderSeqFix', (req, res) => {
    orderController.orderSeqFix(req, res)
})
router.post('/addCancelledBy',[auth.authenticateUser], (req, res) => {
    orderController.addCancelledBy(req, res)
})
router.get('/getOrderedProducts', (req, res) => {
    orderController.getOrderedProducts(req, res)
})
module.exports = router;

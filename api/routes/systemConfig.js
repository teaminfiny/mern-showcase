var express = require('express');
var router = express.Router();
var systemConfigController = require('../controllers/systemConfig.js');
var auth = require("../lib/auth");
var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/getAllSystemConfigList', [auth.authenticateUser], (req, res) => {
  systemConfigController.getAllSystemConfig(req, res)
})

router.post('/getchangeGlobalCod', [auth.authenticateUser, auth.isAuthenticated('getchangeGlobalCod')], (req, res) => {
  systemConfigController.changeGlobalCod(req, res)
})
router.get('/getDeviceVersion', (req, res) => {
  systemConfigController.getDeviceVersion(req, res)
})

module.exports = router;
var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var buyerController = require('../controllers/buyer');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingBuyer', [auth.authenticateUser, auth.isAuthenticated('listingBuyer')], (req, res, next) => {
  buyerController.listingBuyer(req, res);
});

router.post('/getBuyerDetails', [auth.authenticateUser, auth.isAuthenticated('getBuyerDetails')], (req, res, next) => {
  buyerController.getBuyerDetails(req, res);
});

router.post('/searchProduct', [auth.authenticateUser, auth.isAuthenticated('searchProduct')], (req, res, next) => {
  buyerController.searchProduct(req, res);
});

router.post('/finalCalculationBeforeCheckout', [auth.authenticateUser, auth.isAuthenticated('finalCalculationBeforeCheckout')], (req, res, next) => {
  buyerController.finalCalculationBeforeCheckout(req, res);
});

router.post('/Checkout', [auth.authenticateUser, auth.isAuthenticated('Checkout')], (req, res, next) => {
  buyerController.Checkout(req, res);
});

router.post('/payMoneyAfterCheckout', [auth.authenticateUser, auth.isAuthenticated('payMoneyAfterCheckout')], (req, res, next) => {
  buyerController.payMoneyAfterCheckout(req, res);
});

router.post('/depositCreditInWallet', [auth.authenticateUser, auth.isAuthenticated('depositCreditInWallet')], (req, res, next) => {
  buyerController.depositCreditInWallet(req, res);
});


router.post('/visitedCategory', [auth.authenticateUser], (req, res) => {
  buyerController.visitedCategory(req, res)
})

/*
# parameters: token,
# purpose: Buyer can request for bulk product quantity to admin with discount.
# Email will trigger for admin with all product details along with qty,ptr, dicount and user details.
*/
router.post('/bulkRequest', [auth.authenticateUser], (req, res, next) => {
  buyerController.bulkRequest(req, res);
});

router.get('/stats', (req, res) => {
  buyerController.stats(req, res) 
})

module.exports = router;

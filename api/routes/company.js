var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var companyController = require('../controllers/company');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var company = require("../models/company");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingCompany', [auth.authenticateUser, auth.isAuthenticated('listingCompany')], (req, res, next) => {
    companyController.listingCompany(req, res);
});

router.post('/companyDetails', [auth.authenticateUser, auth.isAuthenticated('companyDetails')], (req, res, next) => {
    companyController.companyDetails(req, res);
});

router.post('/addCompanyDetails',  (req, res, next) => {
    companyController.addCompanyDetails(req, res);
});

router.post('/editCompanyDetails', [auth.authenticateUser, auth.isAuthenticated('editCompanyDetails')], (req, res, next) => {
    companyController.editCompanyDetails(req, res);
});

router.get('/deleteDuplicate', (req, res, next) => {
    companyController.deleteDuplicate(req, res);
});

module.exports = router;

var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var shippingPartnerController = require('../controllers/shippingPartner');
var auth = require("../lib/auth");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.get('/listShippingPartner',[auth.authenticateUser], (req, res) => {
  shippingPartnerController.listShippingPartner(req, res);
});

router.post('/addShippingPartner',[auth.authenticateUser], (req, res) => {
  shippingPartnerController.addShippingPartner(req, res);
});

module.exports = router;

var express = require('express');
var router = express.Router();
var productController = require('../controllers/product');
var auth = require("../lib/auth");
var product = require("../models/product");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/addProduct', (req, res, next) => {
    productController.addProduct(req, res);
});
router.post('/deleteProduct', (req, res, next) => {
    productController.deleteProduct(req, res);
});
router.post('/searchProduct', (req, res, next) => {
    productController.searchProduct(req, res);
});

router.post('/editProductDetail', (req, res, next) => {
    productController.editProductDetail(req, res);
});

router.post('/by_company', (req, res, next) => {
    productController.byCompany(req, res);
}); 
 
router.get('/autofill', (req, res, next) => {
    productController.productAutofill(req, res);
}); 

router.post('/list', (req, res, next) => {  
    productController.list(req, res);
}); 

router.post('/top5least5', (req, res, next) => {  
    productController.top5least5(req, res); 
}); 

router.post('/bestOffer', (req, res, next) => {  
    productController.bestOffer(req, res); 
}); 
router.post('/addCountryOfOrigin', (req, res, next) => {  
    productController.addCountryOfOrigin(req, res); 
}); 
router.post('/addMediCategoryToProd', (req, res, next) => {  
    productController.addMediCategoryToProd(req, res); 
}); 
router.get('/removeTabFromProduct', (req, res, next) => {  
    productController.removeTabFromProduct(req, res); 
}); 
router.get('/getProductSurCharge', (req, res) => {  
    productController.getProductSurCharge(req, res); 
});

module.exports = router;

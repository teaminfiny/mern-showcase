var express = require('express');
var router = express.Router();
var ccAvenue = require('../controllers/ccAvaenue')

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Medimny API 1.0' });
});

router.get('/payOnline/:id', (req, res) => {
    ccAvenue.payOnline(req, res)
})
router.post('/ccavRequestHandler', (req, res) => {
    ccAvenue.postReq(req, res)
})

router.post('/ccavResponseHandler', (req, res) => {
    ccAvenue.postRes(req, res)
})
router.post('/getRSA', (req, res) => {
    ccAvenue.getRSA(req, res)
})
router.post('/ccavMobileReponse', (req, res) => {
    ccAvenue.postMobileResponse(req, res)
})
router.get('/getMobileView', (req, res) => {
    res.render('mobilleview')
})
router.post('/ccavstatus', (req, res) => {
    ccAvenue.getStatus(req, res)
})
module.exports = router;

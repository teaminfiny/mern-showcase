var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var bankController = require('../controllers/bankAccount');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var bankAccount = require("../models/bank_account");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/listingBankAccount', [auth.authenticateUser, auth.isAuthenticated('listingBankAccount')], (req, res, next) => {
    bankAccount.listingBankAccount(req, res);
});

router.post('/addBankAccount', [auth.authenticateUser, auth.isAuthenticated('addBankAccount')], (req, res, next) => {
    bankAccount.addBankAccount(req, res);
});

router.post('/deleteBankAccount', [auth.authenticateUser, auth.isAuthenticated('deleteBankAccount')], (req, res, next) => {
    bankAccount.deleteBankAccount(req, res);
});


module.exports = router;

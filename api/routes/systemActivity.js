var express = require('express');
var router = express.Router();
var systemActivityController = require('../controllers/systemActivity.js');
var auth = require("../lib/auth");

router.post('/addActivity', [auth.authenticateUser], (req, res) => {
  systemActivityController.addActivity(req, res)
})

router.post('/addActivityAdminLogin', [auth.authenticateUser], (req, res) => {
  systemActivityController.addActivityAdminLogin(req, res)
})

module.exports = router;
var express = require('express');
var router = express.Router();
var settlementsController = require('../controllers/settlements.js');
var auth = require("../lib/auth");
var multer  = require('multer')
var upload = multer()
var helper = require('../lib/helper')

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/list', [auth.authenticateUser, auth.isAuthenticated('settlementList')], (req, res, next) => {
    settlementsController.list(req, res); 
});

router.post('/settleOrder', [auth.authenticateUser, auth.isAuthenticated('settleOrder')], (req, res, next) => {
    settlementsController.settleOrder(req, res);
});

router.get('/settle', (req, res, next) => {
    settlementsController.settle(req, res);
});
router.post('/getOverallDetails', [auth.authenticateUser, auth.isAuthenticated('getOverallDetails')], (req, res) =>{
    settlementsController.getOverallDetails(req, res);
})
router.post('/settlementList', [auth.authenticateUser, auth.isAuthenticated('settlementList')], (req, res) =>{
    settlementsController.settlementList(req, res);
})
router.post('/getPendingSettlement', [auth.authenticateUser], (req, res) =>{
    settlementsController.getPendingSettlement(req, res);
})
router.post('/editSettlement', [auth.authenticateUser], (req, res) =>{
    settlementsController.editSettlement(req, res);
})
router.post('/groupSettle', [auth.authenticateUser], (req, res) =>{
    settlementsController.groupSettle(req, res);
})
router.post('/settleByCsv',upload.single('fileUpload'), [auth.authenticateUser], (req, res) =>{
    settlementsController.settleByCsv(req, res);
})
router.post('/settleManually', [auth.authenticateUser], (req, res) =>{
    settlementsController.settleManually(req, res);
})
router.get('/testRoute', (req, res) =>{
    helper.insertSettlement('60895b7f0b7be95120d05b71','04/29/2021');
    res.status(200).json({
        error:false,
        data:'test'
    })
})
router.get('/remittanceId', [auth.authenticateUser], (req, res) =>{
    settlementsController.remittanceId(req, res);
})
router.get('/settleIndividualOrder',[auth.authenticateUser, auth.isAuthenticated('settleIndividualOrder')], (req, res, next) => {
    settlementsController.settleIndividualOrder(req, res);
});
router.post('/changeInSettlement',[auth.authenticateUser], (req, res, next) => {
    settlementsController.changeInSettlement(req, res);
});
router.post('/removeDuplicates',[auth.authenticateUser], (req, res, next) => {
    settlementsController.removeDuplicates(req, res);
});
module.exports = router;

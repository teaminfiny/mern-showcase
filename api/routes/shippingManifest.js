var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var shippingManifestController = require('../controllers/shippingManifest');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var config = require("../models/config");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


router.post('/getManifest', [auth.authenticateUser], (req, res, next) => {
  shippingManifestController.getManifest(req, res);
});
router.post('/markManifested', [auth.authenticateUser], (req, res, next) => {
  shippingManifestController.markManifested(req, res);
});
router.get('/getManifestReport', (req, res, next) => {
  shippingManifestController.getManifestReport(req, res);
});
router.get('/deleteManifested', [auth.authenticateUser], (req, res, next) => {
  shippingManifestController.deleteManifested(req, res);
});

module.exports = router;

var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var moment = require('moment-timezone');
var cartController = require('../controllers/cart');
var helper = require("../lib/helper");
var auth = require("../lib/auth");
var user = require("../models/user");
var config = require("../models/cart");
const randomstring = require("randomstring");

var expressValidator = require('express-validator');
router.use(expressValidator())


// router.post('/cartDetails', [auth.authenticateUser, auth.isAuthenticated('cartDetails')], (req, res, next) => {
//     cartController.cartDetails(req, res);
// });
router.post('/cartDetails', [auth.authenticateUser], (req, res, next) => {
    cartController.cartDetails(req, res);
});

// router.post('/addtoCart', [auth.authenticateUser, auth.isAuthenticated('addtoCart')], (req, res, next) => {
//     cartController.addtoCart(req, res);
// });
router.post('/addtoCart', [auth.authenticateUser], (req, res, next) => {
    cartController.addtoCart(req, res);
});

router.post('/removeFromCart', [auth.authenticateUser], (req, res, next) => {
    cartController.removeFromCart(req, res);
});

router.post('/removeAll', [auth.authenticateUser], (req, res, next) => {
    cartController.removeAll(req, res);
});


module.exports = router;

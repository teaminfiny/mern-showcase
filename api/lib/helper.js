const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const async = require('async');
const fs = require('fs');
const moment = require('moment');
const randomstring = require('randomstring');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const multer = require('multer');
const timezone = 'America/New_York';
const user = require('../models/user');
const systemActivity = require('../models/systemActivity');
const order = require('../models/order');
const momentTimezone = require('moment-timezone');
var base64ToImage = require('base64-to-image');
var FCM = require('fcm-push');
var fcm = new FCM('AAAASBbgCvU:APA91bHY0VeNEQ1-2NtdI4FLTHUVS9uHJd9PtTSoaCrr6sPnfVN_ZD3vKJqcawX92jGClMG0suimgOeBHhMEYnUSF6WCLRqjXAlXI6qMGtGnmGKwuwXTKxKmMkiKxg6_fU3BWdsZWqse');
const groupModule = require('../models/permssionGroup');
const inventory = require('../models/inventory');
const transactions = require('../models/transactions')
const axios = require('axios')
var pdf = require("html-pdf");
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./s3_config.json');
var ejs = require("ejs");
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const Settlements = require('../models/settlement');
const Orders = require('../models/order');
const systemCon  = require('../models/systemConfig');
var s3Bucket = new AWS.S3({ params: { Bucket: 'medideals.assets' } });
var s3BaseUrl = "https://s3.ap-south-1.amazonaws.com/medideals.assets/";
const productAutoComp = require('../models/product_autocomp');
var request = require("request");
const notification = require('../models/notification');
var AutoComplete = require("mongoose-in-memory-autocomplete").AutoComplete;
const groupSettlement = require('../models/groupSettlement');
const mediType = require('../models/medicine_type');
const product = require('../models/product');
// Autocomplete configuration
var configuration = {
  //Fields being autocompleted, they will be concatenated
  autoCompleteFields: ["name"],
  //Returned data with autocompleted results
  dataFields: ["_id", "name"],
  //Maximum number of results to return with an autocomplete request
  maximumResults: 10,
  //MongoDB model (defined earlier) that will be used for autoCompleteFields and dataFields
  model: productAutoComp
}
//initialization of AutoComplete Module
var Autofillwords = new AutoComplete(configuration, function () {
  //any calls required after the initialization
  return true
});
var Autofillwordsfn = () => {
  productAutoComp.getProductName(function () {
    Autofillwords = new AutoComplete(configuration, function () {
      //any calls required after the initialization
      return true
    })
  })

};

const sendSMS = function (mobile, msg) {
  axios({
    method: 'get',
    url: 'https://api.textlocal.in/send/?apikey=' + process.env.smsapikey + '&sender=MEDMNY&numbers=' + mobile + '&message=' + msg,
    header: 'Content-Type: application/json'
  })
    .then((result) => {
    })
    .catch((error) => {
    })
}

const sendEmail = (data) => {
  let smtpTransport = nodemailer.createTransport({
    tls: { rejectUnauthorized: false },
    secureConnection: false,
    host: "smtp.mailgun.org",
    port: 587,
    requiresAuth: true,
    auth: {
      user: process.env.mail_username,
      pass: process.env.mail_password
    }
  });

  let mailOptions = {
    to: data.email,
    from: "noreply@medimny.com",
    subject: data.subject,
    html: data.body
  };

  if (data.attachments) {
    mailOptions.attachments = data.attachments
  }

  if (data.cc && data.cc.length > 0) {
    mailOptions.cc = data.cc;
  }

  smtpTransport.sendMail(mailOptions, function (err) {
    //callback(err, 'done');
    console.log('err', err)
    return true;
  });
}

const base64Upload = (req, res, path, base64Str) => {
  createDir(path);
  let ext = base64Str.substring(base64Str.indexOf('/') + 1, base64Str.indexOf(';base64'));
  picName = randomstring.generate({
    length: 8,
    charset: 'alphanumeric'
  });
  var optionalObj = { 'fileName': picName, 'type': ext };
  let newBase64Str = base64Str.replace(/(\r\n|\n|\r)/gm, "")
  let splitbase64 = newBase64Str.substring(newBase64Str.indexOf(',') + 1,newBase64Str.length);
  let base = newBase64Str.includes('base64');
  var Image;
  if( base && splitbase64 ){
    Image = base64ToImage(newBase64Str, path, optionalObj);
    return Image.fileName;
  }else{
    return 
  }
}

const base64UploadS3 = (req, res, path, base64Str) => {
  let ext = base64Str.substring(base64Str.indexOf('/') + 1, base64Str.indexOf(';base64'));
  picName = randomstring.generate({
    length: 8,
    charset: 'alphanumeric'
  });
  var filename = path + "/" + picName + "." + ext
  buf = new Buffer(base64Str.replace(/^data:image\/\w+;base64,/, ""), 'base64')
  var data = {
    Key: filename,
    Body: buf,
    ContentEncoding: 'base64',
    ContentType: 'image/jpeg'
  };
  s3Bucket.putObject(data, function (err, data) {
    if (err) {
    } else {
    }
  });
  return s3BaseUrl + filename;
}

const BufferUploadS3 = (path, buf) => {
  var data = {
    Key: path,
    Body: buf,
    ContentEncoding: 'UTF-8',
    ContentType: 'application/pdf'
  };
  s3Bucket.putObject(data, function (err, data) {
    if (err) {
    } else {
    }
  });
  return s3BaseUrl + path;
}

const deleteFromS3 = (filepath) => {
  file = filepath.replace(s3BaseUrl, "");
  var params = { Bucket: 'medideals.assets', Key: file };
  s3Bucket.deleteObject(params, function (err, data) {
    if (err) console.log(err, err.stack);  // error
    else console.log();                 // deleted
  });
}

const fileUpload = (req, res, dir, fileName, cb) => {
  var storage = multer.diskStorage({
    destination: function (req, file, callback) {

      if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mov') {
        createDir(dir);
        callback(null, dir);
      } else if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        createDir(dir);
        callback(null, dir);
      }
      else if (file.mimetype === 'application/pdf') {
        createDir(dir);
        callback(null, dir);
      } else {
        callback('Mime type not supported');
      }
    },
    filename: function (req, file, callback) {
      if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mov') {
        fileName = fileName + '.mp4';
        callback(null, fileName);
      } else if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        fileName = fileName + '.jpg';
        callback(null, fileName);
      } else if (file.mimetype === 'application/pdf') {
        fileName = fileName + '.pdf';
        callback(null, fileName);
      } else {
        callback('Mime type not supported filename')
      }
    }
  });
  var upload = multer({
    storage: storage
  }).single('files')
  upload(req, res, function (err) {
    if (err) {
      return cb(false, fileName, req, res);
    }
    else {
      return cb(true, fileName, req, res);
    }
  })
}


const createThumbnail = (t, path, cb) => {

  var ext = t.filename.split('.');

  thumb({
    prefix: '',
    suffix: '_small',
    source: `./uploads/${path}` + '/' + t.filename,
    destination: `./uploads/${path}`,
    width: 100,
    overwrite: true,
    concurrency: 4,
    basename: ext[0]
  }).then(function () {
  }).catch(function (e) {
  });

  thumb({
    prefix: '',
    suffix: '_medium',
    source: `./uploads/${path}` + '/' + t.filename,
    destination: `./uploads/${path}`,
    width: 600,
    overwrite: true,
    concurrency: 4,
    basename: ext[0]
  }).then(function () {
  }).catch(function (e) {
  });

  cb(t.filename);
}

const createDir = (targetDir) => {
  const path = require('path');
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : '';
  targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(parentDir, childDir);
    if (!fs.existsSync(curDir)) {
      fs.mkdirSync(curDir);
    }
    return curDir;
  }, initDir);
}

const writeFile = (errLog, folder) => {
  var updatedAt = new Date();
  var date = new Date(updatedAt.getTime() + moment.tz('Asia/Kolkata').utcOffset() * 60000);
  errLog += "########## \r\n";
  createDir(`./${folder}`);
  fs.appendFileSync(`./${folder}/${date.toISOString().slice(0, 10)}.txt`, errLog + "\r\n", function (err) {
    if (err) {
      return console.log(err);
    }
  });
}

const generateToken = (userData, cb) => {
  var token = jwt.sign({
    email: userData.userData ? userData.userData.email : userData.email,
    userId: userData.userData ? userData.userData._id : userData._id,
    user_type: userData.userData ? userData.userData.user_type : userData.user_type,
    isMobile: userData.isMobile ? userData.isMobile : false,
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60 * 1000),

  }, "medideals_03_09_19");
  cb(token)
}

const generateAdminToken = (userData, cb) => {
  var token = jwt.sign({
    email: userData.userData ? userData.userData.email : userData.email,
    userId: userData.userData ? userData.userData._id : userData._id,
    user_type: userData.userData ? userData.userData.user_type : userData.user_type,
    isMobile: userData.isMobile ? userData.isMobile : false,
    asAdmin: '1',
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60 * 1000),

  }, "medideals_03_09_19");
  cb(token)
}

const decodeToken = (token) => {
  var decoded = jwt.decode(token, "medideals_03_09_19");
  return decoded;
}

const permission = [
  { name: 'Dashboard', pathname: '/seller/dashboard', icon: 'zmdi-view-dashboard' },
  { name: 'Orders', pathname: '/seller/orders', icon: 'zmdi-truck' },
  { name: 'Inventory', pathname: '/seller/inventory', icon: 'zmdi-local-store' },
  { name: 'Settlements', pathname: '/seller/settlement', icon: 'zmdi-balance-wallet' },
  { name: 'Product Requests', pathname: '/seller/product-Request', icon: 'zmdi-collection-item' },
  { name: 'Staff Members', pathname: '/seller/staffs', icon: 'zmdi-account' },
  { name: 'Groups', pathname: '/seller/groups', icon: 'zmdi-accounts' },
  { name: 'Compliance Forms', pathname: '/seller/complaince-form', icon: 'zmdi-receipt' },
]
const adminPermission = [
  { name: 'Dashboard', pathname: '/admin/dashboard', icon: 'zmdi-view-dashboard' },
  { name: 'Metadata', icon: 'zmdi-assignment' },
  { name: 'Reports', icon: 'zmdi-file-text' },
  { name: 'Shortbook', pathname: '/admin/shortBook', icon: '' },
  { name: 'Product Bank', pathname: '/admin/medbank', icon: 'zmdi-balance' },
  { name: 'Product Requests', pathname: '/admin/productRequests', icon: 'zmdi-hourglass-alt' },
  { name: 'Settlements', pathname: '/admin/settlement', icon: 'zmdi-balance-wallet' },
  { name: 'Transactions', pathname: '/admin/transaction', icon: 'zmdi-money' },
  { name: 'Sellers', pathname: '/admin/sellers', icon: 'zmdi-account' },
  { name: 'Buyers', pathname: '/admin/buyers', icon: 'zmdi-account' },
  { name: 'Orders', pathname: '/admin/orders', icon: 'zmdi-truck' },
  { name: 'Inventory', pathname: '/admin/inventory', icon: 'zmdi-local-store' },
  { name: 'Promotions', pathname: '/admin/promotions', icon: 'zmdi-balance-wallet' },
  { name: 'Staff Members', pathname: '/admin/staffs', icon: 'zmdi-account' },
  { name: 'Groups', pathname: '/admin/groups', icon: 'zmdi-accounts' },
]

const sortAdminPermissionModule = (details, requestFrom, mainCb) => {
  console.log('details===', details[0], '****************************')
  console.log('details===', details)
  let sidebar = []
  let permissions = []
  details[0].permissions.map((val) => permissions.push(...val.action))
  async.eachOfSeries(adminPermission, function (data, key, cb) {
    let index;
    if (requestFrom === 'getPermissionModule') {
      index = details.findIndex((val) => val.name == data.name)
    } else {
      index = details[0].permissions.findIndex((val) => val.name.includes(data.name))
    }

    if (index > -1) {
      console.log('details===', details[index])

      // permissions.push(...details[index].action)
      if (requestFrom === 'getPermissionModule') {
        sidebar.push(details[index])
      } if (requestFrom === 'getSidbarContent') {

        sidebar.push(data)
      }
      cb()
    } else {
      cb()
    }
  }, function (err) {
    let actions = permissions.filter((item, pos) => permissions.indexOf(item) === pos)
    // console.log('actions', actions)
    mainCb(err, sidebar, actions);
  })
}

const sortPermissionModule = (details, requestFrom, mainCb) => {


  let sidebar = []
  if (requestFrom === 'getPermissionModule') {
    let index1 = details.findIndex((val) => val.name === 'Vacation')
    if (index1 > -1) {
      sidebar.push(details[index1])
    }

  }
  async.eachOfSeries(permission, function (data, key, cb) {
    let index;
    if (requestFrom === 'getPermissionModule') {
      index = details.findIndex((val) => val.name == data.name)
    } else {
      index = details[0].permissions.findIndex((val) => val.name == data.name)
    }

    if (index > -1) {
      if (requestFrom === 'getPermissionModule') {
        sidebar.push(details[index])
      } if (requestFrom === 'getSidbarContent') {
        sidebar.push(data)
      }
      cb()
    } else {
      cb()
    }
  }, function (err) {
    mainCb(err, sidebar);
  })
}

const sendNotification = (deviceId, flag, title, msg, payload, callback) => {

  payload.flag = flag;
  payload.title = 'Medimny';
  payload.vibrate = [100, 50, 100];
  payload.body = msg;
  payload.sound = "default";
  var message = {
    registration_ids: deviceId,
    priority: "high",
    forceshow: true, // required fill with device token or topics
    collapse_key: 'Medimny',
    content_available: true,
    data: payload,
    notification: {
      title: flag,
      body: msg,
      sound: "default"
    }
  };
  fcm.send(message)
    .then(function (response) {
      callback("success", response);
    })
    .catch(function (err) {
      callback("error", err);
    });
}

const getSellerBuyerId = (userType, cb) => {
  Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

  user.findOne({
    $and: [
      { user_type: userType },
      { mainUser: { $eq: null } }
    ]
  })
    .sort({ createdAt: -1 })
    .exec((err, userData) => {

      if (err) {
        return cb(err)
      } else if (!userData) {
        if (userType === 'seller') {
          return cb(null, `MDS00001`)
        }
        if (userType === 'buyer') {
          return cb(null, `MDB${process.env.mode === 'debug' ? 'D' : 'L'}00001`)
        }
      }
      else {
        if (userType === 'seller' && userData.sellerId !== undefined) {
          let tempNumString = parseInt(userData.sellerId.replace(/^\D+/g, ''));
          let sellerId = '';
          let incrementedId = parseInt(tempNumString) + 1;
          sellerId = `MDS`.concat((incrementedId).pad(5));
          return cb(null, sellerId);
        } else if (userType === 'buyer' && userData.buyerId !== undefined) {
          let tempNumString = parseInt(userData.buyerId.replace(/^\D+/g, ''));
          let buyerId = '';
          let incrementedId = parseInt(tempNumString) + 1;
          buyerId = `MDB${process.env.mode === 'debug' ? 'D' : 'L'}`.concat((incrementedId).pad(5));
          return cb(null, buyerId);
        } else {
          if (userType === 'seller') {
            return cb(null, `MDS00001`)
          }
          if (userType === 'buyer') {
            return cb(null, `MDB${process.env.mode === 'debug' ? 'D' : 'L'}00001`)
          }
        }
      }
    });
}


const getOrderId = (cb) => {
  Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }
  let orderId = ''
  let twoDigitsCurrentYear = parseInt(new Date().getFullYear().toString().substr(2, 2));

  orderId = 'OD'.concat(twoDigitsCurrentYear);
  order.findOne()
    .sort({ createdAt: -1 })
    .exec((err, orderData) => {
      if (err) {
        return cb(err)
      } else if (!orderData) {
        orderId = orderId.concat("0000001");
        return cb(null, orderId)
      }
      else {
        if (orderData.order_id !== undefined) {
          if (parseInt(new Date(orderData.createdAt).getFullYear().toString().substr(2, 2) > twoDigitsCurrentYear)) {
            let orderId = ''
            let twoDigitsCurrentYear = parseInt(new Date().getFullYear().toString().substr(2, 2));
            orderId = 'OD'.concat(twoDigitsCurrentYear);
            orderId = orderId.concat((1).pad(7));
            return cb(null, orderId);
          } else {
            let tempNumString = parseInt(orderData.order_id.replace(/^\D+/g, ''));
            let incrementedId = parseInt(tempNumString) + 1;
            orderId = "OD".concat((incrementedId).pad(7));
            return cb(null, orderId);
          }
        } else {
          orderId = orderId.concat("0000001");
          return cb(null, orderId);
        }
      }
    });
}

const monthArray = [{
  "month": "Jan",
  "orders": 0
}, {
  "month": "Feb",
  "orders": 0
}, {
  "month": "Mar",
  "orders": 0
}, {
  "month": "Apr",
  "orders": 0
}, {
  "month": "May",
  "orders": 0
}, {
  "month": "June",
  "orders": 0
}, {
  "month": "July",
  "orders": 0
}, {
  "month": "Aug",
  "orders": 0
}, {
  "month": "Sep",
  "orders": 0
}, {
  "month": "Oct",
  "orders": 0
}, {
  "month": "Nov",
  "orders": 0
}, {
  "month": "Dec",
  "orders": 0
}];

const getMainSellerId = (cb) => {
  groupModule.findOne({ name: "Seller Main" })
    .exec((error, groupData) => {
      cb(error, groupData)
    })
}

const updateInventoryOnRequest = (orderId, updatedProducts) => {
  order.find({ order_id: orderId })
    .exec((error, orderData) => {
      if (error) {
      } else {
        let data = updatedProducts.map((product, key) => {
          let index = orderData[0].products.findIndex((e) => e.inventory_id.toString() === product.inventory_id._id);
          if (index > -1) {
            product.quantity = Number(orderData[0].products[index].quantity) - Number(product.quantity)
            return product
          }
        }).filter((e) => e !== undefined);

        if (data && data.length > 0) {
          inventory.updateInventoryQuantity(data, 'cancel', (response) => {
          });
        }
      }
    })
}
const shipRokectLogin = (cb) => {
  axios({
    method: 'post',
    url: 'https://apiv2.shiprocket.in/v1/external/auth/login',
    header: 'Content-Type: application/json',
    data: {
      "email": process.env.shipment_email,
      "password": process.env.shipment_password
    }
  })
    .then((result) => {
      cb(result)
    })
    .catch((error) => {
      cb(error)
    })
}
const creditDepositRecieved = (userData, balance, status, orderId, unique_invoice) => {
  let transaction = [];
  transaction.user_id = userData._id;
  transaction.settle_amount = balance;
  transaction.paid_amount = balance;
  transaction.unique_invoice = unique_invoice;
  transaction.narration = "Amount credited for cancellation for order " + orderId;
  let paytmPayAmount = 0;
  transaction.status = 'Success';
  transaction.type = status;
  transactions.addTransaction(transaction, (error, transactionData) => {
    user.findOneAndUpdate({ _id: userData._id }, { $inc: { wallet_balance: balance } }, { new: true }).then((userData) => {

    })
  })
}
const rollBackOrderByUniqueInvoice = (unique_invoice) => {
  let orderId = [];
  order.find({ unique_invoice: unique_invoice }).then((result) => {
    if (result && result[0].requested != 'Cancelled') {
      async.eachOfSeries(result, function (ProcessedOrder, key, cb) {
        let qauery = {}
        orderId.push(ProcessedOrder.order_id);
        let statusData = {
          status: 'Cancelled',
          date: new Date(),
          description: 'Order has been cancelled due transaction failure.'
        }

        query = { $set: { paymentStatus: 'failed', requested: 'Cancelled', order_cancel_reason: 'Order has been cancelled due transaction failure.' }, $push: { order_status: statusData } }
        // order.findOneAndUpdate({ order_id: ProcessedOrder.order_id }, query, { new: true }).exec()
        order.updateMany({ unique_invoice: unique_invoice }, query).then(result => result)
        inventory.updateInventoryQuantity(ProcessedOrder.products, 'cancel', (response) => {
          cb()
        });
      }, function (err) {
        transactions.findOne({ unique_invoice: unique_invoice, type: 'Credit Used' }).then((resultResponse) => {
          if (resultResponse && resultResponse.type === 'Credit Used') {
            user.findOneAndUpdate({ _id: resultResponse.user_id }, { $inc: { wallet_balance: resultResponse.settle_amount } }, { new: true }).then((userData) => {
              let transaction = {}
              transaction.status = 'Success';
              transaction.narration = `Amount credited for cancellation for order: ${orderId.map(id => id).join(',')}`
              transaction.type = 'Credit Received'
              transaction.user_id = resultResponse.user_id;
              transaction.paid_amount = resultResponse.paid_amount;
              transaction.settle_amount = resultResponse.settle_amount;
              transaction.unique_invoice = unique_invoice;
              transaction.closing_bal = userData.wallet_balance;
              transactions.addTransaction(transaction, (error, transactionData) => {

              })
            })
          }
        })
      })
    }
  })
}
var generateLabel = async (req, cb) => {
  let order_id = req.query.order_id
  let order_details = await order.aggregate([
    {
      $match: {
        'order_id': order_id
      }
    },
    {
      $lookup: {
        localField: 'user_id',
        foreignField: '_id',
        from: 'users',
        as: 'buyer'
      }
    },
    {
      $unwind: {
        path: '$buyer',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        localField: 'buyer.user_state',
        foreignField: '_id',
        from: 'countries',
        as: 'stateB'
      }
    },
    {
      $unwind: {
        path: '$stateB',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        localField: 'seller_id',
        foreignField: '_id',
        from: 'users',
        as: 'seller'
      }
    },
    {
      $unwind: {
        path: '$seller',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        localField: 'seller.user_state',
        foreignField: '_id',
        from: 'countries',
        as: 'stateS'
      }
    },
    {
      $unwind: {
        path: '$stateS',
        preserveNullAndEmptyArrays: true
      }
    }
  ])
  // (query);

  //res.status(200).send("url");
  var contents = fs.readFileSync('./views/shipping_label.ejs', 'utf8');
  var OrderDetails = {
    base_url: process.env.base_url,
    order_id: req.query.order_id,
    title: "Shubham",

    buyerFName: order_details[0].buyer.first_name,
    buyerLName: order_details[0].buyer.last_name,
    buyerCity: order_details[0].buyer.user_city,
    buyerPin: order_details[0].buyer.user_pincode,
    buyerAddr: order_details[0].buyer.user_address,
    buyerPhone: order_details[0].buyer.phone,
    buyerComp: order_details[0].buyer.company_name,

    sellerFName: order_details[0].seller.first_name,
    sellerLName: order_details[0].seller.last_name,
    sellerCity: order_details[0].seller.user_city,
    sellerPin: order_details[0].seller.user_pincode,
    sellerAddr: order_details[0].seller.user_address,
    sellerPhone: order_details[0].seller.phone,
    sellerComp: order_details[0].seller.company_name,

    paymentType: order_details[0].isBulk ? 'Bulk Prepaid' : order_details[0].paymentType === 'COD' ? 'COD' : 'Prepaid',
    totalAmt: req.body.amount ? req.body.amount : order_details[0].total_amount,
    length: order_details[0].length,
    breadth: order_details[0].breadth,
    height: order_details[0].height,
    invoiceDate: moment(order_details[0].createdAt).format("DD-MM-YYYY"),
    awbNo: req.query.awb ? req.query.awb : "abcdefg123",
    serviceName: req.query.serviceName,
    weight: req.body.weight,

    gst: order_details[0].seller.gstLic ? order_details[0].seller.gstLic.lic ? order_details[0].seller.gstLic.lic : '' : '',
    invoiceNo: req.body.invoiceNumber,

    stateS: order_details[0].stateS.name,
    stateB: order_details[0].stateB.name,
    // sellerCountry: order_details[0].seller.user_country,
    // buyerCountry: order_details[0].buyer.user_country


  }
  var html = ejs.render(contents, OrderDetails);
  pdf.create(html).toBuffer(async function (err, buffer) {
    let url = await BufferUploadS3("order_labels/" + req.query.order_id + ".pdf", buffer);
    cb(url);
  });

}
const delhivaeryUpdateStatus = () => {
  let start_date = new Date();
  start_date.setDate(start_date.getDate() - 31);
  let query = {
    is_deleted: false,
    serviceName: 'Delhivery',
    requested: { $ne: 'Delivered'},
    $and: [{ 'order_status.status': 'Processed' }, { 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'RTO' } }, { 'order_status.status': { $ne: 'Delivered' } }],
    createdAt: { $gte: start_date }
  }
  order.aggregate([
    { $match: query },
    {
      $lookup: {
        localField: 'user_id',
        foreignField: '_id',
        from: 'users',
        as: 'User'
      }
    },
    { $unwind: '$User' },
    //{"$limit": 20 }
  ]).then((result) => {
    async.eachOfSeries(result, function (data, key, cb) {
      var options = {
        'method': 'GET',
        'url': `https://track.delhivery.com/api/v1/packages/json/?waybill=${data.shipment_id}&verbose=2&token=${process.env.delhiveryToken}`,
        'headers': {
        }
      };
      request(options, function (error, response) {
        if (error) {
          return cb()
        }
        let data1 = JSON.parse(response.body)
        if (data1.Error) {
          return cb()
        }
        if ((data.order_status[data.order_status.length - 1].status != data1.ShipmentData[0].Shipment.Status.Status) || (data.order_status[data.order_status.length - 1].description != data1.ShipmentData[0].Shipment.Status.Instructions)) {
          let orderStatus = {
            status: data1.ShipmentData[0].Shipment.Status.Status,
            description: `${data1.ShipmentData[0].Shipment.Status.Instructions}(${data1.ShipmentData[0].Shipment.Status.StatusLocation})`,
            date: new Date(data1.ShipmentData[0].Shipment.Status.StatusDateTime)
          }
          let updatedQuery = { $push: { order_status: orderStatus } }
          let status1 = data1.ShipmentData[0].Shipment.Status.Status.toLowerCase().trim();
          if( status1.includes('delivered') ){
            updatedQuery['requested'] = 'Delivered';
            let date = data1.ShipmentData[0].Shipment.Status.StatusDateTime;
            insertSettlement(data._id,date);
          }
          updateOrder(data, updatedQuery, orderStatus, cb);
        } else {
          cb()
        }
      });
    }, function () {
      return true;
    });
  })
}
const shipRocketUpdateStatus = () => {
  let start_date = new Date();
  start_date.setDate(start_date.getDate() - 31);
  let query = {
    is_deleted: false,
    serviceName: 'shiprocket',
    requested: { $ne: 'Delivered'},
    $and: [{ 'order_status.status': 'Processed' }, { 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } }],
    createdAt: { $gte: start_date }
  }
  shipRokectLogin(async (loginResult) => {
    if (loginResult.status == 200) {
      let shipToken = loginResult.data.token
      order.aggregate([
        { $match: query },
        {
          $lookup: {
            localField: 'user_id',
            foreignField: '_id',
            from: 'users',
            as: 'User'
          }
        },
        { $unwind: '$User' }
      ]).then((results) => {
        async.eachOfSeries(results, function (data, key, cb) {
          request.get({
            url: `https://apiv2.shiprocket.in/v1/external/courier/track/shipment/${data.shipment_id}`,
            json: true,
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${shipToken}`
            }
          }, function (error1, shipUserResponse, shipUserBody) {
            if (shipUserBody != undefined && shipUserBody.tracking_data != undefined && !shipUserBody.tracking_data.error) {

              if ((data.order_status[data.order_status.length - 1].status != shipUserBody.tracking_data.shipment_track_activities[0].status) && getStatus(data.order_status[data.order_status.length - 1].status) !== 'Delivered') {
                let orderStatus = {
                  status: getStatus(shipUserBody.tracking_data.shipment_track_activities[0].status),
                  description: shipUserBody.tracking_data.shipment_track_activities[0].activity,
                  date: new Date(shipUserBody.tracking_data.shipment_track_activities[0].date)
                }
                let updatedQuery = { $push: { order_status: orderStatus } }

                
                updateOrder(data, updatedQuery, orderStatus, cb);

              } else {
                if (data.order_status[data.order_status.length - 1].status == 'EOD-38' || data.order_status[data.order_status.length - 1].status == 'EOD-135' || data.order_status[data.order_status.length - 1].status == 'delivered' || data.order_status[data.order_status.length - 1].status == '000') {
                  order.findOne({ order_id: data.order_id }).then((findres) => {
                    if (findres) {
                      findres.order_status[findres.order_status.length - 1].status = 'Delivered'
                      findres.requested = 'Delivered'
                      let date = shipUserBody.tracking_data.shipment_track_activities[0].date;
                      insertSettlement(data._id,date);
                      findres.save().then((saved) => {
                        cb()
                      })
                    } else {
                      cb()
                    }
                  })
                } else {
                  cb()
                }


              }

            } else {
              cb()
            }

          })
        }, function () {
          return true;
        });
      })
    }
  })
}
const updateOrder = (data, query, orderStatus, cb) => {
  order.findOneAndUpdate({ order_id: data.order_id }, query, { new: true }).exec(async (error, ProcessedOrder) => {
    let notificationTitle = `Order Status updated to ${orderStatus.status}`;
    let notificationMsg = orderStatus.description;
    let devicetoken = data.User.device_token;
    let flag = 'Order';
    let payload = {};
    sendNotification(devicetoken, flag, notificationTitle, notificationMsg, payload, () => {
    });

    notification.addNotification(notificationMsg, data.User.device_token, '', flag, (error, response) => {
    })
    let msgBody = `Order Delivered: Your order on MEDIMNY ${data.order_id} delivered. For any issue please mail us at helpdesk@medimny.com referring ${data.order_id} within two days.`;
    sendSMS(data.User.phone, msgBody);

    cb()
  })
}

const getCommission = async(data, mainCb) => {
  let coolChainData = await mediType.findOne({ name: 'Cool chain' }).then((result) => result);
  let ethicalData = await mediType.findOne({ name: 'Ethical branded' }).then((result) => result);
  let otcData = await mediType.findOne({ name: 'OTC' }).then((result) => result);
  let surgicalData = await mediType.findOne({ name: 'Surgical' }).then((result) => result);
  let otherData = await mediType.findOne({ name: 'Others' }).then((result) => result);
  let genericData = await mediType.findOne({ name: 'Generic' }).then((result) => result);
  let mediComm = 0;
  let prodComm = 0;
  let sellerCommPer = data.seller_id && data.seller_id.additionalComm ? data.seller_id.additionalComm : 0;
  let bulkCommissionData = await systemCon.findOne({key:'bulk_commission'}).exec();
  let sellerComm = data.total_amount * sellerCommPer/100;
  let mediData;
  if(data.orderType){
    mediData = await mediType.findOne({ name: data.orderType }).then((result) => result);
  }else{
    mediData = await mediType.findOne({ name: 'Ethical branded' }).then((result) => result);
  }

  let bulkValue = mediData ? mediData.commission - (bulkCommissionData.value * 100) : 0;

  mediComm = (data.isBulk === true) ? (data.total_amount * (bulkValue)/100 ) : 
  (data.total_amount * mediData.commission/100 );

  const getComm = (valueFromFn) => {
    if (valueFromFn) {
      if (valueFromFn.toLowerCase() == 'cool chain') {
        return data.isBulk === true ? (coolChainData.commission - (bulkCommissionData.value * 100)) : coolChainData.commission
      } else if (valueFromFn.toLowerCase() == 'ethical branded') {
        return data.isBulk === true ? (ethicalData.commission - (bulkCommissionData.value * 100)) : ethicalData.commission
      } else if (valueFromFn.toLowerCase() == 'generic') {
        return data.isBulk === true ? (genericData.commission - (bulkCommissionData.value * 100)) : genericData.commission
      } else if (valueFromFn.toLowerCase() == 'surgical') {
        return data.isBulk === true ? (surgicalData.commission - (bulkCommissionData.value * 100)) : surgicalData.commission
      } else if (valueFromFn.toLowerCase() == 'otc') {
        return data.isBulk === true ? (otcData.commission - (bulkCommissionData.value * 100)) : otcData.commission
      } else if (valueFromFn.toLowerCase() == 'others') {
        return data.isBulk === true ? (otherData.commission - (bulkCommissionData.value * 100)) : otherData.commission
      }
    }
  }
  let twelveJuly = new Date(moment().format('2021-07-14')).getTime();
  let created = new Date(moment(data.createdAt)).getTime();

  async.eachOfSeries(data.products, (data, key, cb) => {
    if(data){
      let temp = data.discount_on_product && data.quantity / data.discount_on_product.purchase;
      temp = data.discount_on_product && temp * data.discount_on_product.bonus;
      let value = data.discount_name && (data.discount_name == 'Same' || data.discount_name == 'SameAndDiscount') ?
        Number(Number(((Number(data.quantity) + Number(temp)) * data.ePTR) + (((Number(data.quantity) + Number(temp)) * data.ePTR) * Number(data.GST) / 100)).toFixed(2)) :
        Number(Number((Number(data.quantity)  * data.ePTR) + ((Number(data.quantity)  * data.ePTR) * Number(data.GST)/100 )).toFixed(2));

      inventory.findById(data.inventory_id).then(data1 => {
        // mediComm += value * getComm(data1.medicineTypeName)/100;
          if( created > twelveJuly ){
            let surchg = data.surCharge ? data.surCharge/100 : 0;
            prodComm += Number(value) * surchg;
          }
          cb();
      })
    }
  }, function(err) {
    let obj = {
      prod_surge_comm: prodComm,
      user_comm: sellerComm,
      medi_type_comm: Number(Number(mediComm).toFixed(2))
    }
    mainCb(obj);
  })
}

const insertSettlement = async(data,date) => {
  let tcsData = await systemCon.findOne({key:'tcs'}).exec();
  let tdsData = await systemCon.findOne({key:'tds'}).exec(); 
  let payment_date = new Date(date); 
  let settleData = await Settlements.findOne({ order_id: ObjectId(data) }).exec();
  if(!settleData){
    Orders.findOne({ _id: ObjectId(data) }).populate('seller_id').then(async (order) => {
      getCommission(order, (dataFromFn) => {
        let { user_comm, medi_type_comm } = dataFromFn;
        let totalComm = user_comm + medi_type_comm;
        let ns = new Settlements({
              order_id: order._id,
              seller_id: order.seller_id._id,
              invoice_id: order.invoice_number,
              order_value: order.total_amount,
              tds: Number((order.total_amount * Number(tdsData.value)).toFixed(2)),
              tcs: Number((order.total_amount * Number(tcsData.value)).toFixed(2)),
              commission: totalComm,
              net_amt: order.total_amount - ((order.total_amount * Number(tdsData.value)) + (order.total_amount * Number(tcsData.value)) + totalComm + dataFromFn.prod_surge_comm),
              payment_due_date: payment_date,
              transaction_id: "",
              commission_comp: dataFromFn
            })

        ns.save();
        let query = { settlement: "Eligible" }
        Orders.findOneAndUpdate({ _id: ObjectId(order._id) }, query, { new: true }).exec();
      })  
    })
  }
}
const getStatus = (data) => {
  switch (data) {
    case "DLVD":
    case "delivered":
    case "EOD-38":
    case "EOD-135":
    case "000":
      return 'Delivered'
    default:
      return data
  }
}
const getOtp = (cb) => {
  var token = randomstring.generate({
    length: 6,
    charset: 'numeric'
  });
  cb(null, token);
}

const getRemittanceId = (data,cb) => {
  Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
  }

  groupSettlement.findOne({ seller_id: ObjectId(data) }).sort({ createdAt: -1 }).exec((err, remiData) => {
    if (err) {
      return cb(err)
    } else if (!remiData) {
        return cb(null,`SRMT01`)
    }
    else {
      if (remiData.remittanceId !== undefined) {
        let tempNumString = parseInt(remiData.remittanceId.replace(/^\D+/g, ''));
        let remittanceId = '';
        let incrementedId = parseInt(tempNumString) + 1;
        remittanceId = `SRMT`.concat((incrementedId).pad(2));
        return cb(null, remittanceId);
      } else {
          return cb(null, `SRMT01`)
      }
    }
  });
}

var generateLabel1 = async (req, res) => {
  let order_id = req.query.orderId;
  let order_details = await order.aggregate([
    {
      $match: {
        'order_id': order_id
      }
    },
    {
      $lookup: {
        localField: 'user_id',
        foreignField: '_id',
        from: 'users',
        as: 'buyer'
      }
    },
    {
      $unwind: {
        path: '$buyer',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        localField: 'buyer.user_state',
        foreignField: '_id',
        from: 'countries',
        as: 'stateB'
      }
    },
    {
      $unwind: {
        path: '$stateB',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        localField: 'seller_id',
        foreignField: '_id',
        from: 'users',
        as: 'seller'
      }
    },
    {
      $unwind: {
        path: '$seller',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        localField: 'seller.user_state',
        foreignField: '_id',
        from: 'countries',
        as: 'stateS'
      }
    },
    {
      $unwind: {
        path: '$stateS',
        preserveNullAndEmptyArrays: true
      }
    }
  ])
  // (query);

  //res.status(200).send("url");
  var contents = fs.readFileSync('./views/shipping_label1.ejs', 'utf8');
  var OrderDetails = {
    base_url: process.env.base_url,
    order_id: order_id,
    title: "Docket",

    buyerFName: order_details[0].buyer.first_name,
    buyerLName: order_details[0].buyer.last_name,
    buyerCity: order_details[0].buyer.user_city,
    buyerPin: order_details[0].buyer.user_pincode,
    buyerAddr: order_details[0].buyer.user_address,
    buyerPhone: order_details[0].buyer.phone,
    buyerComp: order_details[0].buyer.company_name,

    sellerFName: order_details[0].seller.first_name,
    sellerLName: order_details[0].seller.last_name,
    sellerCity: order_details[0].seller.user_city,
    sellerPin: order_details[0].seller.user_pincode,
    sellerAddr: order_details[0].seller.user_address,
    sellerPhone: order_details[0].seller.phone,
    sellerComp: order_details[0].seller.company_name,

    paymentType: order_details[0].isBulk ? 'Bulk Prepaid' : order_details[0].paymentType === 'COD' ? 'COD' : 'Prepaid',
    totalAmt: req.body.amount ? req.body.amount : order_details[0].total_amount,
    length: order_details[0].length,
    breadth: order_details[0].breadth,
    height: order_details[0].height,
    invoiceDate: moment(order_details[0].createdAt).format("DD-MM-YYYY"),
    awbNo: order_details[0].shipment_id ? order_details[0].shipment_id : "abcdefg123",
    serviceName: order_details[0].serviceName,
    weight: Number(order_details[0].weight),

    gst: order_details[0].seller.gstLic ? order_details[0].seller.gstLic.lic ? order_details[0].seller.gstLic.lic : '' : '',
    invoiceNo: order_details[0].seller_invoice,

    stateS: order_details[0].stateS.name,
    stateB: order_details[0].stateB.name,
    // sellerCountry: order_details[0].seller.user_country,
    // buyerCountry: order_details[0].buyer.user_country


  }
  var html = ejs.render(contents, OrderDetails);
  res.status(200).send(html)
  // pdf.create(html).toBuffer(async function (err, buffer) {
  //   console.log('foafmapf',buffer,'foanfiaf',err)
    // let url = await BufferUploadS3("order_labels/" + req.query.order_id + ".pdf", buffer);
    // cb(url);
  // });

}
module.exports = {generateLabel1,
  createDir,
  writeFile,
  timezone,
  generateToken,
  createThumbnail,
  fileUpload,
  sendSMS,
  sendEmail,
  base64Upload,
  base64UploadS3,
  BufferUploadS3,
  deleteFromS3,
  decodeToken,
  sortPermissionModule,
  sendNotification,
  getSellerBuyerId,
  getOrderId,
  monthArray,
  getMainSellerId,
  updateInventoryOnRequest,
  shipRokectLogin,
  sortAdminPermissionModule,
  creditDepositRecieved,
  rollBackOrderByUniqueInvoice,
  Autofillwords,
  Autofillwordsfn,
  generateLabel,
  delhivaeryUpdateStatus,
  shipRocketUpdateStatus,
  getOtp,
  generateAdminToken,
  insertSettlement,
  getCommission,
  getRemittanceId
}
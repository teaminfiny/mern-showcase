
const { body } = require('express-validator/check');
let fs = require('fs');
const request = require('request')
const user = require('../models/user')
const moment = require('moment')
const helper = require('./helper')
const verifyavailabiltyOfZpcode = (type, delivery_codes) => {
    switch (type) {
        case 'COD':
            return delivery_codes.postal_code.cod === 'Y' ? true : false

        case 'Online':
            return delivery_codes.postal_code.pre_paid === 'Y' ? true : false
    }
}
const delhiveryService = (req, res, cb) => {
    try {
        let url = 'https://track.delhivery.com/'
        let token = process.env.delhiveryToken
        var options1 = {
            'method': 'GET',
            'url': `${url}c/api/pin-codes/json?token=${token}&filter_codes=${(req.body.pincode).toString()}`,

        };

        request(options1, function (error, response) {

            if (error) {
                cb({ error: true, title: error })
            }
            fs.writeFile('delhiveryService.txt', response.body, err => {
                if (err){ console.log('delhiveryService-=-=-=-=error',err); }
            });
            let availablePincode = JSON.parse(response.body)
            if (availablePincode.delivery_codes.length == 0) {
                return cb({ error: true, title: 'Delivery not available', moveNext: true })
            }
            let isDelivery = verifyavailabiltyOfZpcode(req.body.type, availablePincode.delivery_codes[0])
            if (!isDelivery) {
                return cb({ error: true, title: 'Delivery not available', moveNext: true })
            }
            
            let options = {
                'method': 'POST',
                'url': `${url}api/backend/clientwarehouse/create/`,
                'headers': {
                    'Authorization': `Token ${token}`,
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({ "phone": req.user.phone, "city": req.user.user_city.replace(/&/gi, ' and '), "name": req.user.sellerId, "pin": req.user.user_pincode, "address": req.user.user_address.replace(/&/gi, ' and '), "country": "India", "email": req.user.email, "registered_name": req.user.sellerId, "return_address": req.user.user_address.replace(/&/gi, ' and '), "return_pin": req.user.user_pincode, "return_city": req.user.user_city.replace(/&/gi, ' and '), "return_state": req.body.sellerState, "return_country": "India" })

            };

            if (req.user.isDeliveryRegistered === true) {
                createOrderDL(req, res, function (data) {
                    cb({ error: false, url: data.url, awb: data.awb, serviceName: data.serviceName, query: data.query })
                })
            } else {
                request(options, (error, response1) => {
                    let registeredDeliveryData = JSON.parse(response1.body)
                    if ((registeredDeliveryData.success == true) || (registeredDeliveryData.error.length > 0 && (registeredDeliveryData.error).some(substring => substring.includes('already exists'))) || ((registeredDeliveryData.error).includes('already exists'))) {
                        // if (false) {
                        user.findByIdAndUpdate({ _id: req.user._id }, { $set: { isDeliveryRegistered: true } }).then(result => result)
                        createOrderDL(req, res, function (data) {
                            cb(data)
                        })
                    } else {
                        var mailData = {
                            email: 'avanish.m@infiny.in',
                            subject: 'Delhivery Error',
                            body: `<pre>${options.body}${registeredDeliveryData.error}</pre>`
                        };
                        helper.sendEmail(mailData);
                        return res.status(200).json({
                            error: true,
                            title: 'Getting issue while registering this user to delivery'
                        })
                    }
                });

            }
        })


    } catch (error) {
        res.status(200).json({
            error: true,
            title: 'Getting issue while registering this user to delivery',
            details: error
        })
    }
}

const createOrderDL = (req, res, cb) => {
    let url = 'https://track.delhivery.com/'
    let token = process.env.delhiveryToken
    let shipments = [{
        add: req.body.address.replace(/&/gi, ' and '),
        phone: req.body.phone,
        payment_mode: req.body.type,
        name: req.body.company.replace(/&/gi, ' and '),
        pin: req.body.pincode,
        order: req.body.orderId,
        consignee_gst_amount: '',
        integrated_gst_amount: '',
        ewbn: '',
        consignee_gst_tin: '',
        seller_gst_tin: '',
        client_gst_tin: '',
        hsn_code: '',
        gst_cess_amount: '',
        client: 'MEDIDEALS SURFACE',
        tax_value: '',
        seller_tin: '',
        seller_gst_amount: '',
        seller_inv: '',
        city: '',
        commodity_value: '',
        weight: '',
        return_state: '',
        document_number: '',
        od_distance: '',
        sales_tax_form_ack_no: '',
        document_type: '',
        seller_cst: '',
        seller_name: req.user.sellerId,
        return_city: '',
        return_phone: '',
        product_quantity: '',
        category_of_goods: '',
        cod_amount: req.body.amount,
        shipment_width: '',
        document_date: '',
        taxable_amount: '',
        products_desc: '',
        state: req.body.state,
        dangerous_good: 'false',
        waybill: '',
        consignee_tin: '',
        order_date: moment(req.body.createdAt).format('YYYY-MM-DD'),
        total_amount: req.body.amount,
        seller_add: req.user.address,

    }]
    let data = JSON.stringify({
        shipments,
        pickup_location: {
            name: req.user.sellerId,
            city: req.user.city,
            pin: req.user.pincode,
            country: 'India',
            phone: req.user.phone,
            add: req.user.address
        }
    })
    var options4 = {
        'method': 'POST',
        'url': `${url}api/cmu/create.json`,
        'headers': {
            'Authorization': `Token ${token}`,
            'Content-Type': 'application/json'
        },
        'body': 'format=json&data=' + data

    };
    request(options4, function (error, response5) {
        let createResponse = JSON.parse(response5.body)

        if (createResponse.success == true) {
            req.query.order_id = req.body.orderId;
            req.query.awb = createResponse.packages[0].waybill
            req.query.serviceName = 'Delhivery'
            let orderStatus = {
                status: 'Processed',
                description: `Order has been Processed, Old: ${Number(req.body.oldAmt).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, New: ${Number(req.body.newAmt).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}`,
                date: new Date()
            }
            let dir = './assets/invoice/';
            helper.createDir(dir);
            helper.generateLabel(req, function (url) {
                let query = {
                    $set: {
                        shipment_id: createResponse.packages[0].waybill, serviceName: 'Delhivery',
                        uploadedInvoice: req.body.uploadedInvoice ? helper.base64Upload(req, res, dir, req.body.uploadedInvoice) : '',
                        total_amount: Number(req.body.newAmt),
                        process_date: req.body.selectedDate, seller_invoice: req.body.invoiceNumber, weight: req.body.weight, requested: "Processed", docket: url
                    }, $push: { order_status: orderStatus }
                }
                cb({ error: false, url, awb: createResponse.packages[0].waybill, serviceName: req.query.serviceName, query: query })
            })
        } else {
            var mailData = {
                email: 'avanish.m@infiny.in',
                subject: 'Delhivery Error Create Order',
                body: `<pre>${options4.body}${createResponse.rmk},${createResponse.packages}</pre>`
            };
            helper.sendEmail(mailData);
            res.status(200).json({
                error: true,
                title: createResponse.rmk,
                data: createResponse.packages
            })
        }

    })
}
const shiprocket = (req, res, cb) => {
    helper.shipRokectLogin((loginResult) => {
        if (loginResult.status == 200) {
            let shipToken = loginResult.data.token
            let orderData = {}
            orderData.order_id = req.body.orderId
            orderData.order_date = moment(req.body.createdAt).format("YYYY-MM-DD hh:mm")
            orderData.pickup_location = req.user.sellerId
            orderData.billing_customer_name = req.body.company
            orderData.billing_last_name = " "
            orderData.billing_address = req.body.address
            orderData.billing_city = req.body.user_city
            orderData.billing_state = req.body.state
            orderData.billing_country = 'India'
            orderData.billing_email = req.body.email
            orderData.billing_pincode = req.body.pincode
            orderData.billing_phone = req.body.phone
            orderData.shipping_is_billing = 1
            orderData.order_items = req.body.orderItems
            orderData.weight = req.body.weight
            orderData.sub_total = req.body.totalAmount
            orderData.payment_method = req.body.type === 'COD' ? 'COD' : 'Prepaid'
            orderData.length = req.body.length
            orderData.breadth = req.body.breadth
            orderData.height = req.body.height


            request.post({
                url: 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
                form: orderData,
                json: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${shipToken}`
                }
            }, function (error, response, body) {
                if (!body.message) {

                    request.get({
                        url: `https://apiv2.shiprocket.in/v1/external/courier/serviceability?Content-Type=application/json&pickup_postcode=${body.shipment_id}&order_id=${body.order_id}&cod=${req.body.type ? 0 : 1}&weight=${req.body.weight}`,
                        form: orderData,
                        json: true,
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${shipToken}`
                        }
                    }, function (error1, shipUserResponse, shipUserBody) {

                        if (!shipUserBody.message) {
                            let company_id = shipUserBody.data.recommended_courier_company_id

                            request.post({
                                url: `https://apiv2.shiprocket.in/v1/external/courier/assign/awb`,
                                form: {
                                    "shipment_id": body.shipment_id,
                                    "courier_id": company_id
                                },
                                json: true,
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${shipToken}`
                                }
                            }, function (error, awbResponse, awbBody) {
                                if (!awbBody.message) {
                                    request.post({
                                        url: `https://apiv2.shiprocket.in/v1/external/courier/generate/pickup`,
                                        form: {
                                            "shipment_id": body.shipment_id,
                                            "courier_id": company_id
                                        },
                                        json: true,
                                        headers: {
                                            'Content-Type': 'application/json',
                                            'Authorization': `Bearer ${shipToken}`
                                        }
                                    }, function (error, generatedResponse, generatedBody) {
                                        if (!generatedBody.message) {
                                            request.post({
                                                url: `https://apiv2.shiprocket.in/v1/external/courier/generate/label`,
                                                form: {
                                                    "shipment_id": [body.shipment_id],
                                                    //"courier_id": company_id
                                                },
                                                json: true,
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                    'Authorization': `Bearer ${shipToken}`
                                                }
                                            }, function (error, manifestResponse, manifestBody) {

                                                if (manifestBody.message) {
                                                    return res.status(200).json({
                                                        error: false,
                                                        message: manifestBody.message
                                                    })
                                                }
                                                let orderStatus = {
                                                    status: 'Processed',
                                                    description: `Order has been Processed, Old: ${Number(req.body.oldAmt).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, New: ${Number(req.body.newAmt).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}`,
                                                    date: new Date()
                                                }
                                                let dir = './assets/invoice/';
                                                helper.createDir(dir);
                                                let query = {
                                                    $set: {
                                                        ship_order_id: body.order_id, shipment_id: body.shipment_id, serviceName: 'shiprocket',
                                                        uploadedInvoice: req.body.uploadedInvoice ? helper.base64Upload(req, res, dir, req.body.uploadedInvoice) : '',
                                                        total_amount: Number(req.body.newAmt),
                                                        process_date: req.body.selectedDate, seller_invoice: req.body.invoiceNumber, weight: req.body.weight, requested: "Processed", docket: manifestBody.label_url
                                                    }, $push: { order_status: orderStatus }
                                                }
                                                cb(query)

                                            })
                                        } else {
                                            return res.status(200).json({
                                                title: awbBody.message,
                                                error: true
                                            });
                                        }
                                    })
                                } else {
                                    return res.status(200).json({
                                        title: awbBody.message,
                                        error: true
                                    });
                                }

                            })
                        } else {
                            return res.status(200).json({
                                title: shipUserBody.message,
                                error: true
                            });
                        }
                    })


                } else {
                    return res.status(200).json({
                        title: body.message,
                        error: true
                    });
                }


            });
        } else {
            return res.status(200).json({
                title: 'Something went wrong from Shipment user. Please try again or contact to admin.',
                error: true
            });
        }
    })
}
const dotZot = (req, res, cb) => {
    var options = {
        'method': 'POST',
        'url': 'https://instacom.dotzot.in/restservice/pushorderdataservice.svc/pushorderdata_pudo_gst',
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "Customer": { "CUSTCD": "CC000101634" },
            "DocketList": [{
                "AgentID": "",
                "AwbNo": "", "Breath": "1",
                "CPD": "16/07/2020", "CollectableAmount": req.body.type == 'COD' ? (parseInt(req.body.totalAmount)).toString() : '0',
                "Consg_Number": req.body.orderId, "Consolidate_EW": "12345",
                "CustomerName": req.body.company,
                "Ewb_Number": "", "GST_REG_STATUS": "Y",
                "HSN_code": "02314h03", "Height": "1",
                "Invoice_Ref": req.body.orderId, "IsPudo": "N",
                "ItemName": "Medicine", "Length": "1",
                "Mode": req.body.type == 'COD' ? 'C' : 'P', "NoOfPieces": "1",
                "OrderConformation": "Y", "OrderNo": req.body.orderId,
                "ProductCode": "00123", "PudoId": "", "REASON_TRANSPORT": "",
                "RateCalculation": "N", "Seller_GSTIN": "123223H2",
                "ShippingAdd1": req.body.address, "ShippingAdd2": req.body.city,
                "ShippingCity": req.body.city, "ShippingEmailId": req.body.email,
                "ShippingMobileNo": (req.body.phone).toString(), "ShippingState": req.body.state,
                "ShippingTelephoneNo": (req.body.phone).toString(), "ShippingZip": (req.body.pincode).toString(),
                "Shipping_GSTIN": "H212hf33", "TotalAmount": (parseInt(req.body.totalAmount)).toString(),
                "TransDistance": "20", "TransporterID": "TID0123",
                "TransporterName": "DTDC Courier Cargo", "TypeOfDelivery": "Home Delivery",
                "TypeOfService": "Express", "UOM": "Per KG", "VendorAddress1": req.user.user_address,
                "VendorAddress2": req.user.user_city, "VendorName": req.user.company_name,
                "VendorPincode": req.user.user_pincode, "VendorTeleNo": req.user.phone,
                "Weight": req.body.weight
            }]
        })
    };
    let temp = JSON.stringify({ "Customer": { "CUSTCD": "CC000101634" }, "DocketList": [{ "AgentID": "", "AwbNo": "", "Breath": "1", "CPD": "16/07/2020", "CollectableAmount": "2999", "Consg_Number": "AB123X2014654", "Consolidate_EW": "12345", "CustomerName": "Abhishek", "Ewb_Number": "", "GST_REG_STATUS": "Y", "HSN_code": "02314h03", "Height": "1", "Invoice_Ref": "AB123X2014654", "IsPudo": "N", "ItemName": "Iphone X 246 gb black", "Length": "1", "Mode": "C", "NoOfPieces": "1", "OrderConformation": "Y", "OrderNo": "AB123X2014654", "ProductCode": "00123", "PudoId": "", "REASON_TRANSPORT": "", "RateCalculation": "N", "Seller_GSTIN": "123223H2", "ShippingAdd1": "Plot number 14 DTDC Express limited", "ShippingAdd2": "GOregaon East", "ShippingCity": "Mumbai", "ShippingEmailId": "Abhishek.pandey@dotzot.in", "ShippingMobileNo": "8433985578", "ShippingState": "Maharashtra", "ShippingTelephoneNo": "0224009038", "ShippingZip": "400063", "Shipping_GSTIN": "H212hf33", "TotalAmount": "2999", "TransDistance": "20", "TransporterID": "TID0123", "TransporterName": "DTDC Courier Cargo", "TypeOfDelivery": "Home Delivery", "TypeOfService": "Express", "UOM": "Per KG", "VendorAddress1": "Plot number 98", "VendorAddress2": "Pump House", "VendorName": "ABC ltd", "VendorPincode": "400063", "VendorTeleNo": "8433985578", "Weight": "0.150" }] })

    request(options, function (error, response) {
        let responseData = JSON.parse(response.body)
        if (responseData[0].Succeed == 'Yes') {
            req.query.order_id = req.body.orderId;
            req.query.awb = responseData[0].DockNo
            req.query.serviceName = 'Delhivery'
            let orderStatus = {
                status: 'Processed',
                description: 'Order has been Processed',
                date: new Date()
            }
            let dir = './assets/invoice/';
            helper.createDir(dir);
            helper.generateLabel(req, function (url) {
                let query = {
                    $set: {
                        shipment_id: responseData[0].DockNo, serviceName: 'dotzot',
                        uploadedInvoice: req.body.uploadedInvoice ? helper.base64Upload(req, res, dir, req.body.uploadedInvoice) : '',
                        total_amount: Number(req.body.newAmt),
                        process_date: req.body.selectedDate, seller_invoice: req.body.invoiceNumber, weight: req.body.weight, requested: "Processed", docket: url
                    }, $push: { order_status: orderStatus }
                }
                cb(query)
            })
        }
    });

}
module.exports = {
    delhiveryService,
    shiprocket,
    dotZot
}
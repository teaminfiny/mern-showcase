var config = require("./config");
var moment = require('moment-timezone');
var jwt = require('jsonwebtoken');
var helper = require("../lib/helper");
let user = require('../models/user');
let GroupModule = require('../models/permssionGroup')

const isAuthenticated = function (data) {
  return async(req, res, next) => {
    let decoded = await jwt.verify(req.headers.token, 'medideals_03_09_19',function(err,decoded){
      return decoded; 
    });

    var updatedAt = new Date();
    let errLog = '\r\n########## \r\n' + new Date(updatedAt.getTime() + moment.tz('Asia/Kolkata').utcOffset() * 60000) + "\r\n";

    errLog += `decoded ---- ${JSON.stringify(decoded)} \r\n`;
    errLog += `data inside isAuthenticated---- ${JSON.stringify(data)} \r\n`;
    
    //check whether the path requested by the user is present in config.js file if not send error.
    let user_type = decoded.user_type;
    let groupData = await GroupModule.findById({_id:req.user.groupId}).populate('permissions').exec()
    let tempPermission = [...groupData.permissions]
     
    let isExists = tempPermission.some((val)=>val.action.findIndex((value)=>value==data)!==-1)
    if (!decoded || decoded == undefined) {
      helper.writeFile(errLog, 'accessLog');
      return res.status(200).json({
        error: true,
        title: 'Invalid access token.'
      });
    }
    
    // if (config.acl[user_type].indexOf(data) > -1) {
    //   // all user can access the route
    //   return next()
    // }
    if(decoded.isMobile&&req.user.isAllPermission&&isExists){
      // for mobile users
      return next()
    }else if (!decoded.isMobile&&isExists) {
      // all user can access the route
      return next()
    }
    else {
      helper.writeFile(errLog, 'accessLog');
        return res.status(200).json({
          error: true,
          title: 'Invalid access token.',
          details:tempPermission
        });
    }
  }
}

const authenticateUser = async (req, res, next) => {
  const token = req.headers.token?req.headers.token:req.query.token; 
  const decoded = jwt.decode(token, "medideals_03_09_19");
  try {
    let userData = await user.findById(decoded.userId).populate('user_state').lean().exec();
    if (!userData || userData == undefined) {
      return res.status(200).json({
        title: 'user not found',
        error: true,
      });
    }
    if(userData.user_status=='denied'){
      return res.status(200).json({
        title: 'Your account has been denied.Please contact admin',
        error: true,
        isDenied:true
      });
    }
    if (parseInt(userData.user_status) == 'blocked') {
      return res.status(200).json({
        title: 'You are blocked.Please contact admin',
        error: true,
        isBlocked:true
      });
    }
    // if (userData.isLoggedIn == false) {
    //   return res.status(200).json({
    //     title: 'Authorization required',
    //     error: true,
    //     detail: "invalid Login"
    //   });
    // }
    if(decoded.asAdmin =='1'){
      userData ={...userData}
      userData.asAdmin = '1'
    }
    req.user = userData;
    return next(null, userData);
  }
  catch (error) {
    return res.status(200).json({
      title: 'Authorization required.',
      error: true,
      detail: error,
      isDenied:true
    });
  }

}



module.exports = {
  isAuthenticated,
  authenticateUser
}
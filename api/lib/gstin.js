const gst_tin_number = [
    {
        state : "Andaman and Nicobar Islands",
        code : "AN",
        tin_number : "35"
    },
    {
        state : "Andhra Pradesh",
        code : "AP",
        tin_number : "28"
    },
    {
        state : "Andhra Pradesh (New)",
        code : "AD",
        tin_number : "37"
    },
    {
        state : "Arunachal Pradesh",
        code : "AR",
        tin_number : "12"
    },
    {
        state : "Assam",
        code : "AS",
        tin_number : "18"
    },
    {
        state : "Bihar",
        code : "BR",
        tin_number : "10"
    },
    {
        state : "Chandigarh",
        code : "CH",
        tin_number : "04"
    },
    {
        state : "Chattisgarh",
        code : "CG",
        tin_number : "22"
    },
    {
        state : "Dadra and Nagar Haveli",
        code : "DH",
        tin_number : "26"
    },
    {
        state : "Daman and Diu",
        code : "DD",
        tin_number : "25"
    },
    {
        state : "Delhi",
        code : "DL",
        tin_number : "07"
    },
    {
        state : "Goa",
        code : "GA",
        tin_number : "30"
    },
    {
        state : "Gujarat",
        code : "GJ",
        tin_number : "24"
    },
    {
        state : "Haryana",
        code : "HR",
        tin_number : "06"
    },
    {
        state : "Himachal Pradesh",
        code : "HP",
        tin_number : "02"
    },
    {
        state : "Jammu and Kashmir",
        code : "JK",
        tin_number : "01"
    },
    {
        state : "Jharkhand",
        code : "JH",
        tin_number : "20"
    },
    {
        state : "Karnataka",
        code : "KA",
        tin_number : "29"
    },
    {
        state : "Kerala",
        code : "KL",
        tin_number : "32"
    },
    {
        state : "Lakshadweep Islands",
        code : "LD",
        tin_number : "31"
    },
    {
        state : "Madhya Pradesh",
        code : "MP",
        tin_number : "23"
    },
    {
        state : "Maharashtra",
        code : "MH",
        tin_number : "27"
    },
    {
        state : "Manipur",
        code : "MN",
        tin_number : "14"
    },
    {
        state : "Meghalaya  ",
        code : "ML",
        tin_number : "17"
    },
    {
        state : "Mizoram",
        code : "MZ",
        tin_number : "15"
    },
    {
        state : "Nagaland",
        code : "NL",
        tin_number : "13"
    },
    {
        state : "Odisha  ",
        code : "OR",
        tin_number : "21"
    },
    {
        state : "Pondicherry",
        code : "PY",
        tin_number : "34"
    },
    {
        state : "Punjab",
        code : "PB",
        tin_number : "03"
    },
    {
        state : "Rajasthan  ",
        code : "RJ",
        tin_number : "08"
    },
    {
        state : "Sikkim  ",
        code : "SK",
        tin_number : "11"
    },
    {
        state : "Tamil Nadu",
        code : "33",
        tin_number : "TN"
    },
    {
        state : "Telangana",
        code : "TS",
        tin_number : "36"
    },
    {
        state : "Tripura",
        code : "TR",
        tin_number : "16"
    },
    {
        state : "Uttar Pradesh",
        code : "UP",
        tin_number : "09"
    },
    {
        state : "Uttarakhand",
        code : "UK",
        tin_number : "05"
    },
    {
        state : "West Bengal",
        code : "WB",
        tin_number : "19"
    }
]

module.exports = gst_tin_number;
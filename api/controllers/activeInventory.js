const mongoose = require('mongoose');
const Order = require('../models/order');
const moment = require('moment');
const ObjectId = mongoose.Types.ObjectId;
const Inventory = require('../models/inventory');
const ActiveInventory = require('../models/activeInventory');
const async = require('async');
let getActiveInventory = async() =>{
    let date = moment();
    let dateAfter3Months = moment().add(3, "M");
    let previousDay = moment().subtract(1, 'days').set({ 'hour': 0, 'minute': 0, 'second': 0 });
    let currentDate = moment().set({ 'hour': 0, 'minute': 0, 'second': 0 }); // current with hour, min, sec set to zero 

    
    let fastInventoryArr = await Order.aggregate([
        {
            $match: {
                $and: [
                    { "order_status.status": { $ne: "Cancelled" } },
                    {
                        "createdAt": {
                            $gte: new Date(previousDay),
                            $lte: new Date(currentDate),
                        }
                    },
                    { "is_deleted": false }
                ]
            }
        },
        {
            $lookup: {
                localField: "products.inventory_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $group: {
                _id: "$Inventory._id",
                "total_amt": { $sum: { $toDouble: "$total_amount" } }
            }
        }, {
            $sort: { total_amt: -1 }
        },
        { $limit : 25 }
    ])
        .then(result => result)
        .catch(error => {
            console.log(error);
        });

    let orderIdArray = await Array.from(fastInventoryArr, x => ObjectId(x._id))

    await Inventory.aggregate([
        {
            $match: {
                $and: [
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } },
                    {status: {$ne: 'Expired'}}
                ]
            }
        },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
        {
            $addFields: { isId: { $in: ['$_id', orderIdArray] } }
        },
    ])
        .then(async(result) => {
            console.log('result',result.length);
            await ActiveInventory.deleteMany({}).catch(error => {
                console.log(error);
            })
            result.map(async(actInv) => {
                let finalDiscount = 0;
                let discountWithPer = 0;
                let discountWithProduct = 0;
                let totalDiscount = 0;
                let totalPrice = 0;
                if(actInv.hasOwnProperty("Discount")){
                    if(actInv.Discount && actInv.Discount.name && actInv.Discount.name === 'Discount'){
                        finalDiscount = actInv.Discount && actInv.Discount.discount_per && Number(actInv.Discount.discount_per).toFixed(2);
                    }else if(actInv.Discount && actInv.Discount.name && (actInv.Discount.name === 'Different')){
                        totalPrice = actInv.Discount && actInv.Discount.discount_on_product && (actInv.Discount.discount_on_product.purchase + actInv.Discount.discount_on_product.bonus) * actInv.PTR;

                        finalDiscount = Number(Number(actInv.PTR / totalPrice) * 100).toFixed(2);
                    }else if(actInv.Discount && actInv.Discount.name && (actInv.Discount.name === 'DifferentAndDiscount')){
                        totalPrice = actInv.Discount && actInv.Discount.discount_on_product && (actInv.Discount.discount_on_product.purchase + actInv.Discount.discount_on_product.bonus) * actInv.PTR;

                        finalDiscount = Number(Number(Number(actInv.PTR / totalPrice) * 100) + Number(actInv.Discount.discount_per)).toFixed(2);
                    }else if (actInv.Discount && actInv.Discount.name && (actInv.Discount.name === 'Same' || actInv.Discount.name === 'SameAndDiscount')){
                        totalPrice = actInv.Discount && actInv.Discount.discount_on_product && (actInv.Discount.discount_on_product.bonus / (actInv.Discount.discount_on_product.purchase + actInv.Discount.discount_on_product.bonus));
                        let dis = actInv.Discount && actInv.Discount.discount_per && Number(actInv.Discount.discount_per) > 0 ? Number(actInv.Discount.discount_per).toFixed(2) : 0;
                        finalDiscount = Number(Number(Number(Number(totalPrice) * 100) + Number(dis)).toFixed(2));
                        console.log('-=-=-=-=-dis',totalPrice,dis,finalDiscount)
                    }
                }
                else{
                    finalDiscount = 0;
                }


                let attribute = [];
                if(actInv.ProductCategory.isFeatured === true){
                    attribute.push('Featured');
                }
                if(actInv.isDealsOfTheDay === true){
                    attribute.push('Deals Of Day');
                }
                if(actInv.isId === true){
                    attribute.push('Fast Moving');
                }
                if(actInv.isJumboDeal === true){
                    attribute.push('Jumbo Deal');
                }
                let discount = actInv.hasOwnProperty("Discount") ? {
                    _id: ObjectId(actInv.Discount._id),
                    name: actInv.Discount.name,
                    discount_type: actInv.Discount.type,
                    discount_per: (actInv.Discount.discount_per),
                    discount_on_product :{
                        inventory_id:actInv.Discount.discount_on_product.inventory_id,
                        purchase: (actInv.Discount.discount_on_product.purchase),
                        bonus: (actInv.Discount.discount_on_product.bonus)
                    }
                } : {};

                let otherProducts =  actInv.hasOwnProperty("OtherProducts") ?  {
                    _id: ObjectId(actInv.OtherProducts._id),
                    name: actInv.OtherProducts.name
                } : {};
                let prepaidIn = actInv.prepaidInven ? actInv.prepaidInven : false;
                let newInventory = new ActiveInventory({
                    inventory_id: ObjectId(actInv._id),
                    medi_attribute:attribute,
                    medi_type:actInv.medicineTypeName,
                    Seller: {
                        _id: ObjectId(actInv.Seller._id),
                        first_name: actInv.Seller.first_name,
                        last_name: actInv.Seller.last_name,
                        company_name: actInv.Seller.company_name,
                        user_city: actInv.Seller.user_city,
                        user_state: actInv.Seller.user_state,
                        user_address: actInv.Seller.user_address,
                        user_pincode: actInv.Seller.user_pincode,
                    },
                    Product: {
                        _id: ObjectId(actInv.Product._id),
                        name: actInv.Product.name,
                        images: actInv.Product.images,
                        company: actInv.Company.name,
                        chem_combination: actInv.Product.chem_combination, 
                        product_type:actInv.Type.name,
                        country_of_origin: actInv.Product.country_of_origin,
                        GST:(actInv.GST.value),
                        isPrepaid: actInv.Product.isPrepaid ? actInv.Product.isPrepaid : false,
                        description: actInv.Product.description ? actInv.Product.description : ''
                    },
                    OtherProducts :otherProducts,
                    Product_Category: {
                        _id: ObjectId(actInv.ProductCategory._id),
                        name: actInv.ProductCategory.name
                    },
                    Discount: discount,
                    final: finalDiscount,
                    Company: {
                        _id: ObjectId(actInv.Company._id),
                        name: actInv.Company.name
                    },
                    status:(actInv.status).toUpperCase() ,
                    expiry_date: actInv.expiry_date,
                    MRP: (actInv.MRP),
                    PTR: (actInv.PTR),
                    ePTR: (actInv.ePTR), //effective price
                    quantity: (actInv.quantity),
                    min_order_quantity: (actInv.min_order_quantity),
                    max_order_quantity: (actInv.max_order_quantity),
                    prepaidInven: prepaidIn
                });
                await newInventory.save()
            });
        })
        .catch(error => {
            console.log(error);
        });
}


const updateManyActiveInventory = async(sellerId) => {
    let date = moment();
    let dateAfter3Months = moment().add(3, "M");
    await Inventory.aggregate([
        {
            $match: {
                $and: [
                    { user_id: ObjectId(sellerId)},
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } },
                    { status: {$ne: 'Expired'}}
                ]
            }
        },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
    ])
    .then(async(result) => {
        console.log('result',result.length);
        await ActiveInventory.deleteMany({'Seller._id':ObjectId(sellerId)}).catch(error => {
            console.log(error);
        })
        async.eachOfSeries(result,async function(actInv,key,mainCb) {
            if(actInv){
                let finalDiscount = 0;
                let discountWithPer = 0;
                let discountWithProduct = 0;
                let totalDiscount = 0;
                let totalPrice = 0;
                if(actInv.hasOwnProperty("Discount")){
                    if(actInv.Discount && actInv.Discount.name && actInv.Discount.name === 'Discount'){
                        finalDiscount = actInv.Discount && actInv.Discount.discount_per && Number(actInv.Discount.discount_per).toFixed(2);
                    }else if(actInv.Discount && actInv.Discount.name && (actInv.Discount.name === 'Different')){
                        totalPrice = actInv.Discount && actInv.Discount.discount_on_product && (actInv.Discount.discount_on_product.purchase + actInv.Discount.discount_on_product.bonus) * actInv.PTR;

                        finalDiscount = Number(Number(actInv.PTR / totalPrice) * 100).toFixed(2);
                    }else if(actInv.Discount && actInv.Discount.name && (actInv.Discount.name === 'DifferentAndDiscount')){
                        totalPrice = actInv.Discount && actInv.Discount.discount_on_product && (actInv.Discount.discount_on_product.purchase + actInv.Discount.discount_on_product.bonus) * actInv.PTR;

                        finalDiscount = Number(Number(Number(actInv.PTR / totalPrice) * 100) + Number(actInv.Discount.discount_per)).toFixed(2);
                    }else if (actInv.Discount && actInv.Discount.name && (actInv.Discount.name === 'Same' || actInv.Discount.name === 'SameAndDiscount')){
                        totalPrice = actInv.Discount && actInv.Discount.discount_on_product && (actInv.Discount.discount_on_product.bonus / (actInv.Discount.discount_on_product.purchase + actInv.Discount.discount_on_product.bonus));
                        let dis = actInv.Discount && actInv.Discount.discount_per && Number(actInv.Discount.discount_per) > 0 ? Number(actInv.Discount.discount_per).toFixed(2) : 0;
                        finalDiscount = Number(Number(Number(Number(totalPrice) * 100) + Number(dis)).toFixed(2));
                        console.log('-=-=-=-=-dis',totalPrice,dis,finalDiscount)
                    }
                }
                else{
                    finalDiscount = 0;
                }


                let attribute = [];
                if(actInv.ProductCategory.isFeatured === true){
                    attribute.push('Featured');
                }
                if(actInv.isDealsOfTheDay === true){
                    attribute.push('Deals Of Day');
                }
                if(actInv.isJumboDeal === true){
                    attribute.push('Jumbo Deal');
                }
                let discount = actInv.hasOwnProperty("Discount") ? {
                    _id: ObjectId(actInv.Discount._id),
                    name: actInv.Discount.name,
                    discount_type: actInv.Discount.type,
                    discount_per: (actInv.Discount.discount_per),
                    discount_on_product :{
                        inventory_id:actInv.Discount.discount_on_product.inventory_id,
                        purchase: (actInv.Discount.discount_on_product.purchase),
                        bonus: (actInv.Discount.discount_on_product.bonus)
                    }
                } : {};

                let otherProducts =  actInv.hasOwnProperty("OtherProducts") ?  {
                    _id: ObjectId(actInv.OtherProducts._id),
                    name: actInv.OtherProducts.name
                } : {};
                let prepaidIn = actInv.prepaidInven ? actInv.prepaidInven : false;
                let newInventory = new ActiveInventory({
                    inventory_id: ObjectId(actInv._id),
                    medi_attribute:attribute,
                    medi_type:actInv.medicineTypeName,
                    Seller: {
                        _id: ObjectId(actInv.Seller._id),
                        first_name: actInv.Seller.first_name,
                        last_name: actInv.Seller.last_name,
                        company_name: actInv.Seller.company_name,
                        user_city: actInv.Seller.user_city,
                        user_state: actInv.Seller.user_state,
                        user_address: actInv.Seller.user_address,
                        user_pincode: actInv.Seller.user_pincode,
                    },
                    Product: {
                        _id: ObjectId(actInv.Product._id),
                        name: actInv.Product.name,
                        images: actInv.Product.images,
                        company: actInv.Company.name,
                        chem_combination: actInv.Product.chem_combination, 
                        product_type:actInv.Type.name,
                        country_of_origin: actInv.Product.country_of_origin,
                        GST:(actInv.GST.value),
                        isPrepaid: actInv.Product.isPrepaid ? actInv.Product.isPrepaid : false,
                        description: actInv.Product.description ? actInv.Product.description : '',
                    },
                    OtherProducts :otherProducts,
                    Product_Category: {
                        _id: ObjectId(actInv.ProductCategory._id),
                        name: actInv.ProductCategory.name
                    },
                    Discount: discount,
                    final: finalDiscount,
                    Company: {
                        _id: ObjectId(actInv.Company._id),
                        name: actInv.Company.name
                    },
                    status:(actInv.status).toUpperCase() ,
                    expiry_date: actInv.expiry_date,
                    MRP: (actInv.MRP),
                    PTR: (actInv.PTR),
                    ePTR: (actInv.ePTR), //effective price
                    quantity: (actInv.quantity),
                    min_order_quantity: (actInv.min_order_quantity),
                    max_order_quantity: (actInv.max_order_quantity),
                    prepaidInven: prepaidIn
                });
                await newInventory.save()
            }
            else{
                mainCb()
            }
        },function(){
        });
    })
    .catch(error => {
        console.log(error);
    });
}

module.exports = { getActiveInventory , updateManyActiveInventory}
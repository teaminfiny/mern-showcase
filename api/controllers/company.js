var Company = require('../models/company');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const Inventory = require('../models/inventory');
const ActiveInventory = require('../models/activeInventory');
const Product = require('../models/product');
const BuyerRequest = require('../models/buyerRequest');
const async = require('async');

/*
# parameters: token,
# purpose: listing of all Company names
*/
const listingCompany = (req, res) => {
    let user = req.user;

}

/*
# parameters: token,
# purpose: Get company details by id
*/
const companyDetails = (req, res) => {
    let user = req.user;
}

/*
# parameters: token,
# purpose: Add Company Details
*/
const addCompanyDetails = (req, res) => {

    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('address', 'Address is required').notEmpty();
    req.checkBody('number', 'Number is required').notEmpty();
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let comapny = new Company();
        comapny.name = req.body.name;
        comapny.address = req.body.address;
        comapny.number = req.body.number;
        comapny.description = req.body.description;
        comapny.save().then((result) => {
            res.status(200).json({
                error: false,
                title: 'Company added successfully'
            })
        })
    }
}

/*
# parameters: token,
# purpose: Edit Company Details
*/
const editCompanyDetails = (req, res) => {
    let user = req.user;
}

const escapeRegex = (string) => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}
const deleteDuplicate = async(req, res) => {
    let allId = [];
    let mainId = '';
    let name = '';
    let keyValue = escapeRegex(req.body.name)
    await Company.find({name: {$regex: new RegExp(keyValue, 'i')}}, (err, data) => {
        if(!err)
        mainId = data[0]._id;
        name = data[0].name
        async.eachOfSeries(data, async function(eachData,index,cb) {
            if(eachData){
                allId.push(eachData._id);
            }
        },async function(){
            console.log("duplicate companies", allId.length)
            console.log("Main Id", mainId)
            async.eachOfSeries(allId, async function(id,index,mainCb) {
                if(id && ObjectId(id) !== ObjectId(mainId)){
                    console.log(id)
                    await Inventory.updateMany({company_id: ObjectId(id)}, {$set: {company_id: ObjectId(mainId)}}, (err) => {
                        if(!err){
                            console.log(id + " is updated with " + mainId);
                        }
                    });
                    await ActiveInventory.updateMany({'Company._id': ObjectId(id)}, {$set: {'Company._id': ObjectId(mainId), 'Company.name':name}}, (err) => {
                        if(!err){
                            console.log(id + " is updated with " + mainId);
                        }
                    });
                    await Product.updateMany({company_id: ObjectId(id)}, {$set: {company_id: ObjectId(mainId)}}, (err) => {
                        if(!err){
                            console.log(id + " is updated with " + mainId);
                        }
                    });
                    await BuyerRequest.updateMany({manufacturer: ObjectId(id)}, {$set: {manufacturer: ObjectId(mainId)}}, (err) => {
                        if(!err){
                            console.log(id + " is updated with " + mainId);
                        }
                    });
                    await Company.deleteOne({_id: ObjectId(id)}, (err) => {
                        if(!err){
                            console.log(id + " deleted successfully");
                        }
                    })
                }
            }, function(){
                res.send("done");
            })
        });
    });
}

module.exports = {
    listingCompany,
    companyDetails,
    addCompanyDetails,
    editCompanyDetails,
    deleteDuplicate
}
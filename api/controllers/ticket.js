var mongoose = require('mongoose');
var Ticket = require('../models/ticket')
var ObjectId = mongoose.Types.ObjectId;


const addTickets = (req, res) => {
  Ticket.addTicket(req, res);
}

module.exports = { addTickets }
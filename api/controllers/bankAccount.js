

/*
# parameters: token,
# purpose: listing bank account
*/
const listingBankAccount = (req, res) => {
    let user = req.user;
    
}

/*
# parameters: token,
# purpose: add bank account
*/
const addBankAccount = (req, res) => {
    let user = req.body.userData;   
}

/*
# parameters: token,
# purpose: delete bank account
*/
const deleteBankAccount = (req, res) => {
    let user = req.body.userData;   
}

module.exports = {
    listingBankAccount,
    addBankAccount,
    deleteBankAccount
}
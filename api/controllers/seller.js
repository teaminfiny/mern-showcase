const bcrypt = require('bcryptjs');
const async = require('async');
const randomstring = require('randomstring');
const moment = require('moment');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

const user = require('../models/user');
const helper = require('../lib/helper');

var User = require('../models/user')
var PermissionModule = require('../models/permissionModule')
var GroupModule = require('../models/permssionGroup')
var PermissionModule = require('../models/permissionModule')
var ProductCategory = require('../models/product_category');
var ProductRequest = require('../models/productRequest')
var ProductType = require('../models/productType');
var Gst = require('../models/gst')

var Inventory = require('../models/inventory');
var Order = require('../models/order')
var settlement = require('../models/settlement')
var Rating = require('../models/rating')
const {updateManyActiveInventory} = require('./activeInventory');
const ActiveInventory = require('../models/activeInventory');
/*
# parameters: token,
# purpose: listing of all seller
*/
const listingSellers = (req, res) => {
    let user = req.user;

}

/*
# parameters: token,
# purpose: Get seller details by id
*/
const getSellerDetails = (req, res) => {
    let user = req.user;
}

/*
#parameters: vacationMode,tillDate
# purpose: Vacction mode on/off
*/
const vacationMode = (req, res) => {
    User.findOne({ _id: req.user._id }).then(async(data) => {
        data.vaccation.vaccationMode = req.body.vacationMode
        if (req.body.tillDate) {
            data.vaccation.tillDate = new Date(req.body.tillDate);
        }
        if (req.body.vacationMode === true) {
            await ActiveInventory.deleteMany({'Seller._id':ObjectId(req.user._id)}).exec()
            var date = moment(req.body.tillDate).format('DD/MM/YYYY');
            var mailData = {
                email: 'support@medimny.com',
                subject: 'Seller going on leave.',
                body:
                    '<p>' +
                    `${req.user.company_name} is going on leave till ${date}.`
            };
            helper.sendEmail(mailData);
        }

        data.save().then(async(savedData) => {
            if(req.body.vacationMode === false){
                await updateManyActiveInventory(req.user._id)
            }
            res.status(200).json({
                error: false,
                title: req.body.vacationMode ? 'Vacation mode activated' : 'Vacation mode deactivated'
            })
        })
    })
}
/*
#parameters: 
#purpose: List permission module
*/
var getPermissionModule = (req, res) => {
    PermissionModule.find({ isDeleted: false }).sort({ name: 1 }).then((results) => {
        if (results && results.length > 0) {
            if (req.user.user_type === "admin") {
                res.status(200).json({
                    error: false,
                    detail: results
                })
            }
            else {
                helper.sortPermissionModule(results, 'getPermissionModule', (error, response) => {
                    res.status(200).json({
                        error: false,
                        detail: response
                    })
                });
            }
        } else {
            res.status(200).json({
                error: false,
                detail: []
            })
        }
    })
}

/*
#parameters:searchText
#purpose: List group module
*/
var getGroupModule = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    let matchQuery = {}
    matchQuery = req.body.searchText ? { name: { $regex: req.body.searchText, $options: 'i' } } : {}
    GroupModule.aggregate([
        {
            $match: {
                mainUser: ObjectId(Id),
                isDeleted: false
            }
        },
        {
            $lookup: {
                localField: 'permissions',
                foreignField: '_id',
                from: 'permissionmodules',
                as: 'Permissions'
            }
        },
        {
            $project: {
                _id: 1,
                name: 1,
                'Permissions.name': 1,
                'Permissions._id': 1,
                members: { $size: '$staffId' }
            }
        },
        {
            $match: matchQuery
        },
        { $sort: { _id: -1 } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
            }
        }
    ])
        .allowDiskUse(true)
        .then((results) => {
            return res.status(200).json({
                error: false,
                detail: results
            })
        })

}

/*
#parameters:userId
#purpose: get group module
*/
var getGroup = async (userId, name) => {
    let data = GroupModule.findOne({ mainUser: userId, name: name, isDeleted: false }).exec()
    return data
}

/*
#parameters:name,permissions
#purpose: add group module
*/
var addGroupModule = async (req, res) => {
    req.checkBody('name', 'Group Name is required').notEmpty();
    PermissionModule.findOne({ name: "Vacation" }).then((permission) => {
        let groupData = getGroup(req.user.mainUser ? req.user.mainUser : req.user._id, req.body.name)
        // if(req.body.admin_access===true&&permission){
        //     req.body.permissions.push(permission)
        // }
        groupData.then((groupResult) => {
            if (groupResult) {
                return res.status(200).json({
                    error: true,
                    title: 'This group already exists',
                });
            }
            let errors = req.validationErrors();
            if (errors) {
                return res.status(200).json({
                    error: true,
                    title: 'Something went wrong please try again.',
                    errors: errors
                });
            }
            let Id = req.user.mainUser ? req.user.mainUser : req.user._id
            var addGroup = new GroupModule({
                name: req.body.name,
                permissions: req.body.permissions,
                mainUser: Id
            })
            addGroup.save().then((result) => {
                res.status(200).json({
                    error: false,
                    title: 'Group added successfully'
                })
            })
        })
    })


}

/*
#parameters:name,permissions,groupId
#purpose: edit group module
*/
var editGroupModule = async (req, res) => {
    req.checkBody('name', 'Group Name is required').notEmpty();
    PermissionModule.findOne({ name: "Vacation" }).then((permission) => {
        // if(req.body.admin_access===true&&permission){
        //     let index = req.body.permissions.findIndex((val)=>val.name==permission.name)
        //     if(index===-1){
        //         req.body.permissions.push(permission)
        //     }

        // }else{
        //     let index = req.body.permissions.findIndex((val)=>val.name==permission.name)
        //     if(index>-1){
        //         req.body.permissions.splice(index,1)
        //     }
        // }
        let groupData = getGroup(req.user.mainUser ? req.user.mainUser : req.user._id, req.body.name)
        groupData.then((groupResult) => {
            if (groupResult && ObjectId(groupResult._id).equals(req.body.groupId) == false) {
                return res.status(200).json({
                    error: true,
                    title: 'This group name already exixts',
                });
            }
            let errors = req.validationErrors();
            if (errors) {
                return res.status(200).json({
                    error: true,
                    title: 'Something went wrong please try again.',
                    errors: errors
                });
            }
            let Id = req.user.mainUser ? req.user.mainUser : req.user._id
            GroupModule.findOne({ _id: req.body.groupId }).then((groupModule) => {
                groupModule.name = req.body.name;
                groupModule.permissions = req.body.permissions;
                groupModule.save().then((result) => {
                    res.status(200).json({
                        error: false,
                        title: 'Group updated successfully'
                    })
                })
            })
        })
    })


}

/*
#parameters:name
#purpose: delete group module
*/
var deleteGroupModule = (req, res) => {
    let groupData = getGroup(req.user.mainUser ? req.user.mainUser : req.user._id, req.body.name)
    groupData.then((groupResults) => {
        if (!groupResults) {
            return res.status(200).json({
                error: true,
                title: 'Something went wrong please try again.',
            })
        }
        groupResults.isDeleted = true;
        groupResults.save().then((result) => {
            res.status(200).json({
                error: false,
                title: 'Group deleted successfully'
            })
        })
    })

}

/*
#parameters:first_name, searchText, page, perPage,
#purpose: get all the list for staff members.
*/

const getStaffList = (req, res) => {
    let matchQuery = {}
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    matchQuery = req.body.searchText ? { first_name: { $regex: req.body.searchText, $options: 'i' } } : {}
    user.aggregate([
        {
            $match: {
                mainUser: ObjectId(Id),
                _id: { $ne: req.user._id },
                is_deleted: false
            }
        },
        {
            $lookup: {
                localField: 'groupId',
                foreignField: '_id',
                from: 'permissiongroups',
                as: 'Groups'
            }
        },
        { $unwind: '$Groups' },
        {
            $project: {
                _id: 1,
                first_name: 1,
                email: 1,
                phone: 1,
                backGroundColor: 1,
                createdAt: 1,
                'Groups.name': 1,
                'Groups._id': 1,
                'Groups.permissions': 1
            }
        },
        {
            $match: matchQuery
        },
        {
            $sort: { createdAt: -1 }
        },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: Number(req.body.perPage) },]
            }
        }
    ])
        .allowDiskUse(true)
        .then((results) => {
            return res.status(200).json({
                error: false,
                detail: results
            })
        })
}


/*
#parameters:first_name, email, phone, address, groupId, staffId
#purpose: add staff member.
*/

const editStaffMember = async (req, res) => {

    try {
        const staff = await user.findByIdAndUpdate(req.body.staffId);

        let oldGroupId = staff.groupId;

        staff.first_name = req.body.first_name;
        staff.phone = req.body.phone;
        staff.email = req.body.email;
        staff.groupId = req.body.groupId;
        staff.isAllPermission = req.body.isAllPermission ? true : false
        staff.save(async (err, staffDetails) => {
            if (err) {
                return res.status(200).json({
                    title: "Something went wrong, Please try again",
                    error: true,
                    detail: err
                })
            }
            if (req.body.groupId.toString() === oldGroupId.toString()) {
                return res.status(200).json({
                    title: "Staff edited successfully",
                    error: false,
                    detail: staffDetails
                })
            } else {
                try {
                    const deleteGroup = await GroupModule.findByIdAndUpdate(oldGroupId, { $pull: { staffId: staff._id } }, { 'new': true });

                    try {
                        const addNewGroup = await GroupModule.findByIdAndUpdate(req.body.groupId, { $push: { staffId: staff._id } }, { 'new': true });

                        return res.status(200).json({
                            title: "Staff edited successfully",
                            error: false,
                            detail: staffDetails
                        })

                    } catch (error) {
                        return res.status(200).json({
                            title: "Something went wrong, Please try again",
                            error: true,
                            detail: error
                        })
                    }

                } catch (error) {
                    return res.status(200).json({
                        title: "Something went wrong, Please try again",
                        error: true,
                        detail: error
                    })
                }
            }
        });

    } catch (error) {
        return res.status(200).json({
            title: "Something went wrong, Please try again",
            error: true,
            detail: error
        })
    }
}

/*
#parameters:first_name, email, phone, address, groupId.
#purpose: add staff member.
*/
const addStaffMember = (req, res) => {
    req.checkBody('first_name', 'First Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is required').isEmail();
    req.checkBody('phone', 'Phone is required').notEmpty();
    // req.checkBody('address', 'Address is required').notEmpty();
    let password = randomstring.generate({
        length: 6,
        charset: 'alphanumeric'
    });

    let errors = req.validationErrors();

    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    }
    let user_type = "seller"
    if (req.user.user_type === "admin") {
        user_type = "admin"
    }
    let staff = new user({
        first_name: req.body.first_name,
        email: req.body.email,
        phone: req.body.phone,
        password: bcrypt.hashSync(password, bcrypt.genSaltSync(10)),
        user_type: user_type,
        groupId: req.body.groupId,
        addedBy: req.user._id,
        mainUser: req.user.addedBy ? req.user.mainUser : req.user._id,
        isVerfied: true,
        user_status: 'active',
        isAllPermission: req.body.isAllPermission,
        backGroundColor: {
            red: req.body.red,
            green: req.body.green,
            blue: req.body.blue
        },
    })

    staff.save()
        .then(async (savedStaffDetails, error) => {

            if (error) {
                return res.status(200).json({
                    title: "Something went wrong, Please try again",
                    error: true,
                    detail: savedStaffDetails
                })
            }

            try {

                const group = await GroupModule.findByIdAndUpdate(req.body.groupId, { $push: { staffId: savedStaffDetails._id } }, { 'new': true });

                var mailData = {
                    email: savedStaffDetails.email,
                    subject: 'Signup successful',
                    body: 'Congratulations, Your Medimny account has been created.' + '<br/>' + ' Please use the following credentials for login' + '<br/>' + '<b>Email :</b>' + req.body.email + '<br/><b> Password :</b>' + password + '<br/>Thank you, Team Medimny.'
                };

                helper.sendEmail(mailData);

                return res.status(200).json({
                    title: "Staff created successfully.",
                    error: false,
                    detail: savedStaffDetails
                })
            } catch (error) {
                return res.status(200).json({
                    title: "Something went wrong, Please try again",
                    error: true,
                    detail: error
                })
            }

        })
        .catch((error) => {
            return res.status(200).json({
                title: "Something went wrong, Please try again",
                error: true,
                detail: error
            })
        })
}

/*
#parameters: staffId
#purpose: delete staff members (Note : All delete are soft delete).
*/
const deleteStaff = async (req, res) => {
    try {
        const userData = await user.deleteOne({ _id: ObjectId(req.body.staffId) });
        /* GroupModule.find({ "staffId": req.body.staffId })
            .exec((error, response) => {
                if (response && response.length > 0) {
                    let staffData = response.map((value, key) => {
                        if (value.toString() == req.body.staffId.toString()) {
                            return undefined
                        } else {
                            return value
                        }
                    })

                    response[0].staffId = staffData;
                    response[0].save();
                }
            }) */
        GroupModule.updateMany({ "staffId": { $in: [req.body.staffId] } }, { $pull: { staffId: req.body.staffId } }, { 'new': true })
            .exec((error, groupData) => {
            })
        return res.status(200).json({
            title: "Staff deleted successfully",
            error: false,
            detail: userData
        })
    } catch (error) {
        return res.status(200).json({
            title: "Something went wrong, Please try again",
            error: true,
            detail: error
        })
    }
}

/*
#parameters: token in header
#purpose: get groupls list for the user.
*/
const getGroupList = async (req, res) => {
    try {
        let groupList = await GroupModule.find({ mainUser: req.user.addedBy ? req.user.mainUser : req.user._id, isDeleted: false }).populate('permissions', 'name').populate('staffId', 'first_name last_name email user_address phone').sort({ _id: - 1 });
        return res.status(200).json({
            title: "Group fetched successfully",
            error: false,
            detail: groupList
        })
    }
    catch (error) {
        return res.status(200).json({
            title: "Something went wrong, Please try again",
            error: true,
            detail: error
        })
    }
}

/*
# parameters: token
# puprpose: get data for product type product category, gst for product request
*/
const getDataforProductRequest = async (req, res) => {
    let productCategory = await ProductCategory.getProductCategory()
    let productType = await ProductType.getProductType()
    let gst = await Gst.getGst()
    res.status(200).json({
        error: false,
        productCategory,
        productType,
        gst
    })
}

/*
#parameters: token,name,companyName,chemicalCombination,gst,mrp,ptr,productType,productCategory
#purpose: Add product request
*/
const addProductRequest = (req, res) => {
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('companyName', 'Company name is required').notEmpty();
    req.checkBody('chemicalCombination', 'Chemical combination is required').notEmpty();
    req.checkBody('gst', 'GST is required').notEmpty();
    req.checkBody('productType', 'Product type is required').notEmpty();
    req.checkBody('productCategory', 'Product category is required').notEmpty();

    let Id = req.user.mainUser ? req.user.mainUser : req.user._id

    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    }
    let addRequest = new ProductRequest();
    addRequest.name = req.body.name;
    addRequest.companyName = req.body.companyName
    addRequest.chemicalCombination = req.body.chemicalCombination
    addRequest.gst = req.body.gst
    addRequest.mrp = req.body.mrp
    addRequest.ptr = req.body.ptr
    addRequest.productType = req.body.productType
    addRequest.productCategory = req.body.productCategory
    addRequest.status = 'pending'
    addRequest.addedBy = req.user._id
    addRequest.mainUser = Id
    addRequest.save().then((result) => {
        res.status(200).json({
            error: false,
            title: 'Product requested successfully'
        })
    })

}

/*
#parameter: token
#purpose: get requested product according to id
*/
const getRequestedProductList = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    let matchQuery = {}
    matchQuery = req.body.searchText ? {
        $or: [{ name: { $regex: req.body.searchText, $options: 'i' } },
        { companyName: { $regex: req.body.searchText, $options: 'i' } }, { mrp: { $regex: req.body.searchText, $options: 'i' } },
        { status: { $regex: req.body.searchText, $options: 'i' } }, { ptr: { $regex: req.body.searchText, $options: 'i' } }]
    } : {}
    let matchUsr = match1 = {
        mainUser: ObjectId(Id),
        status: "pending"
    }
    if (req.user.user_type === "admin") {
        matchUsr = {
            status: "pending"
        }
    }
    ProductRequest
        .aggregate([
            {
                $match: matchUsr
            },
            {
                $lookup: {
                    localField: 'productType',
                    foreignField: '_id',
                    from: 'producttypes',
                    as: 'productType'
                }
            },
            {
                $unwind: {
                    path: '$productType',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'productCategory',
                    foreignField: '_id',
                    from: 'product_categories',
                    as: 'productCategory'
                }
            },
            {
                $unwind: {
                    path: '$productCategory',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'gst',
                    foreignField: '_id',
                    from: 'gsts',
                    as: 'gst'
                }
            },
            {
                $unwind: {
                    path: '$gst',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'addedBy',
                    foreignField: '_id',
                    from: 'users',
                    as: 'addedBy'
                }
            },
            {
                $unwind: {
                    path: '$addedBy',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    name: 1,
                    companyName: 1,
                    mrp: 1,
                    ptr: 1,
                    status: 1,
                    productType:1,
                    chemicalCombination:1,
                    productCategory:1,
                    gst:1,
                    addedBy:1,
                    createdAt:1
                }
            },
            {
                $match: matchQuery
            },
            { $sort: { createdAt: -1 } },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                    data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: Number(req.body.perPage) }]
                }
            }
        ])
        .then((result) => {
            res.status(200).json({
                error: false,
                detail: result
            })
        })
}

const getDashBoardCardDetails = async (req, res) => {
    let date = new Date();
    let tempMonth = date.getMonth() + 1
    let currentMonth = date.getMonth()
    let tempYear = date.getFullYear()
    let Id;

    if (req.user.user_type === 'admin') {
        Id = req.query.id
    } else {
        Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    }
    let lastOrders = await Order.aggregate([{
        $match: {
            seller_id: ObjectId(Id),
            createdAt: { $gte: new Date((new Date().getTime() - (30 * 24 * 60 * 60 * 1000))) } // last 30 days
        }
    },
    { $unwind: '$products' },
    { $group: { _id: '$products.inventory_id' } }
    ]);
    let orderData = Array.from(lastOrders, x => ObjectId(x._id));
    let activeSelling = await Inventory
        .aggregate([
            {
                $match: {
                    user_id: ObjectId(Id),
                    isDeleted: false
                }
            },
            {
                $addFields: { isId: { $in: ['$_id', orderData] } }

            },
            {
                $match: { isId: true }
            }
        ]);
    let slowMoving = await Inventory
        .aggregate([
            {
                $match: {
                    user_id: ObjectId(Id),
                    isDeleted: false
                }
            },
            {
                $addFields: { isId: { $in: ['$_id', orderData] } }

            },
            {
                $match: { isId: false }
            }
        ]);
    let outOfStock =  await Inventory.aggregate([
        {'$addFields': {
            month: {
                $add: [
                    { '$subtract': [{ '$month': '$expiry_date' }, tempMonth] },
                    { $multiply: [12, { $subtract: [{ '$year': '$expiry_date' }, tempYear] }] }
                ]
            }
        }},
        { '$match': { user_id: ObjectId(Id), isDeleted: false  }},
        { '$match': { $or: [{$expr: {$lt: ["$quantity", "$min_order_quantity"]}},{quantity: { $eq: 0 }}]}},
        { '$match': { month: { '$gte': 9 } } }
    ]);
    let shortExpireProduct = await Inventory.aggregate([
        {
            $match: {
                user_id: ObjectId(Id),
                isDeleted: false
            }
        },
        {
            '$addFields': {
                month: {
                    $add: [
                        { '$subtract': [{ '$month': '$expiry_date' }, tempMonth] },
                        { $multiply: [12, { $subtract: [{ '$year': '$expiry_date' }, tempYear] }] }
                    ]
                }
            }
        },
        { '$match': {$expr: {$gte: ["$quantity", "$min_order_quantity"]}}},
        { '$match': { month: { '$lt': 9 } } },
        { '$match': { month: { '$gt': 3 } } },
        {
            $count: 'total'
        }

    ]).then(result => result);
    return res.status(200).json({
        error: false,
        activeSelling: activeSelling ? activeSelling.length : 0,
        outOfStock: outOfStock ? outOfStock.length : 0,
        shortExpireProduct: shortExpireProduct && shortExpireProduct.length > 0 ? shortExpireProduct[0].total : 0,
        slowMoving: slowMoving && slowMoving.length > 0 ? slowMoving.length : 0
    });
}

const getMonthNumber = (data) => {
    let val = data.toLowerCase()
    switch (val) {
        case 'january':
            return 1;
            break;

        case 'february':
            return 2;
            break;

        case 'march':
            return 3;
            break;

        case 'april':
            return 4;
            break;

        case 'may':
            return 5;
            break;

        case 'june':
            return 6;
            break;

        case 'july':
            return 7;
            break;

        case 'august':
            return 8;
            break;

        case 'september':
            return 9;
            break;

        case 'october':
            return 10;
            break;

        case 'november':
            return 11;
            break;

        case 'december':
            return 12;
            break;
        default:
            return 1
    }
}

const getDashboardTopSellingProducts = async (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id)
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: {
                year: req.body.year,
                month: getMonthNumber(req.body.month)
            }
        },
        {
            $project: {
                products: 1,
                month: 1, year: 1,
                // data: {
                //     $filter: {
                //         input: "$order_status",
                //         as: 'item',
                //         cond: { $eq: ["$$item.status", "Processed"] }
                //     }
                // }
            }
        },
        // {
        //     $addFields: {
        //         Delivered: { $size: "$data" }
        //     }
        // },
        // {
        //     $match: {
        //         Delivered: { $gte: 1 }
        //     }
        // },
        {
            $unwind: "$products"
        },
        {
            $group: {
                _id: "$products.inventory_id",
                quantity: { $sum: { $toInt: "$products.quantity" } }
            }
        },
        {
            $sort: { quantity: -1 }
        },
        {
            $limit: 5
        },
        {
            $lookup: {
                localField: "_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: "Inventory.product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        {
            $lookup: {
                localField: "Product.company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $project: {
                quantity: 1,
                "Product.name": 1,
                "Company.name": 1
            }
        }
    ])
        .then((result) => {
            res.status(200).json({
                error: false,
                detail: result
            })
        })
}

const getDataForChart = async (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    let date = new Date()
    let year = date.getFullYear();
    let Apr1 = new Date(moment(date).month("April").startOf('month').format('YYYY-MM-DD'));
    let Mar31 = new Date(moment(date).add('1','year').month("March").endOf('month').format('YYYY-MM-DD'));
    
    let totalOrder = await Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id)
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
            }
        },
        {
            $match: {
                "createdAt": { $gte: Apr1, $lt: Mar31 },
            }
        },
        {
            $project: {
                seller_id: 1,
                status: {
                    $map: {
                        input: "$order_status",
                        as: 'item',
                        in: "$$item.status"
                    }
                }
            }
        },
        {
            $addFields: {
                delivered: { $indexOfArray: ["$status", "Delivered"] },
                cancelled: { $indexOfArray: ["$status", "Cancelled"] }
            }
        },
        { $match: { cancelled: { $eq: -1 }, delivered: { $eq: -1 } } },
        { $count: "total" }
    ]).then(result => result)
    let placedOrder = await Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id)
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
            }
        },
        {
            $match: {
                "createdAt": { $gte: Apr1, $lt: Mar31 },
            }
        },
        {
            $project: {
                seller_id: 1,
                status: {
                    $map: {
                        input: "$order_status",
                        as: 'item',
                        in: "$$item.status"
                    }
                }
            }
        },
        {
            $addFields: {
                delivered: { $indexOfArray: ["$status", "Delivered"] },
                cancelled: { $indexOfArray: ["$status", "Cancelled"] },
                readyForDis: { $indexOfArray: ["$status", "Ready For Dispatch"] },
                Processed: { $indexOfArray: ["$status", "Processed"] },
                placed: { $indexOfArray: ["$status", "New"] }
            }
        },
        {
            $match:
                { cancelled: -1, delivered: -1, readyForDis: -1, Processed: -1, placed: { $gt: -1 } }
        },
        { $count: "total" }

    ]).then(result => result)
    let ProcessedOrder = await Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id)
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
            }
        },
        {
            $match: {
                "createdAt": { $gte: Apr1, $lt: Mar31 },
            }
        },
        {
            $project: {
                seller_id: 1,
                status: {
                    $map: {
                        input: "$order_status",
                        as: 'item',
                        in: "$$item.status"
                    }
                }
            }
        },
        {
            $addFields: {
                delivered: { $indexOfArray: ["$status", "Delivered"] },
                cancelled: { $indexOfArray: ["$status", "Cancelled"] },
                readyForDis: { $indexOfArray: ["$status", "Ready For Dispatch"] },
                Processed: { $indexOfArray: ["$status", "Processed"] },
            }
        },
        {
            $match:
                { cancelled: -1, delivered: -1, readyForDis: -1, Processed: { $gt: -1 } }
        },
        { $count: "total" }

    ])
        .then(result => result)
    let dispatchedOrder = await Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id)
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
            }
        },
        {
            $match: {
                "createdAt": { $gte: Apr1, $lt: Mar31 },
            }
        },
        {
            $project: {
                seller_id: 1,
                status: {
                    $map: {
                        input: "$order_status",
                        as: 'item',
                        in: "$$item.status"
                    }
                }
            }
        },
        {
            $addFields: {
                delivered: { $indexOfArray: ["$status", "Delivered"] },
                cancelled: { $indexOfArray: ["$status", "Cancelled"] },
                readyForDis: { $indexOfArray: ["$status", "Ready For Dispatch"] },
            }
        },
        {
            $match:
                { cancelled: -1, delivered: -1, readyForDis: { $gt: -1 } },
        },
        { $count: "total" }

    ])
        .then(result => result)
    res.status(200).json({
        totalOrder: totalOrder && totalOrder.length > 0 ? totalOrder[0].total : 0,
        placedOrder: placedOrder && placedOrder.length > 0 ? placedOrder[0].total : 0,
        ProcessedOrder: ProcessedOrder && ProcessedOrder.length > 0 ? ProcessedOrder[0].total : 0,
        dispatchedOrder: dispatchedOrder && dispatchedOrder.length > 0 ? dispatchedOrder[0].total : 0
    })
}

const getGraphForDahboard = (req, res) => {
    let data = monthArray = [{
        "month": "Jan",
        "orders": 0
    }, {
        "month": "Feb",
        "orders": 0
    }, {
        "month": "Mar",
        "orders": 0
    }, {
        "month": "Apr",
        "orders": 0
    }, {
        "month": "May",
        "orders": 0
    }, {
        "month": "June",
        "orders": 0
    }, {
        "month": "July",
        "orders": 0
    }, {
        "month": "Aug",
        "orders": 0
    }, {
        "month": "Sep",
        "orders": 0
    }, {
        "month": "Oct",
        "orders": 0
    }, {
        "month": "Nov",
        "orders": 0
    }, {
        "month": "Dec",
        "orders": 0
    }];

    let date = new Date();
    let year = date.getFullYear();
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id)
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: {
                year: year
            }
        },
        {
            $group: {
                _id: "$month",
                value: { $sum: 1 }
            }
        }
    ])
        .then((result) => {
            async.eachOfSeries(result, function (data2, key, cb) {
                data[data2._id - 1].orders = data2.value;
                cb()
            }, function (err) {
                res.status(200).json({
                    error: false,
                    detail: data
                })
            })
        })
}

const getSettlements = (req, res) => {
    let userData = req.user;
    let matchQuery = {}
    matchQuery = req.body.searchText ? { 'order.order_id': { $regex: req.body.searchText, $options: 'i' } } : {}
    let query = {};
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    query = (req.body.year && req.body.month) ? [{
        $match: {
            seller_id: Id,
        }
    },
    {
        $addFields: {
            year: { $year: "$createdAt" },
            month: { $month: "$createdAt" }
        }
    },
    {
        $match: {
            year: Number(req.body.year),
            month: getMonthNumber(req.body.month)
        }
    },
    {
        $lookup: {
            localField: 'order_id',
            foreignField: '_id',
            from: 'orders',
            as: 'order'
        }
    },
    {
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller'
        }
    },
    {
        $unwind: {
            path: '$order',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $unwind: {
            path: '$seller',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $match: matchQuery
    },
    {
        $facet: {
            metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
            data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
        }
    }] : [{
        $match: {
            seller_id: Id
        }
    },
    {
        $lookup: {
            localField: 'order_id',
            foreignField: '_id',
            from: 'orders',
            as: 'order'
        }
    },
    {
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller'
        }
    },
    {
        $unwind: {
            path: '$order',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $unwind: {
            path: '$seller',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $project: {
            "_id": 1,
            "seller.first_name": 1,
            "seller.last_name": 1,
            "seller.phone": 1,
            "seller.user_address": 1,
            "seller.user_pincode": 1,
            "seller.email": 1,
            "order.order_id": 1,
            "order.invoice_number": 1,
            "order.unique_invoice": 1,
            "order.requested": 1,
            "order.total_amount": 1,
            "status": 1,
            "order_id": 1,
            "seller_id": 1,
            "invoice_id": 1,
            "amount": 1,
            "refCode": 1,
            "createdAt": 1
        }
    },
    {
        $match: matchQuery
    },
    {
        $facet: {
            metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
            data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
        }
    }]



    settlement.aggregate(query)
        .then((detail) => {
            settlement.aggregate([
                {
                    $match: {
                        status: "Settled",
                        seller_id: Id
                    }
                },
                {
                    $group: {
                        _id: null,
                        totalEarning: {
                            $sum: { $toDouble: '$amount' }
                        }
                    }
                }
            ])
                .then((totalAmount) => {
                    res.status(200).json({
                        title: 'Settlement Fetched successfully.',
                        error: false,
                        detail,
                        totalEarning: totalAmount && totalAmount.length > 0 ? totalAmount[0].totalEarning : 0
                    })
                })
        })
}


const getDispatcedOrders = (req, res) => {

    let start = moment().subtract(2, 'months').date(1).format('YYYY-MM-DD');
    let end = moment().endOf('month').format('YYYY-MM-DD');
    let startDate = new Date(start);
    let endDate = new Date(end);
    console.log('=-0=-0=-0',startDate,endDate)
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    Order.aggregate([
        {
            $match: {
                "seller_id": ObjectId(Id),
                is_deleted: false,
                requested: { $eq: 'Processed' },
                $and:[{"createdAt": { $gt: startDate }},{"createdAt": { $lt: endDate }}]
            }
        },
        {
            $group: {
                _id: { $substr: ['$createdAt', 5, 2] },
                value: { $sum: 1 }
            }
        }
    ]).then((result) => {
        res.status(200).json({
            error: false,
            detail: result
        })
    })
}

const getDashboardLeastSellingProducts = async (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    let lastTwoMonth = new Date(moment().subtract(60, "days"));
    let match = req.body.limit === 5 ? {
        year: req.body.year,
        month: getMonthNumber(req.body.month)
    } : {};

    let searchCondition = {
        "seller_id": Id,
        "createdAt": { $gt: lastTwoMonth }
    }
    Order.aggregate([
        {
            $match: searchCondition
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: match
        },
        {
            $project: {
                products: 1,
            }
        },
        {
            $unwind: "$products"
        },
        {
            $group: {
                _id: "$products.inventory_id",
                quantity: { $sum: { $toInt: "$products.quantity" } },
                mycount: { $sum: 1 }
            }
        },
        {
            $match: {
                mycount: { $lte: 4 }
            }
        },


        {
            $lookup: {
                localField: "_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: "Inventory.product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        {
            $lookup: {
                localField: "Product.company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $project: {
                "Product.name": 1,
                "Company.name": 1,
                "mycount": 1

            }
        },
        {
            $limit: 5
        }
    ])
        .then((result) => {
            res.status(200).json({
                error: false,
                detail: result
            })
        })
}

const getStats = async (req, res) => {
    seller_id = req.body.seller_id
    if (seller_id) {
        active_products = await Inventory.aggregate([
            {
                $match: {
                    isDeleted: false,
                    isHidden: false,
                    user_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: null, count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail[0].count
        });
        active_products = await Inventory.aggregate([
            {
                $match: {
                    isDeleted: false,
                    isHidden: false,
                    user_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: null, count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail[0].count
        });

        active_products = await Inventory.aggregate([
            {
                $match: {
                    isDeleted: false,
                    isHidden: false,
                    user_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: null, count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail.length > 0 ? detail[0].count : 0
        });
        orders = await Order.aggregate([
            {
                $match: {
                    seller_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: null, count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail.length > 0 ? detail[0].count : 0
        });
        buyers = await Order.aggregate([
            {
                $match: {
                    seller_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: "$user_id", count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail.length > 0 ? detail[0].count : 0
        });
        buyers = await Order.aggregate([
            {
                $match: {
                    seller_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: "$user_id", count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail.length > 0 ? detail[0].count : 0
        });
        ratings = await Rating.aggregate([
            {
                $match: {
                    seller_id: ObjectId(req.body.seller_id)
                }
            },
            { $group: { _id: "$seller_id", count: { $sum: 1 } } },
        ]).then((detail) => {
            return detail.length > 0 ? detail[0].count : 0
        });

        res.status(200).json({
            error: false,
            detail: {
                active_products,
                orders,
                buyers,
                ratings
            }
        })
    }
    else {
        res.status(200).json({
            error: true,
            detail: "Provide: seller_id"
        })
    }

}
const resetPassStaffMember = (req, res) => {
    let password = randomstring.generate({
        length: 6,
        charset: 'alphanumeric'
    });

    try {
        user.findByIdAndUpdate({ _id: req.body.id }, { $set: { password: bcrypt.hashSync(password, bcrypt.genSaltSync(10)) } }, { 'new': true }).then((result) => {
            if (result) {
                var mailData = {
                    email: result.email,
                    subject: 'Password reset successful',
                    body: 'Your Medimny account password has been reset.' + '<br/>' + ' Please use the following credentials for login' + '<br/>' + '<b>Email :</b>' + result.email + '<br/><b> Password :</b>' + password + '<br/>Thank you, Team Medimny.'
                };
                helper.sendEmail(mailData);

                // let msgBody = 'Congratulations, Your Medimny account has been created.' + '<br/>' + ' Please use the following credentials for login' + '<br/>' + '<b>Email :</b>' + result.email + '<br/><b> Password :</b>' + password + '<br/>Thank you, Team Medimny.'
                // helper.sendSMS(result.phone, msgBody);

                return res.status(200).json({
                    title: "Password sent successfully.",
                    error: false
                })
            } else {
                return res.status(200).json({
                    title: "User not found.",
                    error: true
                })
            }
        })
    } catch (error) {
        return res.status(200).json({
            title: "Something went wrong, Please try again",
            error: true,
            detail: error
        })
    }
}

const cancelProductRequest = (req, res) => {
    ProductRequest.findOneAndUpdate({ _id: ObjectId(req.body.id) }, { $set: { status: req.body.status } }, { new: true })
        .then((data) => {
            if(data){
                res.status(200).json({
                    error: false,
                    message: "product request cancelled",
                });
            }
            else{
                res.status(200).json({
                    error: true,
                    message: "Something went wrong !",
                });
            }
        })
        .catch(err => {
            res.status(200).json({
                error: true,
                message: "Something went wrong !",
            });
        })
}


module.exports = {
    listingSellers,
    getSellerDetails,
    vacationMode,
    getPermissionModule,
    getGroupModule,
    addGroupModule,
    editGroupModule,
    deleteGroupModule,
    getStaffList,
    editStaffMember,
    addStaffMember,
    deleteStaff,
    getGroupList,
    getDataforProductRequest,
    addProductRequest,
    getRequestedProductList,
    getDashBoardCardDetails,
    getDashboardTopSellingProducts,
    getDataForChart,
    getGraphForDahboard,
    getMonthNumber,
    getSettlements,
    getDispatcedOrders,
    getDashboardLeastSellingProducts,
    getStats,
    resetPassStaffMember,
    cancelProductRequest
}
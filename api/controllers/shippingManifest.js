const async = require('async');
const randomstring = require('randomstring');
const moment = require('moment');
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const order = require('../models/order');
const jwt = require('jsonwebtoken');
const shippingManifest = require('../models/shippingManifest');
const fs = require('fs');
const { findOne } = require('../models/order');
var ejs = require("ejs");


const markManifested = async(req, res) => {
  try{
      if(req.body.orderArr && req.body.orderID){
          async.forEach(req.body.orderArr, function (data, cb) {
              if (data) {
                  order.findByIdAndUpdate({ _id: data._id }, {
                      $set: { isManifested: true }
                  }).exec();
                  cb()
              } else {
                  cb()
              }
          }, async function (err) {
          let shippingMData = {
              date: new Date(),
              seller_id: req.user._id,
              order_id: req.body.orderID,
              name: req.body.name,
              phone: req.body.phone
          }
          let newData = new shippingManifest(shippingMData);
          newData.save().then();
              return res.status(200).json({
                  title: 'Manifest generated successfully.',
                  error: false
              });
          })
      }else{
          
          return res.status(200).json({
              title: 'Something went wrong.',
              error: true
          })
      }
  }catch (err) {
      return res.status(200).json({
          title: 'Something went wrong.',
          error: true
      })
  }
}

const getManifest = (req, res) => {
  let page = req.body.page ? req.body.page : 1;
  let perPage = req.body.perPage ? req.body.perPage : 1;

  shippingManifest.aggregate([
      {
          $match: {
              seller_id: ObjectId(req.user._id)
          }
      },
      {
          $lookup: {
              localField: 'seller_id',
              foreignField: '_id',
              from: 'users',
              as: 'seller_id'
          }
      },
      {
          $unwind: {
              path: '$seller_id',
              preserveNullAndEmptyArrays: true
          }
      },
      { $sort: { createdAt: -1 } },
      {
          $facet: {
              metadata: [{ $count: "total" }, { $addFields: { page: Number(page) } }],
              data: [{ $skip: (Number(perPage) * Number(page)) - Number(perPage) }, { $limit: perPage ? Number(perPage) : 10 }]
          }
      }
  ]).then((detail) => {
      res.status(200).json({
          error: false,
          title:'Success',
          detail
      })  
  })
}

const getManifestReport = async (req, res) => {
    let list = await shippingManifest.findOne({ _id: ObjectId(req.query.Id) }).populate('seller_id').exec();
    const contents = fs.readFileSync('./views/manifest.ejs', 'utf8');
    // return res.status(200).send(contents);
    let orderData = []
    async.forEachOfSeries(list.order_id,(data,key,cb)=>{
        if(data){
            order.findOne({ order_id: data }).populate('user_id').then((data1)=>{
            orderData.push(data1)
            cb()
        })
        } else{
            cb()
        }
    },async function(){
        const samDetails = {
            title: "Sameer",
            sellerName: list.seller_id.company_name,
            manifestDate: moment(list.date).format('DD/MM/YYYY, h:mm a'),
            sellerId: list.seller_id.sellerId,
            orderArr: orderData,
            name: list.name,
            phone: list.phone
        }
        const html = ejs.render(contents, samDetails);
        res.status(200).send(html)
    })
}

const deleteManifested = async(req, res) => {
    try{
        if(req.query.Id){
            let data = await shippingManifest.findOne({ _id: ObjectId(req.query.Id) }).exec();
            async.forEach(data.order_id, function (data, cb) {
                if (data) {
                    order.findOneAndUpdate({ order_id: data }, {
                        $set: { isManifested: false }
                    }).exec();
                    cb()
                } else {
                    cb()
                }
            }, async function (err) {
                shippingManifest.deleteOne({ _id: ObjectId(req.query.Id) }).exec();
                return res.status(200).json({
                    title: 'Manifest deleted successfully.',
                    error: false
                });
            })
        }else{
            
            return res.status(200).json({
                title: 'Something went wrong.',
                error: true
            })
        }
    }catch (err) {
        return res.status(200).json({
            title: 'Something went wrong.',
            error: true
        })
    }
  }

module.exports = {
  markManifested,
  getManifest,
  getManifestReport,
  deleteManifested

}

var Product = require('../models/product');
var ProductGst = require('../models/gst');
var ProductCompany = require('../models/company');
var ProductCat = require('../models/product_category');
var ProductType = require('../models/productType');
var GSTval = require('../models/gst');
var productAutoComp = require('../models/product_autocomp');
const helper = require('../lib/helper');
var Order = require('../models/order')
const async = require('async');
var mongoose = require('mongoose');
const inventory = require('../models/inventory');
var ObjectId = mongoose.Types.ObjectId;
const moment = require("moment")
var mediCategory = require('../models/medicine_type');
const ActiveInventory = require('../models/activeInventory');
const { Parser } = require('json2csv');
/*
# parameters: token, name, company_id, product_cat_id, chem_combination, GST, Type, images which will be an array of base64 image.
# purpose: Add products in product bank
*/
const addProduct = (req, res) => {
    req.checkBody('name', 'Product Name is required').notEmpty();
    req.checkBody('company_id', 'Company Name is required').notEmpty();
    req.checkBody('product_cat_id', 'Product Category is required').notEmpty();
    req.checkBody('GST', 'GST is required').notEmpty();
    req.checkBody('Type', 'Type is required').notEmpty();
    req.checkBody('chem_combination', 'Chemical combination is required').notEmpty();
    req.checkBody('medicineCategory', 'Medicine type is required').notEmpty();
    req.checkBody('surcharge', 'Surcharge is required').notEmpty();



    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let { name, company_id, product_cat_id, GST, Type, chem_combination, images, sku, countryOfOrigin, medicineCategory, surcharge, isPrepaid, description } = req.body;
        let imageArray;
        let dir = name;
        if (req.body.images.length > 0) {
            imageArray = images.map((value, key) => {
                return helper.base64UploadS3(req, res, dir, value);
            }).filter((e) => e !== undefined);
        }
        let newProduct = new Product({
            name,
            company_id,
            product_cat_id,
            GST,
            Type,
            chem_combination,
            images: imageArray ? imageArray : '',
            sku,
            country_of_origin: countryOfOrigin,
            medicine_type_id: medicineCategory,
            surcharge: surcharge,
            isPrepaid: isPrepaid ? isPrepaid : false,
            description
        });

        newProduct.save((error, savedProduct) => {
            if (error) {
                return res.status(200).json({
                    title: "Something went wrong, Please try again later.",
                    error: true,
                });
            }
            else {
                return res.status(200).json({
                    title: "Product added successfully.",
                    error: false,
                    savedProduct
                });
            }
        });
    }
}

const editProductDetail = (req, res) => {
    req.checkBody('name', 'Product Name is required').notEmpty();
    req.checkBody('company_id', 'Company Name is required').notEmpty();
    req.checkBody('product_cat_id', 'Product Category is required').notEmpty();
    req.checkBody('GST', 'GST is required').notEmpty();
    req.checkBody('Type', 'Type is required').notEmpty();
    req.checkBody('chem_combination', 'Chemical combination is required').notEmpty();
    req.checkBody('medicineCategory', 'Medicine type is required').notEmpty();
    req.checkBody('surcharge', 'Surcharge is required').notEmpty();
    req.checkBody('id', 'Product id is required').notEmpty();



    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        console.log('body', req.body)
        let { id, name, company_id, product_cat_id, GST, Type, chem_combination, images, sku, isPrepaid, countryOfOrigin, medicineCategory, surcharge,description } = req.body;
        let dir = name;
        Product.findById(id)
            .then((products, error) => {
                if (error) {
                    return res.status(200).json({
                        error: true,
                        title: 'Something went wrong please try again.',
                        errors: errors
                    });
                }
                if (!products) {
                    return res.status(200).json({
                        error: true,
                        title: 'No product found.',
                        errors: errors
                    });
                }
                if (products) {
                    if (images && images.length > 0) {
                        let imageArray = images.map((value, key) => {
                            return helper.base64UploadS3(req, res, dir, value);
                        }).filter((e) => e !== undefined);

                        products.images = imageArray;
                        console.log('imageArray',imageArray)
                    }
                    else{
                        products.images = [];
                    }


                        products.name = name !== "" ? name : products.name,
                        products.company_id = company_id !== "" ? company_id : products.company_id,
                        products.product_cat_id = product_cat_id !== "" ? product_cat_id : products.product_cat_id,
                        products.GST = GST !== "" ? GST : products.GST,
                        products.Type = Type !== "" ? Type : products.Type,
                        products.chem_combination = chem_combination !== "" ? chem_combination : products.chem_combination,
                        products.sku = sku !== "" ? sku : products.sku,
                        products.isPrepaid = isPrepaid !=="" ? isPrepaid : products.isPrepaid,
                        products.country_of_origin = countryOfOrigin,
                        products.medicine_type_id = medicineCategory !== "" ? medicineCategory : products.medicine_type_id,
                        products.surcharge = surcharge !=="" ? surcharge : products.surcharge,
                        products.description = description !=="" ? description : products.description
                    products.save(async(error, savedProduct) => {
                        if (error) {
                            return res.status(200).json({
                                title: "Something went wrong, Please try again later.",
                                error: true,
                            });
                        }
                        else {
                            Product.findById(id)
                                .populate('company_id')
                                .populate('product_cat_id')
                                .populate('medicine_type_id')
                                .populate('Type')
                                .populate('GST').then(async(product) => {
                                    if(product){
                                        await inventory.updateMany({product_id:ObjectId(id)}, {$set:{
                                            company_id:ObjectId(product.company_id._id),
                                            medicineType_id:ObjectId(product.medicine_type_id._id),
                                            medicineTypeName:product.medicine_type_id.name,
                                            productName:product.name
                                        }}).exec();
                                        await ActiveInventory.updateMany({'Product._id':ObjectId(id)}, {$set:{
                                                medi_type:product.medicine_type_id.name,
                                                'Product.name': product.name,
                                                'Product.images': product.images,
                                                'Product.company': product.company_id.name,
                                                'Product.chem_combination': product.chem_combination, 
                                                'Product.product_type':product.Type.name,
                                                'Product.country_of_origin': product.country_of_origin,
                                                'Product.GST':product.GST.value,
                                                'Product.isPrepaid':product.isPrepaid,
                                                'Product_Category._id':product.product_cat_id._id,
                                                'Product_Category.name':product.product_cat_id.name,
                                                'Company._id':product.company_id._id,
                                                'Company.name':product.company_id.name,
                                                'Product.description': product.description,
                                            }
                                        }).exec();
                                        return res.status(200).json({
                                            title: "Product updated successfully.",
                                            error: false,
                                            detail: savedProduct
                                        });
                                    }
                                })
                        }
                    });
                }
            });
    }
}

const searchProduct = (req, res) => {
    Product
        .find({ isDeleted: false, name: { $regex: new RegExp('^' + req.body.name), $options: 'i' } })
        .limit(20)
        .populate('company_id')
        .populate('product_cat_id')
        .populate('medicine_type_id')
        .populate('GST')
        .populate('Type')
        .then((results) => {
            res.status(200).json({
                error: false,
                detail: results
            })
        })
}

const productAutofill = (req, res) => {
    var key = req.query.key ? req.query.key : "";
    inventory.aggregate([
        {
            $match: {
                isDeleted: false,
                isHidden: false
            }
        },
        {
            $lookup: {
                from: 'products',
                localField: 'product_id',
                foreignField: '_id',
                as: 'Products'
            }
        },
        {
            $unwind: '$Products'
        },
        {
            $match: {
                'Products.name': { $regex: new RegExp('^' + key), $options: 'i' }
            }
        },

        {
            $group: {
                _id: { name: '$Products.name', id: '$Products._id' }
            }
        },
        {
            $project: {
                _id: 0,
                word: '$_id.name',
                data: ['$_id.id', '$_id.name']
            }
        },
        {
            $sort: {
                'word': 1
            }
        },
        {
            $limit: 20
        }
    ])
        .then((words, err) => {
            if (err) {
                res.json({
                    error: true,
                    message: err
                });
            }
            else {
                res.json({
                    error: false,
                    data: words
                });
            }
        });
}

const byCompany = (req, res) => {
    if (req.body.company_id) {
        let query = req.body.category_id ? { isDeleted: false, company_id: req.body.company_id, product_cat_id: req.body.category_id }
            : { isDeleted: false, company_id: req.body.company_id }
        let limit = req.body.perPage ? Number(req.body.perPage) : parseInt(20);
        let skip = (parseInt(req.body.page ? req.body.page : 1) - 1) * parseInt(limit);    
        Product
            .find(query)
            .skip(skip).limit(limit)
            .populate('company_id')
            .populate('product_cat_id')
            .populate('GST')
            .populate('Type')
            .populate('medicine_type_id')
            .then((results) => {
                Product.countDocuments(query).then((count)=>{
                    let detail = { results, count}
                    res.status(200).json({
                        title:'success',
                        error: false,
                        detail
                    })
                })
            })
    }
    else {
        res.status(200).json({
            error: true,
            detail: "Provide company_id & category_id."
        })
    }
}

const list = async (req, res) => {
    let products = await Product.list(req.body);
    let companies = await ProductCompany.find({ isDeleted: false }, { name: 1, _id: 1 }).sort({ name: 1 });
    let types = await ProductType.find({ isDeleted: false }, { name: 1, _id: 1 }).sort({ name: 1 });
    let categories = await ProductCat.find({ isDeleted: false }, { name: 1, _id: 1 }).sort({ name: 1 });
    let GST = await GSTval.find({ isDeleted: false }, { value: 1, _id: 1 }).sort({ name: 1 });
    detail = {
        products,
        categories,
        types,
        GST,
        companies
    }
    res.status(200).json({
        error: false,
        detail
    })
}

const deleteProduct = (req, res) => {
    try{
        Product.findOneAndUpdate({ _id:ObjectId(req.body.id), isDeleted:false}, {$set:{isDeleted:true}})
        .then(async(data) => {
            if(data){
                await inventory.updateMany({product_id: ObjectId(req.body.id),isDeleted:false}, {$set:{isDeleted:true}})
                .then(async(data1) => {
                    if(data1){
                        await ActiveInventory.deleteMany({'Product._id':ObjectId(req.body.id)}).then(data2 =>{
                            if(data2){
                                res.status(200).json({
                                    error: false,
                                    title:"Product deleted successfully"
                                }) 
                            }
                        })
                    }
                })
            }
            else{
                res.status(200).json({
                    error: true,
                    title:"Something went wrong"
                })
            }
        }).catch(err => {console.log(err)})
    }catch(e){console.log(e)}
}


const top5least5 = async (req, res) => {
    let query = [];
    matchQry = {};
    let d = new Date();
    let monthN = d.getMonth() + 1;
    let year = d.getFullYear();
    // matchQry["createdAt"] = { $gte: new Date((new Date().getTime() - (30 * 24 * 60 * 60 * 1000))) };
    matchQry["order_status.status"] =  { $ne: 'Cancelled' } ;
    matchQry["paymentStatus"] = { $ne : 'pending' };
    let matchDateQuery = (req.body.month && req.body.year) ? {
        "year":  Number(req.body.year) ,
        "month":  getMonthNumber(req.body.month) 
    } : {
        "year":  year,
        "month": monthN
    }
    if (req.body.id) {
        if (req.body.user_type === "seller") {
            matchQry["seller_id"] = ObjectId(req.body.id)
        }
        if (req.body.user_type === "buyer") {
            matchQry["user_id"] = ObjectId(req.body.id)
        }
    }
    query.push({
        $match: matchQry
    })
    query.push(
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: matchDateQuery
        }
    )
    query.push({ $unwind: '$products' })
    let lastOrders = await Order.aggregate(query);
    products = []
    ps = [];
    async.forEach(lastOrders, function (order, cb) {
            let index = products.findIndex((val) => val.name == order['products']['name'])
            if(index>-1){
                products[index].qty = products[index].qty + parseInt(order.products.quantity)
                cb();
            }else{
                products.push({name:order['products']['name'],qty:parseInt(order.products.quantity),ptr: order.products.PTR})
                cb();
            }
        
    }, async function (err) {
        top5 = await products.sort((a, b) => (a.qty > b.qty) ? -1 : 1).slice(0, 10);
        least5 = await products.sort((a, b) => (a.qty > b.qty) ? 1 : -1).slice(0, 10);
        res.status(200).json({
            error: false,
            top5,
            least5
        })
    });
}
const getMonthNumber = (data) => {
    let val = data.toLowerCase()
    switch (val) {
        case 'january':
            return 1;
            break;

        case 'february':
            return 2;
            break;

        case 'march':
            return 3;
            break;

        case 'april':
            return 4;
            break;

        case 'may':
            return 5;
            break;

        case 'june':
            return 6;
            break;

        case 'july':
            return 7;
            break;

        case 'august':
            return 8;
            break;

        case 'september':
            return 9;
            break;

        case 'october':
            return 10;
            break;

        case 'november':
            return 11;
            break;

        case 'december':
            return 12;
            break;
        default:
            return 1
    }
}
const bestOffer = async(req, res) =>{//
    let list=[];
    let product;
    let data;
    let dateAfter3Months = moment().add(3, "M");
    ActiveInventory.find({ 'Product._id': ObjectId(req.body.Id), quantity:{$gt:0}, 
    $expr:{$gt:["$quantity", "$min_order_quantity"]}, expiry_date: { $gt: new Date(dateAfter3Months) }})
    .then((data)=>{
        if(data){
        async.eachOfSeries(data, (prod,key,cb)=>{
            if(prod){
                if(prod.Discount && prod.Discount.name == 'Same'){
                    product = ((prod.PTR * prod.Discount.discount_on_product.purchase) /
                    (Number(prod.Discount.discount_on_product.purchase) + Number(prod.Discount.discount_on_product.bonus)));
                    prodData = prod.Product;
                    disc = prod.Discount;
                    list.push({price:product,data:prodData,disc:disc,minQ:prod.min_order_quantity})
                    cb();
                }else if(prod.Discount && prod.Discount.name == 'Discount'){
                    product = prod.PTR - (prod.PTR/100 * Number(prod.Discount.discount_per));
                    prodData = prod.Product;
                    disc = prod.Discount;
                    list.push({price:product,data:prodData,disc:disc,minQ:prod.min_order_quantity})
                    cb();
                }else if(prod.Discount && prod.Discount.name == 'Different'){
                    inventory.find({ _id: prod.Discount.discount_on_product.inventory_id }).populate('product_id').then((result) => {
                        product = prod.PTR;
                        prodData = prod.Product;
                        disc = prod.Discount;
                        list.push({ price: product, data: prodData, disc: disc, inven: result,minQ:prod.min_order_quantity })
                        cb();
                    }).catch((err) => {
                        cb()
                    });
                }else{
                    product = prod.PTR ;
                    prodData = prod.Product;
                    list.push({price:product,data:prodData,minQ:prod.min_order_quantity})
                    cb();
                }
            }else{
                cb();
            }
        },async function(err){
            let offer = await list.sort((a,b)=> ( a.price > b.price ) ? 1 : -1)
            res.status(200).json({
                error: false,
                data: offer,
                title:'Success'
            })
        })  
        }else{
            res.status(200).json({
                error:true,
                title:'Failed'
            })
        }
    })

}
 const addCountryOfOrigin = async(req,res) => {
    let result = await Product.find().exec();
    console.log('err-=-=-',result.length)
        async.eachOfSeries(result, async(data,key, cb)=> {
            if (data) {
                await Product.findByIdAndUpdate({ _id: data._id},{$set:{country_of_origin:'India'}},{useFindAndModify:false}).exec();
            } else{
                cb()
            }
        }, function (err) {
            console.log('err',err)
            return res.status(200).json({
                title: 'Order details fetched successfully.',
                error: false,
            });
        })
 }
const addMediCategoryToProd = async (req, res) => {
    let medi = await mediCategory.findOne({ name: 'Ethical branded' }).exec();
    console.log('err-=-=-', medi)
    Product.find().then((result) => {
        async.eachOfSeries(result, function (data, key, cb) {
            if (data) {
                Product.findByIdAndUpdate({ _id: data._id }, { $set: { medicine_type_id: medi._id } }, { useFindAndModify: false }).exec();
                console.log('err-=-=-data', data)
            }
            cb()
        }, function (err) {
            return res.status(200).json({
                title: 'success',
                error: false,
            });
        })
    })

}


const removeTabFromProduct = async(req, res) =>{
    Product.find({}).then(products => {
        async.eachOfSeries(products, async function(eachProduct, key, cb){
            if (eachProduct) {
                if(/ +(?= )/.test(eachProduct.name)){
                    console.log('with extre spaces',eachProduct.name)
                    let changedName = (eachProduct.name).replace(/ +(?= )/g, '')
                    let y = / +(?= )/.test(changedName)
                    console.log(y, changedName)
                    await Product.findByIdAndUpdate({ _id: ObjectId(eachProduct._id) }, { $set: { name: changedName } }).exec();
                    await inventory.updateMany({product_id: ObjectId(eachProduct._id)},{$set: {productName:changedName}}).exec();
                    await ActiveInventory.updateMany({'Product.name':eachProduct.name}, {$set:{'Product.name':changedName}}).exec();
                }
            }
            else{
                cb()
            }
        }, function () {
            res.status(200).json({title:'success'})
        })
    })
}

const getProductSurCharge = async (req, res) => {
    let fields = ["No", "Name", "SurCharge"];
    let prodArr = []
    Product.find({ surcharge: { $gt: 0 }, isDeleted: false }).sort({ name: 1 }).then((detail) => {
        async.eachOfSeries(detail, function (prod, key, cb) {
            if (prod) {
                prodArr.push({
                    No: key + 1,
                    'Name': prod.name,
                    'SurCharge': prod.surcharge,
                })
            }
            cb()
        }, function (err) {
            var json2csvParser = new Parser({ fields: fields });
            const csv = json2csvParser.parse(prodArr);
            res.attachment(`ProductWithSurCharge.csv`);
            res.status(200).send(csv);
        });
    })
}

module.exports = {
    addProduct,
    searchProduct,
    editProductDetail,
    productAutofill,
    byCompany,
    list,
    deleteProduct,
    top5least5,
    bestOffer,
    addCountryOfOrigin,
    addMediCategoryToProd,
    removeTabFromProduct,
    getProductSurCharge
}
var mongoose = require('mongoose');

var System = require('../models/systemConfig')
var ObjectId = mongoose.Types.ObjectId;




const getAllSystemConfig = (req, res) => {
  System.getAllSystemConfig(req, res);
}

const changeGlobalCod = (req, res) => {
  System.changeGlobalCod(req, res);
}

const getDeviceVersion = (req, res) => {
  System.find({ key: { $in: ['android_version', 'ios_version'] } }).then((result) => {
    res.status(200).json({
      error: false,
      data: result
    })
  })
    .catch((error) => {
      res.status(200).json({
        error: true,
        title: error
      })
    })
}

module.exports = {
  getAllSystemConfig,
  changeGlobalCod,
  getDeviceVersion
}
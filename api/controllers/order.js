const async = require('async');
const randomstring = require('randomstring');
const request = require("request");
const moment = require('moment');
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const user = require('../models/user');
const order = require('../models/order');
const inventory = require('../models/inventory');
const notification = require('../models/notification');
const helper = require('../lib/helper');
const sellerController = require('../controllers/seller');
var cartModel = require('../models/cart');
var transactions = require('../models/transactions');
const { Parser } = require('json2csv');
const checksum_lib = require('./checksum');
const pincode_serving = require('../models/pincodeServing')
const fs = require('fs');
var pdf = require("html-pdf");
var ejs = require("ejs");
var path = require("path");
var bodyParser = require("body-parser");
var axios = require('axios')
const jwt = require('jsonwebtoken');
var courierService = require('../lib/CourierService');
const systemActivity = require('../models/systemActivity');
const systemConfig = require('../models/systemConfig');
const systemActivityCon = require('./systemActivity');
const { findOne } = require('../models/user');
const mediType = require('../models/medicine_type');
const shortBook = require('../models/shortBook');
const ActiveInventory = require('../models/activeInventory');
const invenCon = require('./inventory');

var PaytmConfig = {
    mid: "LFLTrO22899669406687",
    key: "ORGUmBAjNHAbV45v",
    website: "WEBSTAGING"
}

/*
# parameters: token,
# purpose: listing of all orders for seller or buyer
*/
const getMonthNumber = (data) => {
    let val = data.toLowerCase()
    switch (val) {
        case 'january':
            return 1;
            break;

        case 'february':
            return 2;
            break;

        case 'march':
            return 3;
            break;

        case 'april':
            return 4;
            break;

        case 'may':
            return 5;
            break;

        case 'june':
            return 6;
            break;

        case 'july':
            return 7;
            break;

        case 'august':
            return 8;
            break;

        case 'september':
            return 9;
            break;

        case 'october':
            return 10;
            break;

        case 'november':
            return 11;
            break;

        case 'december':
            return 12;
            break;
        default:
            return 1
    }
}

const orderListing1 = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    let query = {}
    if(req.user.user_type != 'admin'){
        query = { seller_id:ObjectId(Id),is_deleted: false }
    }else{
        query = { is_deleted: false }
    }
    let firstDay = new Date(req.body.from_date)
    let lastDay = new Date(req.body.to_date)
    //
    let orQuery=[]
    if( req.body.searchText ){
        orQuery.push(
            { "order_id": { $regex: req.body.searchText, $options: 'i' } },
            { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } },
            {"buyerCompName": { $regex: req.body.searchText, $options: 'i' }},
            {"sellerCompName": { $regex: req.body.searchText, $options: 'i' }}
            )
    }
    let matchQuery = [
        { "createdAt": { $gte: firstDay } },
        { "createdAt": { $lt: lastDay } },
        {'paymentStatus':{$ne:'pending'}},
        { 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } },{ 'order_status.status': { $ne: 'RTO' } }
    ]
    if(req.body.status){
            query = {
                ...query, $expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, req.body.status] },
                $and: [{ $expr: { $gte: [{ "$arrayElemAt": ["$order_status.date", -1] }, firstDay] } },
                { $expr: { $lt: [{ "$arrayElemAt": ["$order_status.date", -1] }, lastDay] } },
                { 'paymentStatus': { $ne: 'pending' } }]
            }
    }else{
        query={...query, $and: matchQuery }
    }
    if(req.body.searchText && req.body.status){
        query = { ...query, $or: orQuery }
    }
    //
    if (req.body.isManifested === false) {
        query = {...query, isManifested: req.body.isManifested, requested: 'Processed'}
    }
    if(req.body.type && req.body.type !='Bulk'){
        query = {...query, paymentType: req.body.type}
    }else if(req.body.type && req.body.type == 'Bulk'){
        query = {...query, isBulk: true}
    }
    if(req.body.seller_id && req.user.user_type == 'admin'){
        query = {...query, seller_id: ObjectId(req.body.seller_id)}
    }
    if(req.body.buyer_id && req.user.user_type == 'admin'){
        query = {...query, user_id: ObjectId(req.body.buyer_id)}
    }
    let query2 = {path:'user_id'}
    if(req.body.searchText){
        query = {...query, $or: [{ "order_id": { $regex: req.body.searchText, $options: 'i' } },
        { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } },
        {"buyerCompName": { $regex: req.body.searchText, $options: 'i' }},
        {"sellerCompName": { $regex: req.body.searchText, $options: 'i' }}]}
    }
    let limit = parseInt(100);
    let skip = (parseInt(req.body.page ? req.body.page : 1) - 1) * parseInt(limit);
    order.find(query).skip(skip).limit(limit).populate('seller_id').populate(query2).sort({'createdAt':-1}).then((detail)=>{
        order.countDocuments(query).then((count)=>{
            let data ={ detail,count}
            res.status(200).json({
             title:'success',
             error: false,
             data
            })
        })
    })
};

const orderListing = (req, res) => {
    let query = {}
    let query2 = {
        isPending: {

            $and: [{
                $eq: [
                    '$paymentType',
                    'Online'
                ]
            },
            {
                $eq: [
                    '$paymentStatus',
                    'pending'
                ]
            }
            ]

        }
    }
    if (req.user.user_type === "admin") {
        query = {
            is_deleted: false,
            $and: [{ 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } },{ 'order_status.status': { $ne: 'RTO' } }]
        }
        if (req.body.seller_id && req.body.seller_id !== '' && req.body.seller_id !== undefined) {
            query['seller_id'] = ObjectId(req.body.seller_id);

        }
    }
    else {

        let Id = req.user.mainUser ? req.user.mainUser : req.user._id
        query = {
            seller_id: ObjectId(Id),
            is_deleted: false,
            $and: [{ 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } },{ 'order_status.status': { $ne: 'RTO' } }]
        }
    }

    // let matchQuery = {}
    let month = req.body.month ? req.body.month : new Date()
    let d = new Date();
    let monthN = d.getMonth() + 1;
    let year = d.getFullYear();
    let firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
    let lastDay = new Date(d.getFullYear(), d.getMonth() + 1);
    if (req.body.from_date && req.body.from_date != '') {
        firstDay = new Date(req.body.from_date)
    }
    if (req.body.to_date && req.body.to_date != '') {
        lastDay = new Date(req.body.to_date)
    }
    // if((req.body.from_date == req.body.to_date) && req.body.to_date != '' && req.body.from_date !=''){
    //     firstDay = new Date(req.body.from_date);
    //     lastDay = new Date(req.body.to_date);
    //     firstDay.setHours(0,0,0,0);
    //     lastDay.setHours(23,59,59,999);  
    // }
    // firstDay.setHours(0, 0, 0, 0);
    // lastDay.setHours(23, 59, 59);
    let matchDateQuery = (req.user.user_type === "admin" || req.user.user_type === "seller" && req.body.from_date && req.body.to_date && req.body.status =='' ||req.body.status =='Requested') ? {
        $and: [{ "createdAt": { $gte: firstDay } }, { "createdAt": { $lt: lastDay } }]

    } : ((req.user.user_type === "admin" || req.user.user_type === "seller") && req.body.status) ? {
        $and: [{ "current_status.date": { $gte: firstDay } }, { "current_status.date": { $lte: lastDay } }]

    } : (req.user.user_type === "admin" || req.user.user_type === "seller" && req.body.month && req.body.year) ? {
        "year": req.body.year ? Number(req.body.year) : year,
        "month": req.body.month ? getMonthNumber(req.body.month) : monthN
    } : {}
    //matchQuery = req.body.searchText ? { order_id: { $regex: req.body.searchText, $options: 'i' } } : {}
    let matchQuery = req.body.searchText ? {
        $or: [{ "order_id": { $regex: req.body.searchText, $options: 'i' } }, { "user_id.company_name": { $regex: req.body.searchText, $options: 'i' } },
        { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } }]
    } : {}

    if (req.body.status && req.body.status != '' && req.body.status != 'Requested') {
        query["current_status.status"] = req.body.status;
    } else if (req.body.status == 'Requested') {
        query['requested'] = { $eq: 'Requested' }
    }
    if (req.body.type && req.body.type != '' && req.body.type != 'Bulk') {
        query['paymentType'] = { $eq: req.body.type }
    }
    if (req.body.type && req.body.type == 'Bulk') {
        query['isBulk'] = { $eq: true }
    }
    if (req.body.isManifested === false) {
        query['isManifested'] = { $eq: req.body.isManifested };
        // query["current_status.status"] = 'Processed';
        query['requested'] = { $eq: 'Processed' }
    }
    console.log('=-0=-0query2',query2)
    order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: query
        },
        {
            $addFields: query2
        },
        {
            $match: {
                isPending: false
            }
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'user_id'
            }
        },
        {
            $lookup: {
                localField: 'seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'seller_id'
            }
        },
        {
            $unwind: {
                path: '$seller_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$user_id',
                preserveNullAndEmptyArrays: true
            }
        },
        // {
        //     $unwind: {
        //         path: '$Products',
        //         preserveNullAndEmptyArrays: true
        //     }
        // },
        // {
        //     $lookup: {
        //         localField: 'Products.inventory_id',
        //         foreignField: '_id',
        //         from: 'inventories',
        //         as: 'inventory_id'
        //     }
        // },
        // {
        //     $lookup: {
        //         localField: 'Products.discount_on_product',
        //         foreignField: '_id',
        //         from: 'discounts',
        //         as: 'discount_on_product'
        //     }
        // },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: matchQuery
        },
        { $match: matchDateQuery },
        { $sort: { createdAt: -1 } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
            }
        }
    ])
        .then((detail) => {
            res.status(200).json({
                title: 'Order Fetched successfully.',
                error: false,
                detail
            })
        })
}
const allOrderListing = (req,res)=>{
    let user = req.user.user_type;
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    let query = {}
    let firstDay = req.body.from_date ? new Date(req.body.from_date) : new Date()
    let lastDay = req.body.to_date ? new Date(req.body.to_date) : new Date()
    console.log('before',firstDay, lastDay)
    firstDay.setHours(firstDay.getHours()-5);
    firstDay.setMinutes(firstDay.getMinutes()-30);
    lastDay.setHours(lastDay.getHours()-5);
    lastDay.setMinutes(lastDay.getMinutes()-30);
    console.log('after',firstDay, lastDay)
    let sort = {'createdAt':-1}
    if(req.body.status == 'Pending'){
        query = {...query,
       $and: [{ $expr: {$gte: [{"$arrayElemAt": ["$order_status.date", -1]}, firstDay]}},
       {$expr: {$lt: [{"$arrayElemAt": ["$order_status.date", -1]}, lastDay ]}},
       {'paymentStatus':{$eq:'pending'}}]}
    }else if(req.body.status && req.body.tab == 'orderListing'){
        query = {...query, $expr: {$eq: [{"$arrayElemAt": ["$order_status.status", -1]}, req.body.status]},
        $and: [{ $expr: {$gte: [{"$arrayElemAt": ["$order_status.date", -1]}, firstDay]}},
        {$expr: {$lt: [{"$arrayElemAt": ["$order_status.date", -1]}, lastDay ]}},{'paymentStatus':{$ne:'pending'}}]}
    }else if(req.body.status){
       query = {...query, $expr: {$eq: [{"$arrayElemAt": ["$order_status.status", -1]}, req.body.status]},
       $and: [{ $expr: {$gte: [{"$arrayElemAt": ["$order_status.date", -1]}, firstDay]}},
       {$expr: {$lt: [{"$arrayElemAt": ["$order_status.date", -1]}, lastDay ]}}]}
    }else if(req.body.tab == 'orderListing' ){
        query={...query, $and: [
            { "createdAt": { $gte: firstDay } },
            { "createdAt": { $lt: lastDay } },{'paymentStatus':{$ne:'pending'}},
            { 'order_status.status': { $ne: 'Draft' } },
            { 'order_status.status': { $ne: 'Cancelled' } },
            { 'order_status.status': { $ne: 'Delivered' } },
            { 'order_status.status': { $ne: 'RTO' } }
        ]}
    }else if(req.body.tab == 'orderHistory'){
        sort = {'updatedAt':-1}
        query={...query, $and: [
            { $expr: {$gte: [{"$arrayElemAt": ["$order_status.date", -1]}, firstDay]}},
            {$expr: {$lt: [{"$arrayElemAt": ["$order_status.date", -1]}, lastDay ]}},
            {$or: [
            {$expr: {$eq: [{"$arrayElemAt": ["$order_status.status", -1]}, 'Delivered']}},
            { $expr: {$eq: [{"$arrayElemAt": ["$order_status.status", -1]}, 'Cancelled']}},
            { $expr: {$eq: [{"$arrayElemAt": ["$order_status.status", -1]}, 'RTO']}} ]}
        ]}
    }else{
        query={...query, $and: [
            { "createdAt": { $gte: firstDay } },
            { "createdAt": { $lt: lastDay } }
        ]}
    }
    if(req.body.type && req.body.type !='Bulk'){
        if(req.body.type == 'Online'){
            query = {...query, paymentType: req.body.type, isBulk: false}
        }
        else{
            query = {...query, paymentType: req.body.type}
        }
    }else if(req.body.type && req.body.type == 'Bulk'){
        query = {...query, isBulk: true}
    }
    if (req.body.isManifested === false && user == 'seller') {
        query = {...query, isManifested: { $eq: req.body.isManifested },requested: { $eq: 'Processed' }}
    }
    if(user == 'seller'){
        query = {...query, seller_id: ObjectId(Id), paymentStatus:{$ne:'pending'} }
    }
    if(req.body.seller_id){
        query = {...query, seller_id: ObjectId(req.body.seller_id)}
    }
    if(req.body.buyer_id){
        query = {...query, user_id: ObjectId(req.body.buyer_id)}
    }
    let query2 = {path:'user_id'}
    if(req.body.searchText){
        query = {...query, $or: [{ "order_id": { $regex: req.body.searchText, $options: 'i' } },
        { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } },
        {"buyerCompName": { $regex: req.body.searchText, $options: 'i' }},
        {"sellerCompName": { $regex: req.body.searchText, $options: 'i' }}]}
    }
    if(req.body.orderType && req.body.orderType != ''){
        query = {...query, "orderType": { $eq : req.body.orderType }}
     }
    console.log('body',req.body,new Date(req.body.from_date))
    console.log('query',query,query2)
    let limit = req.body.perPage ? Number(req.body.perPage) : parseInt(100);
    let skip = (parseInt(req.body.page ? req.body.page : 1) - 1) * parseInt(limit);
    order.find(query).skip(skip).limit(limit).populate('seller_id').populate(query2).sort(sort).then((detail)=>{
        order.countDocuments(query).then((count)=>{
            let data ={ detail,count}
            res.status(200).json({
             title:'success',
             error: false,
             data
            })
        })
    })
}
const allOrderListing1 = (req, res) => {
    let query = {}
    let query2 = {
        isPending: {

            $and: [{
                $eq: [
                    '$paymentType',
                    'Online'
                ]
            },
            {
                $eq: [
                    '$paymentStatus',
                    'pending'
                ]
            }
            ]

        }
    }
    if (req.user.user_type === "admin") {
        query = {
            is_deleted: false,
        }
        if (req.body.seller_id && req.body.seller_id !== '' && req.body.seller_id !== undefined) {
            query['seller_id'] = ObjectId(req.body.seller_id);

        } else if (req.body.buyer_id && req.body.buyer_id !== '' && req.body.buyer_id !== undefined) {
            query['user_id'] = ObjectId(req.body.buyer_id);

        }
    }

    // let matchQuery = {}
    let month = req.body.month ? req.body.month : new Date()
    let d = new Date();
    let monthN = d.getMonth() + 1;
    let year = d.getFullYear();
    let firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
    let lastDay = new Date(d.getFullYear(), d.getMonth() + 1);
    if (req.body.from_date && req.body.from_date != '') {
        firstDay = new Date(req.body.from_date)
    }
    if (req.body.to_date && req.body.to_date != '') {
        lastDay = new Date(req.body.to_date)
    }
    firstDay.setHours(0, 0, 0, 0);
    lastDay.setHours(23, 59, 59, 999);
    let matchDateQuery =
        ( req.body.from_date && req.body.to_date && req.body.status == '') ? {
            $and: [{ "createdAt": { $gte: firstDay } }, { "createdAt": { $lte: lastDay } }]
        } : ( req.body.status != "") ?
                    {
                        $and: [{ "current_status.date": { $gte: firstDay } }, { "current_status.date": { $lte: lastDay } }]
                    } : {}

    let matchQuery = req.body.searchText ? {
        $or: [{ "order_id": { $regex: req.body.searchText, $options: 'i' } }, { "user_id.company_name": { $regex: req.body.searchText, $options: 'i' } },
        { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } }]
    } : {}
    if (req.body.status && req.body.status != '' && req.body.status != 'Requested') {
        query["current_status.status"] = req.body.status;
    } else if (req.body.status == 'Requested') {
        query['requested'] = { $eq: 'Requested' }
    }
    if (req.body.type && req.body.type != '' && req.body.type != 'Bulk') {
        query['paymentType'] = { $eq: req.body.type }
    }
    if (req.body.type && req.body.type == 'Bulk') {
        query['isBulk'] = { $eq: true }
    }
    order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: query
        },
        {
            $addFields: query2
        },
        {
            $match: {
                isPending: false
            }
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'user_id'
            }
        },
        {
            $lookup: {
                localField: 'seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'seller_id'
            }
        },
        {
            $unwind: {
                path: '$seller_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$user_id',
                preserveNullAndEmptyArrays: true
            }
        },
        // {
        //     $unwind: {
        //         path: '$Products',
        //         preserveNullAndEmptyArrays: true
        //     }
        // },
        // {
        //     $lookup: {
        //         localField: 'Products.inventory_id',
        //         foreignField: '_id',
        //         from: 'inventories',
        //         as: 'inventory_id'
        //     }
        // },
        // {
        //     $lookup: {
        //         localField: 'Products.discount_on_product',
        //         foreignField: '_id',
        //         from: 'discounts',
        //         as: 'discount_on_product'
        //     }
        // },
        {
            $match: matchQuery
        },
        { $match: matchDateQuery },
        { $sort: { createdAt: -1 } },
        { $limit: 100 },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
            }
        }
    ])
        .then((detail) => {
            res.status(200).json({
                title: 'Order Fetched successfully.',
                error: false,
                detail
            })
        })
}

/*
# parameters: token,
# purpose: Change order Status For Seller
*/
const ChangeStatus = (req, res) => {
    let userData = req.user;
}

/*
# parameters: token,
# purpose: get Order Details by buyer Id or seller Id
*/
const getOrderDetails = (req, res) => {
    let userData = req.user;
    order.findOne({ order_id: req.body.orderId })
        .populate('user_id')
        .populate('seller_id')
        .populate('settlementId')
        .populate('cancelledBy')
        .populate({
            select: 'MRP PTR quantity min_order_quantity max_order_quantity Type GST ePTR discount_name',
            path: 'products.inventory_id',
            populate: {
                path: 'product_id',
                model: 'product',
                populate: {
                    path: "GST",
                    model: 'gst'
                },

            }
        })
        .populate({
            path: 'products.inventory_id',
            populate: {
                path: 'discount_id',
                model: 'discounts',

            },
        })
        .populate({
            path: 'products.inventory_id',
            populate: {
                path: 'product_id',
                model: 'product',
                populate: {
                    path: "Type",
                    model: 'productType'
                }

            },


        })
        .populate({
            select: 'first_name email user_status last_name user_address user_city user_pincode drugLic20B drugLic21B fassaiLic gstLic phone company_name ',
            path: 'user_id',
            populate: {
                path: 'user_state',
                model: 'country'
            }
        })
        .populate({
            select: 'first_name email user_status last_name user_address user_pincode drugLic20B drugLic21B fassaiLic gstLic phone company_name ',
            path: 'seller_id',
            populate: {
                path: 'user_state',
                model: 'country'
            }
        })
        .exec((error, orders) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again..',
                    error: true,
                });
            } else {
                let val = 0
                if (orders) {
                    async.eachOfSeries(orders.products, async function (data, key, cb) {
                        if (!data.discount_on_product && data.discount_per > 0) {
                            let totalPtr = (((data.discount_per / 100) * data.PTR) + data.PTR)
                            let tempVal = totalPtr * Number(data.quantity)
                            let finalValue = ((data.inventory_id.product_id.GST.value / 100) * tempVal) + tempVal
                            val = val + finalValue
                            cb()
                        } else {
                            let tempVal = data.PTR * Number(data.quantity)
                            let finalValue = ((data.inventory_id.product_id.GST.value / 100) * tempVal) + tempVal
                            val = val + finalValue
                            cb()
                        }
                    }, function (err) {
                        return res.status(200).json({
                            title: 'Order details fetched successfully.',
                            error: false,
                            detail: orders,
                            totalValue: val,
                            text: `Change request will mark the changed inventory/inventory's as OUT OF STOCK, Do you want to continue ?`,
                            cancelText: `Cancellation of this order will mark the following as OUT OF STOCK, Do you want to continue ?`
                        });
                    })
                } else {
                    return res.status(200).json({
                        title: 'Order details fetched successfully.',
                        error: false,
                        detail: orders,
                        totalValue: 0
                    });
                }


            }
        })
}



const confirm = async(req, cb) => {
    let userData = req.user;
    let globalCod = systemConfig.findOne({ key: 'globalCod' }).then((result) => result);
    let matchMediType;
    let medicineData;
    let coolChainData;
    let ethicalData;
    let genericData;
    if(req.body.medicineId){
        medicineData = await mediType.findOne({ _id: ObjectId(req.body.medicineId) }).then((result) => result);
        if(medicineData.name == 'Generic' || medicineData.name == 'OTC' || medicineData.name == 'Surgical'){
            matchMediType = { $or:[{'Inventory.medicineTypeName': { $eq: 'Generic' }},
            {'Inventory.medicineTypeName': { $eq: 'OTC' }},
            {'Inventory.medicineTypeName': { $eq: 'Surgical' }}]
            }
        }else if(medicineData.name == 'Ethical branded' || medicineData.name == 'Others'){
            matchMediType = { $or:[{'Inventory.medicineTypeName': { $eq: 'Ethical branded' }},
            {'Inventory.medicineTypeName': { $eq: 'Others' }}]
            }
        }else{
            matchMediType = { 'Inventory.medicineTypeName': { $eq: medicineData.name } }
        }
    }else{
        coolChainData = await mediType.findOne({ name: 'Cool chain' }).then((result) => result);
        ethicalData = await mediType.findOne({ name: 'Ethical branded' }).then((result) => result);
        genericData = await mediType.findOne({ name: 'Generic' }).then((result) => result);
    }
    let query1 = []
    try {
        query1 = [{
            $match: {
                user_id: userData._id
            }
        },
        { $unwind: "$cart_details" },
        {
            $lookup: {
                localField: 'cart_details.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'Inventory'
            }
        },
        { $unwind: "$Inventory" }]

        if (req.body.seller != '' && req.body.seller != undefined) {
            query1.push({
                $match: {
                    'Inventory.user_id': ObjectId(req.body.seller)
                }
            })
        }
        if (req.body.medicineId != '' && req.body.medicineId != undefined) {
            query1.push({
                $match: matchMediType
            })
        }

        query1.push({
            $lookup: {
                localField: 'Inventory.discount_id',
                foreignField: '_id',
                from: 'discounts',
                as: 'Discount'
            }
        },
            {
                $unwind: {
                    path: '$Discount',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'Inventory.user_id',
                    foreignField: '_id',
                    from: 'users',
                    as: 'Seller'
                }
            },
            {
                $unwind: {
                    path: '$Seller',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'Inventory.product_id',
                    foreignField: '_id',
                    from: 'products',
                    as: 'Inventory.product_id'
                }
            },
            { $unwind: "$Inventory.product_id" },
            {
                $lookup: {
                    localField: 'Inventory.product_id.GST',
                    foreignField: '_id',
                    from: 'gsts',
                    as: 'Inventory.product_id.gst'
                }
            },
            { $unwind: "$Inventory.product_id.gst" },
            {
                $lookup: {
                    localField: 'Inventory.product_id.company_id',
                    foreignField: '_id',
                    from: 'companies',
                    as: 'Inventory.product_id.company_id'
                }
            },
            { $unwind: "$Inventory.product_id.company_id" },
            {
                $lookup: {
                    localField: 'Discount.discount_on_product.inventory_id',
                    foreignField: '_id',
                    from: 'inventories',
                    as: 'Discount.discount_on_product.inventory_id'
                }
            },
            {
                $lookup: {
                    localField: 'Discount.discount_on_product.inventory_id.product_id',
                    foreignField: '_id',
                    from: 'products',
                    as: 'DiscountProduct'
                }
            });
        cartModel.aggregate(query1).exec((error, cartData) => {



            // cb(error,cartData);
            let orderData = [];
            let sellerData = []
            let prodData = []
            async.eachOfSeries(cartData, (eachOrder, key1, calllback) => {
                let index = sellerData.findIndex((val) => (val.Id).toString() == (eachOrder.Seller._id).toString())
                let product_cost;
                let coolChain_cost = 0;
                let ethical_cost = 0;
                let generic_cost = 0;
                let quantity = eachOrder.cart_details.quantity;
                let prodPayType = eachOrder.Inventory.product_id && eachOrder.Inventory.product_id.isPrepaid ?
                eachOrder.Inventory.product_id.isPrepaid : false;

                if (eachOrder.Discount && eachOrder.Discount.type == 'Same' || eachOrder.Discount.type == 'SameAndDiscount') {
                    let temp = quantity / eachOrder.Discount.discount_on_product.purchase;
                    temp = temp * eachOrder.Discount.discount_on_product.bonus
                    product_cost = ((Number(quantity) + temp) * eachOrder.Inventory.ePTR);
                } else {
                    product_cost = (Number(quantity) * eachOrder.Inventory.ePTR);
                }
                if(eachOrder.Inventory.medicineTypeName === 'Generic' ||
                    eachOrder.Inventory.medicineTypeName === 'OTC' ||
                    eachOrder.Inventory.medicineTypeName === 'Surgical') {
                        generic_cost =  product_cost
                }else if(eachOrder.Inventory.medicineTypeName === 'Cool chain'){
                        coolChain_cost =  product_cost
                }else if(eachOrder.Inventory.medicineTypeName === 'Ethical branded'  ||
                    eachOrder.Inventory.medicineTypeName === 'Others'){
                        ethical_cost =  product_cost
                }
                if (index > -1) {
                    sellerData[index].total = sellerData[index].total + product_cost;
                    sellerData[index].coolChain = sellerData[index].coolChain + coolChain_cost;
                    sellerData[index].ethical = sellerData[index].ethical + ethical_cost;
                    sellerData[index].generic = sellerData[index].generic + generic_cost;
                } else {
                    sellerData.push({ Id: eachOrder.Seller._id, total: product_cost, company_name: eachOrder.Seller.company_name, coolChain: coolChain_cost, generic: generic_cost, ethical: ethical_cost })
                }
                if (prodPayType && req.body.type === 'COD') {
                        prodData.push({ Id: eachOrder.Seller._id, isPrepaid: true, prodName: eachOrder.Inventory.product_id.name, company_name: eachOrder.Seller.company_name, inventory_id: eachOrder.Inventory._id })
                }
                if (eachOrder.Seller.user_status != 'active') {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: 'You cannot checkout as the seller is not available currently',
                        showButton: true
                    });
                } else if (eachOrder.Seller.isVerfied == false) {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: 'You cannot checkout as the seller is not verified currently',
                        showButton: true
                    });

                }else if (eachOrder.Inventory.isDeleted === true) {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: 'You cannot checkout as the seller has deleted this product',
                        showButton: true
                    });

                }else if (eachOrder.Inventory.prepaidInven === true && req.body.type == 'COD') {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: `COD is disabled for ${eachOrder.Inventory.product_id.name} of ${eachOrder.Seller.company_name}.`,
                        showButton: true
                    });

                } else if (eachOrder.Inventory.isHidden === true) {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: 'You cannot checkout as the product is hidden by the seller',
                        showButton: true
                    });
                } else if (new Date(eachOrder.Inventory.expiry_date) < new Date()) {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: 'Product is expired.',
                        showButton: true
                    });
                } else if (eachOrder.Inventory.quantity < 1 || eachOrder.Inventory.quantity < eachOrder.Inventory.min_order_quantity) {
                    orderData.push({
                        type: 'remove',
                        productId: eachOrder.Inventory.product_id._id,
                        qty: eachOrder.cart_details.quantity,
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        msg: 'Product is out of stock.',
                        showButton: true
                    });
                }
                else if (eachOrder.cart_details.quantity > eachOrder.Inventory.max_order_quantity) {
                    orderData.push({
                        type: 'update',
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        quantity: eachOrder.Inventory.max_order_quantity,
                        msg: 'Required quantity is more than Product maximum quantity. Maximum quantity is ' + eachOrder.Inventory.max_order_quantity
                    });
                } else if (eachOrder.cart_details.quantity > eachOrder.Inventory.quantity) {
                    orderData.push({
                        type: 'update',
                        inventory_id: eachOrder.Inventory._id,
                        productName: eachOrder.Inventory.product_id.name,
                        quantity: eachOrder.Inventory.quantity,
                        msg: 'Required quantity is more than Product inventory stock. Available quantity is ' + eachOrder.Inventory.quantity
                    });
                } else if (eachOrder.Discount.type != 'Discount' && eachOrder.Discount.discount_on_product && eachOrder.Discount.discount_on_product.purchase > 0) {
                    let remainder = eachOrder.cart_details.quantity % eachOrder.Discount.discount_on_product.purchase;
                    if (remainder > 0) {
                        orderData.push({
                            type: 'remove',
                            productId: eachOrder.Inventory.product_id._id,
                            qty: eachOrder.cart_details.quantity,
                            inventory_id: eachOrder.Inventory._id,
                            productName: eachOrder.Inventory.product_id.name,
                            msg: `Please add in multiple of ${eachOrder.Discount.discount_on_product.purchase}`,
                            quantity: eachOrder.cart_details.quantity,
                            showButton: true
                        });
                    }
                }
                calllback();
            }, (error) => {

                if (error) {
                    return res.status(200).json({
                        title: 'Something went wrong, Please try again later.',
                        error: true
                    });
                }
                if ((userData.cod === false || userData.codService === false || globalCod.value == '0') && req.body.type == 'COD') {
                    orderData.push({
                        productName: 'Only prepaid available.',
                        type: 'do nothing',
                        msg: `COD is not available currently kindly place prepaid order.`
                    });
                }
                sellerData.map((value) => {
                    if(value.coolChain > 0 && req.body.type === 'COD'){
                        orderData.push({
                            productName: 'Cool chain',
                            type: 'do nothing',
                            msg: `Cool chain products can only be ordered by Online Payment. ( ${value.company_name} )`
                        });
                    }
                    if (req.body.medicineId) {
                        if (medicineData.name == 'Generic' && medicineData.minOrder > value.generic) {
                            orderData.push({
                                productName: 'Generic',
                                type: 'do nothing',
                                msg: `Minimum order amount for Generic medicine type is ₹${(medicineData.minOrder).toFixed(2)}. Add ₹${Number(medicineData.minOrder - value.generic).toFixed(2)} more for ${value.company_name}.`
                            });
                        } else if (medicineData.name == 'Ethical branded' && medicineData.minOrder > value.ethical) {
                            orderData.push({
                                productName: 'Ethical branded',
                                type: 'do nothing',
                                msg: `Minimum order amount for Ethical branded medicine type is ₹${(medicineData.minOrder).toFixed(2)}. Add ₹${Number(medicineData.minOrder - value.ethical).toFixed(2)} more for ${value.company_name}.`
                            });
                        } else if (medicineData.name == 'Cool chain' && medicineData.minOrder > value.coolChain) {
                            orderData.push({
                                productName: 'Cool chain ',
                                type: 'do nothing',
                                msg: `Minimum order amount for Cool chain medicine type is ₹${(medicineData.minOrder).toFixed(2)}. Add ₹${Number(medicineData.minOrder - value.coolChain).toFixed(2)} more for ${value.company_name}.`
                            });
                        }
                    }else{
                        if (value.generic > 0 && genericData.minOrder > value.generic) {
                            orderData.push({
                                productName: 'Generic',
                                type: 'do nothing',
                                msg: `Minimum order amount for Generic medicine type is ₹${(genericData.minOrder).toFixed(2)}. Add ₹${Number(genericData.minOrder - value.generic).toFixed(2)} more for ${value.company_name}.`
                            });
                        }
                        if (value.ethical > 0 && ethicalData.minOrder > value.ethical) {
                            orderData.push({
                                productName: 'Ethical branded',
                                type: 'do nothing',
                                msg: `Minimum order amount for Ethical branded medicine type is ₹${(ethicalData.minOrder).toFixed(2)}. Add ₹${Number(ethicalData.minOrder - value.ethical).toFixed(2)} more for ${value.company_name}.`
                            });
                        }
                        if (value.coolChain > 0 && coolChainData.minOrder > value.coolChain) {
                            orderData.push({
                                productName: 'Cool chain ',
                                type: 'do nothing',
                                msg: `Minimum order amount for Cool chain medicine type is ₹${(coolChainData.minOrder).toFixed(2)}. Add ₹${Number(coolChainData.minOrder - value.coolChain).toFixed(2)} more for ${value.company_name}.`
                            });
                        } 
                    }
                });
                sellerData.map((seller) => {
                    if (seller.total > 2000) {
                        prodData.map((value) => {
                            if (req.body.seller && ((req.body.seller).toString() == (value.Id).toString())) {
                                if (value.isPrepaid) {
                                    orderData.push({
                                        type: 'remove',
                                        inventory_id: value.inventory_id,
                                        productName: value.prodName,
                                        msg: `COD is disabled for ${value.prodName} of ${value.company_name}.`
                                    });
                                }
                            } else {
                                if (value.isPrepaid && seller.total > 2000 && ((seller.Id).toString() == (value.Id).toString())) {
                                    orderData.push({
                                        type: 'remove',
                                        inventory_id: value.inventory_id,
                                        productName: value.prodName,
                                        msg: `COD is disabled for ${value.prodName} of ${value.company_name}.`
                                    });
                                }
                            }
                        })
                    }
                })
                // sellerData.map((value) => {
                //     if (req.body.seller && ((req.body.seller).toString() == (value.Id).toString())) {
                //         if (value.total < 2000) {
                //             orderData.push({
                //                 productName: 'Insufficient billing',
                //                 type: 'do nothing',
                //                 msg: `Minimum order amount is ₹2000. Add ₹${Number(2000 - value.total).toFixed(2)} more for ${value.company_name}.`
                //             });
                //         }
                //     } else if ((req.body.seller == '' || req.body.seller == undefined) && value.total < 2000) {
                //         orderData.push({
                //             productName: 'Insufficient billing',
                //             type: 'do nothing',
                //             msg: `Minimum order amount is ₹2000. Add ₹${Number(2000 - value.total).toFixed(2)} more for ${value.company_name}.`
                //         });
                //     }
                // })
                cb(error, orderData);
            })
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }



}

/*
# parameters: token,
# purpose: Create order
*/
const getOrderStatus = (data) => {
    switch (data) {
        case "Generic":
        case "OTC":
        case "Surgical":
            return 'Generic'
        case "Others":
        case "Ethical branded":   
            return 'Ethical branded'
        case 'Cool chain':
            return 'Cool chain'
        default:
        return data
    }
}

const createOrder = async (req, res) => {
    let userData = await user.findById({ _id: req.user._id }).then((result) => result);
    let userD = req.user;
    let medicineData;
    let matchMediType;
    try {
        confirm(req, async function (error, data) {
            if (data.length > 0) {
                return res.status(200).json({
                    title: 'Error in order',
                    data: data,
                    error: true,
                });
            } else {
                if (req.body.medicineId) {
                    medicineData = await mediType.findOne({ _id: ObjectId(req.body.medicineId) }).then((result) => result);
                    if (medicineData.name == 'Generic' || medicineData.name == 'OTC' || medicineData.name == 'Surgical') {
                        matchMediType = {
                            $or: [{ 'Inventory.medicineTypeName': { $eq: 'Generic' } },
                            { 'Inventory.medicineTypeName': { $eq: 'OTC' } },
                            { 'Inventory.medicineTypeName': { $eq: 'Surgical' } }]
                        }
                    } else if (medicineData.name == 'Ethical branded' || medicineData.name == 'Others'){
                        matchMediType = { $or:[{'Inventory.medicineTypeName': { $eq: 'Ethical branded' }},
                        {'Inventory.medicineTypeName': { $eq: 'Others' }}]
                        }
                    } else {
                        matchMediType = { 'Inventory.medicineTypeName': { $eq: medicineData.name } }
                    }
                }
                let userAgent = {};
                userAgent['headers'] = req.headers;
                userAgent['_peername'] = req.socket._peername
                let query = [{
                    $match: {
                        user_id: userData._id
                    }
                },
                { $unwind: "$cart_details" },
                {
                    $lookup: {
                        localField: 'cart_details.inventory_id',
                        foreignField: '_id',
                        from: 'inventories',
                        as: 'Inventory'
                    }
                },
                { $unwind: "$Inventory" },
                {
                    $lookup: {
                        localField: 'Inventory.discount_id',
                        foreignField: '_id',
                        from: 'discounts',
                        as: 'Discount'
                    }
                },
                {
                    $unwind: {
                        path: '$Discount',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $lookup: {
                        localField: 'Inventory.product_id',
                        foreignField: '_id',
                        from: 'products',
                        as: 'Inventory.product_id'
                    }
                },
                { $unwind: "$Inventory.product_id" },
                {
                    $lookup: {
                        localField: 'Inventory.product_id.GST',
                        foreignField: '_id',
                        from: 'gsts',
                        as: 'Inventory.product_id.gst'
                    }
                },
                { $unwind: "$Inventory.product_id.gst" },
                {
                    $lookup: {
                        localField: 'Inventory.product_id.company_id',
                        foreignField: '_id',
                        from: 'companies',
                        as: 'Inventory.product_id.company_id'
                    }
                },
                { $unwind: "$Inventory.product_id.company_id" },
                {
                    $lookup: {
                        localField: 'Discount.discount_on_product.inventory_id',
                        foreignField: '_id',
                        from: 'inventories',
                        as: 'Discount.discount_on_product.inventory_id'
                    }
                },
                {
                    $lookup: {
                        localField: 'Discount.discount_on_product.inventory_id.product_id',
                        foreignField: '_id',
                        from: 'products',
                        as: 'DiscountProduct'
                    }
                }]
                if (req.body.seller != '' && req.body.seller != undefined) {
                    query.push({
                        $match: {
                            'Inventory.user_id': ObjectId(req.body.seller)
                        }
                    })
                }
                if (req.body.medicineId != '' && req.body.medicineId != undefined) {
                    query.push({
                        $match: matchMediType
                    })
                }
                query.push({
                    $group: {
                        _id: "$Inventory.user_id",
                        data: { $addToSet: '$$ROOT' }
                    }
                })
                let cartDetails = cartModel.aggregate(query).exec((error, cartData) => {
                    if (error) {
                        return res.status(200).json({
                            title: 'Something went wrong, Please try again..',
                            msg: error,
                            error: true,
                        });
                    }
                    let isBuyerVerified = userData.user_status === 'active' ? true : false;
                    let unique_invoice = randomstring.generate({
                        length: 12,
                        charset: 'alphanumeric'
                    });
                    let transactionCost = 0;
                    let orderResponseData = [];
                    let cOrderData = {};
                    let buyerData;
                    async.eachOfSeries(cartData, (eachOrder, key1, mainCallBack) => {
                        let overAllData = []
                        let generic = eachOrder.data.filter(e =>
                        (e.Inventory.medicineTypeName === 'Generic' ||
                            e.Inventory.medicineTypeName === 'OTC' ||
                            e.Inventory.medicineTypeName === 'Surgical')
                        );
                        let ethical = eachOrder.data.filter(e => (e.Inventory.medicineTypeName === 'Ethical branded' || e.Inventory.medicineTypeName === 'Others'));
                        let coolChain = eachOrder.data.filter(e => e.Inventory.medicineTypeName === 'Cool chain');
                        if (generic.length > 0) {
                            overAllData.push(generic);
                        }
                        if (ethical.length > 0) {
                            overAllData.push(ethical);
                        }
                        if (coolChain.length > 0) {
                            overAllData.push(coolChain);
                        }

                        let orderValWithoutGst = 0;
                        async.eachOfSeries(overAllData, (cart_detail1, key2, secondCb) => {
                            let order_Data = {};
                            order_Data.total_amount = 0;
                            order_Data.products = [];
                            order_Data.orderType = '';
                            async.eachOfSeries(cart_detail1, (cart_detail, key3, cb) => {
                                let product_cost;
                                let quantity = cart_detail.cart_details.quantity;
                                if (cart_detail.Discount && (cart_detail.Discount.name == 'Same' || cart_detail.Discount.name == 'SameAndDiscount')) {
                                    let temp = quantity / cart_detail.Discount.discount_on_product.purchase;
                                    temp = temp * cart_detail.Discount.discount_on_product.bonus
                                    product_cost = ((cart_detail.cart_details.quantity + temp) * cart_detail.Inventory.ePTR);
                                    orderValWithoutGst += ((cart_detail.cart_details.quantity + temp) * cart_detail.Inventory.ePTR);
                                } else {
                                    product_cost = (cart_detail.cart_details.quantity * cart_detail.Inventory.ePTR);
                                    orderValWithoutGst += (cart_detail.cart_details.quantity * cart_detail.Inventory.ePTR);
                                }
                                let temparr = {}
                                temparr.images = [];
                                order_Data.orderType = getOrderStatus(cart_detail.Inventory.medicineTypeName);
                                order_Data.seller_id = cart_detail.Inventory.user_id;
                                temparr.inventory_id = cart_detail.cart_details.inventory_id;
                                temparr.productName = cart_detail.Inventory.product_id.name;
                                temparr.name = cart_detail.Inventory.product_id.name;
                                temparr.images = cart_detail.Inventory.product_id.images;
                                temparr.PTR = cart_detail.Inventory.PTR;
                                temparr.ePTR = cart_detail.Inventory.ePTR;
                                temparr.expiry_date = cart_detail.Inventory.expiry_date;
                                temparr.paid = cart_detail.Inventory.PTR;
                                temparr.MRP = cart_detail.Inventory.MRP;
                                temparr.GST = cart_detail.Inventory.product_id.gst.value;
                                temparr.quantity = cart_detail.cart_details.quantity;
                                temparr.discount_per = 0;
                                temparr.surCharge = cart_detail.Inventory.product_id.surcharge ? cart_detail.Inventory.product_id.surcharge : 0;
                                temparr.discount_name = cart_detail.Discount.type;
                                if (cart_detail.Discount && cart_detail.Discount.name !== undefined) {
                                    if (cart_detail.Discount.type === 'Discount' && cart_detail.Discount.discount_per) {
                                        temparr.discount_per = cart_detail.Discount.discount_per;
                                        // let discount_amount = (cart_detail.Discount.discount_per / 100) * product_cost;
                                        // product_cost = product_cost - discount_amount;
                                        if (cart_detail.Inventory.product_id.gst.value > 0) {
                                            let gst_amount = (cart_detail.Inventory.product_id.gst.value / 100) * product_cost;
                                            product_cost = product_cost + gst_amount;
                                        }
                                        order_Data.total_amount = Math.round((order_Data.total_amount + product_cost));
                                        order_Data.products.push(temparr);
                                        cb()
                                    } else if (cart_detail.Discount.type !== 'Discount' && cart_detail.Discount.discount_on_product && cart_detail.Discount.discount_on_product != undefined && cart_detail.Discount.discount_on_product.inventory_id.length > 0) {
                                        temparr.discount_on_product = {}
                                        inventory.findOne({ _id: cart_detail.Discount.discount_on_product.inventory_id[0]._id }).populate('product_id').populate('discount_id').then((inventoryData) => {
                                            // let bonusProduct = cart_detail.cart_details.quantity / cart_detail.Discount.discount_on_product.purchase
                                            let discount_on_product = {
                                                inventory_id: inventoryData._id,
                                                name: inventoryData.product_id.name,
                                                purchase: cart_detail.Discount.discount_on_product.purchase,
                                                bonus: cart_detail.Discount.discount_on_product.bonus
                                            }
                                            if (cart_detail.Discount.type == 'SameAndDiscount' || cart_detail.Discount.type == 'DifferentAndDiscount') {
                                                temparr.discount_per = Number(cart_detail.Discount.discount_per);
                                            } 
                                            temparr.discount_on_product = discount_on_product;
                                            if (cart_detail.Inventory.product_id.gst.value > 0) {
                                                let gst_amount = (cart_detail.Inventory.product_id.gst.value / 100) * product_cost;
                                                product_cost = product_cost + gst_amount;
                                            }
                                            order_Data.total_amount = Math.round((order_Data.total_amount + product_cost));
                                            order_Data.products.push(temparr);
                                            cb()
                                        })
                                    }
                                } else {
                                    if (cart_detail.Inventory.product_id.gst.value > 0) {
                                        let gst_amount = (cart_detail.Inventory.product_id.gst.value / 100) * product_cost;
                                        product_cost = product_cost + gst_amount;
                                    }
                                    order_Data.total_amount = Math.round((order_Data.total_amount + product_cost));
                                    order_Data.products.push(temparr);
                                    cb()
                                }


                                // let temparr = [];


                            }, function (err) {
                                user.findById(order_Data.seller_id).then((sellerInfo) => {
                                    if (!sellerInfo || sellerInfo.user_status !== 'active') {
                                        mainCallBack()
                                    } else {
                                        let invoiceId = randomstring.generate({
                                            length: 12,
                                            charset: 'alphanumeric'
                                        });
                                        helper.getOrderId((error, orderId) => {
                                            let newOrder = new order({
                                                order_id: orderId,
                                                user_id: userData._id,
                                                invoice_number: invoiceId,
                                                unique_invoice: unique_invoice,
                                                requested: 'New',
                                                order_status: {
                                                    status: "New",
                                                    description: "Order has been placed.",
                                                    date: new Date()
                                                },
                                                length: 20,
                                                breadth: 12,
                                                height: 10,
                                                paymentType: req.body.type,
                                                seller_id: order_Data.seller_id,
                                                products: order_Data.products,
                                                total_amount: Number(order_Data.total_amount).toFixed(2),
                                                delivery_charges: userData.isDeliveryCharge === true ? 50 : 0,
                                                paymentStatus: req.body.type === 'COD' ? 'success' : 'pending',
                                                userRequest: userAgent,
                                                buyerCompName: userData.company_name,
                                                sellerCompName: sellerInfo.company_name,
                                                orderType: order_Data.orderType
                                            });
                                            transactionCost = Math.round(Number(transactionCost) + Number(newOrder.total_amount) + Number(newOrder.delivery_charges)).toFixed(2);
                                            newOrder.save((error, createdOrder) => {
                                                if (userD && userD.asAdmin == '1') {
                                                    let data = {
                                                        type: 'Order',
                                                        action: 'Order placed by Admin',
                                                        user_id: userData._id,
                                                        type_id: createdOrder._id
                                                    }
                                                    let newActivity = new systemActivity(data);
                                                    newActivity.save().then();
                                                }
                                                cOrderData = createdOrder;
                                                inventory.updateInventoryQuantity(createdOrder.products, 'create', (response) => {
                                                });
                                                orderResponseData.push(createdOrder);
                                                // user.findOne({ user_type: 'admin' }).then((admin) => {
                                                if (createdOrder.total_amount >= 50000) {
                                                    // fetch admin email for sending E-way Bill email to admin 
                                                    orderResponseData.map((data) => {
                                                        var mailData = {
                                                            // email: admin.email,
                                                            email: 'helpdesk@medimny.com',
                                                            subject: 'E-way Bill',
                                                            body:
                                                                '<p>' +
                                                                `Please check ${data.order_id} Order exceed threshold. Generate E-WAY bill in conslutation with Logistic & Seller.`
                                                        };
                                                        helper.sendEmail(mailData);
                                                        var mailData2 = {
                                                            email: 'support@medimny.com',
                                                            subject: 'E-way Bill',
                                                            body:
                                                                '<p>' +
                                                                `Please check ${data.order_id} Order exceed threshold. Generate E-WAY bill in conslutation with Logistic & Seller.`
                                                        };
                                                        helper.sendEmail(mailData2);
                                                    })
                                                }
                                                secondCb()
                                                //send mail to seller and also to buyer

                                            })
                                        });
                                    }
                                })

                            })
                        }, function (err) {
                            mainCallBack()
                        })
                    }, async function (err2) {
                        orderResponseData.transactionCost = transactionCost;

                        let transaction = [];
                        transaction.user_id = userData._id;
                        transaction.settle_amount = transactionCost;
                        transaction.paid_amount = transactionCost;
                        transaction.unique_invoice = unique_invoice;
                        let orderids = orderResponseData.map(order => {
                            return order.order_id;
                        }).join(',');
                        transaction.narration = `Payment for Order: ${orderids}, transactionID: ` + unique_invoice;
                        let paytmPayAmount = 0;
                        transaction.status = 'Success';
                        transaction.type = 'COD'
                        let balance = transactionCost;
                        transaction.closing_bal = userData.wallet_balance;
                        if (req.body.type !== 'COD') {
                            transaction.status = 'Success';
                            transaction.type = 'Online';
                            if (userData.wallet_balance > 0) {
                                transaction.type = 'Credit Used';
                                transaction.status = 'Success';
                                if (userData.wallet_balance < Number(transactionCost)) {
                                    paytmPayAmount = Number(transactionCost - userData.wallet_balance).toFixed(2);
                                    transaction.settle_amount = userData.wallet_balance;
                                    transaction.paid_amount = userData.wallet_balance;
                                    balance = Number(Number(userData.wallet_balance).toFixed(2));
                                } else {
                                    paytmPayAmount = 0
                                    transaction.settle_amount = Number(transactionCost).toFixed(2);
                                    transaction.paid_amount = Number(transactionCost).toFixed(2);
                                    balance = Number(Number(transactionCost).toFixed(2));
                                    order.updateMany({ unique_invoice }, { $set: { paymentStatus: 'success' } }).then(result => result)
                                }
                                console.log('balance=======***************************************************************', -balance, balance)
                                buyerData = await user.findOneAndUpdate({ _id: userData._id }, { $inc: { wallet_balance: -balance } }, { new: true }).then(result => result);
                                transaction.closing_bal = buyerData.wallet_balance;
                            } else {
                                if (userData.wallet_balance < 0) {
                                    transaction.settle_amount = userData.wallet_balance;
                                    paytmPayAmount = transactionCost - userData.wallet_balance;
                                    transaction.type = 'Credit Used';
                                    buyerData = user.findOneAndUpdate({ _id: userData._id }, { $inc: { wallet_balance: Math.abs(userData.wallet_balance) } }, { new: true }).then(result => result);
                                    transaction.closing_bal = buyerData.wallet_balance;
                                } else {
                                    paytmPayAmount = transactionCost;
                                    transaction.type = 'Online';
                                    transaction.closing_bal = userData.wallet_balance;
                                }

                            }
                        }
                        if (req.body.type === 'COD' || paytmPayAmount == 0) {
                            let removeProd = [];
                            orderResponseData.map(data => {
                                if (data) {
                                    orderNotification(data, userData, data.order_id);
                                    data.products.map(prod => removeProd.push(prod))
                                }
                            })
                            cartModel.updateCart(userData._id, removeProd, 'remove')
                            transactions.addTransaction(transaction, (error, transactionData) => {
                                checkInventory(unique_invoice);
                                return res.status(200).json({
                                    title: 'Order created successfully.',
                                    error: false,
                                    detail: orderResponseData
                                });

                            })
                        } else {
                            transaction.paid_amount = Number(Number(paytmPayAmount).toFixed(2));
                            let orderid = orderResponseData.map(order => {
                                return order.order_id;
                            }).join(',');
                            transaction.status = 'Success';
                            transaction.narration = `Payment for Order: ${orderid}, transactionID: ` + unique_invoice;
                            if (transaction.type === 'Credit Used') {
                                transactions.addTransaction(transaction, (error, transactionData) => {

                                })
                            }

                            var token = jwt.sign({
                                balance: Number(paytmPayAmount),
                                unique_invoice,
                                userId: req.user._id,
                                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60 * 1000),

                            }, "medideal$!@#$(*&^");
                            res.status(200).json({
                                title: 'Order created successfully.',
                                error: false,
                                token,
                                unique_invoice,
                                amount: Number(paytmPayAmount),
                                detail: orderResponseData
                            });
                            // setTimeout(() => timeOutFunction(unique_invoice), 600000)
                        }
                    })

                })
            }
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }

}
const timeOutFunction = (unique_invoice) => {

    transactions.findOne({ unique_invoice: unique_invoice, type: 'Online' }).then((result) => {
        if (!result) {
            helper.rollBackOrderByUniqueInvoice(unique_invoice)
        }
    })

}
/*
# parameters: token, data to update,647801
# purpose: update order
*/
const updateOrder = (req, res) => {
    let userData = req.user;
    let products = req.body.products ? [...req.body.products] : [];
    let notificationTitle = '';
    let notificationMsg = '';
    let errors = []
    if(products.length > 0){
        products.map(data => {
            let disc = data && data.discount_on_product && data.discount_on_product.inventory_id
            if(!data.inventory_id){
                errors.push({err: `Inventory Id can't be empty`});
            }
            if(!data.quantity  && data.quantity !=0){
                errors.push({err: `Quantity can't be empty`});
            }
            if((disc || data.discount_per > 0) && !data.discount_name){
                errors.push({err: `Discount name can't be empty`});
            }
            if(!data.ePTR || !data.PTR){
                errors.push({err: `PTR can't be empty`});
            }
        })
    }
    if(!req.body.orderId){
        errors.push({err: `Order ID is required`});
    }
    if(errors.length > 0){
        return res.status(200).json({
            title: errors[0].err,
            error: true
        });
    }
    order.findOne({ order_id: req.body.orderId }).populate('seller_id').exec(async (error, orderDetails) => {
        if(orderDetails.requested == 'Cancelled'){
            return res.status(200).json({
                title: 'Order has been cancelled.', 
                error: true
            });
        }
        let query = {};
        let transaction = {};
        transaction.user_id = orderDetails.user_id;
        if (req.body.status.toLowerCase() === 'accept') {
            let statusData = {
                status: 'New',
                date: new Date(),
                description: 'Change order request has been Accepted.'
            }
            notificationTitle = 'Order Accepted';
            notificationMsg = 'Change order request has been Accepted.';
            query = { $set: { requested: 'Accepted' }, $push: { order_status: statusData } }
            if(userData.user_type == 'admin'){
                let data1 = {
                    type: 'Order',
                    action: 'Order accepted by Admin',
                    user_id: userData._id,
                    type_id: orderDetails._id
                }
                let newActivity = new systemActivity(data1);
                newActivity.save().then();
            }
            
        } else if (req.body.status === 'Cancelled') {
            let alreadyCancelled = await cancelledOrder(req.body.orderId);
            if (alreadyCancelled) {
                return res.status(200).json({
                    title: 'Order already has been cancelled',
                    error: true
                });
            }
            let orderData = await checkCancelledOrder(req.body.orderId)
            if (orderData) {
                return res.status(200).json({
                    title: 'You cannot cancel this order as this has been processed',
                    error: true
                });
            }
            let statusData;
            if(userData.user_type == 'admin'){
                statusData = {
                    status: 'Cancelled',
                    date: new Date(),
                    description: `Order has been Cancelled by ${userData.first_name}(Admin)`
                }
            }else{
                statusData = {
                    status: 'Cancelled',
                    date: new Date(),
                    description: `Order has been Cancelled by ${userData.company_name}`
                }
            }
            rollBackSingleOrder(orderDetails)
            updateInventoryInOrder(orderDetails.products, 'cancel');
            notificationTitle = 'Order Cancelled';
            notificationMsg = 'Order has been Cancelled.';
            if(orderDetails.requested == 'Requested' && userData.user_type =='buyer'){
                //put requested products in removedProd if requested orde if cancelled buy buyer
                statusData.description = `Order has been Cancelled by ${orderDetails.seller_id.company_name}`
                query = { $set: { paymentStatus: 'failed', requested: req.body.status, order_cancel_reason: req.body.reason, cancelledBy: orderDetails.seller_id._id }, $push: { order_status: statusData, removedProd: orderDetails.requestedProd } }
            }else{
                query = { $set: { paymentStatus: 'failed', requested: req.body.status, order_cancel_reason: req.body.reason, cancelledBy: req.user._id }, $push: { order_status: statusData } }
            }
        } else if (req.body.status == 'Requested') {
            let total_amount;
            if( products[0].ePTR > 0 ){
                total_amount = products.reduce(function (accumulator, product) {
                let temp = product.discount_on_product && product.quantity / product.discount_on_product.purchase;
                temp = product.discount_on_product && temp * product.discount_on_product.bonus
                let value = product.discount_on_product && ((Number(product.quantity) + Number(temp)) * product.ePTR);
                if (product.discount_on_product && product.inventory_id._id == product.discount_on_product.inventory_id) {
                    return accumulator + (value) + ((value) / 100 * Number(product.GST));
                } else if (product.discount_per > 0) {
                    return accumulator + Number(Number(product.ePTR) * product.quantity) + Number(Number(Number(product.ePTR ) * product.quantity) / 100 * Number(product.GST))
                } else {
                    return accumulator + (product.ePTR * product.quantity) + ((product.ePTR * product.quantity) / 100 * Number(product.GST));

                }}, 0);
            }else{
                total_amount = req.body.products.reduce(function (accumulator, product) {
                    if (product.discount_per > 0) {
                        return accumulator + Number(Number(product.PTR - (product.PTR / 100) * Number(product.discount_per)) * product.quantity) + Number(Number(Number(product.PTR - (product.PTR / 100) * Number(product.discount_per)) * product.quantity) / 100 * Number(product.GST))
                    } else {
                        return accumulator + (product.PTR * product.quantity) + ((product.PTR * product.quantity) / 100 * Number(product.GST));
    
                    }}, 0);
            }
            
            console.log('=-0=-0=-0prodR', total_amount, orderDetails.total_amount)
            let newArry = await products.filter(prod => Number(prod.quantity) > 0);
            let negativeQuan = await products.filter(prod => Number(prod.quantity) < 0);
            
            if (total_amount > orderDetails.total_amount) {
                return res.status(200).json({
                    error: true,
                    title: 'Total order amount should NOT be greater than current total amount.'
                })
            } else if (total_amount <= 0) {
                return res.status(200).json({
                    error: true,
                    title: 'Total order amount should be greater than zero'
                })
            } else if (negativeQuan.length > 0) {
                return res.status(200).json({
                    error: true,
                    title: `Quantity can't be negative.`
                })
            } else if (orderDetails.paymentType != 'COD') {
                let balance = orderDetails.total_amount - total_amount;
                query = { $set: { requested: req.body.status, products: newArry, total_amount: Math.round(total_amount), balance: Math.round(balance) } }
            } else {
                query = { $set: { requested: req.body.status, products: newArry, total_amount: Math.round(total_amount) } }
            }


            notificationTitle = 'Order Changed';
            notificationMsg = 'Seller has changed the order. Please check the order details.';
            // helper.updateInventoryOnRequest(req.body.orderId, req.body.products);

        }
        order.findOneAndUpdate({ order_id: req.body.orderId }, query, { new: true })
            .populate({
                select: 'MRP PTR quantity min_order_quantity max_order_quantity',
                path: 'products.inventory_id',
                populate: {
                    path: 'product_id',
                    model: 'product'
                }
            })
            .populate({
                select: 'first_name email last_name user_address user_pincode drugLic20B drugLic21B fassaiLic phone device_token',
                path: 'user_id',
                populate: {
                    path: 'user_state',
                    model: 'country'
                }
            })
            .exec(async (error, updatedOrder) => {

                if (error) {
                    return res.status(200).json({
                        title: 'Something went wrong Please wait.',
                        data: error,
                        error: true
                    });
                }
                if (!updatedOrder) {
                    return res.status(200).json({
                        title: 'Unable to find order.',
                        error: true
                    });
                } else {
                    if (req.body.status == 'Requested' && req.body.products) {
                        let reqArr = [];
                        req.body.products.map((prod) => {
                            let index = orderDetails.products.findIndex((val) => (val.inventory_id._id == prod.inventory_id._id))
                            if (index > -1 && Number(orderDetails.products[index].quantity) != Number(prod.quantity)) {
                                let quan = Number(orderDetails.products[index].quantity) - Number(prod.quantity);
                                reqArr.push({ name: prod.productName, avalQuantity: Number(prod.quantity), reqQuantity: orderDetails.products[index].quantity, prodId: prod.inventory_id.product_id._id, invenId: prod.inventory_id._id, need: quan })
                            }
                        })
                        let para = '';
                        let i = 1;
                        let j = 1;
                        let paraForOrder = '';
                        await reqArr.map(data => {
                            if (data) {
                                para += '<p>' + `${i++}` + '. ' + data.name + ', ' + 'required: ' + data.reqQuantity + ', ' + 'available: ' + data.avalQuantity + '.' + '</p>';
                                paraForOrder += ` ${j++}. ${data.name} ( required: ${data.reqQuantity}, available: ${data.avalQuantity} ).` ;
                            }
                        })
                        await user.findUserById(updatedOrder.user_id, async(error, userDetail) => {
                            console.log('found user', userDetail)
                        reqArr.map(async(data) => { 
                            if (data) {	
                                let foundInventory = await ActiveInventory.find({'Product._id':ObjectId(data.prodId)}).limit(3).then(result => result );
                                let ns = new shortBook({	
                                    product_name: data.name,	
                                    product_id: data.prodId,	
                                    user_id: updatedOrder.user_id,	
                                    quantity: data.need,
                                    inStock: foundInventory.length > 1 ? true : false	
                                })	
                                ns.save();
                                inventory.updateInventory(data.invenId, 'outOfStock', '');	

                                let flag = 'Product Added To ShortBook'
                                let msg = `Product ${data.name.toUpperCase()} is added to your shortbook successfully !` 
                                await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
                                if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                                    let devicetoken = userDetail.device_token;
                                    let title = 'Product Added To ShortBook'
                                    let payload = {};
                                    await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                                }
                            }
                        })
                        })
                        console.log('=-0=-0body', para, reqArr)
                        let statusData = {
                            status: 'Requested',
                            date: new Date(),
                            description: `Order has been Requested. Order Amount: ${Number(orderDetails.total_amount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, ${paraForOrder}`
                        }
                        await order.findOneAndUpdate({ order_id: req.body.orderId }, {$push: { order_status: statusData, requestedProd: reqArr }}, { new: true }).exec();
                        
                        var mailData1 = {
                            email: updatedOrder.user_id.email,
                            // email:'sameer.m@infiny.in',
                            subject: 'Order Modification',
                            body: '<p>' +
                                `Thank you for ordering on <a taget='_blank' href='https://www.medimny.co.in' >Medimny</a>, Seller has requested a change in Order. ${updatedOrder.order_id} as below :<br/>` +
                                `${para}`
                        };
                        helper.sendEmail(mailData1);
                    }
                    if (req.body.acceptFromBuyer == 'accept') {
                        // for seller
                        user.findOne({ _id: orderDetails.seller_id._id }).exec((error, userSellerData) => {
                            let msgBody = `Update - ${updatedOrder.order_id} modification accepted / Declined by the Buyer. Please process the order accordingly on MEDIMNY.`;
                            helper.sendSMS(userSellerData.phone, msgBody);

                            var mailData = {
                                email: userSellerData.email,
                                subject: 'Order Accepted / Declined',
                                body:
                                    '<p>' +
                                    ` Update - ${updatedOrder.order_id} modification accepted / Declined by the Buyer. Please process the order accordingly on MEDIMNY. `
                            };
                            helper.sendEmail(mailData);
                            var mailData2 = {
                                email: 'noreply@medimny.com',
                                subject: 'Order Accepted / Declined',
                                body:
                                    '<p>' +
                                    ` Update - ${updatedOrder.order_id} modification accepted / Declined by the Buyer. Please process the order accordingly on MEDIMNY. `
                            };
                            helper.sendEmail(mailData2);
                            if(userData.user_type == 'admin' && req.body.status.toLowerCase() === 'accept'){
                                let flag = 'Order Accepted / Declined';
                                let msg = `Update - ${updatedOrder.order_id} modification accepted / declined.`;
                                let sellerD = JSON.parse(JSON.stringify(userSellerData))
                                sellerD.seller_id = userSellerData._id
                                notification.addNotification(msg, '', sellerD, flag, (error, response) => {
                                })
                            }
                        })
                    }

                    return res.status(200).json({
                        title: 'Order details updated successfully.',
                        error: false,
                        detail: updatedOrder
                    });
                }
            })
    });
}
const shippingSerivce = (req, res) => {
    courierService.delhiveryService(req, res, function (data) {
        if (data.error == false) {
            let query = data.query
            updateOrderHelper(req, res, query)
        } else {
            courierService.shiprocket(req, res, (query) => {
                updateOrderHelper(req, res, query)
            })
        }
    })

}
const checkThreshold = async(req, cb) => {
    let query = {}
    let query2 = {}
    let startM = moment().subtract(1, 'months').date(1).format('YYYY-MM-DD');
    let endM = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
    let userId = req.user.mainUser ? req.user.mainUser : req.user._id
    let start = new Date(startM)
    let end = new Date(endM)
    let sevenDays = new Date(moment().subtract(7, 'days').format('YYYY-MM-DD'));
    query = { ...query,
        seller_id: ObjectId(userId),
        "order_status.status": { $eq: 'Processed' }
    }
    query = { ...query, $and: [
        { "createdAt": { $gte: start } },
        { "createdAt": { $lt: end } }
    ]}
    query2 = { ...query2,
        seller_id: ObjectId(userId),
        requested: { $eq: 'New' },
        'paymentStatus': { $ne: 'pending' },
        createdAt: { $lt: sevenDays }
    }
    let totalOrders = await order.find(query).then(result => result);
    let tenPer = Math.round(Number(totalOrders.length) * 0.10);
    let orderlessThanSevenDay = await order.find(query2).then(result => result);
    console.log('=-0=-query', totalOrders.length,orderlessThanSevenDay.length)
    if( Number(tenPer) !== 0 && (orderlessThanSevenDay.length > Number(tenPer) || orderlessThanSevenDay.length > 10) ){
        await user.findByIdAndUpdate({ _id: req.user._id },{ $set: { orderThreshold: true } },{useFindAndModify:false}).exec();
        return cb(true)
    }
    if( orderlessThanSevenDay.length == 0 ){
        await user.findByIdAndUpdate({ _id: req.user._id },{ $set: { orderThreshold: false } },{useFindAndModify:false}).exec();
        return cb(false)
    }
    cb(true)
}
const processOrder = async (req, res) => {
    let prcessOrder = await order.findOne({ order_id: req.body.orderId }).then((prcessOrder) => prcessOrder)
    if(prcessOrder.requested == 'Cancelled'){
        return res.status(200).json({
            title: 'Order already cancelled.', 
            error: true
        });
    }
    checkThreshold(req,async function (data) {
        
        let sevenDays = new Date(moment().subtract(7, 'days').format('YYYY-MM-DD'));
        let userId = req.user.mainUser ? req.user.mainUser : req.user._id
        let sellerData = await user.findOne({ _id: userId }).populate('user_state').then(result => result)
        let orderDate = new Date(prcessOrder.createdAt)
        console.log('=-0=-0=-0dates',orderDate,sevenDays,(orderDate).getTime(), (sevenDays).getTime())
        if( sellerData.orderThreshold && ( (orderDate).getTime() > (sevenDays).getTime() )){
            return res.status(200).json({
                title: 'Please process/cancel orders older than 7 days.', 
                error: true
            });
        }else{
            let query = {};
            req.user = sellerData
            let { orderId, invoice, invoiceNumber, weight, selectedDate, status, order_cancel_reason } = req.body;
            req.body.sellerState = sellerData.user_state.name
            let orderStatus = {
                status: 'Processed',
                description: 'Order has been Processed',
                date: new Date()
            }
            let dir = './assets/invoice/';
            helper.createDir(dir);
            let buyerData = await user.findOne({ _id: prcessOrder.user_id }).populate('user_state').then(result => result)
            let minAmt = prcessOrder.total_amount - (prcessOrder.total_amount / 100 * 10);
            let maxAmt = prcessOrder.total_amount + (prcessOrder.total_amount / 100 * 10);
            let orderType = prcessOrder.paymentType == 'COD' ?
                (req.body.invoice != "" && Number(req.body.invoice) >= minAmt && Number(req.body.invoice) <= maxAmt) : (req.body.invoice != "" && Number(req.body.invoice) >= minAmt && Number(req.body.invoice) <= prcessOrder.total_amount)
            if (orderType) {
                if (prcessOrder) {
                    if (prcessOrder.paymentType !== 'COD' && Number(req.body.invoice) <= prcessOrder.total_amount) {
                        let amt = Number(Number(prcessOrder.total_amount - Number(req.body.invoice)).toFixed(2));
                        let buyerData = await user.findByIdAndUpdate({ _id: prcessOrder.user_id }, { $inc: { wallet_balance: amt } }, { new: true }).exec();
                        if (amt > 0) {
                            let transaction = [];
                            transaction.user_id = prcessOrder.user_id;
                            transaction.settle_amount = amt;
                            transaction.paid_amount = amt;
                            transaction.unique_invoice = prcessOrder.unique_invoice;
                            transaction.narration = "Amount credited for modification of order " + prcessOrder.order_id;
                            transaction.type = 'Credit Received'
                            let paytmPayAmount = 0;
                            transaction.status = 'Success';
                            transaction.closing_bal = buyerData.wallet_balance;
                            transactions.addTransaction(transaction, (error, transactionData) => {
                            })
                        }
                    }
                    if (prcessOrder.paymentType !== 'COD' && prcessOrder.balance > 0) {
                        let newBuyerData = await user.findByIdAndUpdate({ _id: prcessOrder.user_id }, { $inc: { wallet_balance: prcessOrder.balance } }, { new: true }).exec();
                        await order.findOneAndUpdate({ order_id: orderId }, { $set: { balance: 0 } }).exec();
                        let transaction = [];
                        transaction.user_id = prcessOrder.user_id;
                        transaction.settle_amount = prcessOrder.balance;
                        transaction.paid_amount = prcessOrder.balance;
                        transaction.unique_invoice = prcessOrder.unique_invoice;
                        transaction.narration = "Amount credited for modification of order " + prcessOrder.order_id;
                        transaction.type = 'Credit Received'
                        let paytmPayAmount = 0;
                        transaction.status = 'Success';
                        transaction.closing_bal = newBuyerData.wallet_balance;
                        transactions.addTransaction(transaction, (error, transactionData) => {
                        })
                    }
                    let totalAmount = req.body.invoice != "" ? Number(req.body.invoice).toFixed(2) : Number(prcessOrder.total_amount).toFixed(2);
                    let userData = await user.findOne({ _id: prcessOrder.user_id }).populate('user_state').then(result => result);
                    let orderItems = []
                    // let inventoryData = await inventory.findOne({ _id: prcessOrder.products[0].inventory_id }).populate('product_id').then((result => result))
                    await orderItems.push({
                        name: 'Medicine', sku: 'Medicine',
                        units: 1, selling_price: totalAmount, discount: 0
                    })
                    let orderData = {}
                    let zipcodequery = { pincode: userData.user_pincode, [prcessOrder.paymentType == 'COD' ? 'cod' : 'prepaid']: true }
                    req.body.pincode = userData.user_pincode
                    req.body.type = prcessOrder.paymentType
                    req.body.address = userData.user_address
                    req.body.phone = userData.phone
                    req.body.orderId = prcessOrder.order_id
                    req.body.amount = Number(totalAmount) + Number(prcessOrder.delivery_charges);
                    req.body.oldAmt =  (prcessOrder.total_amount).toFixed(2);
                    req.body.newAmt = totalAmount;
                    req.body.state = userData.user_state.name
                    req.body.order_date = prcessOrder.createdAt
                    req.body.company = userData.company_name
                    req.body.orderItems = orderItems
                    req.body.city = userData.user_city
                    req.body.totalAmount = Number(totalAmount) + Number(prcessOrder.delivery_charges);
                    req.body.email = userData.email
                    req.body.orderType = prcessOrder.orderType
                    var mailData = {
                        email: userData.email,
                        subject: 'Order Processed',
                        body:
                            '<p>' +
                            `Your order ${orderId} is processed. Soon it will be dispatched for shipment.`
                    };
                    // You can track your order using ${trackingUrl}`
                    helper.sendEmail(mailData);
                    
                    let orderPayType = (prcessOrder.paymentType).toLowerCase() === 'cod' ? sellerData.preferredCourierCod : (prcessOrder.paymentType).toLowerCase() === 'online' ? sellerData.preferredCourierPrepaid : '' ;

                    if(orderPayType && orderPayType.name && (orderPayType.name).toLowerCase() === 'delhivery'){
                        courierService.delhiveryService(req, res, function (data) {
                            if (data.error == false) {
                                let query = data.query
                                updateOrderHelper(req, res, query)
                            } else {
                                return res.status(200).json({
                                    title: 'This pin-code is not serviceable currently.',
                                    error: true
                                });
                                // courierService.shiprocket(req, res, (query) => {
                                //     updateOrderHelper(req, res, query)
                                // })
                            }
                        })
                    }else if(orderPayType &&  orderPayType.name && (orderPayType.name).toLowerCase() === 'shiprocket'){
                        courierService.shiprocket(req, res, (query) => {
                            updateOrderHelper(req, res, query)
                        })
                    }else{
                        courierService.delhiveryService(req, res, function (data) {
                            if (data.error == false) {
                                let query = data.query
                                updateOrderHelper(req, res, query)
                            } else {
                                return res.status(200).json({
                                    title: 'This pin-code is not serviceable currently.',
                                    error: true
                                });
                                // courierService.shiprocket(req, res, (query) => {
                                //     updateOrderHelper(req, res, query)
                                // })
                            }
                        })
                    }
                } else {
                    return res.status(200).json({
                        title: 'No such order found',
                        error: true
                    });
                }
            } else {
                return res.status(200).json({
                    title: 'Invoice amount is incorrect',
                    error: true
                });
            }
        }
    })        
}
const updateOrderHelper = (req, res, query) => {
    order.findOneAndUpdate({ order_id: req.body.orderId }, query, { new: true })
        .exec(async (error, ProcessedOrder) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong Please wait.',
                    error: true
                });
            }
            if (!ProcessedOrder) {
                return res.status(200).json({
                    title: 'Unable to find order.',
                    error: true
                });
            } else {
                res.status(200).json({
                    title: 'Order details updated successfully.',
                    error: false,
                    detail: ProcessedOrder,
                });

            }

        })
}
const adminProcessOrder = async (req, res) => {
    try {
        let { orderId, invoice, invoiceNumber, weight, selectedDate, status, docket, trackingUrl, awbNo, paymentType, otp, uploadedInvoice, deliveryDate, isBulk } = req.body;
        let ext;
        let dir;
        let base64String;
        var bufferFile;
        let url;
        let oldOrderData = await order.findOne({ order_id: orderId }).exec();
        let getServiceName = oldOrderData.serviceName && oldOrderData.serviceName.length > 1 ? oldOrderData.serviceName : trackingUrl.includes('shiprocket') ? 'shiprocket' : trackingUrl.includes('delhivery') ? 'Delhivery' : '' ;
        systemActivityCon.isValid(req, async (data) => {
            if (data) {
                dir = './assets/invoice/';
                helper.createDir(dir);
                if (docket && docket != '') {
                    ext = docket.substring(docket.indexOf('/') + 1, docket.indexOf(';base64'));
                    base64String = docket.replace(/^data:application\/\w+;base64,/, '');
                    bufferFile = await Buffer.from(base64String, 'base64');
                    url = await helper.BufferUploadS3("order_labels/" + orderId + ".pdf", bufferFile);
                }
                if (docket && ext !== 'pdf') {
                    return res.status(200).json({
                        error: true,
                        title: 'Docket file should be in pdf format'
                    })
                }
                order.findOne({ order_id: orderId }).then(async (prcessOrder) => {
                    let trans = await transactions.findOne({ unique_invoice: prcessOrder.unique_invoice }).exec();
                    if (prcessOrder.requested == 'Cancelled' && status == 'Cancelled') {
                        return res.status(200).json({
                            error: true,
                            title: 'Order is already Cancelled.'
                        })
                    }
                    if (trans && prcessOrder.paymentType == 'Online' && Number(req.body.invoice) < Number(oldOrderData.total_amount)) {
                        let balance = Number(Number(Number(oldOrderData.total_amount) - Number(req.body.invoice)).toFixed(2));
                        let buyerData = await user.findOneAndUpdate({ _id: prcessOrder.user_id }, { $inc: { wallet_balance: balance } }, { new: true }).then(result => result);
                        let transaction = [];
                        transaction.user_id = prcessOrder.user_id;
                        transaction.settle_amount = balance;
                        transaction.paid_amount = balance;
                        transaction.unique_invoice = prcessOrder.unique_invoice;
                        transaction.narration = "Amount credited for Modification of order " + prcessOrder.order_id;
                        transaction.type = 'Credit Received'
                        transaction.status = 'Success';
                        transaction.closing_bal = buyerData.wallet_balance;
                        transactions.addTransaction(transaction, (error, transactionData) => {
                        })
                    }
                    if (prcessOrder.paymentType !== 'COD' && prcessOrder.balance > 0) {
                        let newBuyerData = await user.findByIdAndUpdate({ _id: prcessOrder.user_id }, { $inc: { wallet_balance: prcessOrder.balance } }, { new: true }).exec();
                        await order.findOneAndUpdate({ order_id: prcessOrder.order_id }, { $set: { balance: 0 } }).exec();
                        let transaction = [];
                        transaction.user_id = prcessOrder.user_id;
                        transaction.settle_amount = prcessOrder.balance;
                        transaction.paid_amount = prcessOrder.balance;
                        transaction.unique_invoice = prcessOrder.unique_invoice;
                        transaction.narration = "Amount credited for modification of order " + prcessOrder.order_id;
                        transaction.type = 'Credit Received' 
                        let paytmPayAmount = 0;
                        transaction.status = 'Success';
                        transaction.closing_bal = newBuyerData.wallet_balance;
                        transactions.addTransaction(transaction, (error, transactionData) => {
                        })
                    }
                    if (prcessOrder) {
                        let totalAmount = req.body.invoice != "" ? Number(req.body.invoice).toFixed(2) : Number(prcessOrder.total_amount).toFixed(2);
                        let orderStatus = (status == 'Delivered') ? {
                            status: 'Delivered',
                            description: `Order has been marked delivered by Admin (${req.user.first_name}), Old:${Number(oldOrderData.total_amount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, New:${Number(totalAmount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, oldPaymentType: ${oldOrderData.paymentType}, oldIsBulk:${oldOrderData.isBulk}`,
                            date: deliveryDate
                        } : (status == 'Cancelled') ? {
                            status: 'Cancelled',
                            description: `Order has been marked cancelled by Admin (${req.user.first_name}), Old:${Number(oldOrderData.total_amount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, New:${Number(totalAmount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, oldPaymentType: ${oldOrderData.paymentType}, oldIsBulk:${oldOrderData.isBulk}`,
                            date: deliveryDate ? deliveryDate : new Date()
                        } : {
                            status: 'Processed',
                            description: `Order has been marked processed by Admin (${req.user.first_name}), Old:${Number(oldOrderData.total_amount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, New:${Number(totalAmount).toLocaleString('en-IN',{maximumFractionDigits:2, minimumFractionDigits:2})}, oldPaymentType: ${oldOrderData.paymentType}, oldIsBulk:${oldOrderData.isBulk}`,
                            date: new Date()
                        }
                        let query;
                        if (docket != '' && uploadedInvoice != '') {
                            query = {
                                $set: {
                                    uploadedInvoice: req.body.uploadedInvoice ? helper.base64Upload(req, res, dir, req.body.uploadedInvoice) : '',
                                    total_amount: Number(totalAmount),
                                    process_date: selectedDate, seller_invoice: invoiceNumber, weight: weight, requested: (status == 'Delivered') ? 'Delivered' : status, docket: url,
                                    trackingUrl,
                                    shipment_id: awbNo, paymentType: paymentType, isBulk: isBulk,
                                    serviceName: getServiceName
                                }, $push: { order_status: orderStatus }
                            }
                        } else if (docket != '') {
                            query = {
                                $set: {
                                    total_amount: Number(totalAmount),
                                    process_date: selectedDate, seller_invoice: invoiceNumber, weight: weight, requested: (status == 'Delivered') ? 'Delivered' : status, docket: url,
                                    trackingUrl,
                                    shipment_id: awbNo, paymentType: paymentType, isBulk: isBulk,
                                    serviceName: getServiceName
                                }, $push: { order_status: orderStatus }
                            }
                        } else if (uploadedInvoice != '') {
                            query = {
                                $set: {
                                    uploadedInvoice: req.body.uploadedInvoice ? helper.base64Upload(req, res, dir, req.body.uploadedInvoice) : '',
                                    total_amount: Number(totalAmount),
                                    process_date: selectedDate, seller_invoice: invoiceNumber, weight: weight, requested: (status == 'Delivered') ? 'Delivered' : status,
                                    trackingUrl,
                                    shipment_id: awbNo, paymentType: paymentType, isBulk: isBulk,
                                    serviceName: getServiceName
                                }, $push: { order_status: orderStatus }
                            }
                        } else {
                            query = {
                                $set: {
                                    total_amount: Number(totalAmount),
                                    process_date: selectedDate, seller_invoice: invoiceNumber, weight: weight, requested: (status == 'Delivered') ? 'Delivered' : status,
                                    trackingUrl,
                                    shipment_id: awbNo, paymentType: paymentType, isBulk: isBulk,
                                    serviceName: getServiceName
                                }, $push: { order_status: orderStatus }
                            }
                        }
                        user.findOne({ _id: ObjectId(prcessOrder.user_id) }).then((ProcessedOrder) => {
                            var mailData = (status == 'Delivered') ? {
                                email: ProcessedOrder.email,
                                subject: 'Order Delivered',
                                body:
                                    '<p>' +
                                    `Your order ${orderId} is Delivered.`
                            } : {
                                    email: ProcessedOrder.email,
                                    subject: 'Order Processed',
                                    body:
                                        '<p>' +
                                        `Your order ${orderId} is processed. Soon it will be dispatched for shipment.`+`${trackingUrl.length > 0 ? `You can track your order using ${trackingUrl}` :''} `
                                }
                            helper.sendEmail(mailData);
                        })

                        order.findOneAndUpdate({ order_id: orderId }, query, { new: true }).exec(async (error, ProcessedOrder) => {
                            if(status == 'Delivered'){
                              helper.insertSettlement(ProcessedOrder._id,deliveryDate)  
                            }
                            res.status(200).json({
                                error: false,
                                title: `Order ${status} successfully`
                            })
                        })
                    } else {
                        return res.status(200).json({
                            error: true,
                            title: 'Either this order has been already processed or could not find this order'
                        })
                    }
                })
            } else {
                return res.status(200).json({
                    error: true,
                    title: 'Invalid OTP or OTP might be expired'
                })
            }
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }

}
const checkCancelledOrder = async (order_id) => {
    let data = await order.findOne({ order_id, 'order_status.status': 'Processed' }).then(result => result)
    return data
}
const cancelledOrder = async (order_id) => {
    let data = await order.findOne({ order_id, 'order_status.status': 'Cancelled' }).then(result => result)
    return data
}
const delivered = async (order_id) => {
    let data = await order.findOne({ order_id, 'order_status.status': 'Delivered' }).then(result => result)
    return data
}
const cancelOrder = async (req, res) => {
    helper.shipRokectLogin(async (loginResult) => {
        if (loginResult.status == 200) {
            let shipToken = loginResult.data.token
            let query = {};
            let { orderId, invoice, invoiceNumber, weight, selectedDate, status, order_cancel_reason } = req.body;
            if (status === 'Processed') {
                let orderStatus = {
                    status: 'Processed',
                    description: 'Order has been Processed',
                    date: new Date()
                }
                query = { $set: { process_date: selectedDate, seller_invoice: invoiceNumber, total_amount: invoice, weight: weight, requested: "Processed" }, $push: { order_status: orderStatus } }
            } else if (status === 'Cancelled') {
                let alreadyCancelled = await cancelledOrder(orderId);
                let deliveredOrder = await delivered(orderId);
                if (alreadyCancelled) {
                    return res.status(200).json({
                        title: 'Order already has been cancelled',
                        error: true
                    });
                }
                if (deliveredOrder) {
                    return res.status(200).json({
                        title: 'Delivered orders cannot be cancelled',
                        error: true
                    });
                }
                let orderData = await checkCancelledOrder(orderId);
                let validOtp;
                await systemActivityCon.isValid(req, (data) => {
                    validOtp = data;
                });
                if (!validOtp && req.user.user_type == 'admin') {
                    return res.status(200).json({
                        error: true,
                        title: 'Invalid OTP or OTP might be expired'
                    })
                }
                if (orderData && req.user.user_type != 'admin') {
                    return res.status(200).json({
                        title: 'You cannot cancel this order as this has been processed',
                        error: true
                    });
                }
                let orderStatus;
                if(req.user.user_type == 'admin'){
                    orderStatus = {
                        status: 'Cancelled',
                        date: new Date(),
                        description: `Order has been Cancelled by ${req.user.first_name}(Admin)`
                    }
                }else{
                    orderStatus = {
                        status: 'Cancelled',
                        date: new Date(),
                        description: `Order has been Cancelled by ${req.user.company_name}`
                    }
                }
                query = { $set: { paymentStatus: 'failed', order_cancel_reason: order_cancel_reason, requested: "Cancelled", cancelledBy: req.user._id }, $push: { order_status: orderStatus } }
            }

            if (Object.keys(query).length > 0) {
                order.findOneAndUpdate({ order_id: orderId }, query, { new: true })
                    .exec(async (error, ProcessedOrder) => {
                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong Please wait.',
                                error: true
                            });
                        }
                        if (!ProcessedOrder) {
                            return res.status(200).json({
                                title: 'Unable to find order.',
                                error: true
                            });
                        } else {
                            if (status === 'Cancelled') {
                                let userData = req.user;
                                user.findUserById(ProcessedOrder.user_id, (error, userDetail) => {
                                    rollBackSingleOrder(ProcessedOrder)
                                    
                                    let restock = []
                                    req.body.products && req.body.products.length > 0 && ProcessedOrder.products.map(async(data) => {
                                    let index = req.body.products.findIndex((e) => e.inventory_id == data.inventory_id);
                                        if (index == -1) {
                                            restock.push(data)
                                        }
                                    })
                                    if(req.user.user_type === "admin"){
                                        updateInventoryInOrder(ProcessedOrder.products, 'cancel');
                                    }else{
                                        updateInventoryInOrder(restock, 'cancel');
                                    }
                                    if (userData.user_type == 'seller') {
                                        let remProd = [];
                                        if(req.body.products){
                                            req.body.products && req.body.products.length > 0 && req.body.products.map(async(data) => {	
                                            if (data) {
                                                if(data.productName && data.product_id && data.quantity && data.inventory_id){
                                                    remProd.push({name:data.productName, prodId: data.product_id});
                                                    let foundInventory = await ActiveInventory.find({'Product._id':ObjectId(data.product_id)}).limit(3).then(result => result );
                                                    let ns = new shortBook({	
                                                        product_name: data.productName,	
                                                        product_id: data.product_id,	
                                                        user_id: ProcessedOrder.user_id,	
                                                        quantity: Number(data.quantity),
                                                        inStock: foundInventory.length > 1 ? true : false	
                                                    })	
                                                    ns.save();
                                                    inventory.updateInventory(data.inventory_id, 'outOfStock','');
                                                    let flag = 'Product Added To ShortBook'
                                                    let msg = `Product ${data.productName.toUpperCase()} is added to your shortbook successfully !` 
                                                    await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
                                                    if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                                                        let devicetoken = userDetail.device_token;
                                                        let title = 'Product Added To ShortBook'
                                                        let payload = {};
                                                        await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                                                    }
                                                }
                                            }	
                                            })
                                            order.findOneAndUpdate({ order_id: orderId },{ $push: { removedProd: remProd } }).exec();
                                        }
                                        // else{
                                        //     ProcessedOrder.products && ProcessedOrder.products.length > 0 && ProcessedOrder.products.map(data => {	
                                        //         if (data) {
                                        //             inventory.findById(data.inventory_id, (error, invenData) => {
                                        //                 let ns = new shortBook({	
                                        //                     product_name: data.productName,	
                                        //                     product_id: invenData.product_id,	
                                        //                     user_id: ProcessedOrder.user_id,	
                                        //                     quantity: Number(data.quantity)
                                        //                 })	
                                        //                 ns.save();
                                        //             })
                                        //             inventory.updateInventory(data.inventory_id, 'outOfStock','');
                                        //         }	
                                        //     })
                                        // }
                                        // get user details by id and send notification if device token is available.
                                        let flag = 'Order Declined';
                                        let msg;
                                        if(ProcessedOrder.paymentType === 'COD'){
                                            msg = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the seller due to - ${ProcessedOrder.order_cancel_reason}. For more details visit MEDIMNY.`;
                                        }else{
                                            msg = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the seller due to - ${ProcessedOrder.order_cancel_reason}.Refund process will be completed and credited to your MediWallet. For more details visit MEDIMNY.`;
                                        }
                                        
                                        notification.addNotification(msg, userDetail, '', flag, (error, response) => {
                                        })

                                        if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                                            let devicetoken = userDetail.device_token;
                                            let title = 'Order Cancelled'
                                            let payload = {};

                                            helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                            });
                                        }

                                        // complete cancell order proccess and return the response
                                        let msgBody;
                                        if(ProcessedOrder.paymentType === 'COD'){
                                            msgBody = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the seller due to - ${ProcessedOrder.order_cancel_reason}. For more details visit MEDIMNY.`;
                                        }else{
                                            msgBody = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the seller due to - ${ProcessedOrder.order_cancel_reason}.Refund process will be completed and credited to your MediWallet. For more details visit MEDIMNY.`;
                                        }
                                        
                                        helper.sendSMS(userDetail.phone, msgBody);
                                        let emailBody;
                                        if(ProcessedOrder.paymentType === 'COD'){
                                            emailBody = '<p>' +
                                            ` We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the seller due to - ${ProcessedOrder.order_cancel_reason}. For more details visit MEDIMNY. `
                                        }else{
                                            emailBody = '<p>' +
                                            ` We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the seller due to - ${ProcessedOrder.order_cancel_reason}. Refund process will be completed and credited to your MediWallet. For more details visit MEDIMNY.`
                                        }
                                        var mailData = {
                                            email: userDetail.email,
                                            subject: 'Order Cancelled',
                                            body: emailBody
                                        };
                                        helper.sendEmail(mailData);
                                        var mailData2 = {
                                            email: 'noreply@medimny.com',
                                            subject: 'Order Cancelled',
                                            body: emailBody
                                        };
                                        helper.sendEmail(mailData2);
                                    }
                                    if (userData.user_type == 'admin') {
                                        // get user details by id and send notification if device token is available.
                                        let flag = 'Order Declined';
                                        let msg;
                                        if(ProcessedOrder.paymentType === 'COD'){
                                            msg = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the MEDIMNY due to - ${ProcessedOrder.order_cancel_reason} For more details visit MEDIMNY.`;
                                        }else{
                                            msg = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the MEDIMNY due to - ${ProcessedOrder.order_cancel_reason} Refund process will be completed and credited to your MediWallet. For more details visit MEDIMNY.`;
                                        }
                                        notification.addNotification(msg, userDetail, '', flag, (error, response) => {
                                        })
                                        if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                                            let devicetoken = userDetail.device_token;
                                            let title = 'Order Cancelled'
                                            let payload = {};
                                            helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                            });
                                        }
                                        // complete cancell order proccess and return the response
                                        let msgBody;
                                        if(ProcessedOrder.paymentType === 'COD'){
                                            msgBody = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the MEDIMNY due to - ${ProcessedOrder.order_cancel_reason} For more details visit MEDIMNY.`;
                                        }else{
                                            msgBody = `We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the MEDIMNY due to - ${ProcessedOrder.order_cancel_reason} Refund process will be completed and credited to your MediWallet. For more details visit MEDIMNY.`;
                                        }
                                        helper.sendSMS(userDetail.phone, msgBody);
                                        let emailBody;
                                        if(ProcessedOrder.paymentType === 'COD'){
                                            emailBody = '<p>' +
                                            ` We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the MEDIMNY due to - ${ProcessedOrder.order_cancel_reason} For more details visit MEDIMNY. `
                                        }else{
                                            emailBody = '<p>' +
                                            ` We’re sorry to inform you that your Order ${ProcessedOrder.order_id} has been declined by the MEDIMNY due to - ${ProcessedOrder.order_cancel_reason} Refund process will be completed and credited to your MediWallet. For more details visit MEDIMNY.`
                                        }
                                        var mailData = {
                                            email: userDetail.email,
                                            subject: 'Order Cancelled',
                                            body: emailBody
                                        };
                                        helper.sendEmail(mailData);
                                        var mailData2 = {
                                            email: 'noreply@medimny.com',
                                            subject: 'Order Cancelled',
                                            body: emailBody
                                        };
                                        helper.sendEmail(mailData2);
                                    }
                                    return res.status(200).json({
                                        title: 'Order Cancelled successfully.',
                                        error: false,
                                        details: []
                                    });
                                });
                            } else {

                                res.status(200).json({
                                    title: 'Order details updated successfully.',
                                    error: false,
                                    detail: ProcessedOrder
                                });
                                if (status === 'Processed') {
                                    let userData = await user.findOne({ _id: ProcessedOrder.user_id }).populate('user_state').then(result => result);
                                    let orderItems = []
                                    async.eachOfSeries(ProcessedOrder.products, async (val, key, cb) => {
                                        let inventoryData = await inventory.findOne({ _id: val.inventory_id }).populate('product_id').then((result => result))
                                        await orderItems.push({
                                            name: val.name, sku: inventoryData.product_id.sku,
                                            units: val.quantity, selling_price: val.PTR, discount: val.discount_per ? val.discount_per : 0
                                        })
                                    }, function (err) {

                                        let orderData = {}
                                        orderData.order_id = orderId
                                        orderData.order_date = moment(ProcessedOrder.createdAt).format("YYYY-MM-DD hh:mm")
                                        orderData.pickup_location = "AJAY RAN"
                                        orderData.billing_customer_name = userData.first_name
                                        orderData.billing_last_name = userData.last_name
                                        orderData.billing_address = userData.user_address
                                        orderData.billing_city = userData.user_city
                                        orderData.billing_state = userData.user_state.name
                                        orderData.billing_country = userData.user_country
                                        orderData.billing_email = userData.email
                                        orderData.billing_pincode = userData.user_pincode
                                        orderData.billing_phone = userData.phone
                                        orderData.shipping_is_billing = 1
                                        orderData.order_items = orderItems
                                        orderData.weight = weight
                                        orderData.sub_total = ProcessedOrder.total_amount
                                        orderData.payment_method = 'COD'
                                        orderData.length = 10
                                        orderData.breadth = 20
                                        orderData.height = 30

                                        request.post({
                                            url: 'https://apiv2.shiprocket.in/v1/external/orders/create/adhoc',
                                            form: orderData,
                                            json: true,
                                            headers: {
                                                'Content-Type': 'application/json',
                                                'Authorization': `Bearer ${shipToken}`
                                            }
                                        }, function (error, response, body) {

                                        });
                                    })
                                }
                            }
                        }

                    })
            } else {
                return res.status(200).json({
                    title: 'Something went wrong Please wait.',
                    error: true
                });
            }
        } else {
            return res.status(200).json({
                title: 'Something went wrong from Shipment user. Please try again or contact to admin.',
                error: true
            });
        }
    })

}

/*
# parameters: token,
# purpose: list all the order with status Cancelled and Delivered;
*/
const orderHistory = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    let matchQuery = {}
    matchQuery = req.body.searchText ? { $or:[{order_id: { $regex: req.body.searchText, $options: 'i' }},
    { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } }] } : {}
    let query = {};
    let d = new Date();
    let firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
    let lastDay = new Date(d.getFullYear(), d.getMonth()+1);
    if(req.body.from_date && req.body.from_date != req.body.to_date){
        firstDay = new Date(req.body.from_date)
    }
    if(req.body.to_date && req.body.from_date != req.body.to_date){
        lastDay = new Date(req.body.to_date) 
    }
    let matchDateQuery = ( (req.user.user_type === "admin" || req.user.user_type === "seller") && req.body.from_date && req.body.to_date) ? {
        $and:[{ "createdAt":{$gte: firstDay}},{ "createdAt":{$lte: lastDay} }] 

    } : ( (req.user.user_type === "admin" || req.user.user_type === "seller") && req.body.from_date === '' || req.body.to_date === '') ? {
        $and:[{ "createdAt":{$gte: firstDay}},{ "createdAt":{$lte: lastDay} }]

    }: ( req.user.user_type === "seller" && req.body.month && req.body.year) ? {
        "year": req.body.year ? Number(req.body.year) : year,
        "month": req.body.month ? getMonthNumber(req.body.month) : monthN
    } : {};
    firstDay.setHours(0,0,0,0);
    lastDay.setHours(23,59,59,999);
    if (req.user.user_type == 'admin') {
        query1 = {
            is_deleted: false,
        }
        if (req.body.seller_id && req.body.seller_id != '' && req.body.seller_id !== undefined) {
            query1 = {
                ...query1,
                'seller_id': ObjectId(req.body.seller_id)
            }
        }
        if (req.body.status && req.body.status !== '' && req.body.status !== undefined) {
            query1 = {
                ...query1,
                'current_status.status': req.body.status
            }
        } else {
            query1 = {
                ...query1,
                $or: [{ 'current_status.status': { $eq: 'Delivered' } }, { 'current_status.status': { $eq: 'Cancelled' } }]
            }
        }
        query =
            [{
                $addFields: {
                    current_status: { $arrayElemAt: ["$order_status", -1] },
                }
            },
            // {
            //     $addFields: {
            //         year: { $year: "$createdAt" },
            //         month: { $month: "$createdAt" }
            //     }
            // },
            // {
            //     $match: {
            //         year: (req.body.year),
            //         month: sellerController.getMonthNumber(req.body.month)
            //     }
            // },
            {
                $match: query1
            },
            {
                $lookup: {
                    localField: 'user_id',
                    foreignField: '_id',
                    from: 'users',
                    as: 'user_id'
                }
            },
            {
                $unwind: {
                    path: '$user_id',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'seller_id',
                    foreignField: '_id',
                    from: 'users',
                    as: 'seller_id'
                }
            },
            {
                $unwind: {
                    path: '$seller_id',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$Products',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'Products.inventory_id',
                    foreignField: '_id',
                    from: 'inventories',
                    as: 'inventory_id'
                }
            },
            {
                $lookup: {
                    localField: 'Products.discount_on_product',
                    foreignField: '_id',
                    from: 'discounts',
                    as: 'discount_on_product'
                }
            },
            {
                $match: matchQuery
            },
            { 
                $match: matchDateQuery 
            },
            { $sort: { createdAt: -1 } },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                    data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
                }
            }]
    } else {
        query = [
        {
                $addFields: {
                    year: { $year: "$createdAt" },
                    month: { $month: "$createdAt" }
                }
        },
        {
            $match: {
                seller_id: Id,
                is_deleted: false,
                $or: [{ 'order_status.status': { $eq: 'Delivered' } }, { 'order_status.status': { $eq: 'Cancelled' } }]
            }
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'user_id'
            }
        },
        {
            $unwind: {
                path: '$user_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$Products',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Products.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'inventory_id'
            }
        },
        {
            $lookup: {
                localField: 'Products.discount_on_product',
                foreignField: '_id',
                from: 'discounts',
                as: 'discount_on_product'
            }
        },
        {
            $match: matchQuery
        },
        { 
            $match: matchDateQuery 
        },
        { $sort: { createdAt: -1 } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
            }
        }] 
    };
    order.aggregate(query)
        .then((detail) => {
            res.status(200).json({
                title: 'Order Fetched successfully.',
                error: false,
                detail
            })
        })
};
const orderHistory1 = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    let query = {}
    if(req.user.user_type != 'admin'){
        query = { seller_id:ObjectId(Id),is_deleted: false }
    }else{
        query = { is_deleted: false }
    }
    let firstDay = new Date(req.body.from_date)
    let lastDay = new Date(req.body.to_date)
    console.log('-0652652',firstDay,lastDay)
    let orQuery=[]
    if( !req.body.status ){
        orQuery.push(
            {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Delivered'] }},
            {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'RTO'] }},
            {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Cancelled'] }}
        )
    }
    if( req.body.searchText ){
        orQuery.push(
            { "order_id": { $regex: req.body.searchText, $options: 'i' } },
            { "unique_invoice": { $regex: req.body.searchText, $options: 'i' } },
            {"buyerCompName": { $regex: req.body.searchText, $options: 'i' }},
            {"sellerCompName": { $regex: req.body.searchText, $options: 'i' }}
            )
    }
    let matchQuery = [
        { "createdAt": { $gte: firstDay } },
        { "createdAt": { $lt: lastDay } },
        {'paymentStatus':{$ne:'pending'}},
        // {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Delivered'] }},
        // {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'RTO'] }},
        // {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Cancelled'] }}
    ]
    if(req.body.status){
            query = {
                ...query, $expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, req.body.status] },
                $and: [{ $expr: { $gte: [{ "$arrayElemAt": ["$order_status.date", -1] }, firstDay] } },
                { $expr: { $lt: [{ "$arrayElemAt": ["$order_status.date", -1] }, lastDay] } },
                { 'paymentStatus': { $ne: 'pending' } }]
            }
    }else{
        query={...query, $and: matchQuery }
    }
    if(req.body.searchText && req.body.status){
        query = { ...query, $or: orQuery }
    }
    if(req.body.type && req.body.type !='Bulk'){
        query = {...query, paymentType: req.body.type}
    }else if(req.body.type && req.body.type == 'Bulk'){
        query = {...query, isBulk: true}
    }
    if(req.body.seller_id && req.user.user_type == 'admin'){
        query = {...query, seller_id: ObjectId(req.body.seller_id)}
    }
    if(req.body.buyer_id && req.user.user_type == 'admin'){
        query = {...query, user_id: ObjectId(req.body.buyer_id)}
    }
    let query2 = {path:'user_id'}
    
    let limit = parseInt(100);
    let skip = (parseInt(req.body.page ? req.body.page : 1) - 1) * parseInt(limit);
    console.log('-==-0=0-=0-=',query)
    order.find(query).skip(skip).limit(limit).populate('seller_id').populate(query2).sort({'createdAt':-1}).then((detail)=>{
        order.countDocuments(query).then((count)=>{
            let data ={ detail,count}
            res.status(200).json({
             title:'success',
             error: false,
             data
            })
        })
    })
};
/*
# parameters: token,
# purpose: fetch all the data related to orders i.e total order,revenue etc.
# Pending Orders, Monthly Orders, Monthly Sales, Total Orders, Total Sales
*/
const dashboardSalesStats = async (req, res) => {
    let date = new Date();
    //first date//
    let year = req.body.year ? req.body.year : date.getFullYear()
    let date1 = new Date(`${year}-01-01`) ;
    //first date
    
    let newApr1 = new Date(moment(date1).month("April").startOf('month').format('YYYY-MM-DD'));
    let newMar31 = new Date(moment(date1).add('1','year').month("March").endOf('month').format('YYYY-MM-DD'));
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    //first date of year
    let startM = new Date(moment().startOf('month').format('YYYY-MM-DD'));
    // let newApr1 = new Date(moment().month("April").startOf('month').format('YYYY-MM-DD'));
    // let newMar31 = new Date(moment().add('1','year').month("March").endOf('month').format('YYYY-MM-DD'));

    let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    if (req.body.id) {
        Id = ObjectId(req.body.id);
    }
    let montlyRevenueOrders = await order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                "createdAt": { $gte: newApr1, $lte: newMar31 },
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: { $substr: ['$createdAt', 5, 2] },
                orders: { $sum: 1 },
                amount: { $sum: { $toDouble: "$total_amount" } }
            }
        }
    ]);
    let pendingOrders = await order.aggregate([
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                "paymentStatus": {$ne : 'pending'}
            }
        },
        { $project: { id: 1, myField: { $slice: ["$order_status", -1] } } },
        { $match: { "myField.status": "New" } },
        {
            $group: {
                _id: "$seller_id",
                total: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);
    let monthlyOrders = await order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                "createdAt": { $gte: startM },
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: "$seller_id",
                total: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);

    let monthlySales = await order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                "createdAt": { $gte: startM },
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: "$seller_id",
                total: { $sum: { $toDouble: "$total_amount" } },
                value: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);

    let totalOrders = await order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: "$seller_id",
                total: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);
    let totalSales = await order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: "$seller_id",
                total: { $sum: { $toDouble: "$total_amount" } },
                value: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);
    res.status(200).json({
        title: 'Dashboard data Fetched successfully.',
        error: false,
        pendingOrders: pendingOrders.length > 0 ? pendingOrders[0].total : 0,
        monthlyOrders: monthlyOrders.length > 0 ? monthlyOrders[0].total : 0,
        monthlySales: monthlySales.length > 0 ? monthlySales[0].total : 0,
        totalOrders: totalOrders.length > 0 ? totalOrders[0].total : 0,
        totalSales: totalSales.length > 0 ? Math.floor(totalSales[0].total) : 0,
        montlyRevenueOrders
    })
}

const addCourierResponse = (req, res) => {
    order.findOne({ ship_order_id: req.body.order_id }).then((orderData, error) => {
        if (orderData && orderData.requested !== 'Delivered') {
            orderData.order_status.push({ status: req.body.current_status, date: req.body.scans[0].date, description: req.body.scans[0].activity })
            orderData.save().then((result) => {
                res.status(200).json({
                    title: "Response saved successfully.",
                    error: false,
                })
            })
        } else {
            res.status(200).json({
                title: "Something went wrong, Please try again in sometime.",
                error: true,
                detail: error
            })
        }

    })
}

const orderHistoryBuyers = (req, res) => {
    let matchQuery = {}
    matchQuery = req.body.searchText ? { order_id: { $regex: req.body.searchText, $options: 'i' } } : {}
    let d = new Date();
    let year = d.getFullYear();

    let query = {};
    query = (req.body.year && req.body.month) ? [{
        $match: {
            user_id: req.user._id,
            is_deleted: false,
            $or: [{ 'order_status.status': { $eq: 'Delivered' } }, { 'order_status.status': { $eq: 'Cancelled' } },{ 'order_status.status': { $eq: 'RTO' } }]
        }
    },
    {
        $addFields: {
            year: { $year: "$createdAt" },
            month: { $month: "$createdAt" }
        }
    },
    {
        $match: {
            year: Number(req.body.year),
            month: sellerController.getMonthNumber(req.body.month)
        }
    },
    {
        $lookup: {
            localField: 'user_id',
            foreignField: '_id',
            from: 'users',
            as: 'user_id'
        }
    },
    {
        $unwind: {
            path: '$user_id',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller_id'
        }
    },
    {
        $unwind: {
            path: '$seller_id',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $unwind: {
            path: '$Products',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'Products.inventory_id',
            foreignField: '_id',
            from: 'inventories',
            as: 'inventory_id'
        }
    },
    {
        $lookup: {
            localField: 'Products.discount_on_product',
            foreignField: '_id',
            from: 'discounts',
            as: 'discount_on_product'
        }
    },
    {
        $lookup: {
            localField: 'Products.GST',
            foreignField: '_id',
            from: 'gsts',
            as: 'Products.GST'
        }
    },
    {
        $match: matchQuery
    },
    { $sort: { createdAt: -1 } },
    {
        $facet: {
            metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
            data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
        }
    }] : [{
        $match: {
            user_id: req.user._id,
            is_deleted: false,
            $or: [{ 'order_status.status': { $eq: 'Delivered' } }, { 'order_status.status': { $eq: 'Cancelled' } },{ 'order_status.status': { $eq: 'RTO' } }]
        }
    },
    {
        $addFields: {
            year: { $year: "$createdAt" },
        }
    },
    {
        $match: {
            year: Number(year),
        }
    },
    {
        $lookup: {
            localField: 'user_id',
            foreignField: '_id',
            from: 'users',
            as: 'user_id'
        }
    },
    {
        $unwind: {
            path: '$user_id',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller_id'
        }
    },
    {
        $unwind: {
            path: '$seller_id',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $unwind: {
            path: '$Products',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'Products.inventory_id',
            foreignField: '_id',
            from: 'inventories',
            as: 'inventory_id'
        }
    },
    {
        $lookup: {
            localField: 'Products.discount_on_product',
            foreignField: '_id',
            from: 'discounts',
            as: 'discount_on_product'
        }
    },
    {
        $lookup: {
            localField: 'Products.GST',
            foreignField: '_id',
            from: 'gsts',
            as: 'Products.GST'
        }
    },
    {
        $match: matchQuery
    },
    { $sort: { createdAt: -1 } },
    {
        $facet: {
            metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
            data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
        }
    }]

    order.aggregate(query)
        .then((detail) => {
            res.status(200).json({
                title: 'Order Fetched successfully.',
                error: false,
                detail
            })
        })
}


const orderListingBuyers = (req, res) => {
    let d = new Date();
    let year = d.getFullYear();
    let query2 = {
        isPending: {

            $and: [{
                $eq: [
                    '$paymentType',
                    'Online'
                ]
            },
            {
                $eq: [
                    '$paymentStatus',
                    'pending'
                ]
            }
            ]

        }
    }
    let query = []
    query.push(
        {
            $addFields: query2
        }
    )
    req.body.searchText ? 
        query.push(
            {
                $match: {
                    user_id: ObjectId(req.user._id),
                    order_id: { $regex: new RegExp(req.body.searchText, 'i') },
                    is_deleted: false,
                    isPending: false,
                    $and: [{ 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } },{ 'order_status.status': { $ne: 'RTO' } }]
                }
            }
        )
        :
        query.push(
            {
                $match: {
                    user_id: ObjectId(req.user._id),
                    is_deleted: false,
                    isPending: false,
                    $and: [{ 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } },{ 'order_status.status': { $ne: 'RTO' } }]
                }
            }
        );
    (req.body.month && req.body.year) ?
            query.push({
                $addFields: {
                    year: { $year: "$createdAt" },
                    month: { $month: "$createdAt" }
                }
            },
            {
                $match: {
                    year: Number(req.body.year),
                    month: sellerController.getMonthNumber(req.body.month)
                }
            }) :
            query.push({
                $addFields: {
                    year: { $year: "$createdAt" },
                }
            },
            {
                $match: {
                    year: Number(year),
                }
            });
    query.push(
    {
        $lookup: {
            localField: 'user_id',
            foreignField: '_id',
            from: 'users',
            as: 'user_id'
        }
    },
    {
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller_id'
        }
    },
    {
        $unwind: {
            path: '$seller_id',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $unwind: {
            path: '$user_id',
            preserveNullAndEmptyArrays: true
        }
    },
    { $sort: { createdAt: -1 } },
    {
        $facet: {
            metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
            data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
        }
    })
    order.aggregate(query).allowDiskUse(true)
        .then((detail) => {
            res.status(200).json({
                title: 'Order Fetched successfully.',
                error: false,
                detail
            })
        })
}


const checksum = (req, res) => {
    var params = {};
    params['MID'] = PaytmConfig.mid;
    params['WEBSITE'] = PaytmConfig.website;
    params['CHANNEL_ID'] = 'WEB';
    params['INDUSTRY_TYPE_ID'] = 'Retail';
    params['ORDER_ID'] = 'TEST_' + new Date().getTime();
    params['CUST_ID'] = 'Customer001';
    params['TXN_AMOUNT'] = '1.00';
    params['CALLBACK_URL'] = 'http://localhost:4002/callback';
    params['EMAIL'] = 'abc@mailinator.com';
    params['MOBILE_NO'] = '7777777777';

    checksum_lib.genchecksum(params, PaytmConfig.key, function (err, checksum) {

    });

}


// check paytyme responce and verify checksum
const getReponse = async (req, res) => {
    let paytmParams = req.body;
    let errorCheck = 0;
    //check online payment amount from transaction whose payment status is online.

    let transactionDetails = await transactions.findOne({ type: 'Online', unique_invoice: req.body.ORDERID }).exec();
    if (!transactionDetails || (transactionDetails.paid_amount != req.body.TXNAMOUNT)) {
        errorCheck = 1;
    }
    var isValidChecksum = checksum_lib.verifychecksum(paytmParams, PaytmConfig.key, req.body.CHECKSUMHASH);
    if (isValidChecksum) {

    } else {
    }

    if (req.body.STATUS == 'TXN_SUCCESS' && errorCheck == 0 && isValidChecksum) {
        transactions.findOneAndUpdate({ _id: transactionDetails._id }, { status: 'Success' }).then((result) => {
        });
        return res.redirect('/order/redirect');
    } else {
        order.findOne({ unique_invoice: req.body.ORDERID }).exec((error, orderDetails) => {
            inventory.updateInventoryQuantity(orderDetails.products, 'cancel', (response) => {
            });
        });

        transactions.findOneAndUpdate({ _id: transactionDetails._id }, { status: 'Failed' }).exec();
        let statusData = {
            status: 'Failed',
            date: new Date(),
            description: 'Order has been Failed due to payment.'
        }
        query = { $push: { order_status: statusData } }

        order.updateMany({ unique_invoice: transactionDetails.unique_invoice }, query).exec();
        transactions.findOne({ type: 'Credit Used', unique_invoice: req.body.ORDERID }).exec((error, result) => {
            if (result) {

                user.findOneAndUpdate({ _id: result.user_id }, { $inc: { wallet_balance: result.paid_amount } });

                let transaction = [];
                transaction.user_id = result.user_id;
                transaction.settle_amount = result.paid_amount;
                transaction.paid_amount = result.paid_amount;
                transaction.unique_invoice = result.unique_invoice;
                transaction.type = 'Credit Received';
                // transaction.narration = orderID;
                transaction.status = 'Success';
                transactions.addTransaction(transaction, (error, transactionData) => {
                    return res.redirect('/order/redirect');
                });
            } else {
                return res.redirect('/order/redirect');
            }

        });
    }

}



const exportCsv = (req, res) => {
    // const fields = ["sys_id", "id", "date", "seller", "order_status", "items", "payment_type", "amount", "delivery_charges"];
    let fields = [];
    req.user.user_type === "admin" ?
        fields = ["id", "date", "seller_invoice", "seller", "buyer", "order_status", "delivery_date", "amount"] :
        fields = ["id", "date", "seller_invoice", "buyer", "order_status", "delivery_date", "amount"];
    let deli_date;
    let Id;
    let query = {}
    let orderArr = []
    let firstDay = new Date(req.query.from_date)
    let lastDay = new Date(req.query.to_date)
    firstDay.setHours(0, 0, 0, 0);
    lastDay.setHours(23, 59, 59, 999);
    if (req.user.user_type === "admin") {
        query = {
            is_deleted: false,
            //$and: [{ 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } }]
        }
    }
    else {
        Id = req.user.mainUser ? req.user.mainUser : req.user._id
        query = {
            seller_id: ObjectId(Id),
            is_deleted: false,
            //$and: [{ 'order_status.status': { $ne: 'Draft' } }, { 'order_status.status': { $ne: 'Cancelled' } }, { 'order_status.status': { $ne: 'Delivered' } }]
        }
    }
    if ((req.user.user_type === "admin" || req.user.user_type === "seller") && req.query.status && req.query.status != '' && req.query.status != undefined) {
        query = {
            ...query,
            ["current_status.status"]: req.query.status,
            $and: [{ "current_status.date": { $gte: firstDay } }, { "current_status.date": { $lte: lastDay } }]
        }
    }else{
        query = {
            ...query,
            $and: [{ "createdAt": { $gte: firstDay } }, { "createdAt": { $lte: lastDay } }]
        }
    }
    if (req.user.user_type === "admin" && req.query.seller_id && req.query.seller_id != '' && req.query.seller_id != undefined) {
        query = {
            ...query,
            ['seller_id']: ObjectId(req.query.seller_id)
        }
    }
    console.log('=-=-',firstDay,lastDay)
    order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: query
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'user_id'
            }
        },
        {
            $lookup: {
                localField: 'seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'seller_id'
            }
        },
        {
            $unwind: {
                path: '$seller_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$user_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$Products',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Products.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'inventory_id'
            }
        },
        {
            $lookup: {
                localField: 'Products.discount_on_product',
                foreignField: '_id',
                from: 'discounts',
                as: 'discount_on_product'
            }
        },
        { $sort: { createdAt: -1 } }
    ])
        .then((detail) => {

            async.eachOfSeries(detail, function (order, key, cb) {
                if (order.user_id) {
                    async.forEach(order.order_status, function (status_date, forcb) {
                        if (status_date.status === 'Delivered') {
                            deli_date = moment(status_date.date).format("DD/MM/YYYY");
                        } else {
                            forcb();
                        }
                    });
                    orderArr.push({
                        // sys_id: order._id,
                        id: order.order_id,
                        date: moment(order.createdAt).format("DD/MM/YYYY"),
                        seller_invoice: order.seller_invoice ? order.seller_invoice : '',
                        buyer: order.user_id.company_name,
                        seller: order.seller_id ? order.seller_id.company_name ? order.seller_id.company_name : '' : '',
                        // order_status: order.order_status[order.order_status.length - 1],
                        order_status: order.order_status[order.order_status.length - 1].status,
                        // payment_type: order.products.length,
                        delivery_date: deli_date ? deli_date : '',
                        amount: order.total_amount,
                        // delivery_charges: order.delivery_charges
                    })
                }
                cb()
            }, function (err) {
                var json2csvParser = new Parser({ fields: fields });
                const csv = json2csvParser.parse(orderArr);
                res.attachment('export.csv');
                res.status(200).send(csv);
            });
        })



}


// check paytyme responce and verify checksum
// var generateLabel = async (req, cb) => {
//     let order_id = req.query.order_id
//     let order_details = await order.aggregate([
//         {
//             $match: {
//                 'order_id': order_id
//             }
//         },
//         {
//             $lookup: {
//                 localField: 'user_id',
//                 foreignField: '_id',
//                 from: 'users',
//                 as: 'buyer'
//             }
//         },
//         {
//             $unwind: {
//                 path: '$buyer',
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $lookup: {
//                 localField: 'seller_id',
//                 foreignField: '_id',
//                 from: 'users',
//                 as: 'seller'
//             }
//         },
//         {
//             $unwind: {
//                 path: '$seller',
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//     ])
//     // (query);

//     //res.status(200).send("url");
//     var contents = fs.readFileSync('./views/shipping_label.ejs', 'utf8');
//     var OrderDetails = {
//         base_url: process.env.base_url,
//         order_id: req.query.order_id,
//         title: "Shubham",

//         buyerFName: order_details[0].buyer.first_name,
//         buyerLName: order_details[0].buyer.last_name,
//         buyerCity: order_details[0].buyer.city,
//         buyerPin: order_details[0].buyer.user_pincode,
//         buyerAddr: order_details[0].buyer.user_address,
//         buyerPhone: order_details[0].buyer.phone,

//         sellerFName: order_details[0].seller.first_name,
//         sellerLName: order_details[0].seller.last_name,
//         sellerCity: order_details[0].seller.city,
//         sellerPin: order_details[0].seller.user_pincode,
//         sellerAddr: order_details[0].seller.user_address,
//         sellerPhone: order_details[0].seller.phone,

//         paymentType: order_details[0].paymentType,
//         totalAmt: order_details[0].total_amount,
//         length: order_details[0].length,
//         breadth: order_details[0].breadth,
//         height: order_details[0].height,
//         invoiceNo: order_details[0].unique_invoice,
//         invoiceDate: order_details[0].createdAt,
//         awbNo: req.query.awb ? req.query.awb : "abcdefg123",
//         serviceName: req.query.serviceName

//     }
//     var html = ejs.render(contents, OrderDetails);
//     pdf.create(html).toBuffer(async function (err, buffer) {
//         let url = await helper.BufferUploadS3("order_labels/" + req.query.order_id + ".pdf", buffer);
//         cb(url);
//     });

// }
let dotZotDummy = (req, res) => {
    let orderData = {}
    let today = new Date()
    let date = today.toJSON().slice(0, 10);
    let formatedDate = date.slice(8, 10) + '/' + date.slice(5, 7) + '/' + date.slice(0, 4);
    let DocketList = [{
        AgentID: '',
        AwbNo: '',
        Breath: 1,
        CPD: formatedDate,
        CollectableAmount: "500",
        Consg_Number: 'AB123X001',
        Consolidate_EW: '12345',
        CustomerName: `Avanish`,
        Ewb_Number: '876867',
        HSN_code: "02314h03",
        GST_REG_STATUS: 'Y',
        Height: 5,
        Invoice_Ref: req.body.invoiceNumber,
        IsPudo: 'N',
        ItemName: 'Medicine',
        Mode: 'C',
        NoOfPieces: "1",
        OrderConformation: 'Y',
        OrderNo: req.body.orderId,
        length: '1',
        ProductCode: 'medicine',
        PudoId: '',
        REASON_TRANSPORT: '',
        RateCalculation: 'N',
        Seller_GSTIN: '12345856',
        ShippingAdd1: 'Affairies navi mumbai',
        ShippingAdd2: 'Affairies navi mumbai',
        ShippingCity: 'navi mumbai',
        ShippingEmailId: 'avanish.m@infiny.in',
        ShippingMobileNo: '9421693042',
        ShippingState: 'Maharashtra',
        ShippingTelephoneNo: '8452114488',
        ShippingZip: '400705',
        Shipping_GSTIN: 'H212hf33',
        TotalAmount: "500",
        TransDistance: '30',
        TransporterID: 'TID0123',
        TransporterName: '"DTDC Courier Cargo',
        TypeOfDelivery: 'Home Delivery',
        TypeOfService: 'Economy',
        UOM: 'Per KG',
        VendorAddress1: 'pantnagar',
        VendorAddress2: 'pamtnagar',
        VendorName: 'Infiny webcom',
        VendorPincode: '400077',
        VendorTeleNo: '8877445599',
        Weight: "0.12"
    }]
    orderData.Customer = { CUSTCD: 'CC000101634' };
    orderData.DocketList = DocketList
    let options = {
        'method': 'POST',
        'url': 'https://instacom.dotzot.in/restservice/pushorderdataservice.svc/pushorderdata_pudo_gst',
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(orderData)
    }
    request(options, function (error, response) {
        if (error) {
            res.status(200).json({
                title: error,
                error: true
            });
        };
        if (response.body[0].Succeed === 'No') {
            res.status(200).json({
                title: response.body[0].Reason,
                error: true
            });
        } else {
            req.query.orderId = req.body.orderId;
            req.query.awb = response.body[0].DockNo
            // generateLabel(req, function (url) {
            //     res.status(200).json({
            //         manifestUrl: url,
            //         dotzotRepomse: response
            //     })
            // })
        }
    })
}
const rollBackSingleOrder = async(ProcessedOrder) => {
    // inventory.updateInventoryQuantity(ProcessedOrder.products, 'cancel', async (response) => {
        let trans = await transactions.find({ unique_invoice: ProcessedOrder.unique_invoice }).then((result) => result);
        // let transSuccess = await transactions.find({ unique_invoice: ProcessedOrder.unique_invoice, type: 'Credit Used' }).then((result) => result);
        let balance = (ProcessedOrder.delivery_charges > 0) ?
            Number(Number(Number(ProcessedOrder.total_amount) + Number(ProcessedOrder.balance) + 50).toFixed(2))
            :
            Number(Number(Number(ProcessedOrder.total_amount) + Number(ProcessedOrder.balance)).toFixed(2));
        // && ProcessedOrder.paymentStatus != 'pending'
        if (balance > 0 && ProcessedOrder.paymentType === 'Online' && trans != '' ) {
            let buyerData = await user.findOneAndUpdate({ _id: ProcessedOrder.user_id }, { $inc: { wallet_balance: balance } }, { new: true }).then(result => result);
            let transaction = [];
            transaction.user_id = ProcessedOrder.user_id;
            transaction.settle_amount = balance;
            transaction.paid_amount = balance;
            transaction.unique_invoice = ProcessedOrder.unique_invoice;
            transaction.narration = "Amount credited for cancellation for order " + ProcessedOrder.order_id;
            transaction.type = 'Credit Received'
            let paytmPayAmount = 0;
            transaction.status = 'Success';
            transaction.closing_bal = buyerData.wallet_balance;
            transactions.addTransaction(transaction, (error, transactionData) => {

            })
        }
        // if(ProcessedOrder.paymentStatus == 'pending' && transSuccess != ""){
        //     let buyerData = await user.findOneAndUpdate({ _id: ProcessedOrder.user_id }, { $inc: { wallet_balance: transSuccess.settle_amount } }, { new: true }).then(result => result);
        //     let transaction = [];
        //     transaction.user_id = ProcessedOrder.user_id;
        //     transaction.settle_amount = balance;
        //     transaction.paid_amount = balance;
        //     transaction.unique_invoice = ProcessedOrder.unique_invoice;
        //     transaction.narration = "Amount credited for cancellation for order " + ProcessedOrder.order_id;
        //     transaction.type = 'Credit Received'
        //     let paytmPayAmount = 0;
        //     transaction.status = 'Success';
        //     transaction.closing_bal = buyerData.wallet_balance;
        //     transactions.addTransaction(transaction, (error, transactionData) => {

        //     })
        // }
    // });
}
const generatePdf = () => {
    var contents = fs.readFileSync('./views/shipping_label.ejs', 'utf8');
    let OrderDetails = {
        "origin": "BLR_Nagawara_CP (Karnataka)",
        "invoice_reference": null,
        "pin": 122001,
        "cl": "100bestbuy",
        "intl": null,
        "origin_state_code": null,
        "cd": "2016-07-05T13:04:37.583",
        "ewbn": null,
        "rph": [
            ""
        ],
        "snm": "BeeBay",
        "barcode": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOkAAAB6CAIAAADVp+NZAAAOuUlEQVR4nO2be0xT5xvH39oLUK7SwhAKzEvkIiAMBDEpQ5ybXMKcGyKKFzbsIFEnQnQLJlvMwuYFFS0sOCOQqeDcFBYGMoHJ1IBLBwwQgQAbyM0xKJeO0kpPf3+crDk5py31sp+VwBdwFc+R8JMaUW/pNUgAAAAABJRU5ErkJggg==",
        "origin_city": "Bangalore",
        "weight": 100,
        "pt": "COD",
        "rs": 692,
        "destination": "Gurgaon (Haryana)",
        "si": "",
        "destination_city": "Gurgaon",
        "hsn_code": "",
        "tin": "07510226832",
        "origin_state": null,
        "oid_barcode": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU8AAAA7CAIAAAB0a5/GAAAHQ0lEQVR4nO3Yb0hTbxsH88AAAAASUVORK5CYII=",
        "sid": "2016-05-03T00:00:005/3/2016 5:48:11 PM  05:30",
        "cst": "7838277455",
        "prd": "Beebay Surf Rider Shirt",
        "rcty": "Bangalore",
        "consignee_gst_tin": null,
        "cnph": "7838277455",
        "sadd": "A-38,1st Floor, Mayapuri Industrial Area Phase-2",
        "oid": "I060416NG5AZ_14_00000020160503",
        "customer_state": null,
        "radd": "Drive India Enterprise Solution, Survey No 159/1&160/1, Whitefield Hoskote Road, Kannamangla, Bangalore 560067",
        "customer_state_code": null,
        "address": "Old Sibi mandi",
        "rst": "Karnataka",
        "seller_gst_tin": null,
        "product_type": null,
        "name": "Kapil Mongia",
        "st_code": "HR",
        "cl_logo": "",
        "st": "Haryana",
        "client_gst_tin": null,
        "etc": "",
        "delhivery_logo": "https://staging-express.delhivery.com/static/images/new_logo.png",
        "contact": "9034341436",
        "cod": 692,
        "wbn": "5072510000011",
        "sort_code": "GGN/DPC",
        "rpin": 560067
    }
    var html = ejs.render(contents, OrderDetails);
    pdf.create(html).toBuffer(async function (err, buffer) {
        let url = await helper.BufferUploadS3("order_labels/" + 'avanish' + ".pdf", buffer);
        res.status(200).json({
            url
        })
    });
}
const generateOrderMail = async (req, cb) => {
    let order_id = req.query.orderId
    let order_details = await order.aggregate([
        {
            $match: {
                'order_id': order_id
            }
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'buyer'
            }
        },
        {
            $unwind: {
                path: '$buyer',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'seller'
            }
        },
        {
            $unwind: {
                path: '$seller',
                preserveNullAndEmptyArrays: true
            }
        }
    ])
    var OrderDetails = {
        base_url: process.env.base_url,
        order_id: req.query.orderId,
        title: "Shubham",
        orderTitle: 'thank you for your order!',
        orderTitle2: `We've received your order and will contact you as soon as your package is shipped. You can find your purchase
        information below.`,
        name: order_details[0].buyer.first_name,

        buyerFName: order_details[0].buyer.first_name,
        buyerLName: order_details[0].buyer.last_name,
        buyerCity: order_details[0].buyer.city,
        buyerPin: order_details[0].buyer.user_pincode,
        buyerAddr: order_details[0].buyer.user_address,
        buyerPhone: order_details[0].buyer.phone,
        buyerComp: order_details[0].buyer.company_name,

        sellerFName: order_details[0].seller.first_name,
        sellerLName: order_details[0].seller.last_name,
        sellerCity: order_details[0].seller.city,
        sellerPin: order_details[0].seller.user_pincode,
        sellerAddr: order_details[0].seller.user_address,
        sellerPhone: order_details[0].seller.phone,
        sellerComp: order_details[0].seller.company_name,

        totalAmt: order_details[0].total_amount + Number(order_details[0].delivery_charges),
        date: moment(order_details.createdAt).format("MMMM D, YYYY"),

        product: order_details[0].products

    }
    var filePath = './views/order_mail.ejs';
    var compiled = ejs.render(fs.readFileSync(filePath, 'utf8'), OrderDetails);
    // res.render('order_mail',OrderDetails)
    let mailData = {
        email: order_details[0].buyer.email,
        subject: 'Order Placed',
        body: compiled,
    };
    helper.sendEmail(mailData);

    var OrderDetails2 = {
        base_url: process.env.base_url,
        order_id: req.query.orderId,
        title: "Shubham",
        orderTitle: 'new order has been placed!',
        orderTitle2: `You've recieved new order. For futher details Please login. You can find the purchase
        information below.`,
        name: order_details[0].seller.first_name,

        buyerFName: order_details[0].buyer.first_name,
        buyerLName: order_details[0].buyer.last_name,
        buyerCity: order_details[0].buyer.city,
        buyerPin: order_details[0].buyer.user_pincode,
        buyerAddr: order_details[0].buyer.user_address,
        buyerPhone: order_details[0].buyer.phone,
        buyerComp: order_details[0].buyer.company_name,

        sellerFName: order_details[0].seller.first_name,
        sellerLName: order_details[0].seller.last_name,
        sellerCity: order_details[0].seller.city,
        sellerPin: order_details[0].seller.user_pincode,
        sellerAddr: order_details[0].seller.user_address,
        sellerPhone: order_details[0].seller.phone,
        sellerComp: order_details[0].seller.company_name,

        totalAmt: order_details[0].total_amount,
        date: moment(order_details.createdAt).format("MMMM D, YYYY"),

        product: order_details[0].products

    }
    var compiled2 = ejs.render(fs.readFileSync(filePath, 'utf8'), OrderDetails2);
    // res.render('order_mail',OrderDetails2)
    let mailData2 = {
        email: order_details[0].seller.email,
        subject: 'New Order',
        body: compiled2,
    };
    helper.sendEmail(mailData2);
    cb()
}
const dashboardSalesStatsAdmin = async (req, res) => {
    let date = new Date(`${req.body.year}-01-01`);
    //first date
    
    let newApr1 = new Date(moment(date).month("April").startOf('month').format('YYYY-MM-DD'));
    let newMar31 = new Date(moment(date).add('1','year').month("April").startOf('month').format('YYYY-MM-DD'));
    // "createdAt": { $gte: newApr1, $lt: newMar31 },
    let montlyRevenueOrders1 = await order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "is_deleted": false,
                $and:[{"createdAt": { $gte: newApr1 }},{"createdAt": { $lt: newMar31 }}
                ,{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: { $month: "$createdAt" },
                orders: { $sum: 1 },
                amount: { $sum: { $toDouble: "$total_amount" } }
            }
        },
        {
            $sort:{_id:1}
        }
    ]);
    let montlyRevenueOrders = [];

    for(const [month] of Array(12).entries()){
        const data = montlyRevenueOrders1.filter(v => Number(v._id) === month + 1)
        if(data[0]){
            montlyRevenueOrders.push({_id: month < 9 ? `0${month + 1}`: month + 1, orders: data[0].orders, amount: data[0].amount});
        }
        else{
            montlyRevenueOrders.push({_id: month < 9 ? `0${month + 1}`: month + 1, orders: 0, amount:0});
        }
    }

    res.status(200).json({
        title: 'Dashboard data Fetched successfully.',
        error: false,
        montlyRevenueOrders
    }) 
}

const changeOrder = async (req, res) => {
    try {
        let userData = req.user;
        if (req.body.amount != '' && req.body.type != '' && req.body.orderId != '' && req.body.otp != '') {
            systemActivityCon.isValid(req, async (data) => {
                if (data) {
                    let oldOrderData = await order.findOne({ _id: ObjectId(req.body.orderId) }).exec();
                    order.findOneAndUpdate({ _id: ObjectId(req.body.orderId) }, { $set: { paymentType: req.body.type, total_amount: req.body.amount, isBulk: req.body.isBulk } }, { new: true }).then(async (newData) => {
                        let trans = await transactions.findOne({ unique_invoice: newData.unique_invoice }).exec();
                        if (trans && newData.paymentType == 'Online' && Number(req.body.amount) < Number(oldOrderData.total_amount)) {
                            let balance = Number(Number(Number(oldOrderData.total_amount) - Number(req.body.amount)).toFixed(2));
                            let buyerData = await user.findOneAndUpdate({ _id: newData.user_id }, { $inc: { wallet_balance: balance } }, { new: true }).then(result => result);
                            let transaction = [];
                            transaction.user_id = newData.user_id;
                            transaction.settle_amount = balance;
                            transaction.paid_amount = balance;
                            transaction.unique_invoice = newData.unique_invoice;
                            transaction.narration = "Amount credited for Modification of order " + newData.order_id;
                            transaction.type = 'Credit Received'
                            transaction.status = 'Success';
                            transaction.closing_bal = buyerData.wallet_balance;
                            transactions.addTransaction(transaction, (error, transactionData) => {
                            })
                        }

                        if (!newData) {
                            return res.status(200).json({
                                error: true,
                                title: `Something went wrong.`
                            })
                        } else {
                            return res.status(200).json({
                                error: false,
                                title: 'Order updated successfully.',
                                detail: newData
                            })
                        }
                    })
                } else {
                    return res.status(200).json({
                        error: true,
                        title: 'Invalid OTP or OTP might be expired'
                    })
                }
            })
        } else {
            res.status(200).json({
                error: true,
                title: `Parameters can't be empty`
            })
        }

    } catch (err) {
        res.status(200).json({
            error: true,
            title: err
        })
    }
}

const checkInventory = (data) => {
    order.findOne({ unique_invoice: data }).then((result) => {
        if (result) {
            async.eachOfSeries(result.products, async (data, key, cb) => {
                if (data && data.inventory_id) {
                    await inventory.findById({ _id: data.inventory_id }).then(async (invenData) => {
                        if (invenData && invenData.quantity < invenData.min_order_quantity) {
                            await inventory.findByIdAndUpdate({ _id: invenData._id }, {
                                $set: {
                                    quantity: 0,
                                    status: 'Out of Stock'
                                }
                            }).exec();
                            cb()
                        }
                    })
                    cb();
                } else {
                    cb();
                }
            })
        }
    })
}
const delhiveryWebhook = async (req, res) => {
    res.status(200).json({
        error: false
    })
    let data = req.body
    if (data.Shipment && data.Shipment.AWB) {
        let value = await order.findOne({ shipment_id: data.Shipment.AWB }).then(result => result);
        if (value && value.requested !== 'Delivered') {
            if ((value.order_status[value.order_status.length - 1].status != data.Shipment.Status.Status) || (value.order_status[value.order_status.length - 1].description != data.Shipment.Status.Instructions)) {
                value.order_status.push({
                    status: data.Shipment.Status.Status,
                    description: `${data.Shipment.Status.Instructions}(${data.Shipment.Status.StatusLocation})`,
                    date: new Date(data.Shipment.Status.StatusDateTime)
                })
                try{
                    let status1 = data.Shipment.Status && data.Shipment.Status.Status && data.Shipment.Status.Status.toLowerCase().trim();
                    if ( status1.includes('delivered') ) {
                    value.requested = 'Delivered'
                    let date = data.Shipment.Status.StatusDateTime;
                    helper.insertSettlement(value._id, date);
                    }
                } catch(error) {
                    console.log('error in delhivery webhook',error)
                }
                value.save().then(result => result)
            }
        }
    }
}
const markDelivered = (req, res) => {
    let from_date = new Date(req.body.from);
    let to_date = new Date(req.body.to);
    from_date.setHours(0,0,0,0);
    to_date.setHours(23,59,59,999);  
    let matchDateQuery = { $and: [{ "order_status.date": { $gte: from_date } }, { "order_status.date": { $lte: to_date } },{"order_status.status":{$eq:'Delivered'}}] }
    order.aggregate([
        {
            $match: {
                seller_id: ObjectId(req.body.seller)
            }
        },
        { $match: matchDateQuery },
        {$project: {order_id:1}}
    ])
        .then(async(detail) => {
            let data1 =[]
            
            await detail.forEach(data=>{
                data1.push(data.order_id)
            })
            console.log('./././',data1)
            res.status(200).json({
                title: 'Order Fetched successfully.',
                error: false,
                data:detail
            })
        })
}

const orderNotification = (data, userData, orderIds) => {
    let req = { query: { orderId: orderIds } };
    generateOrderMail(req, function () {
        return true
    })
    let flag = 'Order Placed';
    let msg = `Order Confirmation - You've successfully placed ${orderIds}. We will update you once this is processed by our seller.`;
    notification.addNotification(msg, userData, '', flag, (error, response) => {
    })

    let flag1 = 'Order Received';
    let msg1 = `New Order: ${orderIds} received kindly process accordingly.`;
    notification.addNotification(msg1, '', data, flag1, (error, response) => {
    })

    let msgBody2 = `MEDIMNY Order Confirmation - You've successfully placed ${orderIds}. We will update you once this is processed by our seller.`;
    helper.sendSMS(userData.phone, msgBody2);
    var mailData2 = {
        email: 'noreply@medimny.com',
        subject: 'Order Placed',
        body:
            '<p>' +
            ` Order Confirmation - You've successfully placed ${orderIds}. We will update you once this is processed by our seller.`
    };
    helper.sendEmail(mailData2);
    if (userData && userData.device_token && userData.device_token.length > 0) {
        let devicetoken = userData.device_token;
        flag = 'Order Confirmation';
        msg = `Order Confirmation - You've successfully placed ${orderIds}. We will update you once this is processed by our seller.`;
        let title = 'Order Confirmation';
        let payload = {};

        helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
        });
    }
    if (data && data.seller_id) {
        user.findOne({ _id: ObjectId(data.seller_id) }).then((result) => {
            if (result && result.device_token && result.device_token.length > 0) {
                let devicetoken = result.device_token;
                flag = 'Order Received';
                msg = `New Order: ${orderIds} received kindly process accordingly.`;
                let title = 'Order Received';
                let payload = {};
                helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                });
            }
        })
    }
}
const sellerDashGraph = async (req, res) => {
    let start = moment().subtract(2, 'months').date(1).format('YYYY-MM-DD');
    let end = moment().endOf('month').format('YYYY-MM-DD');
    let startDate = new Date(start);
    let endDate = new Date(end);
    console.log('=-0=-0=-0',startDate,endDate)
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "seller_id": Id,
                "is_deleted": false,
                "createdAt": { $gte: startDate, $lte: endDate },
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
            }
        },
        {
            $group: {
                _id: { $substr: ['$createdAt', 5, 2] },
                orders: { $sum: 1 },
                amount: { $sum: { $toDouble: "$total_amount" } }
            }
        }
    ]).then((result) => {
        res.status(200).json({
            error: false,
            detail: result
        })
    })
}
const addBuyerSellerCompName = (req,res) => {
    let d = new Date();
    let last2month = new Date(d.getFullYear(), d.getMonth()-2,14);
    order.find({"createdAt": { $gte: last2month }}).populate('seller_id').populate('user_id').then((result) => {
        console.log('=-0=-0',result.length)
        async.eachOfSeries(result, function(orderD, key, cb){
            if (orderD.user_id && orderD.seller_id) {
               order.findOneAndUpdate({ _id: ObjectId(orderD._id) }, { $set: { sellerCompName: orderD.seller_id.company_name, buyerCompName: orderD.user_id.company_name } },{useFindAndModify:false}).exec();
            }
            cb()
        }, function (err) {
            res.status(200).json({
                title:'success',
                error:false
            });
        });
    })
}

const markOrderNew = async(req, res) => {
    let transaction = {}
    let orders = await order.find({ unique_invoice: req.query.uniqueIn }).then(result => result);
    let buyerData = await user.findOne({ _id: ObjectId(orders[0].user_id) }).then((result) => result);
    let orderids = orders.map(order => {
        return order.order_id;
    }).join(',');
    let trans = await transactions.findOne({ unique_invoice: req.query.uniqueIn, type: 'Credit Used' }).exec();
    let total = orders.reduce( function(acc, current) {
        return acc + current.total_amount + Number(current.delivery_charges)
    }, 0);
    let orderStatus = {
        status: 'New',
        description: `Order has been reinitiated by ${req.user.first_name}(Admin)`,
        date: new Date()
    }
    if(req.user.user_type !== 'admin'){
        return res.status(200).json({
            title:'Permission denied.',
            error:true
        });
    }
    if( orders[0].paymentStatus !== 'pending' ){
        return res.status(200).json({
            title:`Order is already ${orders[0].requested}.`,
            error:true
        });
    }
    if(req.query.status == 'new'){
        async.eachOfSeries(orders, function(orderD, key, cb){
            if (orderD) {
               order.findOneAndUpdate({ _id: ObjectId(orderD._id) }, { $set: { paymentStatus: 'success', requested: 'New' }, $push: { order_status: orderStatus } },{useFindAndModify:false}).exec();
            }
            cb()
        }, function (err) {

            transaction.settle_amount = 0;
            transaction.paid_amount = (trans && Object.keys(trans).length > 1) ? trans.paid_amount : total;
            transaction.unique_invoice = req.query.uniqueIn;
            transaction.narration = `Payment for Order: ${orderids}, transactionID: ${req.query.uniqueIn},  Accepted by admin (${req.user.email})`;
            transaction.status = 'Success';
            transaction.type = 'Online'
            transaction.user_id = buyerData._id;
            transaction.closing_bal = buyerData.wallet_balance;
            transactions.addTransaction(transaction, (error, transactionData) => {

            })
            let removeProd = [];
            orders.map(order => {
                if(order){
                    order.products.map(prod=>removeProd.push(prod))
                    orderNotification(order, buyerData, order.order_id); //---order noti
                }
            });
            cartModel.updateCart(buyerData._id,removeProd,'remove') //---Remove products from cart
            res.status(200).json({
                title:'Order marked as New.',
                error:false
            });
        });
    }else if(req.query.status == 'cancel'){
        helper.rollBackOrderByUniqueInvoice(req.query.uniqueIn)
        res.status(200).json({
            title:'Order marked as Cancelled.',
            error:false
        });
    }else{
        res.status(200).json({
            title:'Please send status',
            error:true
        });
    }
    
}

const checkOrder = (req, res) => {
    let firstDay = new Date(moment('2020-12-31'));
    let fields = ["orderId", "totalAmount", "createdAt"];
    let orderArr = []
    order.aggregate([{
        $match:{
            $and:[{ "createdAt":{$lte: firstDay}},{ "total_amount":{$lt: 5000} },{ "requested":{$eq: 'New'} }]
        }  
    },
    {
        $project: {
            "_id": 1,
            "order_id": 1,
            "total_amount":1,
            "createdAt":1
        }
    },
    ]).then(data=> {
        async.eachOfSeries(data, function (order, key, cb) {
            if (order) {
                orderArr.push({
                    orderId: order.order_id,
                    totalAmount: order.total_amount,
                    createdAt: moment(order.createdAt).format("DD/MM/YYYY")
                })
            }
            cb()
        }, function (err) {
            var json2csvParser = new Parser({ fields: fields });
            const csv = json2csvParser.parse(orderArr);
            fs.writeFile('orderData.csv', csv, err => {
            });
            res.attachment('orderData.csv');
            res.status(200).send(csv);
        });
    })
}
const changeOrder2 = (req, res) =>{
    let firstDay = new Date('2021-01-01')
    let lastDay = new Date('')
    let query={$and: [
            { "createdAt": { $gte: firstDay } },
            { "createdAt": { $lt: lastDay } }
        ]}
    order.find(query).then(async(detail)=>{
        console.log('detail', detail.length)
        async.eachOfSeries(detail, function(eachOrder,index2,mainCb) {
            if(eachOrder){
                async.forEach(eachOrder.products,async(eachProduct,cb)=>{
                    if(eachProduct){
                        let invfound = await inventory.findOne({_id: eachProduct.inventory_id}).populate('discount_id').exec();
                        if(invfound && invfound.discount_id && invfound.discount_id.name){
                            await order.findOneAndUpdate({ order_id: eachOrder.order_id,
                                products: { $elemMatch: { _id: ObjectId(eachProduct._id) }} },{ $set: { "products.$.discount_name" : invfound.discount_id.name }},{useFindAndModify:false}).exec(); 
                        }
                        cb();
                    }else{
                        cb()
                    }
                },async function(err){
                    mainCb()
                })
            }
        },function(){
            res.status(200).json({title: 'Success'})
        })
    })
}


const findCancelledOrders = async(req, res) => {
    let firstDay = new Date('2021-03-01')
    let lastDay = new Date()
    let orderArr = []
    let query = {
        paymentType:'Online',
        paymentStatus:'pending',
        $and: [{ $expr: {$gte: [{"$arrayElemAt": ["$order_status.date", -1]}, firstDay]}},
        {$expr: {$lt: [{"$arrayElemAt": ["$order_status.date", -1]}, lastDay ]}}]}
    await order.find(query).then(async(foundOrders) => {
        if(foundOrders){
             async.eachOfSeries(foundOrders, async(eachOrder,index,cb) =>{
                 if(eachOrder){
                    await transactions.findOne({type: {$eq:'Credit Used'}, unique_invoice: eachOrder.unique_invoice }).then(async(result) => {
                        if(result){
                            orderArr.push(eachOrder.order_id)
                        }
                    }).catch(err => console.log(err))
                }
                else{
                    cb()
                }
             },function(){
                 res.status(200).json({
                     title:"Success",
                     data: orderArr
                 })
             })
        }
    }).catch(err => console.log(err))
}

const getSellerPerformanceOrders = (req,res)=>{
    let firstDay = new Date(req.body.from_date)
    let lastDay = new Date(req.body.to_date)
    firstDay.setHours(firstDay.getHours()-5);
    firstDay.setMinutes(firstDay.getMinutes()-30);
    lastDay.setHours(lastDay.getHours()-5);
    lastDay.setMinutes(lastDay.getMinutes()-30);
    let query = {is_deleted:false}
    if(req.body.type === 'open'){
        query={...query, $and: [
            { "createdAt": { $gte: firstDay } },
            { "createdAt": { $lt: lastDay } },
            {'paymentStatus':{$ne:'pending'}}],
            $or:[
                {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'New'] }},
                {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Requested'] }}]
        }
    }
    else if(req.body.type === 'cancelled'){
        query={...query, 
            $and: [{ $expr: {$gte: [{"$arrayElemAt": ["$order_status.date", -1]}, firstDay]}},
            {$expr: {$lt: [{"$arrayElemAt": ["$order_status.date", -1]}, lastDay ]}},
            {$expr: { $eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Cancelled'] }},
            {"order_cancel_reason": { $not :{$regex: new RegExp('Admin'), $options: 'i' }}}
        ]}
    }else if(req.body.type === 'processed'){
        query={...query, 
            order_status:{
                $elemMatch: {
                    $and: [
                        {"status":"Processed"},
                        {
                            "date": {
                                $gte: firstDay,
                                $lt: lastDay
                            }
                        }
                    ]
                }
            }
        }
    }else{
        query={...query, $and: [
            { "createdAt": { $gte: firstDay } },
            { "createdAt": { $lt: lastDay } }
        ]}
    }
    if(req.body.seller_id){
        query = {...query, seller_id: ObjectId(req.body.seller_id)}
    }
    let query2 = {path:'user_id'}
    let limit = parseInt(100);
    let skip = (parseInt(req.body.page ? req.body.page : 1) - 1) * parseInt(limit);
    order.find(query).skip(skip).limit(limit).populate('seller_id').populate('cancelledBy').populate(query2).sort({'createdAt':-1}).then((detail)=>{
        order.countDocuments(query).then((count)=>{
            if(req.body.cancel && req.body.cancel == 'seller'){
                let filter = req.body.cancel == 'seller' && detail.filter(data => data.cancelledBy && data.cancelledBy.user_type == 'seller');
                user.findOne({_id:ObjectId(req.body.seller_id)}).then(foundSeller => {
                    let seller=foundSeller.company_name;
                    let detail = filter;
                    let count = filter.length;
                    let data ={ detail,count,seller}
                    res.status(200).json({
                     title:'success',
                     error: false,
                     data
                    })
                })
            }else{
            user.findOne({_id:ObjectId(req.body.seller_id)}).then(foundSeller => {
                let seller=foundSeller.company_name;
                let data ={ detail,count,seller}
                res.status(200).json({
                 title:'success',
                 error: false,
                 data
                })
            })
            }
        })
    })
}

const getBuyerSellerByRevenue = (req, res) => {
    if (req.user.user_type === 'admin') {
        let { id, user_type } = req.body;
        let query = [];
        let d = new Date();
        let monthN = d.getMonth() + 1;
        let year = d.getFullYear();

        query.push({
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" },
                current_status: { $arrayElemAt: ["$order_status", -1] }
            }
        })

        if (id && user_type == 'seller') {
            query.push({
                $match: {
                    "seller_id": ObjectId(id),
                    "is_deleted": false,
                    $and: [{ "current_status.status": { $ne: 'Cancelled' } }, { "current_status.status": { $ne: 'RTO' } }, { "paymentStatus": { $ne: 'pending' } }],
                    "year": req.body.year ? Number(req.body.year) : year,
                    "month": req.body.month ? getMonthNumber(req.body.month) : monthN
                }
            })
        }
        if (id && user_type == 'buyer') {
            query.push({
                $match: {
                    "user_id": ObjectId(id),
                    "is_deleted": false,
                    $and: [{ "current_status.status": { $ne: 'Cancelled' } }, { "current_status.status": { $ne: 'RTO' } }, { "paymentStatus": { $ne: 'pending' } }],
                    "year": req.body.year ? Number(req.body.year) : year,
                    "month": req.body.month ? getMonthNumber(req.body.month) : monthN
                }
            })
        }
        query.push({
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'user_id'
            }
        })
        query.push({
            $unwind: {
                path: '$user_id',
                preserveNullAndEmptyArrays: true
            }
        })
        query.push({
            $lookup: {
                localField: 'seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'seller_id'
            }
        })
        query.push({
            $unwind: {
                path: '$seller_id',
                preserveNullAndEmptyArrays: true
            }
        })
        if(user_type == 'seller'){
            query.push({
                $project: {
                    "_id": 1,
                    "user_id._id": 1,
                    "user_id.buyerId": 1,
                    "user_id.company_name": 1,
                    "total_amount": 1
                }
            })
            query.push({
                $group: {
                    _id: "$user_id.company_name",
                    total: { $sum: { $toDouble: "$total_amount" } }
                }
            })
        }
        if(user_type == 'buyer'){
            query.push({
                $project: {
                    "_id": 1,
                    "seller_id._id": 1,
                    "seller_id.sellerId": 1,
                    "seller_id.company_name": 1,
                    "total_amount": 1
                }
            })
            query.push({
                $group: {
                    _id: "$seller_id.company_name",
                    total: { $sum: { $toDouble: "$total_amount" } }
                }
            })
        }

        order.aggregate(query).sort({ total: -1 }).limit(10).then(data => {
            res.status(200).json({
                title: 'success',
                error: false,
                buyer: data
            })
        })
    } else {
        res.status(200).json({
            title: 'Please login as Admin.',
            error: true,
        })
    }

}
const markOrderAsEthical = (req, res) => {
    let lastMonth = new Date(moment().subtract(1, 'months').format('YYYY-MM-DD'));
    let today = new Date(moment().format('YYYY-MM-DD'));
    let query={$and: [
            { "createdAt": { $gte: lastMonth,  } },
            { "createdAt": { $lt: today } },
            { "orderType": { $eq: null }}
        ]}
    order.find(query).then((detail)=>{
        async.eachOfSeries(detail, function(eachOrder, key, cb) {
            console.log('detail=-=-=0', eachOrder.order_id, detail.length)
            if (eachOrder && (eachOrder.orderType == '' || eachOrder.orderType == undefined)) {
                console.log('detail=-=-=1', eachOrder.order_id)
                order.findOneAndUpdate({ order_id: eachOrder.order_id }, { $set: { orderType: 'Ethical branded' }}).exec();
            }
            cb()
        },function(err){
            res.status(200).json({title: 'Success'})
        })
    })
}

const orderSeqFix = (req, res) => {
    let thisMonth = new Date(moment().startOf('months').format('YYYY-MM-DD'));
    let today = new Date(moment().format('YYYY-MM-DD'));
    let query={$and: [
        { "createdAt": { $gte: thisMonth,  } },
        { "createdAt": { $lt: today } },
        { "requested": 'Delivered'},
        { "order_id": { $regex: new RegExp('^' + req.body.orderId), $options: 'i' }}
    ]}
    order.find(query).then((detail)=>{
    async.eachOfSeries(detail, function(eachOrder, key, cb) {
        if(eachOrder){
        console.log('detail=-=-=0', eachOrder.order_id, detail.length)
        let a = eachOrder.order_id;
        let b = a.substring(0, 2) + '20' + a.substring(2, a.length)
        console.log('detail=-=-=1', b)
        order.findOneAndUpdate({ order_id: eachOrder.order_id }, { $set: {  order_id: b }}).exec();
        }
        cb()
    },function(err){
        res.status(200).json({title: 'Success'})
    })
})
}

const updateInventoryInOrder = (products, type) => {
    if(products && products.length > 0){
        async.eachOfSeries(products, (eachProduct, key, cb) => {
        inventory.findById(eachProduct.inventory_id, (error, response) => {
            console.log('-=-=-=-11',response)
            if (error) {
                cb();
            } else {
                // (response.quantity && eachProduct.quantity) ? response.quantity = type === "create" ? (response.quantity - Number(eachProduct.quantity)) < 0 ? 0 : response.quantity - Number(eachProduct.quantity) : response.quantity + Number(eachProduct.quantity) : Number(eachProduct.quantity);
                if(response.quantity && eachProduct.quantity){
                    if(type === "create"){
                        if((response.quantity - Number(eachProduct.quantity)) < 0){
                            response.quantity = 0
                        }else{
                            response.quantity = response.quantity - Number(eachProduct.quantity)
                        }
                    }else{
                        response.quantity = response.quantity + Number(eachProduct.quantity)
                    }
                }else{
                    response.quantity = Number(eachProduct.quantity)
                }
                response.save(async(err, result) => {
                    console.log('-=-=-=-3',result)
                    if(response.quantity < response.min_order_quantity){
                        await ActiveInventory.deleteOne({inventory_id: ObjectId(response._id)}).catch(error => {
                            console.log(error);
                        })
                    }
                    invenCon.updateActiveInventory(eachProduct.inventory_id)
                    cb(error, result)
                    
                })
            }

        })
    }, (err) => {
    });
    }
}

const addCancelledBy = (req, res) => {
    let lastMonth = new Date(moment().subtract('1', 'month').format('YYYY-MM-DD'));
    let today = new Date(moment().format('YYYY-MM-DD'));
    let query = {
        $and: [
            { "createdAt": { $gte: lastMonth, } },
            { "createdAt": { $lt: today } },
            { "requested": 'Cancelled' }
        ]
    }
    order.find(query).populate('seller_id').then((detail) => {
        console.log('detail=detail',detail.length)
        async.eachOfSeries(detail, function (eachOrder, key, cb) {
            if (eachOrder && eachOrder.seller_id) {
                let status = eachOrder.order_status[eachOrder.order_status.length - 1];
                if (status.description.includes(eachOrder.seller_id.company_name)) {
                    console.log('detail=eachOrder',eachOrder.order_id,key+1)
                    // order.findOneAndUpdate({ _id: ObjectId(eachOrder._id) }, { $set: { cancelledBy: eachOrder.seller_id._id } }).exec();
                }
            }
            cb()
        }, function (err) {
            res.status(200).json({ title: 'Success' })
        })
    })
}

const getOrderedProducts = async (req, res) => {
    let fields = [];
    fields = ["No", "Product name"]
    let orderArr = []
    order.aggregate([
        {
            $match: {
                user_id: ObjectId('5fe311014e98b2658bb37d10')
            }
        },
        {
            $sort: { createdAt: -1 }
        }
    ]).then((detail) => {
        console.log('-=-=-=-=-=',detail.length)
        let i = 0;
        async.eachOfSeries(detail, function (order, key, cb) {
            if (order) {
                async.forEach(order.products, function (prod, forcb) {
                    if (prod) {
                        i++;
                        orderArr.push({
                            No: i,
                            'Product name': prod.productName,
                        })
                    } else {
                        cb();
                    }
                });
            }
            cb()
        }, function (err) {
            var json2csvParser = new Parser({ fields: fields });
            const csv = json2csvParser.parse(orderArr);
            res.attachment(`${detail[0].buyerCompName}.csv`);
            res.status(200).send(csv);
        });
    })
}
  
module.exports = {
    orderListing,
    ChangeStatus,
    getOrderDetails,
    createOrder,
    updateOrder,
    cancelOrder,
    processOrder,
    orderHistory,
    dashboardSalesStats,
    addCourierResponse,
    orderHistoryBuyers,
    orderListingBuyers,
    confirm,
    checksum,
    getReponse,
    exportCsv,
    dotZotDummy,
    adminProcessOrder,
    generatePdf,
    generateOrderMail,
    dashboardSalesStatsAdmin,
    allOrderListing,
    changeOrder,
    checkInventory,
    delhiveryWebhook,
    markDelivered,
    orderNotification,
    sellerDashGraph,
    addBuyerSellerCompName,
    markOrderNew,
    checkThreshold,
    checkOrder,
    changeOrder2,
    findCancelledOrders,
    getSellerPerformanceOrders,
    getBuyerSellerByRevenue,
    markOrderAsEthical,
    orderSeqFix,
    addCancelledBy,
    getOrderedProducts
}
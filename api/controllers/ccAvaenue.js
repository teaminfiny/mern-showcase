var http = require('http'),
	fs = require('fs'),
	ccav = require('../lib/ccavutil'),
	async = require('async'),
	qs = require('querystring');
var Order = require('../models/order')
var jwt = require('jsonwebtoken');
var cartModel = require('../models/cart');
const order = require('../models/order');
const inventory = require('../models/inventory')
const transaction = require('../models/transactions')
const User = require('../models/user')
const { Curl } = require('node-libcurl');
const { curly } = require('node-libcurl')
const path = require('path');
const helper = require('../lib/helper');
const orderCont = require('../controllers/order');
const ccavenueRes = require('../models/ccavenueResponse');
var mongoose = require('mongoose');
var axios = require('axios')
var ObjectId = mongoose.Types.ObjectId;
const request = require('request')

exports.postReq = function (req, res) {
	var body = '',
		workingKey = process.env.workingKey,		//Put in the 32-Bit key shared by CCAvenues.
		accessCode = process.env.accessCode,		//Put in the access code shared by CCAvenues.
		merchant_id = process.env.merchant_id,
		ccavenueURL = process.env.ccavenueURL,
		encRequest = '',
		formbody = '';
	if (req.method == "POST") {
		let params = req.body
		let queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
		queryString = encodeURI(queryString);
		encRequest = ccav.encrypt(queryString, workingKey);
		formbody = '<html><head><title>Sub-merchant checkout page</title><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script></head><body><center><!-- width required mininmum 482px --><iframe  width="482" height="100%" scrolling="No" frameborder="0"  id="paymentFrame" src="' + ccavenueURL + '&merchant_id=' + merchant_id + '&encRequest=' + encRequest + '&access_code=' + accessCode + '"></iframe></center><script type="text/javascript">$(document).ready(function(){$("iframe#paymentFrame").load(function() {window.addEventListener("message", function(e) {$("#paymentFrame").css("height",e.data["newHeight"]+"px"); }, false);}); });</script></body></html>';
		res.writeHeader(200, { "Content-Type": "text/html" });
		res.write(formbody);
		res.end();
	}
	return true;
};

exports.postRes = async function (request, response) {
	var ccavEncResponse = '',
		ccavResponse = '',
		workingKey = process.env.workingKey,	//Put in the 32-Bit key provided by CCAvenues.
		ccavPOST = '';
	//ccavEncResponse = request.body;
	//ccavPOST =  qs.parse(ccavEncResponse);
	//var encryption = ccavPOST.encResp;
	ccavResponse = ccav.decrypt(request.body.encResp, workingKey);
	updateOrder(request, response, ccavResponse)
	var pData = '';
	pData = '<table border=1 cellspacing=2 cellpadding=2><tr><td>'
	pData = pData + ccavResponse.replace(/=/gi, '</td><td>')
	pData = pData.replace(/&/gi, '</td></tr><tr><td>')
	pData = pData + '</td></tr></table>'
	htmlcode = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Response Handler</title></head><body><center><font size="4" color="blue"><b>Response Page</b></font><br>' + pData + '</center><br></body></html>';
	// 	response.writeHeader(200, {"Content-Type": "text/html"});
	// response.write(htmlcode);
	// response.end();
	let responseObject = JSON.parse('{"' + decodeURI(ccavResponse.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
	let orderData = await order.find({ unique_invoice: responseObject.order_id }).then((result) => result);
	let orderids = orderData.map(order => {
		return order.order_id;
	}).join(',');
	response.redirect(`${process.env.web_url}/orderStatus?status=${responseObject.order_status == 'Success' ? 'success' : 'failed'}&orderId=${orderids}`)
};
const updateOrder = async (request, response, ccavResponse) => {
	let responseObject = JSON.parse('{"' + decodeURI(ccavResponse.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
	let transactions = {};
	let orderData = await order.find({ unique_invoice: responseObject.order_id }).then((result) => result);
	let buyerData = await User.findOne({ _id: ObjectId(orderData[0].user_id) }).then((result) => result);
	let orderids = orderData.map(order => {
		return order.order_id;
	}).join(',');
	let newData = new ccavenueRes({
		unique_invoice: responseObject.order_id,
		gatewayResponce: responseObject
	})
	newData.save().then((result1) => result1);
	transactions.settle_amount = responseObject.amount;
	transactions.paid_amount = responseObject.amount;
	transactions.unique_invoice = responseObject.order_id;
	transactions.narration = `Payment for Order: ${orderids}, transactionID: ` + responseObject.order_id;
	let paytmPayAmount = 0;
	transactions.status = 'Success';
	order.findOne({ unique_invoice: responseObject.order_id, 'order_status.status': { $ne: 'Cancelled' } }).then(async (orderResult) => {
		if (orderResult) {
			if (responseObject.order_status === 'Aborted' || responseObject.order_status === 'Failure') {
				helper.rollBackOrderByUniqueInvoice(responseObject.order_id)
				let query = { $set: { rawTransaction: responseObject } }
				order.updateMany({ unique_invoice: responseObject.order_id }, query).then(result => result)
			} else if (responseObject.order_status == 'Success') {
				orderResult.paymentStatus = 'success';
				let query = { $set: { paymentStatus: 'success', rawTransaction: responseObject } }
				order.updateMany({ unique_invoice: responseObject.order_id }, query).then(result => result)
				transactions.type = 'Online'
				transactions.user_id = orderResult.user_id;
				transactions.closing_bal = buyerData.wallet_balance;
				transaction.addTransaction(transactions, (error, transactionData) => {

				})
				orderCont.checkInventory(responseObject.order_id);
				let removeProd = [];
				orderData.map(order => {
					if(order){
						order.products.map(prod=>removeProd.push(prod))
						orderCont.orderNotification(order, buyerData, order.order_id); //---order noti
					}
				});
				cartModel.updateCart(buyerData._id,removeProd,'remove') //---Remove products from cart
			}
		}
	})
}
exports.postMobileResponse = async (request, response) => {
	var ccavEncResponse = '',
		ccavResponse = '',
		workingKey = process.env.mobileWorkingKey,	//Put in the 32-Bit key provided by CCAvenues.
		ccavPOST = '';
	//ccavEncResponse = request.body;
	//ccavPOST =  qs.parse(ccavEncResponse);
	//var encryption = ccavPOST.encResp;
	ccavResponse = ccav.decrypt(request.body.encResp, workingKey);
	updateOrder(request, response, ccavResponse)
	let responseObject = JSON.parse('{"' + decodeURI(ccavResponse.replace(/&/g, "\",\"").replace(/=/g, "\":\"").replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25')) + '"}')
	let orderData = await order.find({ unique_invoice: responseObject.order_id }).then((result) => result);
	let orderids = orderData.map(order => {
		return order.order_id;
	}).join(',');
	let status = responseObject.order_status.toLowerCase() == 'success' ? 'success' : 'failed';
	response.render('mobilleview', { status: status, orderId: orderids })

}
exports.payOnline = async (req, res) => {
	var merchant_id = process.env.merchant_id
	let decoded = await jwt.verify(req.params.id, 'medideal$!@#$(*&^', function (err, decoded) {
		return decoded;
	});
	let api = await Order.findOne({ unique_invoice: decoded.unique_invoice }).populate('user_id').populate('seller_id').exec()
	res.render('dataForm', { name: 'avanish', api, merchant_id, redirctUrl: `${process.env.base_url}/ccavResponseHandler`, amount: decoded.balance })
}


exports.getRSA = async (req, res) => {
	const curl = new Curl();
	let url = process.env.rsa_url;
	let ccaInfo = path.resolve(__dirname, '../assets/cacert.pem')
	let params = req.body
	let queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
	queryString = encodeURI(queryString);
	const { data } = await curly.post(url, {
		sslVerifyPeer: false,
		caInfo: ccaInfo,
		postFields: queryString,
		httpHeader: [
			'Content-Type: application/json',
			'Accept: application/json'
		],
	})
	res.status(200).json({
		data,
		ccaInfo,
		queryString
	})
	curl.setOpt(Curl.option.URL, url);
	curl.setOpt(Curl.option.POST, 2)
	curl.setOpt(Curl.option.POSTFIELDS, queryString)
	curl.setOpt(Curl.option.SSL_VERIFYPEER, false)
	curl.setOpt(Curl.option.CAINFO, ccaInfo)
	curl.on('end', function (statusCode, data, headers) {
		console.info(statusCode);
		console.info('---');
		console.info(data.length);
		console.info('---'), data;
		res.status(200).json({
			statusCode, data
		})
		this.close();
	});
	curl.on('error', (error) => {
	});

}
exports.getStatus = async (req, res) => {
	var body = '',
		workingKey = process.env.workingKeyLive,		//Put in the 32-Bit key shared by CCAvenues.
		accessCode = process.env.accessCodeLive,		//Put in the access code shared by CCAvenues.
		merchant_id = process.env.merchant_id,
		ccavenueURL = 'https://secure.ccavenue.com/transaction/transaction.do?',
		encRequest = '',
		formbody = '';
	let params = "110100177020|uEONufviGVlh|"
	//{ order_no: 'uEONufviGVlh', reference_no:'110100177020' }
	// let queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
	// queryString = encodeURI(queryString);
	console.log('DTA=workingKey=1=', workingKey,accessCode)
	encRequest = ccav.encrypt(params, workingKey);
	let url = `${ccavenueURL}encRequest=${encRequest}&access_code=${accessCode}&command=orderStatusTracker&request_type=STRING&version=1.1`
	//&reference_no=109040210288&request_type=JSON&command=orderStatusTracker`
	try {
		// let data = await axios({
		// 	method: 'get',
		// 	url: url,

		// }).then((result) => result)
		// let data2 = await axios({
		// 	method: 'post',
		// 	url: url,

		// }).then((result) => result)
		var options = {
				'method': 'GET',
				'url': `${url}`,

		};
		console.log('options-=-=-',options)
		request(options, function (error, response) {
			console.log('DTA=res=1=', response)
			res.status(200).json({
				res:response,
				err:error,
				url
			})
		})
		var options1 = {
			'method': 'POST',
			'url': `${url}`,

		};
		// request(options1, function (error, response1) {
		// 	console.log('DTA=res=2=', response1)
			// res.status(200).json({
			// 	res:response1,
			// 	err:error,
			// 	url
			// })
		// })
		// console.log('DTA===1', data)
		// console.log('data2===2', data2)

	} catch (error) {
		res.status(200).json({
			error: true,
			error,
			url
		})
	}

}
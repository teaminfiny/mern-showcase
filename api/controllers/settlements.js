const Settlements = require('../models/settlement');
var Orders = require('../models/order');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const settlement = require('../models/settlement');
const systemCon  = require('../models/systemConfig');
const groupSettlement = require('../models/groupSettlement');
const async = require('async');
const multer = require('multer');
const csv = require('fast-csv');
const fs = require('fs');
const { Readable } = require("stream")
const moment = require('moment');
const helper = require('../lib/helper');
const order = require('../models/order');

const list = async(req,res) =>{
    let filter = [];
    let d = new Date();
    let firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
    let lastDay = new Date(d.getFullYear(), d.getMonth()+1);
    if(req.body.from_date && req.body.from_date != ''){
        firstDay = new Date(req.body.from_date)
    }
    if(req.body.to_date && req.body.to_date != ''){
        lastDay = new Date(req.body.to_date) 
    }
    // firstDay.setHours(0,0,0,0);
    // lastDay.setHours(23,59,59,999); 
    
    if(req.user.user_type==="admin"){
        if(req.body.seller_id !== '' && req.body.seller_id !== undefined){
            filter.push({$match:{"seller_id":ObjectId(req.body.seller_id)}})
        }
    }
    else{
        let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
        filter.push({$match:{"seller_id":Id,status:'Settled'}})
    }
    // mm/dd/yyyy
    // let firstDay = new Date(date.getFullYear(), date.getMonth(), 2);
    // let lastDay = new Date(date.getFullYear(), date.getMonth()+1, 1);
    // if(req.body.from_date){
    //     firstDay = new Date(req.body.from_date)
    // }
    // if(req.body.to_date){
    //     lastDay = new Date(req.body.to_date) 
    // }
    
    filter.push({
        $match:{$and:[{ "payment_due_date":{$gte: firstDay}},{ "payment_due_date":{$lt: lastDay} }] } 
    }); 
    filter.push({
        $lookup: {
            localField: 'order_id',
            foreignField: '_id',
            from: 'orders',
            as: 'order'
        }
    }); 
    filter.push({
        $unwind: {
            path: '$order',
            preserveNullAndEmptyArrays: true
        }
    });
    if(req.body.searchText !='' && req.body.searchText != undefined){
    filter.push({
        $match:{ "order.order_id":{ $regex: req.body.searchText, $options: 'i' } }
    }); 
    }
    filter.push({
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller'
        }
    });
    filter.push({
        $unwind: {
            path: '$seller',
            preserveNullAndEmptyArrays: true
        }
    });
    filter.push({
        $lookup: {
            localField: 'order.user_id',
            foreignField: '_id',
            from: 'users',
            as: 'buyer'
        }
    });
    filter.push({
        $unwind: {
            path: '$buyer',
            preserveNullAndEmptyArrays: true
        }
    });
    filter.push({
        $project: {
            "_id":1,
            "order_id": 1,
            "invoice_id": 1,
            "order_value": 1,
            "tds": 1,
            "tcs": 1,
            "commission": 1,
            "payment_due_date": 1,
            "payment_date": 1,
            "payment_due_date_ft": { $dateToString: { format: "%d-%m-%Y", date: "$payment_due_date" } },
            "status": 1,
            "remark": 1,
            "net_amt": 1,
            "seller._id": 1,
            "seller.company_name": 1,
            "order.order_id":1, 
            "transaction_id":1, 
            "order.createdAt": { $dateToString: { format: "%d-%m-%Y", date: "$order.createdAt" } },
            "order.order_status": 1,
            "order.user_id": 1,
            "buyer._id": 1,
            "buyer.company_name": 1,
            "logistic_settlement_date":1,
            "seller_settlement_date":1,
            "commission_comp": 1

        } 
    });
    filter.push({
        $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
            }
    });

    //apply date and seller filter  
    let settlements = await Settlements.aggregate(filter)
    res.status(200).json({
        error: false,
        settlements
    })
}

//This should be crons
const settle = async() =>{
    let d = new Date();
    d.setDate(d.getDate())
    let threeMonth = new Date(d.getFullYear(), d.getMonth()-1, 2);
    let orders = await Orders.aggregate([{
                $match:{
                    $and:[
                    {"order_status.status":"Delivered"},
                    {"settlement":"Not Eligible"},
                    {"order_status.date":{$gte:threeMonth}}
                    ]
                }  
            }])
    let tcsData = await systemCon.find({key:'tcs'}).exec();
    let tdsData = await systemCon.find({key:'tds'}).exec(); 
    let commissionData = await systemCon.find({key:'commission'}).exec();
    let bulkCommissionData = await systemCon.findOne({key:'bulk_commission'}).exec();
    console.log(orders.length);
    orders.forEach(async (order) => {
        console.log("orders  =====>"+order.order_id);
        //let newSettlement
        let issettled = await Settlements.findOne({order_id:order._id}).exec();
        if(!issettled){
            console.log("Insert Data!!!!!!"+order._id); 
            let ns = new Settlements({
                order_id: order._id,
                seller_id: order.seller_id,
                invoice_id: order.invoice_number,
                order_value: order.total_amount,
                tds: order.total_amount * Number(tdsData[0].value),
                tcs: order.total_amount * Number(tcsData[0].value),
                commission: order.isBulk ? order.total_amount * Number(bulkCommissionData.value) : order.total_amount * Number(commissionData[0].value),
                net_amt: order.total_amount - ((order.total_amount * Number(tdsData[0].value)) + (order.total_amount * Number(tcsData[0].value)) + (order.total_amount * Number(commissionData[0].value))),
                payment_due_date: order.order_status[order.order_status.length-1].date,
                transaction_id: ""
            }) 
            await ns.save();
            let query = { settlement: "Eligible" }
            await Orders.findOneAndUpdate({ _id: order._id }, query, { new: true }).exec((error, order) => {
                if (error) {
                }
                else {
                }
            });
        }
    }); 
    return true;

}

//only admin
const settleOrder = async(req,res) =>{
    let settlement_id = ObjectId(req.body.settlement_id)
    if(req.body.transaction_id){
        let query = { $set: 
            { 
                transaction_id:  req.body.transaction_id,
                remark: req.body.remark,
                payment_date: req.body.payment_date,
                status: "Settled",
                net_amt:req.body.net_amt,
                tds:req.body.tds,
                tcs:req.body.tcs,
                commission:req.body.commission
            } 
        } 
        Settlements.findOneAndUpdate({ _id: settlement_id }, query, { new: true }).exec((error,settlement)=>{
            if(error){
            }
            else{
                if(settlement){
                    res.status(200).json({
                        error: false,
                        msg: "orderSettled!"
                    })
                }
                else{
                    res.status(200).json({
                        error: true,
                        msg: "Invalid Id!"
                    })
                }
                
            }
        });
    }
    else{
        res.status(200).json({
            error: true,
            msg: "Kindly pass transaction_id"
        })
    }

    
}
const getOverallDetails = async(req,res)=>{
    //for pagination
    var perPage = req.body.perPage ? parseInt(req.body.perPage) : 50
    var page = req.body.page ? parseInt(req.body.page): 1
    var skipPage = (page - 1) * perPage;
    let query =[];
    query = [
        {
            $project: {
                _id: 1,
                _id: "$_id",
                "seller_id" : "$seller_id",
                "order_value" : "$order_value",
                "createdAt" : "$createdAt",
                "updatedAt" : "$updatedAt",
                "net_amt" : "$net_amt",
                status: {
                    $cond: [ {$eq:["$status",'Pending']}, 'false', 'true' ]
                } 
            }
        },
        { 
            $group: {
                _id: "$seller_id",
                total_amount: { $sum: "$order_value"  },
                "pending_amount": {
                    "$sum": { "$cond":[{ "$eq": ["$status", 'false'] }, "$order_value", 0] }
                },
                "settled_amount": {
                    "$sum": { "$cond":[{ "$eq": ["$status", 'true'] }, "$net_amt", 0] }
                }
            }
        },
        { 
            $project: {
                _id: 1,
                total_amount: { $round: [ '$total_amount', 2 ]},
                pending_amount: { $round: [ '$pending_amount', 2 ]},
                settled_amount: { $round: [ '$settled_amount', 2 ]}
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "_id",
                foreignField: "_id",
                as: "seller_id"
            }
        },        
        {
            $unwind: { path: "$seller_id", preserveNullAndEmptyArrays: true }
        }]

        if(req.body.searchText !='' && req.body.searchText != undefined){
            query.push({
                $match:{ "seller_id.company_name":{ $regex: req.body.searchText, $options: 'i' } }
            }); 
        }

        query.push({
            $unwind: { path: "$_id"}
        },
        {
            $sort: {'seller_id.company_name':1}
        },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }],
                totalCount: [
                    {
                        $count: 'count'
                    }
                ]
            }
        },
        { $unwind: "$totalCount" },
        { 
            $project: {
                data: 1,
                metadata: 1,
                totalCount: "$totalCount.count"
            }
        })

    let data = await Settlements.aggregate(query);
    res.status(200).json({
        error:false,
        data:data
    })
}
// Individual seller settlement list
const settlementList = async(req, res) => {
    let d = new Date();
    let firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
    let lastDay = new Date(d.getFullYear(), d.getMonth()+1);
    if(req.body.from_date && req.body.from_date !='' ){
        firstDay = new Date(req.body.from_date)
    }
    if(req.body.to_date && req.body.to_date != ''){
        lastDay = new Date(req.body.to_date) 
    }
    // firstDay.setHours(0,0,0,0);
    // lastDay.setHours(23,59,59,999);  
    var perPage = req.body.perPage ? parseInt(req.body.perPage) : 50
    var page = req.body.page ? parseInt(req.body.page): 1
    var skipPage = (page - 1) * perPage;
    let matchQuery = req.body.searchText ? { "order.order_id": { $regex: req.body.searchText, $options: 'i' }  } : {}
    let data = await Settlements.aggregate([
        {
            $match:{
                'seller_id':ObjectId(req.body.sellerId),
                "status":'Pending',
                $and: [{ 'payment_due_date': { $gte: firstDay } }, { 'payment_due_date': { $lt: lastDay } }]
            }
        },
        {
            $lookup:{
                from: "orders",
                localField: "order_id",
                foreignField: "_id",
                as: "order"
            }
        },
        {   $unwind: { path: "$order", preserveNullAndEmptyArrays: true } },
        {
            $match: matchQuery
        },
        {   $sort: { "order.order_id": 1 } },
        {
            $lookup:{
                from: "users",
                localField: "seller_id",
                foreignField: "_id",
                as: "seller"
            }
        },
        {   $unwind: { path: "$seller", preserveNullAndEmptyArrays: true } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: 500 }]
            }
        }
    ])
    res.status(200).json({
        error:false,
        data:data
    })
}
const getPendingSettlement = async(req,res)=>{
    let data = await Settlements.aggregate([
        {
            $match:{
                'seller_id':ObjectId(req.body.sellerId),
                "status":'Pending',
            }
        },
        {
            $lookup:{
                from: "users",
                localField: "seller_id",
                foreignField: "_id",
                as: "seller"
            }
        },
        {   $unwind: { path: "$seller", preserveNullAndEmptyArrays: true } },
        { 
            $group: {
                _id: "$seller_id",
                seller:{$first:"$seller.company_name"},
                total_amount: { $sum: "$order_value"  },
                // "pending_amount": {
                //     "$sum": { "$cond":[{ "$eq": ["$status", 'false'] }, "$order_value", 0] }
                // }
                }
        },
    ])
    res.status(200).json({
        error:false,
        data:data
    })
}
const editSettlement = async(req,res) =>{
    let today = new Date();
    let d = new Date();
    let { tds, tcs, commission, net_amt, orderVal } = req.body;
    if ( tds != '' &&  tcs != ''&&  commission != '' &&  net_amt != '' && orderVal ){
        Settlements.findByIdAndUpdate({_id: ObjectId(req.body.settlementId)},
        { $set:{ 
            tds:req.body.tds,
            tcs:req.body.tcs,
            commission: req.body.commission,
            net_amt:req.body.net_amt,
            order_value: orderVal
        }}, { 'new': true }).then((data) => {
            if(!data){
                res.status(200).json({
                    error:true,
                    title:'Something went wrong in edit settlement'
                })
            }else{
                res.status(200).json({
                    error:false,
                    title:'Settlement edited successfuly',
                    data:data
                })
            }
        })
    }else{
        res.status(200).json({
            error:true,
            title:`Value can't be empty`
        })
    }    
    
}
const groupSettle = async (req, res) => {
    try {
        req.checkBody('settlement', 'Settlement Id is required').notEmpty();
        req.checkBody('sellerId', 'Seller Id is required').notEmpty();
        req.checkBody('paymentDate', 'PaymentDate is required').notEmpty();
        req.checkBody('netAmt', 'Net amount is required').notEmpty();
        req.checkBody('orderValue', 'Order Value is required').notEmpty();
        req.checkBody('tcs', 'Tcs is required').notEmpty();
        req.checkBody('tds', 'Tds is required').notEmpty();
        req.checkBody('gst', 'Gst is required').notEmpty();
        req.checkBody('commission', 'Commission is required').notEmpty();
        req.checkBody('otherCharges', 'OtherCharges is required').notEmpty();
        req.checkBody('prodSurChargeWithGst', 'Product surcharge is required').notEmpty();
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: errors[0].msg,
            errors: errors
        })
    } else if (req.body.settlement && req.body.settlement != '' && req.body.settlement != undefined) {
            async.forEach(req.body.settlement, function (data, cb) {
                if (data) {
                    settlement.findByIdAndUpdate({ _id: data }, {
                        $set: {
                            status: 'Settled',
                            transaction_id: req.body.utrNo,
                            seller_settlement_date: req.body.paymentDate
                        }
                    }).exec();
                    cb()
                } else {
                    cb()
                }
            }, async function (err) {
                let newData = new groupSettlement({
                    seller_id: req.body.sellerId,
                    utrNo: req.body.utrNo,
                    payment_date: req.body.paymentDate,
                    remark: req.body.remark,
                    net_amt: req.body.netAmt,
                    order_value: req.body.orderValue,
                    tds: req.body.tds,
                    tcs: req.body.tcs,
                    gst: req.body.gst,
                    commission: req.body.commission,
                    other_charges: req.body.otherCharges,
                    early_remittance_charge: req.body.remittance,
                    remittanceId: req.body.customId,
                    gst_other_charges: req.body.otherChargesGst,
                    prod_surge_with_gst: req.body.prodSurChargeWithGst
                })
                newData.save();
                return res.status(200).json({
                    title: 'Settlement settled successfully.',
                    error: false
                });
            })
        }
    } catch(err) {
        console.log(err);
        res.status(200).json({
            error: true,
            title: 'Something went wrong in settlement'
        })
    }
}

const remittanceId = (req, res) => {
    helper.getRemittanceId(req.query.sellerId,(error, customId) => {
        if(error){
            res.status(200).json({
                error: true,
                title: 'Something went wrong in settlement'
            })
        }else{
            res.status(200).json({
                error: false,
                data: customId
            })
        }
    })
}

const settleByCsv = async (req, res) => {
    try {
        if (req.body.settleDate != '' && req.file.buffer != '') {
            let str = req.file.buffer.toString();
            let date = new Date(req.body.settleDate)
            let dataFile = await req.file.buffer
            let arr = []
            const readable = Readable.from([dataFile])
            readable.pipe(csv.parse())
                .on('data', (row) => {
                    arr.push(row)
                })
                .on('error', (err) => {
                })
                .on('end', () => {
                    console.log('CSV file successfully processed');
                    async.eachOfSeries(arr, function (settlement, key, mainCb) {
                        if (settlement) {
                            async.forEach(settlement, async (data, cb) => {
                                if (data.startsWith("OD")) {
                                    await Orders.findOne({ order_id: data }).exec(async (err, orderData) => {
                                        if (orderData) {
                                            await Settlements.findOneAndUpdate({ order_id: ObjectId(orderData._id) }, { $set: { logistic_settlement_date: date } }).exec();
                                        }
                                    });
                                }else{
                                    cb();
                                }
                            })
                        }
                        mainCb();
                    }, function (err) {
                        res.status(200).json({
                            error: false,
                            title: 'Settlement through CSV successful'
                        })
                    });
                });            
        } else {
            res.status(200).json({
                error: true,
                title: 'Something went wrong in Settlement.'
            })
        }
    } catch {
        res.status(200).json({
            error: true,
            title: 'In catch block, something went wrong in Settlement.'
        })
    }
}

const settleManually = async(req,res) =>{
    let d = new Date();
    d.setDate(d.getDate())
    let threeMonth = new Date(d.getFullYear(), d.getMonth()-1, 1);
    if (req.body.orderId.length > 0 && req.body.sellerId !== ''){
        let tcsData = await systemCon.find({ key: 'tcs' }).exec();
        let tdsData = await systemCon.find({ key: 'tds' }).exec();
        let commissionData = await systemCon.find({ key: 'commission' }).exec();
        let bulkCommissionData = await systemCon.findOne({ key: 'bulk_commission' }).exec();
        async.eachOfSeries(req.body.orderId,(data, key, mainCb) => {
            console.log('=-=-=--ord',data,key)
            if(data){
                Orders.findOne({ order_id: data }).then(async(orderVal) => {
                    let payment_date;
                    let alreadySettle = await settlement.find({ order_id: ObjectId(orderVal._id) }).exec();
                    console.log('=-=-=--alreadySettle',alreadySettle)
                    if(alreadySettle.length > 0){
                        mainCb();
                    }else{
                    async.forEach(orderVal.order_status, function (ordr, cb) {
                        payment_date = new Date();
                        if (ordr.status == 'Delivered') {
                            payment_date = ordr.date;
                            payment_date.setDate(payment_date.getDate());
                            cb();
                        } else {
                            cb();
                        }
                    });
                    console.log('=-=-=--payment_date',payment_date)
                    //let newSettlement
                    let ns = new Settlements({
                        order_id: orderVal._id,
                        seller_id: orderVal.seller_id,
                        invoice_id: orderVal.invoice_number,
                        order_value: orderVal.total_amount,
                        tds: orderVal.total_amount * Number(tdsData[0].value),
                        tcs: orderVal.total_amount * Number(tcsData[0].value),
                        commission: orderVal.isBulk ? orderVal.total_amount * Number(bulkCommissionData.value) : orderVal.total_amount * Number(commissionData[0].value),
                        net_amt: orderVal.total_amount - ((orderVal.total_amount * Number(tdsData[0].value)) + (orderVal.total_amount * Number(tcsData[0].value)) + (orderVal.total_amount * Number(commissionData[0].value))),
                        payment_due_date: payment_date,
                        transaction_id: ""
                    })
                    console.log('=-=-=--ns',ns)
                    ns.save();
                    mainCb()
                    }
                })
            }
        }, function (err) {
            console.log('=-=-=--nshere')
            res.status(200).json({
                error: false,
                title: 'success'
            })
        })
    }else{
        res.status(200).json({
            error: true,
            title: 'failed'
        })
    }
    

}
const settleIndividualOrder = async(req,res) => {
    let alreadySettle = await settlement.findOne({ order_id: ObjectId(req.query.id) }).exec();
    if(alreadySettle){
        res.status(200).json({
            error: true,
            title: 'Order already settled.'
        })
    }else{
        let tcsData = await systemCon.findOne({ key: 'tcs' }).exec();
        let tdsData = await systemCon.findOne({ key: 'tds' }).exec();
        Orders.findOne({ _id: ObjectId(req.query.id) }).populate('seller_id').then(async (order) => {
            //let newSettlement
          helper.getCommission(order, (data) => {
            let { user_comm, medi_type_comm } = data;
            let totalComm = user_comm + medi_type_comm;

            let payment_date = new Date(); 
            let ns = new Settlements({
                order_id: order._id,
                seller_id: order.seller_id._id,
                invoice_id: order.invoice_number,
                order_value: order.total_amount,
                tds: Number((order.total_amount * Number(tdsData.value)).toFixed(2)),
                tcs: Number((order.total_amount * Number(tcsData.value)).toFixed(2)),
                commission: totalComm,
                net_amt: order.total_amount - ((order.total_amount * Number(tdsData.value)) + (order.total_amount * Number(tcsData.value)) + totalComm + data.prod_surge_comm),
                payment_due_date: payment_date,
                transaction_id: "",
                commission_comp: data
            })
        
            ns.save();
            let query = { settlement: "Eligible" }
            Orders.findOneAndUpdate({ _id: ObjectId(order._id) }, query, { new: true }).exec();

            res.status(200).json({
                error: false,
                title: 'Order settled successfully.'
            })
          })
        })
    }
}

const changeInSettlement = async (req, res) => {
    if(req.body.prodSurCharge === '1'){
    let fourteenJuly = new Date(moment().format('2021-07-14')).getTime();
    let created;

    Settlements.find({ 'commission_comp.prod_surge_comm': { $gt: 0 }, status:'Pending' }).then(settle => {
        async.eachOfSeries(settle, (settleData, key, mainCb) => {
            if (settleData) {
                Settlements.findOne({ _id: ObjectId(settleData._id) }).populate('order_id').then(async (settlementData) => {
                    //let newSettlement
                    created = new Date(moment(settlementData.order_id.createdAt)).getTime();
                    console.log('=-=-=-=-=-settleData', settleData._id,created < fourteenJuly, settlementData.order_id.createdAt, settle.length)
                    if (settlementData && settlementData.commission_comp && settlementData.commission_comp.prod_surge_comm && created < fourteenJuly) {
                        let value = settlementData.commission_comp.prod_surge_comm;
                        let totalComm = settlementData.net_amt + Number(Number(value).toFixed(2));
                        settlementData.net_amt = totalComm;
                        settlementData.commission_comp.prod_surge_comm = 0;
                        settlementData.save();
                    }
                })
            }
            mainCb()
        }, function (err) {
            res.status(200).json({
                error: false,
                title: 'success'
            })
        })
    })
    }else {
    let query = {};
    let firstDay = new Date(moment().format('2021-07-05'));
    let lastDay = new Date(moment().format('2021-07-17'));
    query = {
        ...query,
        $and: [
            { $expr: { $gte: [{ "$arrayElemAt": ["$order_status.date", -1] }, firstDay] } },
            { $expr: { $lte: [{ "$arrayElemAt": ["$order_status.date", -1] }, lastDay] } },
            { 'requested': { $eq: 'Delivered' } }
        ]
    }
    // { 'seller_id': ObjectId('5e398445f2cb4b62c468aaa4') }
    let order = await Orders.find(query).exec();

    let tcsData = await systemCon.findOne({ key: 'tcs' }).exec();
    let tdsData = await systemCon.findOne({ key: 'tds' }).exec();
    async.eachOfSeries(order, (orderData, key, mainCb) => {
        if (orderData) {
            Settlements.findOne({ order_id: ObjectId(orderData._id) }).then(async (settlementData) => {
                //let newSettlement
                helper.getCommission(orderData, (data) => {
                    let { user_comm, medi_type_comm } = data;
                    let totalComm = user_comm + medi_type_comm;

                    settlementData.commission = totalComm,
                    settlementData.net_amt = orderData.total_amount - ((orderData.total_amount * Number(tdsData.value)) + (orderData.total_amount * Number(tcsData.value)) + totalComm + data.prod_surge_comm),
                    settlementData.commission_comp = data

                    // settlementData.save();
                })
                mainCb()
            })
        }
    }, function (err) {
        res.status(200).json({
            error: false,
            title: 'success'
        })
    })
}
}

const removeDuplicates = (req, res) => {
    let day = new Date(moment().format('2021-06-01'));
    // ,status:'Pending'
    settlement.find({ createdAt: {$gte: day}}).then( settle => {
        async.eachOfSeries(settle, function(data, key, cb){
            if(data){
                settlement.find({ order_id: ObjectId(data.order_id) }).then(found => {
                if(found.length > 1){
                    found.map( (del,index) => {
                        //  && index > 0
                        if(del.status === 'Pending'){
                            // settlement.deleteOne({ _id : ObjectId(del._id) }).exec();
                            // order.findOne({_id:ObjectId(del.order_id)}).exec((err,orderD)=>{
                            //     console.log('=-=-=-=-=-=-orderD', orderD.order_id);
                            // })
                        }
                    })
                }
                })
            }
            cb();
        }, function (err){
            res.status(200).json({
                error: false,
                title: 'success'
            })
        })
    })
}

module.exports = {
    list,
    settleOrder,
    settle,
    getOverallDetails,
    settlementList,
    getPendingSettlement,
    editSettlement,
    groupSettle,
    settleByCsv,
    settleManually,
    remittanceId,
    settleIndividualOrder,
    changeInSettlement,
    removeDuplicates
}
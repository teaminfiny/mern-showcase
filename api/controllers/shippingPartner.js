
const async = require('async');
const moment = require('moment');
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const shippingPartner = require('../models/shipping_partner');


const listShippingPartner = (req, res) => {
  shippingPartner.find().then( data => {
    if (data) {
      res.status(200).json({
        title: 'success',
        error: false,
        data: data
      })
    } else {
      res.status(200).json({
        title: 'Something went wrong in listing shipping partners.',
        error: true,
      })
    }
  })
}

const addShippingPartner = (req, res) => {
  let ship = new shippingPartner({
    name: req.body.partner
  })
  ship.save().then(data => {
    res.status(200).json({
      title: 'success',
      error: false,
      data: data
    })
  })
}

module.exports = {
  listShippingPartner,
  addShippingPartner
}
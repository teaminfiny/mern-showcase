const moment = require('moment');
var mongoose = require('mongoose');
var medicine = require('../models/medicine_type')
const user = require('../models/user');
var ObjectId = mongoose.Types.ObjectId;
const helper = require('../lib/helper');
const async = require('async');

const addMedCategory = (req, res) => {
  medicine.addCategory(req,res);
}

const listMedCategory = (req, res) => {
  medicine.listMedCategory(req,res);
}

module.exports = {
  addMedCategory,
  listMedCategory
}
const async = require('async');
var cartModel = require('../models/cart');
var inventory = require('../models/inventory');
var discount = require('../models/discounts');
const helper = require('../lib/helper');
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const ShortBook = require('../models/shortBook');
/*
# parameters: token,
# purpose: listing of all order from cart
*/
const cartDetails = (req, res) => {
    let userData = req.user;
    let cartData = [];
    let newCart = [];
    var CurrentDate = new Date();
    CurrentDate.setMonth(CurrentDate.getMonth() + 3);
    let cartDetails = cartModel.aggregate([
        {
            $match: {
                user_id: userData._id
            }
        },
        { $unwind: "$cart_details" },
        {
            $lookup: {
                localField: 'cart_details.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'Inventory'
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: 'Inventory.discount_id',
                foreignField: '_id',
                from: 'discounts',
                as: 'Discount'
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Inventory.user_id',
                foreignField: '_id',
                from: 'users',
                as: 'Seller'
            }
        },
        {
            $unwind: {
                path: '$Seller',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Inventory.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'Inventory.product_id'
            }
        },
        { $unwind: "$Inventory.product_id" },
        {
            $lookup: {
                localField: 'Inventory.product_id.GST',
                foreignField: '_id',
                from: 'gsts',
                as: 'Inventory.product_id.gst'
            }
        },
        { $unwind: "$Inventory.product_id.gst" },
        {
            $lookup: {
                localField: 'Inventory.product_id.company_id',
                foreignField: '_id',
                from: 'companies',
                as: 'Inventory.product_id.company_id'
            }
        },
        { $unwind: "$Inventory.product_id.company_id" },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'Discount.discount_on_product.inventory_id'
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'DiscountProduct'
            }
        },
    ]).exec((error, cartDatas) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong, Please try again..',
                error: true,
            });
        }
        async.forEach( cartDatas ,function(cartD,cb){
            if( cartD.Seller.vaccation.vaccationMode == false && cartD.Inventory.isHidden == false && cartD.Inventory.isDeleted == false && cartD.Seller.user_status == 'active' && (new Date(cartD.Inventory.expiry_date) > CurrentDate ) && cartD.Inventory.quantity >= cartD.Inventory.min_order_quantity ){
                cartData.push(cartD);
                newCart.push(cartD.cart_details);
                cb();
            }else{
                
                cb()
            }
        },async function(err){
            cartModel.updateOne(
                { user_id: userData._id },
                {
                    cart_details:newCart
                },(err,newcart)=>{
                    if(err){
                    }
                    else{
                        return res.status(200).json({
                            title: 'Cart Details',
                            error: false,
                            detail: cartData
                        });
                    }
                });
        })
    })
}

/*
# parameters: token,
# purpose: orders add to Cart
*/
const addtoCart =  (req, res) => {
    let userData = req.user;
    let cart_details = req.body.cart_details;
    let products = []
    if (cart_details.length>0){
        cartModel.findOne({ user_id: userData._id }).exec(async(error, data) => {
            let cartDat = data ? data : new cartModel({ user_id: userData._id, cart_details: [], other_details: req.body.other_details})
            var remainder = 0;
            async.eachOfSeries(cart_details,async(cart_detail,key,cb)=>{
                let quantity = cart_detail.quantity;
               await inventory.findOne({ _id: cart_detail.inventory_id }).then(async(inventoryDetails) => {
                    if (inventoryDetails) {
                        products.push(ObjectId(inventoryDetails.product_id))
                        if (inventoryDetails.discount_id) {
                           await discount.findOne({ _id: inventoryDetails.discount_id }).then((discount) => {
                                if (discount) {
                                    if(discount.name == 'Discount'){
                                        cart_detail.discount_per = discount.discount_per
                                    }else if( discount.name == 'Same' || discount.name == 'SameAndDiscount' ||
                                    discount.name == 'Different' || discount.name == 'DifferentAndDiscount'){
                                        remainder = quantity % discount.discount_on_product.purchase;
                                        var temp = quantity / discount.discount_on_product.purchase;
                                        temp = Math.floor(temp);
                                        temp = temp * discount.discount_on_product.bonus;
                                        let discount_on_product = {
                                            quantity: temp,
                                            inventory_id: discount.discount_on_product.inventory_id
                                        }
                                        cart_detail['discount_on_product'] = discount_on_product;
                                    }
                                }

                            })
                        }
                        let index = cartDat.cart_details.findIndex((e) => e.inventory_id == cart_detail.inventory_id);
                        if (index > -1) {
                            cartDat.cart_details[index].quantity = cart_detail.quantity;
                        } else {
                            cartDat.cart_details.push(cart_detail);
                        }
                        cb()
                    }else{
                        cb()
                    }
                })  
            },function(err){
                if(remainder > 0){
                    return res.status(200).json({
                        title: 'Please add in multiple of min order quantity',
                        error: true,
                    });
                }else{
                    cartDat.save(async(error, newCartData) => {
                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong, Please try again..',
                                error: true,
                            });
                        }
                        await ShortBook.updateMany({user_id: ObjectId(userData._id),product_id:{$in:products}, isDeleted: false, isFulfilled: false}, {$set:{isFulfilled:true, inStock:true}}).exec();
                        return res.status(200).json({
                            title: 'Product Added Into Cart Successfully.',
                            error: false,
                            detail: newCartData
                        });
                    })
                }
            })

            
        })
        
    }else{
        return res.status(200).json({
            title: 'Invalid parameters',
            error: true,
        });
    }
       
}

/*
# parameters: token,
# purpose: order remove from cart
*/
const removeFromCart = async (req, res) => {
    let userData = req.user;
    let newArry = [];
    cartModel.findOne({ user_id: userData._id }).exec((error, cartDat) => {
        if (cartDat) {
            let index = cartDat.cart_details.findIndex((e) => e.inventory_id == req.body.inventory_id)
            if (index > -1) {
                cartDat.cart_details.splice(index, 1);
                newArry = cartDat.cart_details;
            } else {
                return res.status(200).json({
                    title: 'User cart product not found',
                    error: true,
                });
            }
            cartModel.updateOne({ user_id: userData._id },{cart_details:newArry},(error, newCartData) => {
                if (error) {
                    return res.status(200).json({
                        title: 'Something went wrong, Please try again..',
                        error: true,
                    });
                }
                return res.status(200).json({
                    title: 'Cart product remove successfully.',
                    error: false,
                    detail: newCartData
                });
            })
        }
        else {
            if (error) {
                return res.status(200).json({
                    title: 'User cart product not found',
                    error: true,
                });
            }
        }
    })

}

/*
# parameters: token,
# purpose: Remove all card items along with user data
*/
const removeAll = async(req, res) => {
    let userData = req.user;
    if (req.body.inventory && req.body.inventory != '' && req.body.inventory != undefined) {
        let newArry = [];
        await cartModel.findOne({ user_id: ObjectId(userData._id) }).exec((error, cartDat) => {
            if(cartDat){
                async.forEach(req.body.inventory, function (data, cb) {
                    let index = cartDat.cart_details.findIndex((e) => e.inventory_id == data)
                    if (index > -1) {
                        cartDat.cart_details.splice(index, 1);
                        newArry = cartDat.cart_details;
                        cb()
                    } else {
                        cb()
                    }
                }, async function (err) {
                    cartModel.findOneAndUpdate({ user_id: ObjectId(userData._id) },{ $set:{ cart_details: newArry }},{ new : true }).then((newCartData) => {
                        if (!newCartData) {
                            return res.status(200).json({
                                title: 'Something went wrong, Please try again..',
                                error: true,
                            });
                        }
                        return res.status(200).json({
                            title: 'Cart product remove successfully.',
                            error: false,
                            detail: newCartData
                        });
                    })
                })
            }
        })
    } else {
        cartModel.remove({ user_id: userData._id }, function (err) {
            if (err) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again..',
                    error: true,
                });
            }
            else {
                return res.status(200).json({
                    title: 'Cart product remove successfully.',
                    error: false
                });
            }
        });
    }
}



module.exports = {
    cartDetails,
    addtoCart,
    removeFromCart,
    removeAll
}
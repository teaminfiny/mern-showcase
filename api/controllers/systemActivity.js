const systemActivity  = require('../models/systemActivity');
const helper = require('../lib/helper');
const async = require('async');
const mongoose = require('mongoose');
const order = require('../models/order');
var ObjectId = mongoose.Types.ObjectId;

const addActivity = async (req, res) => {
  let userData = req.user;
  let { type, action, counter, typeId } = req.body;
  // let oldOtp = await systemActivity.find({ user_id: ObjectId(userData._id) , isUsed: false }).exec();
  if (type != '' && action != '' && userData.user_type == 'admin' && counter <= 3) {
    let data = (type == 'Transaction') ? {
      type: type,
      action: action,
      user_id: userData._id,
    } : {
      type: type,
      action: action,
      user_id: userData._id,
      type_id: typeId
    }
    let newActivity = new systemActivity(data);
    newActivity.save().then((savedData) => {
      if (!savedData) {
        res.status(200).json({
          error: true,
          title: 'Something went wrong.'
        })
      } else {
        helper.getOtp(async(err,data)=>{
          systemActivity.findOneAndUpdate({_id: savedData._id},{$set:{otp:data}}).exec();
          let msgBody = `Your MEDIMNY OTP is ${data} and is valid for 10min.`;
          
          emailBody = '<p>' +
          `Your MEDIMNY OTP is ${data} and is valid for 10min.<br/>
          For: ${type}, Action: ${action}`

          if(type=="Order"){
            oid = await order.findById(typeId).exec();
            emailBody = emailBody + " for order id: "+ oid.order_id;
            msgBody = `Your MEDIMNY OTP is ${data} and is valid for 10min.`;
          }
          var mailData = {
            email: userData.email,
            // email: 'sameer.m@infiny.in',
            subject: 'OTP generated',
            body:emailBody
          };
          helper.sendSMS(userData.phone, msgBody);
          helper.sendEmail(mailData);
          
          res.status(200).json({
          error: false,
          title: 'OTP sent successfully'
          }); 
        setTimeout(() => timeOutFunction(savedData._id), 600000)
      });
      }
    });
  } else if (counter > 3) {
    var mailData = {
      email: 'support@medimny.com',
      subject: 'Suspicious Activity',
      body:
        '<p>' +
        `${userData.first_name} had entered wrong OTP thrice, his/her email is ${userData.email}`
    };
    helper.sendEmail(mailData);
    res.status(200).json({
      error: true,
      title: 'Please wait for 10min before generating new OTP'
    })
  } else {
    res.status(200).json({
      error: true,
      title: 'Please wait for 10min before generating new OTP'
    })
  }

}
const timeOutFunction = (data) => {
  systemActivity.findOneAndUpdate({_id: data },{$set:{isUsed:true}}).exec();
}

const isValid = async(req,cb) =>{
 await systemActivity.findOne({ user_id: req.user._id, isUsed: false }).sort({ createdAt: -1 }).then((result)=>{
   if(result && req.body.otp !='' && result.otp == req.body.otp){
      systemActivity.findOneAndUpdate({_id: result._id },{$set:{isUsed:true}}).exec();
      cb(result)
   }else{
     cb(false)
   }
 });
}

const addActivityAdminLogin = async(req, res) => {
  let userData = req.user;
  let { type, action, typeId } = req.body;
  // let oldOtp = await systemActivity.find({ user_id: ObjectId(userData._id) , isUsed: false }).exec();
  if (type != '' && action != '' && userData.user_type == 'admin' ) {
    let data = {
      type: type,
      action: action,
      user_id: userData._id,
      type_id: typeId
    }
    let newActivity = new systemActivity(data);
    newActivity.save().then((savedData) => {
      if (!savedData) {
        res.status(200).json({
          error: true,
          title: 'Something went wrong.'
        })
      } else {
        helper.getOtp((err,data)=>{
          systemActivity.findOneAndUpdate({_id: savedData._id},{$set:{otp:data}}).exec();

          let msgBody = `Your MEDIMNY OTP is ${data} and is valid for 10min.`;
          helper.sendSMS(userData.phone, msgBody);
          var mailData = {
            email: userData.email, // 'sameer.m@infiny.in'
            subject: 'OTP generated',
            body:
                '<p>' +
                `Your MEDIMNY OTP is ${data} and is valid for 10min.`
          };
          helper.sendEmail(mailData);
          
          res.status(200).json({
          error: false,
          title: 'OTP sent successfully',
          otp: data
          });
      });
      }
    });
  } else {
    res.status(200).json({
      error: true,
      title: 'Please wait for 10min before generating new OTP'
    })
  }
}

module.exports = {
  addActivity,
  isValid,
  addActivityAdminLogin
}
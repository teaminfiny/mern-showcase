var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

const user = require('../models/user');
const helper = require('../lib/helper');
var inventory = require('../models/inventory');
var Order = require('../models/order')
const moment = require('moment');

/*
# parameters: token,
# purpose: Buyer can request for bulk product quantity to admin with discount.
# Email will trigger for admin with all product details along with qty,ptr, dicount and user details.
*/
const bulkRequest = (req, res) => {
    let userData = req.user;
    inventory.findOne({ _id: req.body.inventory_id })
    .populate('product_id')
    .exec((error, inventoryDetails) => {
        if (inventoryDetails) {
            user.findOne({ user_type: 'admin' }).then((adminDetails) => {
                let mailData = {
                    email: "buybulk@medimny.com,medimnybb@gmail.com",
                    cc: userData.email,
                    subject: 'Bulk Request by: '+userData.company_name,
                    body:
                        '<p>Buyer Name: ' + userData.company_name+ '<br>'+
                        'Product Name: ' + inventoryDetails.product_id.name + '<br>' +
                        'PTR: ' + req.body.ptr + '<br>' +
                        'Quantity: ' + req.body.qty + ' <br>' +
                        'Discount: ' + req.body.discount
                        
                };
                helper.sendEmail(mailData);
                return res.status(200).json({
                    title: 'Bulk product request sent successfully.',
                    error: false,
                });
                
            })
        } else {
            return res.status(200).json({
                title: 'Product not found',
                error: true,
            });
        }
    })
}

/*
# parameters: token,
# purpose: listing of all Buyer
*/
const listingBuyer = (req, res) => {
    let user = req.user;
    
}

/*
# parameters: token,
# purpose: Get buyer details by id
*/
const getBuyerDetails = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: search product based on medicine, product,seller or company name
# buyers can type the name of the medicine, product,seller or company.
# The products listed will be shown based on - minimum order quantity​, cost​ and availability
*/
const searchProduct = (req, res) => {
    let user = req.user;   
}


/*
# parameters: token,
# purpose: calculate final cost based on discount, tax, etc...
# also check the product stock availability in inventory
*/
const finalCalculationBeforeCheckout = (req, res) => {
    let user = req.user;   
}


/*
# parameters: token,
# purpose: checkout order, make payment transaction if payment mode is online
*/
const Checkout = (req, res) => {
    let user = req.user;   
}


/*
# parameters: token,
# purpose: if payment mode is online and seller update the cost of placed order then buyer need to cancel or pay extra moneny
# pay extra money for existing order
*/
const payMoneyAfterCheckout = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: buyer can add money in wallet
*/
const depositCreditInWallet = (req, res) => {
    let user = req.user;   
}

const visitedCategory = async(req, res) => {
    let userData = await user.findOne({ _id: req.user._id }).then(result => result);
    /*  let visited_category = userData.visited_category ;
 
     visited_category = [...visited_category, ...[req.body.visited_category.toString()]];
     visited_category = [...new Set(visited_category)]; */
    let index = userData.visited_category.findIndex((e) => e.toString() === req.body.visited_category.toString());
    if (index < 0) {
        userData.visited_category.push(ObjectId(req.body.visited_category));
        userData.save((err, response) => {
            if (err) {
                return res.status(200).json({
                    error: true,
                    detail: err,
                    title: 'Error occured.',
                });
            }
            else {
                return res.status(200).json({
                    error: false,
                    detail: response,
                    title: 'Category added successfully.',
                });
            }
        })
    } else {
        return res.status(200).json({
            error: false,
            detail: userData,
            title: 'Category added successfully.',
        });
    }
}

const stats = async (req, res) => {
    let date = new Date(`${req.query.year}-01-01`);
    //first date
    
    let newApr1 = new Date(moment(date).month("April").startOf('month').format('YYYY-MM-DD'));
    let newMar31 = new Date(moment(date).add('1','year').month("March").endOf('month').format('YYYY-MM-DD'));
    
    // let Jan1 = new Date(date.getFullYear()+1, 0, 1);
    // let Mar31 = new Date(date.getFullYear()+1, 2, 31);
    // let Apr1 = new Date(date.getFullYear(), 3, 1);
    // let Dec31 = new Date(date.getFullYear(), 11, 31);
    // Jan1.setHours(0, 0, 0, 0);
    // Mar31.setHours(23, 59, 59, 999);
    // Apr1.setHours(0, 0, 0, 0);
    // Dec31.setHours(23, 59, 59, 999); 
    // console.log('=-0=-0',Jan1,Dec31)
    let last30Days =  {$gte: new Date((new Date().getTime() - (30 * 24 * 60 * 60 * 1000)))};
    let Id = req.query.id;
    let montlyOrdersAprToDec = await Order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: { 
                "user_id": ObjectId(Id), 
                "is_deleted": false,
                "createdAt": { $gte: newApr1, $lte: newMar31 },
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
            }  
        },
        {$group: {
            _id: {$substr: ['$createdAt', 5, 2]},
            orders: {$sum: 1},
            amount: { $sum: { $toDouble: "$total_amount" } }
        }}
    ]);
    // let montlyOrdersJanToMar = await Order.aggregate([
    //     {
    //         $addFields: {
    //             current_status: { $arrayElemAt: ["$order_status", -1] },
    //         }
    //     },
    //     {
    //         $match: { 
    //             "user_id": ObjectId(Id), 
    //             "is_deleted": false,
    //             "createdAt": {$gte: Jan1 ,$lte: Mar31 },
    //             $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
    //         }  
    //     },
    //     {$group: {
    //         _id: {$substr: ['$createdAt', 5, 2]},
    //         orders: {$sum: 1},
    //         amount: { $sum: { $toDouble: "$total_amount" } }
    //     }}
    // ]);
    // let montlyOrders = montlyOrdersAprToDec.concat(montlyOrdersJanToMar);
    let montlyOrders = montlyOrdersAprToDec;
    let totalOrders = await Order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: { 
                "user_id": ObjectId(Id), 
                "is_deleted": false,
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
            }  
        },{
            $count: "totalOrders"
          }]);
    let orderAmount = await Order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: { 
                "user_id": ObjectId(Id), 
                "is_deleted": false,
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
            }  
        },
        { 
            $group: { 
                _id:"$user_id",
                saleAmount:{ $sum: "$total_amount"} , 
            }
        } 
    ]);
    totalOrders = totalOrders.length>0 ? totalOrders[0].totalOrders : 0; 
    orderAmount = orderAmount.length>0 ? orderAmount[0].saleAmount : 0;
    last30Orders = await Order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: { 
                "user_id": ObjectId(Id), 
                "is_deleted": false,
                "createdAt": last30Days,
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
            }  
        },{
            $count: "totalOrders"
    }]); 
    last30Orders = last30Orders.length>0?last30Orders[0].totalOrders:0;
    let last30OrderAmt = await Order.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: { 
                "user_id": ObjectId(Id), 
                "is_deleted": false,
                 "createdAt": last30Days,
                 $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }}]
            }  
        },
        { 
            $group: { 
                _id:"$user_id",
                saleAmount:{ $sum: "$total_amount"} , 
            }
        } 
    ]);
    last30OrderAmt = last30OrderAmt.length>0?last30OrderAmt[0].saleAmount:0; 
    return res.status(200).json({
        error: false, 
        montlyOrders,
        totalOrders, 
        orderAmount, 
        last30Orders,
        last30OrderAmt,
    }); 
}

module.exports = {
    bulkRequest,
    listingBuyer,
    getBuyerDetails,
    finalCalculationBeforeCheckout,
    Checkout,
    payMoneyAfterCheckout,
    depositCreditInWallet,
    visitedCategory,
    stats
}
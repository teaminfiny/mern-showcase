var promotions = require('../models/promotions');
const helper = require('../lib/helper');

/*
# parameters: token,
# purpose: listing of all Buyer
*/
const listingPromotions = (req, res) => {
    if(req.body.type == 'admin'){
        promotions.find({}) 
        .populate('seller_id','first_name last_name phone user_address ')
        .exec((error, promotionList) => {
            if(error) {
                return res.status(200).json({
                    title: "Something went wrong, Please try again",
                    error: true
                })
            } else {
                return res.status(200).json({
                    title: "Promotion fetched successfully",
                    error: false,
                    detail: promotionList
                })
            }
        })
    }else{
    promotions.find({isActive : true})
    .sort({updatedAt: -1}) 
    .populate('seller_id','first_name last_name phone user_address ')
    .exec((error, promotionList) => {
        if(error) {
            return res.status(200).json({
                title: "Something went wrong, Please try again",
                error: true
            })
        } else {
            return res.status(200).json({
                title: "Promotion fetched successfully",
                error: false,
                detail: promotionList
            })
        }
    })}
}

/*
# parameters: token,name, from_date, to_date, seller_id, image_banner
# purpose: add promotion
*/
const addPromotion = (req, res) => {
    let user = req.user;   
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('from_date', 'From date is required').notEmpty();
    req.checkBody('to_date', 'To date is required').notEmpty();
    req.checkBody('image_banner', 'Image banner is required').notEmpty();
    req.checkBody('redirect_url', 'Redirect URL is required').notEmpty();
    let { name, from_date, to_date, promo_code, image_banner,redirect_url } = req.body;
    let dir = 'promotions';
    
    let tempPromocode = new promotions({
        image_banner :  helper.base64UploadS3(req, res, dir, image_banner),
        name,
        from_date,
        to_date,
        promo_code,
        redirect_url 
    });

    tempPromocode.save((error, promocode) => {
        if(error) {
            return res.status(200).json({
                title: "Something went wrong, Please try again",
                error: true
            })
        } else {
            return res.status(200).json({
                title: "Promotion create successfully",
                error: false,
                detail: promocode
            })
        }
    })
}

/*
# parameters: token, promotionId
# purpose: edit promotions
*/
const editPromotion = (req, res) => {
    promotions.findOneAndUpdate({ _id: req.body.promotion_id }, { $set: { redirect_url: req.body.redirect_url } }, { new: true })
    .exec((err, promotion) => {
        return res.status(200).json({
            error: false,
            detail: promotion
        });
    });
}

/*
# parameters: token,promotionId
# purpose: delete Promotion
*/
const deletePromotion = (req, res) => {
    let promotionId = req.body.promotion_id;
    promotions.findById(promotionId)
        .then((promotion, error) => {
            if(promotion){
                helper.deleteFromS3(promotion.image_banner); 
                promotion.remove();
                return res.status(200).json({
                    error: false,
                    detail: promotion
                }); 
            }
            else{
                return res.status(200).json({
                    error: false,
                    detail: "Not Found"
                }); 
            }
            
        })
}

const activatePromotion = (req, res) => {
    promotions.activatePromotion(req, res);
}


module.exports = {
    listingPromotions,
    addPromotion,
    editPromotion,
    deletePromotion,
    activatePromotion
}
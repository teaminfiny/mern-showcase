
const moment = require('moment');
var mongoose = require('mongoose');
var productCategory = require('../models/product_category')
var product = require('../models/product')
const user = require('../models/user');
var inventory = require('../models/inventory');
var companies = require('../models/company');
var discounts = require('../models/discounts');
var Orders = require('../models/order')
var ObjectId = mongoose.Types.ObjectId;
const helper = require('../lib/helper');
const async = require('async');
const ActiveInventory = require('../models/activeInventory');
const BuyerRequestController = require('./buyerRequest')
const ShortbookController = require('./shortBook');
// var courierService1 = require('../lib/CourierService')
/*
# parameters: token,
# purpose: Get all Inventory of seller
*/
const listingInventory = async (req, res) => {
    let date = new Date();
    let tempMonth = date.getMonth() + 1;
    let currentMonth = date.getMonth();
    let tempYear = date.getFullYear();
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    let query2 = req.user.user_type != 'admin' ? {
        seller_id: ObjectId(Id),
        createdAt: { $gte: new Date((new Date().getTime() - (30 * 24 * 60 * 60 * 1000))) }
    } : { 
        createdAt: { $gte: new Date((new Date().getTime() - (30 * 24 * 60 * 60 * 1000))) }
    } 
    let lastOrders = await Orders.aggregate([{
        $match: query2
    },
    { $unwind: '$products' },
    { $group: { _id: '$products.inventory_id' } }
    ]);
    let orderData = Array.from(lastOrders, x => ObjectId(x._id));
    let productCategories = await productCategory.find({ status: 'active' }, { name: 1, _id: 0 });
    let query = [];

    if (req.user.user_type != "admin") {
        query.push({
            $match: {
                user_id: ObjectId(Id),
                isDeleted: false
            }
        })
    }
    if ( (req.body.visible === true || req.body.visible === false) && req.body.visible !== '' ) {
        query.push({
            $match: {
                isHidden: req.body.visible
            }
        })
    }

    //online admin should be able to filter inventory by seller and company
    if (req.user.user_type === "admin" && req.body.companyId != undefined && req.body.companyId != "") {
        query.push({
            $match: {
                company_id: ObjectId(req.body.companyId)
            }
        })
    }
    if (req.user.user_type === "admin" && req.body.sellerId != undefined && req.body.sellerId != "") {
        query.push({
            $match: {
                user_id: ObjectId(req.body.sellerId),
                isDeleted: false
            }
        })
    }
    else {
        query.push({
            $match: {
                isDeleted: false
            }
        })
    }
    if (req.body.filter != undefined && req.body.filter.toLowerCase() === 'active selling products') {
        query.push({
            $addFields: { isId: { $in: ['$_id', orderData] } }

        })
        if( req.user.user_type === "admin" && (req.body.sellerId == undefined || req.body.sellerId == "")){
            query.push({
                $match: { isId: false }
            })
        }else{
            query.push({
                $match: { isId: true }
            })
        }

    }
    if (req.body.filter != undefined && req.body.filter.toLowerCase() === 'slow moving products') {
        query.push({
            $addFields: { isId: { $in: ['$_id', orderData] } }

        })
        query.push({
            $match: { isId: false }
        })
    }
    if (req.body.filter != undefined && req.body.filter.toLowerCase() === "short expire products") {
        query.push({
            '$addFields': {
                month: {
                    $add: [
                        { '$subtract': [{ '$month': '$expiry_date' }, tempMonth] },
                        { $multiply: [12, { $subtract: [{ '$year': '$expiry_date' }, tempYear] }] }
                    ]
                }
            }
        },
            { '$match': { isDeleted: false }},
            { '$match': {$expr: {$gte: ["$quantity", "$min_order_quantity"]}}},
            { '$match': { month: { '$lt': 9 } } },
            { '$match': { month: { '$gt': 3 } } });
    }
    if (req.body.filter != undefined && req.body.filter.toLowerCase() === "out of stock products") {
        query.push({
            '$addFields': {
                month: {
                    $add: [
                        { '$subtract': [{ '$month': '$expiry_date' }, tempMonth] },
                        { $multiply: [12, { $subtract: [{ '$year': '$expiry_date' }, tempYear] }] }
                    ]
                }
            }
        },
            { '$match': { isDeleted: false }},
            { '$match': { $or: [{$expr: {$lt: ["$quantity", "$min_order_quantity"]}},{quantity: { $eq: 0 }}]} },
            { '$match': { month: { '$gte': 9 } } });
    }
    if (req.body.filter != undefined && req.body.filter.toLowerCase() === "expired") {
        query.push({
            '$addFields': {
                month: {
                    $add: [
                        { '$subtract': [{ '$month': '$expiry_date' }, tempMonth] },
                        { $multiply: [12, { $subtract: [{ '$year': '$expiry_date' }, tempYear] }] }
                    ]
                }
            }
        },
            { '$match': {$expr: {$gte: ["$quantity", "$min_order_quantity"]}}},
            { '$match': { month: { '$lt': 3 } } });
    }
    query.push(
        {
            $lookup: {
                localField: 'discount_id',
                foreignField: '_id',
                from: 'discounts',
                as: 'Discounts'
            }
        },
        {
            $unwind: {
                path: '$Discounts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'company_id',
                foreignField: '_id',
                from: 'companies',
                as: 'Company'
            }
        },
        {
            $unwind: {
                path: '$Company',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'product_id',
                foreignField: '_id',
                from: 'products',
                as: 'Products'
            }
        },
        {
            $unwind: {
                path: '$Products',
                preserveNullAndEmptyArrays: true
            }
        }, {
        $lookup: {
            localField: 'Products.product_cat_id',
            foreignField: '_id',
            from: 'product_categories',
            as: 'ProductCategory'
        }
    },
        {
            $unwind: {
                path: '$ProductCategory',
                preserveNullAndEmptyArrays: true
            }
        }, {
        $lookup: {
            localField: 'Products.GST',
            foreignField: '_id',
            from: 'gsts',
            as: 'GST'
        }
    },
        {
            $unwind: {
                path: '$GST',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Products.Type',
                foreignField: '_id',
                from: 'producttypes',
                as: 'ProductTypes'
            }
        },
        {
            $unwind: {
                path: '$ProductTypes',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'Seller'
            }
        },
        {
            $unwind: {
                path: '$Seller',
                preserveNullAndEmptyArrays: true
            }
        });
    if (req.body.searchText != undefined && req.body.searchText != undefined && req.body.searchText != "") {
        let keyValue = escapeRegex(req.body.searchText)
        query.push({
            $match: { "Products.name": { $regex: new RegExp(keyValue, 'i') }, isDeleted: false }
        })
    }
    if (req.body.selectedCategory != undefined && req.body.selectedCategory.length > 0) {
        query.push(
            {
                $addFields: { isCatgeory: { $in: ['$ProductCategory.name', req.body.selectedCategory] } }
            },
            {
                $match: {
                    isCatgeory: req.body.selectedCategory.length > 0
                }
            })
    }
    query.push({
        $facet: {
            metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
            data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
        }
    });
    let detail = await inventory.aggregate(query).allowDiskUse(true);
    res.status(200).json({
        error: false,
        detail,
        productCategories
    })
}
//this function is not of any use, it should be removed after 25 april 2020
const oldlistingInventory = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id;
    let currentDate = new Date();
    let matchQuery = req.body.searchText ? { "Products.name": { $regex: req.body.searchText, $options: 'i' } } : {}
    Orders.aggregate([
        {
            $match: {
                seller_id: ObjectId(Id),
                is_deleted: false,
                $and: [{ 'order_status.status': { $ne: 'Delivered' } }, { 'order_status.status': { $ne: 'Cancelled' } }]
            }
        },
        { $unwind: '$products' },
        {
            $group: {
                _id: "$products.inventory_id"
            }
        }
    ])
        .exec((orderError, orderData) => {
            if (orderError) {
                res.status(200).json({
                    error: true,
                    detail: [],
                    productCategories: []
                })
            } else {
                let orderIdArray = Array.from(orderData, x => ObjectId(x._id))
                productCategory.find({ status: 'active' }, { name: 1, _id: 0 }).then((productCategories) => {
                    if (!req.body.filter && req.body.selectedCategory && req.body.selectedCategory.length === 0) {
                        inventory
                            .aggregate([
                                {
                                    $match: {
                                        user_id: ObjectId(Id),
                                        isDeleted: false
                                    }
                                },
                                {
                                    $addFields: { months: { $month: '$expiry_date' } }
                                },
                                {
                                    $lookup: {
                                        localField: 'discount_id',
                                        foreignField: '_id',
                                        from: 'discounts',
                                        as: 'Discounts'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'company_id',
                                        foreignField: '_id',
                                        from: 'companies',
                                        as: 'Company'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'product_id',
                                        foreignField: '_id',
                                        from: 'products',
                                        as: 'Products'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Products',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $match: matchQuery
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.GST',
                                        foreignField: '_id',
                                        from: 'gsts',
                                        as: 'GST'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.Type',
                                        foreignField: '_id',
                                        from: 'producttypes',
                                        as: 'ProductTypes'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.product_cat_id',
                                        foreignField: '_id',
                                        from: 'product_categories',
                                        as: 'ProductCategory'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$ProductCategory',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$GST',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$ProductTypes',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Discounts',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Discounts.discount_on_product.inventory_id',
                                        foreignField: '_id',
                                        from: 'inventories',
                                        as: 'Inventories'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Inventories',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Inventories.product_id',
                                        foreignField: '_id',
                                        from: 'products',
                                        as: 'OtherProducts'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$OtherProducts',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Company',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                { $sort: { createdAt: -1 } },
                                {
                                    $facet: {
                                        metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                                        data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
                                    }
                                }
                            ])
                            .then((detail) => {
                                let tempInventoryData = detail[0].data.map((inventoryData, key) => {
                                    let index = orderIdArray.findIndex((e) => e.toString() === inventoryData._id.toString());
                                    let days1 = (new Date(inventoryData.expiry_date) - currentDate) / (1000 * 3600 * 24)
                                    if (index > -1) {
                                        detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                        detail[0].data[key].isActive = true;
                                        return inventoryData;
                                    } else {
                                        detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                        detail[0].data[key].isActive = false
                                        return inventoryData;
                                    }
                                })

                                res.status(200).json({
                                    error: false,
                                    detail,
                                    productCategories
                                })
                            })
                    } else if (req.body.filter === 'Active Selling Products') {
                        inventory
                            .aggregate([
                                {
                                    $match: {
                                        user_id: ObjectId(Id),
                                        isDeleted: false
                                    }
                                },
                                {
                                    $addFields: { months: { $month: '$expiry_date' } }
                                },
                                {
                                    $addFields: { isId: { $in: ['$_id', orderIdArray] } }

                                },
                                {
                                    $match: { isId: true }
                                },
                                {
                                    $lookup: {
                                        localField: 'discount_id',
                                        foreignField: '_id',
                                        from: 'discounts',
                                        as: 'Discounts'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'company_id',
                                        foreignField: '_id',
                                        from: 'companies',
                                        as: 'Company'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'product_id',
                                        foreignField: '_id',
                                        from: 'products',
                                        as: 'Products'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Products',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $match: matchQuery
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.GST',
                                        foreignField: '_id',
                                        from: 'gsts',
                                        as: 'GST'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.Type',
                                        foreignField: '_id',
                                        from: 'producttypes',
                                        as: 'ProductTypes'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.product_cat_id',
                                        foreignField: '_id',
                                        from: 'product_categories',
                                        as: 'ProductCategory'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$ProductCategory',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $addFields: { isCatgeory: { $in: ['$ProductCategory.name', req.body.selectedCategory] } }
                                },
                                {
                                    $match: {
                                        isCatgeory: req.body.selectedCategory.length > 0
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$GST',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$ProductTypes',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Discounts',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Discounts.discount_on_product.inventory_id',
                                        foreignField: '_id',
                                        from: 'inventories',
                                        as: 'Inventories'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Inventories',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Inventories.product_id',
                                        foreignField: '_id',
                                        from: 'products',
                                        as: 'OtherProducts'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$OtherProducts',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Company',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                { $sort: { createdAt: -1 } },
                                {
                                    $facet: {
                                        metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                                        data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
                                    }
                                }
                            ])
                            .then((detail) => {

                                let tempInventoryData = detail[0].data.map((inventoryData, key) => {
                                    let index = orderIdArray.findIndex((e) => e.toString() === inventoryData._id.toString());
                                    let days1 = (new Date(inventoryData.expiry_date) - currentDate) / (1000 * 3600 * 24)
                                    if (index > -1) {
                                        detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                        detail[0].data[key].isActive = true;
                                        return inventoryData;
                                    } else {
                                        detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                        detail[0].data[key].isActive = false
                                        return inventoryData;
                                    }
                                })


                                res.status(200).json({
                                    error: false,
                                    detail,
                                    productCategories
                                })
                            });
                    } else if (req.body.filter === 'Slow moving Products') {
                        Orders.aggregate([
                            {
                                $match: {
                                    "seller_id": Id,
                                    isDeleted: false
                                }
                            },
                            {
                                $addFields: {
                                    year: { $year: "$createdAt" },
                                    month: { $month: "$createdAt" }
                                }
                            },
                            {
                                $project: {
                                    products: 1,
                                    month: 1, year: 1,
                                    data: {
                                        $filter: {
                                            input: "$order_status",
                                            as: 'item',
                                            cond: { $eq: ["$$item.status", "Delivered"] }
                                        }
                                    }
                                }
                            },
                            {
                                $addFields: {
                                    Delivered: { $size: "$data" }
                                }
                            },
                            {
                                $match: {
                                    Delivered: { $gte: 1 }
                                }
                            },
                            {
                                $unwind: "$products"
                            },
                            {
                                $group: {
                                    _id: "$products.inventory_id",
                                    quantity: { $sum: { $toInt: "$products.quantity" } },
                                    mycount: { $sum: 1 }
                                }
                            },
                            {
                                $match: {
                                    mycount: { $lte: 4 }
                                }
                            },
                            {
                                $sort: { quantity: -1 }
                            },
                            {
                                $limit: req.body.perPage ? req.body.perPage : 0
                            },
                            {
                                $lookup: {
                                    localField: "_id",
                                    foreignField: "_id",
                                    from: "inventories",
                                    as: "Inventory"
                                }
                            },
                            { $unwind: "$Inventory" },
                            {
                                $lookup: {
                                    localField: "Inventory.product_id",
                                    foreignField: "_id",
                                    from: "products",
                                    as: "Product"
                                }
                            },
                            { $unwind: "$Product" },
                            {
                                $lookup: {
                                    localField: "Product.company_id",
                                    foreignField: "_id",
                                    from: "companies",
                                    as: "Company"
                                }
                            },
                            { $unwind: "$Company" },
                            {
                                $project: {
                                    quantity: 1,
                                    month: 1,
                                    "Product.name": 1,
                                    "Company.name": 1
                                }
                            }
                        ])
                            .then((results) => {
                                let orderData = Array.from(results, x => ObjectId(x._id))
                                inventory
                                    .aggregate([
                                        {
                                            $match: {
                                                user_id: ObjectId(Id),
                                                isDeleted: false
                                            }
                                        },
                                        {
                                            $addFields: { months: { $month: '$expiry_date' } }
                                        },
                                        {
                                            $addFields: { isId: { $in: ['$_id', orderData] } }

                                        },
                                        {
                                            $match: { isId: true }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'discount_id',
                                                foreignField: '_id',
                                                from: 'discounts',
                                                as: 'Discounts'
                                            }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'company_id',
                                                foreignField: '_id',
                                                from: 'companies',
                                                as: 'Company'
                                            }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'product_id',
                                                foreignField: '_id',
                                                from: 'products',
                                                as: 'Products'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$Products',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $match: matchQuery
                                        },
                                        {
                                            $lookup: {
                                                localField: 'Products.GST',
                                                foreignField: '_id',
                                                from: 'gsts',
                                                as: 'GST'
                                            }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'Products.Type',
                                                foreignField: '_id',
                                                from: 'producttypes',
                                                as: 'ProductTypes'
                                            }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'Products.product_cat_id',
                                                foreignField: '_id',
                                                from: 'product_categories',
                                                as: 'ProductCategory'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$ProductCategory',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $addFields: { isCatgeory: { $in: ['$ProductCategory.name', req.body.selectedCategory] } }
                                        },
                                        {
                                            $match: {
                                                isCatgeory: req.body.selectedCategory.length > 0
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$GST',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$ProductTypes',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$Discounts',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'Discounts.discount_on_product.inventory_id',
                                                foreignField: '_id',
                                                from: 'inventories',
                                                as: 'Inventories'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$Inventories',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $lookup: {
                                                localField: 'Inventories.product_id',
                                                foreignField: '_id',
                                                from: 'products',
                                                as: 'OtherProducts'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$OtherProducts',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        {
                                            $unwind: {
                                                path: '$Company',
                                                preserveNullAndEmptyArrays: true
                                            }
                                        },
                                        { $sort: { createdAt: -1 } },
                                        {
                                            $facet: {
                                                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                                                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
                                            }
                                        }
                                    ])
                                    .then((detail) => {
                                        let tempInventoryData = detail[0].data.map((inventoryData, key) => {
                                            let index = orderIdArray.findIndex((e) => e.toString() === inventoryData._id.toString());
                                            let days1 = (new Date(inventoryData.expiry_date) - currentDate) / (1000 * 3600 * 24)
                                            if (index > -1) {
                                                detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                                detail[0].data[key].isActive = true;
                                                return inventoryData;
                                            } else {
                                                detail[0].data[key].status = momentdetail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                                detail[0].data[key].isActive = false
                                                return inventoryData;
                                            }
                                        })

                                        res.status(200).json({
                                            error: false,
                                            detail,
                                            productCategories
                                        })
                                    })
                            })
                    } else {
                        let date = new Date();
                        let tempMonth = date.getMonth() + 1
                        let tempYear = date.getFullYear()
                        let match = req.body.filter === 'Out of stock Products' ? { quantity: { $eq: 0 } } : req.body.filter === 'Short expire products' ? { $and: [{ month: { '$lte': 9 } }, { month: { '$gt': 3 } }] } : req.body.filter === 'not expired' ? { month: { '$gt': 0 } } : {}
                        inventory
                            .aggregate([
                                {
                                    $match: {
                                        user_id: ObjectId(Id),
                                        isDeleted: false
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'discount_id',
                                        foreignField: '_id',
                                        from: 'discounts',
                                        as: 'Discounts'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'company_id',
                                        foreignField: '_id',
                                        from: 'companies',
                                        as: 'Company'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'product_id',
                                        foreignField: '_id',
                                        from: 'products',
                                        as: 'Products'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Products',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $match: matchQuery
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.GST',
                                        foreignField: '_id',
                                        from: 'gsts',
                                        as: 'GST'
                                    }
                                },
                                {
                                    $addFields: {
                                        months: { $month: '$expiry_date' }
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.Type',
                                        foreignField: '_id',
                                        from: 'producttypes',
                                        as: 'ProductTypes'
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Products.product_cat_id',
                                        foreignField: '_id',
                                        from: 'product_categories',
                                        as: 'ProductCategory'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$ProductCategory',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $addFields: { isCatgeory: { $in: ['$ProductCategory.name', req.body.selectedCategory ? req.body.selectedCategory : []] } }
                                },
                                {
                                    $match: {
                                        isCatgeory: req.body.selectedCategory ? req.body.selectedCategory.length > 0 : false
                                    }
                                },
                                {
                                    '$addFields': {
                                        month: {
                                            $add: [
                                                { '$subtract': [{ '$month': '$expiry_date' }, tempMonth] },
                                                { $multiply: [12, { $subtract: [{ '$year': '$expiry_date' }, tempYear] }] }
                                            ]
                                        }
                                    }
                                },

                                {
                                    $match: match
                                },
                                {
                                    $unwind: {
                                        path: '$GST',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$ProductTypes',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Discounts',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Discounts.discount_on_product.inventory_id',
                                        foreignField: '_id',
                                        from: 'inventories',
                                        as: 'Inventories'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Inventories',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $lookup: {
                                        localField: 'Inventories.product_id',
                                        foreignField: '_id',
                                        from: 'products',
                                        as: 'OtherProducts'
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$OtherProducts',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                {
                                    $unwind: {
                                        path: '$Company',
                                        preserveNullAndEmptyArrays: true
                                    }
                                },
                                { $sort: { createdAt: -1 } },
                                {
                                    $facet: {
                                        metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                                        data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
                                    }
                                }
                            ])
                            .then((detail) => {
                                let tempInventoryData = detail[0].data.map((inventoryData, key) => {
                                    let index = orderIdArray.findIndex((e) => e.toString() === inventoryData._id.toString());
                                    let days1 = (new Date(inventoryData.expiry_date) - currentDate) / (1000 * 3600 * 24)
                                    if (index > -1) {
                                        detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                        detail[0].data[key].isActive = true;
                                        return inventoryData;
                                    } else {
                                        detail[0].data[key].status = days1 < 180 ? 'Expired' : parseInt(inventoryData.quantity) > 0 ? "In stock" : "Out of Stock";
                                        detail[0].data[key].isActive = false
                                        return inventoryData;
                                    }
                                })

                                res.status(200).json({
                                    error: false,
                                    detail,
                                    productCategories
                                })
                            })
                    }
                })
            }
        })
}

/*
# parameters: token,
# purpose: Get single Inventory Details of seller
*/
const inventoryDetails = (req, res) => {
    let user = req.user;
}

/*
# parameters: token,
# purpose: add single Inventory Details of seller
*/
const addInventoryItem = async(req, res) => {
    let userData = req.user;
    req.checkBody('product_id', 'Name is required').notEmpty();
    req.checkBody('company_id', 'Company name is required').notEmpty();
    req.checkBody('expiry_date', 'Expiry date is required').notEmpty();
    req.checkBody('MRP', 'MRP is required').notEmpty();
    req.checkBody('PTR', 'PTR is required').notEmpty();
    req.checkBody('quantity', 'Quantity is required').notEmpty();
    req.checkBody('min_order_quantity', 'Min order quantity is required').notEmpty();
    req.checkBody('max_order_quantity', 'Max order quantity is required').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    }else if(Number(req.body.PTR) > Number(req.body.MRP)){
        return res.status(200).json({
            error: true,
            title: `PTR can't be greater than MRP.`
        });
    }else if(Number(req.body.min_order_quantity) > Number(req.body.max_order_quantity)){
        return res.status(200).json({
            error: true,
            title: `Min quantity can't be greater than Max quantity.`
        });
    } else {
        let { product_id, company_id, expiry_date, MRP, PTR, quantity, min_order_quantity, max_order_quantity, discount_per, purchase, bonus, discount_name, otherInventoryId } = req.body;

        let prod = await product.findOne({ _id: ObjectId(product_id) }).populate('medicine_type_id').exec();
        
        let sameDisc = (discount_name == 'Same' || discount_name == 'SameAndDiscount' ) ? Number((Number(PTR) * Number(purchase))/(Number(purchase) + Number(bonus))) : 0;

        let discDisc = (discount_name == 'Discount' || discount_name == 'DifferentAndDiscount') ? ( Number(PTR) - (( Number(PTR)/100) * Number(discount_per))) : 0 ;
  
        let noDiscDisc =  Number(PTR) ;

        let sameAndDisc = (discount_name == 'SameAndDiscount') ? sameDisc - (sameDisc/100 * Number(discount_per)) : 0;

        let differentAndDisc = (discount_name == 'DifferentAndDiscount') ?  noDiscDisc - ( noDiscDisc/100 * Number(discount_per) ) :0;

        let totalAmt = discount_name != '' ? (discount_name == 'Same') ? sameDisc : (discount_name == 'Diffrent') ? noDiscDisc : 
        (discount_name == 'Discount') ? discDisc : (discount_name == 'SameAndDiscount') ? sameAndDisc :
        (discount_name == 'DifferentAndDiscount') ? differentAndDisc : Number(PTR) : Number(PTR);
        console.log('=-0=-0=-0=-0=-0=-0=-0=-0=-add',totalAmt)
        
        let newInventory = new inventory({
            product_id,
            company_id,
            expiry_date,
            MRP,
            PTR,
            quantity: parseInt(quantity),
            min_order_quantity: parseInt(min_order_quantity),
            max_order_quantity: parseInt(max_order_quantity),
            user_id: userData.mainUser ? userData.mainUser : userData._id,
            status: 'In stock',
            isDeleted: false,
            ePTR: (totalAmt).toFixed(2),
            medicineType_id: prod.medicine_type_id._id,
            medicineTypeName: prod.medicine_type_id.name,
            productName: prod.name
        })

        newInventory.save(async (error, savedInventory) => {
            let compName = ''
            await companies.findOne({_id: savedInventory.company_id}).then(result => {
                if(result){
                    compName = result.name;
                }
            })
            req.body.Id = savedInventory._id;
            if (error) {
                res.status(200).json({
                    title: 'Something went wrong, Please try again.',
                    error: true,
                    detail: []
                })
            } else {
                if (req.body.discount_name) {
                    discounts.addDiscount(req, res, async (error, discountData) => {
                        if (error) {
                            res.status(200).json({
                                title: 'Something went wrong, Please try again.',
                                error: true,
                                detail: []
                            })
                        } else {
                            try {
                                let data = await inventory.findByIdAndUpdate(savedInventory._id, { $set: { discount_id: discountData._id } }, { 'new': true });

                                if (data) {
                                    if (discount_name !== undefined && discount_name !== 'Discount' && discount_name !== 'nodiscount') {
                                        let disount = {
                                            inventory_id: (discount_name === 'Same' || discount_name === 'SameAndDiscount' )? savedInventory._id : otherInventoryId,
                                            purchase: parseInt(purchase),
                                            bonus: parseInt(bonus),
                                        }
                                        discounts.editDiscount(discountData._id, { discount_on_product: disount }, async(error, updatedData) => {
                                            if (error) {
                                                return res.status(200).json({
                                                    title: "Something went wrong, Please try again",
                                                    error: true,
                                                    detail: error
                                                })
                                            } else {
                                                await BuyerRequestController.checkFulfilledRequest(savedInventory.productName,savedInventory.company_id, compName)
                                                await ShortbookController.markInstockShortbook(savedInventory.product_id)
                                                updateActiveInventory(savedInventory._id);
                                                return res.status(200).json({
                                                    title: "Inventory added successfully",
                                                    error: false,
                                                    detail: data
                                                })
                                            }
                                        })
                                    } else {
                                        await BuyerRequestController.checkFulfilledRequest(savedInventory.productName,savedInventory.company_id, compName)
                                        await ShortbookController.markInstockShortbook(savedInventory.product_id)
                                        updateActiveInventory(savedInventory._id);
                                        return res.status(200).json({
                                            title: "Inventory added successfully",
                                            error: false,
                                            detail: data
                                        })
                                    }
                                } else {
                                    await BuyerRequestController.checkFulfilledRequest(savedInventory.productName,savedInventory.company_id, compName)
                                    await ShortbookController.markInstockShortbook(savedInventory.product_id)
                                    updateActiveInventory(savedInventory._id);
                                    return res.status(200).json({
                                        title: "Inventory added successfully",
                                        error: false,
                                        detail: data
                                    })
                                }
                            } catch (error) {
                                return res.status(200).json({
                                    title: "Something went wrong, Please try again",
                                    error: true,
                                    detail: error
                                })
                            }
                        }
                    })
                } else {
                    await BuyerRequestController.checkFulfilledRequest(savedInventory.productName,savedInventory.company_id, compName)
                    await ShortbookController.markInstockShortbook(savedInventory.product_id)
                    updateActiveInventory(savedInventory._id);
                    return res.status(200).json({
                        title: "Inventory added successfully",
                        error: false,
                        detail: savedInventory
                    })
                }

            }
        })

    }
}

/*
# parameters: token,
# purpose: edit single Inventory Details of seller
*/
const editInventoryItem = (req, res) => {
    inventory.findOne({ _id: ObjectId(req.body.Id) }).then((result) => {
        if (!result) {
            return res.status(200).json({
                error: true,
                title: 'No such inventory found please try again'
            })
        }else if(Number(req.body.PTR) > Number(req.body.MRP)){
            return res.status(200).json({
                error: true,
                title: `PTR can't be greater than MRP.`
            });
        }else if(Number(req.body.min_order_quantity) > Number(req.body.max_order_quantity)){
            return res.status(200).json({
                error: true,
                title: `Min quantity can't be greater than Max quantity.`
            });
        }
        let productId = result.product_id;
        let { PTR, min_order_quantity, discount_per, bonus, discount_name, purchase } = req.body;
        let sameDisc = (discount_name == 'Same' || discount_name == 'SameAndDiscount') ? Number((Number(PTR) * Number(purchase))/(Number(purchase) + Number(bonus))):0;

        let discDisc = (discount_name == 'Discount' || discount_name == 'DifferentAndDiscount') ? ( Number(PTR) - (( Number(PTR)/100) * Number(discount_per))) :0 ;
  
        let noDiscDisc =  Number(PTR) ;

        let sameAndDisc = (discount_name == 'SameAndDiscount') ? sameDisc - (sameDisc/100 * Number(discount_per)):0;

        let differentAndDisc = (discount_name == 'DifferentAndDiscount') ? noDiscDisc - ( noDiscDisc/100 * Number(discount_per) ) :0;

        let totalAmt = discount_name != '' ? (discount_name == 'Same') ? sameDisc : (discount_name == 'Diffrent') ? noDiscDisc : 
        (discount_name == 'Discount') ? discDisc : (discount_name == 'SameAndDiscount') ? sameAndDisc :
        (discount_name == 'DifferentAndDiscount') ? differentAndDisc : Number(PTR) : Number(PTR);

        console.log('=-0=-0=-0=-0=-0=-0=-0=-0=-0edit',totalAmt)
        result.quantity = parseInt(req.body.quantity);
        result.min_order_quantity = parseInt(req.body.min_order_quantity);
        result.max_order_quantity = parseInt(req.body.max_order_quantity);
        result.expiry_date = req.body.expiry_date;
        result.MRP = req.body.MRP;
        result.PTR = req.body.PTR;
        result.ePTR = (totalAmt).toFixed(2);
        result.prepaidInven = req.body.prepaidInven && req.body.prepaidInven == true ? true : false;
        if(parseInt(req.body.quantity) > parseInt(req.body.min_order_quantity)){
            result.status = 'In stock'
        }
        if (!req.body.discount_name && result.discount_id) {
            result.discount_id = null
        }
        result.save().then(async(result) => {
        ActiveInventory.deleteOne({ inventory_id: ObjectId(req.body.inventoryId)}).exec();

            if (req.body.discount_name) {
                let { purchase, bonus, discount_name, discount_per } = req.body;
                if (req.body.discountId) {
                    discounts.findOne({ _id: ObjectId(req.body.discountId) }).then(async(discountResult) => {
                        if (!discountResult) {
                            await ShortbookController.markInstockShortbook(productId)
                            updateActiveInventory(req.body.Id);
                            return res.status(200).json({
                                error: false,
                                title: 'Inventory updated successfully'
                            })
                        }
                        // let { purchase, bonus, discount_name, discount_per } = req.body;
                        discountResult.name = discount_name
                        discountResult.type = discount_name
                        discountResult.discount_per = discount_per ? discount_per : 0
                        if (discount_name !== 'Discount') {
                            discountResult.discount_on_product.purchase = purchase ? parseInt(purchase) : 0
                            discountResult.discount_on_product.bonus = bonus ? parseInt(bonus) : 0
                            discountResult.discount_on_product.inventory_id = (discount_name === 'Same' || discount_name === 'SameAndDiscount') ? req.body.Id : req.body.otherInventoryId
                            discountResult.discount_per = (discount_name === 'DifferentAndDiscount' || discount_name === 'SameAndDiscount') ? discount_per : 0
                        } else {
                            discountResult.discount_on_product = {}
                        }

                        discountResult.save().then(async(savedDiscount) => {
                            await ShortbookController.markInstockShortbook(productId)
                            updateActiveInventory(req.body.Id);
                            res.status(200).json({
                                error: false,
                                title: 'Inventory updated successfully'
                            })
                        })
                    })
                } else {
                    discounts.addDiscount(req, res, async (error, discountData) => {
                        if (error) {
                            res.status(200).json({
                                title: 'Something went wrong, Please try again.',
                                error: true,
                                detail: []
                            })
                        } else {
                            try {
                                let data = await inventory.findByIdAndUpdate(result._id, { $set: { discount_id: discountData._id } }, { 'new': true });

                                if (data) {
                                    if (req.body.discount_name !== undefined && req.body.discount_name !== 'Discount') {
                                        let disount = {
                                            inventory_id: (req.body.discount_name === 'Same' || req.body.discount_name === 'SameAndDiscount') ? result._id : req.body.otherInventoryId,
                                            purchase,
                                            bonus,
                                        }
                                        discounts.editDiscount(discountData._id, { discount_on_product: disount }, async(error, updatedData) => {
                                            if (error) {
                                                return res.status(200).json({
                                                    title: "Something went wrong, Please try again",
                                                    error: true,
                                                    detail: error
                                                })
                                            } else {
                                                await ShortbookController.markInstockShortbook(productId)
                                                updateActiveInventory(req.body.Id);
                                                return res.status(200).json({
                                                    title: "Inventory updated successfully",
                                                    error: false,
                                                    detail: data
                                                })
                                            }
                                        })
                                    } else {
                                        await ShortbookController.markInstockShortbook(productId)
                                        updateActiveInventory(req.body.Id);
                                        return res.status(200).json({
                                            title: "Inventory updated successfully",
                                            error: false,
                                            detail: data
                                        })
                                    }
                                } else {
                                    await ShortbookController.markInstockShortbook(productId)
                                    updateActiveInventory(req.body.Id);
                                    return res.status(200).json({
                                        title: "Inventory updated successfully",
                                        error: false,
                                        detail: data
                                    })
                                }
                            } catch (error) {
                                return res.status(200).json({
                                    title: "Something went wrong, Please try again",
                                    error: true,
                                    detail: error
                                })
                            }
                        }
                    })
                }
            } else {
                await ShortbookController.markInstockShortbook(productId)
                updateActiveInventory(req.body.Id);
                res.status(200).json({
                    error: false,
                    title: 'Inventory updated successfully'
                })
            }
        })
    })
}

/*
# parameters: token,
# purpose: update Stock Quantity single Inventory Details of seller
# seller can update invetory item's Stock Quantity from mobile app
*/
const updateStockQuantity = async(req, res) => {
    let user = req.user;
    let status = ''
    let productId = ''
    await inventory.findOne({ _id: ObjectId(req.body.inventoryId) }).then(result => {
        if(parseInt(req.body.quantity) > parseInt(result.quantity)){
            status = 'In stock'
            productId = result.product_id
        }
        else{
            status = 'Out of Stock'
        }
    })
    await inventory.findByIdAndUpdate({ _id: ObjectId(req.body.inventoryId) }, { $set: { quantity: req.body.quantity, status:status } }).then(async(result) => {
        status === 'In stock' ? await ShortbookController.markInstockShortbook(productId) :'';
        updateActiveInventory(req.body.inventoryId)
        res.status(200).json({
            error: false,
            title: 'Stock updated successfully'
        })
    })
}

/*
# parameters: token,
# purpose: delete single Inventory Details of seller
*/
const deleteInventoryItem = (req, res) => {
    inventory.findByIdAndUpdate({ _id: req.body.inventoryId }, { $set: { isDeleted: true } }).then((result) => {
        ActiveInventory.deleteOne({ inventory_id: ObjectId(req.body.inventoryId)}).exec();
        res.status(200).json({
            error: false,
            title: 'Inventory deleted successfully'
        })
    })
}

/*
# parameters: token,
# purpose: hide single Inventory Details of seller
*/
const hideInventoryItem = (req, res) => {
    let user = '';
    if(req.body.hide && req.user.user_type === "admin"){
        user = "admin";
    }
    else if(req.body.hide && req.user.user_type === "seller"){
        user = "seller"
    }
    inventory.findByIdAndUpdate({ _id: req.body.inventoryId }, { $set: { isHidden: req.body.hide, hiddenBy:user } }).then(async(result) => {
        ActiveInventory.deleteMany({ inventory_id: ObjectId(req.body.inventoryId)}).exec();
        if(req.body.hide === true){
            ShortbookController.updateShortBook('outOfStock', result.product_id);
        }
        if(!req.body.hide){
            updateActiveInventory(req.body.inventoryId);
            let product = await inventory.findOne({ _id: req.body.inventoryId }).then(inv => inv.product_id)
            await ShortbookController.markInstockShortbook(product)
        }
        res.status(200).json({
            error: false,
            title: req.body.hide ? 'This product will not be visible for buyers' : 'This product is visible for buyers'
        })
    })
}


/*
# parameters: token,
# purpose: seller can add new product in medicine bank but need to take aprove from admin 
# this API request to add new product in dedicine bank 
*/
const requestAddProduct = (req, res) => {
    let user = req.user;
}

const searchInventory = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    inventory.aggregate([
        {
            $match: {
                isDeleted: false,
                user_id: ObjectId(Id),
                $expr: {$gt: ["$quantity", "$min_order_quantity"]}
            }
        },
        {
            $lookup: {
                localField: 'product_id',
                foreignField: '_id',
                from: 'products',
                as: 'Products'
            }
        },
        {
            $lookup: {
                localField: 'company_id',
                foreignField: '_id',
                from: 'companies',
                as: 'Company'
            }
        },
        {
            $unwind: "$Products"
        },
        {
            $unwind: "$Company"
        },
        { $addFields: { name: '$Products.name' } },
        {
            $match: {
                'Products.name': { $regex: req.body.name, $options: 'i' }
            }
        },
        {
            $limit: 10
        }
    ])
        .then((detail) => {
            res.status(200).json({
                error: false,
                detail
            })
        })
}

const outOfStockFive = (req, res) => {
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    inventory.aggregate([
        {
            $match: {
                user_id: ObjectId(Id),
                isDeleted: false
            }
        },
        {
            $lookup: {
                localField: 'discount_id',
                foreignField: '_id',
                from: 'discounts',
                as: 'Discounts'
            }
        },
        {
            $lookup: {
                localField: 'company_id',
                foreignField: '_id',
                from: 'companies',
                as: 'Company'
            }
        },
        {
            $lookup: {
                localField: 'product_id',
                foreignField: '_id',
                from: 'products',
                as: 'Products'
            }
        },
        {
            $unwind: {
                path: '$Products',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Products.GST',
                foreignField: '_id',
                from: 'gsts',
                as: 'GST'
            }
        },
        {
            $lookup: {
                localField: 'Products.Type',
                foreignField: '_id',
                from: 'producttypes',
                as: 'ProductTypes'
            }
        },
        {
            $lookup: {
                localField: 'Products.product_cat_id',
                foreignField: '_id',
                from: 'product_categories',
                as: 'ProductCategory'
            }
        },
        {
            $unwind: {
                path: '$ProductCategory',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: { quantity: { $eq: 0 } }
        },
        { $sort: { updatedAt: -1 } },
        { $limit: 5 },
        {
            $unwind: {
                path: '$GST',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$ProductTypes',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$Discounts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discounts.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'Inventories'
            }
        },
        {
            $unwind: {
                path: '$Inventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Inventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$Company',
                preserveNullAndEmptyArrays: true
            }
        },
    ])
        .then((detail) => {
            res.status(200).json({
                error: false,
                detail
            })
        })
}
const escapeRegex = (string) => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}
const v1search = (req, res) => {
    try {
        let page = req.body.page > 0 ? req.body.page : 1
        let perPage = req.body.perPage ? req.body.perPage : 10
        let filter = [{'quantity':{$gt: 0}}]; //initilize filter

        if (req.body.key && req.body.key != '') {
            //product_filter.push({ "Product.name": new RegExp(req.body.key, 'i') })
            let keyValue = escapeRegex(req.body.key)
            filter.push({ $or: [{ "Product.name": { $regex: new RegExp(keyValue, 'i') } }, { "Product.chem_combination": { $regex: new RegExp(keyValue, 'i') } }] })
        }
        if (req.body.category_id && req.body.category_id.length > 0) {
            let data = Array.from(req.body.category_id, x => ObjectId(x))
            filter.push({ "Product_Category._id": { $in: data } })
        }
        if (req.body.company_id) {
            filter.push({ "Company._id": ObjectId(req.body.company_id) })
        }
        if (req.body.seller_id) {
            filter.push({ "Seller._id": ObjectId(req.body.seller_id) })
        }
        if (req.body.discount_id) {
            filter.push({ "Discount._id": ObjectId(req.body.discount_id) })
        }
        if (req.body.mediType && req.body.mediType !== '') {
            if(req.body.mediType === 'Ethical Branded'){
                filter.push({$or: [{ "medi_type": { $regex: new RegExp('Ethical Branded', 'i') } }, { "medi_type": { $regex: new RegExp('Others', 'i') } }]})
            }
            else if(req.body.mediType === 'Cool Chain'){
                filter.push({"medi_type": { $regex: new RegExp('Cool Chain', 'i') } })
            }
            else if(req.body.mediType === 'Generic'){
                filter.push({$or: [{ "medi_type": { $regex: new RegExp('Generic', 'i') } }, { "medi_type": { $regex: new RegExp('Surgical', 'i') } }, { "medi_type": { $regex: new RegExp('OTC', 'i') } }]})
            }
        }
        if (req.body.discount && req.body.discount !== '') {
            filter.push({ "final": {$gte : Number(req.body.discount)}  })
        }
        let sort = (req.body.sortBy == 'name') ? { 'Product.name': 1 } : (req.body.sortBy == 'discountHighToLow') ? { 'final': -1 } : (req.body.sortBy == 'discountLowToHigh') ? { 'final': 1 } : (req.body.sortBy == 'price') ? { 'ePTR': 1 } :{ 'Product.name': 1 };
        let query = [
            {
                $match: {
                    $and: filter
                }  
            },
            { $sort: sort },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(page) } }],
                    data: [{ $skip: (Number(perPage) * Number(page)) - Number(perPage) }, { $limit: Number(perPage) }]
                }
            }
        ];

        ActiveInventory.aggregate(query).allowDiskUse(true).then((products) => {
            if(products[0].data.length < 1){
                if(req.body.key && req.body.key != ''){
                    let keyValue = escapeRegex(req.body.key) ;
                    product.findOne({ "name": { $regex: new RegExp(keyValue, 'i')},isDeleted:false }).populate('company_id').then(result => {
                        if(result){
                            res.status(200).json({
                                error: false,
                                inSystem: true,
                                searchedProduct: result,
                                products
                            })
                        }
                        else{
                            res.status(200).json({
                                error: false,
                                inSystem: false,
                                products
                            })
                        }
                    })
                }
                else{
                    res.status(200).json({
                        error: false,
                        inSystem: false,
                        products
                    })
                }
            }
            else{
                res.status(200).json({
                    error: false,
                    products
                })
            }
        });
    } catch (error) {
    }

}
const search = (req, res) => {
    try {
        let page = req.body.page > 0 ? req.body.page : 1
        let perPage = req.body.perPage ? req.body.perPage : 10
        let date = moment();
        let dateAfter3Months = moment().add(3, "M");
        let product_filter = [{ "Product.isDeleted": false }] //initilize filter on product
        let inventory_filter = [
            { quantity: { $gt: 0 } },
            {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
            { isDeleted: false },
            { isHidden: false },
            { expiry_date: { $gt: new Date(dateAfter3Months) } },
        ] // initilize filter on inventory for seller and discount

        if (req.body.key && req.body.key != '') {
            //product_filter.push({ "Product.name": new RegExp(req.body.key, 'i') })
            let keyValue = escapeRegex(req.body.key)
            product_filter.push({ $or: [{ "Product.name": { $regex: new RegExp(keyValue, 'i') } }, { "Product.chem_combination": { $regex: new RegExp(keyValue, 'i') } }] })
        }
        if (req.body.category_id && req.body.category_id.length > 0) {
            let data = Array.from(req.body.category_id, x => ObjectId(x))
            product_filter.push({ "Product.product_cat_id": { $in: data } })
        }
        if (req.body.company_id) {
            product_filter.push({ "Product.company_id": ObjectId(req.body.company_id) })
        }
        if (req.body.seller_id) {
            inventory_filter.push({ user_id: ObjectId(req.body.seller_id) })
        }
        if (req.body.discount_id) {
            inventory_filter.push({ discount_id: ObjectId(req.body.discount_id) })
        }
        let sort = (req.body.sortBy == 'name') ? { 'Product.name': 1 } : (req.body.sortBy == 'price') ? { 'Inventory.ePTR': 1 } :{ 'Product.name': 1 };
        let query = [
            {
                $match: {
                    $and: inventory_filter
                }  
            },
            {
                $lookup: {
                    localField: "_id",
                    foreignField: "_id",
                    from: "inventories",
                    as: "Inventory"
                }
            },
            { $unwind: "$Inventory" },
            {
                $lookup: {
                    localField: "discount_id",
                    foreignField: "_id",
                    from: "discounts",
                    as: "Discount"
                }
            },
            {
                $unwind: {
                    path: '$Discount',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'Discount.discount_on_product.inventory_id',
                    foreignField: '_id',
                    from: 'inventories',
                    as: 'DiscountInventories'
                }
            },
            {
                $unwind: {
                    path: '$DiscountInventories',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'DiscountInventories.product_id',
                    foreignField: '_id',
                    from: 'products',
                    as: 'OtherProducts'
                }
            },
            {
                $unwind: {
                    path: '$OtherProducts',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: "product_id",
                    foreignField: "_id",
                    from: "products",
                    as: "Product"
                }
            },
            { $unwind: "$Product" },
            {
                $match: {
                    $and: product_filter
                }
            },
            {
                $lookup: {
                    localField: "Product.company_id",
                    foreignField: "_id",
                    from: "companies",
                    as: "Company"
                }
            },
            { $unwind: "$Company" },
            {
                $lookup: {
                    localField: "Product.GST",
                    foreignField: "_id",
                    from: "gsts",
                    as: "GST"
                }
            },
            { $unwind: "$GST" },
            {
                $lookup: {
                    localField: "Product.Type",
                    foreignField: "_id",
                    from: "producttypes",
                    as: "Type"
                }
            },
            { $unwind: "$Type" },
            {
                $lookup: {
                    localField: "Product.product_cat_id",
                    foreignField: "_id",
                    from: "product_categories",
                    as: "ProductCategory"
                }
            },
            { $unwind: "$ProductCategory" },
            {
                $lookup: {
                    localField: "user_id",
                    foreignField: "_id",
                    from: "users",
                    as: "Seller"
                }
            },
            { $unwind: "$Seller" },
            {
                $match: {
                    $and: [
                        { "Seller.is_deleted": false },
                        { "Seller.user_status": "active" },
                        { "Seller.vaccation.vaccationMode": false },
                    ],
                    $or: [
                        { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                    ],
                    $or: [
                        { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                    ]
                }
            },
            {
                $project: {
                    "Inventory": 1,
                    "Product": 1,
                    "Company": 1,
                    "Seller.first_name": 1,
                    "Seller.last_name": 1,
                    "Seller.company_name": 1,
                    "Seller._id": 1,
                    "Discount": 1,
                    "OtherProducts": 1,
                    "ProductCategory.name": 1,
                    _id: 1,
                    MRP: 1,
                    PTR: 1
                }
            },
            { $sort: sort },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(page) } }],
                    data: [{ $skip: (Number(perPage) * Number(page)) - Number(perPage) }, { $limit: Number(perPage) }]
                }
            }
        ];

        inventory.aggregate(query).then((products) => {
            res.status(200).json({
                error: false,
                products
            })
        });
    } catch (error) {
    }

}
// pass seller_id to get seller categories 
// pass company_id to get company category
const getCategory = (req, res) => {
    if (req.body.company_id) {
        product.aggregate([
            {
                $match: {
                    company_id: ObjectId(req.body.company_id)
                }
            },
            {
                $lookup: {
                    localField: "product_cat_id",
                    foreignField: "_id",
                    from: "product_categories",
                    as: "ProductCategory"
                }
            },
            { $unwind: "$ProductCategory" },
            { $group: { _id: "$ProductCategory._id", name: { $addToSet: "$ProductCategory.name" } } },
            { $unwind: "$name" },
            { $sort: { name: 1 } }
        ]).then((detail) => {
            res.status(200).json({
                error: false,
                detail
            })
        })
    }
    else {
        inventory.aggregate([
            {
                $match: req.body.seller_id ? {
                    isDeleted: false,
                    isHidden: false,
                    user_id: ObjectId(req.body.seller_id)
                } : {
                        isDeleted: false,
                        isHidden: false,
                    }
            },
            {
                $lookup: {
                    localField: 'product_id',
                    foreignField: '_id',
                    from: 'products',
                    as: 'Product'
                }
            },
            {
                $unwind: "$Product"
            },
            {
                $lookup: {
                    localField: "Product.product_cat_id",
                    foreignField: "_id",
                    from: "product_categories",
                    as: "ProductCategory"
                }
            },
            { $unwind: "$ProductCategory" },
            { $group: { _id: "$ProductCategory._id", name: { $addToSet: "$ProductCategory.name" } } },
            { $unwind: "$name" },
            { $sort: { name: 1 } }
        ]).then((detail) => {
            res.status(200).json({
                error: false,
                detail
            })
        })
    }
}
const getFilters = async (req, res) => {
    let sellers;
    let buyers;
    let comapnies = await companies.getAllCompanies();
    if( req.query.user_type == 'admin' ){
        sellers = await user.getAllSellers({ user_type: 'seller', mainUser: null, user_status: 'active'});
        buyers = await user.getAllBuyers({ user_type: 'buyer' });
    }else{
        sellers = await user.getAllSellers({ user_type: 'seller', mainUser: null, user_status: 'active', 'vaccation.vaccationMode': false });
    }
    let categories = await inventory.getActiveCategories();
    detail = {
        comapnies,
        sellers,
        categories,
        buyers
    }

    res.status(200).json({
        error: false,
        detail
    })
}

/*
* Used in admin can be used for seller listing
* page, perPage, filter- send direct filter
*/
const getList = async (req, res) => {
    let inventories = await inventory.list(req.body);
    detail = {
        inventories
    }
    res.status(200).json({
        error: false,
        detail
    })
}

const dealOfTheDay = (req, res) => {
    inventory.findOneAndUpdate({ _id: req.body.id, isDeleted: false }, { $set: { isDealsOfTheDay: req.body.isDeal } })
        .then((data) => {
            if (data) {
                updateActiveInventory(data._id);
                return res.status(200).json({
                    error: false,
                    title: "Deals of the day set successfully"
                })
            } else {
                return res.status(200).json({
                    error: true,
                    title: "Something went wrong"
                })
            }
        })
}
const jumboDeal = (req, res) => {
    inventory.findOneAndUpdate({ _id: req.body.id, isDeleted: false }, { $set: { isJumboDeal: req.body.isJumboDeal } })
        .then((data) => {
            if (data) {
                updateActiveInventory(data._id);
                return res.status(200).json({
                    error: false,
                    title: "Jumbo deal set successfully"
                })
            } else {
                return res.status(200).json({
                    error: true,
                    title: "Something went wrong"
                })
            }
        })
}
const Service = (req, res) => {
    // courierService1.delhiveryService(req, res, function (data) {
    //     res.status(200).json({
    //         data
    //     })
    // })
}
const addEptr = async(req, res) => {
    inventory.aggregate([
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        // { $limit: 5 }
    ]).then((result)=>{
        async.forEach(result,(data,cb)=>{
            if(data){
            let sameDisc = data.Discount && data.Discount.discount_on_product ? Number((Number(data.PTR) * Number(data.Discount.discount_on_product.purchase))/(Number(data.Discount.discount_on_product.purchase) + Number(data.Discount.discount_on_product.bonus))) :0;

            let discDisc = data.Discount && data.Discount.discount_per ?  ( Number(data.PTR) - (( Number(data.PTR)/100) * Number(data.Discount.discount_per))) : 0 ;

            let noDiscDisc =  Number(data.PTR) ;

            let sameAndDisc = sameDisc != 0 ? sameDisc - (sameDisc/100 * Number(data.Discount.discount_per)) :0;

            let differentAndDisc = data.Discount && noDiscDisc - ( noDiscDisc/100 * Number(data.Discount.discount_per) );

            let totalAmt = ( data.Discount ) ? (data.Discount.name == 'Same') ? sameDisc : (data.Discount.name == 'Diffrent') ? noDiscDisc : 
            (data.Discount.name == 'Discount') ? discDisc : (data.Discount.name == 'SameAndDiscount') ? sameAndDisc :
            (data.Discount.name == 'DifferentAndDiscount') ? differentAndDisc : Number(data.PTR) : Number(data.PTR);
            inventory.findByIdAndUpdate({ _id: data._id}, { $set:{ ePTR: (totalAmt).toFixed(2) } }).exec();
            cb()
            }else{
               cb() 
            }
        },function(err){
            res.status(200).json({
                title:'Success'
            })
        })
    })
}
const addParameters = (req,res) =>{
    inventory.addParameters(req, res);
}

const updateActiveInventory = async(inventoryId) => {
    let date = moment();
    let dateAfter3Months = moment().add(3, "M");
    await inventory.aggregate([
        {
            $match: {
                $and: [
                    { _id: ObjectId(inventoryId)},
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } },
                    { status: {$ne: 'Expired'}}
                ]
            }
        },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
    ])
        .then(async(result) => {
            if(!result || result.length < 1) return true
            await ActiveInventory.deleteMany({inventory_id: ObjectId(result[0]._id)}).catch(error => {
                console.log(error);
            })
            
                let actInv = result;
                console.log('actInv',actInv)
                let finalDiscount = 0;
                let discountWithPer = 0;
                let discountWithProduct = 0;
                let totalDiscount = 0;
                let totalPrice = 0;
                if(actInv[0].hasOwnProperty("Discount")){
                    if(actInv[0].Discount && actInv[0].Discount.name && actInv[0].Discount.name === 'Discount'){
                        finalDiscount = actInv[0].Discount && actInv[0].Discount.discount_per && Number(actInv[0].Discount.discount_per).toFixed(2);
                    }else if(actInv[0].Discount && actInv[0].Discount.name && (actInv[0].Discount.name === 'Different')){
                        totalPrice = actInv[0].Discount && actInv[0].Discount.discount_on_product && (actInv[0].Discount.discount_on_product.purchase + actInv[0].Discount.discount_on_product.bonus) * actInv[0].PTR;

                        finalDiscount = Number(Number(actInv[0].PTR / totalPrice) * 100).toFixed(2);
                    }else if(actInv[0].Discount && actInv[0].Discount.name && (actInv[0].Discount.name === 'DifferentAndDiscount')){
                        totalPrice = actInv[0].Discount && actInv[0].Discount.discount_on_product && (actInv[0].Discount.discount_on_product.purchase + actInv[0].Discount.discount_on_product.bonus) * actInv[0].PTR;

                        finalDiscount = Number(Number(Number(actInv[0].PTR / totalPrice) * 100) + Number(actInv[0].Discount.discount_per)).toFixed(2);
                    }else if (actInv[0].Discount && actInv[0].Discount.name && (actInv[0].Discount.name === 'Same' || actInv[0].Discount.name === 'SameAndDiscount')){
                        totalPrice = actInv[0].Discount && actInv[0].Discount.discount_on_product && (actInv[0].Discount.discount_on_product.bonus / (actInv[0].Discount.discount_on_product.purchase + actInv[0].Discount.discount_on_product.bonus));
                        let dis = actInv[0].Discount && actInv[0].Discount.discount_per && Number(actInv[0].Discount.discount_per) > 0 ? Number(actInv[0].Discount.discount_per).toFixed(2) : 0;
                        finalDiscount = Number(Number(Number(Number(totalPrice) * 100) + Number(dis)).toFixed(2));
                        console.log('-=-=-=-=-dis',totalPrice,dis,finalDiscount)
                    }
                }
                else{
                    finalDiscount = 0;
                }


                let attribute = [];
                if(actInv[0].ProductCategory.isFeatured === true){
                    attribute.push('Featured');
                }
                if(actInv[0].isDealsOfTheDay === true){
                    attribute.push('Deals Of Day');
                }
                if(actInv[0].isJumboDeal === true){
                    attribute.push('Jumbo Deal');
                }
                let discount = actInv[0].hasOwnProperty("Discount") ? {
                    _id: ObjectId(actInv[0].Discount._id),
                    name: actInv[0].Discount.name,
                    discount_type: actInv[0].Discount.type,
                    discount_per: (actInv[0].Discount.discount_per),
                    discount_on_product :{
                        inventory_id:actInv[0].Discount.discount_on_product.inventory_id,
                        purchase: (actInv[0].Discount.discount_on_product.purchase),
                        bonus: (actInv[0].Discount.discount_on_product.bonus)
                    }
                } : {};

                let otherProducts =  actInv[0].hasOwnProperty("OtherProducts") ?  {
                    _id: ObjectId(actInv[0].OtherProducts._id),
                    name: actInv[0].OtherProducts.name
                } : {};

                let newInventory = new ActiveInventory({
                    inventory_id: ObjectId(actInv[0]._id),
                    medi_attribute:attribute,
                    medi_type:actInv[0].medicineTypeName,
                    Seller: {
                        _id: ObjectId(actInv[0].Seller._id),
                        first_name: actInv[0].Seller.first_name,
                        last_name: actInv[0].Seller.last_name,
                        company_name: actInv[0].Seller.company_name,
                        user_city: actInv[0].Seller.user_city,
                        user_state: actInv[0].Seller.user_state,
                        user_address: actInv[0].Seller.user_address,
                        user_pincode: actInv[0].Seller.user_pincode,
                    },
                    Product: {
                        _id: ObjectId(actInv[0].Product._id),
                        name: actInv[0].Product.name,
                        images: actInv[0].Product.images,
                        company: actInv[0].Company.name,
                        chem_combination: actInv[0].Product.chem_combination, 
                        product_type:actInv[0].Type.name,
                        country_of_origin: actInv[0].Product.country_of_origin,
                        GST:(actInv[0].GST.value),
                        isPrepaid: actInv[0].Product.isPrepaid ? actInv[0].Product.isPrepaid : false,
                        description: actInv[0].Product.description ? actInv[0].Product.description : '',
                    },
                    OtherProducts :otherProducts,
                    Product_Category: {
                        _id: ObjectId(actInv[0].ProductCategory._id),
                        name: actInv[0].ProductCategory.name
                    },
                    Discount: discount,
                    final: finalDiscount,
                    Company: {
                        _id: ObjectId(actInv[0].Company._id),
                        name: actInv[0].Company.name
                    },
                    status:(actInv[0].status).toUpperCase() ,
                    expiry_date: actInv[0].expiry_date,
                    MRP: (actInv[0].MRP),
                    PTR: (actInv[0].PTR),
                    ePTR: (actInv[0].ePTR), //effective price
                    quantity: (actInv[0].quantity),
                    min_order_quantity: (actInv[0].min_order_quantity),
                    max_order_quantity: (actInv[0].max_order_quantity),
                    prepaidInven: (actInv[0].prepaidInven)
                });
                await newInventory.save().catch(error => console.log(error))
        })
        .catch(error => {
            console.log(error);
        });
}


const changeInventory = () =>{
    product.find({isDeleted:true}).then(async(detail)=>{
        async.eachOfSeries(detail,async function(eachProduct,index2,mainCb) {
            if(eachProduct){
                console.log('product',eachProduct.name)
                await inventory.updateMany({product_id: ObjectId(eachProduct._id),isDeleted:false}, {$set:{isDeleted:true}})
                .then(data => {
                    if(data){
                        console.log('success',eachProduct._id)
                    }
                })
            }
            else{
                mainCb()
            }
        },function(){
            res.status(200).json({title: 'Success'})
        })
    })
}

module.exports = {
    listingInventory,
    inventoryDetails,
    addInventoryItem,
    editInventoryItem,
    updateStockQuantity,
    deleteInventoryItem,
    hideInventoryItem,
    requestAddProduct,
    searchInventory,
    outOfStockFive,
    search,
    v1search,
    getCategory,
    getFilters,
    oldlistingInventory,
    getList, // listing on admin 
    dealOfTheDay,
    jumboDeal,
    Service,
    addEptr,
    addParameters,
    updateActiveInventory,
    changeInventory
}
const Orders = require('../models/order');
const async = require('async');
let report ;
let fs = require('fs');
const moment = require('moment');
const User = require('../models/user');

let generateReport = async(req, res) =>{
    let product = req.body.product;
    console.log(product);
    if (product){
        await getProductSalesReport(product, res);
    }
    else{
        res.status(200).json({
            title: "Please insert product",
            error: true
        });
    }
}

let getProductSalesReport = async(product,res) => {
    console.log("product" , product);
    let firstDay = new Date('01/01/2020 00:00:00');
    let lastDay = new Date();
    let keyValue = escapeRegex(product)
    await Orders.aggregate([{
        $match:{
            "products.productName":{ $regex: new RegExp(keyValue, 'i')},
            $and:[{ "createdAt":{$gte: firstDay}},{ "createdAt":{$lte: lastDay} },
                {paymentStatus:{$ne:'pending'}},{paymentStatus:{$ne:'failed'}}],
            requested:{$ne:'Cancelled'}
        }  
    },
    {
        $lookup: {
            localField: 'user_id',
            foreignField: '_id',
            from: 'users',
            as: 'user'
        }
    },
    {
        $unwind: {
            path: '$user',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'user.user_state',
            foreignField: '_id',
            from: 'countries',
            as: 'state'
        }
    },
    {
        $unwind: {
            path: '$state',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'seller_id',
            foreignField: '_id',
            from: 'users',
            as: 'seller'
        }
    },
    {
        $unwind: {
            path: '$seller',
            preserveNullAndEmptyArrays: true
        }
    },
    {
        $lookup: {
            localField: 'products.discount_on_product.inventory_id',
            foreignField: '_id',
            from: 'inventories',
            as: 'otherProduct'
        }
    },
    {
        $unwind: {
            path: '$seller',
            preserveNullAndEmptyArrays: true
        }
    },
    { 
        $sort: { createdAt: -1 } 
    }
    ])
    .then((Orderdata) => {
        console.log('length', Orderdata.length)
        let total = 0;
        let index = 1;
        let rstring = "INDEX,PRODUCT NAME,ORDER DATE,ORDER ID,BUYER,SELLER,DISCOUNT,CITY,STATE,ZIPCODE,QUANTITY\n";
        async.eachOfSeries(Orderdata, function(eachOrder, key, callback){
            if(eachOrder){
                async.eachOfSeries(eachOrder.products, (prod,key,cb) => {
                    let prodreg = new RegExp('^'+keyValue);
                    if(prod && prod.productName.match(prodreg)){
                        rstring+=index+','
                                +(prod.productName).toUpperCase()+','
                                +moment(eachOrder.createdAt).format('DD-MM-YYYY hh:mm:ss A')+','
                                +eachOrder.order_id+','
                                +(eachOrder.user.company_name).toUpperCase()+','
                                +(eachOrder.seller?eachOrder.seller.company_name:'N/A').toUpperCase()+',';
                        if(prod.discount_name === 'Same' && prod.discount_on_product){
                            rstring+='BUY '+prod.discount_on_product.purchase+' GET '+prod.discount_on_product.bonus+' FREE,'
                        }
                        else if(prod.discount_name === 'SameAndDiscount' && prod.discount_on_product){
                            rstring+='BUY '+prod.discount_on_product.purchase+' GET '+prod.discount_on_product.bonus+' FREE AND '+prod.discount_per+' % DISCOUNT,'
                        }
                        else if(prod.discount_name === 'Different' && prod.discount_on_product){
                            rstring+='BUY '+prod.discount_on_product.purchase+' GET '+prod.discount_on_product.bonus+' '+eachOrder.otherProduct.productName +' FREE,'
                        }
                        else if(prod.discount_name === 'DifferentAndDiscount' && prod.discount_on_product){
                            rstring+='BUY '+prod.discount_on_product.purchase+' GET '+prod.discount_on_product.bonus+' '+eachOrder.otherProduct.productName +' FREE AND '+prod.discount_per+' % DISCOUNT,'
                        }else if(prod.discount_name === 'Discount'){
                            rstring+=prod.discount_per+' % DISCOUNT,'
                        }else if(prod.discount_per){
                            rstring+=prod.discount_per+' % DISCOUNT,'
                        }
                        else{
                            rstring+=','
                        }
                        let state = eachOrder.state ? eachOrder.state.name :'';
                        rstring+=((eachOrder.user.user_city).toUpperCase()).replace(/,/g, ' ')+','
                                +(state).toUpperCase()+','
                                +eachOrder.user.user_pincode+','
                                +prod.quantity+'\n';
                        total+=Number(prod.quantity); 
                        index+= 1
                        cb();
                    }
                    else{
                        cb();
                    }
                }, ()=>{
                    callback()
                });
            }
            else{
                callback()
            }
        }, function(){
            rstring+=",,,,,,,,,TOTAL,"+total+"\n"
            fs.writeFile('report.csv', rstring, err => {
                if (err){ 
                    console.log(err);
                    res.status(200).json({
                        title: "Something went wrong while generating report.",
                        error: true
                    });
                }
                else{
                    res.attachment('report.csv');
                    res.status(200).send(rstring);
                    console.log('Done');
                }
            });
        });
    });
}


const escapeRegex = (string) => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

const getSellerPerformance = (req, res) =>{
    let startDate = req.body.from_date ? new Date(req.body.from_date) : new Date();
    let endDate = req.body.to_date ? new Date(req.body.to_date) : new Date();
    startDate.setHours(startDate.getHours()-5);
    startDate.setMinutes(startDate.getMinutes()-30);
    endDate.setHours(endDate.getHours()-5);
    endDate.setMinutes(endDate.getMinutes()-30);
    let query = [
        {
            $match:{
                $and: [
                    {is_deleted:false}
                ]
            }
        },
        {
            $lookup: {
                localField: 'seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'seller'
            }
        },
        { $unwind: "$seller" },
        {
            $lookup: {
                localField: 'cancelledBy',
                foreignField: '_id',
                from: 'users',
                as: 'cancelledBy'
            }
        },
        { $match:{
            "order_cancel_reason": { $not :{$regex: new RegExp('Admin'), $options: 'i' }},
        }},
        { 
            $project: {
                "seller._id": 1,
                "seller.company_name": 1,
                totalOrders: {
                        $cond: [
                            {
                                $and:[
                                    {$or:[
                                        {$eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'New']} ,
                                        {$eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Requested'] }]},
                                    {$gte: [ "$createdAt", startDate]},
                                    {$lt: [ "$createdAt", endDate]},
                                    {$ne: ["$paymentStatus", 'pending']},
                                ]
                            },
                            1,
                            0
                        ]
                    
                },
                rejectedOrders: {
                        $cond: [
                            {
                                $and:[
                                    {$eq: [{ "$arrayElemAt": ["$order_status.status", -1] }, 'Cancelled']},
                                    {$eq: [{ "$arrayElemAt": ["$cancelledBy.user_type", -1] }, 'seller']},
                                    {$gte: [{ "$arrayElemAt": ["$order_status.date", -1] }, startDate]},
                                    {$lt: [{ "$arrayElemAt": ["$order_status.date", -1] }, endDate]}
                                ]
                            },
                            1,
                            0
                        ]
                    
                },
                processed: {
                        $cond: [
                            {
                                $and:[
                                    {$eq: [{ "$arrayElemAt": ["$order_status.status", {"$indexOfArray": ["$order_status.status","Processed"]}] }, 'Processed']},
                                    {$gte: [{ "$arrayElemAt": ["$order_status.date", {"$indexOfArray": ["$order_status.status","Processed"]}] }, startDate]},
                                    {$lt: [{ "$arrayElemAt": ["$order_status.date", {"$indexOfArray": ["$order_status.status","Processed"]}] }, endDate]}
                                ]
                            },
                            1,
                            0
                        ]
                    
                },
                earned: {
                        $cond: [
                            {
                                $and:[
                                    {$eq: [{ "$arrayElemAt": ["$order_status.status", {"$indexOfArray": ["$order_status.status","Processed"]}] }, 'Processed']},
                                    {$gte: [{ "$arrayElemAt": ["$order_status.date", {"$indexOfArray": ["$order_status.status","Processed"]}] }, startDate]},
                                    {$lt: [{ "$arrayElemAt": ["$order_status.date", {"$indexOfArray": ["$order_status.status","Processed"]}] }, endDate]}
                                ]
                            },
                            "$total_amount",
                            0
                        ]
                    
                },
            }
        },
        { 
            $group: {
                _id: "$seller._id",
                sellerName:{$first:"$seller.company_name"},
                open:{$sum:"$totalOrders"},
                processed:{$sum:"$processed"},
                rejected:{$sum:"$rejectedOrders"},
                amount:{$sum:"$earned"},
            }
        },
        {
            $sort:{'amount': -1}
        }
    ]
    
    if(req.body.searchedText && req.body.searchedText != ''){
        query.push(
            {
                $match:{sellerName:{ $regex: new RegExp('^' + req.body.searchedText), $options: 'i' }}
            }
        )
    }

    Orders.aggregate(query).then(result =>{
        res.status(200).json({
            title:'Success',
            error:false,
            data:result,
        })
    })
}

module.exports = {
    generateReport,
    getSellerPerformance
}
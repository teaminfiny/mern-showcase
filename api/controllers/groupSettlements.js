const groupSettlements = require('../models/groupSettlement');
var Orders = require('../models/order');
var Settlement = require('../models/settlement');
var user = require('../models/user');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
const fs = require('fs');
var ejs = require("ejs");
const moment = require('moment');
const helper = require('../lib/helper');
const sellerController = require('../controllers/seller');
const { Parser } = require('json2csv');
const async = require('async');
var user = require('../models/user');

const newData = (req, res) => {
  if(req.body.utrNo != '' && req.body.utrNo != undefined){
    let addData = new groupSettlements({
      utrNo: req.body.utrNo,
      payment_date: req.body.payment_date
    })
    addData.save().then((result)=>{
       res.status(200).json({
        error: false,
        title: 'Data added successfully'
      })
    })
   
  }else{
    res.status(200).json({
      error:true,
      title:'You just got an error'
    })
  }
}

const listGroupSettlement = async(req,res) => {
  let d = new Date();
  let firstDay = new Date(d.getFullYear(), d.getMonth(), 1);
  let lastDay = new Date(d.getFullYear(), d.getMonth()+1);
  if(req.body.from_date && req.body.from_date != ''){
      firstDay = new Date(req.body.from_date)
  }
  if(req.body.to_date && req.body.to_date != ''){
      lastDay = new Date(req.body.to_date) 
  }
  firstDay.setHours(0,0,0,0);
  lastDay.setHours(23,59,59,999);  
  var perPage = req.body.perPage ? parseInt(req.body.perPage) : 50
  var page = req.body.page ? parseInt(req.body.page): 1
  var skipPage = (page - 1) * perPage;
  let matchQuery = req.body.searchText ? { "order.order_id": { $regex: req.body.searchText, $options: 'i' }  } : {}
  let matchQ = req.user.user_type == 'admin' ? { 
    'seller_id': ObjectId(req.body.sellerId)
  } : {
    'seller_id': ObjectId(req.user._id),
    'isPublished': true
  }
  // $and:[{ "createdAt":{$gte: firstDay}},{ "createdAt":{$lte: lastDay} }]
  let data = await groupSettlements.aggregate([
      {
          $match: matchQ
      },
      {
          $lookup:{
              from: "orders",
              localField: "order_id",
              foreignField: "_id",
              as: "order"
          }
      },
      {   $unwind: { path: "$order", preserveNullAndEmptyArrays: true } },
      {
          $match: matchQuery 
      },
      {
          $lookup:{
              from: "users",
              localField: "seller_id",
              foreignField: "_id",
              as: "seller"
          }
      },
      {   $unwind: { path: "$seller", preserveNullAndEmptyArrays: true } },
      { $sort: {payment_date: -1}},
      {
          $facet: {
              metadata: [{ $count: "total" }, { $addFields: { page: req.body.page ? Number(req.body.page) : 1 } }],
              data: [{ $skip: req.body.perPage && req.body.page ? (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) : 0 }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
          }
      }
  ])
  res.status(200).json({
      error:false,
      data:data
  })
}
var generateRemittance = async (req, res) => {
  let Id = req.query.Id
  let groupSettle_details = await groupSettlements.findOne({ _id: ObjectId(Id) }).populate('seller_id').populate('seller_id.user_state', 'name').then(result=>result);
  let state = await user.findOne({ _id: ObjectId(groupSettle_details.seller_id._id) }).populate('user_state', 'name').exec();
  let settlementList = await Settlement.aggregate([
    {
      $match: { 
        seller_id: ObjectId(groupSettle_details.seller_id._id),
        seller_settlement_date: groupSettle_details.payment_date
       }
    },
    {
      $lookup:{
        localField:'order_id',
        foreignField:'_id',
        from:'orders',
        as:'order'
      }
    },
    { 
      $unwind: { path: '$order', preserveNullAndEmptyArrays: true }
    },
    {
      $sort: { 'order.order_id': 1 }
    },
    {
      $lookup:{
        localField:'order.user_id',
        foreignField:'_id',
        from:'users',
        as:'buyer'
      }
    },
    { 
      $unwind: { path: '$buyer', preserveNullAndEmptyArrays: true }
    },
    {
      $lookup:{
        localField:'order.seller_id',
        foreignField:'_id',
        from:'users',
        as:'seller'
      }
    },
    { 
      $unwind: { path: '$seller', preserveNullAndEmptyArrays: true }
    }
  ]).exec();
  var contents = fs.readFileSync('./views/remittance.ejs', 'utf8');
  // return res.status(200).send(contents);
  // let totalSurge = settlementList.reduce((acc,current)=> acc + current.commission_comp.prod_surge_comm,0);
  // let totalSurge = settlementList.reduce(function (accumulator, current) {
  //     let value = current ? current.commission_comp ? current.commission_comp.prod_surge_comm ? current.commission_comp.prod_surge_comm : 0 : 0 : 0;
  //     return accumulator + value
  // }, 0);
  // let sur = totalSurge > 0 ? totalSurge : 0;
  let sur = groupSettle_details.prod_surge_with_gst && groupSettle_details.prod_surge_with_gst > 0 ? groupSettle_details.prod_surge_with_gst : 0;
  var globalGst = (groupSettle_details.gst*groupSettle_details.commission/ 100).toFixed(2);
  var tcsPer = (groupSettle_details.tcs/groupSettle_details.order_value*100).toFixed(2);
  var tdsPer = (groupSettle_details.tds/groupSettle_details.order_value*100).toFixed(2);
  var remittanceVal = groupSettle_details.early_remittance_charge * groupSettle_details.order_value /100;
  var gstOnRemittance = remittanceVal/100 * groupSettle_details.gst;
  var date = new Date();
  var month =  date.toString().slice(4,7);
  var year =  date.toDateString().slice(13,15)
  var SettlementDetails = {
    title: "Sameer",
    sellerName: toTitleCase(groupSettle_details.seller_id.company_name),
    sellerAdd: groupSettle_details.seller_id.user_address,
    sellerCity: groupSettle_details.seller_id.user_city,
    sellerC: groupSettle_details.seller_id.user_country,
    sellerState: state.user_state.name,
    sellerEmail: groupSettle_details.seller_id.email,
    sellerPhone: groupSettle_details.seller_id.phone,
    sellerGst: groupSettle_details.seller_id.gstLic.lic,
    sellerPincode: groupSettle_details.seller_id.user_pincode,
    sellerAcc: groupSettle_details.seller_id.user_bank_details.account_no,
    sellerIfsc: groupSettle_details.seller_id.user_bank_details.ifsc_code,
    paymentDate: moment(groupSettle_details.payment_date).format('DD MMM YYYY'),
    sellerId: groupSettle_details.seller_id.sellerId,
    orderValue: (groupSettle_details.order_value).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits: 2}),
    commAmt: (groupSettle_details.commission).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}),
    otherCharges: (groupSettle_details.other_charges).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits: 2}),
    netAmt: Math.round(groupSettle_details.net_amt).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits: 2}),
    netAmt1: (groupSettle_details.net_amt).toLocaleString('en-IN',{minimumFractionDigits:2, maximumFractionDigits: 2}),
    tcs: (groupSettle_details.tcs).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}),
    tds: (groupSettle_details.tds).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}),
    gst: groupSettle_details.gst,
    utrNo: groupSettle_details.utrNo,
    settlements: settlementList,
    globalGst1: globalGst,
    tcsPer:tcsPer,
    tdsPer:tdsPer,
    monthYear: (month).toUpperCase()+""+year,
    remittanceId: groupSettle_details.remittanceId,
    remittance: (groupSettle_details.early_remittance_charge).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}),
    remittanceVal: (remittanceVal).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}),
    gstOnRemittance: (gstOnRemittance).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}),
    paymentDate: groupSettle_details.payment_date,
    remark: groupSettle_details.remark,
    gstOnOthercharges: groupSettle_details.gst_other_charges ? (groupSettle_details.gst_other_charges).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2}) : 0,
    totalProdSurge: (sur).toLocaleString('en-IN',{minimumFractionDigits: 2,maximumFractionDigits: 2})
  }
  var html = ejs.render(contents, SettlementDetails);
  res.status(200).send(html)
}
const toTitleCase = (data) => {
  return data
    .toLowerCase()
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
};
const sendReportEmail = async(req, res) => {
  if (req.query.Id != '' && req.query.email != '') {
  var mailData = {
      email:  req.query.email,//'sameer.m@infiny.in',
      subject: 'Remittance Advice',
      body:
        '<p>' +
        `Kindly <a href='${process.env.remittance_url}/groupSettlements/generateRemittance?Id=${req.query.Id}' target='_blank'>Click here</a> to view report`
    };
    // helper.sendEmail(mailData);
    let data = await groupSettlements.findOne({ _id: ObjectId(req.query.Id) }).exec();
    await groupSettlements.findByIdAndUpdate({ _id: req.query.Id }, { $set:{ isPublished: !data.isPublished } }).exec();
    res.status(200).json({
      error: false,
      title: 'Report sent successfully.'
    })
  } else {
    res.status(200).json({
      error: true,
      title: 'Something went wrong while sending report.'
    })
  }
}

const sellerGraphData = async(req, res) => {
  let Id = req.query.sellerId;
  let date = new Date();
  let Jan1 = new Date(date.getFullYear(), 0, 2);
  let Dec31 = new Date(date.getFullYear(), 11, 31);
  let graphD = await groupSettlements.aggregate([
      {
          $match: {
            seller_id: ObjectId(Id),
            "payment_date": { $gte: Jan1, $lt: Dec31 }
          }
      },
      {
        $lookup: {
          localField: 'utrNo',
          foreignField: 'transaction_id',
          from: 'settlement',
          as: 'settleOrder'
        }
      },
      {
        $unwind: { path: '$settleOrder', preserveNullAndEmptyArrays: true }
      },
      {
          $group: {
              _id: { $substr: ['$createdAt', 5, 2] },
              // orders: {'settleOrder.seller_settlement_date': {$sum:1} } ,
              amount: { $sum: { $toDouble: "$net_amt" } }
          }
      }
  ]);
  res.status(200).json({
    title: 'Data Fetched successfully.',
    error: false,
    graphD
  })
}

const getSettledCsv = async(req, res) => {
  let fields=[];
    fields = ["Date","Total amount","RTO/Other Charges","TCS","TDS","Commission","GST on Comm","ERC","GST on ERC","Surchrg with GST","UTR","Net amount"]
    let deli_date;
    let Id;
    let query = {}
    let orderArr = []
    let sellerDetail = await user.findOne({ _id: ObjectId(req.query.sellerId) }).exec();
    if (req.user.user_type === "admin" && req.query.month && req.query.year && req.query.sellerId) {
        query = {
            year: Number(req.query.year),
            month: sellerController.getMonthNumber(req.query.month),
            seller_id: ObjectId(req.query.sellerId)
        }
    }
    groupSettlements.aggregate([
        {
          $addFields: {
              year: { $year: "$payment_date" },
              month: { $month: "$payment_date" }
          }
        },
        {
            $match: query
        },
    ]).then((detail) => {
            let total = detail.reduce((acc,currentValue) => acc+currentValue.net_amt,0);
            async.eachOfSeries(detail, function (order, key, cb) {
                if (order) {
                  let remittanceVal = order.early_remittance_charge > 0 ? order.early_remittance_charge * order.order_value /100 : 0;
                  let gstOnRemittance = remittanceVal/100 * order.gst;
                  orderArr.push({
                      Date: moment(order.createdAt).format("DD/MM/YYYY"),
                      'Total amount': (order.order_value).toLocaleString('en-IN',{maximumFractionDigits:2}),
                      'RTO/Other Charges': (order.other_charges).toFixed(2),
                      TCS: order.tcs,
                      TDS: order.tds,
                      Commission: order.commission,
                      'GST on Comm': (order.commission/100 * order.gst).toFixed(2),
                      ERC:remittanceVal > 0 ? (remittanceVal).toLocaleString('en-IN',{maximumFractionDigits:2}) :'N/A',
                      'GST on ERC':gstOnRemittance > 0 ? (gstOnRemittance).toLocaleString('en-IN',{maximumFractionDigits:2}) :'N/A',
                      'Surchrg with GST': order.prod_surge_with_gst ? order.prod_surge_with_gst : 0,
                      UTR: order.utrNo ? order.utrNo : 'N/A',
                      'Net amount': (order.net_amt).toLocaleString('en-IN',{maximumFractionDigits:2})
                  })
                }
                cb()
            }, function (err) {
                var json2csvParser = new Parser({ fields: fields });
                const csv = json2csvParser.parse(orderArr);
                let totalRow = `,\r"","","","","","","","","","","Total",${(total).toFixed(2)}`
                let newCsv = csv.concat(totalRow);
                res.attachment(`${sellerDetail.company_name}.csv`);
                res.status(200).send(newCsv);
            });
        })
}

const fixGrpSettlement = (req, res) => {
  let fourteenJuly = new Date(moment().format('2021-07-14'));
  let created;
  groupSettlements.find({ createdAt: { $gt: fourteenJuly } }).then(grpData => {
    if (grpData) {
      async.eachOfSeries(grpData, function (grpSettlement, key, cb) {
        if (grpSettlement) {
          console.log('-=-=-=-key', key)
          Settlement.find({ seller_settlement_date: { $eq: grpSettlement.payment_date } }).then(async (allSettlement)=>{
            let pordSurge = 0;
            let prodSurgeWithGst = 0;
            let netValue = 0;
            allSettlement && allSettlement.map((currentValue) => {
              if (currentValue && currentValue.commission_comp && currentValue.commission_comp.prod_surge_comm) {
                pordSurge += currentValue.commission_comp.prod_surge_comm;
              }
            })

            prodSurgeWithGst = pordSurge > 0 ? Number(Number(Number(pordSurge) + (Number(pordSurge) * 0.18)).toFixed(2)) : 0;

            netValue = Math.round(Number(grpSettlement.order_value) -
              (Number(grpSettlement.tds) + Number(grpSettlement.tcs) + Number(grpSettlement.other_charges) + Number(grpSettlement.commission) + Number(grpSettlement.gst_other_charges)
                + Number((Number(grpSettlement.gst) * Number(grpSettlement.commission)) / 100) + Number(grpSettlement.order_value) / 100 * Number(grpSettlement.early_remittance_charge)
                + Number(((grpSettlement.order_value) / 100 * Number(grpSettlement.early_remittance_charge)) / 100 * Number(grpSettlement.gst)) + Number(prodSurgeWithGst))
            )
            await groupSettlements.findOneAndUpdate({ _id: ObjectId(grpSettlement._id) }, { $set: { net_amt: netValue, prod_surge_with_gst: prodSurgeWithGst } }).exec();
          });
        }
        cb();
      }, function (err) {
        res.status(200).json({
          title: 'success',
          error: false,
        })
      })
    }
  })
}

module.exports = {
  newData,
  listGroupSettlement,
  generateRemittance,
  sendReportEmail,
  sellerGraphData,
  getSettledCsv,
  fixGrpSettlement
}
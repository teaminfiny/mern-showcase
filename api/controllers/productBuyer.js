const async = require('async');
const nodemailer = require('nodemailer');
const randomstring = require('randomstring');
const moment = require("moment")
const mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

const userModel = require('../models/user');
const Order = require('../models/order');
const notification = require('../models/notification');
const helper = require('../lib/helper');

var Inventory = require('../models/inventory');
var settlement = require('../models/settlement')

var promotionsModel = require('../models/promotions');
var ActiveInventory = require('../models/activeInventory');

/*
# parameters: 
# purpose: to get buyer home page data (featured, deals of the day, visited and fast moving)
*/
const v1featuredProductList = async (req, res) => {

    let bannerArr = await promotionsModel.find({ isActive: true })
        .then(result => result)
        .catch(error => {
            console.log(error);
        });
        
    let nameArr =[];
    let featured = []
    await ActiveInventory.find({'medi_attribute':'Featured'})
        .limit(100)
        .then(result => {
            result.map(eachInv => {
                if(featured.length < 25){
                    let name = eachInv.Product_Category.name;
                    if(! nameArr.includes(name)){
                        nameArr.push(name);
                        featured.push({
                            _id: eachInv.Product_Category._id,
                            name: name,
                            data: [eachInv]
                        });
                    }
                    else{
                        let l = featured.length;
                        for(let i = 0; i < l; i++){
                            if(featured[i].name == name && featured[i].data.length < 4){
                                featured[i].data.push(eachInv);
                            }
                        }
                    }
                }
            });
        });
    
    let dealsArr = await ActiveInventory.find({'medi_attribute':'Deals Of Day'})
        .limit(25)
        .then(result => result);
    
    let fastMovingArr = await ActiveInventory.find({'medi_attribute':'Fast Moving'})
        .limit(10)
        .then(result => result);

    let jumboArr = await ActiveInventory.find({'medi_attribute':'Jumbo Deal'}).sort({updatedAt: -1})
        .limit(20)
        .then(result => result);
    console.log('featured: ', featured.length, ', deals: ',dealsArr.length, ', fast: ', fastMovingArr.length,
    'jumbo', jumboArr.length);

    return res.status(200).json({
        error: false,
        bannerArr: bannerArr,
        featured: featured,
        dealsArr: dealsArr,
        fastMovingArr: fastMovingArr,
        jumboArr: jumboArr
    })
}

/*
# parameters: inventory_id
# purpose: to get inventory details
*/

const v1productDetails = async(req, res) => {
    await ActiveInventory.findOne({ inventory_id: req.body.inventory_id})
        .then(async(resultData) => {

            if (!resultData) {
                return res.status(200).json({
                    title: 'No inventory found',
                    error: true
                });
            }

            let productData = [resultData];

            let otherSellers = []
            await ActiveInventory.aggregate([
                    {
                        $match: {
                            "Product._id": ObjectId(resultData.Product._id),
                            "Seller._id": { $ne: ObjectId(resultData.Seller._id) }
                        }
                    }
                ])
                .then(result => {
                    result.map(eachInv => {
                        if(eachInv != undefined)
                        otherSellers.push(eachInv)
                    })
                })
                .catch(error => console.log(error));

            let relatedProducts = [];
            let start = new Date(moment().subtract(30,'days').format('YYYY-MM-DD'));
            let end = new Date(moment().format('YYYY-MM-DD'));
            await Order.aggregate([
                { 
                    $match: {$and: [{ "createdAt": { $gte: start } }, { "createdAt": { $lte: end } }] } 
                },
                {
                    $project: {
                        products: 1,
                        total_amount: 1,
                        month: 1, year: 1,
                        data: {
                            $filter: {
                                input: "$order_status",
                                as: 'item',
                                cond: { $eq: ["$$item.status", "New"] }
                            }
                        }
                    }
                },
                {
                    $addFields: {
                        Delivered: { $size: "$data" }
                    }
                },
                {
                    $match: {
                        Delivered: { $gte: 1 }
                    }
                },
                {
                    $unwind: "$products"
                },
                {
                    $group: {
                        _id: "$products.inventory_id",
                        total_amt: { $sum: { $toDouble: "$total_amount" } },
                    }
                },
                {
                    $sort: { total_amt: -1 }
                },
                {
                    $lookup: {
                        localField: "_id",
                        foreignField: "inventory_id",
                        from: "active_inventories",
                        as: "Inventory"
                    }
                },
                { $match: { "Inventory.Product_Category._id": ObjectId(resultData.Product_Category._id) } },
                { $limit: 6 }
                ])
                .then(result => {
                    result.map(eachInv => {
                        if(eachInv.Inventory[0] != undefined)
                        relatedProducts.push(eachInv.Inventory[0])
                    })
                })
                .catch(error => console.log(error));
            
            console.log('relatedProducts',relatedProducts.length)
            console.log('Other sellers', otherSellers.length)
            return res.status(200).json({
                title: 'Inventory Fetched successfully.',
                error: false,
                productData: productData,
                otherSellers: otherSellers,
                relatedProducts: relatedProducts
            });
        })
        .catch(error => console.log(error));



}

const v1productForYou = (req, res) => {
    let userData = req.user;
    ActiveInventory.find({ "Product_Category._id": { $in: userData.visited_category } } )
        .limit(10)
        .then((result) => {
            return res.status(200).json({
                title: 'Product Fetched successfully.',
                error: false,
                visitedProduct: result
            });
        })
}

const featuredProductList = async (req, res) => {
    let date = moment();
    let dateAfter3Months = moment().add(3, "M");
    let dateAfterNineMonth = moment().add(9, "M");
    let previousDay = moment().subtract(1, 'days').set({ 'hour': 0, 'minute': 0, 'second': 0 });
    let currentDate = moment().set({ 'hour': 0, 'minute': 0, 'second': 0 }); // current with hour, min, sec set to zero 

    let bannerArr = await promotionsModel.find({ isActive: true })
        .then(result => result)

    let featured = await Inventory.aggregate([
        {
            $match: {
                $and: [
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } }
                ]
            }
        },
        {
            $lookup: {
                localField: "_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
        { $match: { "ProductCategory.isFeatured": true } },
        {
            $group: {
                _id: "$ProductCategory._id",
                "name": { $addToSet: "$ProductCategory.name" },
                "data": { $push: "$$ROOT" },
            }
        },
        {
            $project: {
                data: { $slice: ["$data", 4] },
                name: "$name"
            }
        }
        ,
        { $unwind: "$name" },
        { $limit: 25 }
    ]).allowDiskUse(true)
        .then(result => result);

    let dealsArr = await Inventory.aggregate([
        {
            $match: {
                $and: [
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } },
                    { isDealsOfTheDay: true }
                ]
            }
        },
        {
            $lookup: {
                localField: "_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "Product.company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    // { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        { $limit: 25 }
    ]).allowDiskUse(true)
        .then(result => result)

    let fastInventoryArr = await Order.aggregate([
        {
            $match: {
                $and: [
                    { "order_status.status": { $ne: "Cancelled" } },
                    {
                        "createdAt": {
                            $gte: new Date(previousDay),
                            $lte: new Date(currentDate),
                        }
                    },
                    { "is_deleted": false }
                ]
            }
        },
        {
            $lookup: {
                localField: "products.inventory_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $group: {
                _id: "$Inventory._id",
                "total_amt": { $sum: { $toDouble: "$total_amount" } }
            }
        }, {
            $sort: { total_amt: -1 }
        },
        { $limit : 25 }
    ]).allowDiskUse(true)
        .then(result => result)
    let orderIdArray = await Array.from(fastInventoryArr, x => ObjectId(x._id))
    let fastMovingArr = orderIdArray.length > 0 ? await Inventory.aggregate([
        {
            $addFields: { isId: { $in: ['$_id', orderIdArray] } }

        },
        {
            $match: { isId: true }
        },
        {
            $match: {
                $and: [
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } },
                ]
            }
        },
        {
            $lookup: {
                localField: "_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "Product.company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    // { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        { $limit: 10 }
    ]).allowDiskUse(true)
        .then((result) => result)
        : []


    return res.status(200).json({
        error: false,
        bannerArr: bannerArr,
        featured: featured,
        dealsArr: dealsArr,
        fastMovingArr: fastMovingArr
    })
}

const productDetails = (req, res) => {
    let userData = req.user;
    let userToken = req.headers.token

    let date = moment()
    let dateAfter3Months = moment().add(3, "M");
    Inventory.findOne({ _id: req.body.inventory_id, expiry_date: { $gt: new Date(dateAfter3Months) }, quantity: { $gt: 0 }, isDeleted: false, isHidden: false })
        .populate('product_id')
        .populate({
            path: 'user_id',
            match: {
                is_deleted: false,
                user_status: 'active'
            }
        })
        .populate({
            path: 'product_id',
            populate: {
                path: 'product_cat_id',
                model: 'product_category'
            }
        })
        .exec(async (error, resultData) => {

            if (error) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error
                });
            }
            if (!resultData) {
                return res.status(200).json({
                    title: 'No inventory found',
                    error: true
                });
            }

            let productData = await Inventory
                .aggregate([
                    {
                        $match: { _id: ObjectId(req.body.inventory_id) }
                    },
                    {
                        $match: {
                            $and: [
                                { quantity: { $gt: 0 } },
                                { isDeleted: false },
                                { isHidden: false },
                                { expiry_date: { $gt: new Date(dateAfter3Months) } },
                            ]
                        }
                    },
                    {
                        $lookup: {
                            localField: "discount_id",
                            foreignField: "_id",
                            from: "discounts",
                            as: "Discount"
                        }
                    },
                    {
                        $unwind: {
                            path: '$Discount',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            localField: 'Discount.discount_on_product.inventory_id',
                            foreignField: '_id',
                            from: 'inventories',
                            as: 'DiscountInventories'
                        }
                    },
                    {
                        $unwind: {
                            path: '$DiscountInventories',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            localField: 'DiscountInventories.product_id',
                            foreignField: '_id',
                            from: 'products',
                            as: 'OtherProducts'
                        }
                    },
                    {
                        $unwind: {
                            path: '$OtherProducts',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            localField: "user_id",
                            foreignField: "_id",
                            from: "users",
                            as: "Seller"
                        }
                    },
                    {
                        $unwind: {
                            path: '$Seller',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $match: {
                            $and: [
                                { "Seller.is_deleted": false },
                                { "Seller.user_status": "active" },
                                { "Seller.vaccation.vaccationMode": false },
                            ],
                            $or: [
                                { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                            ],
                            $or: [
                                { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                            ]
                        }
                    },
                    {
                        $lookup: {
                            localField: "Seller.user_state",
                            foreignField: "_id",
                            from: "countries",
                            as: "State"
                        }
                    },
                    { $unwind: "$State" },
                    {
                        $lookup: {
                            localField: "product_id",
                            foreignField: "_id",
                            from: "products",
                            as: "Product"
                        }
                    },
                    { $unwind: "$Product" },
                    {
                        $lookup: {
                            localField: "Product.company_id",
                            foreignField: "_id",
                            from: "companies",
                            as: "Company"
                        }
                    },
                    { $unwind: "$Company" },
                    {
                        $lookup: {
                            localField: "Product.GST",
                            foreignField: "_id",
                            from: "gsts",
                            as: "GST"
                        }
                    },
                    { $unwind: "$GST" },
                    {
                        $lookup: {
                            localField: "Product.Type",
                            foreignField: "_id",
                            from: "producttypes",
                            as: "Type"
                        }
                    },
                    { $unwind: "$Type" },
                    {
                        $lookup: {
                            localField: "Product.product_cat_id",
                            foreignField: "_id",
                            from: "product_categories",
                            as: "ProductCategory"
                        }
                    },
                    { $unwind: "$ProductCategory" }
                ])
                .then(result => result)

            let otherSellers = await Inventory
                .aggregate([
                    {
                        $match: {
                            $and: [
                                { quantity: { $gt: 0 } },
                                { isDeleted: false },
                                { isHidden: false },
                                { expiry_date: { $gt: new Date(dateAfter3Months) } },
                            ]
                        }
                    },
                    {
                        $lookup: {
                            localField: "_id",
                            foreignField: "_id",
                            from: "inventories",
                            as: "Inventory"
                        }
                    },
                    { $unwind: "$Inventory" },
                    {
                        $lookup: {
                            localField: "Inventory.product_id",
                            foreignField: "_id",
                            from: "products",
                            as: "Product"
                        }
                    },
                    { $unwind: "$Product" },
                    {
                        $lookup: {
                            localField: "Product.company_id",
                            foreignField: "_id",
                            from: "companies",
                            as: "Company"
                        }
                    },
                    {
                        $match: {
                            "Product._id": ObjectId(resultData.product_id._id),
                            "user_id": { $ne: ObjectId(resultData.user_id._id) }
                        }
                    },
                    { $unwind: "$Company" },
                    {
                        $lookup: {
                            localField: "Product.GST",
                            foreignField: "_id",
                            from: "gsts",
                            as: "GST"
                        }
                    },
                    { $unwind: "$GST" },
                    {
                        $lookup: {
                            localField: "Product.product_cat_id",
                            foreignField: "_id",
                            from: "product_categories",
                            as: "ProductCategory"
                        }
                    },
                    { $unwind: "$ProductCategory" },
                    {
                        $lookup: {
                            localField: "Inventory.discount_id",
                            foreignField: "_id",
                            from: "discounts",
                            as: "Discount"
                        }
                    },
                    {
                        $lookup: {
                            localField: "Inventory.user_id",
                            foreignField: "_id",
                            from: "users",
                            as: "Seller"
                        }
                    },
                    { $unwind: "$Seller" },
                    {
                        $match: {
                            $and: [
                                { "Seller.is_deleted": false },
                                { "Seller.user_status": "active" },
                                // { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                                { "Seller.vaccation.vaccationMode": false },
                            ],
                            $or: [
                                { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                            ],
                            $or: [
                                { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                            ]
                        }
                    },
                    {
                        $unwind: {
                            path: '$Discount',
                            preserveNullAndEmptyArrays: true
                        }
                    }, {
                        $lookup: {
                            localField: 'Discount.discount_on_product.inventory_id',
                            foreignField: '_id',
                            from: 'inventories',
                            as: 'DiscountInventories'
                        }
                    },
                    {
                        $unwind: {
                            path: '$DiscountInventories',
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $lookup: {
                            localField: 'DiscountInventories.product_id',
                            foreignField: '_id',
                            from: 'products',
                            as: 'OtherProducts'
                        }
                    },
                    {
                        $unwind: {
                            path: '$OtherProducts',
                            preserveNullAndEmptyArrays: true
                        }
                    },

                    { $limit: 10 }
                ])
                .then(result => result)

            let relatedProducts = await Order
                .aggregate([{
                    $project: {
                        products: 1,
                        total_amount: 1,
                        month: 1, year: 1,
                        data: {
                            $filter: {
                                input: "$order_status",
                                as: 'item',
                                cond: { $eq: ["$$item.status", "New"] }
                            }
                        }
                    }
                },
                {
                    $addFields: {
                        Delivered: { $size: "$data" }
                    }
                },
                {
                    $match: {
                        Delivered: { $gte: 1 }
                    }
                },
                {
                    $unwind: "$products"
                },
                {
                    $group: {
                        _id: "$products.inventory_id",
                        total_amt: { $sum: { $toDouble: "$total_amount" } },
                    }
                },
                {
                    $sort: { total_amt: -1 }
                },
                {
                    $lookup: {
                        localField: "_id",
                        foreignField: "_id",
                        from: "inventories",
                        as: "Inventory"
                    }
                },
                { $unwind: "$Inventory" },
                {
                    $match: {
                        $and: [
                            { 'Inventory.quantity': { $gt: 0 } },
                            { 'Inventory.isDeleted': false },
                            { 'Inventory.isHidden': false },
                            { 'Inventory.expiry_date': { $gt: new Date(dateAfter3Months) } },
                        ]
                    }
                },
                {
                    $lookup: {
                        localField: "Inventory.product_id",
                        foreignField: "_id",
                        from: "products",
                        as: "Product"
                    }
                },
                { $unwind: "$Product" },
                {
                    $lookup: {
                        localField: "Inventory.user_id",
                        foreignField: "_id",
                        from: "users",
                        as: "Seller"
                    }
                },
                { $unwind: "$Seller" },
                {
                    $match: {
                        $and: [
                            { "Seller.is_deleted": false },
                            { "Seller.user_status": "active" },
                            // { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                            { "Seller.vaccation.vaccationMode": false },
                        ],
                        $or: [
                            { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                        ],
                        $or: [
                            { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                        ]
                    }
                },
                {
                    $lookup: {
                        localField: "Product.company_id",
                        foreignField: "_id",
                        from: "companies",
                        as: "Company"
                    }
                },
                { $unwind: "$Company" },
                {
                    $lookup: {
                        localField: "Product.product_cat_id",
                        foreignField: "_id",
                        from: "product_categories",
                        as: "ProductCategory"
                    }
                },
                { $unwind: "$ProductCategory" },
                { $match: { "ProductCategory._id": ObjectId(resultData.product_id.product_cat_id._id) } },
                {
                    $lookup: {
                        localField: "Product.GST",
                        foreignField: "_id",
                        from: "gsts",
                        as: "GST"
                    }
                },
                { $unwind: "$GST" },
                {
                    $lookup: {
                        localField: "Product.product_cat_id",
                        foreignField: "_id",
                        from: "product_categories",
                        as: "ProductCategory"
                    }
                },
                { $unwind: "$ProductCategory" },
                {
                    $lookup: {
                        localField: "Inventory.discount_id",
                        foreignField: "_id",
                        from: "discounts",
                        as: "Discount"
                    }
                },
                {
                    $unwind: {
                        path: '$Discount',
                        preserveNullAndEmptyArrays: true
                    }
                },
                // { $unwind: "$Discount" },
                { $limit: 6 }
                ])
                .then(result => result)

            return res.status(200).json({
                title: 'Inventory Fetched successfully.',
                error: false,
                productData: productData,
                otherSellers: otherSellers,
                relatedProducts: relatedProducts
            });
        });



}

const productForYou = (req, res) => {
    let userData = req.user;
    let date = moment();
    let dateAfter3Months = moment().add(3, "M");
    Inventory.aggregate([
        {
            $match: {
                $and: [
                    { quantity: { $gt: 0 } },
                    {$expr: {$gte: ["$quantity", "$min_order_quantity"]}},
                    { isDeleted: false },
                    { isHidden: false },
                    { expiry_date: { $gt: new Date(dateAfter3Months) } }
                ]
            }
        },
        {
            $lookup: {
                localField: "_id",
                foreignField: "_id",
                from: "inventories",
                as: "Inventory"
            }
        },
        { $unwind: "$Inventory" },
        {
            $lookup: {
                localField: "discount_id",
                foreignField: "_id",
                from: "discounts",
                as: "Discount"
            }
        },
        {
            $unwind: {
                path: '$Discount',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'Discount.discount_on_product.inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'DiscountInventories'
            }
        },
        {
            $unwind: {
                path: '$DiscountInventories',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: 'DiscountInventories.product_id',
                foreignField: '_id',
                from: 'products',
                as: 'OtherProducts'
            }
        },
        {
            $unwind: {
                path: '$OtherProducts',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                localField: "product_id",
                foreignField: "_id",
                from: "products",
                as: "Product"
            }
        },
        { $unwind: "$Product" },
        { $match: { "Product.isDeleted": false } },
        {
            $lookup: {
                localField: "Product.company_id",
                foreignField: "_id",
                from: "companies",
                as: "Company"
            }
        },
        { $unwind: "$Company" },
        {
            $lookup: {
                localField: "Product.GST",
                foreignField: "_id",
                from: "gsts",
                as: "GST"
            }
        },
        { $unwind: "$GST" },
        {
            $lookup: {
                localField: "Product.Type",
                foreignField: "_id",
                from: "producttypes",
                as: "Type"
            }
        },
        { $unwind: "$Type" },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        { $unwind: "$Seller" },
        {
            $match: {
                $and: [
                    { "Seller.is_deleted": false },
                    { "Seller.user_status": "active" },
                    { "Seller.vaccation.vaccationMode": false },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $lt: new Date(date) } },
                ],
                $or: [
                    { "Seller.vaccation.tillDate": { $ne: 'undefined' } },
                ]
            }
        },
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
        { $match: { "ProductCategory._id": { $in: userData.visited_category } } },
        {
            $group: {
                _id: "$Product._id",
                "name": { $addToSet: "$Product.name" },
                "data": { $push: "$$ROOT" },
            }
        },
        {
            $project: {
                name: "$name",
                data: { $slice: ["$data", 1] }
            }
        }
        ,
        { $unwind: "$name" },
        { $unwind: "$data" },
        { $limit: 10 }
    ]).allowDiskUse(true)
        .then((result) => {
            return res.status(200).json({
                title: 'Product Fetched successfully.',
                error: false,
                visitedProduct: result
            });
        })
}

module.exports = {
    featuredProductList,
    v1featuredProductList,
    productDetails,
    v1productDetails,
    productForYou,
    v1productForYou
}
const mongoose = require('mongoose');
const BuyerRequest = require('../models/buyerRequest');
const ObjectId = mongoose.Types.ObjectId;
const notification = require('../models/notification');
const user = require('../models/user');
const helper = require('../lib/helper');
const sellerController = require('./seller');
const async = require('async');
const moment = require('moment');
const ActiveInventory = require('../models/activeInventory')
var Product = require('../models/product')
const ShortBook = require('../models/shortBook');

const escapeRegex = (string) => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

const listBuyerRequest = async(req, res) => {
    if(req.user.user_type === 'buyer'){
        await BuyerRequest.find({user_id:ObjectId(req.user._id), isFulfilled:true, isDeleted:false}).then(result =>{
            if(result){
                async.eachOfSeries(result,async(eachRequest, key, cb)=>{
                    await ActiveInventory.findOne({'Product.name': { $regex: new RegExp('^' + eachRequest.product_name + '$'), $options: 'i' }}).then(async(result1) =>{
                        if(result1){
                            await BuyerRequest.updateOne({_id:ObjectId(eachRequest._id)},{$set:{inStock:true}}).exec();
                        }
                        else{
                            await BuyerRequest.updateOne({_id:ObjectId(eachRequest._id)},{$set:{inStock:false}}).exec();
                        }
                    })
                },function(){})
            }
        })
        let query = []
        if(req.body.tab && req.body.tab === 'history'){
            query.push(
                {
                    $match: {
                        user_id: ObjectId(req.user._id),
                        isDeleted: false,
                        $and : [
                            {status: {$ne : 'PENDING'}},
                            {status:{$ne:'CANCELLED'}}
                        ]
                    }
                }
            )
        }
        else{
            query.push(
                {
                    $match: {
                        user_id: ObjectId(req.user._id),
                        isDeleted: false,
                        status: {$eq : 'PENDING'}
                    }
                }
            )
        }
        if(req.body.searchText && req.body.searchText !== undefined && req.body.searchText !== ''){
            let keyValue = escapeRegex(req.body.searchText)
            query.push(
                {
                    $match: {
                        product_name : { $regex: new RegExp(keyValue, 'i') } 
                    }
                }
            )
        }
        if(req.body.month && req.body.year){
            query.push({
                $addFields: {
                    year: { $year: "$createdAt" },
                    month: { $month: "$createdAt" }
                }
            },
            {
                $match: {
                    year: Number(req.body.year),
                    month: sellerController.getMonthNumber(req.body.month)
                }
            })
        }
        query.push(
            {
                $lookup: {
                    localField: 'user_id',
                    foreignField: '_id',
                    from: 'users',
                    as: 'user_id'
                }
            },
            {
                $lookup: {
                    localField: 'manufacturer',
                    foreignField: '_id',
                    from: 'companies',
                    as: 'manufacturer'
                }
            },
            {
                $unwind: {
                    path: '$user_id',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$manufacturer',
                    preserveNullAndEmptyArrays: true
                }
            },
            { $sort: { createdAt: -1 } },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                    result: [{ $skip: req.body.perPage ? (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) : 0 }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
                }
            }
        )
        BuyerRequest.aggregate(query)
        .then(result => {
            if(result){
                res.status(200).json({
                    error: false,
                    title: "Data fetched successfully",
                    data: result[0]
                })
            }
            else{
                res.status(200).json({
                    error: true,
                    title: "something went wrong",
                })
            }
        })
    }
    else if(req.user.user_type === 'admin'){
        let firstDay = req.body.from_date ? new Date(moment(req.body.from_date).format('YYYY-MM-DD')) : new Date(moment().subtract(1, 'months').format('YYYY-MM-DD'))
        let lastDay = req.body.to_date ? new Date(moment(req.body.to_date).add(1, 'days').format('YYYY-MM-DD')) : new Date(moment().add(1, 'days').format('YYYY-MM-DD'))
        firstDay.setHours(firstDay.getHours()-5);
        firstDay.setMinutes(firstDay.getMinutes()-30);
        lastDay.setHours(lastDay.getHours()-5);
        lastDay.setMinutes(lastDay.getMinutes()-30);
        let matchQuery;
        if(req.body.searchText && req.body.searchText !== undefined && req.body.searchText !== ''){
            let keyValue = escapeRegex(req.body.searchText);
            matchQuery = {$and:[
                {isDeleted:false},
                {isFulfilled:false},
                {status:{$ne:'CANCELLED'}},
                {createdAt: { $gte: firstDay }} ,
                {createdAt: { $lt: lastDay } },
                {'product_name': { $regex: new RegExp(keyValue, 'i') } }
            ]}
        }else{
            matchQuery = {$and:[
                {isDeleted:false},
                {isFulfilled:false},
                {status:{$ne:'CANCELLED'}},
                {createdAt: { $gte: firstDay }} ,
                {createdAt: { $lt: lastDay } }
            ]}
        }
        if(req.body.buyer && req.body.buyer != ''){
            matchQuery['user_id'] = ObjectId(req.body.buyer)
        } 
        BuyerRequest.aggregate([
            {
                $match: matchQuery
            },
            {
                $lookup: {
                    localField: 'user_id',
                    foreignField: '_id',
                    from: 'users',
                    as: 'user_id'
                }
            },
            {
                $unwind: {
                    path: '$user_id',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'manufacturer',
                    foreignField: '_id',
                    from: 'companies',
                    as: 'manufacturer'
                }
            },
            {
                $unwind: {
                    path: '$manufacturer',
                    preserveNullAndEmptyArrays: true
                }
            },
            { $sort: { createdAt: -1 } },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                    data: [{ $skip: req.body.perPage ? (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) : 0 }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
                }
            }
        ])
        .then(result => {
            if(result){
                res.status(200).json({
                    error: false,
                    title: "Data fetched successfully",
                    data: result
                })
            }
            else{
                res.status(200).json({
                    error: true,
                    title: "something went wrong",
                })
            }
        })
    }
    else{
        res.status(200).json({
            error: true,
            title: "something went wrong",
        })
    }
}

const createBuyerRequest = async(req, res) => {
    req.checkBody('product', 'Product name is required').notEmpty();
    req.checkBody('manufacturer', 'Manufacturer name is required').notEmpty();
    req.checkBody('quantity', 'Quantity is required').notEmpty();
    let errors = req.validationErrors();
    let keyValue = escapeRegex(req.body.product)
    let found = await BuyerRequest.findOne({product_name: { $regex: `^${keyValue}$`,$options: 'i' }, user_id: ObjectId(req.user._id), isDeleted:false, $and:[{status:{$ne:'CANCELLED'}}, {status:{$ne:'DENIED'}}]}).then(result => {
        if(result){
            return result;
        }
    })
    let foundProduct = await Product.findOne({name: { $regex: `^${keyValue}$`,$options: 'i' }, isDeleted:false}).then(result => {
        if(result){
            return result;
        }
    })
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    }
    else if(found && found != ''){
        return res.status(200).json({
            error: true,
            title: 'Product Request Already Exists !',
            errors: errors
        });
    }
    else if(foundProduct && foundProduct != ''){
        return res.status(200).json({
            error: true,
            title: 'This Product Is Already In Stock !',
            errors: errors
        });
    }
    else {
        let {product,manufacturer,quantity,otherManufacturer} = req.body;
        let buyerRequest = new BuyerRequest({
            product_name: product.toUpperCase(),
            user_id: ObjectId(req.user._id),
            manufacturer: manufacturer === 'others' ? null : ObjectId(manufacturer),
            otherManufacturer: manufacturer === 'others' ? otherManufacturer.toUpperCase() : '',
            quantity: Number(quantity)
        });
        buyerRequest.save(async(err,savedData) => {
            if(err){
                return res.status(200).json({
                    error:true,
                    title: 'Something went wrong'
                })
            }
            else if(savedData){
                await user.findUserById(ObjectId(req.user._id), async(error, userDetail) => {
                    let flag = 'Product Request Generated'
                    let msg = `Your product request for product ${product.toUpperCase()} is generated successfully !` 
                    await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
                    if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                        let devicetoken = userDetail.device_token;
                        let title = 'Product Request Generated'
                        let payload = {};
                        await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                    }
                    let emailBody =`Your product request for product ${product.toUpperCase()} is generated successfully ! We will inform you after admin approve/deny your request.` 
                    var mailData = {
                        email: userDetail.email,
                        subject: 'Product Request Generated',
                        body: emailBody
                    };
                    await helper.sendEmail(mailData);
                })
                return res.status(200).json({
                    error:false,
                    title: 'Product Request Generated Successfully !',
                    data: savedData
                })
            }
        })
    }
}

const updateBuyerRequest = async(req, res) => {
    let {id,status} = req.body;
    let foundReq = await BuyerRequest.findOne({_id: ObjectId(id)}).then(result => {
        if(result){
            return result.product_name;
        }
    })
    let keyValue = escapeRegex(foundReq)
    let prodId;
    let foundProduct = await Product.findOne({name: { $regex: `^${keyValue}$`,$options: 'i' }, isDeleted:false}).then(result => {
        if(result && result !== ''){
            prodId = result._id;
            return true;
        }
        else{
            return false
        }
    })
    if(status === 'APPROVED' && !foundProduct){
        return res.status(200).json({
            error:true,
            title:"Please Add This Product In The Product Bank First !"
        })
    }
    else{
        BuyerRequest.findOneAndUpdate({_id:ObjectId(id), isDeleted:false},{$set:{status:status}})
        .then(async(updatedData) =>{
            if(updatedData){
                if(status === 'APPROVED'){
                    let product = await BuyerRequest.findOne({_id: ObjectId(id)}).then(result => result.product_name)
                    user.find({user_type:'seller', user_status:'active', is_deleted:false, 'vaccation.vaccationMode' : false, productNotification:true}).then((result) => {
                        if (result.length > 0) {
                            let flag = 'Product Notification';
                            let msg = `${product.toUpperCase()} is now available to sell on Medimny. You can add inventory to sell this product.`;
                            let title = 'New Product Available.';
                            let payload = {};
                            async.eachOfSeries(result, async function (data, key, cb) {
                                let devicetoken = data.device_token;
                                await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                                let data1 = JSON.parse(JSON.stringify(data))
                                data1.seller_id = data._id
                                await notification.addNotification(msg, '',data1, flag, (error, response) => {}) 
                                var mailData = {
                                    email: data.email,
                                    subject: title,
                                    body: msg
                                };
                                await helper.sendEmail(mailData);
                                cb();
                            }, function (err) {
                            })
                        } 
                    })
                    //add to shortbook 
                    let shortBook = new ShortBook({
                        product_name: updatedData.product_name.toUpperCase(),
                        user_id: ObjectId(updatedData.user_id),
                        product_id: ObjectId(prodId),
                        quantity: Number(updatedData.quantity)
                    });
                    shortBook.save();
                }
                return res.status(200).json({
                    error:false,
                    title:`Product Request ${status} successfully !`,
                    data: updatedData
                })
            }
            else{
                return res.status(200).json({
                    error:true,
                    title:"Something went wrong"
                })

            }
        })
    }
}

const deleteBuyerRequest = (req, res) => {
    BuyerRequest.findOneAndUpdate({_id:ObjectId(req.body.id), isDeleted:false},{$set:{isDeleted: true}})
    .then(updatedData =>{
        if(updatedData){
            return res.status(200).json({
                error:false,
                title:"Buyer request deleted successfully",
                data: updatedData
            })
        }
        else{
            return res.status(200).json({
                error:true,
                title:"Something went wrong"
            })

        }
    })
}

const checkFulfilledRequest = (product, manufacturer, name) => {
    BuyerRequest.find({product_name: { $regex: new RegExp('^' + product + '$'), $options: 'i' }, $or:[{manufacturer: ObjectId(manufacturer)}, {otherManufacturer:{ $regex: new RegExp('^' + name + '$'), $options: 'i' }}], isFulfilled:false, isDeleted:false})
        .populate('manufacturer')
        .then(result => {
            console.log('checkFulfilledRequest',result)
            if(result){
                async.eachOfSeries(result, async function(eachRequest, key, callback){
                    if(eachRequest){
                        await BuyerRequest.findOneAndUpdate({_id: ObjectId(eachRequest._id)},{$set:{isFulfilled:true, inStock:true, status:'APPROVED'}})
                            .then(updated =>{console.log('updated id', updated._id, updated.isFulfilled)})
                            .catch(err =>{console.log(err)})
                        await user.findUserById(eachRequest.user_id, async(error, userDetail) => {
                            let flag = 'Product Request Fulfilled'
                            let msg = `The product you have requested with name ${product.toUpperCase()} and manufacturer ${eachRequest.manufacturer && eachRequest.manufacturer.name ? eachRequest.manufacturer.name.toUpperCase() : eachRequest.otherManufacturer.toUpperCase()} is available now.` 
                            await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
                            if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                                let devicetoken = userDetail.device_token;
                                let title = 'Product Request Fulfilled'
                                let payload = {};
                                await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                            }
                            let emailBody = '<p>' + `The product you have requested with name ${product.toUpperCase()} and manufacturer ${eachRequest.manufacturer && eachRequest.manufacturer.name ? eachRequest.manufacturer.name.toUpperCase() : eachRequest.otherManufacturer.toUpperCase()} is available now. Visit medimny to shop now !` 
                            var mailData = {
                                email: userDetail.email,
                                subject: 'Product Request Fulfilled',
                                body: emailBody
                            };
                            await helper.sendEmail(mailData);
                        })
                    }
                    else{
                        callback()
                    }
                }, function(){
                });
            }
        })
}

const notifyUser = async(req, res) =>{
    const {buyer_id, message} = req.body
    await user.findUserById(ObjectId(buyer_id), async(error, userDetail) => {
        if(userDetail && userDetail !== '' && userDetail !== {}){
            let flag = 'Product Request Notification'
            let msg = message 
            await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
            if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                let devicetoken = userDetail.device_token;
                let title = 'Product Request Notification'
                let payload = {};
                await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
            }
            var mailData = {
                email: userDetail.email,
                subject: 'Product Request Notification',
                body: message
            };
            await helper.sendEmail(mailData);
            return res.status(200).json({
                error:false,
                title: 'User notified successfully !',
            })
        }
        else{
            return res.status(200).json({
                error:true,
                title: 'Something went wrong !',
            })
        }
    })
}

const changeRequestStatus = async(req, res) =>{
    await BuyerRequest.updateMany({status:"ACTIVE"}, {$set:{status:"PENDING"}}).exec();
    await BuyerRequest.updateMany({isDeleted:false},{$set:{inStock:false}}).exec();
    res.status(200).json({
        title:'success'
    });
}

module.exports = {
    listBuyerRequest,
    createBuyerRequest,
    updateBuyerRequest,
    deleteBuyerRequest,
    checkFulfilledRequest,
    notifyUser,
    changeRequestStatus
}
var productModal = require('../models/product');
/*
# parameters: token,
# purpose: Listing all products from Medicine Bank.
*/
const listingMedicineBank = (req, res) => {
    let user = req.user;
    
}

/*
# parameters: token,
# purpose: Add product in medicine bank
*/
const addProduct = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: Edit product in medicine bank
*/
const editProduct = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: Edit product from medicine bank
*/
const removeProduct = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: Approve product added by seller.
*/
const approveProduct = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: Get all products list on search
*/
const getAllProducts = (req, res) => {
    let user = req.user;
    productModal.find({name: { $regex: '.*' + req.body.name + '.*' } }).exec((error, resultData) => {  
        if (error) {
            return res.status(200).json({
              title: "Error occured",
              error: true,
              details: err
            })
        }else{
            return res.status(200).json({
                title: 'Product Details.',
                error: false,
                details: resultData
            });
        }

    })
}


module.exports = {
    listingMedicineBank,
    addProduct,
    editProduct,
    removeProduct,
    approveProduct,
    getAllProducts
}
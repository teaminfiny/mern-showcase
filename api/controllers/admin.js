const async = require('async');
const request = require("request");
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

const user = require('../models/user');
const Country = require('../models/country');
var Orders = require('../models/order');
const settlement = require('../models/settlement');
const helper = require('../lib/helper');
var ProductCategory = require('../models/product_category')
var productType = require('../models/productType')
var gstModel = require('../models/gst')
var inventoryModel = require('../models/inventory')
var companyModel = require('../models/company')
var productModel = require('../models/product')
var notification = require('../models/notification')
var productRequest = require('../models/productRequest')
var zipCodeServing = require('../models/pincodeServing')
const transactions = require('../models/transactions')
const systemActivityCon = require('./systemActivity')
const systemActivity = require('../models/systemActivity')
const ActiveInventory = require('../models/activeInventory');
const { updateManyActiveInventory } = require('./activeInventory')
// const csv = require('fast-csv');
const fs = require('fs');
const path = require('path');
const csv = require('fast-csv');
const order = require('../models/order');
const moment = require('moment');

const bulkProduct = (req, res) => {
    fs.createReadStream(path.resolve(__dirname, 'assets', '../ProductBank.csv'))
        .pipe(csv.parse({ headers: true }))
        // pipe the parsed input into a csv formatter
        .pipe(csv.format({ headers: true }))
        .on('data', function (data) {  })
        // Using the transform function from the formatting stream
        .transform((row, next) => {
            let PRODUCT_NAME = row.PRODUCT_NAME.trim().toUpperCase();
            productModel.findOne({ "sku": PRODUCT_NAME }).exec((error, productBank) => {
                if (error) {
                    return true;
                }
                if (productBank === null) {
                    let image1 = PRODUCT_NAME.replace(/ /g, "_") + '/1.png';
                    let image2 = PRODUCT_NAME.replace(/ /g, "_") + '/2.png';

                    let ProductArray = new productModel({
                        name: PRODUCT_NAME,
                        chem_combination: row.SALT_COMPOSTION.trim(),
                        HSN: row.HSN_CODE.trim(),
                        pack_type: row.PRODUCT_PACKTYPE.trim(),
                        sku: PRODUCT_NAME,
                        images: Array(image1, image2)
                    });
                    async.waterfall([
                        function (callback) {
                            let company = row.COMPANY_NAME.trim();
                            companyModel.findOne({ name: { $regex: new RegExp(company, "i") } }).exec((error, companyData) => {
                                if (error) {
                                    callback(error);
                                }
                                if (companyData) {
                                    ProductArray.company_id = companyData._id;
                                    callback(null);
                                }
                                else {
                                    let companyArray = new companyModel({
                                        name: company
                                    })
                                    companyArray.save((error, newCompanyData) => {
                                        if (error) {
                                            callback(error);
                                        }
                                        ProductArray.company_id = newCompanyData._id;
                                        callback(null);
                                    })
                                }
                            });

                        },
                        function (callback) {

                            let Category = row.CATEGORY.trim();
                            ProductCategory.findOne({ name: { $regex: new RegExp(Category, "i") } }).exec((error, categoryData) => {
                                if (error) {
                                    callback(error);
                                }
                                if (categoryData) {
                                    ProductArray.product_cat_id = categoryData._id;
                                    callback(null);
                                }
                                else {
                                    let categoryArray = new ProductCategory({
                                        name: Category
                                    })
                                    categoryArray.save((error, newcategoryData) => {
                                        if (error) {
                                            callback(error);
                                        }
                                        ProductArray.product_cat_id = newcategoryData._id;
                                        callback(null);
                                    })
                                }
                            });
                        },
                        function (callback) {
                            let Type = row.TYPE.trim();
                            productType.findOne({ name: { $regex: new RegExp(Type, "i") } }).exec((error, productTypeData) => {
                                if (error) {
                                    callback(error);
                                }
                                if (productTypeData) {
                                    ProductArray.Type = productTypeData._id;
                                    callback(null);
                                }
                                else {
                                    let typeArray = new productType({
                                        name: Type
                                    })
                                    typeArray.save((error, newtypeData) => {
                                        if (error) {
                                            callback(error);
                                        }
                                        ProductArray.Type = newtypeData._id;
                                        callback(null);
                                    })
                                }


                            });

                        },
                        function (callback) {
                            let GST = row.GST.trim();
                            gstModel.findOne({ value: GST }).exec((error, gstData) => {
                                if (error) {
                                    callback(error);
                                }
                                if (gstData) {
                                    ProductArray.GST = gstData._id;
                                    callback(null);
                                }
                                else {
                                    callback('No gstData found');

                                }
                            });

                        }
                    ], function (err) {
                        if (err) {
                            return next();
                        } else {
                            ProductArray.save((error, productBank) => {
                                if (error) {
                                }
                                return next();
                            })
                        }
                    });

                }
                else {
                    return next();
                }
            });
        })
        .on('end', () => {
            return res.status(200).json({
                details: "data uploaded successfully",
                error: false,
            });
        });

}


/*
# parameters: token,
# purpose: to approve or ban or suspend user (seller or buyer).
# user is suspended till specific date
*/
const changeUserStatus = (req, res) => {
    //let user = req.user;
    user.findOne({ _id: req.body.id }).exec(async(error, userData) => {
        if (userData) {
            if (req.body.status == 'active' || req.body.status == 'blocked' || req.body.status == 'suspend' || req.body.status == 'denied') {
                if( req.body.status == 'blocked' || req.body.status == 'suspend' ){
                    await ActiveInventory.deleteMany({'Seller._id':ObjectId(req.body.id)}).exec()
                }
                userData.user_status = req.body.status;
                if (req.body.status == 'suspend') {
                    userData.suspend_date = req.body.date;
                }

                if (req.body.status == 'denied') {
                    userData.is_deleted = true;
                    userData.phone = userData.phone + "_denied";
                    userData.email = userData.email + "_denied";
                }

                userData.save(async(err) => {
                    if (err) {
                        return res.status(200).json({
                            title: "Something went wrong please try again.",
                            details: err,
                            error: true,
                        });
                    } else {
                        if (req.body.status == 'active' && userData.user_type == 'seller'){
                            await updateManyActiveInventory(req.body.id)
                        }
                        return res.status(200).json({
                            title: "User status changed successfuly.",
                            details: '',
                            error: false,
                        });
                    }
                });
            } else {
                return res.status(200).json({
                    title: "Invalid Status.",
                    details: "Something went wrong![code:227]",
                    error: true,
                });
            }
        } else {
            return res.status(200).json({
                title: "User not found.",
                details: error,
                error: true,
            });
        }
    });

}

/*
# parameters: token,
# purpose: Listing config data like creds of smtp, payment gatway, sms gatway etc..
*/
const listingConfigData = (req, res) => {
    let user = req.user;
}

/*
# parameters: token,
# purpose: Add and Edit config data
*/
const addConfigData = (req, res) => {
    let user = req.user;
}



/*
# parameters: token,
# purpose: Listing sellers Or Buyer
*/
const sellerBuyerListing = (req, res) => {
    let user = req.userData;
    user
        .find({ user_type: req.body.type })
        // .populate('awayTeam', 'city shortCode teamName')
        .limit(req.body.skip).skip(req.body.skip * req.body.page)
        .exec((error, resultData) => {
            if (error) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error
                });
            } else {
                return res.status(200).json({
                    title: 'Seller or Buyer Listing',
                    error: false,
                    detail: resultData
                });
            }
        });
}

/*
# parameters: token,
# purpose: Listing sellers
*/
const sellerBuyerDetails = (req, res) => {
    let user = req.userData;
    user
        .findOne({ _id: req.body.id, user_type: req.body.type })
        .populate('orders')
        .populate('transactions')
        .exec((error, resultData) => {
            if (error) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error
                });
            } else {
                return res.status(200).json({
                    title: 'Seller or Buyer Details',
                    error: false,
                    detail: resultData
                });
            }
        });
}

const addStates = (req, res) => {
    Country.addStates(req, res)
}

const getStates = (req, res) => {
    Country.getStates(req, res)
}


const createSettlements = (req, res) => {
    req.checkBody('order_id', 'Order Id is required').notEmpty();
    req.checkBody('seller_id', 'Seller Id is required').notEmpty();
    req.checkBody('invoice_id', 'Invoice Id is required').notEmpty();
    req.checkBody('amount', 'Amount is required').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let userData = req.user;
        let { order_id, seller_id, invoice_id, amount, refCode } = req.body;
        let newSettlement = new settlement({
            order_id: ObjectId(order_id),
            seller_id: ObjectId(seller_id),
            invoice_id,
            amount,
            refCode
        })
        newSettlement.save((error, settlements) => {
            if (error) {
                return res.status(200).json({
                    title: 'Some thing went wrong please try again.',
                    error: true,
                    detail: error
                });
            } else {
                return res.status(200).json({
                    title: 'Settlement create successfully.',
                    error: false,
                    detail: settlements
                });
            }
        })
    }
}
const sellerApproveFunc = (req,res,userData)=>{
    if (req.body.user_type == 'seller') {
        let msgBody = 'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY.'
        let mailData = {
            email: userData.email,
            subject: 'Documents verified',
            body:
                '<p>' +
                'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY.'
        };
        helper.sendEmail(mailData);
        helper.sendSMS(userData.phone, msgBody);

        let flag = 'Documents verified';
        let msg = 'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY';
        notification.addNotification(msg, '', userData , flag, (error, response) => {
        })
        
        if (userData && userData.device_token && userData.device_token.length > 0) {
            let devicetoken = userData.device_token;
            let flag = 'Document verified';
            let msg = 'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY.';
            let title = 'Document verified';
            let payload = {};
            helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
            });
        }
    }
    userData.approvedBy = req.user._id;
    userData.save().then((resultData) => {
        res.status(200).json({
            error: false,
            title: "User approved successfully",
        })
    })
}
const approve = (req, res) => {
    // If user is seller we have to enrolle users address into shiprocket.
    if (req.body.user_type == "seller") {
        helper.shipRokectLogin(async (loginResult) => {
            if (loginResult == undefined) {
                return res.status(200).json({
                    error: true,
                    title: "Shiprocket error"
                })
            }
            let shipToken = loginResult.data.token

            if (req.body.user_id == undefined || req.body.user_type == undefined) {
                return res.status(200).json({
                    error: true,
                    title: "Provide user_id and user_type"
                })
            }
            user.findOne({ _id: req.body.user_id, user_type: req.body.user_type }).populate('user_state').then((userData) => {
                if (!userData) {
                    return res.status(200).json({
                        error: false,
                        title: "User not found"
                    })
                }
                userData.isAllPermission = true;
                    let data = {
                        pickup_location: userData.sellerId,
                        name: userData.company_name,
                        email: userData.email,
                        phone: userData.phone,
                        address: userData.user_address,
                        city: userData.user_city,
                        state: userData.user_state.name,
                        country: "India",
                        pin_code: userData.user_pincode
                    }
                if(userData.user_status==='hold'){
                    userData.user_status = 'active';
                    sellerApproveFunc(req,res,userData)
                    
                }else{
                    userData.user_status = 'active';
                    userData.approvedDate = new Date();
                    request.post({
                        url: 'https://apiv2.shiprocket.in/v1/external/settings/company/addpickup',
                        form: data,
                        json: true,
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${shipToken}`
                        }
                    }, function (error, response, body) {
                        if (body.message) {
                            res.status(200).json({
                                error: true,
                                title: body.message+JSON.stringify(body.errors),
                            })
                        } else {
                            filepath = path.resolve(__dirname, '../assets/sagreement.pdf');
                            let mailData2 = {
                                email: userData.email,
                                subject: 'Welcome to Medidmny',
                                attachments: [{
                                    filename: 'sagreement.pdf',
                                    path: filepath,
                                    contentType: 'application/pdf'
                                }],
                                body:
                                    '<p>' +
                                    'Welcome to Medidmny,Kindly find the agreement attached.'
                            };
                            helper.sendEmail(mailData2);
                            sellerApproveFunc(req,res,userData)
                            
                        }
                    })
                }

            })
        })
    }
    else {
        user.findOne({ _id: req.body.user_id, user_type: req.body.user_type }).populate('user_state').then((userData) => {
            if (!userData) {
                return res.status(200).json({
                    error: false,
                    title: "User not found"
                })
            }
            if (userData && userData.device_token && userData.device_token.length > 0) {
                let devicetoken = userData.device_token;
                let flag = 'Document verified';
                let msg = 'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY.';
                let title = 'Document verified';
                let payload = {};
                helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                });
                notification.addNotification(msg, userData, '', flag, (error, response) => {
                })
            }
            let msgBody = 'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY.'
            let mailData = {
                email: userData.email,
                subject: 'Documents verified',
                body:
                    '<p>' +
                    'Congrats, we’ve verified your KYC documents. Now start enjoying great discounts on thousands of brands on MEDIMNY.'
            };
            helper.sendEmail(mailData);
            helper.sendSMS(userData.phone, msgBody);

            filepath = path.resolve(__dirname, '../assets/bagreement.pdf');
            let mailData2 = {
                email: userData.email,
                subject: 'Welcome to Medimny',
                attachments: [{
                    filename: 'bagreement.pdf',
                    path: filepath,
                    contentType: 'application/pdf'
                }],
                body:
                    '<p>' +
                    'Welcome to Medimny,Kindly find the agreement attached.'
            };
            helper.sendEmail(mailData2);

            userData.approvedBy = req.user._id;
            userData.user_status = 'active';
            userData.approvedDate = new Date();
            userData.save().then((resultData) => {
                res.status(200).json({
                    error: false,
                    title: "User approved successfully",
                })
            })
        });
    }

}

// parameters: token,action,id,name,email
const processProductRequest = (req, res) => {
    productRequest.findOneAndUpdate({ _id: req.body.productRequest_id }, { $set: { status: req.body.action } }, { new: true }).populate('mainUser')
        .then((userData) => {
            if (req.body.action == 'approved') {
                res.status(200).json({
                    error: false,
                    title: "Product request approved.",
                })
                productRequest.status = 'approved';
                let mailData = {
                    email: userData.mainUser.email,
                    subject: 'Product approved',
                    body:
                        '<p>' +
                        `Your product request for ${userData.name} has been approved.`
                };
                helper.sendEmail(mailData);

                // res.status(200).json({
                //     error: false,
                //     message: "successfuly.",
                // });
            }
            else {
                res.status(200).json({
                    error: false,
                    title: "Product rejected.",
                })
                productRequest.status = 'declined';
                let mailData = {
                    email: userData.mainUser.email,
                    subject: 'Product rejected',
                    body:
                        '<p>' +
                        `Your product request for ${userData.name} has been rejected.`
                };
                helper.sendEmail(mailData);

                res.status(200).json({
                    error: false,
                    message: "successfuly.",
                });
            }
        })
}


/*
# eldhose
# parameters: token,
# purpose: make product category isfeatured
*/
const makeCategoryFeatured = (req, res) => {

    ProductCategory.findByIdAndUpdate({ _id: req.body.product_cat_id }, { $set: { isFeatured: req.body.isFeatured } }).then((result) => {
        res.status(200).json({
            error: false,
            title: 'Set featured successfully'
        })
    })
}

/*
# eldhose
# parameters: token,
# purpose: make inventory as deals of the day
*/
const makeInventoryDealsOfDay = (req, res) => {
    inventoryModel.findByIdAndUpdate({ _id: req.body.inventory_id }, { $set: { isDealsOfTheday: req.body.isDealsOfTheday } }).then((result) => {
        res.status(200).json({
            error: false,
            title: 'Set Deals of the day successfully'
        })
    })
}



const getUsers = async (req, res) => {
    var pageNo = req.body.pageNo ? parseInt(req.body.pageNo) : 0;
    var size = req.body.size ? parseInt(req.body.size) : 0;
    var query = req.body.filter;// filter object
    var page = {}
    if (pageNo < 0) {
        response = { "error": true, "message": "invalid page number, should start with 1" };
        return res.json(response)
    }
    page.skip = size * (pageNo - 1)
    page.limit = size
    let users = await user.getAllUsers(query, page, req, res);
    res.status(200).json({
        error: false,
        users
    });
}

const getMetadata = async (req, res) => {
    let companies = await companyModel.find({ isDeleted: false }, { name: 1, _id: 1 }).sort({ name: 1 });
    let types = await productType.find({ isDeleted: false }, { name: 1, _id: 1 }).sort({ name: 1 });
    // let categories = await ProductCategory.find({ status: 'active' }, { name: 1, _id: 1 });
    let categories = await ProductCategory.find({ isDeleted: false }, { name: 1, _id: 1 }).sort({ name: 1 });
    let GST = await gstModel.find({ isDeleted: false }, { name: 1, value: 1, _id: 1 }).sort({ name: 1 });
    detail = {
        companies,
        types,
        categories,
        GST
    }

    res.status(200).json({
        error: false,
        detail
    })
}

const addGST = (req, res) => {
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('value', 'Value is required').notEmpty();
    // gstModel.
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let { name, value } = req.body;
        let newGST = new gstModel({
            name,
            value,
        })
        newGST.save((error, newGSTResponse) => {
            if (!newGSTResponse) {
                return res.status(200).json({
                    title: 'No data found.',
                    error: false,
                    detail: []
                });
            } else {
                return res.status(200).json({
                    title: 'GST added successfully',
                    error: false,
                    detail: newGSTResponse
                });
            }
        })

    }
}

const deleteGST = (req, res) => {
    gstModel.findOneAndUpdate({ _id: req.body.id, isDeleted: false },
        { $set: { isDeleted: true } }).then((data) => {
            if (data) {
                return res.status(200).json({
                    error: false,
                    title: "GST deleted successfully"
                })
            } else {
                return res.status(200).json({
                    error: true,
                    title: "Something went wrong"
                })
            }
        })
}

const addCategory = (req, res) => {
    ProductCategory.addCategory(req, res);
}

const deleteCategory = (req, res) => {
    ProductCategory.deleteCategory(req, res);
}

const addProductType = (req, res) => {
    productType.addProductType(req, res);
}

const deleteProductType = (req, res) => {
    productType.deleteProductType(req, res);
}

const addCompany = (req, res) => {
    companyModel.addCompany(req, res);
}

const deleteCompany = (req, res) => {
    companyModel.deleteCompany(req, res);
}

const getMonthNumber = (data) => {
    let val = data.toLowerCase()
    switch (val) {
        case 'january':
            return 1;
            break;

        case 'february':
            return 2;
            break;

        case 'march':
            return 3;
            break;

        case 'april':
            return 4;
            break;

        case 'may':
            return 5;
            break;

        case 'june':
            return 6;
            break;

        case 'july':
            return 7;
            break;

        case 'august':
            return 8;
            break;

        case 'september':
            return 9;
            break;

        case 'october':
            return 10;
            break;

        case 'november':
            return 11;
            break;

        case 'december':
            return 12;
            break;
        default:
            return 1
    }
}

const getStats = async (req, res) => {
    let date = new Date();
    let d = new Date();
    let monthN = d.getMonth() + 1;
    let year = d.getFullYear();
    let Mar31 = new Date(date.getFullYear(), 2, 31);
    let Apr1 = new Date(date.getFullYear()-1, 3, 1);
    Apr1.setHours(0, 0, 0, 0);
    Mar31.setHours(23, 59, 59, 999);

    let newApr1 = new Date(moment().month("April").startOf('month').format('YYYY-MM-DD'));
    let newMar31 = new Date(moment().add('1','year').month("March").endOf('month').format('YYYY-MM-DD'));
    
    let startM = new Date(moment().startOf('month').format('YYYY-MM-DD'));
    let endM = new Date(moment().add('1','month').startOf('month').format('YYYY-MM-DD'));
    startM.setHours(startM.getHours()-5);
    startM.setMinutes(startM.getMinutes()-30);
    endM.setHours(endM.getHours()-5);
    endM.setMinutes(endM.getMinutes()-30);
    let matchDateQuery = {
        "year": req.body.year ? Number(req.body.year) : year,
        "month": req.body.month ? getMonthNumber(req.body.month) : monthN,
        'is_deleted': false,
        "current_status.status": { $ne: 'Cancelled' }

    }
    let signups = await user.aggregate([
        { $match: { $or: [{ user_type: "seller" }, { user_type: "buyer" }] } },
        { $count: "signups" }
    ]);
    let monthlySales = await Orders.aggregate([
        { $addFields: { year: { $year: "$createdAt" }, month: { $month: "$createdAt" } } },
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "is_deleted": false,
                $and:[{"createdAt": { $gte: startM }},{"createdAt": { $lt: endM }},{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: null,
                total: { $sum: { $toDouble: "$total_amount" } },
                value: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);

    let yearlySales = await Orders.aggregate([
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        {
            $match: {
                "is_deleted": false,
                "createdAt": { $gte: newApr1, $lte: newMar31 },
                $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
            }
        },
        {
            $group: {
                _id: "_id",
                total: { $sum: { $toDouble: "$total_amount" } },
                value: { $sum: 1 }
            }
        },
        {
            $project: {
                "_id": 0,
                total: "$total"
            }
        }
    ]);

    let pendingOrders = await Orders.aggregate([
        {
            $match: {
                "is_deleted": false,
                "paymentStatus": {$ne : 'pending'}
            }
        },
        { $project: { id: 1, myField: { $slice: ["$order_status", -1] } } },
        { $match: { "myField.status": "New" } },
        {
            $count: 'pending'
        }
    ]);

    detail = {
        signups: signups[0].signups,
        monthlySales: monthlySales[0] ? Math.round(monthlySales[0].total) : 0,
        yearlySales: yearlySales[0] ? Math.round(yearlySales[0].total) : 0,
        pendingOrders: pendingOrders[0] ? pendingOrders[0].pending : 0
    }

    res.status(200).json({
        error: false,
        detail
    });

}
const getTopBuyer = async (req, res) => {
    let d = new Date();
    let monthN = d.getMonth() + 1;
    let year = d.getFullYear();
    let matchDateQuery = {
        "year": req.body.year ? Number(req.body.year) : year,
        "month": req.body.month ? getMonthNumber(req.body.month) : monthN,
        'is_deleted': false,
        $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
    }
    let top5Buyers = await Orders.aggregate([
        { $addFields: { year: { $year: "$createdAt" }, month: { $month: "$createdAt" } } },
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        { $match: matchDateQuery },
        {
            $lookup: {
                localField: "user_id",
                foreignField: "_id",
                from: "users",
                as: "Buyers"
            }
        },
        {
            $unwind: {
                path: '$Buyers',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $addFields: { totalAmount: { $toDouble: '$total_amount' } }
        },
        {
            $group: {
                _id: "$user_id",
                total_amount: { $sum: '$totalAmount' },
                buyer: { $first: '$Buyers.company_name' },
            }
        },
        { $sort: { total_amount: -1 } },
        { "$limit": 20 }

    ]).allowDiskUse(true);
    detail = { top5Buyers }

    res.status(200).json({
        error: false,
        detail
    });
}
const getTopSeller = async (req, res) => {
    let d = new Date();
    let monthN = d.getMonth() + 1;
    let year = d.getFullYear();
    let matchDateQuery = {
        "year": req.body.year ? Number(req.body.year) : year,
        "month": req.body.month ? getMonthNumber(req.body.month) : monthN,
        'is_deleted': false,
        $and:[{"current_status.status": { $ne: 'Cancelled' }},{"current_status.status": { $ne: 'RTO' }},{"paymentStatus": {$ne : 'pending'}}]
    }
    let top5Seller = await Orders.aggregate([
        { $addFields: { year: { $year: "$createdAt" }, month: { $month: "$createdAt" } } },
        {
            $addFields: {
                current_status: { $arrayElemAt: ["$order_status", -1] },
            }
        },
        { $match: matchDateQuery },
        {
            $lookup: {
                localField: "seller_id",
                foreignField: "_id",
                from: "users",
                as: "Seller"
            }
        },
        {
            $unwind: {
                path: '$Seller',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $addFields: { totalAmount: { $toDouble: '$total_amount' } }
        },
        {
            $group: {
                _id: "$seller_id",
                total_amount: { $sum: '$totalAmount' },
                buyer: { $first: '$Seller.company_name' },
            }
        },
        { $sort: { total_amount: -1 } },
        { "$limit": 20 }
    ]).allowDiskUse(true);

    detail = { top5Seller }
    res.status(200).json({
        error: false,
        detail
    });
}
const addbulkZipcodes = async (req, res) => {
    let data = []
    fs.createReadStream(path.resolve(__dirname, 'assets', '../codPincode.csv'))
        .pipe(csv.parse({ headers: true }))
        // pipe the parsed input into a csv formatter
        .pipe(csv.format({ headers: true }))
        .on('data', function (data) {  })
        // Using the transform function from the formatting stream
        .transform(async (row, next) => {
            let query = { pincode: row.Pincode }
            zipCodeServing.findOneData(query, function (result) {
                if (result) {
                    return next()
                }
                zipCodeServing.addZipcode(row, function (savedData) {
                    next()
                })
            })
        })
        .on('end', () => {
            res.status(200).json({
                details: 'Zipcodes added successfully!',
                error: false,
            });
        });
}
const fixSearch = async (req, res) => {
    let query = [];
    query.push(
        { $match: { name: req.body.name } },
        {
            $lookup: {
                from: 'products',
                localField: '_id',
                foreignField: 'company_id',
                as: 'prod'
            }
        },
        {
            $unwind: {
                path: '$prod',
                preserveNullAndEmptyArrays: true
            }
        }
    );
    if (req.body.companyDel == true) {
        query.push({
            $skip: 1
        })
    }
    let company = await companyModel.aggregate(query);
    async.forEach(company, function (id, cb) {
        if (id !== '' && req.body.companyDel !== true) {
            // productModel.findByIdAndUpdate({ _id: id.prod._id }, {$set:{ company_id:ObjectId('') }})
        } else if (req.body.companyDel == true) {
            // companyModel.findByIdAndDelete({ _id: id._id })
        } else {
        }
        cb();
    }, async function (err) {
        res.status(200).json({
            error: false,
            title: company
        })
    });
}
const broadcastMessage = (req, res) => {
    try {
        let query = req.body.id ? { _id: req.body.id } : { user_type: { $ne: 'admin' } }
        user.find(query).sort({ updatedAt: -1 }).then((result) => {
            if (result.length > 0) {
                let flag = 'MEDIMNY PRESENTS';
                let msg = req.body.message;
                let title = 'Broadcast from Admin.';
                let payload = {};
                async.eachOfSeries(result, function (data, key, cb) {
                    let devicetoken = data.device_token;
                    helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                    });
                    if(data.user_type == 'seller'){
                        let data1 = JSON.parse(JSON.stringify(data))
                        data1.seller_id = data._id
                       notification.addNotification(msg, '',data1, flag, (error, response) => {
                        }) 
                    }else{
                        notification.addNotification(msg, data, '', flag, (error, response) => {
                        })
                    }
                    cb();
                }, function (err) {
                })
                res.status(200).json({
                    error: false,
                    title: 'Message has been broadcasted'
                })

            } else {
                res.status(200).json({
                    error: true,
                    title: 'User not found.'
                })
            }
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }
}
const verifyPhoneUser = (req, res) => {
    let isBoolean = typeof req.body.status
    if (isBoolean == 'boolean') {
        user.findOneAndUpdate({ _id: req.body.id },
            { $set: { isVerifiedPhone: req.body.status, isVerfied: true } }).then((data) => {
                if (data) {
                    return res.status(200).json({
                        error: false,
                        title: "Status changed successfully"
                    })
                } else {
                    return res.status(200).json({
                        error: true,
                        title: "Something went wrong"
                    })
                }
            })
    } else {
        res.status(200).json({
            error: true,
            title: 'Expected value should boolean at verifyPhoneUser'
        })
    }

}
const updateAddress = (req, res) => {
    let Id = req.body.id;
    let { user_address, user_pincode, user_state, user_city } = req.body;
    user.findByIdAndUpdate({ _id: Id }, { $set: { user_address: user_address, user_pincode: user_pincode, user_city: user_city, user_state: user_state } }, { 'new': true }).then((user) => {
        if (user == '' || user == undefined) {
            res.status(200).json({
                error: true,
                title: "Something went wrong."
            });
        } else {
            res.status(200).json({
                error: false,
                title: "User details updated successfuly.",
                details: user
            });
        }
    });
}
const generateLabelUser = (req, res) => {
    try {
        order.findOne({ order_id: req.body.orderId }).then((result) => {
            if (!result) {
                return res.status(200).json({
                    error: true,
                    title: 'No such order found'
                })
            }
            req.query.order_id = req.body.orderId;
            req.query.awb = result.shipment_id
            req.query.serviceName = result.serviceName
            helper.generateLabel(req, function (url) {
                res.status(200).json({
                    error: false,
                    url
                })
            })
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }


}
const replaceSpecailChar = async (req, res) => {
    try {
        let specailUser = await user.aggregate([
            {
                $project: {
                    first_name: 1,
                    last_name: 1,
                    user_city: 1,
                    company_name: 1
                }
            }
        ]);
        let compName;
        let fname;
        let lname;
        let cityName;
        async.forEach(specailUser, function (eachUser, cb) {
            fname = (eachUser.first_name).replace(/[^a-zA-Z0-9 ]/g, '') ;
            lname = (eachUser.last_name).replace(/[^a-zA-Z0-9 ]/g, '') ;
            cityName = (eachUser.user_city).replace(/[^a-zA-Z0-9 ]/g, '') ;
            compName = (eachUser.company_name).replace(/[^a-zA-Z0-9 ]/g, '') ;
            user.findByIdAndUpdate({ _id: eachUser._id }, { $set: { first_name:fname, last_name:lname, user_city:cityName, company_name:compName }}, { 'new': true }).then( cb() );            
        })
        return res.status(200).json({
            error: false,
            title: 'Specail Characters removal completed'
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }
}

const creditDebitWallet = async(req,res) =>{
    let userData = req.user;
    systemActivityCon.isValid(req,async(data) => {
    if( data ){
    if (req.body.status != '' && req.body.buyerId != '' && req.body.narration != '') {
        let amt = (req.body.status == 'credit') ? req.body.amount : -req.body.amount;
        await user.findByIdAndUpdate({ _id: req.body.buyerId }, { $inc: { wallet_balance: amt } }, { new: true }).then((dataQ) => {
        if (req.body.status == 'credit') {
            let transaction = [];
            transaction.user_id = req.body.buyerId;
            transaction.settle_amount = req.body.amount;
            transaction.paid_amount = req.body.amount;
            transaction.unique_invoice = "";
            transaction.narration = req.body.narration;
            transaction.type = 'Credit Received'
            let paytmPayAmount = 0;
            transaction.status = 'Success';
            transaction.closing_bal = dataQ.wallet_balance;
            transactions.addTransaction(transaction, (error, transactionData) => {
                systemActivity.findOneAndUpdate({_id: data._id },{$set:{type_id:transactionData._id}}).exec();
            })
        } else {
            let transaction = [];
            transaction.user_id = req.body.buyerId;
            transaction.settle_amount = req.body.amount;
            transaction.paid_amount = req.body.amount;
            transaction.unique_invoice = "";
            transaction.narration = req.body.narration;
            transaction.type = 'Credit Used'
            let paytmPayAmount = 0;
            transaction.status = 'Success';
            transaction.closing_bal = dataQ.wallet_balance;
            transactions.addTransaction(transaction, (error, transactionData) => {
                systemActivity.findOneAndUpdate({_id: data._id },{$set:{type_id:transactionData._id}}).exec();
            })
        }
        
            if (dataQ) {
                let newAmt = amt.toString();
                let msgBody = `MediWallet has been ${(req.body.status == 'credit') ? 'credited' : 'debited'} for Rs.${newAmt} by MEDIMNY Admin.`;
                helper.sendSMS(dataQ.phone, msgBody);

                let flag = 'MediWallet';
                let msg = req.body.narration;
                notification.addNotification(msg, dataQ, '', flag, (error, response) => {
                    console.log('add notification', error, response)
                })

                if (dataQ && dataQ.device_token && dataQ.device_token.length > 0) {
                    let devicetoken = dataQ.device_token;
                    let flag = `MediWallet ${(req.body.status == 'credit') ? 'credited' : 'debited'}`;
                    let msg = req.body.narration;
                    let title = `MediWallet ${(req.body.status == 'credit') ? 'credited' : 'debited'}`;
                    let payload = {};

                    helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                    });
                }
                res.status(200).json({
                    error: false,
                    title: `Amount ${req.body.status == 'credit' ? 'credited' : 'debited'} successfully.`
                })
            } else {
                res.status(200).json({
                    error: true,
                    title: 'Something went wrong.'
                })
            }
        })

    }else{
     return res.status(200).json({
            error: true,
            title: 'Something went wrong.'
        })
     }
    } else {
            return res.status(200).json({
                error: true,
                title: 'Invalid OTP or OTP might be expired'
            })
        }
    });
}
const searchBuyer = (req, res) => {
    user.find({ is_deleted: false, user_status: { $ne: "denied"},
           $or:[{ email: { $regex: new RegExp('^' + req.body.name), $options: 'i' } },
                { company_name: { $regex: new RegExp('^' + req.body.name), $options: 'i' } },
                { phone: { $regex: new RegExp('^' + req.body.name), $options: 'i' } }]
            })
        .limit(20)
        .then((results) => {
            res.status(200).json({
                error: false,
                detail: results
            })
        })
}

const getBuyers = async(req, res) => {
    let matchName = (req.body.searchText && req.body.searchText != '') ? {
        $or: [{ email: { $regex: new RegExp('^' + req.body.searchText), $options: 'i' } },
        { company_name: { $regex: new RegExp('^' + req.body.searchText), $options: 'i' } }]
    } : {
        wallet_balance: { $ne:0 }
    }

    let totalWalletBal = await user.aggregate([{ $match: {
        is_deleted: false,
        user_type:'buyer',
        user_status: { $ne: "denied"}
    } },
    { $group: { _id : null, amt : { $sum: "$wallet_balance" } } }]).exec();

    await user.aggregate([
        {
            $match:{
                is_deleted: false,
                user_type:'buyer',
                user_status: { $ne: "denied"}
            }
        },
        {
            $match: matchName 
        },
        {
            $project:{
                company_name:{$toLower: "$company_name" },
                buyerId:1,
                wallet_balance:1
            },
        },
        {
            $sort:{
                company_name:1
            }
        },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
            }
        }
    ]).then((result) => {
        return res.status(200).json({
            title: 'Buyer list fethced successfully.',
            error: false,
            details: result,
            total_amt: totalWalletBal ? totalWalletBal[0].amt : 0
        });
    })
    
}

const getBuyer = (req, res) =>{
    let Id = req.query.buyerId;
    user.findOne( {_id: ObjectId(Id) }).then((result)=>{
        return res.status(200).json({
            error:false,
            result:result
        })
    })
}

const adminToUser =(req, res) => {
    systemActivity.findOne({ otp:req.body.otp , isUsed: false }).then((result) => {
        console.log('=-0=-0=-0-=rrrr',result,req.body)
        if (result) {
            user.findOne({ _id: result.type_id })
                .populate('user_state', 'name')
                .populate('groupId')
                .exec((err, userData) => {
                    helper.generateAdminToken(userData, (token) => {
                        systemActivity.findByIdAndUpdate({ _id: result._id },{ $set:{isUsed:true} }).exec()
                        return res.status(200).json({
                            title: 'Login successful.',
                            error: false,
                            token: token,
                            detail: userData
                        });
                    });
                })
        }else{
            return res.status(200).json({
                error:true,
                title:'Something went wrong.'
            })
        }
    })
}

module.exports = {
    bulkProduct,
    changeUserStatus,
    listingConfigData,
    addConfigData,
    sellerBuyerListing,
    sellerBuyerDetails,
    addStates,
    getStates,
    createSettlements,
    approve,
    getUsers,
    makeCategoryFeatured,
    makeInventoryDealsOfDay,
    getMetadata,
    processProductRequest,
    addGST,
    deleteGST,
    addCategory,
    deleteCategory,
    addProductType,
    deleteProductType,
    addCompany,
    deleteCompany,
    getStats,
    addbulkZipcodes,
    fixSearch,
    broadcastMessage,
    verifyPhoneUser,
    updateAddress,
    generateLabelUser,
    replaceSpecailChar,
    getTopBuyer,
    getTopSeller,
    creditDebitWallet,
    searchBuyer,
    getBuyers,
    getBuyer,
    adminToUser
}
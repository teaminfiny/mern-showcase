var transactions = require('../models/transactions');
const sellerController = require("../controllers/seller");
const moment = require('moment');
let fs = require('fs');
const async = require('async');
const mongoose = require('mongoose');
const { Parser } = require('json2csv');
var ObjectId = mongoose.Types.ObjectId;
const getMonthNumber = (data) => {
    switch (data) {
        case 'January':
            return 1;
            break;

        case 'February':
            return 2;
            break;

        case 'March':
            return 3;
            break;

        case 'April':
            return 4;
            break;

        case 'May':
            return 5;
            break;

        case 'June':
            return 6;
            break;

        case 'July':
            return 7;
            break;

        case 'August':
            return 8;
            break;

        case 'September':
            return 9;
            break;

        case 'October':
            return 10;
            break;

        case 'November':
            return 11;
            break;

        case 'December':
            return 12;
            break;
        default:
            return 1
    }
}
/*
# parameters: token,
# purpose: listing all transactions by ID (For buyer)
*/
const listingTransactions = (req, res) => {
    let userData = req.user;
    let matchQuery = {}
    let matchDateQuery = ( req.body.month !='' && req.body.year !='' ) ? {
        "year": Number(req.body.year) ,
        "month": getMonthNumber(req.body.month)
    } : { };
    matchQuery = req.body.searchText ? { 'buyer.company_name': { $regex: req.body.searchText, $options: 'i' } } : {}
    let matchBuyer =  (req.body.buyer_id && req.body.buyer_id !== '' && req.body.buyer_id !== undefined) ? 
    { user_id: ObjectId(req.body.buyer_id) } : {} ;
    let userQuery = (userData.user_type == 'admin' && req.body.show ===false) ?
        {   
            user_id: ObjectId(req.body.Id),
            $or: [{ 'type': { $ne: 'COD' } }] 
        } :  (userData.user_type == 'admin' && req.body.show ===true) ?
        {
            user_id: ObjectId(req.body.Id)
        } :
        {
            user_id: userData._id,
            $or: [{ 'type': { $eq: 'Online' } }, { 'type': { $eq: 'COD' } }] 
        } 

    transactions.aggregate([
        {
            $match:  userQuery
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: matchDateQuery
        },
        {
            $lookup: {
                localField: 'user_id',
                foreignField: '_id',
                from: 'users',
                as: 'buyer'
            }
        },
        {
            $unwind: {
                path: '$buyer',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: matchQuery
        },
        {
            $match: matchBuyer
        },
        { $sort: { createdAt: -1 } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
            }
        }
    ])
        .then((transactionData) => {
            return res.status(200).json({
                title: 'Transaction list fethced successfully.',
                error: false,
                details: transactionData
            });
        });
}

/*
# parameters: token,
# purpose: view Transaction Details
*/
const TransactionDetails = (req, res) => {
    let user = req.user;
}

/*
# parameters: token,
# purpose: listing all wallet transactions
*/
const listingWalletTransaction = (req, res) => {
    let userData = req.user;

    let matchQuery = {}
    matchQuery = req.body.searchText ? { order_id: { $regex: req.body.searchText, $options: 'i' } } : {}

    transactions.aggregate([
        {
            $match: {
                user_id: userData._id,
                $or: [{ type: 'Credit Used' }, { type: 'Credit Received' }, { type: 'Credit Deposited' }]
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $match: {
                year: Number(req.body.year),
                month: sellerController.getMonthNumber(req.body.month ? req.body.month : new Date().toLocaleDateString('default', { month: 'long' }).toLowerCase())
            }
        },
        {
            $lookup: {
                localField: 'order_id',
                foreignField: 'order_id',
                from: 'orders',
                as: 'order_id'
            }
        },
        {
            $lookup: {
                localField: 'inventory_id',
                foreignField: '_id',
                from: 'inventories',
                as: 'inventory_id'
            }
        },
        {
            $unwind: {
                path: '$inventory_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$order_id',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: matchQuery
        },
        { $sort: { createdAt: -1 } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                data: [{ $skip: (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) }, { $limit: req.body.perPage ? Number(req.body.perPage) : 10 }]
            }
        }
    ])
        .then((transactionData) => {
            return res.status(200).json({
                title: 'Transaction list fethced successfully.',
                error: false,
                details: transactionData
            });
        });
}

/*
# parameters: token,
# purpose: view wallet Transaction Details
*/
const walletTransactionDetails = (req, res) => {
    let user = req.user;

}

const addTransaction = (req, res) => {
    let userData = req.user;
    req.body.user_id = userData._id;
    transactions.addTransaction(req.body, (error, transactionData) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong in add transaction.',
                error: true
            });
        } else {
            return res.status(200).json({
                title: 'Transaction added successfully.',
                error: false,
                details: transactionData
            });
        }
    })
}

const getTransactionDetails = async(req,res) =>{
    let { Id } = req.query;
    await transactions.aggregate([
        { 
            $match:{ _id: ObjectId(Id) }
        },
        {
            $lookup: {
                localField: 'unique_invoice',
                foreignField: 'unique_invoice',
                from: 'orders',
                as: 'order'
            }
        },
        {
            $unwind: { path: '$order', preserveNullAndEmptyArrays: true }
        }
    ]).then((result) => {
        if(result){
            res.status(200).json({
                error:false,
                title:'Details fetched successfully',
                detail:result[0]
            })
        }else{
            res.status(200).json({
                error:true,
                title:'Something went wrong'
            })
        }
    })
}

const downloadTransactionReport = (req, res) => {
    let userData = req.user;
    transactions.aggregate([
        {
            $match: {
                user_id: ObjectId(userData._id),
                $or: [{ type: 'Credit Used' }, { type: 'Credit Received' }, { type: 'Credit Deposited' }]
            }
        },
        {
            $addFields: {
                year: { $year: "$createdAt" },
                month: { $month: "$createdAt" }
            }
        },
        {
            $lookup: {
                localField: 'unique_invoice',
                foreignField: 'unique_invoice',
                from: 'orders',
                as: 'Order'
            }
        },
        {
            $unwind: { path: '$Order', preserveNullAndEmptyArrays: true }
        },
        {
            $lookup: {
                localField: 'Order.seller_id',
                foreignField: '_id',
                from: 'users',
                as: 'Seller'
            }
        },
        {
            $unwind: { path: '$Seller', preserveNullAndEmptyArrays: true }
        },
        {
            $match: {
                year: req.body.year ? Number(req.body.year) : new Date().getFullYear(),
                month: sellerController.getMonthNumber(req.body.month ? req.body.month : new Date().toLocaleDateString('default', { month: 'long' }).toLowerCase())
            }
        },
        { $sort: { createdAt: -1 } }
    ])
    .then((transactionData) => {
        console.log('length', transactionData.length)
        let rstring = "INDEX,DATE,ORDER ID,SELLER,NARRATION,TYPE,AMOUNT,CLOSING BALANCE,STATUS\n";
        async.eachOfSeries(transactionData, function(eachTrans, key, callback){
            if(eachTrans){
                let orderId = eachTrans.Order ? eachTrans.Order.order_id :'';
                let seller = eachTrans.Seller ? eachTrans.Seller.company_name :'';
                rstring+=(Number(key)+1)+","
                        +moment(eachTrans.createdAt).format('DD/MM/YY HH:mm').toString()+","
                        +orderId+","
                        +(seller).replace(/,/g, ' ')+","
                        +(eachTrans.narration).replace(/,/g, ' ')+","
                        +eachTrans.type+","
                        +(eachTrans.settle_amount).toFixed(2)+","
                        +(eachTrans.closing_bal).toFixed(2)+","
                        +(eachTrans.status).toUpperCase()+"\n";
                callback()
            }
            else{
                callback()
            }
        }, function(){
            fs.writeFile('report.csv', rstring, err => {
                if (err){ 
                    console.log(err);
                    res.status(200).json({
                        title: "Something went wrong while generating report.",
                        error: true
                    });
                }
                else{
                    res.attachment('report.csv');
                    res.status(200).send(rstring);
                    console.log('Done');
                }
            });
        });
    });
}

const test = (req, res) => {
    let start = new Date(moment('2021-06-02').format('YYYY-MM-DD'));
    let end = new Date(moment('2021-06-03').format('YYYY-MM-DD'));
    transactions.aggregate([
        {
            $match: {
                $and:[{'createdAt':{$gte:start}},{'createdAt':{$lte:end}}]
            }
        },{
        $lookup: {
            localField: 'user_id',
            foreignField: '_id',
            from: 'users',
            as: 'user_id'
        }},
        {
        $unwind: {
            path: '$user_id',
            preserveNullAndEmptyArrays: true
        }}
    ]).then(data=>{
        let fields = ["Date", "Unique_invoice", "Buyer", "Narration", "Amount"];
        let orderArr = []
        async.eachOfSeries(data, function (trans, key, cb) {
            if (trans) {
                orderArr.push({
                    Date: moment(trans.createdAt).format("DD/MM/YYYY, h:mm a"),
                    Unique_invoice: trans.unique_invoice,
                    Buyer: trans.user_id.company_name,
                    Narration: trans.narration,
                    Amount: trans.settle_amount,
                })
            }
            cb()
        }, function (err) {
            var json2csvParser = new Parser({ fields: fields });
            const csv = json2csvParser.parse(orderArr);
            res.attachment('trans.csv');
            res.status(200).send(csv);
        });
        
    })
}
module.exports = {
    listingTransactions,
    TransactionDetails,
    listingWalletTransaction,
    walletTransactionDetails,
    addTransaction,
    getTransactionDetails,
    downloadTransactionReport,
    test
}
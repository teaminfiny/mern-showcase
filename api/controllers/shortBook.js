const mongoose = require('mongoose');
const ShortBook = require('../models/shortBook');
const ObjectId = mongoose.Types.ObjectId;
const notification = require('../models/notification');
const user = require('../models/user');
const helper = require('../lib/helper');
const Product = require('../models/product');
const ActiveInventory = require('../models/activeInventory');
const sellerController = require('./seller')
const async = require('async');
const moment = require('moment');

const escapeRegex = (string) => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

const listShortBook = async(req, res) => {
    let { perPage } = req.body;
    if(req.user.user_type === 'buyer'){
        await ShortBook.find({user_id:ObjectId(req.user._id), isFulfilled:false, isDeleted:false}).then(result =>{
                async.eachOfSeries(result,async(eachShort, key, cb)=>{
                    await ActiveInventory.findOne({'Product._id':eachShort.product_id}).then(async(result1) =>{
                        if(!result1 && eachShort.inStock == true){
                            await ShortBook.updateMany({product_id:ObjectId(eachShort.product_id)},{$set:{inStock:false}}).exec();
                        }
                        else if(result1 && eachShort.inStock == false){
                            await ShortBook.updateMany({product_id:ObjectId(eachShort.product_id)},{$set:{inStock:true}}).exec();
                        }
                    })
                },function(){})
        })
        let query = []
        if(req.body.tab && req.body.tab === 'history'){
            query.push(
                {
                    $match: {
                        user_id: ObjectId(req.user._id),
                        isDeleted: false,
                        isFulfilled: true
                    }
                }
            )
        }
        else{
            query.push(
                {
                    $match: {
                        user_id: ObjectId(req.user._id),
                        isDeleted: false,
                        isFulfilled: false
                    }
                }
            )
        }
        if(req.body.searchText && req.body.searchText !== undefined && req.body.searchText !== ''){
            let keyValue = escapeRegex(req.body.searchText)
            query.push(
                {
                    $match: {
                        product_name : { $regex: new RegExp(keyValue, 'i') } 
                    }
                }
            )
        }
        // if(req.body.month && req.body.year){
        //     query.push({
        //         $addFields: {
        //             year: { $year: "$createdAt" },
        //             month: { $month: "$createdAt" }
        //         }
        //     },
        //     {
        //         $match: {
        //             year: Number(req.body.year),
        //             month: sellerController.getMonthNumber(req.body.month)
        //         }
        //     })
        // }
        query.push(
            {
                $lookup: {
                    localField: 'product_id',
                    foreignField: '_id',
                    from: 'products',
                    as: 'Product'
                }
            },
            {
                $lookup: {
                    localField: 'Product.company_id',
                    foreignField: '_id',
                    from: 'companies',
                    as: 'manufacturer'
                }
            },
            {
                $unwind: {
                    path: '$Product',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $unwind: {
                    path: '$manufacturer',
                    preserveNullAndEmptyArrays: true
                }
            },
            { $sort: { createdAt: -1 } },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                    result: [{ $skip: req.body.perPage ? (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) : 0 }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
                }
            }
        )
        ShortBook.aggregate(query)
        .then(result => {
            if(result){
                res.status(200).json({
                    error: false,
                    title: "Data fetched successfully",
                    data: result[0]
                })
            }
            else{
                res.status(200).json({
                    error: true,
                    title: "something went wrong",
                })
            }
        })
    }
    else if(req.user.user_type === 'admin'){
        let firstDay = req.body.from_date ? new Date(moment(req.body.from_date).format('YYYY-MM-DD')) : new Date(moment().subtract(1, 'months').format('YYYY-MM-DD'))
        let lastDay = req.body.to_date ? new Date(moment(req.body.to_date).add(1, 'days').format('YYYY-MM-DD')) : new Date(moment().add(1, 'days').format('YYYY-MM-DD'))
        firstDay.setHours(firstDay.getHours()-5);
        firstDay.setMinutes(firstDay.getMinutes()-30);
        lastDay.setHours(lastDay.getHours()-5);
        lastDay.setMinutes(lastDay.getMinutes()-30);
        await ShortBook.find({isFulfilled:false, isDeleted:false}).then(result =>{
                async.eachOfSeries(result,async(eachShort, key, cb)=>{
                    await ActiveInventory.findOne({'Product._id':eachShort.product_id}).then(async(result1) =>{
                        if(!result1 && eachShort.inStock == true){
                            await ShortBook.updateMany({product_id:ObjectId(eachShort.product_id)},{$set:{inStock:false}}).exec();
                        }
                        else if(result1  && eachShort.inStock == false){
                            await ShortBook.updateMany({product_id:ObjectId(eachShort.product_id)},{$set:{inStock:true}}).exec();
                        }
                    })
                },function(){})
        })
        let matchQuery;    
        if(req.body.searchText && req.body.searchText !== undefined && req.body.searchText !== ''){
            let keyValue = escapeRegex(req.body.searchText);
            matchQuery = {$and:[
                {isDeleted:false},
                {createdAt: { $gte: firstDay }} ,
                {createdAt: { $lt: lastDay } },
                {'product_name': { $regex: new RegExp(keyValue, 'i') } }
            ]}
        }else{
            matchQuery = {$and:[
                {isDeleted:false},
                {createdAt: { $gte: firstDay }} ,
                {createdAt: { $lt: lastDay } }
            ]}
        }
        if(req.body.tab === "open"){
            matchQuery['isFulfilled'] = false
        }
        else if(req.body.tab === "history"){
            matchQuery['isFulfilled'] = true
        }
        if(req.body.buyer && req.body.buyer != ''){
            matchQuery['user_id'] = ObjectId(req.body.buyer)
        } 
        ShortBook.aggregate([
            {
                $match: matchQuery
            },
            {
                $lookup: {
                    localField: 'user_id',
                    foreignField: '_id',
                    from: 'users',
                    as: 'User'
                }
            },
            {
                $unwind: {
                    path: '$User',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'product_id',
                    foreignField: '_id',
                    from: 'products',
                    as: 'Product'
                }
            },
            {
                $unwind: {
                    path: '$Product',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    localField: 'Product.company_id',
                    foreignField: '_id',
                    from: 'companies',
                    as: 'Company'
                }
            },
            {
                $unwind: {
                    path: '$Company',
                    preserveNullAndEmptyArrays: true
                }
            },
            { $sort: { createdAt: -1 } },
            {
                $facet: {
                    metadata: [{ $count: "total" }, { $addFields: { page: Number(req.body.page) } }],
                    data: [{ $skip: req.body.perPage ? (Number(req.body.perPage) * Number(req.body.page)) - Number(req.body.perPage) : 0 }, { $limit: req.body.perPage ? Number(req.body.perPage) : 50 }]
                }
            }
        ])
        .then(result => {
            if(result){
                res.status(200).json({
                    error: false,
                    title: "Shortbook fetched successfully",
                    result
                })
            }
            else{
                res.status(200).json({
                    error: true,
                    title: "something went wrong",
                })
            }
        })
    }
    else{
        res.status(200).json({
            error: true,
            title: "something went wrong",
        })
    }
}

const addShortBook = async (req, res) => {
    if (req.body.products) {
        req.body.products && req.body.products.length > 0 && req.body.products.map(async (data) => {
            if (data) {
                if ( data.productName && data.product_id && data.quantity ) {
                    let foundInventory = await ActiveInventory.find({ 'Product._id': ObjectId(data.product_id) }).limit(3).then(result => result);
                    let foundShortbook = await ShortBook.findOne({ product_id: ObjectId(data.product_id), user_id: ObjectId(req.user._id), isDeleted: false, isFulfilled: false }).exec();
                    if ( !foundShortbook ) {
                        let ns = new ShortBook({
                            product_name: data.productName,
                            product_id: data.product_id,
                            user_id: req.user._id,
                            quantity: Number(data.quantity),
                            inStock: foundInventory.length > 0 ? true : false
                        })
                        ns.save();
                        notifySellers(data.productName); //notify sellers
                        let flag = 'Product Added To ShortBook'
                        let msg = `Product ${data.productName.toUpperCase()} is added to your shortbook successfully !`
                        await notification.addNotification(msg, req.user, '', flag, (error, response) => { })
                        if (req.user && req.user.device_token && req.user.device_token.length > 0) {
                            let devicetoken = req.user.device_token;
                            let title = 'Product Added To ShortBook'
                            let payload = {};
                            await helper.sendNotification(devicetoken, flag, title, msg, payload, () => { });
                        }
                    }

                }
            }
        })
        return res.status(200).json({
            error: false,
            title: 'Products Successfully Added Into ShortBook !'
        })
    } else {
        req.checkBody('product', 'Product name is required').notEmpty();
        req.checkBody('id', 'Product id is required').notEmpty();
        req.checkBody('quantity', 'Quantity is required').notEmpty();
        let errors = req.validationErrors();
        let foundShortbook = await ShortBook.findOne({ product_id: ObjectId(req.body.id), user_id: ObjectId(req.user._id), isDeleted: false, isFulfilled: false }).then(result => {
            if (result) {
                return result;
            }
        })
        let foundInventory = await ActiveInventory.find({ 'Product._id': ObjectId(req.body.id) }).limit(3).then(result => {
            if (result) {
                return result;
            }
        })
        if (errors) {
            return res.status(200).json({
                error: true,
                title: 'Something went wrong please try again.',
                errors: errors
            });
        }
        else if (foundShortbook && foundShortbook != '') {
            return res.status(200).json({
                error: true,
                title: 'Product already exists in the shortbook.',
                errors: errors
            });
        } else {
            let { product, id, quantity } = req.body;
            let shortBook = new ShortBook({
                product_name: product.toUpperCase(),
                user_id: ObjectId(req.user._id),
                product_id: ObjectId(id),
                quantity: Number(quantity),
                inStock: foundInventory.length > 0 ? true : false
            });
            shortBook.save(async (err, savedData) => {
                if (err) {
                    return res.status(200).json({
                        error: true,
                        title: 'Something went wrong'
                    })
                }
                else if (savedData) {
                    notifySellers(product); //notify sellers
                    await user.findUserById(ObjectId(req.user._id), async (error, userDetail) => {
                        let flag = 'Product Added To ShortBook'
                        let msg = `Product ${product.toUpperCase()} is added to your shortbook successfully !`
                        await notification.addNotification(msg, userDetail, '', flag, (error, response) => { })
                        if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                            let devicetoken = userDetail.device_token;
                            let title = 'Product Added To ShortBook'
                            let payload = {};
                            await helper.sendNotification(devicetoken, flag, title, msg, payload, () => { });
                        }
                        let emailBody = `Product ${product.toUpperCase()} is added to your shortbook successfully ! You will get notified whenever product is available.`
                        var mailData = {
                            email: userDetail.email,
                            subject: 'Product Added To ShortBook',
                            body: emailBody
                        };
                        await helper.sendEmail(mailData);
                    })
                    return res.status(200).json({
                        error: false,
                        title: 'Product Successfully Added Into ShortBook !',
                        data: savedData
                    })
                }
            })
        }
    }
}

const removeShortBook = (req, res) => {
    ShortBook.findOneAndUpdate({_id:ObjectId(req.body.id), isDeleted:false},{$set:{isDeleted: true}})
    .then(updatedData =>{
        if(updatedData){
            return res.status(200).json({
                error:false,
                title:"Product removed from shortbook successfully",
                data: updatedData
            })
        }
        else{
            return res.status(200).json({
                error:true,
                title:"Something went wrong"
            })

        }
    })
}

const getShortProducts = async(req, res) => {
    let query = {isDeleted:false}
    if(req.body.key && req.body.key !== undefined && req.body.key !== ''){
        let keyValue = escapeRegex(req.body.key)
        query['name']={ $regex: new RegExp(keyValue, 'i') } 
    }
    await Product.find(query).select('_id name chem_combination company_id').limit(20).populate({path:'company_id', select:'_id name'}).sort({'name':1}).then(data =>{
        if(data){
            res.status(200).json({
                title: 'Short products fetched successfully !',
                error:false,
                products:data
            })
        }
        else{
            res.status(200).json({
                title: 'Something went wrong !',
                error:false,
            })
        }
    })
}

const getShortbookFilters = async(req, res) =>{
    let buyers = await user.find({user_type: 'buyer', user_status: 'active', is_deleted:false},{ company_name: 1, _id:1}).sort({ company_name: 1 }).then(result => result);
    let sellers = await user.find({user_type: 'seller', user_status: 'active', 'vaccation.vaccationMode':false ,productNotification:true, is_deleted:false, company_name:{$ne:null}},{ company_name: 1, _id:1}).sort({ company_name: 1 }).then(result => result);
    console.log('length',buyers.length, sellers.length)
    res.status(200).json({
        error:false,
        title:'Success',
        buyers:buyers,
        sellers:sellers
    })
}

const notifyUsers = async(req, res) =>{
    const {seller, data, buyer, message} = req.body ;
    if(seller && seller != '' && seller.length > 0){
        let keyValue = escapeRegex(data.Product.name) ;
        let found = await ActiveInventory.find({ "Product.name": { $regex: new RegExp(keyValue, 'i')} }).then(result => result);
        if(found.length === 0){
            await user.find({_id:{$in:seller}, user_type: 'seller', user_status: 'active', productNotification:true, is_deleted:false, 'vaccation.vaccationMode':false }).then(async(foundSellers) =>{
                if(foundSellers){
                    async.eachOfSeries(foundSellers, async(eachSeller, key, cb) =>{
                        if(eachSeller){
                            let flag = 'Shortbook Notification'
                            let msg = `Buyer has added product, ${data.Product.name.toUpperCase()} to the shortbook, please add inventory of this product !` 
                            await notification.addNotification(msg, eachSeller, '', flag, (error, response) => {})
                            if (eachSeller && eachSeller.device_token && eachSeller.device_token.length > 0) {
                                let devicetoken = eachSeller.device_token;
                                let title = 'Shortbook Notification'
                                let payload = {};
                                await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                            }
                            let emailBody = `Buyer has added product, ${data.Product.name.toUpperCase()} to the shortbook, please add inventory of this product !` 
                            var mailData = {
                                email: eachSeller.email,
                                subject: 'Shortbook Notification',
                                body: emailBody
                            };
                            await helper.sendEmail(mailData);
                        }
                        else{
                            cb()
                        }
                    }, function(){

                    })
                }
            })
        }
    }
    else if(buyer && buyer !== ''){
        await user.findUserById(ObjectId(buyer), async(error, userDetail) => {
            if(userDetail && userDetail !== '' && userDetail !== {}){
                let flag = 'Shortbook Notification'
                let msg = message 
                await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
                if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                    let devicetoken = userDetail.device_token;
                    let title = 'Shortbook Notification'
                    let payload = {};
                    await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                }
                var mailData = {
                    email: userDetail.email,
                    subject: 'Shortbook Notification',
                    body: message
                };
                await helper.sendEmail(mailData);
            }
        })
    }
    res.status(200).json({
        title:'Notification Sent Successfully !',
        error:false
    })
}

const markInstockShortbook = async(id) =>{
    ShortBook.find({product_id: ObjectId(id), inStock:false, isFulfilled:false, isDeleted:false})
        .then(result => {
            if(result){
                async.eachOfSeries(result, async function(eachShort, key, callback){
                    if(eachShort){
                        await ShortBook.findOneAndUpdate({_id: ObjectId(eachShort._id)},{$set:{inStock:true}})
                            .then(updated =>{console.log('updated id', updated._id, updated.inStock)})
                            .catch(err =>{console.log(err)})
                        await user.findUserById(eachShort.user_id, async(error, userDetail) => {
                            let flag = 'Shortbook Available In Stock'
                            let msg = `The product you have added in shortbook with name ${eachShort.product_name.toUpperCase()} is available now.` 
                            await notification.addNotification(msg, userDetail, '', flag, (error, response) => {})
                            if (userDetail && userDetail.device_token && userDetail.device_token.length > 0) {
                                let devicetoken = userDetail.device_token;
                                let title = 'Shortbook Available In Stock'
                                let payload = {};
                                await helper.sendNotification(devicetoken, flag, title, msg, payload, () => {});
                            }
                            let emailBody = '<p>' + `The product you have added in shortbook with name ${eachShort.product_name.toUpperCase()} is available now. Visit medimny to shop now !` 
                            var mailData = {
                                email: userDetail.email,
                                subject: 'Shortbook Available In Stock',
                                body: emailBody
                            };
                            await helper.sendEmail(mailData);
                        })
                    }
                    else{
                        callback()
                    }
                }, function(){
                });
            }
        })
}

const updateShortBook = (type, id) => {
    if(type == 'outOfStock'){
        ShortBook.updateMany({product_id: ObjectId(id)},{$set:{inStock:false}}).exec();
    }
}

const notifySellers = async(data) => {
    let keyValue = escapeRegex(data);
    let found = await ActiveInventory.find({ "Product.name": { $regex: new RegExp(keyValue, 'i') } }).then(result => result);
    if (found.length === 0) {
        user.find({ user_type: 'seller', user_status: 'active', productNotification: true, is_deleted: false, 'vaccation.vaccationMode': false }).then(async (foundSellers) => {
            if (foundSellers) {
                async.eachOfSeries(foundSellers, async (eachSeller, key, cb) => {
                    if (eachSeller) {
                        let flag = 'Shortbook Notification'
                        let msg = `Buyer has added product, ${data.toUpperCase()} to the shortbook, please add inventory of this product !`
                        await notification.addNotification(msg, eachSeller, '', flag, (error, response) => { })
                        if (eachSeller && eachSeller.device_token && eachSeller.device_token.length > 0) {
                            let devicetoken = eachSeller.device_token;
                            let title = 'Shortbook Notification'
                            let payload = {};
                            await helper.sendNotification(devicetoken, flag, title, msg, payload, () => { });
                        }
                        let emailBody = `Buyer has added product, ${data.toUpperCase()} to the shortbook, please add inventory of this product !`
                        var mailData = {
                            email: eachSeller.email,
                            subject: 'Shortbook Notification',
                            body: emailBody
                        };
                        await helper.sendEmail(mailData);
                    }
                    else {
                        cb()
                    }
                }, function () {

                })
            }
        })
    }
}

module.exports = {
    listShortBook,
    addShortBook,
    removeShortBook,
    getShortProducts,
    getShortbookFilters,
    notifyUsers,
    markInstockShortbook,
    updateShortBook,
    notifySellers
}
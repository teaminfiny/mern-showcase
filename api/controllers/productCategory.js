var ProductCategory = require('../models/product_category')
const helper = require('../lib/helper');

/*
# parameters: token,
# purpose: listing of all Product Categories
*/
const listingProductCategory = (req, res) => {
    let user = req.user;
    
    ProductCategory.find({"status": "active"})
    .exec((error, catData) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong, Please try again..',
                error: true,
            });
        }
        return res.status(200).json({
            title: 'Product catgories Fetched successfully.',
            error: false,
            detail: catData
        });
    })
}

/*
# parameters: token,
# purpose: add Product Category details
*/
const addProductCategory = (req, res) => {
    req.checkBody('name', 'Category Name is required').notEmpty();
    req.checkBody('photo', 'Category Photo is required').notEmpty();
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    }
   
    let productCatgory = new ProductCategory();
    productCatgory.name = req.body.name
    let dir = './assets/product-category/';
    helper.createDir(dir);
    productCatgory.photo = helper.base64Upload(req, res, dir, req.body.photo);
    productCatgory.status = 'active';
    productCatgory.description = req.body.description;
    productCatgory.save().then((result)=>{
        res.status(200).json({
            error:false,
            title:'Product category added successfully'
        })
    })
    
}

/*
# parameters: token,
# purpose: edit Product Category
*/
const editProductCategory = (req, res) => {
    let user = req.user;   
}

/*
# parameters: token,
# purpose: delete Product Category
*/
const deleteProductCategory = (req, res) => {
    let user = req.user;   
}



module.exports = {
    listingProductCategory,
    addProductCategory,
    editProductCategory,
    deleteProductCategory
}
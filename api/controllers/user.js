const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const async = require('async');
const randomstring = require('randomstring');
var mongoose = require('mongoose');
var GroupModule = require('../models/permssionGroup')
const user = require('../models/user');
const notification = require('../models/notification');
const rating = require('../models/rating');
const helper = require('../lib/helper');
var Discount = require('../models/bank_account');
var ObjectId = mongoose.Types.ObjectId;
const path = require('path');
const systemConfig = require('../models/systemConfig')
const { check } = require('express-validator');
const shipping = require('../models/shipping_partner');

const Order = require('../models/order');
const { Parser } = require('json2csv');
const moment = require('moment');
/*
# parameters: first_name, last_name, email, password, drugLic20B, drugLic21B, fassaiLic, gstLic, user_type, drugLic20BExpiry, fassaiLicExpiry, drugLic20BExpiry, drugLic21BExpiry
# purpose: signup for buyer and seller
# get basic details and complince form in image or pdf formate
# store details with status = inactive and upload complince form
# generate OTP on mobile number and email for OTP verification  
*/

const signup = (req, res) => {
    req.checkBody('first_name', 'First Name is required').notEmpty();
    req.checkBody('last_name', 'Last Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('drugLic20B', 'Drug License 20B is required').notEmpty();
    req.checkBody('drugLic21B', 'Drug License 21B is required').notEmpty();
    req.checkBody('user_state', 'State is required').notEmpty();
    req.checkBody('user_address', 'Address is required').notEmpty();
    req.checkBody('phone', 'Phone is required').notEmpty();
    req.checkBody('account_no', 'Account no is required').notEmpty();
    req.checkBody('ifsc_code', 'Ifsc code is requiregetSellerBuyerIdd').notEmpty();
    req.checkBody('recipient_name', 'Recipient name is required').notEmpty();
    req.checkBody('red', 'Red is required').notEmpty();
    req.checkBody('green', 'Green is required').notEmpty();
    req.checkBody('blue', 'Blue is required').notEmpty();
    req.checkBody('user_pincode', 'Pin code is required').notEmpty();
    req.checkBody('user_city', 'City is required').notEmpty();
    req.checkBody('cancelledCheck', 'Cancelled check is required').notEmpty();
    req.checkBody('company_name', 'company_name is required').notEmpty();


    // req.checkBody('fassaiLic', 'FASSAI certificate is required').notEmpty();
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: errors[0].msg,
            errors: errors
        });
    } else {
        helper.getMainSellerId((err, groupData) => {
            if (err) {
                return res.status(200).json({
                    title: "Something went wrong please try again.(Error: 58)",
                    error: true,
                });
            } else {
                user.find({ email: req.body.email, user_type: "seller" }).exec((error, userData) => {
                    if (error) {
                        return res.status(200).json({
                            title: "Something went wrong please try again. (Error: 65)",
                            error: true,
                        });
                    }
                    else if (userData.length > 0) {
                        return res.status(200).json({
                            title: "User email ID already exists.",
                            error: true,
                        });
                    } else {
                        let dir = './assets/users/';
                        helper.createDir(dir);
                        helper.getSellerBuyerId("seller", (error, customId) => {
                            if (error) {
                                return res.status(200).json({
                                    error: true,
                                    title: 'Something went wrong, please try again. (Error: 81)',
                                });
                            } else {
                                var newUser = new user({
                                    first_name: req.body.first_name,
                                    last_name: req.body.last_name,
                                    email: req.body.email.trim().toLowerCase(),
                                    user_type: "seller",
                                    device_token: req.body.device_token,
                                    sellerId: customId,
                                    phone: req.body.phone,
                                    company_name: req.body.company_name,
                                    user_state: req.body.user_state,
                                    user_address: req.body.user_address,
                                    user_pincode: req.body.user_pincode,
                                    user_city: req.body.user_city,
                                    user_bank_details: {
                                        account_no: req.body.account_no,
                                        ifsc_code: req.body.ifsc_code,
                                        recipient_name: req.body.recipient_name,
                                        cancelledCheck: helper.base64Upload(req, res, dir, req.body.cancelledCheck)
                                    },
                                    backGroundColor: {
                                        red: req.body.red,
                                        green: req.body.green,
                                        blue: req.body.blue
                                    },
                                    password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)),
                                    groupId: groupData._id
                                });

                                newUser.drugLic20B.name = helper.base64Upload(req, res, dir, req.body.drugLic20B);
                                newUser.drugLic21B.name = helper.base64Upload(req, res, dir, req.body.drugLic21B);
                                newUser.drugLic20B.expires = req.body.drugLic20BExpiry;
                                newUser.drugLic20B.lic = req.body.drugLic20BLicNo
                                newUser.drugLic21B.expires = req.body.drugLic21BExpiry;
                                newUser.drugLic21B.lic = req.body.drugLic21BLicNo
                                if (req.body.gstLic && req.body.gstLic !== '') {
                                    newUser.gstLic.name = helper.base64Upload(req, res, dir, req.body.gstLic);
                                    newUser.gstLic.expires = req.body.gstLicExpiry;
                                    newUser.gstLic.lic = req.body.gstLicNo
                                }
                                if (req.body.fassaiLic && req.body.fassaiLic !== '') {
                                    newUser.fassaiLic.name = helper.base64Upload(req, res, dir, req.body.fassaiLic);
                                    newUser.fassaiLic.expires = req.body.fassaiLicExpiry;
                                    newUser.fassaiLic.lic = req.body.fassaiLicNo
                                }

                                var token = randomstring.generate({
                                    length: 6,
                                    charset: 'numeric'
                                });

                                newUser.otp = token;
                                newUser.otp_expired = Date.now() + 600000; // 10 min
                                newUser.save((err, savedUser) => {
                                    if (err) {
                                        return res.status(200).json({
                                            title: "Something went wrong please try again. (Error: 139)",
                                            error: true,
                                        });
                                    } else {
                                        let msgBody = token + " is your One Time Password. Please complete your OTP verification on MEDIMNY. OTP Valid for 15 min.";
                                        helper.sendSMS(savedUser.phone, msgBody);
                                        let mailData = {
                                            email: savedUser.email,
                                            subject: 'Signup OTP',
                                            body:
                                                '<p>' + 'Medimny OTP genarated is ' + token + ' for  verify signup user. OTP is valid for 10 min to verify signup user.'
                                        };
                                        helper.sendEmail(mailData);


                                        // get user details by id and send notification if device token is available.
                                        if (savedUser && savedUser.device_token && savedUser.device_token.length > 0) {
                                            let devicetoken = savedUser.device_token;
                                            let flag = 'otp_sent';
                                            let msg = 'OTP has been Sent on your mobile number.';
                                            let title = 'OTP Sent.';
                                            let payload = {};

                                            helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                            });

                                            notification.addNotification(msg, savedUser, '', flag, (error, response) => {
                                            })
                                        }

                                        let flag2 = 'welcome';
                                        let msg2 = 'Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.';

                                        notification.addNotification(msg2, '', savedUser, flag2, (error, response) => {
                                        })

                                        let msgBody2 = "Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.";
                                        helper.sendSMS(savedUser.phone, msgBody2);
                                        let mailData3 = {
                                            email: savedUser.email,
                                            subject: 'Welcome',
                                            body:
                                                '<p>' + 'Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.'
                                        };
                                        helper.sendEmail(mailData3);

                                        helper.generateToken(savedUser, (token) => {
                                            return res.status(200).json({
                                                title: "Signup successful.",
                                                token: token,
                                                detail: savedUser,
                                                error: false,
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    }

                })
            }
        })
    }
}


const registerBuyer = (req, res) => {
    req.checkBody('first_name', 'First Name is required').notEmpty();
    req.checkBody('last_name', 'Last Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('drugLic20B', 'Drug License 20B is required').notEmpty();
    req.checkBody('drugLic21B', 'Drug License 21B is required').notEmpty();
    req.checkBody('user_state', 'State is required').notEmpty();
    req.checkBody('user_address', 'Address is required').notEmpty();
    req.checkBody('phone', 'Phone is required').notEmpty();
    req.checkBody('red', 'Red is required').notEmpty();
    req.checkBody('green', 'Green is required').notEmpty();
    req.checkBody('blue', 'Blue is required').notEmpty();
    req.checkBody('user_pincode', 'Pin code is required').notEmpty();
    req.checkBody('user_city', 'City is required').notEmpty();
    req.checkBody('company_name', 'company_name is required').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: errors[0].msg,
            errors: errors
        });
    } else {
        user.find({ email: req.body.email, user_type: "buyer" }).exec((error, userData) => {
            if (error) {
                return res.status(200).json({
                    title: "Something went wrong please try again.(Error: 252)",
                    error: true,
                });
            }
            else if (userData.length > 0) {
                return res.status(200).json({
                    title: "User email ID already exists.",
                    error: true,
                });
            } else {
                let dir = './assets/users/';
                helper.createDir(dir);
                helper.getSellerBuyerId("buyer", (error, customId) => {
                    if (error) {
                        return res.status(200).json({
                            error: true,
                            title: 'Something went wrong, please try again.(Error: 269)',
                        });
                    } else {
                        var newUser = new user({
                            first_name: req.body.first_name,
                            last_name: req.body.last_name,
                            buyerId: customId,
                            email: req.body.email.trim().toLowerCase(),
                            user_type: "buyer",
                            device_token: req.body.device_token,
                            phone: req.body.phone,
                            company_name: req.body.company_name,
                            user_state: req.body.user_state,
                            user_address: req.body.user_address,
                            user_pincode: req.body.user_pincode,
                            user_city: req.body.user_city,
                            backGroundColor: {
                                red: req.body.red,
                                green: req.body.green,
                                blue: req.body.blue
                            },
                            password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10))
                        });


                        newUser.drugLic20B.name = helper.base64Upload(req, res, dir, req.body.drugLic20B);
                        newUser.drugLic21B.name = helper.base64Upload(req, res, dir, req.body.drugLic21B);
                        newUser.drugLic20B.expires = req.body.drugLic20BExpiry;
                        newUser.drugLic20B.lic = req.body.drugLic20BLicNo
                        newUser.drugLic21B.expires = req.body.drugLic21BExpiry;
                        newUser.drugLic21B.lic = req.body.drugLic21BLicNo
                        if (req.body.gstLic && req.body.gstLic !== '') {
                            newUser.gstLic.name = helper.base64Upload(req, res, dir, req.body.gstLic);
                            newUser.gstLic.expires = req.body.gstLicExpiry;
                            newUser.gstLic.lic = req.body.gstLicNo
                        }
                        if (req.body.fassaiLic && req.body.fassaiLic !== '') {
                            newUser.fassaiLic.name = helper.base64Upload(req, res, dir, req.body.fassaiLic);
                            newUser.fassaiLic.expires = req.body.fassaiLicExpiry;
                            newUser.fassaiLic.lic = req.body.fassaiLicNo
                        }

                        var token = randomstring.generate({
                            length: 6,
                            charset: 'numeric'
                        });

                        newUser.otp = token;
                        newUser.otp_expired = Date.now() + 600000; // 10 min
                        newUser.save((err, savedUser) => {
                            if (err) {
                                return res.status(200).json({
                                    title: "Something went wrong please try again.(Error: 322)",
                                    error: true,
                                });
                            } else {
                                let msgBody = token + " is your One Time Password. Please complete your OTP verification on MEDIMNY. OTP Valid for 15 min.";
                                helper.sendSMS(savedUser.phone, msgBody);
                                let mailData = {
                                    email: savedUser.email,
                                    subject: 'Signup successful',
                                    body:
                                        '<p>' +
                                        'Medimny OTP genarated is ' + token + ' for  verify signup user. OTP is valid for 10 min to verify signup user.'
                                };
                                helper.sendEmail(mailData);
                                // get user details by id and send notification if device token is available.
                                if (savedUser && savedUser.device_token && savedUser.device_token.length > 0) {
                                    let devicetoken = savedUser.device_token;
                                    let flag = 'otp_sent';
                                    let msg = 'OTP has been Sent on your mobile number.';
                                    let title = 'OTP Sent.';
                                    let payload = {};

                                    helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                    });

                                    notification.addNotification(msg, savedUser, '', flag, (error, response) => {
                                    })
                                }
                                helper.generateToken(savedUser, (token) => {
                                    // get user details by id and send notification if device token is available.
                                    if (savedUser && savedUser.device_token && savedUser.device_token.length > 0) {
                                        let devicetoken = savedUser.device_token;
                                        let flag = 'signup';
                                        let msg = 'Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.';
                                        let title = 'Signup successful.';
                                        let payload = {};

                                        helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                        });

                                        notification.addNotification(msg, savedUser, '', flag, (error, response) => {
                                        })
                                    }


                                    let flag = 'welcome';
                                    let msg = 'Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.';
                                    notification.addNotification(msg, savedUser, '', flag, (error, response) => {
                                    })

                                    let msgBody1 = "Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.";
                                    helper.sendSMS(savedUser.phone, msgBody1);

                                    let mailData2 = {
                                        email: savedUser.email,
                                        subject: 'Welcome',
                                        body:
                                            '<p>' +
                                            'Welcome to India’s fastest growing Pharma B2B marketplace. Upload Your KYC Documents for verification to enjoy GoodDeals@Medimny.'
                                    };
                                    helper.sendEmail(mailData2);

                                    return res.status(200).json({
                                        title: "Signup successful.",
                                        token: token,
                                        detail: savedUser,
                                        error: false,
                                    });
                                });
                            }
                        });
                    }
                });
            }

        })
    }
}

/*
# parameters: token,
# purpose: login for seller, buyer, admin and admin staff
# check user Id, pass, and user type
# check user is suspended or blocked
# if login creds are correct then genarate token and send user details.
*/

const login = (req, res) => {
    user.findOne({ $and: [{ email: req.body.email.trim().toLowerCase() }] })
        .populate('user_state', 'name')
        .populate('groupId')
        .exec((err, userData) => {
            if (err) {
                return res.status(200).json({
                    title: "Something went wrong. Please try again. (Error: 437)",
                    error: true
                })
            }
            if (!userData) {
                return res.status(200).json({
                    title: "You have entered an invalid username or password",
                    error: true
                });
            }

            if (!bcrypt.compareSync(req.body.password, userData.password)) {
                return res.status(200).json({
                    title: 'You have entered an invalid username or password',
                    error: true
                });
            } else {
                currentDate = new Date();
                var origin = req.headers.origin;
                //if seller is trying to login into admin deny the access
                if (origin != undefined && origin.indexOf("admin") > 0 && userData.user_type != "admin") {
                    return res.status(200).json({
                        title: "Kindly login to seller panel.",
                        error: true,
                        token: "Invalid",
                        detail: userData,
                    });
                }
                if (userData.user_status == 'denied') {
                    return res.status(200).json({
                        title: "Your account has been denied. Please contact admin.",
                        error: true,
                        isDenied: true
                    });
                }
                if (userData.user_status == 'blocked' || (userData.user_status == 'suspend' && new Date(userData.suspend_date) >= currentDate)) {
                    let tempField = userData.user_status == 'blocked' ? 'blocked' : 'suspend';
                    return res.status(200).json({
                        title: "User is Blocked or Suspend",
                        error: true,
                        isActive: false,
                        isBlocked: true
                    });
                } if (userData.drugLic20B.expires < new Date() || userData.drugLic21B.expires < new Date() || (userData.fassaiLic && userData.fassaiLic.expires < new Date())) {

                    {
                        userData.user_type === 'admin' ?
                            helper.generateToken(userData, (token) => {
                                return res.status(200).json({
                                    title: "Logged in successfully",
                                    error: false,
                                    token: token,
                                    detail: userData,
                                    isDocumentExpired: true,
                                });
                            })
                            :
                            helper.generateToken(userData, (token) => {
                                return res.status(200).json({
                                    title: "Your Document is expired, Kindly update the document ASAP.",
                                    error: false,
                                    token: token,
                                    detail: userData,
                                    isDocumentExpired: true,
                                });
                            });
                    }

                } else if (userData.isVerfied === false) {
                    // Check if user is verified or not, if user is not verified send otp to user.

                    let token = randomstring.generate({
                        length: 6,
                        charset: 'numeric'
                    });
                    user.findOneAndUpdate({ _id: userData._id }, { $set: { otp: token, otp_expired: (Date.now() + 600000) } }, { new: true }, (error, updated) => {
                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong, Please try again.(Error: 520)',
                                error: true
                            });
                        }
                        else {
                            let msgBody = 'Medimny OTP genarated is ' + token + ' for verify signup user. OTP is valid for 10 min to verify signup user.'

                            let mailData = {
                                email: userData.email,
                                subject: 'Signup successful',
                                body:
                                    '<p>' +
                                    'Medimny OTP genarated is ' + token + ' for  verify signup user. OTP is valid for 10 min to verify signup user.'
                            };
                            helper.sendEmail(mailData);
                            helper.sendSMS(userData.phone, msgBody);
                            userData.isMobile = req.body.device_token ? true : false
                            // get user details by id and send notification if device token is available.
                            if (userData && userData.device_token && userData.device_token.length > 0) {
                                let devicetoken = userData.device_token;
                                let flag = 'otp_sent';
                                let msg = 'OTP has been Sent on your mobile number.';
                                let title = 'OTP Sent.';
                                let payload = {};

                                helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                });

                                notification.addNotification(msg, userData, '', flag, (error, response) => {
                                })
                            }

                            helper.generateToken(userData, (token) => {
                                return res.status(200).json({
                                    title: 'An otp has been sent your register Email/Phone.',
                                    error: true,
                                    token: token,
                                    detail: userData,
                                    isVerfied: false
                                });
                            });
                        }
                    })
                } else if (userData.user_status == 'inactive') {
                    // get user details by id and send notification if device token is available.
                    if (userData && userData.device_token && userData.device_token.length > 0) {
                        let devicetoken = userData.device_token;
                        let flag = 'Document_not_verification';
                        let msg = 'Your document is under verification, please try again later.';
                        let title = 'Document not verification.';
                        let payload = {};

                        helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                        });

                        // notification.addNotification(msg, userData, ProcessedOrder.seller_id, flag, (error, response) => {
                        // })
                    }
                    return res.status(200).json({
                        title: "Your document is under verification, please try again after sometime.",
                        error: true,
                        inactive: false
                    });
                } else {
                    // userData.isLoggedIn = true;

                    if (req.body.device_token) {
                        if (userData.user_type === 'seller' && userData.groupId.permissions.length < 9 && userData.user_status === 'active') {
                            return res.status(200).json({
                                title: 'You dont have enough permission to login to this device',
                                error: true
                            });
                        }
                        userData.device_token = req.body.device_token;
                    }

                    userData.save().then();

                    if (userData.user_status === 'hold') {
                        let msgBody = 'Your KYC verification is pending. Pl upload your KYC documents for verification on MEDIMNY.'

                        let mailData = {
                            email: userData.email,
                            subject: 'Verification pending',
                            body:
                                '<p>' +
                                'Your KYC verification is pending. Please upload your KYC documents for verification on MEDIMNY.'
                        };
                        helper.sendEmail(mailData);
                        helper.sendSMS(userData.phone, msgBody);

                        let flag = 'KYC Verification pending';
                        let msg = 'Your KYC verification is pending. Please upload your KYC documents for verification on MEDIMNY.';
                        notification.addNotification(msg, userData, '', flag, (error, response) => {
                        })
                        if (userData && userData.device_token && userData.device_token.length > 0) {
                            let devicetoken = userData.device_token;
                            let flag = 'Document_not_verification';
                            let msg = 'Your KYC verification is pending. Please upload your KYC documents for verification on MEDIMNY.';
                            let title = 'Document not verification.';
                            let payload = {};

                            helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                            });
                        }
                    }

                    helper.generateToken(userData, (token) => {
                        return res.status(200).json({
                            title: 'Login successful.',
                            error: false,
                            token: token,
                            detail: userData
                        });
                    });
                }

            }
        });

}

const signin = (req, res) => {
    user.findOne({ $and: [{ email: req.body.email.trim().toLowerCase() }, { user_type: "buyer" }] })
        .populate('user_state', 'name')
        .populate('groupId')
        .exec((err, userData) => {
            if (err) {
                return res.status(200).json({
                    title: "Something went wrong. Please try agin. (Error: 653)",
                    error: true
                })
            }
            if (!userData) {
                return res.status(200).json({
                    title: "You have entered an invalid username or password",
                    error: true
                });
            }

            if (!bcrypt.compareSync(req.body.password, userData.password)) {
                return res.status(200).json({
                    title: 'You have entered an invalid username or password',
                    error: true
                });
            } else {
                if (userData.user_status == 'denied') {
                    return res.status(200).json({
                        title: "Your account has been denied. Please contact admin.",
                        error: true,
                        isDenied: true
                    });
                }
                currentDate = new Date();
                if (userData.user_status == 'blocked' || (userData.user_status == 'suspend' && new Date(userData.suspend_date) >= currentDate)) {
                    let tempField = userData.user_status == 'blocked' ? 'blocked' : 'suspend';
                    return res.status(200).json({
                        title: "User is Blocked or Suspend",
                        error: true,
                        isActive: false,
                        isBlocked: true
                    });
                } else if (userData.isVerfied === false) {
                    // Check if user is verified or not, if user is not verified send otp to user.

                    let token = randomstring.generate({
                        length: 6,
                        charset: 'numeric'
                    });
                    user.findOneAndUpdate({ _id: userData._id }, { $set: { otp: token, otp_expired: (Date.now() + 600000) } }, { new: true }, (error, updated) => {
                        if (error) {
                            return res.status(200).json({
                                title: 'Something went wrong, Please try again.(Error: 700)',
                                error: true
                            });
                        }
                        else {
                            let msgBody = token + ' is your One Time Password. Please complete your OTP verification on MEDIMNY. OTP Valid for 15 min.'
                            let mailData = {
                                email: userData.email,
                                subject: 'Signup successful',
                                body:
                                    '<p>' +
                                    'Medimny OTP genarated is ' + token + ' for  verify signup user. OTP is valid for 10 min to verify signup user.'
                            };
                            helper.sendEmail(mailData);
                            helper.sendSMS(userData.phone, msgBody);
                            // get user details by id and send notification if device token is available.
                            if (userData && userData.device_token && userData.device_token.length > 0) {
                                let devicetoken = userData.device_token;
                                let flag = 'otp_sent';
                                let msg = 'OTP has been Sent on your mobile number.';
                                let title = 'OTP Sent.';
                                let payload = {};

                                helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                });

                                notification.addNotification(msg, userData, '', flag, (error, response) => {
                                })
                            }

                            userData.isMobile = req.body.device_token ? true : false

                            helper.generateToken(userData, (token) => {
                                return res.status(200).json({
                                    title: 'An otp has been sent your register Email/Phone.',
                                    error: true,
                                    token: token,
                                    detail: userData,
                                    isVerfied: false
                                });
                            });
                        }
                    })
                } else {
                    if (req.body.device_token) {
                        userData.device_token = [req.body.device_token]
                    }

                    userData.save().then();
                    if (userData.user_status === 'hold' || userData.user_status === 'inactive') {
                        let msgBody = 'Your KYC verification is pending. Pl upload your KYC documents for verification on MEDIMNY.'

                        let mailData = {
                            email: userData.email,
                            subject: 'Verification pending',
                            body:
                                '<p>' +
                                'Your KYC verification is pending. Please upload your KYC documents for verification on MEDIMNY.'
                        };
                        helper.sendEmail(mailData);
                        helper.sendSMS(userData.phone, msgBody);

                        let flag = 'KYC Verification pending';
                        let msg = 'Your KYC verification is pending. Please upload your KYC documents for verification on MEDIMNY.';
                        notification.addNotification(msg, '', userData, flag, (error, response) => {
                        })
                        if (userData && userData.device_token && userData.device_token.length > 0) {
                            let devicetoken = userData.device_token;
                            let flag = 'Document_not_verification';
                            let msg = 'Your KYC verification is pending. Please upload your KYC documents for verification on MEDIMNY.';
                            let title = 'Document not verification.';
                            let payload = {};

                            helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                            });
                        }
                    }

                    helper.generateToken(userData, (token) => {
                        return res.status(200).json({
                            title: 'Login successful.',
                            error: false,
                            token: token,
                            detail: userData
                        });
                    });
                }

            }
        });
}
/*
# parameters: newPassword
# purpose: change Password for seller, buyer, admin and admin staff
*/

const changePassword = (req, res) => {
    let userData = req.user;
    user.findOne({ email: userData.email }, function (err, userDetails) {
        if (userDetails) {
            userDetails.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
            userDetails.save((error, savedUser) => {
                if (error) {
                    return res.status(200).json({
                        title: 'Something went wrong in change password.(Error: 805',
                        error: true
                    });
                } else {
                    // get user details by id and send notification if device token is available.
                    if (savedUser && savedUser.device_token && savedUser.device_token.length > 0) {
                        let devicetoken = savedUser.device_token;
                        let flag = 'password_changed';
                        let msg = 'Congrats. Your password has been reset. You can continue shopping on Medimny.';
                        let title = 'Password Changed.';
                        let payload = {};

                        helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                        });

                        notification.addNotification(msg, savedUser, '', flag, (error, response) => {
                        })
                    }

                    let msgBody = `Congrats. Your password has been reset. You can continue shopping on Medimny.`;
                    helper.sendSMS(userData.phone, msgBody);

                    return res.status(200).json({
                        title: 'Password changed successfully.',
                        error: false,
                        detail: savedUser
                    });
                }
            });
        } else {
            return res.status(200).json({
                title: 'Something went wrong, Please try again.(Error: 838)',
                error: true
            });
        }
    });
}


/*
# parameters: email,
# purpose: send forgot pass otp on mobile and email
# OTP is valid for 1 hour
*/

const forgotPassword = (req, res) => {

    async.waterfall([
        function (done) {
            var token = randomstring.generate({
                length: 6,
                charset: 'numeric'
            });
            done(null, token);
        },
        function (token, done) {
            let query = '';
            let title = '';
            if (!isNaN(req.body.email)) {
                query = { phone: req.body.email };
                title = 'No account with that mobile number exists.';
            } else {
                query = { email: req.body.email };
                title = 'No account with that email address exists.';
            }
            user.findOne(query, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        title: title,
                        error: true
                    });
                }
                user.otp = token;
                user.otp_expired = Date.now() + 600000; // 10 min
                user.save(function (errr) {
                    done(errr, token, user);
                });
            });
        },
        function (token, user, done) {
            let msgBody = token + " is your One Time Password. Please complete your OTP verification on MEDIMNY. OTP Valid for 15 min.";
            helper.sendSMS(user.phone, msgBody);

            // get user details by id and send notification if device token is available.
            if (user && user.device_token && user.device_token.length > 0) {
                let devicetoken = user.device_token;
                let flag = 'otp_sent';
                let msg = 'OTP has been Sent on your mobile number.';
                let title = 'OTP Sent.';
                let payload = {};

                helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                });

                notification.addNotification(msg, user, '', flag, (error, response) => {
                })
            }

            let mailData = {
                email: user.email,
                subject: 'Medimny Password Reset',
                body:
                    '<p>' +
                    'You are receiving this because you (or someone else) have requested the reset of the password for your account. <br>' +
                    'Medimny OTP genarated is ' + token + ' for Reset Password. OTP is valid for 10 min to reset password.'
            };
            helper.sendEmail(mailData);
            done(null, user);

        }
    ], function (err, user) {
        helper.generateToken(user, (token) => {
            return res.status(200).json({
                title: 'Password otp sent successfully.',
                error: false,
                token: token
            });
        });
    });
}


const resendOTP = (req, res) => {
    let userData = req.user;
    user.findOne({ email: userData.email }, function (err, users) {
        if (!users) {
            return res.status(200).json({
                title: title,
                error: true
            });
        }

        var token = randomstring.generate({
            length: 6,
            charset: 'numeric'
        });

        users.otp = token;
        users.otp_expired = Date.now() + 600000; // 10 min
        users.save(function (errr) {

            let msgBody = token + " is your One Time Password. Please complete your OTP verification on MEDIMNY. OTP Valid for 15 min.";
            helper.sendSMS(users.phone, msgBody);
            let mailData = {
                email: users.email,
                subject: 'Medimny OTP',
                body:
                    '<p>' + 'Medimny OTP genarated is ' + token
            };
            helper.sendEmail(mailData);
            // get user details by id and send notification if device token is available.
            // if (user && user.device_token && user.device_token.length > 0) {
            //     let devicetoken = user.device_token;
            //     let flag = 'otp_sent';
            //     let msg = 'OTP has been Sent on your mobile number.';
            //     let title = 'OTP Sent.';
            //     let payload = {};

            //     helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
            //     });

            //     notification.addNotification(msg, user, '', flag, (error, response) => {
            //     })
            // }

            let mailData2 = {
                email: users.email,
                subject: 'Medimny Password Reset',
                body:
                    '<p>' +
                    'You are receiving this because you (or someone else) have requested the reset of the password for your account. <br>' +
                    'Medimny OTP genarated is ' + token + ' for Reset Password. OTP is valid for 10 min to reset password.'
            };
            helper.sendEmail(mailData2);
            helper.generateToken(users, (token) => {
                return res.status(200).json({
                    title: 'OTP resent successfully.',
                    error: false,
                    token: token
                });
            });
        });
    });
}

/*
# parameters: token,
# purpose: verify token for forgot password and for user after signup
*/

const verifyOTP = (req, res) => {
    async.waterfall([
        function (done) {
            user.findOne({ otp: req.body.verificationOtp, otp_expired: { $gte: Date.now() } }, function (err, user) {
                if (!user) {
                    return res.status(200).json({
                        title: 'Password reset token is invalid or has expired.',
                        error: true
                    });
                }
                user.otp = undefined;
                user.otp_expired = undefined;
                user.isVerifiedPhone = true;
                user.isVerfied = true;

                // if (req.body.device_token) {
                //     if (user.device_token) {
                //         user.device_token = []
                //         user.device_token.push(req.body.device_token)
                //     } else {
                //         user.device_token.push(req.body.device_token);
                //     }
                // }

                user.save(function (err, user) {
                    done(err, user);
                });
            });
        },
        function (user, done) {
            let mailData = {
                email: user.email,
                subject: 'Your password has been changed',
                body: 'Hello,\n\n' +
                    'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            helper.sendEmail(mailData);
            done(null, user);
        }
    ], function (err, user) {

        helper.generateToken(user, (token) => {
            if (user.user_status === "inactive" && user.user_type !== 'buyer') {
                return res.status(200).json({
                    title: 'Otp verified successfully.',
                    error: false,
                    token: null,
                    user_type: user['user_type']
                });
            } else {
                return res.status(200).json({
                    title: 'Otp verified successfully.',
                    error: false,
                    token: token,
                    user_type: user['user_type']
                });
            }
        });
    });
}

/*
# parameters: userToken
# Variables used : token, decoded
# purpose: to logout user.
*/
const userLogout = async (req, res) => {
    user.findOne({ _id: req.user._id }, (error, result) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong, Please try again.',
                error: true,
                details: error
            });
        } else {
            // let index = result.device_token.findIndex((e) => e == req.body.device_token)
            // if (index > -1) {
            //     result.device_token.splice(index, 1);
            //     result.save().then();
            // }

            return res.status(200).json({
                title: 'logout success',
                error: false
            });
        }
    });
}


/*
# parameters: token
# Variables used : token, decoded
# purpose: Edit user details (For admin, seller and buyer)
*/
const editUserProfile = async(req, res) => {
    // let errors = req.validationErrors();
    // if (errors) {
    //     return res.status(200).json({
    //         error: true,
    //         title: errors[0].msg,
    //         errors: errors
    //     });
    // }
    let userData = await user.findOne({ _id:req.user._id }).exec();
    let dir = './assets/users/';
    helper.createDir(dir);

    userData.first_name = req.body.first_name;
    userData.last_name = req.body.last_name;
    userData.phone = req.body.phone;
    userData.user_address = req.body.user_address;
    userData.company_name = req.body.company_name
    if (req.body.user_city) {
        userData.user_city = req.body.user_city;
    }
    if (req.body.user_state) {
        userData.user_state = req.body.user_state;
    }

    if (req.body.user_pincode !== undefined || req.body.user_pincode !== "") {
        userData.user_pincode = req.body.user_pincode;
    }

    userData.save((err, updatedUser) => {
        if (err) {
            return res.status(200).json({
                title: 'An error occurred. (Error: 58)',
                error: true
            });
        }
        updatedUser
            .populate('user_state', 'name')
            .execPopulate()
            .then((data) => {
                return res.status(200).json({
                    title: 'Profile updated succesfully.',
                    error: false,
                    detail: data
                });
            })
    });
}


/*
# parameters: userToken
# purpose: Get user Details using ID.
*/
const getuserDetails = (req, res) => {
    let userData = req.user;
    user.findOne({ _id: userData._id })
        .exec((err, updatedUserdata) => {
            if (err) {
                return res.status(200).json({
                    title: 'User not found.',
                    error: true
                });
            }

            updatedUserdata
                .populate('user_state', 'name')
                .populate({ path: 'mainUser', populate: { path: 'user_state' } })
                .execPopulate()
                .then((data) => {
                    let data2 = data.mainUser ? data.mainUser : data
                    let currentDate = new Date();
                    let days1 = (new Date(data.drugLic20B.expires) - currentDate) / (1000 * 3600 * 24)
                    let days2 = (new Date(data.drugLic21B.expires) - currentDate) / (1000 * 3600 * 24)
                    let exprireDay1 = days1 > 90
                    let exprireDay2 = days2 > 90
                    let message = (!exprireDay1 || !exprireDay2) ? `Your document ${!exprireDay1 ? 'Drug Lic 20B' : ''} ${!exprireDay2 ? 'Drug Lic 21B' : ''} will get expired within 90 days` : ''
                    return res.status(200).json({
                        title: 'User Detail fetched  succesfully.',
                        error: false,
                        detail: data,
                        user_address: data2.user_address,
                        user_pincode: data2.user_pincode,
                        user_state: data2.user_state,
                        user_city: data2.user_city,
                        company_name: data2.company_name,
                        message: message
                    });
                })
        });
}

/*
-# parameters: userToken, verificationOtp
-# purpose: verify user through otp after signup.
-*/

const signupVerificationOtp = (req, res) => {
    let token = req.headers.token;
    let userData = jwt.decode(token, "medideals_03_09_19");
    user.findOne({ email: userData.email, otp: req.body.verificationOtp, otp_expired: { $gte: Date.now() } }, function (err, user) {
        if (err) {
            return res.status(200).json({
                title: 'Invalid OTP.',
                error: true
            });
        }
        else if (!user) {
            return res.status(200).json({
                title: 'Either the otp has been expired or you have entered a wrong otp.',
                error: true
            });
        } else {
            var token = randomstring.generate({
                length: 6,
                charset: 'numeric'
            });
            user.isVirfiedEmail = true;
            user.isAllPermission = true;
            user.isVerfied = true;
            user.otp = token;
            user.otp_expired = Date.now() + 600000; // 10 min
            user.save((error, newUser) => {
                helper.generateToken(newUser, (token) => {
                    let msgBody = 'Medimny OTP genarated is ' + token + ' for verify signup user. OTP is valid for 10 min to verify signup user.'
                    helper.sendSMS(newUser.phone, msgBody);
                    let mailData = {
                        email: newUser.email,
                        subject: 'Medimny OTP',
                        body:
                            '<p>' + 'Medimny OTP genarated is ' + token
                    };
                    helper.sendEmail(mailData);
                    return res.status(200).json({
                        title: 'User Verified Successfully.',
                        error: false,
                        token: token,
                        detail: newUser
                    });
                });
            });

        }
    });
}
const mobileVerification = (req, res) => {
    let token = req.headers.token;
    let userData = jwt.decode(token, "medideals_03_09_19");
    user.findOne({ email: userData.email, otp: req.body.verificationOtp, otp_expired: { $gte: Date.now() } }, function (err, user) {
        if (err) {
            return res.status(200).json({
                title: 'Invalid OTP.',
                error: true
            });
        }
        else if (!user) {
            return res.status(200).json({
                title: 'Either the otp has been expired or you have entered a wrong otp.',
                error: true
            });
        } else {
            // var token = randomstring.generate({
            //     length: 6,
            //     charset: 'numeric'
            // });
            user.isVerifiedPhone = true;
            user.isVerfied = true;
            user.isAllPermission = true;
            // user.otp = token;
            // user.otp_expired = Date.now() + 600000; // 10 min
            user.save((error, newUser) => {
                helper.generateToken(newUser, (token) => {
                    var msgBody = 'Medimny OTP genarated is ' + token + ' for verify signup user. OTP is valid for 10 min to verify signup user.'
                    helper.sendSMS(newUser.phone, msgBody);
                    return res.status(200).json({
                        title: 'User Verified Successfully.',
                        error: false,
                        token: token,
                        detail: newUser
                    });
                });
            });

        }
    });
}

/*
#parameters: token
#purpose: Get sidebar for seller
*/

const getSidbarContent = (req, res) => {
    user.findOne({ _id: req.user._id }).populate('mainUser').then((userData, error) => {
        if (error) {
            res.status(200).json({
                error: false,
                detail: []
            })
        } else {
            if (req.user.user_type === "admin") {
                GroupModule.find({ _id: req.user.groupId }, { 'permissions': 1 }).populate('permissions').then((detail) => {
                    helper.sortAdminPermissionModule(detail, 'getSidbarContent', (error, response, actions) => {
                        res.status(200).json({
                            error: false,
                            detail: response,
                            actions: actions
                            // user_details: userData,
                            // message,
                            // isVaccatioMode: isVaccatioMode
                        })
                    });
                    // res.status(200).json({
                    //     error: false,
                    //     detail: detail
                    // }) 
                })
            }
            else {
                let currentDate = new Date();
                let days1 = (new Date(userData.drugLic20B.expires) - currentDate) / (1000 * 3600 * 24)
                let days2 = (new Date(userData.drugLic21B.expires) - currentDate) / (1000 * 3600 * 24)
                let exprireDay1 = days1 > 90
                let exprireDay2 = days2 > 90
                let message = (!exprireDay1 || !exprireDay2) ? `Your document ${!exprireDay1 ? 'Drug Lic 20B' : ''} ${!exprireDay2 ? 'Drug Lic 21B' : ''} will get expired within 90 days` : ''
                GroupModule.find({ _id: req.user.groupId }, { 'permissions': 1 }).populate('permissions').then((detail) => {
                    let isVaccatioModeIndex = detail.length > 0 ? detail[0].permissions.findIndex((val) => val.name === 'Vacation') : -1
                    let isVaccatioMode = isVaccatioModeIndex > -1 ? true : false
                    if (detail.length > 0) {
                        if (userData.drugLic20B.expires < new Date() || userData.drugLic21B.expires < new Date() || (userData.fassaiLic && userData.fassaiLic.expires < new Date())) {
                            helper.sortPermissionModule(detail, 'getSidbarContent', (error, response) => {
                                let index = response.findIndex((e) => e.name === "Complaince Form");
                                if (index > -1) {
                                    res.status(200).json({
                                        error: false,
                                        detail: [response[index]],
                                        user_details: userData,
                                        isVaccatioMode: isVaccatioMode
                                    })
                                } else {
                                    res.status(200).json({
                                        error: false,
                                        detail: response,
                                        user_details: userData,
                                        message,
                                        isVaccatioMode: isVaccatioMode
                                    })
                                }
                            });
                        }
                        else {
                            helper.sortPermissionModule(detail, 'getSidbarContent', (error, response) => {
                                res.status(200).json({
                                    error: false,
                                    detail: response,
                                    user_details: userData,
                                    message,
                                    isVaccatioMode: isVaccatioMode
                                })
                            });
                        }
                    } else {
                        res.status(200).json({
                            error: false,
                            detail: []
                        })
                    }

                })
            }
        }
    })
}

/*
#parameters: email
#purpose:Validate if email is present in database or not.
*/
const validateEmail = (req, res) => {
    let query = req.body.email ? { email: req.body.email.trim().toLowerCase() } : { phone: req.body.phone }
    user.findOne(query)
        .exec((error, userData) => {
            if (error) {
                return res.status(200).json({
                    error: false,
                    isExist: false
                })
            }
            if (userData && req.body.Id) {
                if (userData && (ObjectId(userData._id).equals(ObjectId(req.body.Id)))) {
                    return res.status(200).json({
                        error: false,
                        isExist: false
                    })
                }
                return res.status(200).json({
                    error: false,
                    isExist: true
                })
            } else if (userData && !req.body.Id) {
                return res.status(200).json({
                    error: false,
                    isExist: true
                })
            }
            else {
                return res.status(200).json({
                    error: false,
                    isExist: false
                })
            }
        })
}

/*
#parameters: token
#purpose:get notification list.
*/
const getNotification = (req, res) => {
    notification.listNotification(req, res);
}

/*
#parameters: token
#purpose:Mark notifications status to read.
*/
const markNotificationRead = (req, res) => {
    notification.markNotificationRead(req, res);
}

/*
#parameters: seller_id, ratingvalue 
#purpose: to add rating to seller from buyers.
*/
const addRating = (req, res) => {
    rating.addRating(req, res);
}

/*
#parameters: token
#purpose:get rating list.
*/
const getRating = (req, res) => {
    if (req.body.sellerReviewList == true) {
        rating.getSellersReviewList(req, res);
    } else {
        rating.getRating(req, res);
    }
}

/*
#parameters: token
#purpose:get average rating value.
*/
const getAverageRating = (req, res) => {
    rating.getAverageRating(req, res);
}

const updateComplainceForm = (req, res) => {
    let userData = req.user;
    let dir = './assets/users/';
    let Id;
    if (req.body.userType === 'admin') {
        Id = req.body.id
    } else {
        Id = req.user.mainUser ? req.user.mainUser : req.user._id
    }
    helper.createDir(dir);
    user.findOne({ _id: Id }).then((userData) => {
        let { drugLic20B, drugLic21B, drugLic21BExpiry, drugLic20BExpiry, fassaiLic, fassaiLicExpiry, gstLic, drugLic20BLicNo, drugLic21BLicNo, fassaiLicNo, gstLicNo } = req.body;

        if (drugLic20B && drugLic20B !== '') {
            userData.drugLic20B.name = helper.base64Upload(req, res, dir, drugLic20B)

        }

        if (drugLic21B && drugLic21B !== '') {
            userData.drugLic21B.name = helper.base64Upload(req, res, dir, drugLic21B)
        }

        if (fassaiLic && fassaiLic !== '') {
            userData.fassaiLic.name = helper.base64Upload(req, res, dir, fassaiLic)
        }

        if (gstLic && gstLic !== '') {
            userData.gstLic.name = helper.base64Upload(req, res, dir, gstLic)
        }

        if (drugLic21BExpiry && drugLic21BExpiry !== '') {
            userData.drugLic21B.expires = drugLic21BExpiry
        }

        if (drugLic20BExpiry && drugLic20BExpiry !== '') {
            userData.drugLic20B.expires = drugLic20BExpiry
        }

        if (fassaiLicExpiry && fassaiLicExpiry !== '') {
            userData.fassaiLic.expires = fassaiLicExpiry
        }
        if (gstLicNo) {
            userData.gstLic.lic = gstLicNo
        }
        if (fassaiLicNo) {
            userData.fassaiLic.lic = fassaiLicNo
        }
        if (drugLic21BLicNo) {
            userData.drugLic21B.lic = drugLic21BLicNo
        }
        if (drugLic20BLicNo) {
            userData.drugLic20B.lic = drugLic20BLicNo
        }
        userData.user_status = 'hold'
        userData.save((err, updatedUser) => {
            if (err) {
                // get user details by id and send notification if device token is available.
                if (userData && userData.device_token && userData.device_token.length > 0) {
                    let devicetoken = userData.device_token;
                    let flag = 'ComplainceForm';
                    let msg = 'Compliance form not updated.';
                    let title = 'Compliance form not updated.';
                    let payload = {};

                    helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                    });

                    notification.addNotification(msg, userData, '', flag, (error, response) => {
                    });
                }

                return res.status(200).json({
                    title: 'An error occurred',
                    error: true
                });
            }
            updatedUser
                .populate('user_state', 'name')
                .execPopulate()
                .then((data) => {
                    // get user details by id and send notification if device token is available.
                    if (userData && userData.device_token && userData.device_token.length > 0) {
                        let devicetoken = userData.device_token;
                        let flag = 'ComplainceForm';
                        let msg = 'Compliance form updated successfully.';
                        let title = 'Compliance form updated successfully.';
                        let payload = {};

                        helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                        });

                        notification.addNotification(msg, userData, '', flag, (error, response) => {
                        });
                    }
                    return res.status(200).json({
                        title: 'Compliance form updated successfully.',
                        error: false,
                        detail: data
                    });
                })
        });
    })

}

const updateBankDetails = (req, res) => {
    req.checkBody('account_no', 'Account no is required').notEmpty();
    req.checkBody('ifsc_code', 'Ifsc code is requiregetSellerBuyerIdd').notEmpty();
    req.checkBody('recipient_name', 'Recipient name is required').notEmpty();
    req.checkBody('cancelledCheck', 'Cancelled check is required').notEmpty();
    let dir = './assets/users/';
    let Id = req.user.mainUser ? req.user.mainUser : req.user._id
    user.findOne({ _id: Id }).then((data) => {
        data.user_bank_details.account_no = req.body.account_no
        data.user_bank_details.ifsc_code = req.body.ifsc_code
        data.user_bank_details.recipient_name = req.body.recipient_name
        data.user_bank_details.cancelledCheck = req.body.cancelledCheck.length > 100 ? helper.base64Upload(req, res, dir, req.body.cancelledCheck) : data.user_bank_details.cancelledCheck
        data.save().then((results) => {
            res.status(200).json({
                error: false,
                message: "Bank details updated successfullly",
                data: results.user_bank_details,
                details: results
            })
        })
    })
}

const updateProfile = async (req, res) => {
    let userData = await user.findOne({ _id: req.user._id}).exec();
    // let errors = req.validationErrors();
    // if (errors) {
    //     return res.status(200).json({
    //         error: true,
    //         title: errors[0].msg,
    //         errors: errors
    //     });
    // }
    let { first_name, last_name, email, phone, user_address, user_pincode, user_state, user_city } = req.body
    if (userData.email == email) {
        userData.first_name = first_name;
        if (last_name) {
            userData.last_name = last_name;
        }

        userData.phone = phone;
        userData.user_address = user_address;
        userData.user_pincode = user_pincode;
        userData.user_city = user_city;
        userData.user_state = user_state
        userData.save((err, usersaved) => {
            if (err || !usersaved) {
                res.status(200).json({
                    error: true,
                    message: "Something went wrong, Please try again (Error: 1616)",
                    details: []
                });
            } else {
                // get user details by id and send notification if device token is available.
                if (usersaved && usersaved.device_token && usersaved.device_token.length > 0) {
                    let devicetoken = usersaved.device_token;
                    let flag = 'user_updated';
                    let msg = 'User details updated successfully.';
                    let title = 'Updated successfully.';
                    let payload = {};

                    helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                    });

                    notification.addNotification(msg, usersaved, '', flag, (error, response) => {
                    })
                }
                res.status(200).json({
                    error: false,
                    message: "User details updated successfully.",
                    details: usersaved
                });
            }
        });
    } else {
        user.findOne({ email: email })
            .exec((error, user) => {
                if (error) {
                    res.status(200).json({
                        error: true,
                        message: "Something went wrong, Please try again (Error: 1649)",
                        details: []
                    })
                }
                if (user && user !== undefined) {
                    res.status(200).json({
                        error: true,
                        message: "Email already taken.",
                        details: []
                    });
                } else {
                    userData.first_name = first_name;
                    // userData.last_name = last_name;
                    userData.email = email;
                    userData.phone = phone;
                    userData.user_address = user_address;
                    userData.user_pincode = user_pincode;
                    userData.user_city = user_city;
                    userData.user_state = user_state
                    userData.save((err, usersaved) => {
                        if (err || !usersaved) {
                            res.status(200).json({
                                error: true,
                                message: "Something went wrong, Please try again (Error: 1672)",
                                details: []
                            });
                        } else {
                            // get user details by id and send notification if device token is available.
                            if (usersaved && usersaved.device_token && usersaved.device_token.length > 0) {
                                let devicetoken = usersaved.device_token;
                                let flag = 'user_updated';
                                let msg = 'User details updated successfully.';
                                let title = 'Updated successfully.';
                                let payload = {};

                                helper.sendNotification(devicetoken, flag, title, msg, payload, () => {
                                });

                                notification.addNotification(msg, usersaved, '', flag, (error, response) => {
                                })
                            }
                            res.status(200).json({
                                error: false,
                                message: "User details updated successfully.",
                                details: usersaved
                            });
                        }
                    });
                }
            })
    }
}

const sms = async (req, res) => {
    var otpmsg = "123988 is your One Time Password. Please complete your OTP verification. OTP Valid for 15 min.";
    helper.sendSMS('9820100067', otpmsg);
    res.status(200).json({
        error: false,
        message: "successfuly.",
    });
}

const email = async (req, res) => {
    filepath = path.resolve(__dirname, '../assets/sagreement.pdf');
    let mailData = {
        email: 'nikhil@infiny.in',
        subject: 'Signup successful',
        attachments: [{
            filename: 'agreement.pdf',
            path: filepath,
            contentType: 'application/pdf'
        }],
        body:
            '<p>' +
            'Medimny agreement'
    };
    helper.sendEmail(mailData);

    res.status(200).json({
        error: false,
        message: "successfuly.",
    });
}

const sendBroadcast = async (req, res) => {
    let devicetoken = "fSopBr6fRvuYZDVgBdx_fs:APA91bFdImpAFPdJk5kwuKim0fhmGGWa2jjLfV277chp6T6vKHa6qjSjaUDJZdB0BmtLfeyaDQyZ7FcPRQdlNHsbNTI7o3xjXE0lQ0sA9KMxW8Lly6VsvBdJBorlswY24Rpf7b2d2jJB";
    let flag = 'Test notification';
    let msg = 'Hello!!!.';
    let payload = {};

    helper.sendNotification(devicetoken, flag, msg, payload, () => {
    });
    res.status(200).json({
        error: false,
        message: "Broadcast Sent.",
    });
}

const markRead = (req, res) => {
    let userData = req.user;
    notification.updateMany({
        $or: [
            { buyerId: userData._id },
            { sellerId: userData._id }
        ], isRead: false
    }, { $set: { isRead: true } })
        .then((data) => {
            return res.status(200).json({
                error: false,
                message: "Notifications read successfully.",
            })
        })
}

const codOnOff = (req, res) => {
    let user_id = req.body.user_id
    user.findByIdAndUpdate({ _id: user_id }, { $set: { cod: req.body.status } }, (error, updated) => {
        if (updated) {
            return res.status(200).json({
                error: false,
                message: "COD updated successfully.",
            })
        }
        else {
            return res.status(200).json({
                error: true,
                message: "User not found.",
            })
        }
    })
}

const resetPassAdmin = (req, res) => {
    let user_id = req.body.user_id
    user.findByIdAndUpdate({ _id: user_id }, { $set: { password: "$2a$10$oQa822t3zfD4f7SiFT867u9TVHP6MuwAWOfPAPa1edbvnszqf4.U6" } }, (error, updated) => {
        if (updated) {
            return res.status(200).json({
                error: false,
                message: "Password reset successfully.",
            })
        }
        else {
            return res.status(200).json({
                error: true,
                message: "Something went wrong.",
            })
        }
    })
}
const getCodService = async (req, res) => {
    try {
        let systemValue = await systemConfig.findOne({ key: 'globalCod' }).then(result => result)
        res.status(200).json({
            error: false,
            userCod: req.user.cod,
            pincodeCod: req.user.codService,
            globalCod: systemValue.value == 1 ? true : false,
            finalValue: req.user.cod && req.user.codService && systemValue.value == 1
        })
    } catch (error) {
        res.status(200).json({
            error: true,
            title: error
        })
    }

}
const rtoCharge = (req, res) => {
    if (req.body.msg != '' && req.body.amt != '') {
        user.findByIdAndUpdate({ _id: req.body.buyerId }, { $set: { 'charges.msg': req.body.msg, 'charges.amt': req.body.amt } }, { 'new': true }).then((data) => {
            if (data) {
                res.status(200).json({
                    error: false,
                    data: data,
                    title: 'RTO charges applied.'
                })
            } else {
                res.status(200).json({
                    error: true,
                    title: 'Something went wrong while applying RTO charges.'
                })
            }
        })
    } else {
        res.status(200).json({
            error: true,
            title: 'Something went wrong.'
        })
    }
}

const deliveryChargeOnOff = (req, res) => {
    let user_id = req.body.user_id
    user.findByIdAndUpdate({ _id: user_id }, { $set: { isDeliveryCharge: req.body.status } }, (error, updated) => {
        if (updated) {
            return res.status(200).json({
                error: false,
                title: "Delivery charge updated successfully.",
            })
        }
        else {
            return res.status(200).json({
                error: true,
                title: "User not found.",
            })
        }
    })
}
const addRemark = (req,res) => {
    if(req.body.userId != '' && req.body.remark != '' ){
        let remarkData = { 
            msg: req.body.remark,
            date: new Date(),
            remarkBy: req.user._id           
        };
        let query =  { $push: { remark: remarkData } };
        user.findOneAndUpdate({ _id: ObjectId(req.body.userId) }, query, {new:true} ).populate('remark.remarkBy').then((data)=>{
            if(data){
                res.status(200).json({
                    error: false,
                    data: data,
                    title: "Remark submited successfully.",
                })
            }else{
                res.status(200).json({
                    error: true,
                    title: "User not found.",
                })
            }
        })
    }else{
        res.status(200).json({
            error: true,
            title: "User not found.",
        })
    }
}

const sellerConfig = async(req, res) => {
    if( req.user.user_type == 'admin' ){
        let { codCourier_id, prepaidCourier_id, additionalComm, productNotification } = req.body;
        let shipCod, shipObjCod, shipPrepaid, shipObjPrepaid;
        let query;
        if(codCourier_id && prepaidCourier_id){
            shipCod = await shipping.findById(codCourier_id).exec();
            shipPrepaid = await shipping.findById(prepaidCourier_id).exec();
            shipObjCod = {
                name: shipCod.name,
                courier_id: shipCod._id
            }
            shipObjPrepaid = {
                name: shipPrepaid.name,
                courier_id: shipPrepaid._id
            }
            query = { $set: { preferredCourierCod: shipObjCod,preferredCourierPrepaid: shipObjPrepaid, additionalComm: additionalComm, productNotification: productNotification  } }
        }else if (codCourier_id){
            shipCod = await shipping.findById(codCourier_id).exec();
            shipObjCod = {
                name: shipCod.name,
                courier_id: shipCod._id
            }
            query = { $set: { preferredCourierCod: shipObjCod, additionalComm: additionalComm, productNotification: productNotification  } }
        }else if (prepaidCourier_id){
            shipPrepaid = await shipping.findById(prepaidCourier_id).exec();
            shipObjPrepaid = {
                name: shipPrepaid.name,
                courier_id: shipPrepaid._id
            }
            query = { $set: { preferredCourierPrepaid: shipObjPrepaid, additionalComm: additionalComm, productNotification: productNotification  } }
        }else{
            query = { $set: { additionalComm: additionalComm, productNotification: productNotification  } }
        }
        user.findOneAndUpdate({ _id: ObjectId(req.body.userId) }, query, {new:true} ).then((data)=>{
            if(data){
                res.status(200).json({
                    error: false,
                    data: data,
                    title: "Seller config updated.",
                })
            }else{
                res.status(200).json({
                    error: true,
                    title: "User not found.",
                })
            }
        })
    }else{
        res.status(200).json({
            error: true,
            title: "Admin creds needed.",
        })
    }
}
const getBuyerslessthan90 = (req, res) => {
    let fields = [ "No", "Buyer Id", "Company Name", "Phone" ];
    let orderArr = [];
    let days = new Date(moment().subtract(3, 'months').format('YYYY-MM-DD'));
    let i = 1;
    user.find({ user_type: 'buyer', user_status: 'active', is_deleted: false }, { _id: 1, buyerId: 1, email: 1, phone: 1, company_name: 1 }).then(buyers => {
        async.eachOfSeries(buyers, function (buyer, key, cb) {
            if (buyer) {
                Order.findOne({ user_id: ObjectId(buyer._id) }, {createdAt: 1}).sort({ createdAt: -1 }).then(orderD => {
                    if( orderD && (orderD.createdAt).getTime() < days.getTime() ){
                        console.log('=-=-=-=-=-=orderD.createdAt',(orderD.createdAt).getTime(), days.getTime(),orderD._id, orderD.createdAt)
                        orderArr.push({
                            No: i++,
                            'Buyer Id': buyer.buyerId,
                            'Company Name': buyer.company_name,
                            Phone: buyer.phone
                        })
                        cb()
                    }else{
                       cb() 
                    }
                })
            }
        }, function (err) {
            var json2csvParser = new Parser({ fields: fields });
            const csv = json2csvParser.parse(orderArr);
            res.attachment('BuyerData.csv');
            res.status(200).send(csv);
        })
    })
}
const sendEmailToAdmin = (req, res) => {
    req.checkBody('phone', 'Phone numner is required').notEmpty();
    req.checkBody('shop_name', 'Shop Name is required').notEmpty();
    req.checkBody('email', 'Email is required').isEmail();
    req.checkBody('city', 'City is required').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: errors[0].msg,
            errors: errors
        });
    }else{
        let { shop_name, name, phone, city, email, msg } = req.body;
        let mailData = {
            email: 'vijay.ranjan@medimny.com',
            subject: 'Onboard email',
            body:
                `${name ? 'Name: '+ name+',':'' }<br/>
                Shop Name: ${shop_name},<br/>
                Email: ${email},<br/>
                Phone: ${phone},<br/>
                City: ${city},<br/>
                ${msg ? 'Message: '+ msg+'.':'' }`
        };
        helper.sendEmail(mailData);
        res.status(200).json({
            error: false,
            title: "Your msg has been sent successfully.",
        })
    }

}
module.exports = {
    signup,
    login,
    changePassword,
    forgotPassword,
    resendOTP,
    verifyOTP,
    userLogout,
    editUserProfile,
    getuserDetails,
    signupVerificationOtp,
    getSidbarContent,
    validateEmail,
    getNotification,
    markNotificationRead,
    addRating,
    getRating,
    getAverageRating,
    updateComplainceForm,
    updateBankDetails,
    mobileVerification,
    registerBuyer,
    signin,
    updateProfile,
    sms,
    email,
    sendBroadcast,
    markRead,
    codOnOff,
    resetPassAdmin,
    getCodService,
    rtoCharge,
    deliveryChargeOnOff,
    addRemark,
    sellerConfig,
    getBuyerslessthan90,
    sendEmailToAdmin
}
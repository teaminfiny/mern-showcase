const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    inventory_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'inventory'
    },
    medi_attribute:[String],
    medi_type:String,
    Seller: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'user'
        },
        first_name: String,
        last_name: String,
        company_name: String,
        user_city: String,
        user_state: String,
        user_address: String,
        user_pincode: String,
    },
    Product: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'product'
        }, 
        name: String,
        images: [String],
        company: String,
        chem_combination: String, 
        product_type:String,
        country_of_origin: String,
        GST:Number,
        isPrepaid:Boolean,
        description: String
    },
    OtherProducts: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'product'
        },
        name: String
    },
    Product_Category: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'product_category'
        },
        name: String
    },
    Discount: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'discount'
        },
        name: String,
        discount_type: String,
        discount_per: Number,
        discount_on_product :{
            inventory_id: {
                type: mongoose.SchemaTypes.ObjectId,
                ref: 'inventory'
            },
            purchase: Number,
            bonus: Number
        }
    },
    final: {
        type: Number,
        default: 0
    },
    Company: {
        _id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'company'
        },
        name: String
    },
    status:{
        type:String,
        enum:['IN STOCK', 'SHORT EXPIRY', 'OUT OF STOCK']
    },
    expiry_date: Date,
    MRP: Number,
    PTR: Number,
    ePTR: Number, //effective price
    quantity: Number,
    min_order_quantity: Number,
    max_order_quantity: Number,
    prepaidInven: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

module.exports = new mongoose.model('Active_Inventory', schema);
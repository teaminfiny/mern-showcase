var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var schema = new Schema({
    product_name: String,
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    status:{
        type:String,
        default:'PENDING',
        enum:['PENDING', 'APPROVED', 'DENIED','CANCELLED']
    },
    manufacturer: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'company'
    },
    otherManufacturer:String,
    quantity: Number,
    isFulfilled:{
        type:Boolean,
        default:false
    },
    isDeleted:{
        type:Boolean,
        default:false
    },
    inStock:{
        type:Boolean,
        default:false
    }
}, {
    timestamps: true
});

const BuyerRequest = module.exports = mongoose.model('buyerRequest', schema);
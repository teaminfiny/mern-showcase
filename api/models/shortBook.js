var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var schema = new Schema({
    product_name: String,
    product_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'product'
    },
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    quantity: Number,
    inStock:{
        type:Boolean,
        default:false
    },
    isFulfilled:{
        type:Boolean,
        default:false
    },
    isDeleted:{
        type:Boolean,
        default:false
    }
}, {
    timestamps: true
});

const ShortBook = module.exports = mongoose.model('shortBook', schema);
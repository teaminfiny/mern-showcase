var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    name: String,          
    address: String,
    number: String,
    status: String,
    other_details: {
        type: String,
    },
    description: String,
    isDeleted:{
        type:Boolean,
        default:false
    }
}, {
    timestamps: true
});

const company = module.exports = mongoose.model('company', schema);

module.exports.getAllCompanies = async(cb)=>{
    let data = await company.find().sort({ name: 1 }).then(result=>result)
    return data
 }

 module.exports.addCompany = (req, res) => {
    req.checkBody('name', 'Name is required').notEmpty();
    // req.checkBody('value', 'Value is required').notEmpty();
    // gstModel.
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let { name,address,description,number,status} = req.body;
        let newCompany = new company({
            name,address,description,number,status
        })
        newCompany.save((error, newCompanyResponse) => {
            if (!newCompanyResponse) {
                return res.status(200).json({
                    title: 'No data found.',
                    error: false,
                    detail: []
                });
            } else {
                return res.status(200).json({
                    title: 'Company added successfully',
                    error: false,
                    detail: newCompanyResponse
                });
            }
        })

    }
 }

 module.exports.deleteCompany = (req, res) => {
    company.findOneAndUpdate({_id:req.body.id, isDeleted:false},{$set:{ isDeleted:true }})
    .then((data) =>{
        if(data){
        return res.status(200).json({
            error:false,
            title:"Company deleted successfully"
        })
    }else{
    return res.status(200).json({
        error:true,
        title:"Something went wrong"
    })
    }
    })
 }
var mongoose = require('mongoose')
var productType = mongoose.Schema({
    name:String,
    isDeleted:{
        type:Boolean,
        default:false
    }
})

let ProductType = module.exports = mongoose.model('productType',productType)

module.exports.getProductType = async(cb)=>{
   let data = await ProductType.find({ isDeleted:false }).sort({ name : 1 }).then(result=>result)
   return data
}

module.exports.addProductType = (req, res) => {
    req.checkBody('name', 'Name is required').notEmpty();
    // req.checkBody('value', 'Value is required').notEmpty();
    // gstModel.
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let { name } = req.body;
        let newProductType = new ProductType({
            name
        })
        newProductType.save((error, newProductTypeResponse) => {
            if (!newProductTypeResponse) {
                return res.status(200).json({
                    title: 'No data found.',
                    error: false,
                    detail: []
                });
            } else {
                return res.status(200).json({
                    title: 'Product type added successfully',
                    error: false,
                    detail: newProductTypeResponse
                });
            }
        })

    }
}

module.exports.deleteProductType = (req, res) => {
    ProductType.findOneAndUpdate({_id:req.body.id, isDeleted:false},{$set:{ isDeleted:true }})
    .then((data) =>{
        if(data){
        return res.status(200).json({
            error:false,
            title:"Product type deleted successfully"
        })
    }
    return res.status(200).json({
        error:true,
        title:"Something went wrong"
    })
    })
}
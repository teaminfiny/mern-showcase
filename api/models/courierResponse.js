var mongoose = require('mongoose');
var courierResponse = mongoose.Schema({
    data: mongoose.SchemaTypes.Mixed
})

let CourierResponse = module.exports = mongoose.model('courierResponse', courierResponse);

module.exports.addCourierResponse = (req, res) => {
    let newCourierResponse = new CourierResponse({
        data: req.body
    });
    newCourierResponse.save((error, response) => {
        if (error) {
            res.status(200).json({
                title : "Something went wrong, Please try again in sometime.",
                error: true,
                detail : error
            })
        } else {
            res.status(200).json({
                title : "Response saved successfully.",
                error: false,
                detail : response
            })
        }
    })
}
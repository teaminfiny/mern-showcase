var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    seller_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    ratingValue: String,
    description: String
}, {
    timestamps: true
});

const rating = module.exports = mongoose.model('rating', schema);

module.exports.addRating = (req, res) => {
    let { seller_id, ratingValue } = req.body;
    let userData = req.user;
    if(seller_id == '' || ratingValue == '' || seller_id == undefined || ratingValue == undefined){
        return res.status(200).json({
            title: 'Parameters missing',
            error: true,
            detail: []
        });
    }else{
    rating.findOne({user_id:userData._id,seller_id:req.body.seller_id}).then((data)=>{ 
    if(!data){
        let newrating = new rating({
            user_id: userData._id,
            seller_id,
            ratingValue
        })
        newrating.save((error, ratingResponse) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again later.',
                    error: true
                });
            }
            if (!ratingResponse) {
                return res.status(200).json({
                    title: 'No rating found.',
                    error: false,
                    detail: []
                });
            } else {
                return res.status(200).json({
                    title: 'Thank you for your feedback.',
                    error: false,
                    detail: ratingResponse
                });
            }
        })
    }   
    else{
        data.ratingValue = req.body.ratingValue;
        data.save((err, rating) => {
            return res.status(200).json({
                title: 'Thank you for your feedback.',
                error: false,
                detail: data
            });
        });
    }
})
}
}


module.exports.getRating = (req, res) => {
    let userData = req.user;
    let query = '';
    if (userData.user_type == 'seller') {
        query = { seller_id: userData._id };
    } else if (userData.user_type == 'buyer') {
        query = { user_id: userData._id }
    }
    rating.find(query)
        .populate('user_id', 'first_name last_name backGroundColor company_name')
        .populate('seller_id', 'first_name last_name backGroundColor company_name')
        .sort({updatedAt:-1})
        .exec((error, ratingList) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again later.',
                    error: true
                });
            }
            if (!ratingList) {
                return res.status(200).json({
                    title: 'No rating found.',
                    error: false,
                    detail: []
                });
            } else {
                let averageRating = 0
                if (ratingList && ratingList.length > 0) {
                    let totalRating = ratingList.reduce((accumulator, currentValue, currentIndex, array) => {
                        return accumulator + Number(currentValue.ratingValue);
                    }, 0);
                    averageRating = Math.floor(totalRating / ratingList.length);
                }
                return res.status(200).json({
                    title: 'Rating fetched successfully.',
                    error: false,
                    detail: ratingList,
                    averageRating: averageRating
                });
            }
        })
}

module.exports.getAverageRating = (req, res) => {
    let userData = req.user;
    let query = '';
    if (userData.user_type == 'seller') {

        rating.aggregate([
            { $match: { seller_id: userData._id } },
            { $group: { _id: '$seller_id', average: { $avg: { "$toDouble": "$ratingValue" } } } }
        ], function (err, result) {
            return res.status(200).json({
                title: 'Rating fetched successfully.',
                error: false,
                detail: result
            });
        });
    } else {
        return res.status(200).json({
            title: 'Rating fetched successfully.',
            error: false,
            detail: []
        });
    }
}

module.exports.getSellersReviewList = (req, res) => {
    let userData = req.user;
    rating.find({seller_id : req.body.sellerId})
        .populate('user_id', 'first_name last_name backGroundColor company_name')
        .sort({updatedAt:-1})
        .exec((error, ratingList) => {
            if (error) {
                return res.status(200).json({
                    title: 'Something went wrong, Please try again later.',
                    error: true
                });
            }
            if (!ratingList) {
                return res.status(200).json({
                    title: 'No rating found.',
                    error: false,
                    detail: []
                });
            } else {
                let averageRating = 0
                if (ratingList && ratingList.length > 0) {
                    let totalRating = ratingList.reduce((accumulator, currentValue, currentIndex, array) => {
                        return accumulator + Number(currentValue.ratingValue);
                    }, 0);
                    averageRating = Math.floor(totalRating / ratingList.length);
                }
            rating.findOne({user_id:userData._id,seller_id:req.body.sellerId}).then((data)=>{
                if(data){
                    return res.status(200).json({
                        title: 'Rating fetched successfully.',
                        error: false,
                        detail: ratingList,
                        averageRating: averageRating,
                        userRating:Number(data.ratingValue),
                    });
                        }
                 
                else{
                    return res.status(200).json({
                        title: 'Rating fetched successfully.',
                        error: false,
                        detail: ratingList,
                        averageRating: averageRating,
                    });
                }  
            })
            }
        })
}
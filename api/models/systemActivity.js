var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
  type: String, // collection name
  type_id: { // collection id
    type: mongoose.SchemaTypes.ObjectId,
    ref: ''
  },
  action: String, // credit/debit/order chg/payment method
  otp: String,
  isUsed: { // otp used or not
    type: Boolean,
    default:false
  },
  user_id: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'user'
}
}, {
  timestamps: true
});

module.exports = mongoose.model('systemActivity',schema);


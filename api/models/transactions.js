var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    settle_amount: Number,
    paid_amount: Number,
    type: {
        type: String,
        enum: ['Online', 'COD', 'Credit Used', 'Credit Received', 'Credit Deposited'],
        required: true
    },
    narration: String,
    unique_invoice : String,
    status: {
        type: String,
        default: "Pending"
    },
    order_id: String,
    invoice_id: String,
    closing_bal: Number,
    inventory_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'inventory'
    }
}, {
    timestamps: true
});

schema.index({ order_id: 1, invoice_id: 1 })
const transaction = module.exports = mongoose.model('transactions', schema);

module.exports.listTransaction = (query, callback) => {
    transaction.find(query, (error, transactionData) => {
        callback(error, transactionData);
    })
}

module.exports.addTransaction = (data, callback) => {
    let newTransaction = new transaction(data);
    newTransaction.save((error, savedData) => {
        callback(error, savedData);
    });
}


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    order_id:{
        type:mongoose.SchemaTypes.ObjectId,
        ref:'order'
    },
    seller_id:{
        type:mongoose.SchemaTypes.ObjectId,
        ref:'user'
    },
    invoice_id:{
        type: String,
        required : true
    },
    order_value : Number,
    tds : {
        type:Number,
        default:1
    },
    tcs : {
        type:Number,
        default:1
    },
    commission : {
        type:Number,
        default:3.54
    },
    net_amt : {
        type:Number,
        default:0
    },
    payment_due_date : Date,
    payment_date : Date,
    transaction_id : {
        type : String,
        required : false
    },
    status : {
        type : String,
        default : 'Pending'
    },
    remark: {
      type: String,
      default: false
    },
    seller_settlement_date: {
        type: Date,
        default: ''
    },
    logistic_settlement_date:{
        type: Date,
        default: ''
    },
    logistic_settlement_amt: Number,
    commission_comp: {
        medi_type_comm: {
            type: Number,
            default: 0
        },
        prod_surge_comm: {
            type: Number,
            default: 0
        },
        user_comm: {
            type: Number,
            default: 0
        },
    }  
},
{
    timestamps: true
});

module.exports = mongoose.model('settlement', schema);
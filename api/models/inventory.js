var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Schema = mongoose.Schema;
let async = require("async")
const moment = require('moment');
var medi = require('./medicine_type');
var product = require('./product');
var activeInventory = require('./activeInventory');

var schema = new Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    product_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'product'
    },
    discount_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'discounts'
    },
    company_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'company'
    },
    status:{
        type:String,
        enum:['In stock','Out of Stock','Short Expiry','Expired']
    },
    expiry_date: Date,
    MRP: Number,
    PTR: Number,
    ePTR: Number, //effective price
    quantity: Number,
    min_order_quantity: Number,
    max_order_quantity: Number,
    isDeleted: {
        type: Boolean,
        default: false
    },
    isHidden: {
        type: Boolean,
        default: false
    },
    isDealsOfTheDay: {
        type: Boolean,
        default: false
    },
    isJumboDeal: {
        type: Boolean,
        default: false
    },
    isArchived: {    // expired product
        type: Boolean,
        default: false
    },
    medicineType_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'medicine_type'
    },
    medicineTypeName: String,
    productName: String,
    hiddenBy: String,
    prepaidInven: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

const inventory = module.exports = mongoose.model('inventory', schema);

module.exports.updateInventoryQuantity = (products, type, mainCb) => {
    async.eachOfSeries(products, (eachProduct, key, cb) => {
        inventory.findById(eachProduct.inventory_id, (error, response) => {
            if (error) {
                cb(error, response)
            } else {
                (response.quantity && eachProduct.quantity) ? response.quantity = type === "create" ? (response.quantity - Number(eachProduct.quantity)) < 0 ? 0 : response.quantity - Number(eachProduct.quantity) : response.quantity + Number(eachProduct.quantity) : Number(eachProduct.quantity);
                response.save(async(err, result) => {
                    if(response.quantity < response.min_order_quantity){
                        await activeInventory.deleteOne({inventory_id: ObjectId(response._id)}).catch(error => {
                            console.log(error);
                        })
                    }
                    cb(error, result)
                   
                })
            }

        })
    }, (err) => {
        mainCb(err)
    });
}
module.exports.upDateManyData = (query,cb)=>{
    inventory.updateMany(query).then((data)=>{
        cb(data)
    })
}


module.exports.getActiveCategories = async() => {
    let dateAfterSixMonth = moment().add(6, "M");
    return inventory.aggregate([
        {
            $match: {
                isDeleted: false,
                isHidden: false,
                expiry_date: { $gt: new Date(dateAfterSixMonth) } ,
            }  
        },
        {
            $lookup: {
                localField: 'product_id',
                foreignField: '_id',
                from: 'products',
                as: 'Product'
            }
        },
        {
            $unwind: "$Product"
        }, 
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $unwind: "$ProductCategory" },
       { $group : { _id : "$ProductCategory._id" ,name:{$addToSet:"$ProductCategory.name"}} },
       {$unwind:"$name"},
       {$sort: { "name": 1 }}      
    ]).then(result=>result) 
}


module.exports.list = async(reqData) => {
    //req.body.page,req.body.perPage,req.body.filter,req.body.filter
    page=reqData.page?reqData.page:1
    perPage=reqData.perPage?reqData.perPage:10
    filter=reqData.filter?reqData.filter:{}
    filter_product=reqData.product_filter?reqData.product_filter:{}  
    let product_filter = {} 
    if(filter_product.key){ 
        product_filter = {$or: [{ "Product.name": {$regex:filter_product.key,$options:'i'} },{ "Product.chem_combination": {$regex:filter_product.key,$options:'i'} }] }
    }  
    
    return inventory.aggregate([
        {
            $match: filter 
        },
        {
            $lookup: {
                localField: 'product_id',
                foreignField: '_id',
                from: 'products',
                as: 'Product'
            }
        },
        {
            $unwind: "$Product"
        }, 
        {
            $lookup: {
                localField: "Product.product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { $match: product_filter 
        },
        { $unwind: "$ProductCategory" },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(page)} }],
                data: [{ $skip: (Number(perPage) * Number(page)) - Number(perPage) }, { $limit:  Number(perPage) }]
            }
        }
    ]).then(result=>result) 
}

module.exports.addParameters = async(req, res) => {
    let medicine = await medi.findOne({ name: 'Ethical branded' }).exec();
    inventory.find().then((data)=> {
        async.eachOfSeries(data, async(inven, key, cb) =>{
            if(inven){
                let prod = await product.findOne({ _id: inven.product_id }).exec();
                inventory.findByIdAndUpdate({ _id: inven._id },{$set: { medicineType_id: medicine._id, medicineTypeName: medicine.name, productName: prod.name }}, { useFindAndModify: false }).exec();
            }
        },function(){
            res.status(200).json({
                error: false,
                title:'success'
            })
        })
    })
}

module.exports.updateInventory = (data, type, value) => {
    console.log('=-0=-0=-0=-0=-updateInventory',data, type, value)
    if (type === 'outOfStock') {
        inventory.findOneAndUpdate({_id: ObjectId(data) }, { $set: { quantity: 0, status: 'Out of Stock' } },{useFindAndModify: false}).exec();
        activeInventory.deleteMany({ inventory_id: ObjectId(data) }).catch(error => {
            console.log(error);
        })
    }
    if (type === 'replace'){
        inventory.findOneAndUpdate({_id: ObjectId(data) }, { $set: { quantity: value.avalQuantity } },{useFindAndModify: false}).exec();
        activeInventory.findOneAndUpdate({ inventory_id: ObjectId(data) }, { $set: { quantity: value.avalQuantity } },{useFindAndModify: false}).exec();
    }
}
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    promo_code: String,
    settlement: {
        type: String,
        default: 'Not Eligible'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    requested: {
        type: String
    },
    order_id: {
        type: String,
        required: true,
        unique: true
    },
    order_status: [{
        status: {
            type: String,
            default: 'New'
        },
        description: String,
        date: Date,
    }],
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    seller_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    serviceName: String,
    invoice_number: String,
    unique_invoice: String,
    products: [
        {
            inventory_id: {
                type: mongoose.SchemaTypes.ObjectId,
                ref: 'inventory'
            },
            productName: String,
            PTR: Number,
            expiry_date: Date,
            ePTR: Number,
            images: Array,
            paid: Number,
            name: String,
            GST: String,
            discount_per: String,
            quantity: String,
            MRP: String,
            discount_name: String,
            surCharge: Number,
            discount_on_product: {
                inventory_id: {
                    type: mongoose.SchemaTypes.ObjectId,
                    ref: 'inventory'
                },
                name: String,
                // type: String,
                purchase: Number,
                bonus: Number
            }
        }
    ],
    delivery_charges: String,
    total_amount: Number,
    balance: {
        type: Number,
        default: 0
    },
    weight: String,
    seller_invoice: String,
    process_date: Date,
    order_cancel_reason: String,
    ship_order_id: String,
    length: Number,
    breadth: Number,
    height: Number,
    shipment_id: String,
    uploadedInvoice: String,
    docket: String,
    shipping_awb: String,
    shippingCompany: String,
    shippingPartner: String,
    paymentStatus: {
        type: String,
        enum: ['pending', 'success', 'failed']
    },
    cancelledBy: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    removedProd: [
        {
            name: String,
            prodId: {
                type: mongoose.SchemaTypes.ObjectId,
                ref: 'product'
            }
        }
    ],
    paymentType: {
        type: String,
        default: "COD"
    },
    rawTransaction: mongoose.Schema.Types.Mixed,
    trackingUrl: String,
    userRequest: mongoose.Schema.Types.Mixed, // for andriod, web, location, ip
    isBulk:{
        type: Boolean, // for bulk req
        default: false
    },
    isManifested:{
        type: Boolean,
        default: false
    },
    sellerCompName: String,
    buyerCompName: String,
    orderType: {
        type: String,
        required: true,
    },
    requestedProd: [
        {
            name: String,
            avalQuantity: Number,
            reqQuantity: Number,
            prodId: {
                type: mongoose.SchemaTypes.ObjectId,
                ref: 'product'
            },
            invenId: {
                type: mongoose.SchemaTypes.ObjectId,
                ref: 'inventory'
            }
        }
    ]
},
    {
        timestamps: true
    });

module.exports = mongoose.model('order', schema);
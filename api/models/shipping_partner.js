var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    name: String
}, {
    timestamps: true
});

const shippingPartner = module.exports = mongoose.model('shippingPartner', schema);

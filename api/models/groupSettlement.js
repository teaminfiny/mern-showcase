var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var schema = new Schema({
    seller_id:{
        type:mongoose.SchemaTypes.ObjectId,
        ref:'user'
    },
    utrNo: String,
    payment_date : Date,
    other_charges: Number,
    gst_other_charges: Number,
    order_value : Number,
    gst : Number,
    tds : {
        type:Number,
        default:1
    },
    tcs : {
        type:Number,
        default:1
    },
    commission : {
        type:Number,
        default:3.54
    },  
    remark: {
        type: String,
        default: ''
      },
    net_amt : {
        type:Number,
        default:0
    },
    early_remittance_charge: {
        type: Number,
        default: 0
    },
    prod_surge_with_gst: {
        type: Number,
        default: 0
    },
    remittanceId: String,
    isPublished: {
        type: Boolean,
        default: false
    }
    
}, {
    timestamps: true
});

module.exports = mongoose.model('groupSettlements', schema);

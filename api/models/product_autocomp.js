var mongoose = require('mongoose');
var product = require('./product');
var inventory = require('./inventory');
var Schema = mongoose.Schema;
const async = require('async');


var schema = new Schema({
    name: String
}, {
    timestamps: true
});

const productAutoComp = module.exports = mongoose.model('product_autocomp', schema);

module.exports.getProductName = async (cb) => {
    let date = new Date();
    let tempMonth = date.getMonth() + 4;
    let tempYear = date.getFullYear();
    await productAutoComp.remove();
    let prod = await inventory.aggregate([
        {
            $addFields: {
                year: { $year: "$expiry_date" },
                month: { $month: "$expiry_date" },
                quantity: "$quantity"
            }
        },
        {
            $match: {
                month: { $gt: tempMonth },
                year: { $gte: tempYear },
                quantity: { $gt: 0 },
                isHidden: false,
                isDeleted: false
            }
        },
        {
            $lookup: {
                from: 'products',
                localField: 'product_id',
                foreignField: '_id',
                as: 'name'
            }
        },
        {
            $unwind: {
                path: '$name',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $group: { _id: "$name.name" }
        }
    ]);
    async.forEach(prod, function (id, cb1) {
        let addData = new productAutoComp({
            name: id
        })
        addData.save(function (err, result) {
            if (err) {
            }
            else {
                cb1();
            }
        })
        
    }, async function (err) {
        productAutoComp.count({},function(err, c) {
            cb()
       });
        
    });
}
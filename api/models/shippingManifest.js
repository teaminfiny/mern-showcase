var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
  date: Date,
  order_id: Array,
  seller_id:{
    type:mongoose.SchemaTypes.ObjectId,
    ref:'user'
  },
  name: String,
  phone: String
},
{
  timestamps: true
});

module.exports = mongoose.model('shippingManifest', schema);
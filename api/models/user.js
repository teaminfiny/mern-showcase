var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Types.ObjectId;

var schema = new Schema({
    addedBy: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    mainUser: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    groupId: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'permissionGroup'
    },
    sellerId: String,
    buyerId: String,
    vaccation: {
        vaccationMode: {
            type: Boolean,
            default: false
        },
        tillDate: Date
    },
    first_name: String,
    last_name: String,
    company_name: String,
    user_type: {
        type: String,
        enum: ['admin', 'seller', 'buyer'],
        required: true
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'other']
    },
    dob: Date,
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: true
    },
    alternative_phone: {
        type: String,
        required: false
    },
    password: String,
    profile_image: String,
    device_token: Array,
    user_status: {  //user status
        type: String,
        enum: ['inactive', 'pending', 'active', 'blocked', 'suspend', 'hold', 'denied'],
        "default": 'inactive',
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    suspend_date: {
        type: Date,
        required: false
    },
    // facebook_id: String,
    // facebook_accessToken: String,
    drugLic20B: {
        name: String,
        expires: Date,
        lic: String
    },
    // charges:{
    //     msg: String,
    //     amt: Number
    // }, 
    drugLic21B: {
        name: String,
        expires: Date,
        lic: String
    },
    gstLic: {
        name: String,
        expires: Date,
        lic: String
    },
    fassaiLic: {
        name: String,
        expires: Date,
        lic: String
    },
    last_login: {
        type: Date,
        required: false
    },
    isLoggedIn: {
        type: Boolean,
        default: false
    },
    otp: {      //forgot password otp
        type: String,
        required: false
    },
    isVerifiedEmail: {
        type: Boolean,
        default: false
    },
    isVerifiedPhone: {
        type: Boolean,
        default: false
    },
    otp_expired: Date,
    isVerfied: {
        type: Boolean,
        default: false
    },
    user_city: String,
    user_state: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'country'
    },
    user_country: {
        type: String,
        default: "India"
    },
    user_address: String,
    user_pincode: String,
    profile_image: String,
    licence_id: String,
    isAllPermission: {
        type: Boolean,
        default: false
    },
    wallet_balance: {    //employer wallet
        type: Number,
        default: 0
    },
    user_bank_details: {
        account_no: String,
        ifsc_code: String,
        recipient_name: String,
        cancelledCheck: String
    },
    backGroundColor: {
        red: String,
        blue: String,
        green: String,
    },
    cod: {
        type: Boolean,
        default: true
    },
    codService: {
        type: Boolean,
        default: true
    },
    isDeliveryRegistered: {
        type: Boolean,
        default: false
    },
    isDeliveryCharge: {
        type: Boolean,
        default: false
    },
    visited_category: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'product_category'
        }
    ],
    approvedBy: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    approvedDate: Date,
    remark: [{
        msg: String,
        date: Date,
        remarkBy: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'user'
        },
    }],
    orderThreshold: {
        type: Boolean,
        default: false
    },
    additionalComm: {
        type: Number,
        default: 0
    },
    preferredCourierCod:{
        name: String,
        courier_id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'shippingPartner'
        },
    },
    preferredCourierPrepaid:{
        name: String,
        courier_id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'shippingPartner'
        },
    },
    productNotification: {
        type: Boolean,
        default: true
    }
},
    {
        timestamps: true
    });

const user = module.exports = mongoose.model('user', schema);

module.exports.findUserById = (id, cb) => {
    user.findById(id)
        .exec((error, userDetails) => {
            if (error) {
                cb(error)
            }
            if (!userDetails) {
                cb(error, userDetails)
            } else {
                cb(error, userDetails)
            }
        })
}

module.exports.getAllUsers = async (query = {}, page = {}, req) => {
    if (query._id != undefined) {
        query._id = ObjectId(query._id);
    }
    let query2 = req.body.name ? 
        {"$or": [{ 'company_name': { $regex: req.body.name ? req.body.name : '', $options: 'i' } },
        { 'email': { $regex: req.body.name ? req.body.name : '', $options: 'i' } },
        { 'phone': { $regex: req.body.name ? req.body.name : '', $options: 'i' } }]} : {}
    return user.aggregate([
        { $addFields: {
                current_remark: { $arrayElemAt: ["$remark", -1] },
            }
        },
        { $match: query },
        { $match: query2 },
        { $lookup : {  localField: "approvedBy", foreignField: "_id", from: "users", as: "ApprovedBy" }},
        { $unwind: { path: '$ApprovedBy', preserveNullAndEmptyArrays: true } },
        { $lookup : {  localField: "current_remark.remarkBy", foreignField: "_id", from: "users", as: "current_remark.remarkBy" }},
        { $unwind: { path: '$current_remark.remarkBy', preserveNullAndEmptyArrays: true } },
        { $project: { password: 0, device_token: 0, visited_category: 0, backGroundColor: 0 } },
        { $sort: { user_status: -1 } },
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { pageNo: Number(req.body.pageNo) } }],
                data: [{ $skip: (Number(req.body.size) * Number(req.body.pageNo)) - Number(req.body.size) }, { $limit: req.body.size ? Number(req.body.size) : 50 }]
            }
        }
    ])
}

module.exports.getAllSellers = async (query = { user_type: 'seller' }, page = {}) => {
    return user.find(query, { password: 0, device_token: 0, visited_category: 0, backGroundColor: 0 }, page).sort({ company_name: 1 }).then(result => result)
}

module.exports.getAllBuyers = async (query = { user_type: 'buyer' }, page = {}) => {
    return user.find(query, { password: 0, device_token: 0, visited_category: 0, backGroundColor: 0 }, page).sort({ company_name: 1 }).then(result => result)
}
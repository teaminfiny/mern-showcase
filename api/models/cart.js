var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Types.ObjectId;
const async = require('async');

var schema = new Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },         
    cart_details: [{
        inventory_id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'inventory'
        },
        quantity: Number,
        discount_per: String,
        discount_on_product: {
            inventory_id: {
                type: mongoose.SchemaTypes.ObjectId,
                ref: 'inventory'
            },
            quantity: Number
        }
    }],
    other_details: {
        type: String,
    }
}, {
    timestamps: true
});

const cart = module.exports = mongoose.model('cart', schema);

module.exports.updateCart = (userId, products, type) => {
    if(type == 'remove'){
        cart.findOne({ user_id: ObjectId(userId) }).then((cartProducts) => {
            if(cartProducts){
                let newProducts = [];
                async.eachOfSeries(cartProducts.cart_details, (eachProduct, key, cb) => {
                    if(eachProduct){
                        let index = products.findIndex((e) => (e.inventory_id).toString() == (eachProduct.inventory_id).toString())
                        console.log('=-0=-0in', index)
                        if (index === -1) {
                            newProducts.push(eachProduct)
                        }
                    }
                    cb()
                }, async (err) => {
                    await cart.findByIdAndUpdate({ _id: cartProducts._id }, { $set: { cart_details: newProducts } }).exec();
                }) 
            }
        })
    }
}
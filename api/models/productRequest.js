var mongoose = require('mongoose')

var productRequest = mongoose.Schema({
    name: String,
    companyName: String,
    chemicalCombination: String,
    gst: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'gst'
    },
    mrp: String,
    ptr: String,
    productType: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'productType'
    },
    productCategory: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'product_category'
    },
    status: {
        type: String,
        enum: ['pending', 'declined', 'approved','cancelled']
    },
    addedBy: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    mainUser: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})
productRequest.index({ name: "text", companyName: "text", mrp: 'text', ptr: 'text', status: 'text' })
let ProductRequest = module.exports = mongoose.model('productRequest', productRequest)

module.exports.getProductRequest = async (cb) => {
    let data = await ProductRequest.find().then(result => result)
    return data
}
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    name: String,
    description: String
}, {
    timestamps: true
});

module.exports = mongoose.model('user_permissions', schema);
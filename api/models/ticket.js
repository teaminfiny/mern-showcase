var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var schema = new Schema({
  createdBy: String,
  createdById: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'user'
  },
  order: {
    orderNo: String,
    order_id: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'orders'
    }
  },
  buyer: {
    compName: String,
    user_id: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user'
    }
  },
  seller: {
    compName: String,
    user_id: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user'
    }
  },
  resolvedDate: Date,
  isResolved: {
    type: Boolean,
    default: false
  },
  resolvedBy: {
    name: String,
    email: String,
    user_id: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user'
    }
  }
},{
    timestamps: true
});

const ticket = module.exports = mongoose.model('ticket', schema);

module.exports.addTicket = (req, res) => {
  let { buyerName, buyerId, sellerName, sellerId, orderId, orderNo } = req.body;
  let userData = req.user;
  let tc = new ticket({
    createdBy: userData.name,
    createdById: userData._id,
    'buyer.compName': buyerName,
    'buyer.user_id': buyerId,
    'seller.compName': sellerName,
    'seller.user_id': sellerId,
    'order.order_id': orderId,
    'order.orderNo': orderNo,
    // resolvedBy: req.body.resolvedBy
  })
  tc.save().catch();
  res.status(200).json({
    title: 'success',
    error: false
  })
}
var mongoose  = require('mongoose');
var pincode_serving = mongoose.Schema({
    pincode:String,
    city:String,
    state:String,
    region:String,
    prepaid:Boolean,
    cod:Boolean,
    reversePickup:Boolean,
    pickup:Boolean,
    servicedBy:String
})

const PincodeServing = module.exports = mongoose.model('pincode_serving',pincode_serving)
module.exports.addZipcode = (data,cb)=>{
    let pincode = new PincodeServing({
        pincode:data.Pincode,
        city :(data.City).toLowerCase(),
        state : (data.State).toLowerCase(),
        region : (data.Region).toLowerCase(),
        prepaid : data.Prepaid=='Y'?true:false,
        cod : data.COD=='Y'?true:false,
        reversePickup : data.REVERSEPICKUP=='Y'?true:false,
        pickup : data.Pickup=='Y'?true:false,
        servicedBy: (data.Serviceable_By).toLowerCase()
    })
    pincode.save().then((result)=>{
        cb()
    })

}
module.exports.findOneData = (query,cb)=>{
    PincodeServing.findOne(query).then((result)=>{
        cb(result)
    })
}
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    buyerId: {       //the user who gets the notification
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    sellerId: {       //the user who gets the notification
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    message: {
        type: String,
        required: true
    },
    flag: {
        type: String,
        required: true
    },
    isRead: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

const notification = module.exports = mongoose.model('notification', schema);

module.exports.addNotification = (message, userData, orderData, flag, cb) => {
    let newNotification = new notification({
        buyerId: userData._id,
        sellerId: orderData.seller_id,
        message: message,
        flag: flag
    });

    newNotification.save((error, response) => {
        if (error) {
            return cb(error);
        }
        else {
            return cb(null, response);
        }
    });
}

module.exports.listNotification = (req, res) => {
    let query = {};
    let userData = req.user;
    let limit = req.body.perPage ? Number(req.body.perPage) : parseInt(15);
    let skip = (parseInt(req.body.page ? req.body.page : 1) - 1) * parseInt(limit);
    query = {
        $or: [
            { buyerId: userData._id },
            { sellerId: userData._id }
        ]
    }
    notification.find(query)
        .populate('buyerId', 'first_name last_name backGroundColor')
        .populate('sellerId', 'first_name last_name backGroundColor')
        .sort({ createdAt: -1 }).skip(skip).limit(limit)
        .exec(async (error, notificationList) => {
            if (error) {
                return res.status(200).json({
                    title: "Something went wrong, Please try again.",
                    error: true,
                });
            }
            if (notificationList === undefined) {
                return res.status(200).json({
                    title: "No notification found.",
                    detail: [],
                    error: false,
                });
            } else {
                try {
                    let unReadNotification = await notification.find({
                        $or: [
                            { buyerId: userData._id },
                            { sellerId: userData._id }
                        ],
                        $and: [
                            { isRead: false }
                        ]
                    }).populate('buyerId', 'first_name last_name backGroundColor')
                        .populate('sellerId', 'first_name last_name backGroundColor');
                    notification.countDocuments(query).then((count)=>{
                        return res.status(200).json({
                            title: "Notification fetched successfully.",
                            detail: notificationList,
                            total: count,
                            count: unReadNotification && unReadNotification.length > 0 ? unReadNotification.length : 0,
                            unReadNotification : unReadNotification,
                            error: false,
                        });
                    })
                }
                catch (error) {
                    return res.status(200).json({
                        title: "Something went wrong, Please try again.",
                        error: true,
                    });
                }
            }
        })
}

module.exports.markNotificationRead = async (req, res) => {
    notification.updateMany({ isRead: false }, { $set: { isRead: true } }, { multi: true }, (error, response) => {
        if (error) {
            return res.status(200).json({
                title: 'Something went wrong, Please try again later.',
                error: true
            });
        } else {
            return res.status(200).json({
                title: 'Notification updated successfully.',
                error: false,
                detail: []
            });
        }
    })
}
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    name: String,          
    status: String,
    description: String,
    photo:String,
    isFeatured: Boolean,
    isDeleted:{
        type:Boolean,
        default:false
    }
}, {
    timestamps: true
});

let product_category = module.exports = mongoose.model('product_category', schema);

module.exports.getProductCategory = async(cb)=>{
   let data = await product_category.find({ isDeleted:false }).sort({ name : 1 }).then(result=>result)
   return data
}

module.exports.addCategory = (req, res) =>{
    req.checkBody('name', 'Name is required').notEmpty();
    // req.checkBody('status', 'Statu is required').notEmpty();
    // gstModel.
    let errors = req.validationErrors();
    if (errors) {
        return res.status(200).json({
            error: true,
            title: 'Something went wrong please try again.',
            errors: errors
        });
    } else {
        let { name,status,description,photo,isFeatured } = req.body;
        let newCategory = new product_category({
            name,status,description,photo,isFeatured
        })
        newCategory.save((error, newCategoryResponse) => {
            if (!newCategoryResponse) {
                return res.status(200).json({
                    title: 'No data found.',
                    error: false,
                    detail: []
                });
            } else {
                return res.status(200).json({
                    title: 'Category added successfully',
                    error: false,
                    detail: newCategoryResponse
                });
            }
        })

    }
}

module.exports.deleteCategory = (req, res) => {
    product_category.findOneAndUpdate({_id:req.body.id, isDeleted:false},{$set:{ isDeleted:true }})
    .then((data) =>{
        if(data){
        return res.status(200).json({
            error:false,
            title:"Category deleted successfully"
        })
    }else{
    return res.status(200).json({
        error:true,
        title:"Something went wrong"
    })
}
    })
}
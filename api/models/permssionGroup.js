var mongoose = require('mongoose')

var permissionGroup = mongoose.Schema({
    name: String,
    permissions: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'permissionModule'
    }],
    mainUser: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    staffId: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'user'
        }
    ]
}, {
    timestamps: true
})

module.exports = mongoose.model('permissionGroup', permissionGroup)
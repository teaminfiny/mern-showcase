var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name: String,
    from_date: Date,
    to_date:Date,
    promo_code: String,
    image_banner : String,
    redirect_url : String,
    is_expired: {
      type: Boolean,
      default: false
    },
    isActive: {
      type:Boolean,
      default:false
    }  
},
{
    timestamps: true
});

const promotion = module.exports = mongoose.model('promotions', schema);

module.exports.activatePromotion = (req, res) => {
  promotion.findOneAndUpdate({ _id:req.body.id },{$set:{ isActive:req.body.active }})
 .then((data) =>{
     if(data){
     return res.status(200).json({
         error:false,
         title:"Promotion set successfully"
     })
 }else{
 return res.status(200).json({
     error:true,
     title:"Something went wrong"
 })
 }
 })
}
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema ({
  unique_invoice: String,
  gatewayResponce: mongoose.Schema.Types.Mixed
},
{
    timestamps: true
});

module.exports = mongoose.model('ccavenueResponse',schema);
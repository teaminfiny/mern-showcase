var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    name: String,
    details: {
        type: String,
    },
    description: String
}, {
    timestamps: true
});

module.exports = mongoose.model('config', schema);
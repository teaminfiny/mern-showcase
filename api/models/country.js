var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const async = require('async');

var schema = new Schema({
    code: String,
    name: String
}, {
    timestamps: true
});

const country = module.exports = mongoose.model('country', schema);

module.exports.addStates = (req, res) => {
    let data = JSON.parse(req.body.data);
    async.eachOfSeries(data, async (eachData, key, cb) => {
        await country.findOneAndUpdate(
            { name: eachData.name }, // find a document with that filter
            { name: eachData.name, code: eachData.code }, // document to insert when nothing was found
            { upsert: true, new: true, runValidators: true });
        if (key === data.length) {
            cb();
        }
    }, (err) => {
        return res.status(200).json({
            title: 'States added successfully',
            error: false,
            detail: []
        });
    });
}

module.exports.getStates = (req, res) => {
    country.find()
        .sort({ name: 1 })
        .exec((err, stateList) => { // callback
            if (err) {
                return res.status(200).json({
                    title: 'Something went wrong.',
                    error: true,
                    detail: err
                });
            } else {
                return res.status(200).json({
                    title: 'States fetched successfully',
                    error: false,
                    detail: stateList
                });
            }
        }
        );
}
var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var schema = new Schema({
    name: String,
    company_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'company'
    },
    product_cat_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'product_category'
    },
    medicine_type_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'medicine_type'
    },
    chem_combination: String, 
    status: String, 
    cost: Number,
    Type: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'productType'
    },
    pack_type: String,
    HSN: String, 
    GST: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'gst'
    },
    images: Array,
    other_details: Array,
    description: String,
    sku:String,
    country_of_origin: String,
    isDeleted:{
        type:Boolean,
        default:false
    },
    surcharge: Number,
    isPrepaid:{
        type:Boolean,
        default:false
    },
}, {
    timestamps: true
});

const productBank = module.exports = mongoose.model('product', schema);

const escapeRegex = (string) => {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

module.exports.list = async(reqData) => {
    //req.body.page,req.body.perPage,req.body.filter,req.body.filter
    page=reqData.page?reqData.page:1
    perPage=reqData.perPage?reqData.perPage:10
    let query = [];
    if(reqData.filter.name && reqData.filter.name != ""){
        let keyValue = escapeRegex(reqData.filter.name)
        query.push({
            $match:{
                'name': { $regex: new RegExp(keyValue, 'i') }
            }
            
        })
    }
    query.push(...[
        {
            $match:{
                'isDeleted':false
            }
        },
        {
            $lookup: {
                localField: 'company_id',
                foreignField: '_id',
                from: 'companies',
                as: 'Company'
            }
        },
        {
            $unwind: "$Company"
        }, 
        {
            $lookup: {
                localField: "product_cat_id",
                foreignField: "_id",
                from: "product_categories",
                as: "ProductCategory"
            }
        },
        { 
            $sort: { name: 1
             } 
        },
        { $unwind: "$ProductCategory" },
        {
            $lookup: {
                localField: 'medicine_type_id',
                foreignField: '_id',
                from: 'medicine_types',
                as: 'MedicineType'
            }
        },
        {
            $unwind: "$MedicineType"
        }, 
        {
            $lookup: {
                localField: 'Type',
                foreignField: '_id',
                from: 'producttypes',
                as: 'Type'
            }
        },
        {
            $unwind: "$Type"
        }, 
        {
            $lookup: {
                localField: 'GST',
                foreignField: '_id',
                from: 'gsts',
                as: 'GST'
            }
        },
        {
            $unwind: "$GST"
        }, 
        {
            $facet: {
                metadata: [{ $count: "total" }, { $addFields: { page: Number(page)} }],
                data: [{ $skip: (Number(perPage) * Number(page)) - Number(perPage) }, { $limit:  Number(perPage) }]
            }
        }
    ]);
    return productBank.aggregate(query).then(result=>result) 
}

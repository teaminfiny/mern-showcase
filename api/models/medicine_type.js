var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name: {
      type: String,
    },
    commission: {
      type: Number,
      required: true
    },
    minOrder: {
      type: Number,
      default: false
    },
    onlyPrepaid:{
      type:Boolean,
      default:false
    },
    exclusive:{
      type:Boolean,
      default:false
    },
    colourCode:{
      type:String,
      enum:['Orange','Blue','Green']
    },
    refundable:{
      type:Boolean,
      default:true
    },
    chgReqAmount: Number
}, {
    timestamps: true
});

const mediCategory = module.exports = mongoose.model('medicine_type', schema);

// name - commission - mini order - color

// Ethical branded - 3 - 2500 - Orange
// cool chain - 3 - 10000 - Blue
// generic - 6 - 2500 - Green
// otc - 6 - 2500 - Green
// surgical  - 6 - 2500 - Green
// PD - 6 - 2500 - Green
const category = [{
    name: "Ethical branded",
    commission: 3,
    minOrder: 2500,
    colourCode: 'Orange',
    exclusive: true,
    chgReqAmount: 2000
  },
  {
    name: "Cool chain",
    commission: 3,
    minOrder: 10000,
    colourCode: 'Blue',
    exclusive: true,
    onlyPrepaid: true,
    refundable: false,
    chgReqAmount: 8000
  },
  {
    name: "Generic",
    commission: 6,
    minOrder: 2500,
    colourCode: 'Green',
    chgReqAmount: 2000
  },
  {
    name: "OTC",
    commission: 6,
    minOrder: 2500,
    colourCode: 'Green',
    chgReqAmount: 2000
  },
  {
    name: "Surgical",
    commission: 6,
    minOrder: 2500,
    colourCode: 'Green',
    chgReqAmount: 2000
  },
  {
    name: "Others",
    commission: 6,
    minOrder: 2500,
    colourCode: 'Orange',
    chgReqAmount: 2000
  },
]
module.exports.addCategory = async(req, res) => {
  category.forEach(element => {
    let medicine = new mediCategory({
      name: element.name,
      commission: element.commission,
      minOrder: element.minOrder,
      colourCode: element.colourCode,
      onlyPrepaid: element.onlyPrepaid,
      exclusive: element.exclusive,
      refundable: element.refundable,
      chgReqAmount: element.chgReqAmount
    })
    medicine.save();
    console.log('=-0=-0',element)
  });
  res.status(200).json({
      error:false,
      title: 'success'
  })
}

module.exports.listMedCategory = (req, res) => {
  mediCategory.find().sort({ name: 1 }).then(data => {
    res.status(200).json({
      error: false,
      data: data
    })
  })
}
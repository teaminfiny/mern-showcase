var mongoose = require('mongoose');
var gst = mongoose.Schema({
    name:String,
    value:Number,
    isDeleted:{
        type:Boolean,
        default:false
    }
})

let Gst = module.exports = mongoose.model('gst',gst);
module.exports.getGst = async()=>{
    let data = await Gst.find({ isDeleted:false }).exec()
    return data
}
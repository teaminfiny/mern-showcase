var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    user_id: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'user'
    },
    acc_number: String,
    ifsc_code: String,
    recipient_name: String,
    last_four_number: String, 
    rounting_number: String,          
    card_details: [{
        cardId: {
            type: String,
            required: false,
        },
        card_name: {
            type: String,
            required: false,
        },
        card_number: {
            type: String,
            required: false,
        },
        expiryMonth: {
            type: String,
            required: false,
        },
        expiryYear: {
            type: String,
            required: false,
        },
        brand: {
            type: String,
            required: false,
        },
        fingerprint: {
            type: String,
            required: false,
        },
        isPrimary: {
            type: Boolean,
            default: false,
            required: false,
        }
    }],
    other_details: {
        type: String,
    },
    description: String
}, {
    timestamps: true
});

module.exports = mongoose.model('bank_account', schema);
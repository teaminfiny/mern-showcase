var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
  key: String,
  value: String
},
{
  timestamps: true
});

const systemConfig = module.exports = mongoose.model('systemConfig', schema);

module.exports.getAllSystemConfig = (req, res) => {
 
  systemConfig.find({ }).then((sysdata) => {
      if(!sysdata) {
          return res.status(200).json({
              title: "Something went wrong, Please try again",
              error: true
          })
      } else {
           res.status(200).json({
              title: "fetched successfully",
              error: false,
              detail: sysdata
          })
      }
  })
}

module.exports.changeGlobalCod = (req, res) => {
  systemConfig.findOneAndUpdate({ key:req.body.key },{$set:{ value:req.body.value }})
  .then((data) =>{
      if(data){
      return res.status(200).json({
          error:false,
          title:req.body.value == '1' ? 'Global Cod is Enabled' : 'Global Cod is Disabled'
      })
  }
  return res.status(200).json({
      error:true,
      title:"Something went wrong"
  })
  })
}
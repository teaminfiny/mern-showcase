var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var schema = new Schema({
    name: String,
    type: String,
    discount_per: String,
    discount_on_product: {
        inventory_id: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'inventory'
        },
        purchase: Number,
        bonus: Number
    }
}, {
    timestamps: true
});

const discounts = module.exports = mongoose.model('discounts', schema);

module.exports.addDiscount = async (req, res, next) => {
    let { purchase, bonus, discount_name, discount_per } = req.body;
    let discount_on_product = {}
    if(discount_name === undefined || discount_name === 'Discount' || discount_name === 'nodiscount' || discount_name === null || discount_name === ''  ){
        purchase=0;
        bonus=0
    }
    if(discount_name !== 'Discount' && discount_name !== 'DifferentAndDiscount' && discount_name !== 'SameAndDiscount'){
        discount_per=0
    }
    let newDiscounts = new discounts({
        name: discount_name,
        type: discount_name,
        discount_per : discount_per ? discount_per : 0,
        discount_on_product: {
            purchase : purchase ? purchase : 0,
            bonus : bonus ? bonus : 0,
        }
    });

    try {
        let saveDiscount = await newDiscounts.save();
        next(null, saveDiscount)
    } catch (error) {
        next(error, null)
    }
}

module.exports.editDiscount = async (id, paramToUpdate, next) => {
    try {
        let discountData = await discounts.findByIdAndUpdate(id, { $set: paramToUpdate }, { 'new': true });
        next(null, discountData)
    } catch (error) {
        next(error, null)
    }
}
